var ids ={
	name: null,
	email: null,
	password: null,
	confirmPassword: null,
	mobile: null,
//	country: null,
	state: null,
	city: null,
//	captcha: null,
//    termsCheckBox: null
};
           
var termsAndConditions = null;


$(document).ready(function(){
	$('.signup-response').hide();
});

   $('body').on('click',"#checkbox2",function(){
//	   dataPass(ids);
	   						   if($('#checkbox2').prop('checked')==true){
								   termsAndConditions=true;
							   }
	   							else{
									termsAndConditions=false;
								}

	   validateAll();
   })
   









				$("#first-name").blur(function(){
                       ids.name= $("#first-name").val();
                       if(ids.name == "")
                       {
						   $('#validateName').css("display","block");
                       }
                        else{
						   $('#validateName').hide();
                       }
                       validateAll();
                   })

					$("#emailId").blur(function(){
						var emailIdLowerCase = $("#emailId").val();
//                       ids.email= $("#emailId").val();
                        ids.email= emailIdLowerCase.toLowerCase();
						console.log('email id to lower',ids.email);
                        
                        var atpos = ids.email.indexOf("@");
                        var dotpos = ids.email.lastIndexOf(".");
                        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=ids.email.length) {
//                            alert("Not a valid e-mail address");
                            $('#validateEmail').css("display","block");
                            return false;
                        }
                        
//                       if(ids.email.indexOf("@") == -1)
//                       {
//						    $('#validateEmail').css("display","block");
//                       }
                        else{
						     $('#validateEmail').hide();
                       }
                       validateAll();
                   })

				   $("#passwordSignup").blur(function(){
                       ids.password= $("#passwordSignup").val();

                       if(ids.password == "")
                       {
						    $('#validatePassword').css("display","block");
                       }
					   
                        else{
						     $('#validatePassword').hide();
                       }
                       validateAll();
                   })
					
					$("#confirmPassword").blur(function(){
                       ids.password= $("#passwordSignup").val();
//					   alert(ids.password);
					    ids.confirmPassword= $("#confirmPassword").val();
//					   alert(ids.confirmPassword);
                       if((ids.password!=ids.confirmPassword)||(ids.confirmPassword == ""))
                       {
						    $('#validateConfirmPassword').css("display","block");
                       }
                        else{
						    $('#validateConfirmPassword').hide();
                       }
                       validateAll();
                   })
				
                   $("#phone").blur(function(){
                       ids.mobile = $("#phone").val();
                       if(ids.mobile == ""||isNaN(ids.mobile)==true||((ids.mobile).length)<10)
                       {
						    $('#validatePhone').css("display","block");
                       }
                       else{
						    $('#validatePhone').hide();
					   }
                       validateAll();

                   })
				   
				    $("#state").blur(function(){
                       ids.state = $("#state").val();
//						alert(ids.country.length);
                       if(ids.state == "")
                       {
						    $('#validateState').css("display","block");
                       }
                       else{
							$('#validateState').hide();
					   }
                       validateAll();

                   })
					
					$("#city").blur(function(){
                       ids.city = $("#city").val();
//						alert(ids.country.length);
                       if(ids.city == "")
                       {
						    $('#validateCity').css("display","block");
//                        jQuery("label[for='validateCity']").html("Invalid City");
                       }
                       else{
						   							$('#validateCity').hide();
// 						jQuery("label[for='validateCity']").html(" ");          
					   }
                       validateAll();

                   })
					
//					$("#captcha").blur(function(){
//                       ids.captcha= $("#captcha").val();
//                       if(ids.captcha == "")
//                       {
//                        jQuery("label[for='validateCaptcha']").html("Re-enter code");
//                       }
//                        else{
//                           jQuery("label[for='validateCaptcha']").html("code entered");
//                       }
//                       validateAll();
//                   })

   var validateAll = function(){
                       if (($("#first-name").val()!="")
						   && ($("#emailId").val()!="")
						   &&($("#phone").val()!="")
						   &&($("#state").val()!="")
						   &&($("#city").val()!="")
//						   &&($("#city").val()!="")
						   &&($("#passwordSignup").val()!="")
						   &&($("#confirmPassword").val()!="")
						   &&(termsAndConditions==true)
						  )
//						   &&($("#captcha").val()!=""))
					   {
//                         	alert('working');
						   console.log('ids',ids)
						   $('#userSubmit1').removeAttr('disabled');
                       }
                       else{
//						   alert("all fields are mandatory,please fill in all fields");
//						   	alert('not working');
						   console.log('ids',ids);
                                          $('#userSubmit1').attr('disabled','disabled');
                       }
                   }

   
   
     $('body').on('click',"#userSubmit1",function(){
	   dataPass(ids);
   	})
   
   
   // alert(esHost);
   var dataPass = function(ids){
   	$.ajax({
        type: "POST",
        url: window.location.origin+'/signup/',
        data: ids,
        success: function(response) {
			responseDisplaySignup(response);
		},
        error: function(error) {
      		console.log('error response',response);
        },
        dataType: "JSON"
    })
   }
   
   
   var responseDisplaySignup = function(response){
//   			$('.signup-response-message').html(JSON.stringify(response));
//	   alert(response.error)
	   if(response.error){
		   $('.signup-response').show();
		   $('.signup-response').removeClass("alert alert-success");
		   $('.signup-response').addClass("alert alert-danger");
	   		$('.signup-response-message').html(response.error);
	   }
	   else{
		   		   $('.signup-response').show();
		   		   $('.signup-response').removeClass("alert alert-danger");
		   		   $('.signup-response').addClass("alert alert-success");
	   		$('.signup-response-message').html('Account was created successfully');
	   }
   }