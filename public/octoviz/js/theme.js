(function($) {
    "use strict";

    // ---------------------------------------------------------------------------------------------------------------------------->
    // GENERAL SCRIPTS FOR ALL PAGES    ||----------- 
    // ---------------------------------------------------------------------------------------------------------------------------->

    $(document).ready(function() {
        $('.column').matchHeight();
        openSite();


    });

    function openSite() {
        fullScreenSlider();
        header();
        scroll();
        winResize();
        pushmenu();
        shortcodeElements();

    };

    // ---------------------------------------------------------------------------------------------------------------------------->
    // RESIZE FUNCTIONS   ||-----------
    // ---------------------------------------------------------------------------------------------------------------------------->
    function winResize() {
        $(window).resize(function() {

        })
    };


})(jQuery);


// ---------------------------------------------------------------------------------------------------------------------------->
// SCROLL FUNCTIONS   ||-----------
// ---------------------------------------------------------------------------------------------------------------------------->

function scroll() {

    // //Click Event to Scroll to Top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }


    });
    $('.scroll-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    // Scroll Down Elements
    $('.scroll-down[href^="#"], .scroll-to-target[href^="#"]').on('click', function(e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function() {
            window.location.hash = target;
        });
    });



};


// ---------------------------------------------------------------------------------------------------------------------------->
// HEADER FUNCTIONS   ||-----------
// ---------------------------------------------------------------------------------------------------------------------------->

// Toggle Function
$('#sign-up').click(function() {
    $('.sign-up').css('display', 'block');
    $('.sign-in').css('display', 'none');
});
$('#sign-in').click(function() {
    $('.sign-up').css('display', 'none');
    $('.sign-in').css('display', 'block');
});
$('.toggle').click(function() {
    $(this).children('i').toggleClass('fa-pencil');

    if ($(this).children('i').hasClass('fa-pencil')) {
        $('.sign-text').html("Sign Up");
    } else {
        $('.sign-text').html("Login");
    }

    // Switches the forms  
    $('.form').animate({
        height: "toggle",
        'padding-top': 'toggle',
        'padding-bottom': 'toggle',
        opacity: "toggle"
    }, "slow");
});

// ---------------------------------------------------------------------------------------------------------------------------->
// HEADER FUNCTIONS   ||-----------
// ---------------------------------------------------------------------------------------------------------------------------->

function header() {

    // Sticky Header Elements
    $(window).scroll(function() {
        if ($(this).scrollTop() > 650) {
            $('.header').addClass("sticky");
            $('.inner-inro').css('z-index', '-1');
        } else {
            $('.header').removeClass("sticky");
            $('.inner-inro').css('z-index', 'auto');
        }
    });
    heightElement();

    function heightElement() {

        // windowWidth = $(window).width();
        var windowHeight = $(window).height();
        if ($(window).width() > 767) {
            // $('.inner-inro').css('height', windowHeight - 200);
        } else {
            // $('.inner-inro').css({ 'height': '400px', 'padding-top': '64px' });
        }
    }

    $(window).resize(function() {
        heightElement();
    });
};


// ---------------------------------------------------------------------------------------------------------------------------->
// PUSHMENU FUNCTIONS (Top, Bottom, Right, Left)  ||-----------
// ---------------------------------------------------------------------------------------------------------------------------->

function pushmenu() {
    $('.toggle-menu').jPushMenu();
};


// ---------------------------------------------------------------------------------------------------------------------------->
// FULLSCREEN SLIDER FUNCTIONS  ||-----------
// ---------------------------------------------------------------------------------------------------------------------------->

function fullScreenSlider() {
    if ($('.fullscreen-carousel').length > 0) {

        $('.fullscreen-carousel').flexslider({
            animation: "slide",
            //  startAt: 0,
            animationSpeed: 700,
            animationLoop: true,
            slideshow: true,
            easing: "swing",
            controlNav: false,
            before: function(slider) {
                //   $('.fullscreen-carousel .overlay-hero .caption-hero').fadeOut();
                $('.fullscreen-carousel .overlay-hero .caption-hero').fadeOut().animate({
                    top: '80px'
                }, {
                    queue: false,
                    easing: 'easeOutQuad',
                    duration: 700
                });
                slider.slides.eq(slider.currentSlide).delay(400);
                slider.slides.eq(slider.animatingTo).delay(400);

            },
            after: function(slider) {
                //$('.fullscreen-carousel .flex-active-slide').find('.caption-hero').delay().fadeIn(1500);
                $('.fullscreen-carousel .flex-active-slide').find('.caption-hero').fadeIn(2000).animate({
                    top: '0'
                }, {
                    queue: false,
                    easing: 'easeOutQuad',
                    duration: 1200
                });



            },
            start: function(slider) {
                $('body').removeClass('loading');

            },
            useCSS: true,
        });
    };
    fullScreenCarousel();

    function fullScreenCarousel() {
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();

        if ($(window).width() > 767) {
            $('.hero-slider-1 .slides li').css("height", windowHeight);
        } else {
            $('.hero-slider-1 .slides li').css("height", '400px');
        }

    };
    $(window).resize(function() {
        fullScreenCarousel();
    });


};




// ---------------------------------------------------------------------------------------------------------------------------->
// SHORTCODE ELEMENTS  ||-----------
// ---------------------------------------------------------------------------------------------------------------------------->

shortcodeElements();

function shortcodeElements() {


    //Parallax Function element
    $('.parallax').each(function() {
        var $el = $(this);
        $(window).scroll(function() {
            parallax($el);
        });
        parallax($el);
    });


    function parallax($el) {
        var diff_s = $(window).scrollTop();
        var parallax_height = $('.parallax').height();
        var yPos_p = (diff_s * 0.5);
        var yPos_m = -(diff_s * 0.5);
        var diff_h = diff_s / parallax_height;

        if ($('.parallax').hasClass('parallax-section1')) {
            $el.css('top', yPos_p);
        }
        if ($('.parallax').hasClass('parallax-section2')) {
            $el.css('top', yPos_m);
        }
        if ($('.parallax').hasClass('parallax-static')) {
            $el.css('top', (diff_s * 1));
        }
        if ($('.parallax').hasClass('parallax-opacity')) {
            $el.css('opacity', (1 - diff_h * 1));
        }

        if ($('.parallax').hasClass('parallax-background1')) {
            $el.css("background-position", 'left' + " " + yPos_p + "px");
        }
        if ($('.parallax').hasClass('parallax-background2')) {
            $el.css("background-position", 'left' + " " + -yPos_p + "px");

        }
    };




    // Skills Progressbar Elements
    skillsProgressBar();

    function skillsProgressBar() {
        $('.skillbar').each(function() {
            $(this).find('.skillbar-bar').animate({
                width: $(this).attr('data-percent')
            }, 2000);
        });
    };



    //Counter
    $('.counter').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');
        $({
            countNum: $this.text()
        }).animate({
            countNum: countTo
        }, {
            duration: 8000,
            easing: 'linear',
            step: function() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum);
                //alert('finished');
            }
        });
    });

};


// Jquery UI Elements
jqueryUi();

function jqueryUi() {



    // Price Filter Slider
    $(function() {

    });
};