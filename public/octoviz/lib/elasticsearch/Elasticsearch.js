function Elasticsearch(esURL, esQueryFunction, esQueryParameters,queryName) {
    this.esURL = esURL;
    this.esQueryFunction = esQueryFunction;
    this.esQueryParameters = esQueryParameters;
    this.queryParameters = {};
    this.queryName=queryName;
    this.prefix = "";
    this.format = "";
    this.indicesString = "";
    this.typeString = "";
    this.isCount = false;
    this.scoreFunctions = [];
    this.indexType = "";
    this.scoringField="";
    this.socialMedia="";
    this.fromDate="";
    this.toDate="";

}

Elasticsearch.prototype = {
    constructor: Elasticsearch,
    addIndexType: function(indexType){
        this.indexType = indexType;

    },
    addQueryParameter: function (key, value) {
        this.esQueryParameters[key] = value;
    },
    setCount: function () {
        this.isCount = true;
    },
    getIndex: function () {
        this.esURL = this.esURL.replace(/\/$/, '');
        var tokens = this.esURL.split(/\//);
        // console.log("tokens...",tokens);
        if (tokens.length == 5) {
            this.indicesString = tokens[3];
            this.typeString = tokens[4];
        } else if (tokens.length == 4) {
            this.indicesString = tokens[3];
        }
        return this.indicesString;
    },
    addNotLookupTermsMatch: function(referanceField,index,type,docID,field){
        this.queryParameters[referanceField] = {
        type: "notTermsMatch",
        params : {
                index: index,
                indexType: type,
                docID: docID,
                field : field,
            referanceField: referanceField
        }
        };  
    },
    addScoringFunction: function(scoringFunction){
    this.scoreFunctions.push(scoringFunction);
    },
    addMultiMatch: function(fields,keyWords){
        if (fields == null) {
            fields = "_all";
        }
        this.queryParameters["_multiMatch"] = {
            type: "multiMatch",
            fields: fields,
            keyWords : keyWords
        };
    },
    getType: function () {
        this.getIndex();
        return this.typeString;
    },
    getQueryName: function () {
        return this.queryName; 
    },

    reset: function(){
        this.queryParameters = {};
    this.scoreFunctions = [];
    },
    isCount: function () {
        return this.isCount;
    },
    applyScoring:function(scoreField,socialMedia){
        this.scoringField=scoreField;
        this.socialMedia=socialMedia;
    },
    search: function (callBack) {
        var restParameters = {
            "index" :this.getIndex(),
            "type": this.getType(),
            "queryParams":this.queryParameters,
            "queryName":this.queryName,
            "liveParams":this.esQueryParameters,
            "socialMedia":this.socialMedia,
            "scoringField":this.scoringField,
            "fromDate":this.fromDate,
            "toDate":this.toDate,
            "prefix":this.prefix,
            "format":this.format
           
        };

        $.ajax({
            type: "POST",
            url: esHostServer+"/_query",
            data:JSON.stringify(restParameters),
            contentType: 'application/json; charset=utf-8',
            success: callBack,
            error : function(err ){ 
                // var str = "User = " + user + " Tag = " + globalTagValue + " URL = " + url + "\n Content= " + JSON.stringify(restParameters) + " \n Response= " + JSON.stringify(msg);
                alertError(err.responseJSON.error);
                // console.log("Call failed with.....",err);
            },
            dataType: "JSON"
        });
    },
    addQueryString: function (field, text) {
        if (field == null) {
            field = "_all";
        }
        if(isWhitespaceOrEmpty(text)){
            text = "*";
        }
        
        this.queryParameters[field] = {
            type: "queryString",
            text: text
        };
    },
    addTermMatch: function (field, value, isShould) {
        var values = [];
        if( typeof value === 'string' ) {
            values = [value];
        }
        else{
            values = value;
        }
        if (!this.queryParameters.hasOwnProperty(field)) {
            this.queryParameters[field] = {
                type: "termMatch",
                values: values,
                isShould: isShould
            };
        } else {
            for(var nValueIndex in values){
                var nValue = values[nValueIndex];
                if(this.queryParameters[field].values.indexOf(nValue) == -1){
                    this.queryParameters[field].values.push(nValue);
                }
            }
        }
    },
    addTermLookupMatch: function (referanceField, index, indexType, docID, field, isShould) {
        this.queryParameters[referanceField] = {
            type: "termLookupMatch",
            isShould: isShould,
            params: {
                referanceField: referanceField,
                index: index,
                indexType: indexType,
                docID: docID,
                field: field
            }
        };
    },
    removeTermMatch: function (field, value) {
        if(isNull(value)){
            delete this.queryParameters[field];
        }
        else if (this.queryParameters.hasOwnProperty(field) && this.queryParameters[field].values.indexOf(value) != -1) {
            this.queryParameters[field].values.splice(this.queryParameters[field].values.indexOf(value), 1);
            if(this.queryParameters[field].values.length == 0){
                delete this.queryParameters[field];
            }
        }
    },
    removeDateRange: function (field) {
        if (this.queryParameters.hasOwnProperty(field)) {
            delete this.queryParameters[field];
        }
    },
    addDateRangeMatch: function (field, from, to) {
        this.queryParameters[field] = {
            type: "dateRange",
            from: from,
            to: to
        };
        console.log("from date ",from);
         console.log("to  date ",to);
         this.fromDate=from;
         this.toDate=to;
     
        // var fromDate = convertDate(from);
        // var toDate = convertDate(to);
        // console.log("F ", fromDate, " T ", toDate);
        // var indices = this.getIndices(fromDate, toDate);
        // console.log("Indices are ", indices);
        // this.indicesString = indices.join(",");
    },
    resetQuery: function (esBody) {
        delete esBody['query'];
        esBody['query'] = {
            "function_score": {
                "query": {
                    "bool": {
                        "must": []
                    }
                },
                "filter": {
                    "bool": {
                        "must": [],
                        "should": [],
            "must_not" : []
                    }
                }
            }
        };
        return esBody;
    },
    update: function (id, data, callBack) {
       
    
        var docData = {
            "docData":{
            "doc": data,
            "upsert": data
             },
             "id":id

        };
         // console.log(docData);
        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            /*url: this.esURL + "/configs/config/" + id + "/_update",*/
            url:esHostServer+"/_update",
            data: JSON.stringify(docData),
            success: callBack,
            error : function(err ){ 
                console.log(err);
                alertError(err.responseJSON.error);
            },
            dataType: "JSON"
        });
    },  
    updateContent: function (data,id,indexType, callBack) {
    
    /*    $.ajax({
            type: "POST",
            url: this.esURL + '/'+indexType+'?op_type=create',
            data: JSON.stringify(data),
            success: callBack,
            dataType: "JSON"
        });*/
        
        var docData = {
        
            "data": data,
            "indexType":indexType
             

        };
          console.log(docData);
        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            /*url: this.esURL + "/configs/config/" + id + "/_update",*/
            url:esHostServer+"/_updateContent",
            data: JSON.stringify(docData),
            success: callBack,
             error : function(err ){ 
                alertError(err.responseJSON.error);
            },
            dataType: "JSON"
        });


    },
    
    addIndexPattern: function (prefix, format) {
        this.prefix = prefix;
        this.format = format;
    },
  /*  getIndices: function (fromDate, toDate) {
    
    fromDate =  new Date(fromDate.getTime()  +  fromDate.getTimezoneOffset() * 60000);
    toDate =  new Date(toDate.getTime()  +  toDate.getTimezoneOffset() * 60000);
        prefix = this.prefix;
        format = this.format;
        var indices = [];
    if(isEmpty(format) ){
        return indices;
    }
        var tempDate = new Date(fromDate);
    console.log("UTC ",fromDate,toDate,tempDate);
        while (tempDate <= toDate) {
             if(prefix=="twitter-"){
                console.log("index");
                var index = prefix +tempDate.getIndex();
             }
             else{
              var index = prefix + moment(tempDate).format(format);
               }
        console.log("UTC ",tempDate,index);
            if (indices.indexOf(index) == -1) {
                if(index.length > 0){
                    indices.push(index);
                }
        if(index.localeCompare(firstAvaiableIndex) == 0){
            break;
        }
            }
            tempDate.setDate(tempDate.getDate() + 1);
        }
   
        var index = indices.indexOf("twitter-2015-53");
         if (index > -1) {
         indices.splice(index, 1);
          }
        console.log('indices', indices);
        return indices;
    },*/
    reindex:  function (id,data, callBack) {
    

         var docData = {
            
            "data":data,
            "id":id

        };
          console.log(docData);
        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            /*url: this.esURL + "/configs/config/" + id + "/_update",*/
            url:esHostServer+"/_reindex",
            data: JSON.stringify(docData),
            success: callBack,
             error : function(err ){ 
                alertError(err.responseJSON.error);
            },
            dataType: "JSON"
        });
    },
    validateQuery: function(callBack){
        // var esBody = this.createQuery();
        // $.ajax({
        //     type: "POST",
        //     data:JSON.stringify(esBody),
        //     url: this.esURL + "/configs/config/_validate/query",
        //     success: callBack,
        //     error: callBack,
        //     dataType: "JSON"
        // });
           var restParameters = {
            "queryParams":this.queryParameters,
            "queryName":this.queryName,
            "liveParams":this.esQueryParameters
           
           };

        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data:JSON.stringify(restParameters),
            url:esHostServer+"/_validateQuery",
            success: callBack,
            error : function(err ){ 
                alertError(err.responseJSON.error);
            },
            dataType: "JSON"
        });
    },
    updateGet: function (id, callBack) {
    
          var docData = {
            
             "id":id

        };
          console.log(docData);
        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            /*url: this.esURL + "/configs/config/" + id + "/_update",*/
            url:esHostServer+"/_updateGet",
            data: JSON.stringify(docData),
            success: callBack,
             error : function(err ){ 
                alertError(err.responseJSON.error);
            },
            dataType: "JSON"
        });
    },	

     updateRest: function (id,index,type,data,callBack) {
       
    
        var docData = {
            "docData":{
            "doc": data,
            "upsert": data
             },
             "id":id,
             "index":index,
             "type":type

        };
       
        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            url:esHostServer+"/_updateRest",
            data: JSON.stringify(docData),
            success: callBack,
            error : function(err ){ 
                console.log(err);
                alertError(err.responseJSON.error);
            },
            dataType: "JSON"
        });
    },  


	 updateSmm: function (host,index,type,id, data, callBack) {
        var docData = {
            "doc": data,
            "upsert": data
        };
        console.log('smm data inside client',docData);
        $.ajax({
            type: "POST",
            url: host +'/' + index + '/'+ type+ '/' + id + "/_update",
            data: JSON.stringify(docData),
            success: callBack,
            dataType: "JSON"
        });
    }, 
    reindexSmm:  function (host,index,type,id,data, callBack) {
        $.ajax({
            type: "POST",
            url: host +'/' + index + '/'+ type+ '/' + id,
            success: callBack,
            data: JSON.stringify(data),
            error: callBack,
            dataType: "JSON"
        });
    },
    updateGetSmm: function (host,index,type,id, callBack) {
        $.ajax({
            type: "GET",
            url: host +'/' + index + '/'+ type+ '/'+ id,
            success: callBack,
            error: callBack,
            dataType: "JSON"
        });

    }
}

