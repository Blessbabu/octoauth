var generateInstagramMainQuery = function (liveParametersInstagram) {
	var size = liveParametersInstagram.size || 10;
	//var granularity = liveParametersFb.granularity || "day";

	var sortVariable = liveParametersInstagram.sortField || "comments.count";
	var granularity = liveParametersInstagram.granularity || "hour";
    
	var queryBodyInsta = {
		"from": liveParametersInstagram.pageNum,
		"size": 21,
		"sort": [
			{
				"comments.count": {
					"order": "desc"
				}
    }
  ],
		"query": {
			"bool": {
				"must": []
			}
		},
		"aggs": {
			"trendingTags": {
				"terms": {
					"field": "tags",
					"size":size
				}
			},
			"graph_data_insta": {
				"date_histogram": {
					"field": "created_time",
					"interval": granularity
				}
			},
			"totalLikes":{
			     "sum": {
				 	"field": "likes.count"
				 }
			},
			"totalComment":{
				"sum" : {
					"field" : "comments.count"
				}
			}
		}
	};
	queryBodyInsta.sort[0] = {};
	queryBodyInsta.sort[0][sortVariable] = {
		"order": "desc"
	};
	return queryBodyInsta;

};