function imgError(image) {
	$(image).remove();
	//alert("hi");
    //image.onerror = "";
    //image.src = "js/instagram/bu.jpg";
    return true;
}

var isFirstSearch = true;
var chartInsta =null;


var searchFieldInsta = null;
var liveParametersInstagram ={
	sortField:null,
	granularity:null,
	pageNum:0
};

var durationInsta = globalDuration;
var esClientInstagram = null;
var instaCharts = [];


function resetOrInitInsta(){
    if(esClientInstagram == null){
        esClientInstagram = new Elasticsearch( esHost + '/instagram', generateInstagramMainQuery,liveParametersInstagram);
    }
    else{
        esClientInstagram.reset();
    }

    esClientInstagram.addTermMatch("ownedBy.name", user);
    esClientInstagram.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esClientInstagram.addDateRangeMatch("created_time", "now-4M", null);
}



globalFunctionChangeInsta = function(globalTagValue){
    handleLoadingInsta(true);
    resetOrInitInsta();
	esClientInstagram.removeTermMatch("ownedBy.query");
	esClientInstagram.addTermMatch("ownedBy.query",globalTagValue);
	esClientInstagram.search(singleRenderFuntionInstagram);
}

// $('#submit-search').on('click', function () {
// 	searchFieldInsta = $("#main-search").val();
// 	esClientInstagram.addQueryString("caption.text", searchFieldInsta);
// 	conductSearchInstagram();
// });
function conductSearchInstagram(isStreaming) {
        if (isNullInsta(isStreaming)) {
        handleLoadingInsta(true);
    }
    esClientInstagram.search(singleRenderFuntionInstagram);
     


};
onSearchInstagram = function(searchField) {
   esClientInstagram.addQueryString("caption.text", searchField);
   conductSearchInstagram();
}

$('#sort-likes').on('click', function () {
	searchFieldInsta = $("#main-search").val();
	liveParametersInstagram.sortField = $("#main-search").val();
	esClientInstagram.addQueryString("caption.text", searchFieldInsta);
	conductSearchInstagram();
});

$('body').on('click', '.sortSelector', function () {
searchFieldInsta = $("#main-search").val();
	var changeVal = ($(this).find('span').html());
//	($(this).find('active').removeClass('active'));
	 $(".sortSelector").removeClass("active");
     $(this).addClass("active");
	$('.sortBy').html(changeVal);
	var like =$(this).hasClass("likeSort");
//     alert(like);
	if(like==true)
		{
		liveParametersInstagram.sortField = "likes.count";
		}	
	else {
		liveParametersInstagram.sortField = "comments.count";
	}
esClientInstagram.addQueryString("caption.text", searchFieldInsta || '*');
	conductSearchInstagram();
});

// $('body').on('click', '.granularityPickerinsta', function() {

//     var currentGranularity = $(this).attr("gValue");
//     $('.granularityTitle span:first').html(currentGranularity);
//     $(".granularityPicker").removeClass("selectedGran");
//     $(this).addClass("selectedGran");
//     var granularityValue = $(this).attr("granularityValue");
//     liveParametersInstagram.granularity = granularityValue;
//     generateInstagramMainQuery(liveParametersInstagram);
//     conductSearchInsatgram();
// });

var tagAddFunction = function (element, identifier) {
    var className = "tag";
    if (identifier == "tag") {

    } else if (identifier == "influencers") {
        className = "tagInf";
    }


    $('#filter-result-instagram').append('<li class="blue tags instaselectedTag" field =' + element.field + ' fieldValue= ' + element.value + '><a>' + element.value + '<span data-role="remove">x</span></a></li>').html();

};

$('body').on('click', '.instaselectedTag', function () {
    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClientInstagram.removeTermMatch(field, value);
    conductSearchInstagram();
    $(this).remove();
});

// $('body').on('click', '.downloadLink', function (e) {
// 	e.preventDefault();
//     var downloadToken = $(this).attr('downloadToken');
//     var blob = new Blob([csvInsta[downloadToken]], {
//         type: "text/plain;charset=utf-8"
//     });
//     saveAs(blob, "downloadToken.csv");
// });


var clickedFiltersInsta = [];
//
//$('body').on('click', '.timeFilter', function () {
//	$(".timeFilter").removeClass("active");
//	$(this).addClass("active");
//	var duration = $(this).attr("duration");
//	var name =$(this).attr("name");
//	$('.time-select span').html('<i class="fa fa-clock-o"></i>'+name);
//
//});

onTimeChangeInsta = function(globalDuration){

   
	esClientInstagram.addDateRangeMatch("created_time", "now-" + globalDuration, null);
	conductSearchInstagram();
}


$('body').on('click', '.checkbox', function () {
    var testingTit = $(this).children(".trends1").attr("id");
    var testingIpt = $(this).children(".trends2").attr("id");
    var testingInt = $(this).children(".trends3").attr("id");
    var identifier = $(this).attr('fieldType');

    if (($("#" + testingTit).is(':checked')) ||
        ($("#" + testingIpt).is(':checked')) ||
        ($("#" + testingInt).is(':checked'))) {

        var name = $(this).attr('value');
        clickedFiltersInsta.push({
            "field": "tags",
            "value": name
        });
        tagAddFunction({
            "field": "tags",
            "value": name
        }, identifier);
        esClientInstagram.addTermMatch("tags", name);
        conductSearchInstagram();
    }



});



function queryFunctionInstagram() {
	conductSearchInstagram();
	
};

function isNullInsta(obj) {
	if (typeof obj === 'undefined' || obj == null) {
		return true;
	}
	return false;
}



function initTabInstagram() {
    resetOrInitInsta();
    conductSearchInstagram();
};
var singleRenderFuntionInstagram = function (response) {
    instaCharts=[];
    console.log("singleRender response",response);
	
	    handleLoadingInsta(false);
		
		$('#galleryherepls').empty();
		graphInsta(response);
		imagePushingFunction(response);
		printFunctionInsta(response);
		trendsTable(response);
    	$('.column').matchHeight();

       
	
	
};

var printFunctionInsta = function (response){
	$('#totalPhotos').html(response.hits.total);
	$('#likesInsta').html(response.aggregations.totalLikes.value);
	$('#commentsInsta').html(response.aggregations.totalComment.value);
};



var trendsTable = function(response){
	$('#pageDetails-1').empty();
var bucketIndex =0;
var tdaTrends = [];
var totalBuckets = response.aggregations.trendingTags.buckets
for(bucketIndex in totalBuckets){
  tdaTrends.push({
    name: totalBuckets[bucketIndex].key,
	count:totalBuckets[bucketIndex].doc_count
  });
}
if(tdaTrends.length<1){
    $(".dataContentInstaTable").html("No data Available");   
    }
	console.log(tdaTrends);
		$("#pageDetails-1").html($("#pageDetails1").render(tdaTrends.slice(0,10)));
		// $("#pageDetails-2").html($("#pageDetails2").render(tdaTrends.slice(3,6)));
		// $("#pageDetails-3").html($("#pageDetails3").render(tdaTrends.slice(6,9)));
		$('span.numbers').digits();

}


function handleLoadingInsta(isLoading) {
    if (isLoading==true) {
       
   
    $(".dataContentInstaGallerya").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
    $(".dataContentInstaTrend").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
    $(".dataContentInstaTable").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

    } 
    if (isLoading==false) {

    $(".dataContentInstaGallerya").empty();
    $(".dataContentInstaTrend").empty();
    $(".dataContentInstaTable").empty();

    }
    
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var imagePushingFunction = function(response){
	var index = 0;
	var totalHits = response.hits.hits;
    var totalHitsCount =response.hits.total;
     if(totalHits.length<1){
    $(".dataContentInstaGallerya").html("No data Available");   
    }
	for(index in totalHits){
		var caption = totalHits[index]._source.caption;
        var text = "";
        var fullName = "";
		if((typeof caption === 'undefined' || caption == null)){
            caption = "";
            text = "";
            fullName = "";
        }
        else{
            text = totalHits[index]._source.caption.text;
            fullName = totalHits[index]._source.caption.from.full_name;
        }
        var height = totalHits[index]._source.images.standard_resolution.width * (.8 +  .2 * (index%2)/2) ;
         
        $('#galleryherepls').prepend('<img  onerror="imgError(this);"'
                                 +  ' height=' + height 
                                 +  '     src='+totalHits[index]._source.images.standard_resolution.url 
                                 +  " data-fullsrc=" + totalHits[index]._source.images.standard_resolution.url  
                                 +    " title='" + fullName  +   "' data-desc='" + text  + "' />");
	}
    $('#galleryherepls').galereya();
  
	
	if(isPaginationTriggered == false){
						addPagination(totalHitsCount);
						}
						isPaginationTriggered = false;


};

var graphInsta = function (response) {
   		
 //    var data = []
 //    var viewTrendInsta = [];
 //    for (var bucketIndex in response.aggregations.graph_data_insta.buckets) {
 //        var bucket = response.aggregations.graph_data_insta.buckets[bucketIndex];
 //        var milliSecondValue = bucket.key;
 //        var tweetCount = 0;

 //        viewTrendInsta.push({
 //            series: 0,
 //            x: (milliSecondValue),
 //            y: bucket.doc_count
 //        });
 //    }
 //    var dataBucket = {
 //        area: true,
 //        key: "Posts",
 //        seriesIndex: 0,
 //        values: viewTrendInsta
 //    };
 //    data.push(dataBucket);
	// console.log(data);


    var totalTrendsInsta = [];
    for (var bucketIndex in response.aggregations.graph_data_insta.buckets) {
        var bucket =response.aggregations.graph_data_insta.buckets[bucketIndex];

        var date = new Date(bucket.key);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        date = day + '-' + month + '-' + year;

        totalTrendsInsta.push({

            date: date,
            total:bucket.doc_count
        });
    }

    graphTrendInsta(totalTrendsInsta);


   
};

function graphTrendInsta(data) {
    if(data.length<1){
    $(".dataContentInstaTrend").html("No data Available");   
    }
	$('#instaTrendGraph').empty();
   var chart = AmCharts.makeChart("instaTrendGraph", {
        "type": "serial",
        "theme": "light",
        "marginRight": 30,
        // "legend": {
        //     "equalWidths": false,
        //     "periodValueText": "total: [[value.sum]]",
        //     "position": "top",
        //     "valueAlign": "left",
        //     "valueWidth": 100
        // },
        "dataProvider": data,

        "graphs": [{
            "balloonText": "<span><b>Total :</b></span><span style='font-size:14px; color:#000000;'><b>[[total]]</b></span>",


            "fillAlphas": 0.6,
            "lineAlpha": 0.4,
            "title": "Total",
            "fillColors": "#50b6b6",
            "valueField": "total"
        }],
        "plotAreaBorderAlpha": 0,
        "marginTop": 10,
        "marginLeft": 0,
        "marginBottom": 0,
        "chartCursor": {
            "cursorAlpha": 0
        },
        "categoryField": "date",
        "categoryAxis": {
            "startOnAxis": true,
            "axisColor": "#DADADA",
            "gridAlpha": 0.07,
            "title": "date"

        },

        "export": {
            "enabled": true
        }
    });
   instaCharts.push(chart);
};

var cp=1;
var isPaginationTriggered = false;

var addPagination = function(totalHitsCount) {
$("#pagination").pagination({
		items: totalHitsCount,
		itemsOnPage: 10,
		cssStyle: 'dark-theme',
		currentPage: cp,
		displayedPages: 10,
		onPageClick: function (a) {
			cp=a;
			console.log(a);
			isPaginationTriggered = true;
			esClientInstagram.addQueryParameter("pageNum" ,(cp - 1) * 10);
			conductSearchInstagram();
			return false;
		}
	});
}


var layoutInstagram = {
        /*
        ** Array of objects or strings which represents the output
        */
    content: [
        {
            text: "Instagram Trends",
            color:"#3A907F"
        }, {
            image: "image_1", // reference to the image mapping below
            fit: [523.28, 769.89] // fits the image to those dimensions (A4)
        }
         
         

    ],


    //  ** Mapping object, holds the actual imagery

    images: {

        }
}
function instagramCreateReport() {
  var pdf_images = 0;
  var pdf_layout = layoutInstagram; // loaded from another JS file
  for (var i = 0; i <instaCharts.length; i++ ) {
     var chart =instaCharts[ i ];
    // Capture current state of the chart
    chart.export.capture( {}, function() {

      // Export to PNG
      this.toJPG( {
        // pretend to be lossless
         
        // Add image to the layout reference
      }, function( data ) {
        pdf_images++;
        pdf_layout.images[ "image_" + pdf_images ] = data;

        // Once all has been processed create the PDF
        if ( pdf_images == instaCharts.length ) {

          // Save as single PDF and offer as download
          this.toPDF( pdf_layout, function( data ) {
            this.download( data, "application/pdf", "amcharts.pdf" );
          } );
        }
      } );
    } );
  }
}
