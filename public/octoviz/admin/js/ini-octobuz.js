// JavaScript Document
$.ajaxPrefilter(function(options) {
    "use strict";
    if (!options.beforeSend) {
        options.beforeSend = function(xhr) {
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        };
    }
});

var globalConfig = null;
//var globalTagValue = "Neeraj Ghaywan";
//var globalTagValue = "null";
//var globalTagValue = "fbone";

var globalTagValue = "Ghana Police Octobuz";
//var globalTagValue = "BokoHaram";

var globalEsClient = new Elasticsearch(esHost);
var processTagChange = null;
var tagValuesArray = null;

function updateGlobalSource(callBack) {
    globalEsClient.updateGet(user, function(response) {
        globalConfig = response;
        console.log('global config',globalConfig);
        console.log(response);
        if (isNull(response._source)) {
            globalConfig = {
                _source: null
            };
        }
        if (!isNull(callBack)) {
            callBack();
        }
    });
}

//updateGlobalSource();
console.log("Fetching source")
$(document).ready(function(){
updateGlobalSource(function() {
    globalTagValue = initialTagValueGen(globalConfig);
  //  alert("onload"+globalTagValue);
    loadInnerHTMLPage();
});
});
$('body').on('click', '.contentTags', function() {
    alertInfo("Changed Tab");
    globalTagValue = $(this).attr("value");
    $('.tagsAddition').html(globalTagValue);
    if (!isNull(globalFunctionChange)) {
        console.log("Triggering global tag change fn");
        globalFunctionChange(globalTagValue);
    }
});

var globalDuration = "1w";
var globalStartDuration="now-1w";
var globalEndDuration=null;
var duration = "1w";
var onTimeChange = null;


var mainSearch = null;
$('search-btn').on('click', function() {
    searchField = $(".search-box").val();
    esClient.addQueryString("text", searchField);
    streamingClient.addQueryString("text", searchField);
    liveTweetQueryFn();
    liveParameters.foamtreeQuery = searchField;
    esFoamTree.addQueryString("text", searchField);
    createFoamTree(foamtreeId.listening, esFoamTree);
    emptyPieChartValuesOnChange();
    mainSearch(globalSearch);
});

