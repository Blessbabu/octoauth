//ES data request


var foamtreeHome = null;
var currentFoamTreeId;
var csv = {
    "foamTreeCsv": ""
}

var generatemediaFoamMainQuery = function(liveParameters) {
    var ftQuery = liveParameters.foamtreeQuery || "*";
    var request = {
        "search_request": {
            "fields": ["text"],
            "query": {
                "bool": {
                    "must": [{
                        "query_string": {
                            "query": ftQuery
                        }
                    }]
                }
            },
            "sort": [{
                "created_at": {
                    "order": "desc"
                }
            }],
            "size": 1000 //jobin size
        },

        "query_hint": "*",
        "algorithm": "lingo",
        "max_hits": 10,
        "include_hits": false,
        "field_mapping": {
            "title": ["fields.text"],
            "content": ["fields.text"],
            "url": ["fields._id"]
        }
    };
    return request;
}




var esFoamTree = new FoamTreeSearch(esHost, generatemediaFoamMainQuery, {}, "twitterFoamTreeQuery");
esFoamTree.addQueryString("_all", "*");
esFoamTree.addTermMatch("tweetType", "tweet");
// esFoamTree.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
esFoamTree.addIndexPattern("twitter-", twitterTimeFormat);
esFoamTree.addDateRangeMatch("created_at", globalStartDuration, globalEndDuration);

// var esFoamTreeCluster =new ElasticsearchClusterClient(esHost +"/" +  esFoamTree.indicesString , generatemediaFoamMainQuery,{});

var foamTreeIDMap = {};
var idList = [];

function setFoamTree(input, foamtreeIds, result) {

    // foamtreeHome = null;
    if (foamtreeHome)
        foamtreeHome.dispose();
    // alert("entered");
    // alert(foamtreeIds);
    $(".dataContentFoamtreeListening").empty();
    //  console.log("result from setfoamtree",result);
    //var foamtreeHome = foamTreeIDMap[foamtreeIds];
    // if (currentFoamTreeId!==foamtreeIds) {
    // alert("xf");
    //alert("not null");
    $(".dataContentFoamtreeListening").empty();
    //    $("#foamtreeHome").detach();
    //    loadingFuntion(foamtree);
    foamtreeHome = new CarrotSearchFoamTree({
        id: foamtreeIds,
        backgroundColor: "#fff",
        logging: true,
        wireframePixelRatio: 1,
        layout: "squarified",
        layoutByWeightOrder: true,
        showZeroWeightGroups: true,
        groupMinDiameter: 10,
        rectangleAspectRatioPreference: -1,
        relaxationInitializer: "ordered",
        relaxationMaxDuration: 3000,
        relaxationVisible: false,
        relaxationQualityThreshold: 1.03,
        stacking: "hierarchical",
        descriptionGroupType: "floating",
        descriptionGroupPosition: 225,
        descriptionGroupDistanceFromCenter: 1,
        descriptionGroupSize: 0.22,
        descriptionGroupMinHeight: 53,
        descriptionGroupMaxHeight: 0,
        descriptionGroupPolygonDrawn: true,
        maxGroups: 50000,
        maxGroupLevelsDrawn: 4,
        maxGroupLabelLevelsDrawn: 3,
        groupGrowingDuration: 0,
        groupGrowingEasing: "bounce",
        groupGrowingDrag: 0,
        groupResizingBudget: 2,
        groupBorderRadius: 0,
        groupBorderWidth: 0,
        groupBorderWidthScaling: 0.6,
        groupInsetWidth: 0,
        groupBorderRadiusCorrection: 1,
        groupSelectionOutlineWidth: 5,
        groupSelectionOutlineColor: "#fff",
        groupSelectionOutlineShadowSize: 2,
        groupSelectionOutlineShadowColor: "#000",
        groupSelectionFillHueShift: 0,
        groupSelectionFillSaturationShift: 0,
        groupSelectionFillLightnessShift: 0,
        groupSelectionStrokeHueShift: 0,
        groupSelectionStrokeSaturationShift: 0,
        groupSelectionStrokeLightnessShift: -10,
        groupFillType: "plain",
        groupFillGradientRadius: 1.2,
        groupFillGradientCenterHueShift: 0,
        groupFillGradientCenterSaturationShift: 0,
        groupFillGradientCenterLightnessShift: 30,
        groupFillGradientRimHueShift: 0,
        groupFillGradientRimSaturationShift: 0,
        groupFillGradientRimLightnessShift: 0,
        groupStrokeType: "plain",
        groupStrokeWidth: 2.5,
        groupStrokePlainHueShift: 0,
        groupStrokePlainSaturationShift: 0,
        groupStrokePlainLightnessShift: -10,
        groupStrokeGradientRadius: 1,
        groupStrokeGradientAngle: 45,
        groupStrokeGradientUpperHueShift: 0,
        groupStrokeGradientUpperSaturationShift: 0,
        groupStrokeGradientUpperLightnessShift: 20,
        groupStrokeGradientLowerHueShift: 0,
        groupStrokeGradientLowerSaturationShift: 0,
        groupStrokeGradientLowerLightnessShift: 0,
        groupHoverFillHueShift: 0,
        groupHoverFillSaturationShift: 0,
        groupHoverFillLightnessShift: 20,
        groupHoverStrokeHueShift: 0,
        groupHoverStrokeSaturationShift: 0,
        groupHoverStrokeLightnessShift: 10,
        groupExposureScale: 1.15,
        groupExposureShadowColor: "#000",
        groupExposureShadowSize: 50,
        groupExposureZoomMargin: 0.1,
        groupUnexposureLightnessShift: -50,
        groupUnexposureSaturationShift: -65,
        groupUnexposureLabelColorThreshold: 0.15,
        exposeDuration: 700,
        exposeEasing: "squareInOut",
        groupContentDecoratorTriggering: "onLayoutDirty",
        openCloseDuration: 500,
        rainbowColorDistribution: "linear",
        rainbowColorDistributionAngle: 45,
        rainbowLightnessDistributionAngle: 45,
        rainbowSaturationCorrection: 0.1,
        rainbowLightnessCorrection: 0.4,
        rainbowStartColor: "hsla(164, 83%, 36%, 1)",
        rainbowEndColor: "hsla(176, 70%, 87%, 1)",
        rainbowLightnessShift: 30,
        rainbowLightnessShiftCenter: 0.4,
        parentFillOpacity: 0.7,
        parentStrokeOpacity: 1,
        parentLabelOpacity: 1,
        parentOpacityBalancing: true,
        wireframeDrawMaxDuration: 15,
        wireframeLabelDrawing: "auto",
        wireframeContentDecorationDrawing: "auto",
        wireframeToFinalFadeDuration: 500,
        wireframeToFinalFadeDelay: 300,
        finalCompleteDrawMaxDuration: 80,
        finalIncrementalDrawMaxDuration: 100,
        finalToWireframeFadeDuration: 200,
        androidStockBrowserWorkaround: false,
        incrementalDraw: "fast",
        groupLabelFontFamily: "sans-serif",
        groupLabelFontStyle: "normal",
        groupLabelFontWeight: "normal",
        groupLabelFontVariant: "normal",
        groupLabelLineHeight: 1.05,
        groupLabelHorizontalPadding: 1,
        groupLabelVerticalPadding: 1,
        groupLabelMinFontSize: 6,
        groupLabelMaxFontSize: 160,
        groupLabelMaxTotalHeight: 0.9,
        groupLabelUpdateThreshold: 0.05,
        groupLabelDarkColor: "#000",
        groupLabelLightColor: "hsla(173, 73%, 78%, 1)",
        groupLabelColorThreshold: 0.35,
        rolloutStartPoint: "center",
        rolloutEasing: "squareOut",
        rolloutMethod: "groups",
        rolloutDuration: 2000,
        rolloutScalingStrength: -0.7,
        rolloutTranslationXStrength: 0,
        rolloutTranslationYStrength: 0,
        rolloutRotationStrength: -0.7,
        rolloutTransformationCenter: 0.7,
        rolloutPolygonDrag: 0.1,
        rolloutPolygonDuration: 0.5,
        rolloutLabelDelay: 0.8,
        rolloutLabelDrag: 0.1,
        rolloutLabelDuration: 0.5,
        rolloutChildGroupsDrag: 0.1,
        rolloutChildGroupsDelay: 0.2,
        pullbackStartPoint: "center",
        pullbackEasing: "squareIn",
        pullbackMethod: "individual",
        pullbackDuration: 1500,
        pullbackScalingStrength: -0.7,
        pullbackTranslationXStrength: 0,
        pullbackTranslationYStrength: 0,
        pullbackRotationStrength: -0.7,
        pullbackTransformationCenter: 0.7,
        pullbackPolygonDelay: 0.3,
        pullbackPolygonDrag: 0.1,
        pullbackPolygonDuration: 0.8,
        pullbackLabelDelay: 0,
        pullbackLabelDrag: 0.1,
        pullbackLabelDuration: 0.3,
        pullbackChildGroupsDelay: 0.1,
        pullbackChildGroupsDrag: 0.1,
        pullbackChildGroupsDuration: 0.3,
        fadeDuration: 1200,
        fadeEasing: "cubicInOut",
        zoomMouseWheelFactor: 1.5,
        zoomMouseWheelDuration: 500,
        zoomMouseWheelEasing: "squareOut",
        maxLabelSizeForTitleBar: 0,
        titleBarFontFamily: null,
        titleBarFontStyle: "normal",
        titleBarFontWeight: "normal",
        titleBarFontVariant: "normal",
        titleBarBackgroundColor: "rgba(0, 0, 0, 0.5)",
        titleBarTextColor: "rgba(255, 255, 255, 1)",
        titleBarMinFontSize: 0,
        titleBarMaxFontSize: 0,
        titleBarTextPaddingLeftRight: 20,
        titleBarTextPaddingTopBottom: 15,
        attributionText: null,
        attributionLogo: null,
        attributionLogoScale: 0.1,
        attributionUrl: "http://carrotsearch.com/foamtree",
        attributionPosition: 45,
        attributionDistanceFromCenter: 1,
        attributionWeight: 0.025,
        attributionTheme: "light",
        interactionHandler: "builtin",
        imageData: null,
        viewport: null,
        times: null
    });
    // foamTreeIDMap[foamtreeIds] = foamtreeHome;
    currentFoamTreeId = foamtreeIds;
    // }

    console.log("foamtree Home", foamtreeHome);
    //  loadingFuntion(foamtree);
    //$("#foamtree").empty();
    foamtreeHome.set("onGroupClick", function(info) {
        slimScrollCall("slimScrollComboModalTweet", "500px");
        slimScrollCall("slimScrollComboModalHome", "500px");

        console.log('clicked group info', info);
        console.log('foamtreeIds', foamTreeIDMap);
        var ids = [];
        var topic = "";
        var sentiments = {};
        console.log('current results', currentResults);
        for (var clusterIndex in currentResults.clusters) {
            var cluster = currentResults.clusters[clusterIndex];
            if (cluster.label == info.group.label) {
                ids = cluster.documents;
                topic = cluster.label;
                sentiments = cluster.sentiments;
            }
        }
        console.log("foamtree ids are", ids);
        console.log("foamtree ids are twitter", ids);
        console.log("global start duration", globalStartDuration);
        console.log("global end duration", globalEndDuration);
        idList = [];
        for (var i = 0; i < ids.length; i++) {
            idList.push(ids[i]);
        }
        // alert("dwfre");
        //  esFoamTreeCluster.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);
        //   esFoamTreeCluster.addIndexPattern("twitter-", twitterTimeFormat);
        esFoamTreeCluster.search(ids, 10, modalRenderFunction);
    });
    foamtreeHome.set("dataObject", input);


};

$('body').on('click', '.sizePickerModalAnalytics', function() {
    //    alert("x");
    $(".sizePickerModalAnalytics").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    //  alert('hi');
    esFoamTreeCluster.search(idList, sizeValue, modalRenderFunction);

});


var modalRenderFunction = function(response) {
    $(".dataContentModal").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
    csv.foamTreeCsv = "Tweet Text,Sentiments,Author,Date\n" +
        convertBucketsToCSVFoamtree(response);

    //  csv.tweetsCSV = "Trending Tweets\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
    //       convertBucketsToCSVRetweets(response.aggregations.trendingTweets.hits);
    console.log('modal window render is', response);
    var docStatsArray = [];
    var totalHitsCount = response.hits.total;
    var hit = response.hits.hits;
    var sentimentValue = null;
    for (var index in hit) {
        docStatsArray.push({
            text: hit[index]._source.text,
            senti: hit[index]._source.senti.overallSentiment,
            screen_name: hit[index]._source.user.screen_name,
            id: hit[index]._id,
            author: hit[index]._source.user.name,
            authorShort: ((hit[index]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            image: hit[index]._source.user.profile_image_url,
            date: utcToDateFormat(hit[index]._source.created_at)
        });

    }
    console.log('modal response',docStatsArray);
    $("#docStats").empty();
    $("#docStats").html($("#docStatsTable").render(docStatsArray));
    leftRightSwitchingFoamtree("trt", totalHitsCount);
    for (var index in hit) {
        sentimentValue = hit[index]._source.senti.overallSentiment;
        var indexConv = Number(index);
        sentimentColorChangeFunction(sentimentValue, indexConv);
    }

    $(".dataContentModal").empty();
    $('#myModal').modal('show');
    $(".dataContentModal").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');

    //    $("#myModal").modal('show');
};

var sentimentColorChangeFunction = function(sentimentValue, index) {

    if (sentimentValue == "POSITIVE") {
        $(".tweet-senti-foam-" + index).addClass('tweet-foam-positive');
    }
    if (sentimentValue == "NEGATIVE") {
        $(".tweet-senti-foam-" + index).addClass('tweet-foam-negative');
    }
    if (sentimentValue == "MIXED") {
        $(".tweet-senti-foam-" + index).addClass('tweet-foam-mixed');
    }
    if (sentimentValue == "NEUTRAL") {
        $(".tweet-senti-foam-" + index).addClass('tweet-foam-neutral');
    }

}

var leftRightSwitchingFoamtree = function(id, totalAggsCount) {
    //    alert(id);
    for (var i = 0; i < totalAggsCount; i++) {
        if (i % 2 == 0) {
            $('div.left-right-switcher-' + id + '' + i + '').addClass('pull-left');
        } else {
            $('div.left-right-switcher-' + id + '' + i + '').addClass('pull-right');
            $('div.left-right-switcher-second-' + id + '' + i + '').addClass('on-left');
        }
    }
};
var convertBucketsToCSVFoamtree = function(response) {
    //console.log('response',response);
    var csvInside = "";
    if (!isNull(response.hits)) {
        //        var totalHits = response.hits.hits;
        var hitArray = response.hits.hits;
        var hitArrayLength = hitArray.length;
        console.log('hits array', hitArray);
        //        var hitIndex = 0;
        for (var hitIndex in hitArray) {
            console.log('hitindex', hitIndex)
            var hit = hitArray[hitIndex];
            if (isNull(hit) || isNull(hit._source)) {
                csvInside += "";
                continue;
            }
            csvInside += (hit._source.text.replace(/,/g, '')).replace(/(\r\n|\n|\r)/gm, "") + ',' + hit._source.senti.overallSentiment + ',' + hit._source.user.name + ',' + utcToDateFormat(hit._source.created_at) + '\n';
        }
    }

    console.log('csvInside build', csvInside);
    return csvInside;

};

$('body').on('click', '.downloadLinkModalFoamTwitter', function() {
    var downloadToken = $(this).attr('downloadtokenAnalyticsTwitter');
    //    alert(downloadToken);
    var blob = new Blob([csv[downloadToken]], {
        type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "downloadToken.csv");
});


var createFoamTree = function(foamtreeIds, searchInstance) {

    // alert("done");
    var index = "octobuzz";
    var indexType = "twitter";


    // esFoamTree.addQueryString("_all","*");
    // esFoamTree.addTermMatch ("tweetType","tweet");
    // esFoamTree.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    // esFoamTree.addIndexPattern("twitter-",twitterTimeFormat);
    // esFoamTree.addDateRangeMatch("created_at", "now-" + globalDuration, null);
    searchInstance.search(function(result) {
        console.log('foamtree', result);
        if ((result.hits.hits).length != 0) {
            $(".dataContentFoamTree").empty();
            currentResults = result;
            var idx = -1;
            for (var index in result.clusters) {
                if (result.clusters[index].label == "Other Topics") {
                    idx = index;
                }
            }
            if (idx != -1) {
                result.clusters.splice(idx, 1);
            }
          var negatedKeywords=foamTreeNegationKeywords("twitter");
          console.log("negatedKeywords twitter.......",negatedKeywords);
          result=removeKeywordsFoamTree(negatedKeywords,result);
            function calculateUniqueDocumentsCount(cluster) {
                var uniqueIds = {};
                if (cluster.documents) {
                    cluster.documents.forEach(function(id) {
                        uniqueIds[id] = true;
                    });
                }

                if (cluster.clusters) {
                    cluster.clusters.forEach(function(subcluster) {
                        for (var key in calculateUniqueDocumentsCount(subcluster)) {
                            uniqueIds[key] = true;
                        };
                    });
                }
                cluster.uniqueDocumentsCount = Object.keys(uniqueIds).length;
                return uniqueIds;

            }

            result.clusters.forEach(function(cluster) {
                calculateUniqueDocumentsCount(cluster);
            });

            // Convert the results to the format required by the visualization:
            // http://download.carrotsearch.com/circles/demo/api/#dataObject
            var visualizationInput = {
                groups: result.clusters.map(function mapper(cluster) {
                    return {
                        label: cluster.phrases[0],
                        weight: cluster.uniqueDocumentsCount,
                        groups: (cluster.clusters || []).map(mapper)
                    }
                })
            };
            if (result.info.algorithm == "lingo") {
                setFoamTree(visualizationInput, foamtreeIds, result);
            }

        } else {
            //    $('#'+foamtreeIds+'').empty();
            setFoamTree(visualizationInput, foamtreeIds, result);
            //    $('#foamtreeListening').html("No data Available");
            $(".dataContentFoamtreeListening").empty();
            $(".dataContentFoamtreeListening").html("No data Available");

        }


    });
}