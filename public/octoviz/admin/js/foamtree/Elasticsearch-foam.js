/*function ElasticsearchClusterClient(esURL, esQueryFunction, esQueryParameters) {
	this.esURL = esURL;
	this.esQueryFunction = esQueryFunction;
	this.esQueryParameters = esQueryParameters;
	this.queryParameters = {};
}

ElasticsearchClusterClient.prototype = {
	constructor: ElasticsearchClusterClient,
	addQueryParameter: function (key, value) {
		this.esQueryParameters[key] = value;
	},
	search: function (ids,size, callBack) {
		var esBody = this.esQueryFunction(this.esQueryParameters);
		var size = size;
		esBody = {
			"size": size,
			query: {
					"ids": {
						"values": ids
					}
			}
		};
		$.ajax({
			type: "POST",
			url: this.esURL + "/_search",
			data: JSON.stringify(esBody),
			success: callBack,
			dataType: "JSON"
		});
	}
}
*/

function ElasticsearchClusterClient(esURL, esQueryFunction, esQueryParameters) {
	this.esURL = esURL;
	this.esQueryFunction = esQueryFunction;
	this.esQueryParameters = esQueryParameters;
	this.queryParameters = {};
    this.prefix = "";
    this.format = "";
    this.fromDate="";
    this.toDate="";
}

ElasticsearchClusterClient.prototype = {
	constructor: ElasticsearchClusterClient,
	addQueryParameter: function (key, value) {
		this.esQueryParameters[key] = value;
	},

	   addIndexPattern: function (prefix, format) {
        this.prefix = prefix;
        this.format = format;
    },

       
    addDateRangeMatch: function (field, from, to) {
         this.fromDate=from;
         this.toDate=to;
    },


	search: function (ids,size, callBack) {
		// alert("dsfgdh");
		// var esBody = this.esQueryFunction(this.esQueryParameters);
		var size = size;

		   // var restParameters = {
     //        "queryParams":{"ids":ids,size:size},
     //        "queryName":this.queryName,
     //        // "liveParams":this.esQueryParameters,
     //        "fromDate":this.fromDate,
     //        "toDate":this.toDate,
     //        "prefix":this.prefix,
     //        "format":this.format
           
     //    };
        // alert("cvdgh");
		esBody = {
			"size": size,
			query: {
					"ids": {
						"values": ids
					}
			}
		};


		$.ajax({
			type: "POST",
			url: this.esURL + "/_search?ignore_unavailable=true",
			data: JSON.stringify(esBody),
			success: callBack,
			dataType: "JSON"
		});
	}
}
