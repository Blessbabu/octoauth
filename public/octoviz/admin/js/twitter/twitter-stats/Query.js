var generateTwitterMainQuery = function (liveParameters) {
	var size = liveParameters.size || 10;
	var granularity = liveParameters.granularity || "hour";
	var queryBody =
{
  "size": 10,
  "aggs": {
	  "negative": {
				"filter": {
					"term": {
						"senti.overallSentiment": "negative"
					}
				},
				"aggs": {
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			},
			"positive": {
				"filter": {
					"term": {
						"senti.overallSentiment": "positive"
					}
				},
				"aggs": {
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			},
    "uniqueUsers": {
      "cardinality": {
        "field": "user.id"
      }
    },
    "topMentions": {
      "terms": {
        "field": "mention.screen_name",
        "size":15
      }
    },
    "retweetsCount": {
      "filter": {
        "term": {
          "tweetType": "tweet"
        }
      },
      "aggs": {
        "retweetSum": {
          "sum": {
            "field": "retweet_count"
          }
        },
        "favoriteSum": {
          "sum": {
            "field": "favorite_count"
          }
        },
        "followers": {
          "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
          }
        }
      }
    },
    "languages": {
      "terms": {
        "field": "language",
        "size": 50
      }
    },
    "locations": {
      "terms": {
        "field": "place.country_code"
      }
    },
    "tweetTypes": {
      "terms": {
        "field": "tweetType"
      },
        "aggs":{
            "byDays": {
			  "terms": {
				"script": "dateConversion",
				"params": {
				  "date_field": "created_at",
				  "format": "EEEEEE"
				}
			  }
			 },
			"byHours": {
			  "terms": {
				  "order" : { "_term" : "asc" },
				"script": "dateConversion",
				"params": {
				  "date_field": "created_at",
				  "format": "HH"
				},
                  "size": 24
			  }
			}
            
        }
    },
    "sentiments": {
      "terms": {
        "field": "senti.overallSentiment"
      }
    },
    "trends": {
      "date_histogram": {
        "field": "created_at",

        "interval": granularity
      },
      "aggs": {
        "tweetTypes": {
          "terms": {
            "field": "tweetType"
          }
        }
      }
    },
    "trendingTweets": {
      "top_hits": {
        "size": size,
        "sort": [
          {
            "retweet_count": {
              "order": "desc"
            }
          }
        ]
      }
    },
       "followers": {
        "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
        }
    }
      
  }
}
	return queryBody;
};


var liveTweetQueryFunction = function () {
	var liveTweetQuery = {
		"size": 10,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        }
      ]
			}
		},
		"sort": [
			{
				"created_at": {
					"order": "desc"
				}
    }
  ]
	};

	return liveTweetQuery;
};

var esPositiveClientTweetsQuery = function(liveParameters) {
	var size = liveParameters.size||10;
	var granularity = liveParameters.granularity||"hour";
	var queryBodyPositive = {
	"size" : 10,
		"aggs":{
			"positive" : {
			   "filter":{
			   		"term" : {
					  "senti.overallSentiment" : "positive"
					}
			   },
				"aggs":{
				  "tweets" :{
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			}
		}
	};
	return queryBodyPositive;
}


var esNegativeClientTweetsQuery = function(liveParameters){
	var size = liveParameters.size||10;
	var granularity = liveParameters.granularity||"hour";
	var queryBodyNegative = {
	"size" : 10,
		"aggs":{
			"negative" : {
			   "filter":{
			   		"term" : {
					  "senti.overallSentiment" : "negative"
					}
			   },
				"aggs":{
				  "tweets" :{
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			}
		}
	};
	return queryBodyNegative;

}
