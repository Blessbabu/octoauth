var generatemediaMainQuery = function (liveParameters) {
	var size = liveParameters.size || 10;
	var granularity = liveParameters.granularity || "hour";
	var queryBody = {
		"size": 0,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        },
					{
						"range": {
							"created_at": {
								"gt": "now-1w"
							}
						}
        }
      ]
			}
		},
		"aggs": {
			"retweetsCount": {
				"filter": {
					"term": {
						"tweetType": "tweet"
					}
				},
				"aggs": {
					"retweetSum": {
						"sum": {
							"field": "retweet_count"
						}
					},
					"favoriteSum": {
						"sum": {
							"field": "retweet_count"
						}
					},
					"followersSum": {
						"sum": {
							"field": "user.followers"
						}
					}

				}
			},
			"languages": {
				"terms": {
					"field": "language",
					"size": 100
				}
			},
			"countries": {
				"terms": {
					"field": "place.country_code",
					"size": 100
				}
			},
			"pieChartSenti": {
				"terms": {
					"field": "senti.overallSentiment"
				}
			},
			"filterRetweets": {
				"filter": {
					"terms": {
						"tweetType": ["retweet", "reply", "tweet"]
					}
				},
				"aggs": {
					"trendingUser": {
						"terms": {
							"field": "user.screen_name",
							"order": {
								"followers": "desc"
							},
							"size": 100
						},
						"aggs": {
							"followers": {
								"max": {
									"field": "user.followers"
								}
							},
							"details": {
								"top_hits": {
									"size": 1,
									"sort": {
										"created_at": "desc"
									}
								}
							}
						}
					}
				}
			},
			"trendingTweets": {
				"top_hits": {
					"size": size,
					"sort": [
						{
							"retweet_count": {
								"order": "desc"
							}
          }
        ]
				}
			},
			"trends": {
				"date_histogram": {
					"field": "created_at",
					"interval": granularity
				},
				"aggs": {
					"tweetTypes": {
						"terms": {
							"field": "tweetType"
						}



					}
				}
			},
			"trendingTags": {
				"terms": {
					"field": "hashtag.text",
					"size": size
				}
			},
			"negative": {
				"filter": {
					"term": {
						"senti.overallSentiment": "negative"
					}
				},
				"aggs": {
					"users": {
						"terms": {
							"field": "user.screen_name",
							"size": size,
							"order": {
								"sumOfSentiments": "asc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					},
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					},
					"trends": {
						"date_histogram": {
							"field": "created_at",
							"interval": granularity
						}
					},
					"tags": {
						"terms": {
							"field": "hashtag.text",
							"size": size,
							"order": {
								"sumOfSentiments": "asc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					}
				}
			},
			"positive": {
				"filter": {
					"term": {
						"senti.overallSentiment": "positive"
					}
				},
				"aggs": {
					"users": {
						"terms": {
							"field": "user.screen_name",
							"size": size,
							"order": {
								"sumOfSentiments": "desc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					},
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					},
					"trends": {
						"date_histogram": {
							"field": "created_at",
							"interval": granularity
						}
					},
					"tags": {
						"terms": {
							"field": "hashtag.text",
							"size": size,
							"order": {
								"sumOfSentiments": "desc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					}
				}
			}
		}
	};
	return queryBody;
};


var liveTweetQueryFunction = function () {
	var liveTweetQuery = {
		"size": 10,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        }
      ]
			}
		},
		"sort": [
			{
				"created_at": {
					"order": "desc"
				}
    }
  ]
	};

	return liveTweetQuery;
};