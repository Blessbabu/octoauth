var clickedFilters = [];

var csv = {
    trendsCSV: "",
    tagsCSV: "",
    influencersCSV: "",
    tweetsCSV: ""
};

var liveParameters = {
    granularity: null,
    size: null,
    foamtreeQuery: null
};

var colourMapmedia = {
    positive: "green",
    negative: "red",
    neutral: "blue",
    mixed: "blue"
};

var tweetParameters = {
    size: null
}

var data = {};
var dataNegative = {};
var dataPositive = {};
var eventIndexNegative = 0;
var eventIndexPositive = 0;
var dataArraysNegative = [];
var dataArraysPositive = [];
var sinceDate = null;
var fromDate = null;
var previousHits = [];
var streamRefreshInterval = 2;
var elementsShowinInStream = 10;
var searchField = null;
var chart = null;


$(document).ready(function () {
    $('.key').tooltip();

});

var followersQuery = {
    "followers": {
        "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
        }
    }
};

$(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
$(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');

var followersCount = function (response) {
    $(".dataContentFollowers").empty();
    $("#cFollowers").html(response.aggregations.followers.value);
    $("span.numbers").digits();
};

$('.tagsAddition').html(globalTagValue);

var esClient = null;
   // streamingClient = null;

var initOrReset = function () {
   // if (isNull(streamingClient)) {
       // streamingClient = new Elasticsearch(esHost, liveTweetQueryFunction, tweetParameters);
   // } else {
       // esClient.reset();
    ///}
    if (isNull(esClient)) {
        esClient = new Elasticsearch(esHost, generatemediaMainQuery, liveParameters);
    } 
//	else {
//       // streamingClient.reset();
//    }

    esClient.addIndexPattern("twitter-", twitterTimeFormat);

    //streamingClient.addIndexPattern("twitter-", twitterTimeFormat);
    //streamingClient.addDateRangeMatch("created_at", getFromString(), null);
    //streamingClient.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);

    applyScoring(esClient, "created_at", "twitter");

    esClient.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esClient.addDateRangeMatch("created_at", "now-1w", null);
    esClient.searchWithCustomAgg(followersQuery, followersCount);
};

initOrReset();

globalFunctionChange = function (globalTagValue) {
    initOrReset();
    esClient.removeTermMatch("ownedBy.id");
    //	esClient.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esClient.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esClient.searchWithCustomAgg(followersQuery, followersCount);
    //esFoamTree.addQueryString("text",globalTagValue.toLowerCase());
    //esFoamTree.removeTermMatch("ownedBy.id");
   // esFoamTree.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    previousHits = [];
    //streamingClient.removeTermMatch("ownedBy.id");
    //streamingClient.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    //createFoamTree(foamtreeId.listening, esFoamTree);
    emptyPieChartValuesOnChange();
    printValuesEmptyFunction();
    conductSearch();
}


var emptyPieChartValuesOnChange = function () {
    var count = 0;
    for (count; count < nameClassValue.length; count++) {
        $('.' + nameClassValue[count] + '').empty();
    }
}



function pageLoadmedia(data) {
    nv.addGraph(function () {
        if (chart == null) {
            chart = nv.models.multiBarChart()
                .color([
//                     '#cf6d51', '#61b082', '#618fb0'
                     '#6f87d8', '#6fb6d8', '#736fd8'
                    //'#618fb0', '#61b082'
                ])
                .transitionDuration(350)
                .stacked(true)
                .reduceXTicks(true) //If 'false', every single x-axis tick label will be rendered.
                .rotateLabels(0) //Angle to rotate x-axis labels.
                .showControls(false) //Allow user to switch between 'Grouped' and 'Stacked' mode.
                .groupSpacing(0.1) //Distance between each group of bars.
            ;

            chart.xAxis
                .tickFormat(function (d) {
                    //                    return d3.time.format('%Y-%m-%a')(new Date(d))
                    return d3.time.format('%d-%m-%Y')(new Date(d))
                });

            chart.yAxis
                .tickFormat(d3.format(',.0f'));
            //			.tickFormat(function(d) { 
            //				     if(d<1000){return d}
            //				     else{
            //				         var  y = d/Math.pow(10,3); 
            //						 return (Math.round(y))+"K"}
            //					});
        }

        //		d3.select('.nv-series')
        //			.hide();

        d3.select('#visits-chart svg')
            .datum(data)
            .call(chart);


        nv.utils.windowResize(chart.update);

        return chart;
    });

    //Generate some nice data.
    function exampleData() {
        return stream_layers(3, 10 + Math.random() * 100, .1).map(function (data, i) {
            return {
                key: 'Stream #' + i,
                values: data
            };

        });
    }

};







function liveTweetQueryFn() {
   // if (sinceDate != null) {
       // streamingClient.addDateRangeMatch("created_at", sinceDate, null);
   //}
    var streamLiveTweet = function () {
       // streamingClient.search(liveTweetTable);
        if (currentTab == "ajax/listening-twitter.html") {
            setTimeout(streamLiveTweet, 5000);
        }
    }
    streamLiveTweet();
};

function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

function queryFunctionNew() {
    conductSearch();
};


$('#submit-search').on('click', function () {
    $("#liveTweets").empty();
    searchField = $(".main-search").val();
    esClient.addQueryString("text", searchField);
   // streamingClient.addQueryString("text", searchField);
    liveTweetQueryFn();
   // liveParameters.foamtreeQuery = searchField;
    //esFoamTree.addQueryString("text", searchField);
    //createFoamTree(foamtreeId.listening, esFoamTree);
    emptyPieChartValuesOnChange();
    conductSearch();
});

function conductSearch(isStreaming) {
    if (isNull(isStreaming)) {
        handleLoading(true);
    }
    esClient.search(singleRenderFuntion);
}

$('.main-search').keypress(function (e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});


//$('body').on('click', '.timeFilter', function () {
//    $(".timeFilter").removeClass("active");
//    $(this).addClass("active");
//    duration = $(this).attr("duration");
//	var name =$(this).attr("name");
//	$('.time-select span').html('<i class="fa fa-clock-o"></i>'+name);
//
//});

onTimeChange = function (globalDuration) {
    esClient.addDateRangeMatch("created_at", "now-" + globalDuration, null);
    esClient.searchWithCustomAgg(followersQuery, followersCount);
   // esFoamTree.removeTermMatch("ownedBy.query");
   // esFoamTree.addTermMatch("ownedBy.query", globalTagValue.toLowerCase());
   // esFoamTree.addDateRangeMatch("created_at", "now-" + globalDuration, null);
  //  createFoamTree(foamtreeId.listening, esFoamTree);
    printValuesEmptyFunction();
    emptyPieChartValuesOnChange();
    conductSearch();
};

var printValuesEmptyFunction = function () {
    $("#cReach").empty();
    $("#cTweets").empty();
    $("#cRetweets").empty();
    $("#cFollowers").empty();
    $(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
    $(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
}

$('body').on('click', '#customButton', function () {
    var fromDate = $("#datepicker1").val();
    var toDate = $("#datepicker2").val();
    esClient.addDateRangeMatch("created_at", fromDate, toDate);
    //	esClient.getIndices(fromDate,toDate,"twitter-","2015-03-20");
    conductSearch();
});

$('body').on('click', '.granularityPicker', function () {
    var currentGranularity = $(this).attr("gValue");
    $('.granularityTitle span:first').html(currentGranularity);
    $(".granularityPicker").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var granularityValue = $(this).attr("granularityValue");
    liveParameters.granularity = granularityValue;
    generatemediaMainQuery(liveParameters);
    conductSearch();
});

$('body').on('click', '.sizePicker', function () {
    $(".sizePicker").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    liveParameters.size = sizeValue;
    generatemediaMainQuery(liveParameters);
    conductSearch();
});


$('body').on('click', '.downloadLink', function () {
    var downloadToken = $(this).attr('downloadToken');
    var blob = new Blob([csv[downloadToken]], {
        type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "downloadToken.csv");
});



$('body').on('click', '.graph-control-item', function () {
    //$('.graph-control-item').on('click', function () {
    var controlObject = $(this).find("i");
    var lightControlObject = $(this).find("a");
    if (controlObject.hasClass('fa-play')) {
        controlObject.removeClass('fa-play');
        controlObject.addClass('fa-pause');
        lightControlObject.removeAttr('id', "button")
        enableRealtimeStreaming();
    } else {
        controlObject.removeClass('fa-pause');
        controlObject.addClass('fa-play');
        lightControlObject.attr('id', "button")
        enableStreaming = false;
    }

});

var enableStreaming = true;

function enableRealtimeStreaming() {
    var streamFeeds = function () {
        if (!enableStreaming) {
            return;
        }
        conductSearch(false);
        setTimeout(streamFeeds, 5 * 1000);
    };
    streamFeeds();
}

var singleRenderFuntion = function (response) {
    // Empty everything
	
    $('#chartID').empty();
    $('#chartIDPie').empty();
    $('#vis').empty();
	
	if(response.aggregations.trends.buckets.length == 0){
		 $(".dataContentBubbleLoader").empty();
		 $(".dataContentBubbleLoader").html('No data available');
		}

    //	graph data csv
    csv.trendsCSV = "Tweets\n" + 'Tweet Time,Tweet Count\n' +
        convertBucketsToCSVGraphInner(response.aggregations.trends.buckets, "tweets") +
        "\nRetweets\n" + 'Retweet Time,Tweet Count\n' +
        convertBucketsToCSVGraphInner(response.aggregations.trends.buckets, "retweets") +
        "\Reply\n" + 'Reply Time,Reply Count\n' +
        convertBucketsToCSVGraphInner(response.aggregations.trends.buckets, "reply");

    //	tag data csv
    csv.tagsCSV = "Trending Tags\n" + "Trending Tags,Count\n" +
        convertBucketsToCSV(response.aggregations.trendingTags.buckets) +
        "\nTrending Positive Tags\n" + "Most Positive Tags,Sentiments Score\n" +
        convertBucketsToCSV(response.aggregations.positive.tags.buckets) +
        "\nTrending Negative Tags\n" + "Most Negative Tags,Sentiments Score\n" +
        convertBucketsToCSV(response.aggregations.negative.tags.buckets);

    //	influencers data csv
    csv.influencersCSV = "Trending Influencers\n" +
        convertBucketsToCSV(response.aggregations.filterRetweets.trendingUser.buckets);

    //	tweet data csv
    csv.tweetsCSV = "Trending Tweets\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
        convertBucketsToCSVRetweets(response.aggregations.trendingTweets.hits.hits) +
        "\nTrending Tweets Positive\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
        convertBucketsToCSVRetweets(response.aggregations.positive.tweets.hits.hits) +
        "\nTrending Tweets Negative\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
        convertBucketsToCSVRetweets(response.aggregations.negative.tweets.hits.hits);

    handleLoading(false);
    $("#trendingTags").empty();
    $("#mostPositive").empty();
    $("#mostNegative").empty();
    $("#trendingInfulencers").empty();
    $("#influencePositiveTable").empty();
    $("#influenceNegativeTable").empty();
     
    graphDataNegative(response);
//    tableTrendingMostNegative(response);
//    tableTrendingMostPositive(response);
//    tableTrendingTags(response);
//    imageArrayGen(null, response, tableTrendingInfluencers);
    // tableTrendingInfluencers(response);
//    tableRetweetPositive(response);
//    tableRetweetNegative(response);
//    tableRetweetTrending(response);
    printFunctionNew(response);
    graphDataSenti(response);
};



var graphDataNegative = function (response) {

    data = []
    var tweetTrend = [],
        retweetTrend = [],
        replyTrend = [];
    for (var bucketIndex in response.aggregations.trends.buckets) {
        var bucket = response.aggregations.trends.buckets[bucketIndex];
        var milliSecondValue = bucket.key;
        var tweetCount = 0,
            retweetCount = 0,
            replyTweet = 0;
        for (var typeBucketIndex in bucket.tweetTypes.buckets) {
            var typeBucket = bucket.tweetTypes.buckets[typeBucketIndex];
            if (typeBucket.key == "tweet") {
                tweetCount = typeBucket.doc_count;
            } else if (typeBucket.key == "retweet") {
                retweetCount = typeBucket.doc_count;
            } else if (typeBucket.key == "reply") {
                replyTweet = typeBucket.doc_count;
            }
        }
        tweetTrend.push({
            series: 0,
            x: (milliSecondValue),
            y: tweetCount
        });
        retweetTrend.push({
            series: 0,
            x: (milliSecondValue),
            y: retweetCount
        });
        replyTrend.push({
            series: 0,
            x: (milliSecondValue),
            y: replyTweet
        });
    }
    var dataBucket = {
        area: true,
        key: "Tweet",
        seriesIndex: 0,
        values: tweetTrend
    };
    data.push(dataBucket);
    dataBucket = {
        area: true,
        key: "Retweet",
        seriesIndex: 0,
        values: retweetTrend
    };
    data.push(dataBucket);
    dataBucket = {
        area: true,
        key: "Reply",
        seriesIndex: 0,
        values: replyTrend
    };
    data.push(dataBucket);
    pageLoadmedia(data);
    //	pageLoad(data);
};

var colorMapPiemedia = {
    positive: "#57c17b",
    negative: "#9e3533",
    neutral: "#96a6db",
    neutral: "#96a6db",
    mixed: "#daa05d"
};

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

var nameClassValue = ["ssm-pie-neutral-val", "ssm-pie-positive-val", "ssm-pie-negative-val", "ssm-pie-mixed-val"];
var nameClassCode = ["ssm-pie-neutral", "ssm-pie-positive", "ssm-pie-negative", "ssm-pie-mixed"];

var displayPieLegendsSenti = function (indexVal, sentiVal, docCount) {

    if (docCount != 0) {
        $('.' + nameClassCode[indexVal] + '').empty();
        $('.' + nameClassCode[indexVal] + '').html(sentiVal);
        $('.' + nameClassCode[indexVal] + '').css('color', colorMapPiemedia[sentiVal]);
        $('.' + nameClassValue[indexVal] + '').empty();
        $('.' + nameClassValue[indexVal] + '').html(commaSeparateNumber(docCount));
    } else {
        $('.' + nameClassCode[indexVal] + '').html(sentiVal);
        $('.' + nameClassCode[indexVal] + '').css('color', colorMapPiemedia[sentiVal]);
        $('.' + nameClassValue[indexVal] + '').html('0');
    }

}




var graphDataSenti = function (response) {
    var graphData = [];
    for (var bucketIndex in response.aggregations.pieChartSenti.buckets) {
        var bucket = response.aggregations.pieChartSenti.buckets[bucketIndex];

        if (bucket.key == "neutral") {
            displayPieLegendsSenti(0, 'neutral', bucket.doc_count);
            graphData.push({
                key: "neutral",
                color: colorMapPiemedia['neutral'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "positive") {
            displayPieLegendsSenti(1, 'positive', bucket.doc_count);
            graphData.push({
                key: "positive",
                color: colorMapPiemedia['positive'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "negative") {
            displayPieLegendsSenti(2, 'negative', bucket.doc_count);
            graphData.push({
                key: "negative",
                color: colorMapPiemedia['negative'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "mixed") {
            displayPieLegendsSenti(3, 'mixed', bucket.doc_count);
            graphData.push({
                key: "mixed",
                color: colorMapPiemedia['mixed'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        }


    }
    graphSentiChart(graphData);
    //	pageLoad(data);
};
//$('.match-all').eqHeights();

function graphSentiChart(graphData) {
    $(".dataContentAllPie").empty();
    nv.addGraph(function () {
        var chart = nv.models.pieChart()
            .x(function (d) {
                return d.label
            })
            .y(function (d) {
                return d.value
            })

        .color(function (d) {
                return d.data.color
            })
            .showLabels(true) //Display pie labels
            .labelThreshold(.05) //Configure the minimum slice size for labels to show up
            .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
            .donut(true) //Turn on Donut mode. Makes pie chart look tasty!
            .donutRatio(0.35) //Configure how big you want the donut hole size to be.
            .showLegend(false);

        chart.valueFormat(d3.format(',.d'));

        d3.select("#chartSenti svg")
            .datum(graphData)
            .transition().duration(350)
            .call(chart);

        d3.selectAll('.nv-slice')
            .on('click', function () {
                var fillValue = $(this).attr("fill");
                //		alert(fillValue);
                var name = null;
                if (fillValue == "#96a6db") {
                    name = "neutral";
                }
                if (fillValue == "#57c17b") {
                    name = "positive";
                }
                if (fillValue == "#9e3533") {
                    name = "negative";
                }
                if (fillValue == "#daa05d") {
                    name = "mixed";
                }
                tagAddFunction({
                    "field": "senti.overallSentiment",
                    "value": name
                });
                esClient.addTermMatch("senti.overallSentiment", name);
               // esFoamTree.addTermMatch("senti.overallSentiment", name);
               // createFoamTree(foamtreeId.listening, esFoamTree);
               // createFoamTree(foamtreeId.listening, esFoamTree);
                conductSearch();
            });
        //	 d3.select(".nv-legendWrap")
        //      .attr("transform","translate(100,350)") 

        return chart;
    });

};


var tagAddFunction = function (element) {
    $('#filter-result').append('<li class="blue tags selectedTag" field =' + element.field + ' fieldValue= ' + element.value + '><a href="#">' + element.value + '<span data-role="remove">x</span></a></li>').html();

};

$('body').on('click', '.selectedTag', function () {
    //alert($(this).find("i"));

    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClient.removeTermMatch(field, value);
  //  esFoamTree.removeTermMatch(field, value);
    //createFoamTree(foamtreeId.listening, esFoamTree);
    conductSearch();
    $(this).remove();
});

//$('body').on('click', function () {
//			$(".dataContent").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
//
//
//});

$('body').on('click', '.checkbox', function () {
    var testingMn = $(this).children(".mn").attr("id");
    var testingMp = $(this).children(".mp").attr("id");
    var testingTt = $(this).children(".tt").attr("id");

    var testingTit = $(this).children(".tit").attr("id");
    var testingIpt = $(this).children(".ipt").attr("id");
    var testingInt = $(this).children(".int").attr("id");
    var identifier = $(this).attr('fieldType');

    if (($("#" + testingMn).is(':checked')) ||
        ($("#" + testingMp).is(':checked')) ||
        ($("#" + testingTt).is(':checked')) ||
        ($("#" + testingTit).is(':checked')) ||
        ($("#" + testingIpt).is(':checked')) ||
        ($("#" + testingInt).is(':checked'))) {

        var name = $(this).attr('value');
        clickedFilters.push({
            "field": "hashtag.text",
            "value": name
        });
        tagAddFunction({
            "field": "hashtag.text",
            "value": name
        });
        esClient.addTermMatch("hashtag.text", name);
       // esFoamTree.addTermMatch("hashtag.text", name);
       // createFoamTree(foamtreeId.listening, esFoamTree);
        conductSearch();
    }



});

//dateConversion("now-1h");

function handleLoading(isLoading) {
    //        <div class="spinner"><img  id="img-spinner" src="img/spinner.gif" alt="Loading" /> </div>


    if (isLoading) {
        $(".dataContent").empty();
        //		        $(".dataContentFoamTree").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
        $(".dataContent").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
        $(".dataContentAllPie").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
		$(".dataContentBubbleLoader").html('<div>Loading Images</div>' + '<div><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
    } else {
        $(".dataContent").empty();
    }
}

var convertBucketsToCSV = function (buckets) {
    var csv = "";
    for (var bucketIndex in buckets) {
        var bucket = buckets[bucketIndex];
        csv += bucket.key + "," + bucket.doc_count + "\n";
    }
    return csv;
};

var convertBucketsToCSVRetweets = function (hits) {
    var csv = "";
    var totalHits = hits.length;
    var hitIndex = 0;
    for (hitIndex; hitIndex < totalHits; hitIndex++) {
        var hit = hits[hitIndex];
        if (isNull(hit._source) || isNull(hit._source.retweet_count)) {
            csv += "";
            continue;
        }
        csv += hit._source.user.name + ',' + hit._source.user.screen_name + ',' + hit._source.user.followers + ',' + hit._source.user.friends + ',' + hit._source.favorite_count + ',' + hit._source.retweet_count + ',' + utcToDateFormat(hit._source.created_at) + ',' + (hit._source.text.replace(/,/g, '')).replace(/(\r\n|\n|\r)/gm,"") + '\n';
    }

    return csv;
};

var convertBucketsToCSVGraph = function (histogram) {
    var csv = "";
    for (var histogramIndex in histogram) {
        histogramData = histogram[histogramIndex];
        csv += histogramData.key_as_string + ',' + histogramData.doc_count + '\n';
    }
    return csv;
};

var convertBucketsToCSVGraphInner = function (histogram, key) {
    var csv = "";
    var innerHistogram = null;
    for (var histogramIndex in histogram) {
        for (var innerBucket in histogram[histogramIndex].tweetTypes.buckets) {
            histogramData = histogram[histogramIndex].tweetTypes.buckets[innerBucket];
            if (key == "tweets") {
                if (histogramData.key == "tweet") {
                    innerHistotgram = histogramData.doc_count;
                    csv += histogram[histogramIndex].key_as_string + ',' + histogramData.key + ',' + histogramData.doc_count + '\n';
                }
            } else if (key == "retweets") {
                if (histogramData.key == "retweet") {
                    innerHistotgram = histogramData.doc_count;
                    csv += histogram[histogramIndex].key_as_string + ',' + histogramData.key + ',' + histogramData.doc_count + '\n';
                }
            } else if (key == "reply") {
                if (histogramData.key == "reply") {
                    innerHistotgram = histogramData.doc_count;
                    csv += histogram[histogramIndex].key_as_string + ',' + histogramData.key + ',' + histogramData.doc_count + '\n';
                }
            }

        }
    }
    return csv;
};

var tableTrendingMostNegative = function (response) {
    $(".trendingTags1").show();
    var tableIndex = 0;
    var totalAggs = response.aggregations.negative.tags.buckets;
    var totalAggsCount = totalAggs.length;
    var tableDataArraysNegative = [];
    var printArrayNegativeTags = [];
    for (tableIndex in totalAggs) {

        tableDataArraysNegative.push({
            mostNegative: totalAggs[tableIndex].key,
            negativeCount: totalAggs[tableIndex].sumOfSentiments.value
        });
    }
    $("#mostNegative").html($("#tableTrendsNegative").render(tableDataArraysNegative));
};

var tableTrendingMostPositive = function (response) {
    $(".trendingTags2").show();
    var className = ".mp";
    var tableIndex = 0;
    var totalAggs = response.aggregations.positive.tags.buckets;
    var totalAggsCount = totalAggs.length;
    var tableDataArraysPositive = [];
    for (tableIndex in totalAggs) {

        tableDataArraysPositive.push({
            mostPositive: totalAggs[tableIndex].key,
            positiveCount: totalAggs[tableIndex].sumOfSentiments.value
        });
    }
    $("#mostPositive").html($("#tableTrendsPositive").render(tableDataArraysPositive));


};

var tableTrendingTags = function (response) {
    $(".trendingTags3").show();
    var className = ".tt";
    var tableIndex = 0;
    var totalAggs = response.aggregations.trendingTags.buckets;
    var tdaTrendingTags = [];
    for (tableIndex in totalAggs) {

        tdaTrendingTags.push({
            trendingTags: totalAggs[tableIndex].key,
            count: totalAggs[tableIndex].doc_count
        });
    }

    $("#trendingTags").html($("#tableTrends").render(tdaTrendingTags));


};

var tableTrendingInfluencersPositive = function (response) {
    $(".influencers1").show();
    var className = ".ipt";
    var tableIndex = 0;
    var totalAggs = response.aggregations.positive.users.buckets;
    var totalAggsCount = totalAggs.length;
    var tdaInfluencersPositive = [];
    for (tableIndex in totalAggs) {

        tdaInfluencersPositive.push({
            mostPositive: totalAggs[tableIndex].key,
            positiveCount: totalAggs[tableIndex].sumOfSentiments.value
        });
    }

    $("#influencePositiveTable").html($("#influencePositive").render(tdaInfluencersPositive));

};



var tableTrendingInfluencersNegative = function (response) {
    $(".influencers2").show();
    var className = ".int";
    var tableIndex = 0;
    var totalAggs = response.aggregations.negative.users.buckets;
    var totalAggsCount = totalAggs.length;
    var tdaInfluencersNegative = [];
    for (tableIndex in totalAggs) {

        tdaInfluencersNegative.push({
            mostNegative: totalAggs[tableIndex].key,
            negativeCount: totalAggs[tableIndex].sumOfSentiments.value
        });
    }

    $("#influenceNegativeTable").html($("#influenceNegative").render(tdaInfluencersNegative));

};


var tableTrendingInfluencers = function (response) {
    $(".dataContentBubbleLoader").empty();
    console.log('bubble response is', response);
    var bubbleParams = {
        width: 980,
        height: 510,
        bubbleRadius: 45
    };
    var className = ".tit";
    var tableIndex = 0;
    var totalAggs = response.aggregations.filterRetweets.trendingUser.buckets;
    var tdaTrendingUser = [];
    var data = [];
    for (tableIndex in totalAggs) {
        if (tableIndex == 0)
            continue;
        data.push({
            id: "twitterPage",
            name: totalAggs[tableIndex].key,
            word: totalAggs[tableIndex].key,
            imageURL: totalAggs[tableIndex].details.hits.hits[0]._source.user.profile_image_url.replace(/normal/, "400x400"),
            //            imageURL : imageUrlStore,
            count: totalAggs[tableIndex].doc_count * 2,
            followers: totalAggs[tableIndex].details.hits.hits[0]._source.user.followers,
            favorites: totalAggs[tableIndex].details.hits.hits[0]._source.user.favorites,
            retweets: totalAggs[tableIndex].details.hits.hits[0]._source.retweet_count,
            favoritesCount: totalAggs[tableIndex].details.hits.hits[0]._source.favorite_count
//            sentiments: totalAggs[tableIndex].details.hits.hits[0]._source.senti.overallSentiment
        });
        tdaTrendingUser.push({
            trendingUser: totalAggs[tableIndex].key,
            count: totalAggs[tableIndex].doc_count
        });
    }
    plotBubble("#vis", data, bubbleParams);
    //$("#trendingInfulencers").html($("#trendingInfluencersTable").render(tdaTrendingUser));
};


var tableRetweetTrending = function (response) {
    $(".mostRetweets1").show();
    var tableIndex = 0;

    var totalAggs = response.aggregations.trendingTweets.hits.hits;

    var totalAggsCount = totalAggs.length;
    var tdaRetweetsTrends = [];
    for (tableIndex in totalAggs) {
        if (isNull(totalAggs[tableIndex]._source) || isNull(totalAggs[tableIndex]._source.retweet_count)) {
            continue;
        }
        var profile_image = totalAggs[tableIndex]._source.user.profile_image_url;
        // if (isNull(profile_image)){
        //     profile_image='https://abs.twimg.com/sticky/default_profile_images/default_profile_3_200x200.png';
        // }
        tdaRetweetsTrends.push({
            image: profile_image,
            personName: totalAggs[tableIndex]._source.user.name,
            personNameShort: ((totalAggs[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            id: totalAggs[tableIndex]._source.user.screen_name,
            followers: totalAggs[tableIndex]._source.user.followers,
            following: totalAggs[tableIndex]._source.user.friends,
            text: totalAggs[tableIndex]._source.text,
            date: utcToDateFormatReverse(totalAggs[tableIndex]._source.created_at),
            retweetCount: totalAggs[tableIndex]._source.retweet_count,
            favouriteCount: totalAggs[tableIndex]._source.favorite_count,
            tweetID: totalAggs[tableIndex]._id

        });
    }
    console.log('the new array data is', tdaRetweetsTrends);
    $("#retweetsTrending").html($("#tableRetweetsTrending").render(tdaRetweetsTrends));
//    slimScrollCall();
};


var tableRetweetPositive = function (response) {
    $(".mostRetweets2").show();
    var tableIndex = 0;
    var totalAggs = response.aggregations.positive.tweets.hits.hits;
    var totalAggsCount = totalAggs.length;
    var tdaRetweetsPositive = [];
    for (tableIndex in totalAggs) {
        if (isNull(totalAggs[tableIndex]._source) || isNull(totalAggs[tableIndex]._source.retweet_count)) {
            continue;
        }
        var profile_image = totalAggs[tableIndex]._source.user.profile_image_url;
        tdaRetweetsPositive.push({
            image: profile_image,
            personName: totalAggs[tableIndex]._source.user.name,
            personNameShort: ((totalAggs[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            id: totalAggs[tableIndex]._source.user.screen_name,
            followers: totalAggs[tableIndex]._source.user.followers,
            following: totalAggs[tableIndex]._source.user.friends,
            text: totalAggs[tableIndex]._source.text,
            date: utcToDateFormatReverse(totalAggs[tableIndex]._source.created_at),
            retweetCount: totalAggs[tableIndex]._source.retweet_count,
            favouriteCount: totalAggs[tableIndex]._source.favorite_count,
            tweetID: totalAggs[tableIndex]._id

        });
    }
    $("#retweetsPositive").html($("#tableRetweetsPositive").render(tdaRetweetsPositive));

};

var tableRetweetNegative = function (response) {
    $(".mostRetweets3").show();
    var tableIndex = 0;
    var totalAggs = response.aggregations.negative.tweets.hits.hits;
    var totalAggsCount = totalAggs.length;
    var tdaRetweetsNegative = [];
    for (tableIndex in totalAggs) {
        if (isNull(totalAggs[tableIndex]._source) || isNull(totalAggs[tableIndex]._source.retweet_count)) {
            continue;
        }
        var profile_image = totalAggs[tableIndex]._source.user.profile_image_url;
        tdaRetweetsNegative.push({
            image: profile_image,
            personName: totalAggs[tableIndex]._source.user.name,
            personNameShort: ((totalAggs[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            id: totalAggs[tableIndex]._source.user.screen_name,
            followers: totalAggs[tableIndex]._source.user.followers,
            following: totalAggs[tableIndex]._source.user.friends,
            text: totalAggs[tableIndex]._source.text,
            date: utcToDateFormatReverse(totalAggs[tableIndex]._source.created_at),
            retweetCount: totalAggs[tableIndex]._source.retweet_count,
            favouriteCount: totalAggs[tableIndex]._source.favorite_count,
            tweetID: totalAggs[tableIndex]._id

        });
    }
    $("#retweetsNegative").html($("#tableRetweetsNegative").render(tdaRetweetsNegative));

};

var liveTweetTable = function (response) {
	$("#liveTweets").empty();
	$(".liveTweetsTable").empty();
    $(".spinnerLive").hide();
    $(".liveTweetsSpinner").show();
    var tdaLiveTweets = [];
    var tableIndex = 0;
    var tdaLiveTweets = [];
    //    sinceDate = sinceDate + 1;
    var newFeeds = response.hits.hits;
    for (var element in previousHits) {
        newFeeds.push(previousHits[element]);
    }
    if (newFeeds.length > elementsShowinInStream) {
        newFeeds = newFeeds.slice(0, elementsShowinInStream);
    }
    previousHits = newFeeds;
    var totalHits = newFeeds.splice(5,5);
    console.log('live-tweets', totalHits);
    //	sinceDate=response.hits.hits[0].created_at;
    
    if (totalHits.length > 0) {
        var totalHitsCount = totalHits.length;
        for (tableIndex in totalHits) {
            fromDate = response.hits.hits[0]._source.created_at;
            tdaLiveTweets.push({
                image: totalHits[tableIndex]._source.user.profile_image_url,
                tweetID: totalHits[tableIndex]._id,
                date: utcToDateFormatReverse(totalHits[tableIndex]._source.created_at),
                id: totalHits[tableIndex]._source.user.name,
                followers: totalHits[tableIndex]._source.user.followers,
                following: totalHits[tableIndex]._source.user.friends,
                content: totalHits[tableIndex]._source.text,
                retweetCount: totalHits[tableIndex]._source.retweet_count,
                sentiments: totalHits[tableIndex]._source.senti.overallSentiment
            });
        }
        $("#liveTweets").html($("#liveTweetsTable").render(tdaLiveTweets));
        $("span.numbers").digits();
    }


};




var graphDataTrends = function (response) {

};

var genrateTrendData = function (histogram) {
    //		var series=0;
    var trend = [];
    for (eventIndexNegative in histogram) {
        var milliSecondValue = (histogram[this.eventIndexNegative].key);
        trend.push({
            series: 0,
            x: (milliSecondValue),
            y: (histogram[this.eventIndexNegative].doc_count)
        });
    }
    return trend;
}

var printFunctionNew = function (response) {
    var tweetTypes = response.aggregations.tweetTypes.buckets;
    var retweetCount = 0;
    console.log("BUCKET is ",tweetTypes);
    for(var idx in tweetTypes){
	if(tweetTypes[idx].key == "retweet"){
		retweetCount = tweetTypes[idx].doc_count;
	}
    }
    $(".dataContentOthers").empty();
    var totalReachValue = response.hits.total;
    $("#cReach").html(totalReachValue);
    $("#cTweets").html(response.aggregations.retweetsCount.doc_count);
    //    $("#cFollowers").html(response.aggregations.retweetsCount.followersSum.value);
    //$("#cRetweets").html(response.aggregations.retweetsCount.retweetSum.value);
    $("#cRetweets").html(retweetCount);
    $("#cFavourites").html(response.aggregations.retweetsCount.favoriteSum.value);
    $("span.numbers").digits();
};
var graphDataPositive = function (response) {
    var series = 0;
    var totalBuckets = response.aggregations.positive.trends.buckets;
    var totalBucketsCount = totalBuckets.length;
    for (eventIndexPositive in totalBuckets) {
        var milliSecondValue = (totalBuckets[this.eventIndexPositive].key);
        dataArraysPositive.push({
            series: series,
            x: (milliSecondValue),
            y: (totalBuckets[this.eventIndexPositive].doc_count)
        });
    }
    dataPositive = {
        area: true,
        key: "Positive Trend",
        seriesIndex: 0,
        values: dataArraysPositive
    };

    //data=[dataNegative,dataPositive];
    //	pageLoad(data);
};
