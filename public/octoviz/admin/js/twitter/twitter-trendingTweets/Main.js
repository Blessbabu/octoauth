var clickedFilters = [];

var csv = {
    trendsCSV: "",
    tagsCSV: "",
    influencersCSV: "",
    tweetsCSV: ""
};

var liveParameters = {
    granularity: null,
    size: null,
    foamtreeQuery: null
};

var colourMapmedia = {
    positive: "green",
    negative: "red",
    neutral: "blue",
    mixed: "blue"
};

var tweetParameters = {
    size: null
}

var data = {};
var dataNegative = {};
var dataPositive = {};
var eventIndexNegative = 0;
var eventIndexPositive = 0;
var dataArraysNegative = [];
var dataArraysPositive = [];
var sinceDate = null;
var fromDate = null;
var previousHits = [];
var streamRefreshInterval = 2;
var elementsShowinInStream = 10;
var searchField = null;
var chart = null;


$(document).ready(function () {
    $('.key').tooltip();

});

var followersQuery = {
    "followers": {
        "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
        }
    }
};

$(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
$(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');

var followersCount = function (response) {
    $(".dataContentFollowers").empty();
    $("#cFollowers").html(response.aggregations.followers.value);
    $("span.numbers").digits();
};

$('.tagsAddition').html(globalTagValue);

var esClientTrending = null;
    streamingClient = null;

var initOrReset = function (){
    if (isNull(streamingClient)) {
        streamingClient = new Elasticsearch(esHost, liveTweetQueryFunction, tweetParameters);
    } else {
        esClientTrending.reset();
    }
    if (isNull(esClientTrending)) {
        esClientTrending = new Elasticsearch(esHost, generatemediaTrendingQuery, liveParameters);
    } else {
        streamingClient.reset();
    }

    esClientTrending.addIndexPattern("twitter-", twitterTimeFormat);

    streamingClient.addIndexPattern("twitter-", twitterTimeFormat);
    streamingClient.addDateRangeMatch("created_at", getFromString(), null);
    streamingClient.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);

    applyScoring(esClientTrending, "created_at", "twitter");

    esClientTrending.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esClientTrending.addDateRangeMatch("created_at", getFromString(), null);
//    esClientTrending.searchWithCustomAgg(followersQuery, followersCount);
};



globalFunctionChange = function (globalTagValue) {
    initOrReset();
    esClientTrending.removeTermMatch("ownedBy.id");
    //	esClientTrending.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esClientTrending.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
//    esClientTrending.searchWithCustomAgg(followersQuery, followersCount);
    previousHits = [];
    streamingClient.removeTermMatch("ownedBy.id");
    streamingClient.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    //createFoamTree(foamtreeId.listening, esFoamTree);
//    emptyPieChartValuesOnChange();
    printValuesEmptyFunction();
    conductSearchTrending();
}


var emptyPieChartValuesOnChange = function () {
    var count = 0;
    for (count; count < nameClassValue.length; count++) {
        $('.' + nameClassValue[count] + '').empty();
    }
}

var currentTab = null;
var url = location.hash.replace(/^#/, "");
currentTab = url;
//alert(currentTab);

function liveTweetQueryFn() {
    if (sinceDate != null) {
        streamingClient.addDateRangeMatch("created_at", sinceDate, null);
   }
    var streamLiveTweet = function () {
        streamingClient.search(liveTweetTable);
//        if (currentTab == "ajax/listening-twitter.html") {
        if (currentTab == "includes/twitter-trending.html") {
            setTimeout(streamLiveTweet, 5000);
        }
    }
    streamLiveTweet();
};

function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

function initTabTrendingTweets() {
    initOrReset();
    conductSearchTrending();
};


$('#submit-search').on('click', function () {
    $("#liveTweets").empty();
    searchField = $(".main-search").val();
    esClientTrending.addQueryString("text", searchField);
    streamingClient.addQueryString("text", searchField);
    liveTweetQueryFn();
   // liveParameters.foamtreeQuery = searchField;
    //esFoamTree.addQueryString("text", searchField);
    //createFoamTree(foamtreeId.listening, esFoamTree);
    // emptyPieChartValuesOnChange();
    conductSearchTrending();
});

function conductSearchTrending(isStreaming) {
    if (isNull(isStreaming)) {
        handleLoading(true);
    }
    esClientTrending.search(singleRenderFunctionTrending);
}

$('.main-search').keypress(function (e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});


//$('body').on('click', '.timeFilter', function () {
//    $(".timeFilter").removeClass("active");
//    $(this).addClass("active");
//    duration = $(this).attr("duration");
//	var name =$(this).attr("name");
//	$('.time-select span').html('<i class="fa fa-clock-o"></i>'+name);
//
//});

onTimeChange = function (globalDuration) {
    esClientTrending.addDateRangeMatch("created_at", "now-" + globalDuration, null);
    printValuesEmptyFunction();
    // emptyPieChartValuesOnChange();
    conductSearchTrending();
};

var printValuesEmptyFunction = function () {
    $("#cReach").empty();
    $("#cTweets").empty();
    $("#cRetweets").empty();
    $("#cFollowers").empty();
    $(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
    $(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
}

$('body').on('click', '#customButton', function () {
    var fromDate = $("#datepicker1").val();
    var toDate = $("#datepicker2").val();
    esClientTrending.addDateRangeMatch("created_at", fromDate, toDate);
    //	esClientTrending.getIndices(fromDate,toDate,"twitter-","2015-03-20");
    conductSearchTrending();
});

$('body').on('click', '.granularityPicker', function () {
    var currentGranularity = $(this).attr("gValue");
    $('.granularityTitle span:first').html(currentGranularity);
    $(".granularityPicker").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var granularityValue = $(this).attr("granularityValue");
    liveParameters.granularity = granularityValue;
    generatemediaTrendingQuery(liveParameters);
    conductSearchTrending();
});

$('body').on('click', '.sizePicker', function () {
    $(".sizePicker").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    liveParameters.size = sizeValue;
    generatemediaTrendingQuery(liveParameters);
    conductSearchTrending();
});


$('body').on('click', '.downloadLink', function () {
	e.preventDefault();
    var downloadToken = $(this).attr('downloadToken');
    var blob = new Blob([csv[downloadToken]], {
        type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "downloadToken.csv");
});


$('body').on('click', '.graph-control-item', function () {
    //$('.graph-control-item').on('click', function () {
    var controlObject = $(this).find("i");
    var lightControlObject = $(this).find("a");
    if (controlObject.hasClass('fa-play')) {
        controlObject.removeClass('fa-play');
        controlObject.addClass('fa-pause');
        lightControlObject.removeAttr('id', "button")
        enableRealtimeStreaming();
    } else {
        controlObject.removeClass('fa-pause');
        controlObject.addClass('fa-play');
        lightControlObject.attr('id', "button")
        enableStreaming = false;
    }

});

var enableStreaming = true;

function enableRealtimeStreaming() {
    var streamFeeds = function () {
        if (!enableStreaming) {
            return;
        }
        conductSearchTrending(false);
        setTimeout(streamFeeds, 5 * 1000);
    };
    streamFeeds();
}

var singleRenderFunctionTrending = function (response) {
    // Empty everything
	
//    $('#chartID').empty();
//    $('#chartIDPie').empty();
//    $('#vis').empty();
	
//	if(response.aggregations.trends.buckets.length == 0){
//		 $(".dataContentBubbleLoader").empty();
//		 $(".dataContentBubbleLoader").html('No data available');
//		}

    csv.tweetsCSV = "Trending Tweets\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
        convertBucketsToCSVRetweets(response.aggregations.trendingTweets.hits.hits) +
        "\nTrending Tweets Positive\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
        convertBucketsToCSVRetweets(response.aggregations.positive.tweets.hits.hits) +
        "\nTrending Tweets Negative\n" + "User Name,Sceen Name,Followers,Following,Favourites,Retweets,Date,Tweet\n" +
        convertBucketsToCSVRetweets(response.aggregations.negative.tweets.hits.hits);



    handleLoading(false);
    $("#trendingTags").empty();
    $("#mostPositive").empty();
    $("#mostNegative").empty();
    $("#trendingInfulencers").empty();
    $("#influencePositiveTable").empty();
    $("#influenceNegativeTable").empty();
     
	console.log('trending',response);
    tableRetweetPositive(response);
    tableRetweetNegative(response);
    tableRetweetTrending(response);
//    slimScrollCall('550px');
};





function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}






var tagAddFunction = function (element) {
    $('#filter-result').append('<li class="blue tags selectedTag" field =' + element.field + ' fieldValue= ' + element.value + '><a href="#">' + element.value + '<span data-role="remove">x</span></a></li>').html();

};

$('body').on('click', '.selectedTag', function () {
    //alert($(this).find("i"));

    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClientTrending.removeTermMatch(field, value);
  //  esFoamTree.removeTermMatch(field, value);
    //createFoamTree(foamtreeId.listening, esFoamTree);
    conductSearchTrending();
    $(this).remove();
});

//$('body').on('click', function () {
//			$(".dataContent").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
//
//
//});

$('body').on('click', '.checkbox', function () {
    var testingMn = $(this).children(".mn").attr("id");
    var testingMp = $(this).children(".mp").attr("id");
    var testingTt = $(this).children(".tt").attr("id");

    var testingTit = $(this).children(".tit").attr("id");
    var testingIpt = $(this).children(".ipt").attr("id");
    var testingInt = $(this).children(".int").attr("id");
    var identifier = $(this).attr('fieldType');

    if (($("#" + testingMn).is(':checked')) ||
        ($("#" + testingMp).is(':checked')) ||
        ($("#" + testingTt).is(':checked')) ||
        ($("#" + testingTit).is(':checked')) ||
        ($("#" + testingIpt).is(':checked')) ||
        ($("#" + testingInt).is(':checked'))) {

        var name = $(this).attr('value');
        clickedFilters.push({
            "field": "hashtag.text",
            "value": name
        });
        tagAddFunction({
            "field": "hashtag.text",
            "value": name
        });
        esClientTrending.addTermMatch("hashtag.text", name);
       // esFoamTree.addTermMatch("hashtag.text", name);
       // createFoamTree(foamtreeId.listening, esFoamTree);
        conductSearchTrending();
    }



});

//dateConversion("now-1h");

function handleLoading(isLoading) {
    //        <div class="spinner"><img  id="img-spinner" src="img/spinner.gif" alt="Loading" /> </div>


    if (isLoading) {
        $(".dataContent").empty();
        //		        $(".dataContentFoamTree").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
        $(".dataContent").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
        $(".dataContentAllPie").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
		$(".dataContentBubbleLoader").html('<div>Loading Images</div>' + '<div><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');
    } else {
        $(".dataContent").empty();
    }
}

var convertBucketsToCSV = function (buckets) {
    var csv = "";
    for (var bucketIndex in buckets) {
        var bucket = buckets[bucketIndex];
        csv += bucket.key + "," + bucket.doc_count + "\n";
    }
    return csv;
};

var convertBucketsToCSVRetweets = function (hits) {
    var csv = "";
    var totalHits = hits.length;
    var hitIndex = 0;
    for (hitIndex; hitIndex < totalHits; hitIndex++) {
        var hit = hits[hitIndex];
        if (isNull(hit._source) || isNull(hit._source.retweet_count)) {
            csv += "";
            continue;
        }
        csv += hit._source.user.name + ',' + hit._source.user.screen_name + ',' + hit._source.user.followers + ',' + hit._source.user.friends + ',' + hit._source.favorite_count + ',' + hit._source.retweet_count + ',' + utcToDateFormat(hit._source.created_at) + ',' + (hit._source.text.replace(/,/g, '')).replace(/(\r\n|\n|\r)/gm,"") + '\n';
    }

    return csv;
};

var leftRightSwitching = function(id,totalAggsCount){
	for(var i=0;i<totalAggsCount;i++){
	  if(i%2==0){
	    $('div.left-right-switcher-'+id+''+i+'').addClass('pull-left');
	  }
		else{
		$('div.left-right-switcher-'+id+''+i+'').addClass('pull-right');
		$('div.left-right-switcher-second-'+id+''+i+'').addClass('on-left');
		}
	}
}

var tableRetweetPositive = function (response) {
    $(".mostRetweets2").show();
    var tableIndex = 0;
    var totalAggs = response.aggregations.positive.tweets.hits.hits;
    var totalAggsCount = totalAggs.length;
    var tdaRetweetsPositive = [];
    for (tableIndex in totalAggs) {
        if (isNull(totalAggs[tableIndex]._source) || isNull(totalAggs[tableIndex]._source.retweet_count)) {
            continue;
        }
        var profile_image = totalAggs[tableIndex]._source.user.profile_image_url;
        tdaRetweetsPositive.push({
            image: profile_image,
            personName: totalAggs[tableIndex]._source.user.name,
            personNameShort: ((totalAggs[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            id: totalAggs[tableIndex]._source.user.screen_name,
            followers: totalAggs[tableIndex]._source.user.followers,
            following: totalAggs[tableIndex]._source.user.friends,
            text: totalAggs[tableIndex]._source.text,
            date: utcToDateFormatReverse(totalAggs[tableIndex]._source.created_at),
            retweetCount: totalAggs[tableIndex]._source.retweet_count,
            favouriteCount: totalAggs[tableIndex]._source.favorite_count,
            tweetID: totalAggs[tableIndex]._id

        });
    }
    $("#retweetsPositive").html($("#tableRetweetsPositive").render(tdaRetweetsPositive));
	leftRightSwitching("trp",totalAggsCount);
	 
};

var tableRetweetNegative = function (response) {
    $(".mostRetweets3").show();
    var tableIndex = 0;
    var totalAggs = response.aggregations.negative.tweets.hits.hits;
    var totalAggsCount = totalAggs.length;
    var tdaRetweetsNegative = [];
    for (tableIndex in totalAggs) {
        if (isNull(totalAggs[tableIndex]._source) || isNull(totalAggs[tableIndex]._source.retweet_count)) {
            continue;
        }
        var profile_image = totalAggs[tableIndex]._source.user.profile_image_url;
        tdaRetweetsNegative.push({
            image: profile_image,
            personName: totalAggs[tableIndex]._source.user.name,
            personNameShort: ((totalAggs[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            id: totalAggs[tableIndex]._source.user.screen_name,
            followers: totalAggs[tableIndex]._source.user.followers,
            following: totalAggs[tableIndex]._source.user.friends,
            text: totalAggs[tableIndex]._source.text,
            date: utcToDateFormatReverse(totalAggs[tableIndex]._source.created_at),
            retweetCount: totalAggs[tableIndex]._source.retweet_count,
            favouriteCount: totalAggs[tableIndex]._source.favorite_count,
            tweetID: totalAggs[tableIndex]._id

        });
    }
    $("#retweetsNegative").html($("#tableRetweetsNegative").render(tdaRetweetsNegative));
	leftRightSwitching("trn",totalAggsCount);

};

var tableRetweetTrending = function (response) {
//    $(".mostRetweets1").show();
    var tableIndex = 0;

    var totalAggs = response.aggregations.trendingTweets.hits.hits;

    var totalAggsCount = totalAggs.length;
    var tdaRetweetsTrends = [];
    for (tableIndex in totalAggs) {
        if (isNull(totalAggs[tableIndex]._source) || isNull(totalAggs[tableIndex]._source.retweet_count)) {
            continue;
        }
        var profile_image = totalAggs[tableIndex]._source.user.profile_image_url;
        // if (isNull(profile_image)){
        //     profile_image='https://abs.twimg.com/sticky/default_profile_images/default_profile_3_200x200.png';
        // }
        tdaRetweetsTrends.push({
            image: profile_image,
            personName: totalAggs[tableIndex]._source.user.name,
            personNameShort: ((totalAggs[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
            id: totalAggs[tableIndex]._source.user.screen_name,
            followers: totalAggs[tableIndex]._source.user.followers,
            following: totalAggs[tableIndex]._source.user.friends,
            text: totalAggs[tableIndex]._source.text,
            date: utcToDateFormatReverse(totalAggs[tableIndex]._source.created_at),
            retweetCount: totalAggs[tableIndex]._source.retweet_count,
            favouriteCount: totalAggs[tableIndex]._source.favorite_count,
            tweetID: totalAggs[tableIndex]._id

        });
    }
    console.log('the new array data is', tdaRetweetsTrends);
    $("#retweetsTrending").html($("#tableRetweetsTrending").render(tdaRetweetsTrends));
	leftRightSwitching("trt",totalAggsCount);
//    slimScrollCall();
};




var liveTweetTable = function (response) {
	console.log('live tweet table response 1',response);
	$("#liveTweets").empty();
	$(".liveTweetsTable").empty();
    $(".spinnerLive").hide();
    $(".liveTweetsSpinner").show();
    var tdaLiveTweets = [];
    var tableIndex = 0;
//    previousHits = [];
    //    sinceDate = sinceDate + 1;
    var newFeeds = response.hits.hits;
//	console.log('new feeds before',newFeeds.length);
    for (var element in previousHits) {
        newFeeds.push(previousHits[element]);
    }
//	console.log('new feeds',newFeeds.length);
    if (newFeeds.length > elementsShowinInStream) {
        newFeeds = newFeeds.slice(0, elementsShowinInStream);
    }
//	console.log('elementsShownInStream',elementsShowinInStream);
    previousHits = newFeeds;
//	console.log('previous hits',previousHits);
    var totalHits = newFeeds.slice(0,10);
//    var totalHits = newFeeds;
//    console.log('live-tweets', totalHits);
    //	sinceDate=response.hits.hits[0].created_at;
    
    if (totalHits.length > 0) {
        var totalHitsCount = totalHits.length;
        for (tableIndex in totalHits) {
            fromDate = response.hits.hits[0]._source.created_at;
            tdaLiveTweets.push({
                image: totalHits[tableIndex]._source.user.profile_image_url,
                tweetID: totalHits[tableIndex]._id,
                date: utcToDateFormatReverse(totalHits[tableIndex]._source.created_at),
                id: totalHits[tableIndex]._source.user.name,
                personNameShort: ((totalHits[tableIndex]._source.user.name).slice(0, 5) + '..').replace(/\s+/g, ''),
                followers: totalHits[tableIndex]._source.user.followers,
                following: totalHits[tableIndex]._source.user.friends,
                content: totalHits[tableIndex]._source.text,
                retweetCount: totalHits[tableIndex]._source.retweet_count,
                sentiments: totalHits[tableIndex]._source.senti.overallSentiment
            });
        }
		console.log('tda live tweets',tdaLiveTweets);
        $("#liveTweets").html($("#liveTweetsTable").render(tdaLiveTweets));
		leftRightSwitching("lt",totalHitsCount);
        $("span.numbers").digits();
    }


};

