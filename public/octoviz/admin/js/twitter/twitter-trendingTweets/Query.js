var generatemediaTrendingQuery = function (liveParameters) {
	var size = liveParameters.size || 10;
	var granularity = liveParameters.granularity || "hour";
	var queryBody = {
		"size": 0,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        },
					{
						"range": {
							"created_at": {
								"gt": "now-1w"
							}
						}
        }
      ]
			}
		},
		"aggs": {
			
			"trendingTags": {
				"terms": {
					"field": "hashtag.text",
					"size": size
				}
			},
				"trendingTweets": {
				"top_hits": {
					"size": size,
					"sort": [
						{
							"retweet_count": {
								"order": "desc"
							}
          }
        ]
				}
			},
			"negative": {
				"filter": {
					"term": {
						"senti.overallSentiment": "negative"
					}
				},
				"aggs": {
					"users": {
						"terms": {
							"field": "user.screen_name",
							"size": size,
							"order": {
								"sumOfSentiments": "asc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					},
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					},
					"trends": {
						"date_histogram": {
							"field": "created_at",
							"interval": granularity
						}
					},
					"tags": {
						"terms": {
							"field": "hashtag.text",
							"size": size,
							"order": {
								"sumOfSentiments": "asc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					}
				}
			},
			"positive": {
				"filter": {
					"term": {
						"senti.overallSentiment": "positive"
					}
				},
				"aggs": {
					"users": {
						"terms": {
							"field": "user.screen_name",
							"size": size,
							"order": {
								"sumOfSentiments": "desc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					},
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					},
					"trends": {
						"date_histogram": {
							"field": "created_at",
							"interval": granularity
						}
					},
					"tags": {
						"terms": {
							"field": "hashtag.text",
							"size": size,
							"order": {
								"sumOfSentiments": "desc"
							}
						},
						"aggs": {
							"sumOfSentiments": {
								"sum": {
									"field": "senti.overallScore"
								}
							}
						}
					}
				}
			}
		}
	};
	return queryBody;
};

