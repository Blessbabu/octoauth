var Bubbles, root, texts;
var bubleSizeField = "followers";

root = typeof exports !== "undefined" && exports !== null ? exports : this;

Bubbles = function (bubbleParams) {
	var chart, clear, click, collide, collisionPadding, connectEvents, data, force, gravity, hashchange, height, idValue, jitter, label, margin, maxRadius, minCollisionRadius, mouseout, mouseover, node, rScale, rValue, textValue, tick, transformData, update, updateActive, updateLabels, updateNodes, width;
	width = bubbleParams.width;
	height = bubbleParams.height;
	var ratio  =  Math.max(1,Math.round(width/height));
	console.log("Ratio is ",ratio);
	data = [];
  var rectangeDefenition  = [ [200,0] , [700,700] ];

	node = null;
	label = null;
	margin = {
		top: 5,
		right: 0,
		bottom: 0,
		left: 0
	};
	maxRadius = bubbleParams.bubbleRadius;
	rScale = d3.scale.sqrt().range([10, maxRadius]);
	rValue = function (d) {
		return parseInt(d[bubleSizeField]);
	};
	idValue = function (d) {
		return d.name;
	};
	textValue = function (d) {
		return d.name;
	};
	collisionPadding = 4;
	minCollisionRadius = 12;
	jitter = .5;
	transformData = function (rawData) {
		rawData.forEach(function (d) {
			d.count = parseInt(d[bubleSizeField]);
			return rawData.sort(function () {
				return 0.5 - Math.random();
			});
		});
		return rawData;
	};
	tick = function (e) {
		var dampenedAlpha;
		dampenedAlpha = e.alpha * 0.1;
		node.each(gravity(dampenedAlpha)).each(collide(jitter)).attr("transform", function (d) {
			//console.log(d);
			return "translate(" + d.x + "," + d.y + ")";
		});
		return label.style("left", function (d) {
			return ((margin.left + d.x) - d.dx / 2) + "px";
		}).style("top", function (d) {
			return ((margin.top + d.y) - d.dy / 2) + "px";
		});
	};
	force = d3.layout.force().gravity(0).charge(0).size([width, height]).on("tick", tick);
	chart = function (selection) {
		return selection.each(function (rawData) {
			var maxDomainValue, svg, svgEnter;
			data = transformData(rawData);
			maxDomainValue = d3.max(data, function (d) {
				return rValue(d);
			});
			rScale.domain([0, maxDomainValue]);
			svg = d3.select(this).selectAll("svg").data([data]);
			svgEnter = svg.enter().append("svg");
			svg.attr("width", width + margin.left + margin.right);
			svg.attr("height", height + margin.top + margin.bottom);
			node = svgEnter.append("g").attr("id", "bubble-nodes").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
			node.append("rect").attr("id", "bubble-background").attr("width", width).attr("height", height).on("click", clear);
			label = d3.select(this).selectAll("#bubble-labels").data([data]).enter().append("div").attr("id", "bubble-labels");
			update();
			hashchange();
			return d3.select(window).on("hashchange", hashchange);
		});
	};
	update = function () {
		data.forEach(function (d, i) {
			return d.forceR = Math.max(minCollisionRadius, rScale(rValue(d)));
		});
		force.nodes(data).start();
		updateNodes();
		return updateLabels();
	};
	updateNodes = function () {
		node = node.selectAll(".bubble-node").data(data, function (d) {
			return idValue(d);
		});
		node.exit().remove();
		return node.enter()
			.append("a")
			.attr("class", "bubble-node")
			.call(force.drag)
			.call(connectEvents)
			.append("circle")
			.attr("r", function (d) {
				return 0;
			});

	};
	updateLabels = function () {
		var labelEnter;
		label = label.selectAll(".bubble-label").data(data, function (d) {
			return idValue(d);
		});
		label.exit().remove();
		labelEnter = label.enter().append("a").attr("class", "bubble-label").attr("href", function (d) {
			return "#" + (encodeURIComponent(idValue(d)));
		}).call(force.drag).call(connectEvents);

		labelEnter.append("img").attr("src", function (d) {
			return d.imageURL;
		}).attr("class", "circular").attr("width", function (d) {
			return (2 * rScale(rValue(d))) + "px";
		}).attr("onError","this.onerror=null;this.src='https://abs.twimg.com/sticky/default_profile_images/default_profile_3_200x200.png';") ;
		label.style("font-size", function (d) {
			return Math.max(8, rScale(rValue(d) / 2)) + "px";
		}).style("width", function (d) {
			return rScale(rValue(d)) + "px";
		});
		label.append("span").text(function (d) {
			return textValue(d);
		}).each(function (d) {
			return d.dx = Math.max(2.5 * rScale(rValue(d)), this.getBoundingClientRect().width);
		}).remove();
		label.style("width", function (d) {
			return d.dx + "px";
		});
		return label.each(function (d) {
			return d.dy = this.getBoundingClientRect().height;
		});
	};
	gravity = function (alpha) {
		var ax, ay, cx, cy;
		cx = width / 2;
		cy = height / 2;
		ax = alpha / ratio;
		ay = alpha;
		return function (d) {
			d.x += (cx - d.x) * ax;
			return d.y += (cy - d.y) * ay;
		};
	};
  var isInsideRectangle = function(d){
      if(d.x < rectangeDefenition[0][0]){
          d.x = d.x +1;
      }
      else if(d.x > rectangeDefenition[1][0]){
          d.x = d.x -1;
      }
      
      if(d.y < rectangeDefenition[0][1]){
          d.y = d.y +1;
      }
      else if(d.y > rectangeDefenition[1][1]){
         d.y = d.y -1;
      }
  }

	collide = function (jitter) {
		return function (d) {
			return data.forEach(function (d2) {
				var distance, minDistance, moveX, moveY, x, y;
				if (d !== d2) {
					x = d.x - d2.x;
					y = d.y - d2.y;
					distance = Math.sqrt(x * x + y * y);
					minDistance = d.forceR + d2.forceR + collisionPadding;
			                isInsideRectangle(d);
          				isInsideRectangle(d2);
					if (distance < minDistance) {
						distance = (distance - minDistance) / distance * jitter;
						moveX = x * distance;
						moveY = y * distance;
						d.x -= moveX;
						d.y -= moveY;
						d2.x += moveX;
						return d2.y += moveY;
					}
				}
			});
		};
	};
	connectEvents = function (d) {
		d.on("click", click);
		d.on("mouseover", mouseover);
		return d.on("mouseout", mouseout);
	};
	clear = function () {
		//return location.replace("#");
	};
	
	var homeModalFunction = function(d){
		esClientHome.addTermMatch("user.screen_name",d.name);
		d.name=null;
		esClientHome.search(bubbleDataRender);
		esClientHome.removeTermMatch("user.screen_name", d.name);
		
		
//	 $("#docStatsBubble").html($("#docStatsTableBubble").render(d));
//		$('#bubbleModal').modal('show');
	};
	var bubbleDataRender = function(response){
		var hit = response.hits.hits;
		var bubbleDataHolder = [];
		$('#modalTitle').html(response.hits.hits[0]._source.user.name);
		for(var hitIndex in hit){
			bubbleDataHolder.push({
				id:hit[hitIndex]._source._id,
				name:hit[hitIndex]._source.user.name,
				totalTweets:hit.total,
				tweet:hit[hitIndex]._source.text,
				followers:hit[hitIndex]._source.user.followers,
				retweets:hit[hitIndex]._source.retweet_count,
				favourites:hit[hitIndex]._source.favorite_count
			})
		
		}
		
		$("#docStatsBubble").html($("#docStatsTableBubble").render(bubbleDataHolder));
		$('#bubbleModal').modal('show');
//        slimScrollCall();
		};
	
	var twitterModalFunction = function(d){
		 $("#docStatsBubblemedia").html($("#docStatsTableBubblemedia").render(d));
		$('#bubbleModalmedia').modal('show');
	};
	
	click = function (d) {
		if(d.id == "twitterPage"){
		    twitterCallFunction(d);
		}
		if(d.id == "homePage"){
			homeCallFunction(d);			
		}
		else{
			shareOfVoiceFunction(d);
		}
		
//        var win=window.open("http://twitter.com/" + encodeURIComponent(idValue(d)), '_blank');
//        win.focus();
		//location.replace("#" + encodeURIComponent(idValue(d)));
		return d3.event.preventDefault();
	};
	hashchange = function () {
		var id;
		id = decodeURIComponent(location.hash.substring(1)).trim();
		return updateActive(id);
	};
	updateActive = function (id) {
		node.classed("bubble-selected", function (d) {
			return id === idValue(d);
		});
		console.log("Selected  Me " , id);
	};
	mouseover = function (d) {
		return node.classed("bubble-hover", function (p) {
			return p === d;
		});
	};
	mouseout = function (d) {
		return node.classed("bubble-hover", false);
	};
	chart.jitter = function (_) {
		if (!arguments.length) {
			return jitter;
		}
		jitter = _;
		force.start();
		return chart;
	};
	chart.height = function (_) {
		if (!arguments.length) {
			return height;
		}
		height = _;
		return chart;
	};
	chart.width = function (_) {
		if (!arguments.length) {
			return width;
		}
		width = _;
		return chart;
	};
	chart.r = function (_) {
		if (!arguments.length) {
			return rValue;
		}
		rValue = _;
		return chart;
	};
	return chart;
};

root.plotData = function (selector, data, plot) {
	return d3.select(selector).datum(data).call(plot);
};


function plotBubble(id, data,bubbleParams) {
	var display, key, plot, text;
	plot = Bubbles(bubbleParams);
	display = function (id, data) {
		return plotData(id, data, plot);
	};
	display(id, data);
}
