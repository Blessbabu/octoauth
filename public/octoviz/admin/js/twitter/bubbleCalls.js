var liveParametersBubble = {
};

var shareOfVoiceFunction = function (d) {
	var esClientBubble = new Elasticsearch(esHost, generatemediaMainQueryBubble, liveParametersBubble);
	esClientBubble.addIndexPattern("twitter-", twitterTimeFormat);
	esClientBubble.addDateRangeMatch("created_at", "now-" + globalDuration, null);
	esClientBubble.addTermMatch("ownedBy.id", user + "%%%" + d.id);
	esClientBubble.addTermMatch("user.screen_name", d.name);
	esClientBubble.search(bubbleDataRender);
	esClientBubble.removeTermMatch("user.screen_name", d.name);
};

var homeCallFunction = function (d) {
	var esClientBubble = new Elasticsearch(esHost, generatemediaMainQueryBubble, liveParametersBubble);
	esClientBubble.addIndexPattern("twitter-", twitterTimeFormat);
	esClientBubble.addDateRangeMatch("created_at", "now-" + globalDuration, null);
	esClientBubble.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
	esClientBubble.addTermMatch("user.screen_name", d.name);
	esClientBubble.search(bubbleDataRender);
	esClientBubble.removeTermMatch("user.screen_name", d.name);
};

var twitterCallFunction = function (d) {
	var esClientBubble = new Elasticsearch(esHost, generatemediaMainQueryBubble, liveParametersBubble);
	esClientBubble.addIndexPattern("twitter-", twitterTimeFormat);
	esClientBubble.addDateRangeMatch("created_at", "now-" + globalDuration, null);
	esClientBubble.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
	esClientBubble.removeTermMatch("user.screen_name", d.name);
	esClientBubble.addTermMatch("user.screen_name", d.name);
	esClientBubble.search(bubbleDataRendermedia);

};

var fullContactCall = function (fcType, name, identifier) {
	var url = fcType + name;
//	console.log('url to full contact is', url);
	var fullContact = new FullContact(url);
	if (identifier == "twitter") {
		fullContact.getProfile(url, 'id', fullContactRender);
	} else {
		fullContact.getProfile(url, 'id', fullContactRenderOther);
	}
};

var fullContactRender = function (response) {
	console.log('full contact response', response);
	bubbleDataHoldermedia.profile = [];
	if (response._source) {
		bubbleDataHoldermedia.profile = response._source;
	} else {
		bubbleDataHoldermedia.profile = response;
	}
	console.log('total response response', bubbleDataHoldermedia);
	$(".influencers-block").html($.templates("#single-render-bubble-home").render(innermediaDataPush(bubbleDataHoldermedia)));
	tabSwitchFunctionModal();
//	slimScrollCall();
	$("#myModalProfileHome").modal('show');
};

var fullContactRenderOther = function (response) {
	console.log('full contact response other', response);
	bubbleDataHolder.profile = [];
	if (response._source) {
		bubbleDataHolder.profile = response._source;
	} else {
		bubbleDataHolder.profile = response;
	}

	console.log('total response response', bubbleDataHolder);
	$(".influencers-block").html($.templates("#single-render-bubble-home").render(innermediaDataPush(bubbleDataHolder)));
	tabSwitchFunctionModal();
//	slimScrollCall();
	$("#myModalProfileHome").modal('show');
};

var innermediaDataPush = function (bubbleDataHolderCom) {
	console.log('inner data', bubbleDataHolderCom);
	var data = [];
	var indexRef = null;
	var bucket = bubbleDataHolderCom.profile.socialProfiles;
	var index = 0;
	for (index; index < bucket.length; index++) {
		if (bucket[index].type == "twitter") {
			data.push(bubbleDataHolderCom.twitter);
			indexRef = index;
		}

	}
	console.log('data zero in inner twitterDataPush', data[0]);
	bubbleDataHolderCom.profile.socialProfiles[indexRef].stats = data[0];
	console.log('inner bubble twitter data is', bubbleDataHolderCom);
	kloutDataAdditionFunction(bubbleDataHolderCom);

	return bubbleDataHolderCom;
};

var kloutDetails = {};

var kloutDataAdditionFunction = function (bubbleDataHolderCom) {
	kloutDetails = {};
	var buckets = bubbleDataHolderCom.profile;
	var index = 0;
	var dataTags = [];

	if (!isNull(buckets.digitalFootprint)) {
		if ((buckets.digitalFootprint.scores.length) != 0) {
			var scoreBuckets = buckets.digitalFootprint.scores;
			for (var indexScores = 0; indexScores < scoreBuckets.length; indexScores++) {
				if (scoreBuckets[indexScores].provider == "klout") {
					kloutDetails.score = scoreBuckets[indexScores].value;
				}
			}
		}
		if ((buckets.digitalFootprint.topics.length) != 0) {
			var topicsBuckets = buckets.digitalFootprint.topics;
			for (var indexTopics = 0; indexTopics < topicsBuckets.length; indexTopics++) {
				if (topicsBuckets[indexTopics].provider == "klout") {
					dataTags.push(topicsBuckets[indexTopics].value);
					var dataTagsString = dataTags.toString();
					kloutDetails['tags'] = dataTagsString;
				}
			}
		}
	}

	if ((buckets.socialProfiles.length) != 0) {
		var kloutBuckets = buckets.socialProfiles
		for (var indexKlout = 0; indexKlout < kloutBuckets.length; indexKlout++) {
			if (kloutBuckets[indexKlout]['type'] == 'klout') {
				for (var kloutDetailsIndex in kloutDetails) {
					kloutBuckets[indexKlout]['score'] = kloutDetails.score;
					kloutBuckets[indexKlout]['tags'] = kloutDetails.tags;
				}
			}

		}
	}
	console.log('with klout data', bubbleDataHolderCom);
	console.log('kloutDetails', kloutDetails);
};

var generatemediaMainQueryBubble = function (liveParameters) {
	var queryBody = {
		"size": 10,
		"sort": {
			"retweet_count": {
				"order": "desc"
			}
		}
	};
	return queryBody;
};

var bubbleDataHolder = {};

var bubbleDataRender = function (response) {
	console.log('home foam response', response);
	var hit = response.hits.hits;
	console.log('hits in bubble', hit);
	bubbleDataHolder.twitter = [];
	for (var hitIndex in hit) {
		bubbleDataHolder.twitter.push({
			id: hit[hitIndex]._id,
			fullName: hit[hitIndex]._source.user.name,
			name: hit[hitIndex]._source.user.screen_name,
			created_at: utcToDateFormat(hit[hitIndex]._source.created_at),
			action: hit[hitIndex]._source.tweetType,
			totalTweets: hit.total,
			tweet: hit[hitIndex]._source.text,
			followers: hit[hitIndex]._source.user.followers,
			retweets: hit[hitIndex]._source.retweet_count,
			friends: hit[hitIndex]._source.user.friends,
			favourites: hit[hitIndex]._source.favorite_count
		})

	}
	console.log('bubbledataholder home', bubbleDataHolder);
	console.log('his highness', hit[0]._source.user.screen_name);
	fullContactCall(fcmediaUrl, hit[0]._source.user.screen_name, "other");
//	slimScrollCall();

};

var bubbleDataHoldermedia = {};

var bubbleDataRendermedia = function (response) {
	bubbleDataHoldermedia.twitter = [];
	console.log('twitter bubble response', response);
	var hit = response.hits.hits;
	for (var hitIndex in hit) {
		bubbleDataHoldermedia.twitter.push({
			id: hit[hitIndex]._id,
			fullName: hit[hitIndex]._source.user.name,
			name: hit[hitIndex]._source.user.screen_name,
			created_at: utcToDateFormat(hit[hitIndex]._source.created_at),
			action: hit[hitIndex]._source.tweetType,
			totalTweets: hit.total,
			tweet: hit[hitIndex]._source.text,
			friends: hit[hitIndex]._source.user.friends,
			followers: hit[hitIndex]._source.user.followers,
			retweets: hit[hitIndex]._source.retweet_count,
			favourites: hit[hitIndex]._source.favorite_count
		})

	}
//	slimScrollCall();
	console.log('bubbledataholder twitter', bubbleDataHoldermedia);
	fullContactCall(fcmediaUrl, hit[0]._source.user.screen_name, "twitter");
};
