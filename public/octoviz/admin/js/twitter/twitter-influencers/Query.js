var generatemediaMainQueryInfluencers = function (liveParameters) {
    var size = liveParameters.size || 18;
    if(liveParameters.sortIdentifier=="_count"||null){
        var order1 = {
        "retweetCount":"desc"
    }
    }
    else if(liveParameters.sortIdentifier=="max_followers.value"){
        var order1 = {
        "max_followers.value":"desc"
    } 
    }   
    var granularity = liveParameters.granularity || "hour";
    var queryBody = {
          "size": 0,
          "aggs": {
            "influencers": {
              "terms": {
                "field": "user.screen_name",
                "size": size,
                  "order":order1
              },
              "aggs": {
                "retweetCount" : {
                    "sum" : {
                        "field" : "retweet_count"
                    }
                },
                "personStats": {
                  "terms": {
                    "field": "tweetType"
                  }
                },
                "personInfo": {
                  "top_hits": {
                    "size": 1,
                    "sort": {
                      "created_at": {
                        "order": "desc"
                      }
                    }
                  }
                },
                "max_followers": {
                  "max": {
                    "field": "user.followers"
                  }
                }
              }
            }
          }
};
    return queryBody;
};




var generateTwitterBubbleQuery = function (liveParameters) {
    var queryBody = {
        "size": 10,
        "sort": {
            "retweet_count": {
                "order": "desc"
            }
        }
    };
    return queryBody;
};
