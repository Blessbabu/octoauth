var clickedFilters = [];

var csv = {
    trendsCSV: "",
    tagsCSV: "",
    influencersCSV: "",
    tweetsCSV: ""
};

var liveParametersInf = {
    granularity: null,
    size: null,
    foamtreeQuery: null
};

var colourMapmedia = {
    positive: "green",
    negative: "red",
    neutral: "blue",
    mixed: "blue"
};

var tweetParameters = {
    size: null
}

var data = {};
var dataNegative = {};
var dataPositive = {};
var eventIndexNegative = 0;
var eventIndexPositive = 0;
var dataArraysNegative = [];
var dataArraysPositive = [];
var sinceDate = null;
var fromDate = null;
var previousHits = [];
var streamRefreshInterval = 2;
var elementsShowinInStream = 10;
var searchField = null;
var chart = null;


$(document).ready(function () {
    $('.key').tooltip();

});

var followersQuery = {
    "followers": {
        "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
        }
    }
};

$(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
$(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');

var followersCount = function (response) {
    $(".dataContentFollowers").empty();
    $("#cFollowers").html(response.aggregations.followers.value);
    $("span.numbers").digits();
};

$('.tagsAddition').html(globalTagValue);

var esClientInfluencers = null;

var intiOrResetInf = function () {

    if (isNull(esClientInfluencers)) {
        esClientInfluencers = new Elasticsearch(esHost, generatemediaMainQueryInfluencers, liveParametersInf);
    } 


    esClientInfluencers.addIndexPattern("twitter-", twitterTimeFormat);


    applyScoring(esClientInfluencers, "created_at", "twitter");

    esClientInfluencers.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esClientInfluencers.addDateRangeMatch("created_at", "now-1w", null);
    esClientInfluencers.searchWithCustomAgg(followersQuery, followersCount);
};



globalFunctionChangeInfluencers = function (globalTagValue) {
    intiOrResetInf();
    esClientInfluencers.removeTermMatch("ownedBy.id");
    //	esClientInfluencers.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esClientInfluencers.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esClientInfluencers.searchWithCustomAgg(followersQuery, followersCount);
    previousHits = [];
    emptyPieChartValuesOnChange();
    printValuesEmptyFunction();
    conductSearchInfluencers();
}


var emptyPieChartValuesOnChange = function () {
    var count = 0;
    for (count; count < nameClassValue.length; count++) {
        $('.' + nameClassValue[count] + '').empty();
    }
}

onSearchInfluencers = function(){
	esClientInfluencers.addQueryString("text", searchField);
	conductSearchInfluencers();
}


function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

function initTabInfluencers() {
    intiOrResetInf();
    conductSearchInfluencers();
};


$('#submit-search').on('click', function () {
    $("#liveTweets").empty();
    searchField = $(".main-search").val();
    esClientInfluencers.addQueryString("text", searchField);
    emptyPieChartValuesOnChange();
    conductSearchInfluencers();
});

function conductSearchInfluencers(isStreaming) {
    if (isNull(isStreaming)) {
        handleLoadingInfluencers(true);
    }
    esClientInfluencers.search(singleRenderFunctionInfluencers);
}

$('.main-search').keypress(function (e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});




onTimeChangeInfluencers = function (globalDuration) {
    esClientInfluencers.addDateRangeMatch("created_at", "now-" + globalDuration, null);
//    esClientInfluencers.searchWithCustomAgg(followersQuery, followersCount);
    printValuesEmptyFunction();
    emptyPieChartValuesOnChange();
    conductSearchInfluencers();
};

var printValuesEmptyFunction = function () {
    $("#cReach").empty();
    $("#cTweets").empty();
    $("#cRetweets").empty();
    $("#cFollowers").empty();
    $(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
    $(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
}

$('body').on('click', '#customButton', function () {
    var fromDate = $("#datepicker1").val();
    var toDate = $("#datepicker2").val();
    esClientInfluencers.addDateRangeMatch("created_at", fromDate, toDate);
    //	esClientInfluencers.getIndices(fromDate,toDate,"twitter-","2015-03-20");
    conductSearchInfluencers();
});

$('body').on('click', '.granularityPicker', function () {
    var currentGranularity = $(this).attr("gValue");
    $('.granularityTitle span:first').html(currentGranularity);
    $(".granularityPicker").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var granularityValue = $(this).attr("granularityvalue");
    liveParametersInf.granularity = granularityValue;
    generatemediaMainQueryInfluencers(liveParametersInf);
    conductSearchInfluencers();
});

$('body').on('click', '.sizePicker', function () {
    $(".sizePicker").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    liveParametersInf.size = sizeValue;
    generatemediaMainQueryInfluencers(liveParametersInf);
    conductSearchInfluencers();
});


$('body').on('click', '.downloadLink', function () {
    var downloadToken = $(this).attr('downloadToken');
    var blob = new Blob([csv[downloadToken]], {
        type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "downloadToken.csv");
});



$('body').on('click', '.graph-control-item', function () {
    //$('.graph-control-item').on('click', function () {
    var controlObject = $(this).find("i");
    var lightControlObject = $(this).find("a");
    if (controlObject.hasClass('fa-play')) {
        controlObject.removeClass('fa-play');
        controlObject.addClass('fa-pause');
        lightControlObject.removeAttr('id', "button")
        enableRealtimeStreaming();
    } else {
        controlObject.removeClass('fa-pause');
        controlObject.addClass('fa-play');
        lightControlObject.attr('id', "button")
        enableStreaming = false;
    }

});

var enableStreaming = true;

function enableRealtimeStreaming() {
    var streamFeeds = function () {
        if (!enableStreaming) {
            return;
        }
        conductSearchInfluencers(false);
        setTimeout(streamFeeds, 5 * 1000);
    };
    streamFeeds();
}

var singleRenderFunctionInfluencers = function (response) {

    handleLoadingInfluencers(false);
	$('#influencers').empty();
//    influencerHandler(response);
    influencerHandler2(response);
	console.log('influencers',response);

};




function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}


function handleLoadingInfluencers(isLoading) {
    //        <div class="spinner"><img  id="img-spinner" src="img/spinner.gif" alt="Loading" /> </div>


    if (isLoading==true) {
 		$(".dataContentInfluencers").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
    }
      if (isLoading==false) {
        $(".dataContentInfluencers").empty();
    }
}

var influencerHandler2 = function(response) {
var commonBucket = {};
//commonBucket.dataFirstBucket = [];
dataSecondBucket = [];
var sampleData = [];
	var firstAggBucket = response.aggregations.influencers.buckets;
	for(var buckets in firstAggBucket) {
//	   if (buckets == 0) 
//		   continue;
		commonBucket[buckets]=({
			name : firstAggBucket[buckets].key,
			count: firstAggBucket[buckets].doc_count,
			screen_name:firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.screen_name,
			imageURL: firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.profile_image_url.replace(/normal/,"400x400"),
			followers:firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.followers,
			favourites:firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.favorites,
			retweets:firstAggBucket[buckets].personInfo.hits.hits[0]._source.retweet_count,
			favouritesCount:firstAggBucket[buckets].personInfo.hits.hits[0]._source.favorite_count		
		});
		var secondLevelBucket = firstAggBucket[buckets].personStats.buckets;
		for (var innerBucket in secondLevelBucket){
			sampleData.push({
				tweetType : firstAggBucket[buckets].personStats.buckets[innerBucket].key,
				tweetTypeCount: firstAggBucket[buckets].personStats.buckets[innerBucket].doc_count
			});
		}
		commonBucket[buckets].tweetTypeInfo = sampleData;
		sampleData=[];
//		dataFirstBucket[buckets]['tweetInfo'].push(sampleData);
	}
	var data = $.map(commonBucket, function(value, key) {return {value: value, key: key}});
//console.log('common bucketo',commonBucket);
console.log('common bucketo',data);
//console.log('sample data',commonBucket);
		$('.twitter-influencers').html($('#influencersTrending').render(data));
//	isotopeCall();
	setTimeout(isotopeCall, 1000);
    hoverOverlay();
}


var influencerHandler = function(response){
	$(".dataContentInfluencers").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>')
	var tweetDataArray = [];
	var imageArray = [];
	var data = [];
	var buckets = 0;
	var firstAggBucket = response.aggregations.influencers.buckets;
		console.log('influencer response',firstAggBucket.length);
	for(var buckets in firstAggBucket ){
//		if (buckets == 0)
//            continue;
		data.push({
			name : firstAggBucket[buckets].key,
			count: firstAggBucket[buckets].doc_count,
			screen_name:firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.screen_name,
			imageURL: firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.profile_image_url.replace(/normal/,"400x400"),
			followers:firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.followers,
			favourites:firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.favorites,
			retweets:firstAggBucket[buckets].personInfo.hits.hits[0]._source.retweet_count,
			favouritesCount:firstAggBucket[buckets].personInfo.hits.hits[0]._source.favorite_count,
			tweetType:firstAggBucket[buckets].personStats.buckets[0].key,
			tweetTypeCount:firstAggBucket[buckets].personStats.buckets[0].doc_count
		});
		var secondAggBucket = firstAggBucket[buckets].personStats.buckets;
		for(var secondBuckets in secondAggBucket){
				tweetDataArray.push({
					typeOfTweet : secondAggBucket[secondBuckets].key,
					typeTweetCount : secondAggBucket[secondBuckets].doc_count
				})
		}
	}
	
	for(var imageBuckets in firstAggBucket){
		imageArray.push( firstAggBucket[buckets].personInfo.hits.hits[0]._source.user.profile_image_url.replace(/normal/,"400x400"))
	}
//	preloadPictures(imageArray,renderFunction(data));
	console.log('influencer data array',data);
	console.log('influencer data array',tweetDataArray);
     if(data.length<1){
    $(".dataContentInfluencers").html("No data Available");   
    }
	
	$('.twitter-influencers').html($('#influencersTrending').render(data));
//	isotopeCall();
	setTimeout(isotopeCall, 1000);

    hoverOverlay();	
}

var renderFunction = function (data){
$('.twitter-influencers').html($('#influencersTrending').render(data));
}

var isotopeCall = function(){
//	alert('inside  isotope');
	 $(".dataContentInfluencers").empty();
 $('.twitter-influencers').isotope({
            itemSelector: '.item'
        });
}
//$(document).ready(function() {
//var $container = jQuery('.twitter-influencers');
//    // use imagesLoaded, instead of window.load
//    $container.imagesLoaded( function() {
//        $container.isotope({
//            itemSelector: '.influencers-grid',
//            // masonry is default layoutMode, no need to specify it
//            sortBy: 'random'
//        });
//    })
//});


$('body').on('click', '.influencers-view', function () {
	var idValue = $(this).attr("idPick");
//	alert(idValue);
//	alert(globalDuration);
//	alert(globalTagValue);
	var esClientBubble = new Elasticsearch(esHost, generateTwitterBubbleQuery, liveParametersInf);
	esClientBubble.addIndexPattern("twitter-", twitterTimeFormat);
	esClientBubble.addDateRangeMatch("created_at", "now-" + globalDuration, null);
	esClientBubble.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
	esClientBubble.removeTermMatch("user.screen_name", idValue);
	esClientBubble.addTermMatch("user.screen_name", idValue);
	esClientBubble.search(bubbleDataRenderTwitter);
});

var bubbleDataHolderTwitter = {};

//var bubbleDataRenderTwitter = function (response) {
//console.log('bubble data response',response);
//}

var bubbleDataRenderTwitter = function (response) {
	bubbleDataHolderTwitter.twitter = [];
	console.log('twitter bubble response', response);
	var hit = response.hits.hits;
	for (var hitIndex in hit) {
		bubbleDataHolderTwitter.twitter.push({
			id: hit[hitIndex]._id,
			fullName: hit[hitIndex]._source.user.name,
			name: hit[hitIndex]._source.user.screen_name,
			created_at: utcToDateFormat(hit[hitIndex]._source.created_at),
			action: hit[hitIndex]._source.tweetType,
			totalTweets: hit.total,
			tweet: hit[hitIndex]._source.text,
			friends: hit[hitIndex]._source.user.friends,
			followers: hit[hitIndex]._source.user.followers,
			retweets: hit[hitIndex]._source.retweet_count,
			favourites: hit[hitIndex]._source.favorite_count
		})

	}
//	slimScrollCall();
	console.log('bubbledataholder twitter', bubbleDataHolderTwitter);
	fullContactCall(fcTwitterUrl, hit[0]._source.user.screen_name, "twitter");
};

var fullContactCall = function (fcType, name, identifier) {
	var url = fcType + name;
//	console.log('url to full contact is', url);
	var fullContact = new FullContact(url);
	if (identifier == "twitter") {
		fullContact.getProfile(url, 'id', fullContactRender);
	} else {
		fullContact.getProfile(url, 'id', fullContactRenderOther);
	}
};

var fullContactRender = function (response) {
	console.log('full contact response', response);
	bubbleDataHolderTwitter.profile = [];
	if (response._source) {
		bubbleDataHolderTwitter.profile = response._source;
	} else {
		bubbleDataHolderTwitter.profile = response;
	}
	console.log('total response response', bubbleDataHolderTwitter);
	$(".influencers-block").html($.templates("#single-render-bubble-home").render(innerTwitterDataPush(bubbleDataHolderTwitter)));
	tabSwitchFunctionModal();
	slimScrollCall("tweet-content","300px");
	$("#myModalProfileHome").modal('show');
};

var fullContactRenderOther = function (response) {
	console.log('full contact response other', response);
	bubbleDataHolder.profile = [];
	if (response._source) {
		bubbleDataHolder.profile = response._source;
	} else {
		bubbleDataHolder.profile = response;
	}

	console.log('total response response', bubbleDataHolder);
	$(".influencers-block").html($.templates("#single-render-bubble-home").render(innerTwitterDataPush(bubbleDataHolder)));
	tabSwitchFunctionModal();
//	slimScrollCall();
	slimScrollCall("tweet-content","300px");
	$("#myModalProfileHome").modal('show');
};

var innerTwitterDataPush = function (bubbleDataHolderCom) {
	console.log('inner data', bubbleDataHolderCom);
	var data = [];
	var indexRef = null;
	var bucket = bubbleDataHolderCom.profile.socialProfiles;
	var index = 0;
	for (index; index < bucket.length; index++) {
		if (bucket[index].type == "twitter") {
			data.push(bubbleDataHolderCom.twitter);
			indexRef = index;
		}

	}
	console.log('data zero in inner twitterDataPush', data[0]);
	bubbleDataHolderCom.profile.socialProfiles[indexRef].stats = data[0];
	console.log('inner bubble twitter data is', bubbleDataHolderCom);
	kloutDataAdditionFunction(bubbleDataHolderCom);

	return bubbleDataHolderCom;
};

var kloutDetails = {};

var kloutDataAdditionFunction = function (bubbleDataHolderCom) {
	kloutDetails = {};
	var buckets = bubbleDataHolderCom.profile;
	var index = 0;
	var dataTags = [];

	if (!isNull(buckets.digitalFootprint)) {
		if ((buckets.digitalFootprint.scores.length) != 0) {
			var scoreBuckets = buckets.digitalFootprint.scores;
			for (var indexScores = 0; indexScores < scoreBuckets.length; indexScores++) {
				if (scoreBuckets[indexScores].provider == "klout") {
					kloutDetails.score = scoreBuckets[indexScores].value;
				}
			}
		}
		if ((buckets.digitalFootprint.topics.length) != 0) {
			var topicsBuckets = buckets.digitalFootprint.topics;
			for (var indexTopics = 0; indexTopics < topicsBuckets.length; indexTopics++) {
				if (topicsBuckets[indexTopics].provider == "klout") {
					dataTags.push(topicsBuckets[indexTopics].value);
					var dataTagsString = dataTags.toString();
					kloutDetails['tags'] = dataTagsString;
				}
			}
		}
	}

	if ((buckets.socialProfiles.length) != 0) {
		var kloutBuckets = buckets.socialProfiles
		for (var indexKlout = 0; indexKlout < kloutBuckets.length; indexKlout++) {
			if (kloutBuckets[indexKlout]['type'] == 'klout') {
				for (var kloutDetailsIndex in kloutDetails) {
					kloutBuckets[indexKlout]['score'] = kloutDetails.score;
					kloutBuckets[indexKlout]['tags'] = kloutDetails.tags;
				}
			}

		}
	}
	console.log('with klout data', bubbleDataHolderCom);
	console.log('kloutDetails', kloutDetails);
};


