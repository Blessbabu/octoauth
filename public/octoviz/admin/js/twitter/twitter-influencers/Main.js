$('.main-search').keypress(function (e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});

//DOWNLOAD INFLUENCER LIST
$('body').on('click','.downloadLinkInfl',function(){
	var downloadToken = $(this).attr('downloadtokenInfl');
	
	var blob = new Blob([globalObject.csvInfl[downloadToken]], {
			type: "text/plain;charset=utf-8"
		});
    saveAs(blob, "downloadToken.csv");
});


//SIZE PICKER
$('body').on('click', '.sizePickerInf', function () {
	$(".dataContentInfluencersHeading").html(' <img src="../img/item-loader.gif"> <br>loading');
    $(".sizePickerInf").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizevalue");
    globalObject.liveParametersInf.size = sizeValue;
    // influencerAnalyticsGlobal.handleLoadingInfluencers(true);
    globalObject.initTab();
});

//SORT PICKER
$('body').on('click', '.sortPickerInfluencer', function () {
	$(".dataContentInfluencersHeading").html(' <img src="../img/item-loader.gif"> <br>loading');
    $(".sortPickerInfluencer").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sortIdentifier");
//    var sizeValue = $(this).attr("sizevalue");
    globalObject.liveParametersInf.sortIdentifier = identifier;
    // influencerAnalyticsGlobal.handleLoadingInfluencers(true);
    globalObject.initTab();
});

//BUBBLE MODAL TRIGGER
var idLoaderPick = null;
$('body').on('click', '.influencers-view', function () {
	var idValue = $(this).attr("idPick");
	/*alert(idValue);*/
	idLoaderPick = '#'+$(this).attr("idPickLoader");
	var loaderImage = '';
//	$(idLoaderPick).html('loading..');
	$(idLoaderPick).addClass('influencer-loader');
	$(idLoaderPick).html('loading <img src="../img/loading.gif">');
	
//    $(this).addClass("dataContentLoaderViewMore");
//    $(".dataContentLoaderViewMore").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
	var esClientBubble = new Elasticsearch(esHost, generateTwitterBubbleQuery,{},"twitterBubbleQuery");
	esClientBubble.addIndexPattern("twitter-", twitterTimeFormat);
	esClientBubble.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);
	esClientBubble.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
	// esClientBubble.removeTermMatch("user.name", idValue.toLowerCase());
	esClientBubble.addTermMatch("user.name", idValue.toLowerCase());
	esClientBubble.search(function(resonse){
        console.log('influencer click',resonse)
		globalObject.bubbleDataRenderTwitter(resonse);
	});
});

$(".dataContentOthers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');
$(".dataContentFollowers").html('<div><img  id="img-spinner"  src="img/follower.gif" alt="Loading" /> </div>');


