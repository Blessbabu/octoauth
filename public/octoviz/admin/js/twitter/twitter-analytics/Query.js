var generatemediaAnalyticsQuery = function (liveParametersAnalytics) {
	var size = liveParametersAnalytics.size || 10;
	var granularity = liveParametersAnalytics.granularity || "hour";
	var queryBody = {
		"size": 0,
  "aggs": {
    "topTags": {
      "terms": {
        "field": "hashtag.text",
		 "size": size
      },
      "aggs": {
        "stats": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        },
        "trends": {
          "date_histogram": {
            "field": "created_at",
            "interval": granularity
          }
        }
      }
    }
  }
	};
	return queryBody;
};

var streamingLiveTweetsQuery = function(liveParametersAnalytics){
	var size = liveParametersAnalytics.sizeStream || 10;
	var queryBody = {
		"size": size,
		  "sort": [
			{
			  "created_at": {
				"order": "desc"
			  }
			}
		  ]
		};
	return queryBody;
}
