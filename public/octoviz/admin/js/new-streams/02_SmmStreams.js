//STREAMS IN SOCIAL MEDIA ANALYTICS, HTML GENERATION AND DATA RENDERING HAPPENING HERE
//The getStateSmm initiates the process which will retrieve the previous session's stream data of the logged in user
//Following is the order in which the functions are written
// timeLineHtmlGenBuffer()          generates Html for timeline-twitter
// timeLineRenderTwitter()          rendering data for timeline-twitter
// mentionsHtmlGenBuffer()          generates Html for mentions-twitter
// mentionsRenderSingleTwitter()    rendering data for mentions-twitter    
// userMessageHtmlGenBuffer()       generates Html for user-messages-twitter
// userMessageRenderSingleTwitter() rendering data for user-messages-tiwtter
// userTweetHtmlGenBuffer()         generates Html for user-tweets-twitter
// userTweetRenderSingleTwitter()   rendering data for user-tweet-twitter
// timeLineHtmlGenFaceBook()        generates Html for user-timeline-facebook
// timeLineRenderSingleFaceBook()   rendering data for user -timeline-facebook

function StreamsClass() {
    this.liveParameterSmm = {
        pageNum: 0,
        granularity: null,
        size: null
    };
    this.cpStreamMyTw = 1;
    this.cpStreamTimelineTw = 1;
    this.cpStreamMyTweetTw = 1;
    this.cp = 1;

    this.isPaginationTwiterMyTwt = false;
    this.isPaginationTwiterTimeline = false;
    this.isPaginationTwiterMentions = false;
    this.isPaginationTwiterMessages = false;
    this.isPaginationTimelineFb = false;

    this.clientTwitterTimeLine = null;

}

StreamsClass.prototype = {
    getStateSmm: function() {
        if (isNull(this.esClientSmmStateGet)) {
            this.esClientSmmStateGet = new Elasticsearch(esSmmStateHost, null, null)
        } else {
            this.esClientSmmStateGet.reset();
        }

        var self = this;
        this.esClientSmmStateGet.updateGetSmm(esSmmStateHost, smmStateIndex, smmStateType, user, function(response, error) {
            console.log('success message', error); //for indexed users error=success, for non indexed users error=error
            console.log('success message', response);
            if (error != 'error') {
                console.log('response in get function', response);
                if (!isNull(response._source.timeLineTwitter)) {
                    smmStateVariable['timeLineTwitter'] = response._source.timeLineTwitter;
                }
                if (!isNull(response._source.mentionsTwitter)) {
                    smmStateVariable['mentionsTwitter'] = response._source.mentionsTwitter;
                }
                if (!isNull(response._source.messagesTwitter)) {
                    smmStateVariable['messagesTwitter'] = response._source.messagesTwitter;
                }
                if (!isNull(response._source.tweetsTwitter)) {
                    smmStateVariable['tweetsTwitter'] = response._source.tweetsTwitter;
                }
                if (!isNull(response._source.timeLineFb)) {
                    smmStateVariable['timeLineFb'] = response._source.timeLineFb;
                }


                //          smmStateVariable = response;
                console.log('inside first call', smmStateVariable);
                self.savedStreamsData(smmStateVariable);
            } else {
                //          alert('error captured');
                console.log('inside first call', smmStateVariable);
            }

        });
    },
    stateUpdateFunction: function(smmStateVariable) {
        var smmStateClient = new Elasticsearch(esSmmStateHost, null, null);
        smmStateClient.updateSmm(esSmmStateHost, smmStateIndex, smmStateType, user, smmStateVariable, function(response) {
            console.log('response after update', response);
        })
    },
    stateReindexFunction: function(smmStateVariable) {
        var smmStateClient = new Elasticsearch(esSmmStateHost, null, null);
        smmStateClient.reindexSmm(esSmmStateHost, smmStateIndex, smmStateType, user, smmStateVariable, function(response) {
            console.log('response after reindex', response);
        })
    },
    savedStreamsData: function(smmStateVariable) {
        //console.log('captured response',response);
        var twitterTimeLineData = smmStateVariable.timeLineTwitter;
        var twitterMentionsData = smmStateVariable.mentionsTwitter;
        var twitterMessagesData = smmStateVariable.messagesTwitter;
        var twitterMyTweetsData = smmStateVariable.tweetsTwitter;
        var fbTimeLineData = smmStateVariable.timeLineFb;
        if (!isNull(twitterTimeLineData)) {
            for (var index in smmStateVariable.timeLineTwitter) {
                var param = {};
                param.id = "init";
                param.uName = index;
                // $('.streams-buffer').append(this.timeLineHtmlGenBuffer(param.uName));
                this.timeLineRenderTwitter(smmStateVariable.timeLineTwitter[index], param);
            }
        }
        if (!isNull(twitterMentionsData)) {
            for (var index in smmStateVariable.mentionsTwitter) {
                var param = {};
                param.id = "init";
                param.uName = index;
                // $('.streams-buffer').append(this.mentionsHtmlGenBuffer(param.uName));
                this.mentionsRenderSingleTwitter(smmStateVariable.mentionsTwitter[index], param);
            }
        }
        if (!isNull(twitterMessagesData)) {
            for (var index in smmStateVariable.messagesTwitter) {
                var param = {};
                param.id = "init";
                param.uName = index;
                // $('.streams-buffer').append(this.userMessageHtmlGenBuffer(param.uName));
                this.userMessageRenderSingleTwitter(smmStateVariable.messagesTwitter[index], param);
            }
        }
        if (!isNull(twitterMyTweetsData)) {
            for (var index in smmStateVariable.tweetsTwitter) {
                var param = {};
                param.id = "init";
                param.uName = index;
                // $('.streams-buffer').append(this.userTweetHtmlGenBuffer(param.uName));
                this.userTweetRenderSingleTwitter(smmStateVariable.tweetsTwitter[index], param);
            }

        }
        if (!isNull(fbTimeLineData)) {
            for (var index in fbTimeLineData) {
                var param = {};
                param.id = "init";
                if (fbTimeLineData[index].hits.hits.length > 1) {
                    param.uName = fbTimeLineData[index].hits.hits[0]._source.Page.name;
                    console.log('param uname fb', param.uName);
                } else {
                    param.uName = "no data"
                }
                param.pageId = index;
                /*                        $('.streams-buffer').append(this.timeLineHtmlGenFaceBook(param.pageId,param.uName));
                        console.log('fbTimeLineData',fbTimeLineData[index]) 
*/
                this.timeLineRenderSingleFaceBook(fbTimeLineData[index], param);
            }
        }
    },
    //TIME-LINE-HTML-GENERATOR-TWITTER
    timeLineHtmlGenBuffer: function(userNameDummy) {
        var timeLineHtml = '<div class="social-item tiwtter-streams time-line-tab-' + userNameDummy + ' item-intro clearfix">' +
            '<section class="social-stream">' +
            '<div class="stream-header">' +
            '<h4><i class="ion-social-twitter"></i>Time Line<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
            '<div class="stream-controls">' +
            '<a class="close time-line-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
            '</div>' +
            '</div>' +
            '<div class="stream-data">' +
            //      '<div class="dataContentTwitterTimelineStreams'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
            '<div class="stream-content slimScrollTimeLine">' +
            '<div class="dataContentTwitterTimelineStreams' + userNameDummy + '" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>' +
            '<div class="stream-content-timeline-' + userNameDummy + '" style="text-align: center"></div>' +
            '<ul class="vertical-list timeline-list-' + userNameDummy + '">' +

            '</ul>' +
            '</div>' +
            '</div>' +
            ' </section>' +
            '</div>';
        return timeLineHtml;
    },
    //TIME-LINE-DATA-RENDER-TWITTER
    timeLineRenderTwitter: function(response, param) {
        console.log('response from timeline', response);
        console.log('smm state variabe', smmStateVariable);

        var totalHits = response.hits.total;
        // alert(totalHits);
        // if(this.isPaginationTwiterTimeLine == false) {
        //   alert("vdnfmg");
        if (!this.isPaginationTwiterTimeLine) {
            this.addPagination(totalHits, 'userTimeLineStreamPagination');
        }

        // }
        this.isPaginationTwiterTimeLine = false;

        var hit = response.hits.hits
        console.log('totalhits A', totalHits);
        var data = [];
        for (var buckets in hit) {
            console.log('buckets', buckets);
            data.push({
                name: hit[buckets]._source.user.name,
                favourites: hit[buckets]._source.favorite_count,
                retweets: hit[buckets]._source.retweet_count,
                followers: hit[buckets]._source.user.followers,
                image: hit[buckets]._source.user.profile_image_url,
                friends: hit[buckets]._source.user.friends,
                tweet: hit[buckets]._source.text,
                created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
            })
        }
        // console.log('time line data..................',data);
        // $('.dataContentTwitterTimelineStreams'+this.managementUserName+'').empty();
        // if(param.id=='init'){
        //     $('.timeline-list-'+param.uName+'').html($('#time-line').render(data));
        //     if(totalHits==0){
        //         $('.stream-content-timeline-'+param.uName+'').html('no data available');
        //     }
        // }
        // if(param.id==null){
        //    $('.timeline-list-'+globalObject.managementUserName+'').html($('#time-line').render(data));
        //       if(totalHits==0){
        //         $('.stream-content-timeline-'+globalObject.managementUserName+'').html('no data available');
        //     }
        // }

        if (param.id == 'init') {
            $('#twitter-timelines').html($('#twitter-timelines-render').render(data));

        }
        if (param.id == null) {
            $('#twitter-timelines').html($('#twitter-timelines-render').render(data));

        }
        globalObject.timeLineTwitterData = data;
        slimScrollCall("slimScrollStreamTimeLine", "500px");
    },

    //MENTIONS-HTML-GENERATOR-TWITTER
    mentionsHtmlGenBuffer: function(userNameDummy) {
        var mentionsHtml = '<div class="social-item tiwtter-streams mentions-tab-' + userNameDummy + ' item-intro clearfix">' +
            '<section class="social-stream">' +
            '<div class="stream-header">' +
            '<h4><i class="ion-social-twitter"></i> Mentions<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
            '<div class="stream-controls">' +
            '<a class="close mentions-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
            '</div>' +
            '</div>' +
            '<div class="stream-data">' +
            //      '<div class="dataContentTwitterMentions'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
            '<div class="stream-content slimScrollMentions">' +
            '<div class="dataContentTwitterMentions' + userNameDummy + '" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>' +
            '<div class="stream-content-mentions-' + userNameDummy + '" style="text-align: center"></div>' +
            '<ul class="vertical-list mentions-list-' + userNameDummy + '">' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</section>' +
            '</div>';
        return mentionsHtml;
    },
    //MENTIONS-DATA-RENDER-TWITTER
    mentionsRenderSingleTwitter: function(mentionsResponse, param) {
        var totalHits = mentionsResponse.hits.hits.length;
        if (this.isPaginationTwiterMentions == false) {
            this.addPagination(totalHits, 'userMentionStreamPagination');
        }
        this.isPaginationTwiterMentions = false;
        var hit = mentionsResponse.hits.hits
        console.log('totalhits B', totalHits);
        var data = [];
        for (var buckets in hit) {
            console.log('buckets', buckets);
            data.push({
                name: hit[buckets]._source.user.name,
                favourites: hit[buckets]._source.favorite_count,
                retweets: hit[buckets]._source.retweet_count,
                followers: hit[buckets]._source.user.followers,
                image: hit[buckets]._source.user.profile_image_url,
                friends: hit[buckets]._source.user.friends,
                tweet: hit[buckets]._source.text,
                created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
            })
        }
        console.log('mentions data', data);

        // $("#twitter-mentions").empty();
        // $('.dataContentTwitterMentions'+this.managementUserName).empty();
        // if(param.id=='init'){
        //     $('.mentions-list-' + param.uName + '').html($('#mentions').render(data));
        //      if(totalHits==0){
        //         $('.stream-content-mentions-'+param.uName+'').html('no data available');
        //     }
        // }
        // if(param.id==null){
        //     $('.mentions-list-' + globalObject.managementUserName + '').html($('#mentions').render(data));
        //      if(totalHits==0){
        //         $('.stream-content-mentions-'+globalObject.managementUserName+'').html('no data available');
        //     }
        //     else{

        //     }
        // }

        if (param.id == 'init') {
            $('#twitter-mentions').html($('#twitter-mentions-render').render(data));

        }
        if (param.id == null) {
            $('#twitter-mentions').html($('#twitter-mentions-render').render(data));

        }
        globalObject.mentionsTwitterData = data;
        slimScrollCall("slimScrollStreamMentions", "500px");
    },
    //USER MESSAGE-HTML-GENERATOR-TWITTER
    userMessageHtmlGenBuffer: function(userNameDummy) {
        var userMessageHtml = '<div class="social-item tiwtter-streams user-message-tab-' + userNameDummy + ' item-intro clearfix">' +
            '<section class="social-stream">' +
            '<div class="stream-header">' +
            ' <h4><i class="ion-social-twitter"></i>Messages<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
            '<div class="stream-controls">' +
            '<a class="close message-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
            '</div>' +
            '</div>' +
            '<div class="stream-data">' +
            //          '<div class="dataContentTwitterUserMessage'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
            '<div class="stream-content stream-content-message-' + userNameDummy + ' slimScrollMessage">' +
            '<div class="dataContentTwitterUserMessage' + userNameDummy + '" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>' +
            '<div class="stream-content-message-' + userNameDummy + '" style="text-align: center"></div>' +
            '<ul class="vertical-list message-list-' + userNameDummy + '">' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</section>' +
            '</div>';
        return userMessageHtml;
    },
    //USER MESSAGE-DATA-RENDER-TWITTER
    userMessageRenderSingleTwitter: function(tweetsResponse, param) {
        var totalHits = tweetsResponse.hits.total;

        if (this.isPaginationTwiterMessages == false) {
            this.addPaginationMessages(totalHits, 'userMessageStreamPagination');
        }
        this.isPaginationTwiterMessages = false;
        var hit = tweetsResponse.hits.hits
        console.log('totalhits C', totalHits);
        var data = [];
        for (var buckets in hit) {
            console.log('buckets', buckets);
            data.push({
                name: hit[buckets]._source.sender_screen_name,
                //          favourites : hit[buckets]._source.favorite_count,
                //          retweets : hit[buckets]._source.retweet_count,
                //          followers : hit[buckets]._source.user.followers,
                image: hit[buckets]._source.sender.profile_image_url,
                //          friends : hit[buckets]._source.user.friends,
                message: hit[buckets]._source.text,
                created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
            })
        }
        console.log("twitter messages data....", data);
        // $('.dataContentTwitterUserMessage'+globalObject.managementUserName+'').empty();
        // if(param.id=='init'){
        //     $('.message-list-' + param.uName + '').html($('#message-render').render(data));
        //     if(totalHits==0){
        //         $('.stream-content-message-'+param.uName+'').html('no data available');
        //     }
        // }
        // if(param.id==null){
        //     $('.message-list-' + globalObject.managementUserName + '').html($('#message-render').render(data));
        //     if(totalHits==0){
        //         $('.stream-content-message-'+globalObject.managementUserName+'').html('no data available');
        //     }
        // }

        if (param.id == 'init') {
            $('#twitter-messages').html($('#twitter-messages-render').render(data));

        }
        if (param.id == null) {
            $('#twitter-messages').html($('#twitter-messages-render').render(data));

        }
        globalObject.messagesTwitterData = data;
        slimScrollCall("slimScrollStreamMessages", "500px");

    },
    //USER TWEET-HTML-GENERATOR-TWITTER
    userTweetHtmlGenBuffer: function(userNameDummy) {
        var userTweetHtml = '<div class="social-item tiwtter-streams user-tweet-tab-' + userNameDummy + ' item-intro clearfix">' +
            ' <section class="social-stream">' +
            '<div class="stream-header">' +
            ' <h4><i class="ion-social-twitter"></i>Tweets<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
            '<div class="stream-controls">' +
            '<a class="close user-tweet-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
            '</div>' +
            '</div>' +
            '<div class="stream-data">' +
            //      '<div class="dataContentTwitterTweets'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
            '<div class="stream-content slimScrollTweets">' +
            '<div class="dataContentTwitterTweets' + userNameDummy + '" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>' +
            '<div class="stream-content-tweets-' + userNameDummy + '" style="text-align: center"></div>' +
            '<ul class="vertical-list tweet-list-' + userNameDummy + '">' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</section>' +
            '</div>';
        return userTweetHtml;
    },
    //USER TWEET-DATA-RENDER-TWITTER
    userTweetRenderSingleTwitter: function(tweetsResponse, param) {
        // alert("cdzfgxh");
        var totalHits = tweetsResponse.hits.total;
        // alert(totalHits);
        // alert(totalHits);

        if (this.isPaginationTwiterMyTwt == false) {
            this.addPaginationMyTweet(totalHits, 'userTweetStreamPagination');
        }
        this.isPaginationTwiterMyTwt = false;

        var hit = tweetsResponse.hits.hits
        console.log('totalhits D', totalHits);
        var data = [];
        for (var buckets in hit) {
            console.log('buckets', buckets);
            data.push({
                name: hit[buckets]._source.user.name,
                favourites: hit[buckets]._source.favorite_count,
                retweets: hit[buckets]._source.retweet_count,
                followers: hit[buckets]._source.user.followers,
                image: hit[buckets]._source.user.profile_image_url,
                friends: hit[buckets]._source.user.friends,
                tweet: hit[buckets]._source.text,
                created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
            })
        }
        $('.divtwitter-my-tweets').empty();
        if(hit.length < 1){
            $('.divtwitter-my-tweets').html('No data available');
        }

        // $('.dataContentTwitterTweets'+this.managementUserName+'').empty();
        if (param.id == 'init') {
            $('#twitter-my-tweets').html($('#twitter-mytweets-render').render(data));
            // $('.tweet-list-' + param.uName + '').html($('#tweets-render').render(data));
            // if(totalHits==0){
            //     $('.stream-content-tweets-'+param.uName+'').html('no data available');
            // }
        }
        if (param.id == null) {
            $('#twitter-my-tweets').html($('#twitter-mytweets-render').render(data));
            // $('#twitter-mytweets-render').html($('#twitter-my-tweets').render(data));
            // $('.tweet-list-' + globalObject.managementUserName + '').html($('#tweets-render').render(data));
            //  if(totalHits==0){
            //     $('.stream-content-tweets-'+globalObject.managementUserName +'').html('no data available');
            // }
        }
        globalObject.myTweetsData = data;
        slimScrollCall("slimScrollStreamTweet", "400px");
    },

    addPagination: function(totalHitsPn, paginationClass) {
        var self = this;
        $('.' + paginationClass).pagination({
            items: totalHitsPn,
            itemsOnPage: 10,
            cssStyle: 'dark-theme',
            currentPage: self.cpStreamTimelineTw,
            displayedPages: 10,
            onPageClick: function(a) {
                self.cpStreamTimelineTw = a;
                console.log("hi",a);
                self.isPaginationTwiterTimeline = true;
                clientTwitterTimeLine.addQueryParameter("pageNum", (self.cpStreamTimelineTw - 1) * 10);
                self.streamstimeLineRender(globalObject.managementUserName);
                return false;
            }
        });
    },

    addPaginationMyTweet: function(totalHitsPn, paginationClass) {
        var self = this;
        $('.' + paginationClass).pagination({
            items: totalHitsPn,
            itemsOnPage: 10,
            cssStyle: 'dark-theme',
            currentPage: self.cpStreamMyTweetTw,
            displayedPages: 10,
            onPageClick: function(a) {
                self.cpStreamMyTweetTw = a;
                console.log(a);
                self.isPaginationTwiterMyTwt = true;
                clientTwitterTimeLine.addQueryParameter("pageNum", (self.cpStreamMyTweetTw - 1) * 10);
                self.streamsMyTweetRender(globalObject.managementUserName);
                return false;
            }
        });
    },


    addPaginationMessages: function(totalHitsPn, paginationClass) {
        var self = this;
        $('.' + paginationClass).pagination({
            items: totalHitsPn,
            itemsOnPage: 10,
            cssStyle: 'dark-theme',
            currentPage: self.cpStreamMessagesTw,
            displayedPages: 10,
            onPageClick: function(a) {
                self.cpStreamMessagesTw = a;
                console.log(a);
                self.isPaginationTwiterMessages = true;
                clientTwitterTimeLine.addQueryParameter("pageNum", (self.cpStreamMessagesTw - 1) * 10);
                self.streamsMessagesRender(globalObject.managementUserName);
                return false;
            }
        });
    },


    addPaginationTimelineFb: function(totalHitsPn, paginationClass) {
        var self = this;
        $('.' + paginationClass).pagination({
            items: totalHitsPn,
            itemsOnPage: 10,
            cssStyle: 'dark-theme',
            currentPage: self.cpStreamMessagesTw,
            displayedPages: 10,
            onPageClick: function(a) {
                self.cpStreamMessagesTw = a;
                console.log(a);
                self.isPaginationTwiterMessages = true;
                
                self.streamFbRender(globalObject.userPageId,globalObject.managementUserName,(self.cpStreamMessagesTw - 1) * 10);
                return false;
            }
        });
    },




    //USER TIMELINE-HTML-GENERATOR-FACEBOOOK
    timeLineHtmlGenFaceBook: function(pageIdFbPick, pageNameFb) {
        if (!isNull(pageNameFb)) {
            var pageName = pageNameFb
        } else {
            var pageName = globalObject.pageNameFbPickBuffer
        }

        var timeLineHtml = '<div class="social-item tiwtter-streams time-line-tab-' + pageIdFbPick + ' item-intro clearfix">' +
            '<section class="social-stream">' +
            '<div class="stream-header">' +
            '<h4><i class="ion-social-facebook"></i>Time Line<span class="twitter-name-display">' + pageName + '</span></h4>' +
            '<div class="stream-controls">' +
            '<a class="close time-line-close-fb" closeId="' + pageIdFbPick + '"><i class="ion-ios-close-empty"></i></a>' +
            '</div>' +
            '</div>' +
            '<div class="stream-data">' +
            '<div class="stream-content slimScrollTimeLine">' +
            '<div class="dataContentFbStreams' + pageIdFbPick + '" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>' +
            '<div class="stream-content-timelineFb-' + pageIdFbPick + '" style="text-align: center"></div>' +
            '<ul class="vertical-list timeline-list-' + pageIdFbPick + '">' +
            '</ul>' +
            '</div>' +
            '</div>' +
            ' </section>' +
            '</div>';


        return timeLineHtml;
    },
    //USER TIMELINE-DATA-RENDER-FACEBOOOK
    timeLineRenderSingleFaceBook: function(response, param) {
        console.log('response for fb post timeline.......', response);

        $('.divFbTimeLine').empty();

        var totalHits = response.hits.total;
        var hit = response.hits.hits;
        if (this.isPaginationTimelineFb == false) {
            this.addPaginationTimelineFb(totalHits, 'userFbStreamPagination');
        }
        this.isPaginationTimelineFb = false;

        //  console.log('totalhits',totalHits);
        var data = [];
        for (var buckets in hit) {
            console.log('buckets', buckets);
            data.push({
                text: hit[buckets]._source.text,
                likesCount: hit[buckets]._source.likesCount,
                sharesCount: hit[buckets]._source.sharesCount,
                pageName: hit[buckets]._source.Page.name,
                pageId: hit[buckets]._source.Page.id,
                userName: hit[buckets]._source.managementUser.name,
                userId: hit[buckets]._source.managementUser.id,

                created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
            })
        }
        globalObject.timeLineFacebookData =data;

        
        if(hit.length <1){
            $('.divFbTimeLine').html('No data available');
        }

        if (param.id == 'init') {
            $('#fb-timeline').html($('#fb-timeline-render').render(data));

        }
        if (param.id == null) {
            $('#fb-timeline').html($('#fb-timeline-render').render(data));
        }


        slimScrollCall("slimScrollStreamFbTimeLine", "400px");
    },

    streamstimeLineRender: function(username) {

        globalObject.managementUserName = username;
        if (isNull(this.clientTwitterTimeLine)) {
            clientTwitterTimeLine = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', function() {
                return {}
            }, this.liveParameterSmm, "ssmQueryFunction");
        }

        /*  $('.streams-buffer').append(StreamsClassObj.timeLineHtmlGenBuffer(globalObject.managementUserName));
    $('.dataContentTwitterTimelineStreams'+globalObject.managementUserName+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
*/
        clientTwitterTimeLine.addTermMatch("managementUser", globalObject.managementUserName);
        clientTwitterTimeLine.addTermMatch("managementSource", "timeline");
        clientTwitterTimeLine.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);
        var self = this;
        clientTwitterTimeLine.search(function(response) {
            // var name = globalObject.managementUserName;
            // smmStateVariable['timeLineTwitter'][''+name+''] = response;
            // self.stateUpdateFunction(smmStateVariable);
            var param = {
                id: null
            }
            self.timeLineRenderTwitter(response, param);
        });


    },
    streamsMentionsRender: function(username) {
        globalObject.managementUserName = username;
        clientNameMentions = 'esClientTwitterMentionsNew' + globalObject.managementUserName + '';
        clientNameMentions = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', function() {
            return {}
        }, {}, "emptyQueryFunction");

        clientNameMentions.addTermMatch("managementUser", globalObject.managementUserName);
        clientNameMentions.addTermMatch("managementSource", "mention");
        clientNameMentions.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);
        // $('.streams-buffer').append(StreamsClassObj.mentionsHtmlGenBuffer(globalObject.managementUserName));
        // $('.dataContentTwitterMentions'+globalObject.managementUserName+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
        var self = this;
        clientNameMentions.search(function(response) {
            var name = globalObject.managementUserName;
            smmStateVariable['mentionsTwitter']['' + name + ''] = response;
//            self.stateUpdateFunction(smmStateVariable);
            var param = {
                id: null
            }
            self.mentionsRenderSingleTwitter(response, param);
        });


    },

    streamsMessagesRender: function(username) {
        globalObject.managementUserName = username;
        clientNameNew = 'esClientTwitterMessagesNew' + globalObject.managementUserName + '';
        clientNameNew = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', "", this.liveParameterSmm, "ssmQueryFunction");
        clientNameNew.addTermMatch("managementUser", globalObject.managementUserName);
        clientNameNew.addTermMatch("managementSource", "message");
        clientNameNew.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);
        var self = this;
        clientNameNew.search(function(response) {
            var name = globalObject.managementUserName;
            smmStateVariable['messagesTwitter']['' + name + ''] = response;
//            self.stateUpdateFunction(smmStateVariable);
            var param = {
                id: null
            }
            self.userMessageRenderSingleTwitter(response, param);
        });

    },
    streamsMyTweetRender: function(username) {
        globalObject.managementUserName = username;

        var clientNameNew = 'esClientTwitterTweetNew' + globalObject.managementUserName + '';
        clientNameNew = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', "", this.liveParameterSmm, "ssmQueryFunction");
        clientNameNew.addTermMatch("managementUser", globalObject.managementUserName);
        clientNameNew.addTermMatch("managementSource", "tweet");
        clientNameNew.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
        var self = this;
        $(".divtwitter-my-tweets").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        clientNameNew.search(function(response) {

            var name = globalObject.managementUserName;
            smmStateVariable['tweetsTwitter']['' + name + ''] = response;
//            self.stateUpdateFunction(smmStateVariable);
            var param = {
                id: null
            }
            self.userTweetRenderSingleTwitter(response, param);
        });

    },

    streamFbRender: function(pageId, username,pageNo) {

        globalObject.pageIdFbPickBuffer = pageId;
        globalObject.pageNameFbPickBuffer = username;
        clientNameFbTimeline = 'esClientFbTimeline' + globalObject.pageIdFbPickBuffer + '';
        clientNameFbTimeline = new Elasticsearch(esClientDemoHostFb + '/fbmandata/post', function() {
            return {}
        }, this.liveParameterSmm, "ssmQueryFunction");  
        
        var self = this;
        if(pageNo){
           clientNameFbTimeline.addQueryParameter("pageNum", pageNo); 
        }
        

        clientNameFbTimeline.addTermMatch("Page.id", globalObject.pageIdFbPickBuffer);

        clientNameFbTimeline.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);

        $(".divFbTimeLine").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>')

        clientNameFbTimeline.search(function(response) {

            var pageId = globalObject.pageIdFbPickBuffer;
            var name = globalObject.pageNameFbPickBuffer;
            smmStateVariable['timeLineFb']['' + pageId + ''] = response;
//            self.stateUpdateFunction(smmStateVariable);
            var param = {
                id: null
            }
            self.timeLineRenderSingleFaceBook(response, param);
        });



    },
  

    processFacebookTwitterResponse:function(){

     if (globalObject.namesHolderTwitter.length > 0) {
        globalObject.nonEmptyTwitterArrayCalls();
        StreamsClassObj.streamstimeLineRender(globalObject.userNameHold);
        StreamsClassObj.streamsMentionsRender(globalObject.userNameHold);
        StreamsClassObj.streamsMessagesRender(globalObject.userNameHold);
        StreamsClassObj.streamsMyTweetRender(globalObject.userNameHold);



    } else {
        globalObject.onEmptyTwitterArrayCalls();
    }

    if (globalObject.namesHolderFaceBook.length > 0) {
        globalObject.nonEmptyFacebookArrayCalls();
        StreamsClassObj.streamFbRender(globalObject.userPageId, globalObject.userNameHold);
    } else {
        globalObject.onEmptyFacebookArrayCalls();
    }



    }

}
var StreamsClassObj = new StreamsClass();

//TIMELINE FACEBOOK PAGE
$('body').on('click', '.timelineFbNew', function() {
    globalObject.pageIdFbPickBuffer = $(this).attr('page-id');
    globalObject.pageNameFbPickBuffer = $(this).attr('user-pageName');
    clientNameFbTimeline = 'esClientFbTimeline' + globalObject.pageIdFbPickBuffer + '';
    clientNameFbTimeline = new Elasticsearch(esClientDemoHostFb + '/fbmandata/post', function() {
        return {}
    }, this.liveParameterSmm, "ssmQueryFunction");
    var self = this;
    clientNameFbTimeline.addTermMatch("Page.id", globalObject.pageIdFbPickBuffer);

    clientNameFbTimeline.addDateRangeMatch("created_at", globalStartDuration, globalEndDuration);

    $(".divFbTimeLine").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>')

    clientNameFbTimeline.search(function(response) {
        console.log("fb new response................", response);
        var pageId = globalObject.pageIdFbPickBuffer;
        var name = globalObject.pageNameFbPickBuffer;
        smmStateVariable['timeLineFb']['' + pageId + ''] = response;
//        StreamsClassObj.stateUpdateFunction(smmStateVariable);
        var param = {
            id: null
        }
        StreamsClassObj.timeLineRenderSingleFaceBook(response, param);
    });



});


$('body').on('click', '.individual-users', function() {

    globalObject.namesHolderTwitter = [];
    globalObject.namesHolderFaceBook = [];
    globalObject.pageIdsFbContentPost = [];
    globalObject.pageNamesFbHolder = [];
    $('.individual-users ').removeClass("current");
    globalObject.userNameHold = $(this).attr('user-name');
    globalObject.userIndex = $(this).attr('user-index');
    globalObject.userType = $(this).attr('user-type');
    globalObject.userPageId = $(this).attr('page-id');
    var userPageName = $(this).attr('user-pageName')
  /*  console.log("name holder twitter....", globalObject.namesHolderTwitter);*/
    if ($(this).attr('user-type') == "facebook") {
          $(".stream-buffer-section").click();
        $(".twitter-tab-list").hide();
        $(".twitter-tab-content").hide();
        $(".fb-tab-list").show();
        $(".fb-tab-content").show();
        $(".generalReportAggregateFbBuffer").show();
        $(".generalReportAggregateBuffer").hide();


    } else if ($(this).attr('user-type') == "twitter") {

       $(".stream-buffer-section").click();
        $(".generalReportAggregateFbBuffer").hide();
        $(".generalReportAggregateBuffer").show();

        $(".fb-tab-list").hide();
        $(".fb-tab-content").hide();
        $(".twitter-tab-list").show();
        $(".twitter-tab-content").show();
    }

    if ($(this).hasClass("current")) {
        $(this).removeClass('current');
        var indexValTwitter = globalObject.namesHolderTwitter.indexOf(globalObject.userNameHold);
        var indexValFb = globalObject.namesHolderFaceBook.indexOf(globalObject.userNameHold);
        var indexValFbPostId = globalObject.pageIdsFbContentPost.indexOf(globalObject.userPageId);
        var indexValFbPageName = globalObject.pageNamesFbHolder.indexOf(userPageName);
        if (indexValTwitter > -1) {
            globalObject.namesHolderTwitter.splice(indexValTwitter, 1);
        }
        if (indexValFb > -1) {
            globalObject.namesHolderFaceBook.splice(indexValFb, 1);
            globalObject.pageIdsFbContentPost.splice(indexValFbPostId, 1);
            globalObject.pageNamesFbHolder.splice(indexValFbPageName, 1);
        }

    } else {
        $(this).addClass('current');
        if (globalObject.userType == "twitter") {
            globalObject.namesHolderTwitter.push(globalObject.userNameHold);
        } else {
            globalObject.namesHolderFaceBook.push(globalObject.userNameHold);
            globalObject.pageIdsFbContentPost.push(globalObject.userPageId);
            globalObject.pageNamesFbHolder.push(userPageName);
        }
    }
    //    console.clear(); // ADVISABLE -> SOMETIMES FOR TESTING IT IS GOOD TO COMMENT THIS
    console.log('twitter array', globalObject.namesHolderTwitter); //DONT REMOVE THIS CONSOLE
    console.log('fb array', globalObject.namesHolderFaceBook); //DONT REMOVE THIS CONSOLE
    console.log('fb ids array', globalObject.pageIdsFbContentPost); //DONT REMOVE THIS CONSOLE
    console.log('fb page names array', globalObject.pageNamesFbHolder); //DONT REMOVE THIS CONSOLE



    StreamsClassObj.processFacebookTwitterResponse();

});




//CLOSE TIMELINE TWITTER
$('body').on('click', '.time-line-close', function() {
    var closeId = $(this).attr('closeId');
    var removeId = closeId;
    delete smmStateVariable.timeLineTwitter[removeId];
    console.log('removed smm twitter timeline', smmStateVariable);
//    StreamsClassObj.stateReindexFunction(smmStateVariable);
    $('.time-line-tab-' + closeId + '').hide();
});
//CLOSE TIMELINE FACEBOOK
$('body').on('click', '.time-line-close-fb', function() {
    var closeId = $(this).attr('closeId');
    delete smmStateVariable.timeLineFb[closeId];
    console.log('removed smm facebok timeline', smmStateVariable);
//    StreamsClassObj.stateReindexFunction(smmStateVariable);
    $('.time-line-tab-' + closeId + '').hide();
});
//CLOSE MENTIONS TWITTER
$('body').on('click', '.mentions-close', function() {
    var closeId = $(this).attr('closeId');
    delete smmStateVariable.mentionsTwitter[closeId];
    console.log('removed smm twitter mentions', smmStateVariable);
//    StreamsClassObj.stateReindexFunction(smmStateVariable)
    $('.mentions-tab-' + closeId + '').hide();
});
//CLOSE USER TWEET TWITTER
$('body').on('click', '.user-tweet-close', function() {
    var closeId = $(this).attr('closeId');
    delete smmStateVariable.tweetsTwitter[closeId];
    console.log('removed smm twitter tweets', smmStateVariable);
//    StreamsClassObj.stateReindexFunction(smmStateVariable);
    $('.user-tweet-tab-' + closeId + '').hide();
});
//CLOSE USER MESSAGE TWITTER
$('body').on('click', '.message-close', function() {
    var closeId = $(this).attr('closeId');
    delete smmStateVariable.messagesTwitter[closeId];
    console.log('removed smm twitter messages', smmStateVariable);
//    StreamsClassObj.stateReindexFunction(smmStateVariable);
    $('.user-message-tab-' + closeId + '').hide();
});