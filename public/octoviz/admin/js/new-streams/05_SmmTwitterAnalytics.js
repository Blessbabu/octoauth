//LIST OF METHODS
//    followersTimeLine()
//    followersAnalyticsGraphDataGen()
//    retweetsTimeLine()
//    retweetsAnalyticsGraphDataGen()
//    mentionsTimeLine()
//    mentionsAnalyticsGraphDataGen()


function SmmTwitterAnalyticsClasses() {}

SmmTwitterAnalyticsClasses.prototype = {
    
    followersTimeLine: function (namesHolderTwitter) {
        console.log('in followers timeline', globalObject.namesHolderTwitter)
        var esClientTwitterReportHolderFollowers = [];
        var index = 0;
        for (index; index < globalObject.namesHolderTwitter.length; index++) {
            var esClientTwitterReport = 'esClientTwitterReport' + index;
            console.log(esClientTwitterReport);
            esClientTwitterReport = new Elasticsearch(esHostTwitterStream + '/tweetman-new/analysis', "", globalObject.parametersFollowersAggregate, "followersAnalytics");
            esClientTwitterReport.addQueryString('managementUser', globalObject.namesHolderTwitter[index]);
            esClientTwitterReport.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
            esClientTwitterReportHolderFollowers.push(esClientTwitterReport);
        }

        var esClientMultiTwitterReport = new ElasticsearchMultiQuery(esHost);
        esClientMultiTwitterReport.applyESInstances(esClientTwitterReportHolderFollowers);
    
        var self = this;
        $(".dataContentChartHomeTrendsFOA").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
         $(".dataContentChartHomeTrendsFG").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

        esClientMultiTwitterReport.search(function (response) {
                self.followersAnalyticsGraphDataGen(response);
            })
          
    },
    followersAnalyticsGraphDataGen: function (responseObject) {
        // alert("dadb");
        console.log('response object for followers', responseObject);
        var followersGrowth=[];
        var barChart = {};
        for (var arrayIndex = 0; arrayIndex < globalObject.namesHolderTwitter.length; arrayIndex++) {
            barChart['' + globalObject.namesHolderTwitter[arrayIndex] + ''] = [];
        }
        //      console.log('response for followers',responseObject.responses);
        if (responseObject) {
            var response = responseObject.responses;
            for (var index = 0; index < response.length; index++) {
                arIndex = parseInt(index);
                var aggregation = response[index].aggregations.trendsFollowersAnalytics.buckets;
                //            console.log('aggregation',aggregation);
                for (var firstIndex = 0; firstIndex < aggregation.length; firstIndex++) {
                    var milliSecondValue = aggregation[firstIndex].key;
                    var date = new Date(milliSecondValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month = monthAbbr[month];
                    var year = date.getFullYear();
                    var followersCount = aggregation[firstIndex].followers.hits.hits[0]._source.followers_count;
                    barChart['' + globalObject.namesHolderTwitter[arIndex] + ''].push({
                        "dateInEpoch": milliSecondValue,
                        "date": day + ' ' + month + ' ' + year,
                        "value": followersCount
                    })
                    followersGrowth.push({
                        "dateInEpoch": milliSecondValue,
                        "date": day + ' ' + month + ' ' + year,
                        "value": followersCount
                    });

                }
            }
        }

        console.log('followersGrowth chart', followersGrowth);
        var combinedData = getCombinedData(barChart, globalObject.namesHolderTwitter.length);
        var balancedData = getBalancedData(combinedData, globalObject.namesHolderTwitter);
        var balancedArray = getBalancedArray(balancedData);
        var graph = generateGraphAggr(globalObject.namesHolderTwitter);
        // console.log("graph  followers.....",graph);

        var graphId = "followersAggregate";
        var commentClass =".dataContentChartHomeTrendsFOA";
        generateAggregateGraph(balancedArray, graph, graphId,"",commentClass);
         

         var graphId = "followersGrowth";
         commentClass =".dataContentChartHomeTrendsFG";
         var balancedArray=this.dataForFollowersGrowth(followersGrowth);
          this.generateFollowersGrowthGraph(balancedArray, graphId,commentClass);
         


     },

   generateFollowersGrowthGraph:function(data, graphId,commentClass){
        $(commentClass).empty();
       if(data.length < 1) {
         $(commentClass).html('No data available');
       }
       var chart = AmCharts.makeChart(graphId, {
        "type": "serial",
        "theme": "light",
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0.5,
            "gridAlpha": 0,
            
             "integersOnly":true
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
           
            "lineAlpha": 0.7,
            "type": "smoothedLine",
            "lineColor":"#04A182",
            "negativeLineColor": "#04A182",
            "valueField": "value"
        }],
        "chartCursor": {

            "cursorAlpha": 0

        },
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
          
            "minorGridEnabled": true,
            "labelRotation":-40

        },
        
        "export": {
          "enabled": true,
          "fileName" : "news media statistics",
          "libs": {
            "path": "http://amcharts.com/lib/3/plugins/export/libs/"
          },
          "menu": [
            {
              "class": "export-main",
              "menu": [
                {
                  "label": "Download as image",
                  "menu": [
                    "PNG",
                    "JPG",
                    "SVG",
                    "PDF"
                  ]
                },
                {
                  "label": "Download data",
                  "menu": [
                    "CSV",
                    "XLSX",
                    "JSON"
                  ]
                },
                {
                  "label": "Print",
                  "menu": [
                    "PRINT"
                  ]
                }
              ]
            }
          ]
        }

    })

    globalObject.twitterBufferCharts.push(chart);

     },


     dataForFollowersGrowth:function(balancedArray){
       console.log("followers array......",balancedArray);
       
        var newA = [];
        for (var i = 1; i < balancedArray.length; i++) {
             
              newA.push({ "dateInEpoch": balancedArray[i].dateInEpoch,
                        "date":balancedArray[i].date,
                        "value":(balancedArray[i].value-balancedArray[i-1].value)});

        }
        return newA;

     },




    retweetsTimeLine: function (namesHolderTwitter) {
        var esClientTwitterReportHolderRetweets = [];
        var index = 0;
        for (index; index < namesHolderTwitter.length; index++) {
            var esClientTwitterReportRetweet = 'esClientTwitterReportRetweet' + index;
            console.log(esClientTwitterReportRetweet);
            esClientTwitterReportRetweet = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', "", globalObject.parametersRetweetsAggregate, "retweetsAnalytics");
            esClientTwitterReportRetweet.addQueryString('managementUser', namesHolderTwitter[index]);
            esClientTwitterReportRetweet.addQueryString('tweetType', "retweet");
            esClientTwitterReportRetweet.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
            esClientTwitterReportHolderRetweets.push(esClientTwitterReportRetweet);
        }

        var esClientMultiTwitterReportRetweets = new ElasticsearchMultiQuery(esHost);
        esClientMultiTwitterReportRetweets.applyESInstances(esClientTwitterReportHolderRetweets);

        var self = this;
        $(".dataContentChartHomeTrendsRA").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

        esClientMultiTwitterReportRetweets.search(function (response) {
            self.retweetsAnalyticsGraphDataGen(response);
        });
        //    console.log('bar chart',barChart);
    },
    retweetsAnalyticsGraphDataGen: function (responseObject) {
        var barChart = {};
        for (var arrayIndex = 0; arrayIndex < globalObject.namesHolderTwitter.length; arrayIndex++) {
            barChart['' + globalObject.namesHolderTwitter[arrayIndex] + ''] = [];
            //        var dataArray = 'dataArray'+arrayIndex ;
            //        dataArray =[];
        }
        //      console.log('response for retweets',responseObject.responses);
        if (responseObject) {
            var response = responseObject.responses;
            var self = this;
            var dataRetweetsAnalytics = [];

            for (var index = 0; index < response.length; index++) {
                arIndex = parseInt(index);
                var aggregation = response[index].aggregations.trendsRetweetsAnalytics.buckets;
                //            console.log('aggregation',aggregation);
                for (var firstIndex = 0; firstIndex < aggregation.length; firstIndex++) {
                    var milliSecondValue = aggregation[firstIndex].key;
                    var date = new Date(milliSecondValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month = monthAbbr[month];
                    var year = date.getFullYear();
                    barChart['' + globalObject.namesHolderTwitter[arIndex] + ''].push({
                        "dateInEpoch": milliSecondValue,
                        "date": day + ' ' + month + ' ' + year,
                        "value": aggregation[firstIndex].doc_count
                    })
                }
            }
        }
        console.log('bar chart', barChart);
        var combinedData = getCombinedData(barChart, globalObject.namesHolderTwitter.length);
        var balancedData = getBalancedData(combinedData, globalObject.namesHolderTwitter);
        var balancedArray = getBalancedArray(balancedData);
        var graph = generateGraphAggr(globalObject.namesHolderTwitter);
        var graphId = "retweetAggregate"
        var commentClass = ".dataContentChartHomeTrendsRA";
        generateAggregateGraph(balancedArray, graph, graphId,"",commentClass);
    },
    mentionsTimeLine: function (namesHolderTwitter) {
        var esClientTwitterReportHolderMentions = [];
        var index = 0;
        for (index; index < namesHolderTwitter.length; index++) {
            var esClientTwitterReportMention = 'esClientTwitterReportMention' + index;
            console.log(esClientTwitterReportMention);
            esClientTwitterReportMention = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', "", globalObject.parametersMentionsAggregate, "mentionsAnalytics");
            esClientTwitterReportMention.addQueryString('managementUser', namesHolderTwitter[index]);
            esClientTwitterReportMention.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
            esClientTwitterReportMention.addQueryString('managementSource', "mention");
            esClientTwitterReportHolderMentions.push(esClientTwitterReportMention);
        }

        var esClientMultiTwitterReportMentions = new ElasticsearchMultiQuery(esHost);
        esClientMultiTwitterReportMentions.applyESInstances(esClientTwitterReportHolderMentions);
        //    
        var self = this;
        $(".dataContentChartHomeTrendsMA").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        esClientMultiTwitterReportMentions.search(function (response) {
            self.mentionsAnalyticsGraphDataGen(response);
        });
    },


    //FUNCTION 48
    mentionsAnalyticsGraphDataGen: function (responseObject) {
        var barChart = {};
        for (var arrayIndex = 0; arrayIndex < globalObject.namesHolderTwitter.length; arrayIndex++) {
            barChart['' + globalObject.namesHolderTwitter[arrayIndex] + ''] = [];
        }
        //      console.log('response for mentions',responseObject.responses);
        if (responseObject) {
            var response = responseObject.responses;
            for (var index = 0; index < response.length; index++) {
                arIndex = parseInt(index);
                var aggregation = response[index].aggregations.mentionsAnalytics.buckets;
                //            console.log('aggregation',aggregation);
                for (var firstIndex = 0; firstIndex < aggregation.length; firstIndex++) {
                    var milliSecondValue = aggregation[firstIndex].key;
                    var date = new Date(milliSecondValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month = monthAbbr[month];
                    var year = date.getFullYear();
                    barChart['' + globalObject.namesHolderTwitter[arIndex] + ''].push({
                        "dateInEpoch": milliSecondValue,
                        "date": day + ' ' + month + ' ' + year,
                        "value": aggregation[firstIndex].doc_count
                    })
                }
            }
        }

        console.log('mentions bar chart', barChart);
        var combinedData = getCombinedData(barChart, globalObject.namesHolderTwitter.length);
        var balancedData = getBalancedData(combinedData, globalObject.namesHolderTwitter);
        var balancedArray = getBalancedArray(balancedData);
        var graph = generateGraphAggr(globalObject.namesHolderTwitter);
        var graphId = "mentionsAggregate";
        var commentId = ".dataContentChartHomeTrendsMA";
        generateAggregateGraph(balancedArray, graph, graphId,"",commentId);
    },
    favouritesTimeLine:function(namesHolderTwitter){

        var userName= namesHolderTwitter[0];
        
        var clientNameNew = 'esClientTwitterTweetNew' + globalObject.managementUserName + '';
        clientNameNew = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', "", {},"favouritesTimelineQuery");
        clientNameNew.addTermMatch("managementUser",userName);
        clientNameNew.addTermMatch("managementSource", "tweet");
        clientNameNew.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
      
            var self = this;
        $(".dataContentChartHomeTrendsFA").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');    
        clientNameNew.search(function(response){
           var data= self.favouritesGraphDataGen(response);

             self.favouritesGraph(data);
           
        });

    },

    favouritesGraphDataGen:function(response){
        console.log('favourite response............',response);
        var favouritesArray=[];
        var response=response.hits.hits;
          for(index in response){

             var dateValue=response[index]._source.created_at;
             console.log("dateValue........",dateValue);
              var date = new Date(dateValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month = monthAbbr[month];
                    var year = date.getFullYear();
                 date=day + ' ' + month + ' ' + year; 
                 favouritesArray.push({"date":date,"value":response[index]._source.favorite_count});
                  
          }

          return favouritesArray;

    },

    favouritesGraph:function (data) {
        console.log("favaourites graph....",data);
    /*if(data.length < 1) {
       $('.dataContentChartHomeMediaStats').html('No data available');
    }*/
    $('.dataContentChartHomeTrendsFA').empty();
    if(data.length < 1){
        $('.dataContentChartHomeTrendsFA').html('No data available');
    }
    var chart = AmCharts.makeChart("favouritesAggregate", {
        "type": "serial",
        "theme": "light",
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0.5,
            "gridAlpha": 0,
             "minimum":0,
             "integersOnly":true
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "fillColors":"#04A182",
            "valueField": "value"
        }],
        "chartCursor": {

            "cursorAlpha": 0

        },
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "labelRotation":-40

        },
        
        "export": {
          "enabled": true,
          "fileName" : "favourite count",
          "menu": [
            {
              "class": "export-main",
              "menu": [
                {
                  "label": "Download as image",
                  "menu": [
                    "PNG",
                    "JPG",
                    "SVG",
                    "PDF"
                  ]
                },
                {
                  "label": "Download data",
                  "menu": [
                    "CSV",
                    "XLSX",
                    "JSON"
                  ]
                },
                {
                  "label": "Print",
                  "menu": [
                    "PRINT"
                  ]
                }
              ]
            }
          ]
        }

    });
    globalObject.twitterBufferCharts.push(chart);

}
}