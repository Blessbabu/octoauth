//COMMON FUNCTION REQUIRED IN SMM SOCIAL MEDIA MANAGEMENT
    //composeSuccessFunction();
    //generateAggregateGraph();
    //getCombinedData();
    //getBalancedData();
    //getBalancedArray();
    //generateGraphAggr();
    function composeSuccessFunction() {
        $('.compose-message').show();
    };
    function generateAggregateGraph(data, graph, graphId,min,commentClass) { //amcharts graph generation
        console.log("aggregate graph", data);
        $('.dataContentChartHomeTrends').empty();
        $(commentClass).empty();
        if (data.length < 1) {
            $(commentClass).html('No data available');
        }
        var chart = AmCharts.makeChart(graphId, {
            "type": "serial",
            "theme": "light",
            "marginRight": 30,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.5,
                "gridAlpha": 0,
                "minimum":min||0
        }],
            "dataProvider": data,
            "legend": {
                "enabled": true,
                "markerType": "circle"

            },
            "colors": ['#04A182', '#cd8545', '#82D0C2'],
            //        "colors":['#5f99ec','#cd8545','#428498'],
            "graphs": graph,

            "chartCursor": {
                "cursorAlpha": 0
            },
            "categoryField": "date",
            "categoryAxis": {

                "axisColor": "#DADADA",
                "labelRotation": -40
            },
            "export": {
                "enabled": true,
                "menu": [
                    {
                        "class": "export-main",
                        "menu": [
                            {
                                "label": "Download as image",
                                "menu": [
						"PNG",
						"JPG",
						"SVG",
						"PDF"
					  ]
					},
                            {
                                "label": "Download data",
                                "menu": [
						"CSV",
						"XLSX",
						"JSON"
					  ]
					},
                            {
                                "label": "Print",
                                "menu": [
						"PRINT"
					  ]
					}
				  ]
				}
			  ]
            }
        });
            console.log("Graphid Srivathsa ............",graphId);
            if( graphId =="viewsFbAggregate"){
                globalObject.fbBufferCharts.push(chart); 
            }
            else if (graphId =="mentionsAggregate" || graphId =="followersAggregate" || graphId =="retweetAggregate"){
                globalObject.twitterBufferCharts.push(chart);
            }

     


    };
    function getCombinedData(data, count) { //data conversion
        var combinedData = {};
        for (var name in data) {
            data[name].map(function (userData, i) {
                var dt = userData.date;
                if (!combinedData[dt]) combinedData[dt] = {};
                combinedData[dt][name] = userData.value;
            });
        }
        console.log('combined data', combinedData)
        return combinedData;
    };
    function getBalancedData(data, nameList) { //data conversion
        for (var key in data) {
            nameList.map(function (name, i) {
                if (!data[key][name]) data[key][name] = 0;
            });
        }
        console.log('balanced data', data);
        return data;
    };
    function getBalancedArray(data) { //data conversion
        var sorted_keys = Object.keys(data);
        //        var sorted_keys = Object.keys(data).sort();
        var arrayData = [];
        sorted_keys.map(function (key, i) {
            var tmp = {};
            tmp.date = key;
            for (var name in data[key]) {
                tmp[name] = data[key][name];
            }
            arrayData.push(tmp);
        });
        console.log('array data', arrayData);
        return arrayData;
    };
    function generateGraphAggr(namesHolderTwitter) { //here some of the data needed for graphs are generated
        var graph = [];
        var colors = ['#04A182', '#cd8545', '#82D0C2'];
        for (var firstIndex = 0; firstIndex < namesHolderTwitter.length; firstIndex++) {
            graph.push({
                "fillAlphas": 0.9,
                "lineAlpha": 0.4,
                //  "labelText": "[[value]]",
                "title": namesHolderTwitter[firstIndex],
                // "type": "column",
                //            "fillColors": "#04A182",
                "fillColors": colors[firstIndex],
                "type": "column",
                "valueField": namesHolderTwitter[firstIndex]
            });
        }
        return graph;
    }