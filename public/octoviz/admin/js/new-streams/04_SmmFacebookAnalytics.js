//LIST OF METHODS
// 01 responseGenFacebookAggregate()        //create the clients for all the selected users
// 02 faceBookAggDataGen()                  //gather the responses,and extract and display the general statistics in a table format 
// 03 responseGenViewsTrendsAggregate()     //generating clients for page view trends information and doing the search
// 04 faceBookAggDataGenViews()             //data modification from the multi-response to be passed on to the amcharts graph function
// 05 generateAggregateGraph()              //amcharts graph generation
// 05 responseGenCountryTrendsAggregate()   //client creation and search for country wise data generation
// 06 responseGenFacebookInsights()         //insights facebook
// 07 responseGenFacebookInsightsElse()     //insight facebook
// 08 getCombinedData()                     //data conversion
// 09 getBalancedData()                     //data conversion
// 10 getBalancedArray()                    //data conversion
// 11 generateGraphAggr()                   //here some of the data needed for graphs are generated
// 12 countrySortAndTrimFunction()  

function SmmFacebookAnalyticsClasses() {}

SmmFacebookAnalyticsClasses.prototype = {
    responseGenFacebookAggregate: function () {
        var esClientFbAggGeneralHolderArray = [];
        var index = 0;
        for (index; index < globalObject.namesHolderFaceBook.length; index++) {
            var esClientFbGeneralAgg = 'esClientFbGeneralAgg' + index;
            console.log(esClientFbGeneralAgg);
            esClientFbGeneralAgg = new Elasticsearch(esHostAnalysisIndGeneralFb + '/fbmandata/post', "", globalObject.parametersGenAggAnalytics, "genIndFbSt");
            esClientFbGeneralAgg.addQueryString('managementUser.name', globalObject.ownerNamesOfAccounts[index]);
            esClientFbGeneralAgg.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
            esClientFbGeneralAgg.addQueryString('from.id', globalObject.pageIdsFbContentPost[index]);
            esClientFbAggGeneralHolderArray.push(esClientFbGeneralAgg);
        }

        var esClientMultiFaceBookGenral = new ElasticsearchMultiQuery(esHost);
        esClientMultiFaceBookGenral.applyESInstances(esClientFbAggGeneralHolderArray);
        //    
        var self = this;
        esClientMultiFaceBookGenral.search(function (response) {
            self.faceBookAggDataGen(response);
        });

    },
    faceBookAggDataGen: function (responseObject) {
        console.log('response object for fb aggs', responseObject);
        var dataHolder = [];
        if (responseObject) {
            var response = responseObject.responses;
            for (var responseIndex = 0; responseIndex < response.length; responseIndex++) {
                var totalPosts = response[responseIndex].hits.total;
                console.log('total posts', totalPosts)
                var aggregationLikes = response[responseIndex].aggregations.sumOfLikes;
                var aggregationShares = response[responseIndex].aggregations.sumOfShares;
                dataHolder.push({
                    name: globalObject.namesHolderFaceBook[responseIndex],
                    pageName: globalObject.pageNamesFbHolder[responseIndex],
                    totalPosts: totalPosts,
                    sumOfLikes: aggregationLikes.value || 0,
                    sumOfShares: aggregationShares.value || 0
                })
            }
        }
        $("#generalStatistics").html($("#generalStatisticsRender").render(dataHolder));
    },

    responseGenViewsTrendsAggregate: function () { //generating clients for page view trends information and doing the search
        var esClientViewsAggFbHolderArray = [];
        var index = 0;
        for (index; index < globalObject.namesHolderFaceBook.length; index++) {
            var esClientViewsAggFb = 'esClientViewsAggFb' + index;
            esClientViewsAggFb = new Elasticsearch(esHostAnalysisIndFb + '/fbmandata/analysis', "", globalObject.parametersViewsAggAnalyticsFb, "viewsTimeLine");
            esClientViewsAggFb.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
            esClientViewsAggFb.addQueryString('managementUser.name', globalObject.ownerNamesOfAccounts[index]);
            esClientViewsAggFb.addQueryString('page.id', globalObject.pageIdsFbContentPost[index]);
            esClientViewsAggFbHolderArray.push(esClientViewsAggFb);
        }

        var esClientMultiFaceBookViewsAgg = new ElasticsearchMultiQuery(esHost);
        esClientMultiFaceBookViewsAgg.applyESInstances(esClientViewsAggFbHolderArray);
        var self = this;
        esClientMultiFaceBookViewsAgg.search(function (response) {
            self.faceBookAggDataGenViews(response)
        });
    },
    faceBookAggDataGenViews: function (responseObject) { //data modification from the multi-response to be passed on to the amcharts graph function
        console.log('response object for facebook page views', responseObject);
        var barChart = {};
        for (var arrayIndex = 0; arrayIndex < globalObject.namesHolderFaceBook.length; arrayIndex++) {
            barChart['' + globalObject.namesHolderFaceBook[arrayIndex] + ''] = [];
        }

        response = responseObject.responses;
        var min=response[0].aggregations.min_views.value;
        for (var index = 0; index < response.length; index++) {
            var aggregation = response[index].aggregations.viewsTimeLine.buckets;
            for (var aggsIndex = 0; aggsIndex < aggregation.length; aggsIndex++) {
                var milliSecondValue = aggregation[aggsIndex].key;
                var date = new Date(milliSecondValue);
                var day = date.getDate();
                var month = date.getMonth();
                month = monthAbbr[month];
                var year = date.getFullYear();
                console.log('testing march............', globalObject.namesHolderFaceBook[index]);
                barChart['' + globalObject.namesHolderFaceBook[index] + ''].push({
                    "dateInEpoch": milliSecondValue,
                    "date": day + ' ' + month + ' ' + year,
                    "value": aggregation[aggsIndex].top_hits.hits.hits[0]._source.views
                })
            }
        }

        var combinedData = getCombinedData(barChart, globalObject.namesHolderFaceBook.length);
        var balancedData = getBalancedData(combinedData, globalObject.namesHolderFaceBook);
        var balancedArray = getBalancedArray(balancedData);
        var graph = generateGraphAggr(globalObject.namesHolderFaceBook);
        var graphId = "viewsFbAggregate";
        generateAggregateGraph(balancedArray, graph, graphId,min);

        //	console.log('views aggs facebook',barChart)
    },
    responseGenCountryTrendsAggregate: function () { //client creation and search for country wise data generation
        $('.fb-country-pie').empty();
        //	CHANGE ALL CLIENT NAMES
        var esClientCountriesAggFbHolderArray = [];
        var index = 0;
        for (index; index < globalObject.namesHolderFaceBook.length; index++) {
            var esClientCountriesAggFb = 'esClientViewsAggFb' + index;
            esClientCountriesAggFb = new Elasticsearch(esHostAnalysisIndFb + '/fbmandata/analysis', "", globalObject.parametersViewsAggAnalyticsFb, "viewsByCountry");
            esClientCountriesAggFb.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
            esClientCountriesAggFb.addQueryString('managementUser.name', globalObject.ownerNamesOfAccounts[index]);
            esClientCountriesAggFb.addQueryString('page.id', globalObject.pageIdsFbContentPost[index]);
            esClientCountriesAggFbHolderArray.push(esClientCountriesAggFb);
        }

        var esClientMultiFaceBookCountriesAgg = new ElasticsearchMultiQuery(esHost);
        esClientMultiFaceBookCountriesAgg.applyESInstances(esClientCountriesAggFbHolderArray);
        var self = this;
        esClientMultiFaceBookCountriesAgg.search(function (response) {
            self.faceBookAggDataGenCountries(response);
        });
    },
    responseGenFacebookInsights: function () { //insights facebook
        $('#click').html('Cant fetch data,permission denied');
        $('#likes').html('Cant fetch data,permission denied');
        $('#comment').html('Cant fetch data,permission denied');
        $('#share').html('Cant fetch data,permission denied');
        $('#reach').html('Cant fetch data,permission denied');
        $('#posts').html('Cant fetch data,permission denied');
    },
    responseGenFacebookInsightsElse: function () { //insight facebook
        $('#click').html('Please select user/users for inisights on click');
        $('#likes').html('Please select user/users for insights on likes');
        $('#comment').html('Please select user/users for insights on comment');
        $('#share').html('Please select user/users for insights on share');
        $('#reach').html('Please select user/users for insights on reach');
        $('#posts').html('Please select user/users for insights on reach');
    },
    
    faceBookAggDataGenCountries : function(responseObject){
	console.log('aggs countries response fb',responseObject);
	var response = responseObject.responses;
	for(var index=0;index<response.length;index++){
		var graphData = [];
		if(!isNull((response[index].aggregations.date_sorted.hits.hits[0]))){
		if(!isNull((response[index].aggregations.date_sorted.hits.hits[0]._source.countryStatistics))){
			var countryInfo = this.countrySortAndTrimFunction(response[index].aggregations.date_sorted.hits.hits[0]._source.countryStatistics);
		for(var key in countryInfo){
			graphData.push({
				key: key,
				value: countryInfo[key]
			})
		}
		var graphId ='fbSmmChartPie'+index;
        $('.dataContentFb-Country-Pie').empty();
		$('.fb-country-pie').append(this.htmlGenForPieChartGeneration(index));
        $('.dataContentViewTrendsPie'+index+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
		this.viewsCountrylineAffGraphFb(graphData,graphId,index);
		}
        else{
            $('.dataContentFb-Country-Pie').empty();
            $('.dataContentFb-Country-Pie').html('No data available');            
        }
	}
    else{
        $('.dataContentFb-Country-Pie').empty();
        $('.dataContentFb-Country-Pie').html('No data available');  
        }
	}
},

//FUNCTION 59
htmlGenForPieChartGeneration : function(index){
	var countryHtml = '<div class="col-lg-12 col-md-12 col-sm-12">'+
							'<div class="fb-smm-pie-global fb-smm-analytics-inner-'+globalObject.pageIdsFbContentPost[index]+'">'+
								'<h4 class="pie-headers-smm-fb">Page Name &nbsp <span class="pie-headers-smm-fb-page-name">'+globalObject.namesHolderFaceBook[index]+'</span></h4>'+
                                '<div class="dataContentViewTrendsPie'+index+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
								 '<div class="fbSmmChartPieGeneral" id="fbSmmChartPie'+index+'"></div>'+
							'</div>'+
					   '</div>' ;
    return countryHtml;
},
	
//FUNCTION 60
countrySortAndTrimFunction : function(obj){
	var tuples = [];

	for (var key in obj){
		tuples.push([key, obj[key]]);
	}
	
	tuples.sort(function(a, b) {
		a = a[1];
		b = b[1];
		return a > b ? -1 : (a < b ? 1 : 0);
	});
	
	var sortedObject = {};
	for (var i = 0; i < 5; i++) {
		var key = tuples[i][0];
		var value = tuples[i][1];
		console.log('key',key)
		console.log('value',value)
		sortedObject[key]=value;
		// do something with key and value
	}
	console.log('sorted object',sortedObject);
	return sortedObject;
},
	
//FUNCTION 61
viewsCountrylineAffGraphFb : function(data,graphId,index){
     
	if(data.length < 1) {
            $('.dataContentViewTrendsPie'+index+'').empty();
		}
        else{
		      $('.dataContentViewTrendsPie'+index+'').empty()
		}
        $("#mediaColumnTotal").empty(); 
	var chart = AmCharts.makeChart(graphId, {
        "type": "pie",
        "theme": "light",
        "autoMargins": true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        
        "dataProvider": data,
        "titleField":"key",
        "valueField": "value",
        "startDuration":0,
         "responsive": {
            "enabled":true
         },
         "legend":{
           "autoMargins":false,
           "marginLeft":20,
            "enabled": true,
            "spacing":20,
            "markerType":"circle",
           
            "horizontalGap" :20,
            
         },
          "labelsEnabled":false,
        //"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      
        "colorField": "color",
        "innerRadius": "60%",
       
             "export": {
                "enabled": true,
				"fileName":"sentiments chart fb",
				"menu": [ {
					"class": "export-main",
					"menu": [ 
						{
						  "label": "Download as image",
						  "menu": [ "PNG", "JPG", "SVG","PDF" ]
					    }, 
					   {
						  "label": "Download data",
						  "menu": [ "CSV", "XLSX","JSON" ]
					   },
						{
						 "label": "Print",
						 "menu" : ["PRINT"]
						}
					]
				  } ]
            	}
    });

  globalObject.fbBufferCharts.push(chart);

}
}