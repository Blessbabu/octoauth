function SocialMediaManagementClasses(){
	this.namesHolderTwitter = [];
	this.namesHolderFaceBook = [];
	this.userNameHold=null;
	this.userIndex =null;
	this.userType=null;
	this.userPageId=null;//DAY 2

	this.faceBookNamesArray = ["arunFb","hariFb","jagguFb"];// FOR TESTING WITH DUMMY VALUES
	this.twitterNamesArray = ["2arunpmohan","hariTwitter","jagguTwitter"];// FOR TESTING WITH DUMMY VALUES

	this.networkArrayFacebook = [];
	this.networkArrayTwitter =[];

	this.idsArrayFb=[];	//DAY 2
	this.idsArrayTwitter=[];	//DAY 2
	this.newArrayNetworks =[];
	this.userNamesArrayFbStreamsNew = [];	//DAY 2
	this.pageNamesFbStreamsNew = [];	//DAY 2
	this.pageIdsFbStreamsNew = [];	//DAY 2
	this.userNamesArrayTwitterStreams=[];	//DAY 2
	this.newArrayIds = [];	//DAY 2
	this.twitterAuthConfigStreams = null //DAY 2
	this.pageIdsFbContentPost = [];//DAY 2

	this.streamClientNew = new Elasticsearch(esHost); //DAY 2
	this.esClientContentPostNew = null;	//DAY 2
	this.esClientTwitterTweetNew=null;	//DAY 2
	this.esClientTwitterMessagesNew=null;	//DAY 2
	this.esClientTwitterTimelineNew=null;	//DAY 2
	this.esClientTwitterMentionsNew=null;	//DAY 2
	this.pageIdFbPickBuffer=null; //DAY 2
	this.esClientSmmStateGet = null;
	this.pageNamesOfUsersArrayFb = [];
	this.pageNamesOfUsersArrayTwitter =[];
	this.newArrayPageNames = [];	
	this.pageNamesFbHolder = [];
	this.pageNameFbPickBuffer = null;
	//this.newArray= _.union(faceBookNamesArray, twitterNamesArray);
	//console.log('merged array',newArray);

	this.parametersFollowersAggregate={
		interval : null
	}
	this.parametersRetweetsAggregate={
		interval : null
	}
	this.parametersMentionsAggregate={
		interval : null
	}
	this.parametersGenAggAnalytics = {
		interval : null
	}
	this.parametersViewsAggAnalyticsFb = {
		interval : null
	}
	this.ownerNamesOfAccounts = [];
	this.type = "column";
	this.managementUserName = null;

}

SocialMediaManagementClasses.prototype = {
	
//FUNCTION 01
emptyUsedArrayFunctions : function(){
	
	$('#stream').empty();
	$('#generalStatistics').empty();
	$('#viewsFbAggregate').empty();
	$('#mentionsAggregate').empty();
	$('#followersAggregate').empty();
	$('#retweetAggregate').empty();
	$('.fb-country-pie').empty();
	this.newArrayIds = [];
	this.newArrayPageNames = [];
	this.namesHolderTwitter = [];
	this.namesHolderFaceBook = [];
	this.pageIdsFbContentPost = [];
	this.pageNamesFbHolder = [];
	this.newArrayNetworks = [];


	this.networkArrayTwitter=[]
	this.idsArrayTwitter=[]
	this.pageNamesOfUsersArrayTwitter=[]


	this.networkArrayFacebook=[];
	this.idsArrayFb=[];
	this.pageNamesOfUsersArrayFb=[];

	this.pageNamesFbStreamsNew=[];
	this.pageIdsFbStreamsNew=[];
},
	
//FUNCTION 02
returnNetworkTwitter : function(name,index){
	globalObject.networkArrayTwitter.push("twitter");
	globalObject.idsArrayTwitter.push('blank');
	globalObject.pageNamesOfUsersArrayTwitter.push('blank')
},
	
//FUNCTION 03
returnNetworkFacebook : function(name,index){
	globalObject.networkArrayFacebook.push("facebook");
	globalObject.idsArrayFb.push(globalObject.pageIdsFbStreamsNew[index]);
	globalObject.pageNamesOfUsersArrayFb.push(globalObject.pageNamesFbStreamsNew[index]);
},

//FUNCTION 04
userListGeneration : function(newArray){
	var renderData = [];
	for(var index =0;index<newArray.length;index++){
		renderData.push({
			name: newArray[index],
			network: this.newArrayNetworks[index],
			ids:this.newArrayIds[index],
			pageName:this.newArrayPageNames[index],
			ownerName:this.ownerNamesOfAccounts[index]
	})
}
	 $(".userNamesList").html($("#userNamesRender").render(renderData));
},


	
//FUNCTION 07 
//INITITALISATION FUNCTION
initTabStreams : function(){
	this.getStateSmm();
	this.initOrResetStreamsNew();
	this.renderUserDataStreamsFb(function(){
    });
},

//FUNCTION 08
//INITIALISATION AND RESET OF ES CLIENTS USED
initOrResetStreamsNew : function(){
	 if (isNull(this.esClientContentPostNew)) {
        this.esClientContentPostNew = new Elasticsearch(esClientContentHost,"", {},"emptyQueryFunction")
    } else {
        this.esClientContentPostNew.reset();
    }
},

getStateSmm : function(){
	if(isNull(this.esClientSmmStateGet)){
		this.esClientSmmStateGet = new Elasticsearch(esSmmStateHostLocal,null,null)
	} else {
		this.esClientSmmStateGet.reset();
	}
	
	var self = this;
	this.esClientSmmStateGet.updateGetSmm(esSmmStateHostLocal,'smm-state-index','smm-state-type',user,function(response,error){
		console.log('success message',error); //for indexed users error=success, for non indexed users error=error
		console.log('success message',response);
		if(error!='error'){
			console.log('response in get function',response);
			if(!isNull(response._source.timeLineTwitter)){
				smmStateVariable['timeLineTwitter']= response._source.timeLineTwitter;
			}
			if(!isNull(response._source.mentionsTwitter)){
				smmStateVariable['mentionsTwitter']= response._source.mentionsTwitter;
			}
			if(!isNull(response._source.messagesTwitter)){
				smmStateVariable['messagesTwitter']= response._source.messagesTwitter;
			}
			if(!isNull(response._source.tweetsTwitter)){
				smmStateVariable['tweetsTwitter']= response._source.tweetsTwitter;
			}
			if(!isNull(response._source.timeLineFb)){
				smmStateVariable['timeLineFb']= response._source.timeLineFb;
			}
			
			
//			smmStateVariable = response;
			console.log('inside first call',smmStateVariable);
			self.savedStreamsData(smmStateVariable);
		}
		else{
//			alert('error captured');
			console.log('inside first call',smmStateVariable);
		}

});
},
	
savedStreamsData : function(smmStateVariable){
//console.log('captured response',response);
	var twitterTimeLineData = smmStateVariable.timeLineTwitter;
	var twitterMentionsData = smmStateVariable.mentionsTwitter;
	var twitterMessagesData = smmStateVariable.messagesTwitter;
	var twitterMyTweetsData = smmStateVariable.tweetsTwitter;
	var fbTimeLineData = smmStateVariable.timeLineFb;
	if(!isNull(twitterTimeLineData)){
		for(var index in smmStateVariable.timeLineTwitter){
			var param = {};
			param.id = "init";
			param.uName = index;
			$('.streams-buffer').append(this.timeLineHtmlGenBuffer(param.uName));
			this.timeLineRenderTwitter(smmStateVariable.timeLineTwitter[index],param);
		}
	}
	if(!isNull(twitterMentionsData)){
		for(var index in smmStateVariable.mentionsTwitter){
				var param = {};
				param.id = "init";
				param.uName = index;
				$('.streams-buffer').append(this.mentionsHtmlGenBuffer(param.uName));
				this.mentionsRenderSingleTwitter(smmStateVariable.mentionsTwitter[index],param);
			}
	}
	if(!isNull(twitterMessagesData)){
		for(var index in smmStateVariable.messagesTwitter){
				var param = {};
				param.id = "init";
				param.uName = index;
				$('.streams-buffer').append(this.userMessageHtmlGenBuffer(param.uName));
				this.userMessageRenderSingleTwitter(smmStateVariable.messagesTwitter[index],param);
			}
	}
	if(!isNull(twitterMyTweetsData)){
		for(var index in smmStateVariable.tweetsTwitter){
					var param = {};
					param.id = "init";
					param.uName = index;
					$('.streams-buffer').append(this.userTweetHtmlGenBuffer(param.uName));
					this.userTweetRenderSingleTwitter(smmStateVariable.tweetsTwitter[index],param);
			}
	
	}
	if(!isNull(fbTimeLineData)){
		for(var index in fbTimeLineData){
					var param = {};
					param.id = "init";
					if(fbTimeLineData[index].hits.hits.length >1){
						param.uName = fbTimeLineData[index].hits.hits[0]._source.Page.name;
						console.log('param uname fb',param.uName);
					}
					else{
						param.uName = "no data"
					}
					param.pageId = index;
					$('.streams-buffer').append(this.timeLineHtmlGenFaceBook(param.pageId,param.uName));
					console.log('fbTimeLineData',fbTimeLineData[index])	
					this.timeLineRenderSingleFaceBook(fbTimeLineData[index],param);
			}
	}
},
//FUNCTION 08
//CALL TO COLLECT DATA FOR REGISTERED FACEBOOK USERS
renderUserDataStreamsFb : function(callBack){
   var esRenderUserDataClientFb = new Elasticsearch(esClientDemoHostFb + '/management-fb/credential', function() {
            return {}
        }, {},"emptyQueryFunction");
    esRenderUserDataClientFb.addQueryString("userName", user.toLowerCase());
	var self = this;
    esRenderUserDataClientFb.search(function(response){
		self.userNamesGeneratorFbStreamsNew(response)
	});
},

//FUNCTION 09
//GENERATE FACEBOOK USER ARRAY + // CALL TO COLLECT DATA FOR REGISTERED TWITTER USERS
userNamesGeneratorFbStreamsNew : function(response){
	// console.log('response for fb user data',response);
	 var totalHits = response.hits.hits;
    this.userNamesArrayFbStreamsNew =[];
    this.pageNamesFbStreamsNew =[];
    this.pageIdsFbStreamsNew =[];
    var sampleArrayFb =[];
    // console.log('fb-auth response is',response);
//    // console.log('fb-auth check',response.hits.hits[0]._source.userInfo.name);
    
    for(var index=0;index<totalHits.length;index++){
        var pageArrayLength=totalHits[index]._source.data.length;
        var pageArrayHits=totalHits[index]._source.data;
            for(var innerIndex=0;innerIndex<pageArrayLength;innerIndex++){
				this.userNamesArrayFbStreamsNew.push(totalHits[index]._source.userInfo.name);
                this.pageNamesFbStreamsNew.push(pageArrayHits[innerIndex].name);   
                this.pageIdsFbStreamsNew.push(pageArrayHits[innerIndex].id);                   
                sampleArrayFb.push(totalHits[index]._source.userInfo.name);
            }
    }

    // console.log('userNamesArrayFbStreamsNew fb',this.userNamesArrayFbStreamsNew);
    // console.log('pageNamesFbStreamsNew fb',this.pageNamesFbStreamsNew);
    // console.log('pageIdsFbStreamsNew fb',this.pageIdsFbStreamsNew);
	
	var self = this;
	this.renderUserDataStreamsTwitter(function() {
			self.userNamesGeneratorTwitterStreamsNew(self.twitterAuthConfigStreams);
    })
},

//FUNCTION 10
//CALL TO COLLECT TWITTER USER DATA
renderUserDataStreamsTwitter : function(callBack){
	var self = this;
    this.streamClientNew.updateGet(user, function(response) {
        self.twitterAuthConfigStreams = response;
        // console.log('twitter auth config', self.twitterAuthConfigStreams);
        if (isNull(response._source)) {
            self.twitterAuthConfigStreams = {
                _source: null
            };
        }
        if (!isNull(callBack)) {
            callBack();
        }
    });
},

//FUNCTION 11
//GENERATE TWITTER USER ARRAY + COMBINING FACEBOOK USER ARRAY AND TWITTER USER ARRAY
userNamesGeneratorTwitterStreamsNew : function(response){
	// console.log('response for twitter user data',response)
	this.userNamesArrayTwitterStreams = [];
    var tweetMan = response._source.tweetMan;
    var count = 0;
	if(!isNull(tweetMan)){
    for (count; count < tweetMan.length; count++) {
        if (!isNull(tweetMan[count].userDetail)) {
            this.userNamesArrayTwitterStreams.push(tweetMan[count].userDetail.screen_name);
        } else {
            this.userNamesArrayTwitterStreams.push(tweetMan[count].screen_name)
        }
    }
	}
    // console.log('usernames are', this.userNamesArrayTwitterStreams);
	
	var newArray= _.union(this.pageNamesFbStreamsNew, this.userNamesArrayTwitterStreams); //MERGIN USERNAMES OF FACEBOOK AND TWITTER USERS
	// console.log('new array passed',newArray);
	for(var index=0;index<newArray.length;index++){
		if(this.userNamesArrayFbStreamsNew[index]){
			this.ownerNamesOfAccounts.push(this.userNamesArrayFbStreamsNew[index])
		}
		else{
			this.ownerNamesOfAccounts.push('blank')
		}
	}
	// console.log('owner names of accounts',this.ownerNamesOfAccounts);
	this.networkArraysGenerator();
	this.userListGeneration(newArray);
},
	
//FUNCTION 12
//CREATING SUB ARRAYS OF THE SAME LENGTH OF THE MERGED ARRAY (newArray in function userNamesGeneratorTwitterStreamsNew)
//1st sub array newArrayNetwork is to identify which network
//2nd sub array newArrayIds is to save the facebook ids and twitter ids(twitter ids currently not taken)
networkArraysGenerator:function(){
	// console.log('the passing fb array is',this.pageNamesFbStreamsNew);
	_.forEach(this.pageNamesFbStreamsNew,this.returnNetworkFacebook);
	_.forEach(this.userNamesArrayTwitterStreams,this.returnNetworkTwitter);
	this.newArrayNetworks =  $.merge(this.networkArrayFacebook, this.networkArrayTwitter);
	this.newArrayIds =  $.merge(this.idsArrayFb,this.idsArrayTwitter);
	this.newArrayPageNames =  $.merge(this.pageNamesOfUsersArrayFb,this.pageNamesOfUsersArrayTwitter);
	// console.log('new array ids',this.newArrayIds);
	// console.log('new array page names',this.newArrayPageNames);
	// console.log('new array network',this.newArrayNetworks);
},



//FUNCTION 14
contentPostTwitter: function(userIds,content,clickTime,scheduledTime){
	var postData = {
		text: content,
		toBeCreatedOn : scheduledTime,
		created_at:clickTime,
		isProcessed: false,
		ids:userIds
	};
	// console.log('postdata',postData);
	var indexTypeName = "management-twitter/input";
	if(userIds.length  < 1){
		// console.log('no users selected');
	}
	else{
		var self = this;
		this.esClientContentPostNew.updateContent(postData,"demo",indexTypeName,self.postContentResponseTwitter);
	}
},


//FUNCTION 15
contentPostFaceBook : function(userIds,content,clickTime,scheduledTime){
	// console.log('scheduled time format',scheduledTime);
	var postData = {
		text: content,
		toBeCreatedOn : scheduledTime,
		created_at:clickTime,
		isProcessed: false,
		ids:userIds
	};
	var indexTypeName = "management-fb/input/";
//	var indexTypeName = "flatsearch-01/";
	if(userIds.length  < 1){
		// console.log('no users selected');
	}
	else{
		var self = this;
		this.esClientContentPostNew.updateContent(postData,"demo",indexTypeName,self.postContentResponseFacebook);
	}
},

//FUNCTION 15a
composeSuccessFunction : function(){
	$('.compose-message').show();
},
	


//FUNCTION 16
postContentResponseTwitter:function(response){
 var error = null;
			if(error){
				 console.log('error');
			}
			else{
				globalObject.composeSuccessFunction();
				console.log('added data to new index');
			}
},


//FUNCTION 17
postContentResponseFacebook:function(response){
 var error = null;
			if(error){
				console.log('error');
			}
			else{
				globalObject.composeSuccessFunction();
				console.log('added data to new index');
			}
},


	
//FUNCTION 19
//TIME-LINE-TWITTER-RENDER
timeLineRenderTwitter : function(response,param){
		console.log('response from timeline',response);
		console.log('smm state variabe',smmStateVariable);

var totalHits = response.hits.hits.length;
	var hit = response.hits.hits
	console.log('totalhits',totalHits);
	var data =[];
	for (var buckets in hit) {
		console.log('buckets',buckets);
		data.push({
			name : hit[buckets]._source.user.name,
			favourites : hit[buckets]._source.favorite_count,
			retweets : hit[buckets]._source.retweet_count,
			followers : hit[buckets]._source.user.followers,
			image : hit[buckets]._source.user.profile_image_url,
			friends : hit[buckets]._source.user.friends,
			tweet : hit[buckets]._source.text,
			created_at : utcToDateFormatReverse(hit[buckets]._source.created_at)
		})
	}
	console.log('data',data);
	$('.dataContentTwitterTimelineStreams'+this.managementUserName+'').empty();
	if(param.id=='init'){
		$('.timeline-list-'+param.uName+'').html($('#time-line').render(data));
        if(totalHits==0){
            $('.stream-content-timeline-'+param.uName+'').html('no data available');
        }
	}
	if(param.id==null){
	   $('.timeline-list-'+this.managementUserName+'').html($('#time-line').render(data));
          if(totalHits==0){
            $('.stream-content-timeline-'+this.managementUserName+'').html('no data available');
        }
	}
	slimScrollCall("slimScrollTimeLine","500px");
},
	
//FUNCTION 20
//TIME-LINE-HTML-GENERATOR
timeLineHtmlGenBuffer : function(userNameDummy) {
    var timeLineHtml = '<div class="social-item tiwtter-streams time-line-tab-' + userNameDummy + ' item-intro clearfix">' +
        '<section class="social-stream">' +
        '<div class="stream-header">' +
        '<h4><i class="ion-social-twitter"></i>Time Line<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
        '<div class="stream-controls">' +
        '<a class="close time-line-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
        '</div>' +
        '</div>' +
         '<div class="stream-data">' +
//		'<div class="dataContentTwitterTimelineStreams'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content slimScrollTimeLine">' +
        '<div class="dataContentTwitterTimelineStreams'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content-timeline-'+userNameDummy+'" style="text-align: center"></div>'+
        '<ul class="vertical-list timeline-list-' + userNameDummy + '">' +

        '</ul>' +
        '</div>' +
         '</div>' +
        ' </section>' +
        '</div>';
    return timeLineHtml;
},
 

//FUNCTION 22
mentionsRenderSingleTwitter : function(mentionsResponse,param){
 var totalHits = mentionsResponse.hits.hits.length;
    var hit = mentionsResponse.hits.hits
    console.log('totalhits', totalHits);
    var data = [];
    for (var buckets in hit) {
        console.log('buckets', buckets);
        data.push({
            name: hit[buckets]._source.user.name,
            favourites: hit[buckets]._source.favorite_count,
            retweets: hit[buckets]._source.retweet_count,
            followers: hit[buckets]._source.user.followers,
            image: hit[buckets]._source.user.profile_image_url,
            friends: hit[buckets]._source.user.friends,
            tweet: hit[buckets]._source.text,
            created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
        })
    }
    console.log('mentions data', data);
	$('.dataContentTwitterMentions'+this.managementUserName+'').empty()
	if(param.id=='init'){
		$('.mentions-list-' + param.uName + '').html($('#mentions').render(data));
         if(totalHits==0){
            $('.stream-content-mentions-'+param.uName+'').html('no data available');
        }
	}
	if(param.id==null){
		$('.mentions-list-' + this.managementUserName + '').html($('#mentions').render(data));
         if(totalHits==0){
            $('.stream-content-mentions-'+this.managementUserName+'').html('no data available');
        }
	}
    slimScrollCall("slimScrollMentions", "500px");
},
	
//FUNCTION 23
mentionsHtmlGenBuffer : function(userNameDummy){
  var mentionsHtml = '<div class="social-item tiwtter-streams mentions-tab-' + userNameDummy + ' item-intro clearfix">' +
        '<section class="social-stream">' +
        '<div class="stream-header">' +
        '<h4><i class="ion-social-twitter"></i> Mentions<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
        '<div class="stream-controls">' +
        '<a class="close mentions-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
        '</div>' +
        '</div>' +
        '<div class="stream-data">' +
//	  	'<div class="dataContentTwitterMentions'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content slimScrollMentions">' +
        '<div class="dataContentTwitterMentions'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content-mentions-'+userNameDummy+'" style="text-align: center"></div>'+
        '<ul class="vertical-list mentions-list-' + userNameDummy + '">' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '</section>' +
        '</div>';
    return mentionsHtml;
},
	
//FUNCTION 25
userTweetRenderSingleTwitter: function(tweetsResponse,param){
    var totalHits = tweetsResponse.hits.hits.length;
    var hit = tweetsResponse.hits.hits
    console.log('totalhits', totalHits);
    var data = [];
    for (var buckets in hit) {
        console.log('buckets', buckets);
        data.push({
            name: hit[buckets]._source.user.name,
            favourites: hit[buckets]._source.favorite_count,
            retweets: hit[buckets]._source.retweet_count,
            followers: hit[buckets]._source.user.followers,
            image: hit[buckets]._source.user.profile_image_url,
            friends: hit[buckets]._source.user.friends,
            tweet: hit[buckets]._source.text,
            created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
        })
    }
	$('.dataContentTwitterTweets'+this.managementUserName+'').empty();
	if(param.id=='init'){
		$('.tweet-list-' + param.uName + '').html($('#tweets-render').render(data));
        if(totalHits==0){
            $('.stream-content-tweets-'+param.uName+'').html('no data available');
        }
	}
	if(param.id==null){
		$('.tweet-list-' + this.managementUserName + '').html($('#tweets-render').render(data));
         if(totalHits==0){
            $('.stream-content-tweets-'+this.managementUserName +'').html('no data available');
        }
	}
    slimScrollCall("slimScrollTweets", "500px");
},

//FUNCTION 26
userTweetHtmlGenBuffer : function(userNameDummy){
    var userTweetHtml = '<div class="social-item tiwtter-streams user-tweet-tab-' + userNameDummy + ' item-intro clearfix">' +
        ' <section class="social-stream">' +
        '<div class="stream-header">' +
        ' <h4><i class="ion-social-twitter"></i>Tweets<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
        '<div class="stream-controls">' +
        '<a class="close user-tweet-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
        '</div>' +
        '</div>' +
        '<div class="stream-data">' +
//		'<div class="dataContentTwitterTweets'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content slimScrollTweets">' +
        '<div class="dataContentTwitterTweets'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content-tweets-'+userNameDummy+'" style="text-align: center"></div>'+
        '<ul class="vertical-list tweet-list-' + userNameDummy + '">' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '</section>' +
        '</div>';
    return userTweetHtml;
},


	
//FUNCTION 28
userMessageRenderSingleTwitter : function(tweetsResponse,param){
    var totalHits = tweetsResponse.hits.hits.length;
    var hit = tweetsResponse.hits.hits
    console.log('totalhits', totalHits);
    var data = [];
    for (var buckets in hit) {
        console.log('buckets', buckets);
        data.push({
            name: hit[buckets]._source.sender_screen_name,
            //			favourites : hit[buckets]._source.favorite_count,
            //			retweets : hit[buckets]._source.retweet_count,
            //			followers : hit[buckets]._source.user.followers,
            image: hit[buckets]._source.sender.profile_image_url,
            //			friends : hit[buckets]._source.user.friends,
            message: hit[buckets]._source.text,
            created_at: utcToDateFormatReverse(hit[buckets]._source.created_at)
        })
    }
	
	$('.dataContentTwitterUserMessage'+globalObject.managementUserName+'').empty();
	if(param.id=='init'){
		$('.message-list-' + param.uName + '').html($('#message-render').render(data));
        if(totalHits==0){
            $('.stream-content-message-'+param.uName+'').html('no data available');
        }
	}
	if(param.id==null){
    	$('.message-list-' + this.managementUserName + '').html($('#message-render').render(data));
        if(totalHits==0){
            $('.stream-content-message-'+this.managementUserName+'').html('no data available');
        }
	}
    slimScrollCall("slimScrollMessage", "500px");

},

//FUNCTION 29
userMessageHtmlGenBuffer : function(userNameDummy){
	var userMessageHtml = '<div class="social-item tiwtter-streams user-message-tab-' + userNameDummy + ' item-intro clearfix">' +
			'<section class="social-stream">' +
			'<div class="stream-header">' +
			' <h4><i class="ion-social-twitter"></i>Messages<span class="twitter-name-display">' + userNameDummy + '</span></h4>' +
			'<div class="stream-controls">' +
			'<a class="close message-close" closeId="' + userNameDummy + '"><i class="ion-ios-close-empty"></i></a>' +
			'</div>' +
			'</div>' +
			'<div class="stream-data">' +
//		    '<div class="dataContentTwitterUserMessage'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
			'<div class="stream-content stream-content-message-'+userNameDummy+' slimScrollMessage">' +
            '<div class="dataContentTwitterUserMessage'+userNameDummy+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
            '<div class="stream-content-message-'+userNameDummy+'" style="text-align: center"></div>'+
			'<ul class="vertical-list message-list-' + userNameDummy + '">' +
			'</ul>' +
			'</div>' +
			'</div>' +
			'</section>' +
			'</div>';
		return userMessageHtml;
},
	


	
//FUNCTION 31
timeLineRenderSingleFaceBook : function(response,param){
    console.log('response for fb post timeline',response);
var totalHits = response.hits.hits.length;
	var hit = response.hits.hits
//	console.log('totalhits',totalHits);
	var data =[];
	for (var buckets in hit) {
		console.log('buckets',buckets);
		data.push({
			text : hit[buckets]._source.text,
			likesCount : hit[buckets]._source.likesCount,
			sharesCount : hit[buckets]._source.sharesCount,
			pageName : hit[buckets]._source.Page.name,
			pageId : hit[buckets]._source.Page.id,
			userName : hit[buckets]._source.managementUser.name,
			userId : hit[buckets]._source.managementUser.id
//            ,
//			created_at : utcToDateFormatReverse(hit[buckets]._source.created_at)
		})
	}
//	console.log('datafb',data);
//	console.log('dataContentFbStreams id',globalObject.pageIdFbPickBuffer);
	$('.dataContentFbStreams'+globalObject.pageIdFbPickBuffer+'').empty();
	if(param.id=='init'){
			$('.timeline-list-'+param.pageId+'').html($('#time-line-fb').render(data));
            if(totalHits==0){
                $('.stream-content-timelineFb-'+param.pageId+'').html('no data available');
            }
	}
	if(param.id==null){
		$('.timeline-list-'+this.pageIdFbPickBuffer+'').html($('#time-line-fb').render(data));
         if(totalHits==0){
            $('.stream-content-timelineFb-'+this.pageIdFbPickBuffer+'').html('no data available');
        }
	}
	
	slimScrollCall("slimScrollTimeLine","500px");
},

timeLineHtmlGenFaceBook : function(pageIdFbPick,pageNameFb) {
	if(!isNull(pageNameFb)){
		var pageName = pageNameFb
	}
	else{
		var pageName = globalObject.pageNameFbPickBuffer 
	}
	
    var timeLineHtml = '<div class="social-item tiwtter-streams time-line-tab-' + pageIdFbPick + ' item-intro clearfix">' +
        '<section class="social-stream">' +
        '<div class="stream-header">' +
        '<h4><i class="ion-social-facebook"></i>Time Line<span class="twitter-name-display">' +pageName + '</span></h4>' +
        '<div class="stream-controls">' +
        '<a class="close time-line-close-fb" closeId="' + pageIdFbPick + '"><i class="ion-ios-close-empty"></i></a>' +
        '</div>' +
        '</div>' +
         '<div class="stream-data">' +
        '<div class="stream-content slimScrollTimeLine">' +
		'<div class="dataContentFbStreams'+pageIdFbPick+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
        '<div class="stream-content-timelineFb-'+pageIdFbPick+'" style="text-align: center"></div>'+
        '<ul class="vertical-list timeline-list-' + pageIdFbPick + '">' +
        '</ul>' +
        '</div>' +
         '</div>' +
        ' </section>' +
        '</div>';
	

    return timeLineHtml;
},
	
//DAY 3

	
//FUNCTION 41
followersTimeLine : function(namesHolderTwitter){
	console.log('in followers timeline',globalObject.namesHolderTwitter)
    var esClientTwitterReportHolderFollowers = [];
    var index =0;
    for(index;index<this.namesHolderTwitter.length;index++){
        var esClientTwitterReport = 'esClientTwitterReport'+index ;
        console.log(esClientTwitterReport);
		esClientTwitterReport=new Elasticsearch(esHostTwitterStream +'/tweetman-new/analysis',"",this.parametersFollowersAggregate,"followersAnalytics");
		esClientTwitterReport.addQueryString('managementUser',this.namesHolderTwitter[index]);
		esClientTwitterReport.addDateRangeMatch("created_at", "now-" + "1M", null);
		esClientTwitterReportHolderFollowers.push(esClientTwitterReport);
    }
    
    var esClientMultiTwitterReport = new ElasticsearchMultiQuery(esHost);
		esClientMultiTwitterReport.applyESInstances(esClientTwitterReportHolderFollowers);
//    
	var self = this;
    esClientMultiTwitterReport.search(function(response){
		self.followersAnalyticsGraphDataGen(response);
	})
//    console.log('esClientTwitterReportHolderFollowers',esClientTwitterReportHolderFollowers);
},
	

//FUNCTION 42
followersAnalyticsGraphDataGen : function(responseObject){
    console.log('response object for followers',responseObject);
     var barChart ={};
    for(var arrayIndex=0;arrayIndex<this.namesHolderTwitter.length;arrayIndex++){
        barChart[''+this.namesHolderTwitter[arrayIndex]+'']=[];
    }
//      console.log('response for followers',responseObject.responses);
    if(responseObject){
            var response = responseObject.responses;
            for(var index=0;index<response.length;index++){
                arIndex = parseInt(index);
                var aggregation = response[index].aggregations.trendsFollowersAnalytics.buckets;
    //            console.log('aggregation',aggregation);
                for (var firstIndex=0;firstIndex<aggregation.length;firstIndex++){
                    var milliSecondValue = aggregation[firstIndex].key;
                    var date = new Date(milliSecondValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month=monthAbbr[month];
                    var year = date.getFullYear();
                    var followersCount = aggregation[firstIndex].followers.hits.hits[0]._source.followers_count;
                     barChart[''+this.namesHolderTwitter[arIndex]+''].push({
                        "dateInEpoch": milliSecondValue,
                        "date":day + ' ' + month + ' ' + year,
                        "value": followersCount
                     })
                }            
            }
    }

                console.log('bar chart',barChart);
     var combinedData = this.getCombinedData(barChart,this.namesHolderTwitter.length);
    var balancedData = this.getBalancedData(combinedData, this.namesHolderTwitter);
    var balancedArray = this.getBalancedArray(balancedData);
    var graph=this.generateGraphAggr(this.namesHolderTwitter);
    var graphId = "followersAggregate";
    this.generateAggregateGraph(balancedArray,graph,graphId);
},

//FUNCTION 43
retweetsTimeLine : function(namesHolderTwitter){
    var esClientTwitterReportHolderRetweets = [];
    var index =0;
    for(index;index<namesHolderTwitter.length;index++){
        var esClientTwitterReportRetweet = 'esClientTwitterReportRetweet'+index ;
        console.log(esClientTwitterReportRetweet);
		esClientTwitterReportRetweet=new Elasticsearch(esHostTwitterStream +'/tweetman-new/tweet',"",this.parametersRetweetsAggregate,"retweetsAnalytics");
		esClientTwitterReportRetweet.addQueryString('managementUser',namesHolderTwitter[index]);
		esClientTwitterReportRetweet.addQueryString('tweetType',"retweet");
		esClientTwitterReportHolderRetweets.push(esClientTwitterReportRetweet);
    }
    
     var esClientMultiTwitterReportRetweets = new ElasticsearchMultiQuery(esHost);
		esClientMultiTwitterReportRetweets.applyESInstances(esClientTwitterReportHolderRetweets);
    
	var self = this;
    esClientMultiTwitterReportRetweets.search(function(response){
		self.retweetsAnalyticsGraphDataGen(response);
	});
//    console.log('bar chart',barChart);
},
	
//FUNCTION 44
retweetsAnalyticsGraphDataGen : function(responseObject){
     var barChart ={};
    for(var arrayIndex=0;arrayIndex<this.namesHolderTwitter.length;arrayIndex++){
        barChart[''+this.namesHolderTwitter[arrayIndex]+'']=[];
//        var dataArray = 'dataArray'+arrayIndex ;
//        dataArray =[];
    }
//      console.log('response for retweets',responseObject.responses);
        if(responseObject){
            var response = responseObject.responses;
            var self = this;
            var dataRetweetsAnalytics =[];

            for(var index=0;index<response.length;index++){
                arIndex = parseInt(index);
                var aggregation = response[index].aggregations.trendsRetweetsAnalytics.buckets;
    //            console.log('aggregation',aggregation);
                for (var firstIndex=0;firstIndex<aggregation.length;firstIndex++){
                    var milliSecondValue = aggregation[firstIndex].key;
                    var date = new Date(milliSecondValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month=monthAbbr[month];
                    var year = date.getFullYear();
                     barChart[''+this.namesHolderTwitter[arIndex]+''].push({
                        "dateInEpoch": milliSecondValue,
                        "date":day + ' ' + month + ' ' + year,
                        "value": aggregation[firstIndex].doc_count
                     })
                }            
            }
        }
                console.log('bar chart',barChart);
     var combinedData = this.getCombinedData(barChart,this.namesHolderTwitter.length);
    var balancedData = this.getBalancedData(combinedData, this.namesHolderTwitter);
    var balancedArray = this.getBalancedArray(balancedData);
    var graph=this.generateGraphAggr(this.namesHolderTwitter);
    var graphId = "retweetAggregate"
     this.generateAggregateGraph(balancedArray,graph,graphId);
},

//FUNCTION 45
generateGraphAggr:function(namesHolderTwitter){
  var graph=[];
var colors = ['#04A182','#cd8545','#82D0C2'];
for (var firstIndex=0;firstIndex<namesHolderTwitter.length;firstIndex++){
    graph.push({
            "fillAlphas": 0.9,
            "lineAlpha": 0.4,
          //  "labelText": "[[value]]",
            "title":namesHolderTwitter[firstIndex],
           // "type": "column",
//            "fillColors": "#04A182",
            "fillColors":colors[firstIndex],
              "type": "column",
            "valueField":namesHolderTwitter[firstIndex]
        });
}
return graph;
},
	
//FUNCTION 46
generateAggregateGraph:function(data,graph,graphId) {
    console.log("aggregate graph",data);
    $('.dataContentChartHomeTrends').empty();
    if(data.length < 1){
        $('.dataContentChartHomeTrends').html('No data available');
    }
    var chart = AmCharts.makeChart(graphId, {
        "type": "serial",
        "theme": "light",
        "marginRight": 30,
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.5,
            "gridAlpha": 0
        }],
        "dataProvider": data,
        "legend":{
         "enabled":true,
         "markerType":"circle"

        },
        "colors":['#04A182','#cd8545','#82D0C2'],
//        "colors":['#5f99ec','#cd8545','#428498'],
        "graphs":graph,

        "chartCursor": {
            "cursorAlpha": 0
        },
        "categoryField": "date",
        "categoryAxis": {

            "axisColor": "#DADADA" ,
             "labelRotation":-40         
        },
        "export": {
			  "enabled": true,
			  "libs": {
				"path": "http://amcharts.com/lib/3/plugins/export/libs/"
			  },
			  "menu": [
				{
				  "class": "export-main",
				  "menu": [
					{
					  "label": "Download as image",
					  "menu": [
						"PNG",
						"JPG",
						"SVG",
						"PDF"
					  ]
					},
					{
					  "label": "Download data",
					  "menu": [
						"CSV",
						"XLSX",
						"JSON"
					  ]
					},
					{
					  "label": "Print",
					  "menu": [
						"PRINT"
					  ]
					}
				  ]
				}
			  ]
			}
    });
    
},


//FUNCTION 47
mentionsTimeLine : function(namesHolderTwitter){
    var esClientTwitterReportHolderMentions = [];
    var index =0;
    for(index;index<namesHolderTwitter.length;index++){
        var esClientTwitterReportMention = 'esClientTwitterReportMention'+index ;
        console.log(esClientTwitterReportMention);
		esClientTwitterReportMention=new Elasticsearch(esHostTwitterStream +'/tweetman-new/tweet',"",this.parametersMentionsAggregate,"mentionsAnalytics");
		esClientTwitterReportMention.addQueryString('managementUser',namesHolderTwitter[index]);
		esClientTwitterReportMention.addQueryString('managementSource',"mention");
		esClientTwitterReportHolderMentions.push(esClientTwitterReportMention);
    }
    
     var esClientMultiTwitterReportMentions = new ElasticsearchMultiQuery(esHost);
		esClientMultiTwitterReportMentions .applyESInstances(esClientTwitterReportHolderMentions);
//    
	var self = this;
    esClientMultiTwitterReportMentions.search(function(response){
		self.mentionsAnalyticsGraphDataGen(response);
	});
},
	

//FUNCTION 48
mentionsAnalyticsGraphDataGen: function(responseObject){
     var barChart ={};
    for(var arrayIndex=0;arrayIndex<this.namesHolderTwitter.length;arrayIndex++){
        barChart[''+this.namesHolderTwitter[arrayIndex]+'']=[];
    }
//      console.log('response for mentions',responseObject.responses);
        if(responseObject){ 
            var response = responseObject.responses;        
            for(var index=0;index<response.length;index++){
                arIndex = parseInt(index);
                var aggregation = response[index].aggregations.mentionsAnalytics.buckets;
    //            console.log('aggregation',aggregation);
                for (var firstIndex=0;firstIndex<aggregation.length;firstIndex++){
                    var milliSecondValue = aggregation[firstIndex].key;
                    var date = new Date(milliSecondValue);
                    var day = date.getDate();
                    var month = date.getMonth();
                    month=monthAbbr[month];
                    var year = date.getFullYear();
                     barChart[''+this.namesHolderTwitter[arIndex]+''].push({
                        "dateInEpoch": milliSecondValue,
                        "date":day + ' ' + month + ' ' + year,
                        "value": aggregation[firstIndex].doc_count
                     })
                }            
            }
        }

                console.log('mentions bar chart',barChart);
     var combinedData = this.getCombinedData(barChart,this.namesHolderTwitter.length);
    var balancedData = this.getBalancedData(combinedData, this.namesHolderTwitter);
    var balancedArray = this.getBalancedArray(balancedData);
    var graph=this.generateGraphAggr(this.namesHolderTwitter);
    var graphId = "mentionsAggregate"
     this.generateAggregateGraph(balancedArray,graph,graphId);
},
	

//FUNCTION 49
getCombinedData : function(data, count){
        var combinedData = {};
        for(var name in data){
            data[name].map(function(userData, i){
                var dt = userData.date;
                if( !combinedData[dt] ) combinedData[dt]  = {};
                combinedData[dt][name] = userData.value;
            });
        }
	console.log('combined data',combinedData)
        return combinedData;
    },

//FUNCTION 50
getBalancedData : function(data, nameList){
        for(var key in data){
            nameList.map(function(name, i){
                if(!data[key][name]) data[key][name] = 0;
            });
        }
	console.log('balanced data',data);
        return data;
    },

//FUNCTION 51
getBalancedArray : function(data){
        var sorted_keys = Object.keys(data);
//        var sorted_keys = Object.keys(data).sort();
        var arrayData = [];
        sorted_keys.map(function(key, i){
            var tmp = {};
            tmp.date = key;
            for(var name in data[key]){
                tmp[name] = data[key][name];
            }
            arrayData.push(tmp);
        });
        console.log('array data',arrayData);
        return arrayData;
    },

//FACEBOOK AGGREGATE

	
//FUNCTION 53
responseGenFacebookAggregate : function(){
	var esClientFbAggGeneralHolderArray = [];
    var index =0;
    for(index;index<this.namesHolderFaceBook.length;index++){
        var esClientFbGeneralAgg = 'esClientFbGeneralAgg'+index ;
        console.log(esClientFbGeneralAgg);
		esClientFbGeneralAgg=new Elasticsearch(esHostAnalysisIndGeneralFb +'/fbmandata/post',"",this.parametersGenAggAnalytics,"genIndFbSt");
		esClientFbGeneralAgg.addQueryString('managementUser.name',this.ownerNamesOfAccounts[index]);
		esClientFbGeneralAgg.addQueryString('from.id',this.pageIdsFbContentPost[index]);
		esClientFbAggGeneralHolderArray.push(esClientFbGeneralAgg);
    }
    
     var esClientMultiFaceBookGenral = new ElasticsearchMultiQuery(esHost);
		esClientMultiFaceBookGenral.applyESInstances(esClientFbAggGeneralHolderArray);
//    
	var self = this;
    esClientMultiFaceBookGenral.search(function(response){
		self.faceBookAggDataGen(response);
	});

},

//FUNCTION 54
faceBookAggDataGen : function(responseObject){
	console.log('response object for fb aggs',responseObject);
	var dataHolder =[];
    if(responseObject){
        var response = responseObject.responses;
        for(var responseIndex=0;responseIndex<response.length;responseIndex++){
            var totalPosts = response[responseIndex].hits.total;
            console.log('total posts',totalPosts)
            var aggregationLikes = response[responseIndex].aggregations.sumOfLikes;
            var aggregationShares = response[responseIndex].aggregations.sumOfShares;
                    dataHolder.push({
                        name: this.namesHolderFaceBook[responseIndex],
                        pageName:this.pageNamesFbHolder[responseIndex],
                        totalPosts: totalPosts,
                        sumOfLikes: aggregationLikes.value || 0,
                        sumOfShares:aggregationShares.value || 0
                    })
        }
    }
		$("#generalStatistics").html($("#generalStatisticsRender").render(dataHolder));
},
	
//FUNCTION 55
responseGenViewsTrendsAggregate : function(){
	var esClientViewsAggFbHolderArray = [];
	var index = 0;
	for(index;index<this.namesHolderFaceBook.length;index++){
		var esClientViewsAggFb = 'esClientViewsAggFb'+index;
		esClientViewsAggFb = new Elasticsearch(esHostAnalysisIndFb+'/fbmandata/analysis',"",this.parametersViewsAggAnalyticsFb,"viewsTimeLine");
		esClientViewsAggFb.addQueryString('managementUser.name',this.ownerNamesOfAccounts[index]);
		esClientViewsAggFb.addQueryString('page.id',this.pageIdsFbContentPost[index]);
		esClientViewsAggFbHolderArray.push(esClientViewsAggFb);	
	}
	
	var esClientMultiFaceBookViewsAgg = new ElasticsearchMultiQuery(esHost);
		esClientMultiFaceBookViewsAgg.applyESInstances(esClientViewsAggFbHolderArray);
		var self = this;
		esClientMultiFaceBookViewsAgg.search(function(response){
			self.faceBookAggDataGenViews(response)
		});
},
	

//FUNCTION 56
faceBookAggDataGenViews : function(responseObject){
	console.log('response object for facebook page views',responseObject);
	var barChart = {};
	for(var arrayIndex=0;arrayIndex<this.namesHolderFaceBook.length;arrayIndex++){
        barChart[''+this.namesHolderFaceBook[arrayIndex]+'']=[];
    }
	
	response= responseObject.responses;
	
		for(var index=0;index<response.length;index++){
				var aggregation = response[index].aggregations.viewsTimeLine.buckets;
				for(var aggsIndex=0;aggsIndex<aggregation.length;aggsIndex++){
					 var milliSecondValue = aggregation[aggsIndex].key;
					 var date = new Date(milliSecondValue);
					 var day = date.getDate();
					 var month = date.getMonth();
					 month=monthAbbr[month];
					 var year = date.getFullYear();
					 console.log('testing march............',this.namesHolderFaceBook[index]);
					 barChart[''+this.namesHolderFaceBook[index]+''].push({
						"dateInEpoch": milliSecondValue,
						"date":day + ' ' + month + ' ' + year,
						"value": aggregation[aggsIndex].top_hits.hits.hits[0]._source.views
					 })
			}
		}
	
	var combinedData = this.getCombinedData(barChart,this.namesHolderFaceBook.length);
    var balancedData = this.getBalancedData(combinedData, this.namesHolderFaceBook);
    var balancedArray = this.getBalancedArray(balancedData);
    var graph=this.generateGraphAggr(this.namesHolderFaceBook);
    var graphId = "viewsFbAggregate";
     this.generateAggregateGraph(balancedArray,graph,graphId);

//	console.log('views aggs facebook',barChart)
},

//FUNCTION 57
responseGenCountryTrendsAggregate : function(){
	$('.fb-country-pie').empty();
//	CHANGE ALL CLIENT NAMES
	var esClientCountriesAggFbHolderArray = [];
	var index = 0;
	for(index;index<this.namesHolderFaceBook.length;index++){
		var esClientCountriesAggFb = 'esClientViewsAggFb'+index;
		esClientCountriesAggFb = new Elasticsearch(esHostAnalysisIndFb+'/fbmandata/analysis',"",this.parametersViewsAggAnalyticsFb,"viewsByCountry");
		esClientCountriesAggFb.addQueryString('managementUser.name',this.ownerNamesOfAccounts[index]);
		esClientCountriesAggFb.addQueryString('page.id',this.pageIdsFbContentPost[index]);
		esClientCountriesAggFbHolderArray.push(esClientCountriesAggFb);	
	}
	
	var esClientMultiFaceBookCountriesAgg = new ElasticsearchMultiQuery(esHost);
		esClientMultiFaceBookCountriesAgg.applyESInstances(esClientCountriesAggFbHolderArray);
		var self = this;
		esClientMultiFaceBookCountriesAgg.search(function(response){
			self.faceBookAggDataGenCountries(response);
		});
},

//FUNCTION 58
faceBookAggDataGenCountries : function(responseObject){
	console.log('aggs countries response fb',responseObject);
	var response = responseObject.responses;
	for(var index=0;index<response.length;index++){
		var graphData = [];
		if(!isNull((response[index].aggregations.date_sorted.hits.hits[0]))){
		if(!isNull((response[index].aggregations.date_sorted.hits.hits[0]._source.countryStatistics))){
			var countryInfo = this.countrySortAndTrimFunction(response[index].aggregations.date_sorted.hits.hits[0]._source.countryStatistics);
		for(var key in countryInfo){
			graphData.push({
				key: key,
				value: countryInfo[key]
			})
		}
		var graphId ='fbSmmChartPie'+index;
		$('.fb-country-pie').append(this.htmlGenForPieChartGeneration(index));
        $('.dataContentViewTrendsPie'+index+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
		this.viewsCountrylineAffGraphFb(graphData,graphId,index);
		}
	}
	}
},


//FUNCTION 59
htmlGenForPieChartGeneration : function(index){
	var countryHtml = '<div class="col-lg-6 col-md-6 col-sm-12">'+
							'<div class="fb-smm-pie-global fb-smm-analytics-inner-'+this.pageIdsFbContentPost[index]+'">'+
								'<h4 class="pie-headers-smm-fb">Page Name &nbsp <span class="pie-headers-smm-fb-page-name">'+this.namesHolderFaceBook[index]+'</span></h4>'+
                                '<div class="dataContentViewTrendsPie'+index+'" style="position: absolute;left: 50%;top: 50%;width: 120px;margin-top: -18px;margin-left: -52px;text-align: center;z-index: 9;" ></div>'+
								 '<div class="fbSmmChartPieGeneral" id="fbSmmChartPie'+index+'"></div>'+
							'</div>'+
					   '</div>' ;
    return countryHtml;
},
	
//FUNCTION 60
countrySortAndTrimFunction : function(obj){
	var tuples = [];

	for (var key in obj){
		tuples.push([key, obj[key]]);
	}
	
	tuples.sort(function(a, b) {
		a = a[1];
		b = b[1];
		return a > b ? -1 : (a < b ? 1 : 0);
	});
	
	var sortedObject = {};
	for (var i = 0; i < 5; i++) {
		var key = tuples[i][0];
		var value = tuples[i][1];
		console.log('key',key)
		console.log('value',value)
		sortedObject[key]=value;
		// do something with key and value
	}
	console.log('sorted object',sortedObject);
	return sortedObject;
},
	
//FUNCTION 61
viewsCountrylineAffGraphFb : function(data,graphId,index){
     
	if(data.length < 1) {
            $('.dataContentViewTrendsPie'+index+'').empty();
		}
        else{
		      $('.dataContentViewTrendsPie'+index+'').empty()
		}
        $("#mediaColumnTotal").empty(); 
	var chart = AmCharts.makeChart(graphId, {
        "type": "pie",
        "theme": "light",
        "autoMargins": true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        
        "dataProvider": data,
        "titleField":"key",
        "valueField": "value",
        "startDuration":0,
         "responsive": {
            "enabled":true
         },
         "legend":{
           "autoMargins":false,
           "marginLeft":20,
            "enabled": true,
            "spacing":20,
            "markerType":"circle",
           
            "horizontalGap" :20,
            
         },
          "labelsEnabled":false,
        //"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      
        "colorField": "color",
        "innerRadius": "60%",
       
             "export": {
                "enabled": true,
				"fileName":"sentiments chart fb",
                "libs": {
                    "path": "http://amcharts.com/lib/3/plugins/export/libs/"
                },
				"menu": [ {
					"class": "export-main",
					"menu": [ 
						{
						  "label": "Download as image",
						  "menu": [ "PNG", "JPG", "SVG","PDF" ]
					    }, 
					   {
						  "label": "Download data",
						  "menu": [ "CSV", "XLSX","JSON" ]
					   },
						{
						 "label": "Print",
						 "menu" : ["PRINT"]
						}
					]
				  } ]
            	}
    });

},	

//FUNCTION 62
stateUpdateFunction : function(smmStateVariable){
//	alert('inside update')
		var smmStateClient = new Elasticsearch(esSmmStateHostLocal,null,null);
		smmStateClient.updateSmm(esSmmStateHostLocal,'smm-state-index','smm-state-type',user,smmStateVariable,function(response){
			console.log('response after update',response);
		})
},
//FUNCTION 63
stateReindexFunction : function(smmStateVariable){
//	alert('inside reindex')
	var smmStateClient = new Elasticsearch(esSmmStateHostLocal,null,null);
	smmStateClient.reindexSmm(esSmmStateHostLocal,'smm-state-index','smm-state-type',user,smmStateVariable,function(response){
			console.log('response after update',response);
		})
},
responseGenFacebookInsights : function(){
   $('#click').html('Cant fetch data,permission denied');
   $('#likes').html('Cant fetch data,permission denied');
   $('#comment').html('Cant fetch data,permission denied');
   $('#share').html('Cant fetch data,permission denied');
   $('#reach').html('Cant fetch data,permission denied');
   $('#posts').html('Cant fetch data,permission denied');
},
responseGenFacebookInsightsElse : function(){
    $('#click').html('Please select user/users for inisights on click');
   $('#likes').html('Please select user/users for insights on likes');
   $('#comment').html('Please select user/users for insights on comment');
   $('#share').html('Please select user/users for insights on share');
   $('#reach').html('Please select user/users for insights on reach');
   $('#posts').html('Please select user/users for insights on reach');
}


}


//FUNCTION 05 - BODY ON CLICK FUNCTION -01
//USER LIST POPULATION
$('body').on('click','.individual-users',function(){

	 globalObject.userNameHold = $(this).attr('user-name');
	 globalObject.userIndex = $(this).attr('user-index');
	 globalObject.userType = $(this).attr('user-type');
	 globalObject.userPageId = $(this).attr('page-id');
	 var userPageName = $(this).attr('user-pageName')
//pageNamesFbHolder
	if($(this).hasClass("current")){
		$(this).removeClass('current');
		var indexValTwitter = globalObject.namesHolderTwitter.indexOf(globalObject.userNameHold);
        var indexValFb = globalObject.namesHolderFaceBook.indexOf(globalObject.userNameHold);
		var indexValFbPostId = globalObject.pageIdsFbContentPost.indexOf(globalObject.userPageId);
		var indexValFbPageName = globalObject.pageNamesFbHolder.indexOf(userPageName);
        if (indexValTwitter > -1) {
            globalObject.namesHolderTwitter.splice(indexValTwitter, 1);
        }
        if(indexValFb > -1){
            globalObject.namesHolderFaceBook.splice(indexValFb,1);
            globalObject.pageIdsFbContentPost.splice(indexValFbPostId,1);
            globalObject.pageNamesFbHolder.splice(indexValFbPageName,1);
        }
		
	}else{
		$(this).addClass('current');
		if(globalObject.userType=="twitter"){
            globalObject.namesHolderTwitter.push(globalObject.userNameHold);
        }
        else{
            globalObject.namesHolderFaceBook.push(globalObject.userNameHold);
            globalObject.pageIdsFbContentPost.push(globalObject.userPageId);
            globalObject.pageNamesFbHolder.push(userPageName);
        }
	}
	
		console.log('twitter array',globalObject.namesHolderTwitter);
	console.log('fb array',globalObject.namesHolderFaceBook);
	console.log('fb ids array',globalObject.pageIdsFbContentPost);
	console.log('fb page names array',globalObject.pageNamesFbHolder);
    
    if(globalObject.namesHolderTwitter.length>0){
        $(".dataContentChartHomeTrends").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $('#mentionsAggregate').empty();
        $('#followersAggregate').empty();
        $('#retweetAggregate').empty();
        globalObject.followersTimeLine(globalObject.namesHolderTwitter);
        globalObject.retweetsTimeLine(globalObject.namesHolderTwitter);
        globalObject.mentionsTimeLine(globalObject.namesHolderTwitter);
    }
    else{
        $('#mentionsAggregate').html('No twitter users selected');
        $('#followersAggregate').html('No twitter users selected');
        $('#retweetAggregate').html('No twitter users selected');
    }
    
    if(globalObject.namesHolderFaceBook.length>0){
        $(".dataContentChartHomeTrends").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $('#viewsFbAggregate').empty();
        $('#generalStatistics').empty();
        $('.fb-country-pie').empty();
        globalObject.responseGenFacebookAggregate();
        globalObject.responseGenViewsTrendsAggregate();
        globalObject.responseGenCountryTrendsAggregate();
        globalObject.responseGenFacebookInsights();
    }
    
    else{
        $('.dataContentChartHomeTrends').empty();
        $('#viewsFbAggregate').html('No  facebook accounts selected');
        $('#generalStatistics').empty();
        $('.fb-country-pie').html('No  facebook accounts selected');
        globalObject.responseGenFacebookInsightsElse();
    }

    
});


	
//FUNCTION 06 - BODY ON CLICK FUNCTION -02
//TOGGLING IN USER NAMES FOR STREAMS DATA
$('body').on('click','.social-drop',function(){
		if($(this).children().hasClass('ion-ios-arrow-down')){
				$(this).children().removeClass('ion-ios-arrow-down');
                $(this).children().addClass('ion-ios-arrow-up');
		}
		else{
				$(this).children().removeClass('ion-ios-arrow-up');
                $(this).children().addClass('ion-ios-arrow-down');
		}
//                $(this).children().removeClass('ion-android-radio-button-off');
//                $(this).children().addClass('ion-android-radio-button-on');
                $(this).parent().siblings('.social-buttons').toggle('slide', {
                    duration: 1000,
                    direction: 'up'
                });
 });
	

//FUNCTION 13 - BODY ON CLICK FUNCTION -03
//POST CONTENT TO FACEBOOK AND TWITTER RESPECTIEVLY
$('body').on('click','.post-content-buffer',function(){
	
	
	var content = $('.message-content-buffer').val();
	if((globalObject.namesHolderTwitter.length>0)&&(content.length>140)){
		alert('twitter posts should be less than 140 characters')
	}
//	alert(namesHolderTwitter.length);
	
	var dateValue = $('.datepicker').val();
	var timeValue = $('.timepicker').val();
//var dateTimeFormat = 'Tue Sep 29 2015 16:22:51 GMT+0530 (IST)';
	var newArray =[];
	var dateValues=dateValue.split("-");
	var timeValues=timeValue.split(":");
    
    var userEnteredTime = dateValues[2]+'-'+dateValues[0]+'-'+dateValues[1]+'T'+timeValue+':'+'00Z';
    var jun = moment(userEnteredTime);
//    jun.tz('America/Los_Angeles').format('ha z'); 
    console.log('jun',jun);
    console.log('userEnteredTime',userEnteredTime);
	console.log('datevalue',dateValue);
	console.log('timevalue',timeValue);
    console.log('datevalues',dateValues);
	console.log('timevalues',timeValues);


//	var iDay=dateValues[0];
	var iMonth=dateValues[0]-1;
	var iDate=dateValues[1];
	var iYear=dateValues[2];
     
	var iHour = timeValues[0];
	var iMinute = timeValues[1];

	var newDateFormat1 = new Date(iYear,iMonth,iDate,iHour,iMinute);
	console.log('new date format1',newDateFormat1);
//	alert(reqTimeFormat);
	
	var pT = new Date();
    console.log('pT',pT);
//	alert(pT);
//	var utcPostTime = pT.toUTCString();
	var utcPostTime = (pT.toISOString()).slice(0,-5);
    console.log('utc post time',utcPostTime);
//	alert(utcPostTime.slice(0,-5));

	
	//STEP 1
		if(content.length >1 && timeValue.length > 1){
			var reqTimeFormat = newDateFormat1.toISOString().slice(0,-5);
				console.log('reqTimeformat',reqTimeFormat);
				console.log('twitter name holder length',globalObject.namesHolderTwitter.length)
				console.log('facebook name holder length',globalObject.pageIdsFbContentPost.length)
			if(globalObject.namesHolderTwitter.length > 0){
				globalObject.contentPostTwitter(globalObject.namesHolderTwitter,content,utcPostTime,reqTimeFormat);
			}
			if(globalObject.pageIdsFbContentPost.length > 0){
				globalObject.contentPostFaceBook(globalObject.pageIdsFbContentPost,content,utcPostTime,reqTimeFormat);
			}
			if(globalObject.pageIdsFbContentPost.length < 1 && globalObject.namesHolderTwitter.length < 1){
				alert('no users were selected');
			}
		}
		else if(content.length <1){
			alert('please write a content');
		}
		else if(timeValue.length < 1){
			alert('please select a time range')
		}
});

//FUNCTION 18 - BODY ON CLICK FUNCTION -04
//TIME-LINE TWITTER INITIATE CALL
$('body').on('click', '.timeline-new', function() { 	
	globalObject.managementUserName = $(this).attr('user-name');
    var clientNameNew = 'esClientTwitterTimelineNew' + globalObject.managementUserName + '';
    clientNameNew = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', function() {
        return {}
    }, {},"emptyQueryFunction");
	
	$('.streams-buffer').append(globalObject.timeLineHtmlGenBuffer(globalObject.managementUserName));
	$('.dataContentTwitterTimelineStreams'+globalObject.managementUserName+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
    clientNameNew.addTermMatch("managementUser", globalObject.managementUserName);
    clientNameNew.addTermMatch("managementSource", "timeline");
    clientNameNew.addDateRangeMatch("created_at", "now-" + "4M", null);
    clientNameNew.search(function(response){
		var name = globalObject.managementUserName;
		smmStateVariable['timeLineTwitter'][''+name+''] = response;
		globalObject.stateUpdateFunction(smmStateVariable);
		var param = {
			id : null
		}
		globalObject.timeLineRenderTwitter(response,param);
	});
});

	
//FUNCTION 21 - BODY ON CLICK FUNCTION -05
//MENTIONS TWITTER INITIATE CALL
$('body').on('click', '.mentions-new', function() {
	globalObject.managementUserName = $(this).attr('user-name');	
    var clientNameMentions = 'esClientTwitterMentionsNew' + globalObject.managementUserName + '';
    clientNameMentions = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', function() {
        return {}
    }, {},"emptyQueryFunction");
	
    clientNameMentions.addTermMatch("managementUser", globalObject.managementUserName);
    clientNameMentions.addTermMatch("managementSource", "mention");
    clientNameMentions.addDateRangeMatch("created_at", "now-" + "4M", null);
	$('.streams-buffer').append(globalObject.mentionsHtmlGenBuffer(globalObject.managementUserName));
	$('.dataContentTwitterMentions'+globalObject.managementUserName+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
	var self = this;
    clientNameMentions.search(function(response){
		var name = globalObject.managementUserName;
		smmStateVariable['mentionsTwitter'][''+name+''] = response;
		globalObject.stateUpdateFunction(smmStateVariable);
		var param = {
			id: null
		}
		globalObject.mentionsRenderSingleTwitter(response,param);
	});
});

//FUNCTION 24 - BODY ON CLICK FUNCTION -06
//MY TWEETS TWITTER INITIATE CALL
$('body').on('click', '.userTweet-new', function() {	
	globalObject.managementUserName = $(this).attr('user-name');
    var clientNameNew = 'esClientTwitterTweetNew' + globalObject.managementUserName + '';
    clientNameNew = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', "", {},"emptyQueryFunction");
    clientNameNew.addTermMatch("managementUser", globalObject.managementUserName);
    clientNameNew.addTermMatch("managementSource", "tweet");
    clientNameNew.addDateRangeMatch("created_at", "now-" + "4M", null);
	    $('.streams-buffer').append(globalObject.userTweetHtmlGenBuffer(globalObject.managementUserName));
		$('.dataContentTwitterTweets'+globalObject.managementUserName+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
	var self = this;
    clientNameNew.search(function(response){
		var name = globalObject.managementUserName;
		smmStateVariable['tweetsTwitter'][''+name+''] = response;
		globalObject.stateUpdateFunction(smmStateVariable);
		var param = {
			id: null
		}
		globalObject.userTweetRenderSingleTwitter(response,param);
	});
});
	
//FUNCTION 27 - BODY ON CLICK FUNCTION -06
//MESSAGES TWITTER INITIATE CALL
$('body').on('click', '.userMessage-new', function() {
	globalObject.managementUserName = $(this).attr('user-name');	
    var clientNameNew = 'esClientTwitterMessagesNew' + globalObject.managementUserName + '';
    clientNameNew = new Elasticsearch(esHostTwitterStream + '/tweetman-new/tweet', emptyQueryFunction, {},"emptyQueryFunction");
    clientNameNew.addTermMatch("managementUser", globalObject.managementUserName);
    clientNameNew.addTermMatch("managementSource", "message");
    clientNameNew.addDateRangeMatch("created_at", "now-" + "4M", null);
	$('.streams-buffer').append(globalObject.userMessageHtmlGenBuffer(globalObject.managementUserName));
	$('.dataContentTwitterUserMessage'+globalObject.managementUserName+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
	var self = this;
    clientNameNew.search(function(response){
		var name = globalObject.managementUserName;
		smmStateVariable['messagesTwitter'][''+name+''] = response;
		globalObject.stateUpdateFunction(smmStateVariable);
		var param = {
			id: null
		}
		globalObject.userMessageRenderSingleTwitter(response,param);
	});
});
	
//FUNCTION 30 - BODY ON CLICK FUNCTION -08
//TIMELINE FACEBOOK PAGE
$('body').on('click', '.timelineFbNew', function() {
	globalObject.pageIdFbPickBuffer = $(this).attr('page-id');	
	globalObject.pageNameFbPickBuffer = $(this).attr('user-pageName');	
	$('.streams-buffer').append(globalObject.timeLineHtmlGenFaceBook(globalObject.pageIdFbPickBuffer,globalObject.pageNameFbPickBuffer));
	$('.dataContentFbStreams'+globalObject.pageIdFbPickBuffer+'').html('<span class="dataLoader" style="display: inline-block;width: 100%;clear: both;"></span><span class="dataText">Loading..</span>');
    var clientName = 'esClientFbTimeline' + globalObject.pageIdFbPickBuffer + '';
    clientName = new Elasticsearch(esClientDemoHostFb + '/fbmandata/post', function() {
        return {}
    }, {},"emptyQueryFunction");
	var self = this;
    clientName.addTermMatch("Page.id", globalObject.pageIdFbPickBuffer);
//    clientName.addTermMatch("managementSource", "post");
//    clientName.addDateRangeMatch("created_at", "now-" + "24h", null);
    clientName.search(function(response){
		var pageId = globalObject.pageIdFbPickBuffer;
		var name = globalObject.pageNameFbPickBuffer;
		smmStateVariable['timeLineFb'][''+pageId+''] = response;
		globalObject.stateUpdateFunction(smmStateVariable);
		var param = {
			id: null
		}
		globalObject.timeLineRenderSingleFaceBook(response,param)
	});
	

	
});
	
//FUNCTION 33 - BODY ON CLICK FUNCTION -09
//REFRESH USERNAMES
$('body').on('click', '.refresh-button-buffer', function() {
    $('.message-content-buffer').val('');
	globalObject.emptyUsedArrayFunctions();
	globalObject.initTabStreams();
});



//REMOVING STREAM CONTENTS (ON CLICKING THE CLOSE BUTTON ON TOP OF TIMELINE,MENTION,MESSAGES AND MY TWEETS TAB)
//FUNCTION 34 - BODY ON CLICK FUNCTION -10
$('body').on('click', '.time-line-close', function() {
    var closeId = $(this).attr('closeId');
	var removeId = closeId ;
	delete smmStateVariable.timeLineTwitter[removeId];
	console.log('removed smm twitter timeline',smmStateVariable);
	globalObject.stateReindexFunction(smmStateVariable);
    $('.time-line-tab-' + closeId + '').hide();
});

$('body').on('click', '.time-line-close-fb', function() {
    var closeId = $(this).attr('closeId');
	delete smmStateVariable.timeLineFb[closeId];
	console.log('removed smm facebok timeline',smmStateVariable);
	globalObject.stateReindexFunction(smmStateVariable);
    $('.time-line-tab-' + closeId + '').hide();
});

//FUNCTION 35 - BODY ON CLICK FUNCTION -11
$('body').on('click', '.mentions-close', function() {
    var closeId = $(this).attr('closeId');
	delete smmStateVariable.mentionsTwitter[closeId];
	console.log('removed smm twitter mentions',smmStateVariable);
	globalObject.stateReindexFunction(smmStateVariable)
    $('.mentions-tab-' + closeId + '').hide();
});
//messagesTwitter
//tweetsTwitter
//FUNCTION 36 - BODY ON CLICK FUNCTION -12
$('body').on('click', '.user-tweet-close', function() {
    var closeId = $(this).attr('closeId');
	delete smmStateVariable.tweetsTwitter[closeId];
	console.log('removed smm twitter tweets',smmStateVariable);
	globalObject.stateReindexFunction(smmStateVariable);	
    $('.user-tweet-tab-' + closeId + '').hide();
});

//FUNCTION 37 - BODY ON CLICK FUNCTION -13
$('body').on('click', '.message-close', function() {
    var closeId = $(this).attr('closeId');
	delete smmStateVariable.messagesTwitter[closeId];
	console.log('removed smm twitter messages',smmStateVariable);
	globalObject.stateReindexFunction(smmStateVariable);
    $('.user-message-tab-' + closeId + '').hide();
});

//ADDITION OF USER TWITTER AND FACEBOOK ACCOUNTS
//FUNCTION 38 - BODY ON CLICK FUNCTION -14
$('body').on('click', '.authorizeTwitter', function() {
    window.open('http://octoviz.octobuz.com/auth/twitter?email=' + user + '', 'mywindow', 'width=600,height=400,left=400,top=200,scrollbars=1,status=0');
});

//FUNCTION 39 - BODY ON CLICK FUNCTION -15
$('body').on('click', '.authorizeFacebook', function() {
    window.open('http://octobuz.com/auth/facebook', 'mywindowFb', 'width=600,height=400,left=400,top=200,scrollbars=1,status=0');
});

//REPORT GENERATION - TWITTER AGGREGATE
//FUNCTION 40 - BODY ON CLICK FUNCTION -16
$('body').on('click','.generalReportAggregateBuffer',function(){

});
	

//FUNCTION 52 - BODY ON CLICK FUNCTION -17
$('body').on('click','.generalReportAggregateFbBuffer',function(){

});

$('body').on('click','.compose-section-new',function(){
	$('.compose-message').hide();
});
	

