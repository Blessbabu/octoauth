//LIST OF METHODS
//      contentPostTwitter()
//      postContentResponseTwitter()
//      contentPostFaceBook()
//      postContentResponseFacebook()

function SmmComposeClasses() {
    this.esClientContentPostNew = null;
}

SmmComposeClasses.prototype = {

    initOrResetCompose: function () {
        if (isNull(this.esClientContentPostNew)) {
            this.esClientContentPostNew = new Elasticsearch(esClientContentHost, "", {}, "emptyQueryFunction")
        } else {
            this.esClientContentPostNew.reset();
        }
    },

    contentPostTwitter: function (userIds, content, clickTime, scheduledTime) {
        var postData = {
            text: content,
            toBeCreatedOn: scheduledTime,
            created_at: clickTime,
            isProcessed: false,
            ids: userIds
        };
        // console.log('postdata',postData);
        var indexTypeName = "management-twitter/input";
        if (userIds.length < 1) {
            // console.log('no users selected');
        } else {
            var self = this;
            this.esClientContentPostNew.updateContent(postData, "demo", indexTypeName, self.postContentResponseTwitter);
        }
    },
    postContentResponseTwitter: function (response) {
        var error = null;
        if (error) {
            console.log('error');
        } else {
            composeSuccessFunction();
            console.log('added data to new index');
        }
    },
    contentPostFaceBook: function (userIds, content, clickTime, scheduledTime) {
        // console.log('scheduled time format',scheduledTime);
        var postData = {
            text: content,
            toBeCreatedOn: scheduledTime,
            created_at: clickTime,
            isProcessed: false,
            ids: userIds
        };
        var indexTypeName = "management-fb/input/";
        //	var indexTypeName = "flatsearch-01/";
        if (userIds.length < 1) {
            // console.log('no users selected');
        } else {
            var self = this;
            this.esClientContentPostNew.updateContent(postData, "demo", indexTypeName, self.postContentResponseFacebook);
        }
    },
    postContentResponseFacebook: function (response) {
        var error = null;
        if (error) {
            console.log('error');
        } else {
            composeSuccessFunction();
            console.log('added data to new index');
        }
    }
}
$('body').on('click', '.post-content-buffer', function () {
    var SmmComposeObject = new SmmComposeClasses();

    SmmComposeObject.initOrResetCompose();
    var content = $('.message-content-buffer').val();
    if ((globalObject.namesHolderTwitter.length > 0) && (content.length > 140)) {
        alert('twitter posts should be less than 140 characters')
    }
    //	alert(namesHolderTwitter.length);

    var dateValue = $('.datepicker').val();
    var timeValue = $('.timepicker').val();
    //var dateTimeFormat = 'Tue Sep 29 2015 16:22:51 GMT+0530 (IST)';
    var newArray = [];
    var dateValues = dateValue.split("-");
    var timeValues = timeValue.split(":");

    var userEnteredTime = dateValues[2] + '-' + dateValues[0] + '-' + dateValues[1] + 'T' + timeValue + ':' + '00Z';
    var jun = moment(userEnteredTime);
    //    jun.tz('America/Los_Angeles').format('ha z'); 
    console.log('jun', jun);
    console.log('userEnteredTime', userEnteredTime);
    console.log('datevalue', dateValue);
    console.log('timevalue', timeValue);
    console.log('datevalues', dateValues);
    console.log('timevalues', timeValues);


    //	var iDay=dateValues[0];
    var iMonth = dateValues[0] - 1;
    var iDate = dateValues[1];
    var iYear = dateValues[2];

    var iHour = timeValues[0];
    var iMinute = timeValues[1];

    var newDateFormat1 = new Date(iYear, iMonth, iDate, iHour, iMinute);
    console.log('new date format1', newDateFormat1);
    //	alert(reqTimeFormat);

    var pT = new Date();
    console.log('pT', pT);
    //	alert(pT);
    //	var utcPostTime = pT.toUTCString();
    var utcPostTime = (pT.toISOString()).slice(0, -5);
    console.log('utc post time', utcPostTime);
    //	alert(utcPostTime.slice(0,-5));

    //STEP 1
    if (content.length > 1 && timeValue.length > 1) {
        var reqTimeFormat = newDateFormat1.toISOString().slice(0, -5);
        console.log('reqTimeformat', reqTimeFormat);
        console.log('twitter name holder length', globalObject.namesHolderTwitter.length)
        console.log('facebook name holder length', globalObject.pageIdsFbContentPost.length)
        if (globalObject.namesHolderTwitter.length > 0) {
            SmmComposeObject.contentPostTwitter(globalObject.namesHolderTwitter, content, utcPostTime, reqTimeFormat);
        }
        if (globalObject.pageIdsFbContentPost.length > 0) {
            SmmComposeObject.contentPostFaceBook(globalObject.pageIdsFbContentPost, content, utcPostTime, reqTimeFormat);
        }
        if (globalObject.pageIdsFbContentPost.length < 1 && globalObject.namesHolderTwitter.length < 1) {
            alert('no users were selected');
        }
    } else if (content.length < 1) {
        alert('please write a content');
    } else if (timeValue.length < 1) {
        alert('please select a time range')
    }
});

$('body').on('click', '.compose-section-new', function () {
    $('.compose-message').hide();
});