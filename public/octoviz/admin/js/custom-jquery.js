var animationSideBar = function() {

    $('#side-menu').hide();
    setTimeout(
        function() {
            $('#side-menu').fadeIn(200);
        }, 100);
}
var openClose = false; //default close
var hoverFlag = false;
var buttonClick = function() {
    //		alert('getting called');
    if (openClose) {
        $('#wrapper').removeClass("mini-navbar");
        $('.nav-second-level').slideUp(300);
        openClose = false;
    } else {
        $('#wrapper').addClass("mini-navbar");
        openClose = true;
    }
};

var hoverFunctionIn = function() {
    $("#wrapper").addClass("mini-navbar");
    animationSideBar();
    if (!hoverFlag) {

        animationSideBar();
    } else {

    }


}

var hoverFunctionOut = function() {
    if (openClose) {

        $("#wrapper").addClass("mini-navbar");

    } else {
        $("#wrapper").removeClass("mini-navbar");

        $('.nav-second-level').slideUp(300);
        openClose = false;

    }

}
$('body').on('click', '.navbar-minimalize', function() {
    buttonClick();
    animationSideBar();
});


$('body').on('click', '.language-btn', function() {
    $('.add-tag-language').toggle();
});

$('.sidebar-collapse').hover(hoverFunctionIn, hoverFunctionOut);



//--- side navigation END --//


function fix_height() {
    var heightWithoutNavbar = $("body > #wrapper").height() - 61;
    $(".sidebar-collapse").css("min-height", heightWithoutNavbar + "px");

    var navbarHeigh = $('.sidebar-collapse').height();
    var wrapperHeigh = $('#page-wrapper').height();

    if (navbarHeigh > wrapperHeigh) {
        $('#page-wrapper').css("min-height", navbarHeigh + "px");
    }

    if (navbarHeigh < wrapperHeigh) {
        $('#page-wrapper').css("min-height", $(window).height() + "px");
    }

    if ($('body').hasClass('fixed-nav')) {
        $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
    }

}

//--- side navigation END --//
var hoverOverlay = function() {
    if (Modernizr.touch) {
        // show the close overlay button
        $(".close-overlay").removeClass("hidden");
        // handle the adding of hover class when clicked
        $(".infu-item").click(function(e) {
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".close-overlay").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".infu-item").hasClass("hover")) {
                $(this).closest(".infu-item").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".infu-item").mouseenter(function() {
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function() {
                $(this).removeClass("hover");
            });
    }

}


var streamsDropdown = function() {
    $('.messageTools, .profile-picker, .compose-footer, .profile-feild, .search-icon').hide();
    $('.messageBoxContainer').hover(
        function() {

            $(this).addClass('expanded');
            $(this).removeClass('collapsed');
            $('.send-to').hide();
            $('.messageTools, .profile-picker, .compose-footer, .profile-feild, .search-icon').show();
        },
        function() {
            $(this).removeClass('expanded');
            $(this).addClass('collapsed');
            $('.send-to').show();
            $('.messageTools, .profile-picker, .compose-footer, .profile-feild, .search-icon').hide();
            if ($('.profile-catogery').hasClass('open')) {
                $('.profile-catogery').removeClass('open')

            }
        }
    );
}


$('body').on('click', '.calendar-trigger', function() {

    $('.autoSchedule').toggle();

});


$(document).ready(function() {
    $('.nav-second-level').hide();
    $('#side-menu > li > a').click(function() {


        if ($(this).parent().hasClass('active')) {
            console.log("Applying slide");
            $(this).children('.nav-second-level').slideDown(100);
            $(this).next().slideToggle(100);
        } else {
            $(this).children('.nav-second-level').slideUp(100);
            $(this).next().slideToggle(100);
            $('#side-menu li').removeClass('active');
            $(this).parent().addClass('active');

        }
    });
    $('.user-name').html('<span>' + userName + '</span>');
    $(".time-select").click(function() {
        $(".time-container").slideToggle(300);
    });
    // serch area//

});



$(document).ready(function() {

    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small');
    } else {
        $('body').removeClass('body-small');
    }
    // Open close right sidebar

    $('.right-sidebar-toggle').click(function() {
        if ($(this).children().hasClass('fa-indent')) {
            $(this).children().removeClass('fa-indent');
            $(this).children().addClass('fa-dedent');
        } else {
            $(this).children().removeClass('fa-dedent');
            $(this).children().addClass('fa-indent');

        }
        $('#right-sidebar').toggleClass('sidebar-open');

    });

    // Initialize slimscroll for right sidebar
    $('.sidebar-container').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
    });
    // Full height of sidebar


    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function() {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    });



    // Move right sidebar top after scroll
    $(window).scroll(function() {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    $(window).bind("load resize scroll", function() {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    $("[data-toggle=popover]")
        .popover();

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    });
    //functions
    // Minimalize menu when screen is less than 768px
    $(window).bind("resize", function() {
        if ($(this).width() < 769) {
            $('body').addClass('body-small');
        } else {
            $('body').removeClass('body-small');
        }
    });
    // For demo purpose - animation css script


});
$('[data-toggle="dropdown"]').bootstrapDropdownHover({});