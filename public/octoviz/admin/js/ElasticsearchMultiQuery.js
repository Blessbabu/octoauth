
function ElasticsearchMultiQuery(esURL, esQueryFunction, esQueryParameters) {
    this.esURL = esURL;
    this.esInstances = [];
}
ElasticsearchMultiQuery.prototype = {
    applyESInstances: function (esInstances) {
        this.esInstances = esInstances;
    },
    reset: function(){
        for(var instanceIndex in this.esInstances){
            var instance = this.esInstances[instanceIndex];
            if(!isNull(instance)){
                instance.reset();
            }
        }
    },
    search: function (callBack) {
        var esBody =[];
       
        for (var esInstanceIndex in this.esInstances) {
            var esInstance = this.esInstances[esInstanceIndex];

            restParameters = {
            index:esInstance.getIndex(),
            type: esInstance.getType(),
            queryParams:esInstance.queryParameters,
            queryName:esInstance.queryName,
            liveParams:esInstance.esQueryParameters,
            socialMedia:esInstance.socialMedia,
            scoringField:esInstance.scoringField,
            fromDate:esInstance.fromDate,
            toDate:esInstance.toDate,
            prefix:esInstance.prefix,
            format:esInstance.format

           
            };
        esBody.push(restParameters);

        };
        
       
    $.ajax({
        type:"POST",
        contentType: 'application/json; charset=utf-8',
        url:esHostServer+"/_mse",
        data:JSON.stringify({data:esBody}),
        success:callBack,
         error : function(err ){ 
            console.log("err multi...",err);
            alertError(err.responseJSON.error);
            },
        dataType: "JSON"
    });
    }
}



