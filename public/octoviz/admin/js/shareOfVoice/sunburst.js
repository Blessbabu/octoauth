//var width = 100,
//    height = 100,
//    radius = Math.min(width, height) / 1.5;

//var x = d3.scale.linear()
//    .range([0, 2 * Math.PI]);
//
//var y = d3.scale.linear()
//    .range([0, radius]);

var color = d3.scale.category20c();
var svg = null;
var partition = null;
var arc = null;
var x = null;
var y = null;
var width = null;
var height = null;
var radius = null;
var drawSunBurst = function(root, error) {
	$('.dataContentSunburst').empty();
    var sampleHeight = $('.widget-home-dash-pie-sunburst').height();
    var svgHeight = $('#sunburst-graph').height();

    headHeight = $('.sunburst-header').height();
    $('.sunburst-body').css('height', sampleHeight - headHeight);
    width = $('#sunburst-graph').width();
    height = sampleHeight - headHeight;

    radius = Math.min(width, height) / 3;
    x = d3.scale.linear()
        .range([0, 2 * Math.PI]);

    y = d3.scale.linear()
        .range([0, radius]);

    //    alert(width);
    //    alert(height);
    svg = d3.select("#sunburst-graph").append("svg")
        //        .attr("width", width)
        //        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + height / 1.5 + "," + (height / 2) + ")");

    partition = d3.layout.partition()
        .value(function(d) {
            return d.size;
        });

    arc = d3.svg.arc()
        .startAngle(function(d) {
            return Math.max(0, Math.min(2 * Math.PI, x(d.x)));
        })
        .endAngle(function(d) {
            return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx)));
        })
        .innerRadius(function(d) {
            return Math.max(0, y(d.y));
        })
        .outerRadius(function(d) {
            return Math.max(0, y(d.y + d.dy));
        });


    var g = svg.selectAll("g")
        .data(partition.nodes(root))
        .enter().append("g");

    var path = g.append("path")
        .attr("d", arc)
        .style("fill", function(d) {
            return d.color || color((d.children ? d : d.parent).name);
        })
        .on("click", click);

    var text = g.append("text")
        .attr("transform", function(d) {
            return "rotate(" + computeTextRotation(d) + ")";
        })
        .attr("x", function(d) {
            return y(d.y);
        })
        .attr("dx", "6") // margin
        .attr("dy", ".35em") // vertical-align
        .text(function(d) {
            return d.name;
        });

    function click(d) {
        // fade out all text elements
        text.transition().attr("opacity", 0);

        path.transition()
            .duration(750)
            .attrTween("d", arcTween(d))
            .each("end", function(e, i) {
                // check if the animated element's data e lies within the visible angle span given in d
                if (e.x >= d.x && e.x < (d.x + d.dx)) {
                    // get a selection of the associated text element
                    var arcText = d3.select(this.parentNode).select("text");
                    // fade in the text element and recalculate positions
                    arcText.transition().duration(750)
                        .attr("opacity", 1)
                        .attr("transform", function() {
                            return "rotate(" + computeTextRotation(e) + ")"
                        })
                        .attr("x", function(d) {
                            return y(d.y);
                        });
                }
            });
    }
};

d3.select(self.frameElement).style("height", height + "px");

// Interpolate the scales!
function arcTween(d) {
    var xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
        yd = d3.interpolate(y.domain(), [d.y, 1]),
        yr = d3.interpolate(y.range(), [d.y ? 20 : 0, radius]);
    return function(d, i) {
        return i ? function(t) {
            return arc(d);
        } : function(t) {
            x.domain(xd(t));
            y.domain(yd(t)).range(yr(t));
            return arc(d);
        };
    };
}

function computeTextRotation(d) {
    return (x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
}