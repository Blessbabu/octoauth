var sunburstQueryFunction = function(parameters){

var queryBody = {
  "query": {
    "terms": {
      "ownedBy.id": [
        parameters.user1,
        parameters.user2
      ]
    }
  },
  "aggs": {
    "user2": {
      "filter": {
        "term": {
          "ownedBy.id": parameters.user2
        }
      },
      "aggs": {
        "sentiments": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        }
      }
    },
    "user1": {
      "filter": {
        "term": {
          "ownedBy.id": parameters.user1
        }
      },
      "aggs": {
        "sentiments": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        }
      }
    }
  }
};
return queryBody;

}