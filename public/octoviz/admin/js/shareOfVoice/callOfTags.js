var liveParametersTagCompare = {
	granularity: null,
	size: null,
	bubbleUserId: null
};

var createSingleQueryCall = function (queryName, duration) {
	globalDuration = duration;
	var esClientFb = null;
	var esClientInstagram = null;
	var esClientYt = null;
	var esTwitterClient = null;
	var esClientMedia = null;
	var esInstances = null;

	esClientFb = new Elasticsearch(esHost, fbQueryVoice, liveParametersTagCompare);
	esClientInstagram = new Elasticsearch(esHost + '/instagram', function () {
		return {}
	}, {});
	esClientYt = new Elasticsearch(esHost + '/youtube/video', function () {
		return {}
	}, {});
	esTwitterClient = new Elasticsearch(esHost, twitterQueryVoice, liveParametersTagCompare);
	esClientMedia = new Elasticsearch(esMediaHost + '/mediabuzz-new', mediaQueryVoice, liveParametersTagCompare);

	esClientFb.addIndexPattern("fbdata-",facebookTimeFormat);
    esClientFb.addIndexType("post");
	esClientFb.addTermMatch("Page.name.raw", getFacebookKeywords(queryName));
	esClientFb.addTermMatch("Group.name.raw", getFaceBookKeywordsGroups(queryName));
	esClientFb.addDateRangeMatch("created_at", "now-" + globalDuration, null);

	//CALL-02 INSTAGRAM
	esClientInstagram.addTermMatch("ownedBy.name", user);
	esClientInstagram.addTermMatch("ownedBy.query", queryName.toLowerCase());
	esClientInstagram.addDateRangeMatch("created_time", "now-" + globalDuration, null);

	//CALL-03 YOUTUBE

	esClientYt.addTermMatch("ownedBy.name", user);
	esClientYt.addTermMatch("ownedBy.query", queryName.toLowerCase());
	esClientYt.addDateRangeMatch("snippet.publishedAt", null, getFromString());


	//CALL-04 TWITTER
	esTwitterClient.addIndexPattern("twitter-", twitterTimeFormat);
	esTwitterClient.addTermMatch("ownedBy.id", user + "%%%" + queryName);
	esTwitterClient.addDateRangeMatch("created_at", getFromString(), null);
	//esTwitterClient.addTermMatch("ownedBy.name", user);
	//esTwitterClient.addTermMatch("ownedBy.query",queryName.toLowerCase());
	//esTwitterClient.addDateRangeMatch("created_at", "now-" + globalDuration, null);

	//CALL-05 MEDIA
	esClientMedia.addDateRangeMatch("actualTimeStamp", "now-" + globalDuration, null);
	var keywords = getKeywords(queryName);
	esClientMedia.addMultiMatch(["Title", "Content"], keywords.join(" "));



	esInstances = [esClientFb, esClientInstagram, esClientYt, esTwitterClient, esClientMedia];
	return esInstances;
}
