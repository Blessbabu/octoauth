function ShareOfVoice(){
this.liveParametersTagCompare ={
    granularity: null,
    size: null,
    bubbleUserId: null
};
this.isRendered = false;
this.selectedTagHolder=[];
this.sunburstParameters = {};
this.responseNames = ["facebook","instagram","youtube","twitter","media"];
this.colorMapSunburst = {
        positive:"#57c17b",
        negative:"#9e3533",
        neutral :"#96a6db",
        mixed: "#daa05d"
}
this.mapForClientsSov = {};

//  this.layoutTagCompare = {
//         /*
//         ** Array of objects or strings which represents the output
//         */
//     content: [
//         {
//             text: "Social Media Statistics Facebook",
//              color:"#3A907F"
//         }, {
//             image: "image_1",
//             // reference to the image mapping below
//             fit: [300,200]
//             // fits the image to those dimensions (A4)
//         },

//          {
//             text: "Social Media Statistics Instagram",
//              color:"#3A907F"
//         }, {
//             image: "image_2", // reference to the image mapping below
//             fit: [300,200]
//              // fits the image to those dimensions (A4)
//         },
//          {
//             text: "Social Media Statistics Youtube",
//              color:"#3A907F"
//         }, {
//             image: "image_3", // reference to the image mapping below
//             fit:[300,200] ,
//             pageBreak: "after"
             
//          // fits the image to those dimensions (A4)
//         },
//         {
//             text: "Social Media Statistics Twitter",
//              color:"#3A907F"
//         }, {
//             image: "image_4", // reference to the image mapping below
//              fit:[300,200] // fits the image to those dimensions (A4)
//         },
//          {
//             text: "Social Media Statistics Media",
//              color:"#3A907F"
//         }, {
//             image: "image_5", // reference to the image mapping below
//             fit:[300,200] // fits the image to those dimensions (A4)
//         },
//          {
//             text: "Tweet Map 1",
//              color:"#3A907F"
//         }, {
//             image: "image_6", // reference to the image mapping below
//             fit: [300,200] ,
//             pageBreak: "after"// fits the image to those dimensions (A4)
//         },
//         {
//             text: "Tweet Map 2",
//              color:"#3A907F"
//         },
//         {
//             image: "image_7", // reference to the image mapping below
//             fit: [300,200] // fits the image to those dimensions (A4)
//         },
//          {
//             text: " Social Media Sentiments 1",
//              color:"#3A907F"
//         }, {
//             image: "image_8", // reference to the image mapping below
//             fit: [300,200] // fits the image to those dimensions (A4)
//         },
//         {
//             text: "Social Media Sentiments 2",
//              color:"#3A907F"
//         }, {
//             image: "image_9", // reference to the image mapping below
//             fit:[300,200] // fits the image to those dimensions (A4)
//         }


//     ],

//     images: {

//         }
// }

     this.layoutTagCompare = {
        pageMargins: [40, 100, 40, 40],

        /*
         ** Header; Shown on every page
         */
        header: function(currentPage, totalPage) {
            return {
                image: "logo",
                fit: [70, 70],
                margin: 40,
                alignment:"right"
            }
        },

        content: [

            /*
             ** PAGE 1
             */
            {
                text: "Facebook",
                style: ["header", "safetyDistance"]
            }, {
                columnGap:20,
                columns: [{
                    stack: [{
                        text: "Social Media Statistics Tag 1",
                        style: "subheader"
                    }, {
                        image: "image_1",
                        fit: [(700/ 2) - 60, (595.28 / 2) - 60] // 1 column width incl. margins
                    }]
                },
                   {
                    stack: [{
                        text: "Social Media Statistics Tag 2",
                        style: "subheader"
                    }, {
                        image: "image_2",
                        fit: [(595.28 / 2) - 60, (595.28 / 2) - 60] // 1 column width incl. margins
                    }]
                }
                

                 
                ],
                style: "safetyDistance"
            },

            {
                columnGap: 40,
                columns: [
                {
                    stack: [{
                        text: "Social Media Sentiments Tag1",
                        style: "subheader"
                    }, {
                        image: "image_5",
                        fit: [(595.28 / 2) - 60, (595.28 / 2) - 60] // 1 column width incl. margins
                    }]
                },
                {
                  stack: [{
                        text: "Social Media Sentiments Tag2",
                        style: "subheader"
                    }, {
                        image: "image_6",
                        fit: [(595.28 / 2) - 60, (595.28 / 2) - 60],
                        pageBreak: "after" // 1 column width incl. margins
                    }]
                }




                 
                ],
                style: "safetyDistance"
            },


            // {
            //     columnGap: 40,
            //     columns: [ {
            //         stack: [{
            //                 text: "Social Media Statistics Media",
            //                 style: "subheader"
            //             },


            //             {
            //                 image: "image_5",
            //                 fit: [(595.28 / 2) - 60, (595.28 / 2) - 60],
            //                 // 1 column width incl. margins

            //             }

            //         ]
            //     }
            //     ],
            //     style: "safetyDistance"
            // },
              {
                columnGap: 40,
                columns: [
                {
                    stack: [{
                        text: "Tweet Map 1",
                        style: "subheader"
                    }, {
                        image: "image_3",
                        fit: [(595.28 / 2) - 60, (595.28 / 2) - 60] // 1 column width incl. margins
                    }]
                },
                {
                  stack: [{
                        text: "Tweet Map 2",
                        style: "subheader"
                    }, {
                        image: "image_4",
                        fit: [(595.28 / 2) - 60, (595.28 / 2) - 60],
                        pageBreak:"after"
                       // 1 column width incl. margins
                    }]
                }




                 
                ],
                style: "safetyDistance"
            }
            //  {
            //     columnGap: 40,
            //     columns: [
            //     {
            //         stack: [{
            //             text: "SOCIAL MEDIA SENTIMENTS 1",
            //             style: "subheader"
            //         }, {
            //             image: "image_8",
            //             fit: [(595.28 / 2) - 60, (595.28 / 2) - 60] // 1 column width incl. margins
            //         }]
            //     },
            //     {
            //       stack: [{
            //             text: "SOCIAL MEDIA SENTIMENTS 2",
            //             style: "subheader"
            //         }, {
            //             image: "image_9",
            //             fit: [(595.28 / 2) - 60, (595.28 / 2) - 60]
            //            // 1 column width incl. margins
            //         }]
            //     }




                 
            //     ],
            //     style: "safetyDistance"
            // }


            


        ],

        /*
         ** Footer; Shown on every page
         */
        footer: function(currentPage, totalPage) {
            return {
                text: [currentPage, "/", totalPage].join(""),
                alignment: "center"
            }
        },

        /*
         ** Predefined styles which can be applied through "style" on every content element
         */
        styles: {
            header: {
                fontSize: 18,
                bold: true
            },
            subheader: {
                bold: true
            },
            tableExample: {
                margin: [0, 5, 0, 15]
            },
            description: {
                fontSize: 10,
                color: "#CCCCCC",
                margin: [0, 5, 0, 10]
            },
            safetyDistance: {
                color: "#3A907F",
                margin: [0, 0, 20, 20]
            },

        },

        /*
         ** Predefined images
         */
        images: {
            logo: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoEAAADICAYAAACXmzs1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCNTlENTg1OUQ4NzExRTVBRkNFQjFEOUM2NzJCQ0I0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCNTlENTg2OUQ4NzExRTVBRkNFQjFEOUM2NzJCQ0I0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUI1OUQ1ODM5RDg3MTFFNUFGQ0VCMUQ5QzY3MkJDQjQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUI1OUQ1ODQ5RDg3MTFFNUFGQ0VCMUQ5QzY3MkJDQjQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6aKkUXAABIxUlEQVR42uydB5hcVfnGv5nZvtn0kAQICb1L6KCUCKJIE6RIRxDhDygiKAgKgtgAEVFUQIQoUqWDSgeVIr2GZpAWCIH0ZJOtM//z7vmGTDZT7jlzZ+bOzPt7nvNkszt3Zu655577nu98JZZKpYQQQgghhNQXcXYBIYQQQghFICGEEEIIoQgkhBBCCCEUgYQQQgghhCKQEEIIIYRQBBJCCCGEEIpAQgghhBBCEUgIIYQQQigCCSGEEEIIRSAhhBBCCKEIJIQQQgghFIGEEEIIIYQikBBCCCGEUAQSQgghhBCKQEIIIYQQQhFICCGEEEIoAgkhhBBCCEUgIYQQQghFICGEEEIIoQgkhBBCCCEUgYQQQgghhCKQEEIIIYRQBBJCCCGEkOqhoVZPrKmlpTq+aDIp8WFDZKWLTjY/p3zfJWFam2mtpg01beWMNt60lUwbZdpw0zr0dc2mNeqxA9/EtF7TekzrNG2xaQtMm2PaLNNmmva+aR/ozwtNW6KvTfJW0lVVW4ss+ffzMufX10q8uYUdQkgpps3uLmnfZVsZceKBklywmB1CIsV7h5xBEVgDxKAlxVpLU1n+llTBlKrAd2tTgbeKaeuYtq62NfR3w0r8+QtUEP7XtNdNe820N0z7UAXiEg4fQgghhCKwWhlh2gWmtecQgRA6P1MhVBzxmMSHtJoVbad551i+V25j2t4q9jYybe0KXcNh2jbI+F23aW+aNs20F7Th57c4lAghhBCKwGoC4u/wPH0EYXhF0SLQiL7kwk5ZdMP90nHg5yU5f1E+IYit20PFWvuiRrOKQrT9VST/V4Xgv0x7wLTpHFaEEEIIRWCl2dG0zcVasAbkmNjt33tUuPSL9Ycbm+P4eWJ96IoDgq8vKQuuv3vg544DPifJxUtzvfpR03Yw3/RiSab2kFQqyv2LLetNtEEUYvv4OdPuMu3vYreOCSGEEEIRWHYOMe0oWX6rF0KwS0VgSv9fehJxiccarRA0P7fvvKUkO7tyvfp/5sWHxttbfxJrajwu1dNbDRHeCECZpG0PsYEmEIJ/Me0ZoQ8hIYQQQhFYRvpkWWRsJpWJdI0bIdjYKItuuNe0+3K/LpWS/r7eBa2T1/3GqFO/+k6sveXcVGdXcxX1OwThqqYdY9rRpj1s2pWmPSg2qIQQQggh5ZAedXzuKcfflx5sDRsxiECRPK09kWg4YOnzr+8956d/vDg5b/HhsfbWhVU8/nYSaxG8zbQTxQa9EEIIIYQisKIiMHpOd7HY8dLYcEOiqfHWrpen3zH3vKmv9b0z80AjBOdF3EewEFuZdrFpN5t2utgUOIQQQggpEfW8Hdye4/ctGQJ5aJ7jh2T0H0QLUsq4bCVjKxr59i41bbbDcXsNiD1sHzc1faF7+rufmnfRtUcNP/ErBzWuvvJ1qc6uEQXSzESdydoOFLtN/EexiasJIYQQQhEYCvepaBscHfy8/h/Rv8dmiMJMYnrcdBWTp4ityOHKfNNuchSB1o9xIGwlJvHm5vE9Mz68af5vbth7xLcOOrhh0vhbUouXtla5EASfMu0Xph1k2jmm3S1RtMwSQgghFIFVx3WmXa/CIpu4QMTq1QHeB6XYujy/AyqO9DseM3fQd3zECMGNe9778OZ5l92y88hTDjk2MWr4VanOroTEamJ8bm3arWL9Bs+TMJJzE0IIIaTufQKT+i8sgKipO6RA69DXDsY3otjnuFczfn7ctH1N2ybe1PR8z3/fuXXhn/92jyRTP4u1NNXStUL089dMu9+0IyX3Vj4hhBBCAsKKIVYIf9W0LwYQZbCtYRv59xX8vo8PEpEQSLMlFjsu3tA4deljL/wl1ty077Aj99pG4vHPSTJZS9dqNbF+grhW2CKexuFLCCGEUAQWA7Yc9w742kUVFoFPifUlxDY06gePFFtG7rvSkLjc/Hvu4oeeOrJ1642Obd58/f+klnSNqcHrhQokm6gQvJbDlxBCCHEnzi4YoNPhtYsq/F1Rbu1u/Xm8aZuZ9qZpu0kqtX+sIX5dPBb7waLb/xVPLV5yxkDewdpkHdP+YNpvJH8UNyGEEEIoAkOh0uEWqFd8U8b/Uf4OwSJ/Nu3zkpLWWFPT9J5X37x4znl/uik2pPW+iOcPTKkI/0CslRPl5G4Qa+FDzkBsv6Pm8Ex9XWa9ZtQn/oa+bj0OTUIIISQ43A6uTv5j2kumbSzWPw5WMUTOIkhkF4jEWCJxbP+sudv2TnvzzKZ1J22fXLy0ZaDiSHTAljaCXP4htnTca6bNkex+mUiLgxQ8k8RuA2P7flOx5edWMu1zpt1r2tdNu4fDgxBCCCkMLYHVyfum3Zgh5H9g2iumnWHamoII5oaGl/vmLfjRgqv//kyspflKSURGACKtzV9NO9S07Uw717R/m/ax5A7MQRqdj0x7UuwW8NF67H6mnSk2ahjR29gmR87GGIcIIYQQkh9aAqsXbIEeY9oEsUEtu5l2iWkbmNZq2nXxRMMvez+c/aXO+5+4qO2zW+ybnL94bIXl0bOm/cS0W3L8faxYqyYsfAh8aVEBiG1gbHm/JzZBN6yIS1U8oiGp9Ke1H44XG0V8jiyfU5EQQgghFIE1AbZSkez6u2Ituhea9oJpJ6mY6peGxM/7Fyz+7tJHX9imdbtNr4k1xE9O9VcsZQz8GL9j2jtZ/jbFtMNM21y/OyKeByc6ROk4VHH5SM/9UbH+g++KTdb9oDaU4dtDrK/kDfr68hGPSayRtxUhhJDow+3g6gZ1dd/SnxEYcYEKIgitGaa9lGho2HLpi2/stfTxF38ba2/7oELfE9VZjswiABHdjLQ2sAweJdbfb5xkT8iN7d4JKhSxlXyxaY+IrSQCP8hGfR22xc837U6xVsSy2T5jDQnpfesDmX/lbRJvauboJIQQQhFYY/2QCLEf40WKlNdNuyzj/wepAErXO35b4om49PQe0j3tzf+lurrukESi3H17l9ht68WDfr+5/g3BHCM83rdJRSEsfrAIIjDkANOGfXLuNtCkfKHRsZikunulb95CqYHazYQQQih+6gKX/bvBKgpPe98yZu0hXIPfmfZ0xv+xPYwtUfgIfmpgezIe337JPY9t3DN9xkWxxoZ5ZexX1Pk9PosA3EJsYMtmIV6/KWK3f2FV/JLYSirlx4i/GONSCCGE1Jj4qVXgJHe9Cpb+Qo94sTnrMkGAws/FWrNcHO4gJhcISr4VB5JXnyY21Up6G/UIbTKQI7ChYbz09e2YnL/oklgidqf5zeFl6Ffk8/u22GCOTLDdi23sNUr0uTtpQ97EX5v2DIc4IYQQQhGYi4e1+QAfvPMq/P0REHGRisEVlWs8Lv3J5HbxjvYrpLEBkbRfkdJbyuCr97csv/+pwEJZeiB0dxRbUQRl/pZwmBNCCCHL4HZw7QBr5N1Z/5JKSbyxcbP5V9w6vv+j+S/FmhrvLPF3maXiazC7ivXbKxcTxaaPub1MwpMQQgipGmgJtCCydLLY9CRBt3SxNbxQbALj7gicA3LnwR9wddPWXeHLJhKr98yYNa5/4aK34iM6LpXu3n1L6LyGZNDPZemvr4m//2Qu0PfYkkcQCLbWYZlFYMxobYgqRkWR+7V/rha3bXtCCCGEIrCGgTD5qYoFF6bpMR8Oei+ID1hZSxGZOjOP6HzZtJNV6Ixc/k+phrjEx80581IZ98eznjaS7AGP8w0CfBTvyPJ7pH/ZKqTPwNYuUuM8ZNo/TXtRbO3hzow+h/hbRT93B7F+gkhHs41pZ4mtUEIIIYRQBNY5EA69Hsf1ZhF6O4v1EWyT8C1OsKbtb9pTeV6DdClIyQI/uDGZZxhriLcgRUyqr2+BxGNXSzK1k4TvEvCE2NrGg0G939WKfG9Y/FAbGImnkV5mQZ7XIiL5dW2IRB5q2pfF5iOEGDxbbHJtQgghhCKQhALy1K1Vwr5tDfAalJRDKphfirWEWeLxDyRhNF8SinBALD6l4ixMMQ3L3KJBv4fQ3LTI98Z2LtLh3O4prrF1P9W0a8RWJzlWxeHDHLKEEEIoAkkYwDrYXcK+7Q/4OkQM7ys2GTMsfg9I2oKItDESg//cTSGLwDkqArMJ49U93xOC8mem/UGKT6eTvj5Xiq02sq1pW0p+y2oUiWt/rmnayqaNEuuGkFCBvFRFL0rmIUXPWyH1XTkYr+e0ko6b1iznhcCj98W6RpQCWND/r0bnx5QuqB4p4WesOWhstmlfpq8h7umPdWy+WeKx2aDzH1JS9YX0nngfuKTM17H4gZTevQT3/J6mfUb7sBBIF5beBQkjMwL676iAz5+4zrMwRkwr8Fr448NdB37sPVV8X6G/Z+i9VVWZKCgCaxtMsGfoRDxfVtzyhlUNFrG1Qvo83ATZ8vKN0Ie7Ky+JzTX4gMMxEAyr6qSCfzv0vD/USXG6ivQ3xFYVWVO/W/GCAlbW0tVmhhhCypsvivVrXFUfri06kWYmMU/qg6on46GL8/2X2G30aRKd4BiMjSli/VMn63m16qTaKMuq6qQyzqtbJ1oIwRd0wfOQLkLCAAFiZ9Xw/NgfsgiE/+2nxUb/Y2G1WsCx2aWi/g39PnfpPR9sbGIxmyzodo1UWMfrfROWj3ZK+7BXz6NTBS3uq0d1Ify+hOsTjrG4t2lfdTgGffr3kEQJnhHfd+yjNwOIwBZ9Bu0t1R2wF1NjwrMUgSRqJDNXqanu5RZbSJB9T4gi8OUcNwAeCMMd3+sunbzfC/DaRj2H3bVBTKSrscQzBESPCkGIX0Qwv6qtSV/nPwnFYtL79kyZff6fJdYcagpGiNQDTTvYtLVlWY3kQivxJm14QI/RlfwXdCLHNUci7ccruPrGuRyu57Wa51w0UcXH1/Whi4CoK2TFGtU+90wtR5CHZRHDmDpAr+F6nmNztL7Prjo2Uf7xYhVTOcdmvLlFOh98UprWmSjtX/y0JBcszidGEpK9HnmYwDIPixb8seGrfJ9pf9LzWBiS8HQVlf0hjuOkx+v7A55XIuDYiTqNUs4ypSHBPIH1hFk5N22wxuC6tpio5oY0STyb429NEsyXMQ0STR8aUACiBjESZT9m2gVqVRqeYYGI6ThvUDEK38SzdZV8ngqQnmIny1hDQuacf7WklnSFdbVW0Yfiv037sWkbhDBRpkscflkftr8ybVKZR2GHintY7n6gAqDYxWiDCsIfqBXmWBUY5XzoVdOisNgHFSzn39OxCVeNjUN6iGOO+FKGEFyzkHzoffM9Sc5fBH/nQmKoXGDOQWYGJOSHFQ4uN3tJ9przPkKwUmM4VcJxViv3Wj9FIIm2BkwmZcRx+9lty2U8pZN5sWCL7oUCAiQIKCl3nOSP/E2LpHPEJsg+QdwtjdiGRN5AWAX3DKN/Y40NgwW2L7Bm3qzib3yJhkOT9vPtKgrLAQQ4yvn9Vq9fKYAYRO3sK1U4k3D5vAobiL+VS/QZEPXwyUSqqf1yPryam2XhPY9J71vvS6y1Kar9tYvYeuawvK/K4UMoAkllzQCdWX2KkSOxs8i3xjbwG3lWkUFWSLBKfktsepd87GbarWL9tkYX+b2xdYxtxBMjcolOERvBvHWZPu9TKszODslakYsDVNjuXabz2l8fvrvzrg8N+OfeIHYLvhxsoGPzJ5KtzGUqJYmGRll068OSnLNgoDxmRMF9dbyO/604jAhFIKkMqZw6DFVPLi7y3eGH9UGeSbDQlh8e2N8sIEbxHgh0wXbxliH2DKJQsZV8eoWvEEr/naffp5xgi/iHYi10LSV4/5PE+uqtXubzWlcF/td48xf9nDhHx+bwMn92q97zl2f77FhDg3Q996okl3aHZYUvJRCAfy2jiCaEIpBkTJit5vkezzlRwq/uuSLeHo7QuXw7GguIi8d0pbwoz2uQ7BkJsM8Vu5UbNthPOtu0b1Ti0oi1ap4qpbXGFeJYHQdhfgdYj84X6wtYCTBWkF/yUM4A3sBt4gdSWef9w3Wh1jp4YRtvapa55/1JUr291dCX8EH+k2Qp7UkIRWD19UXV9F+yu1tGnnSwNIwZmSuNCXJ1wdl7qc/bi93qyEVHHhH4tlj/n1l5jof/GLaFji5xn0MI/khC8hF04GAVgFEwZeBafD+k94Kv4TlS+cg/XFdYOXfkNOd1Dc+NyFyH+//krBPQwk6zyG2uFrd8ZDJA3tPhHF6EIjA6/bBMpGBbIRVoNmmrsT6ENQ/+N65TKRJkvpjn7yvl+D18/7AF/FKeYyeJ3f79Upn6AJaj86X4EndB2UCFUnuExgEsPzsV+R6I+v2xVM4COBhYkn8jNgcgCcZEvReilL7j+9nGZqqvX7qenCbSUDXT8fZiLZsJDjNSSZgn0IKghksGxEwq1ZPq70d9XasOm5py+ZpgYkRusgU11A8Qf0gbspHY3HRBwTZtd56/r5rns+7KcxyEGIIkyu1Dg5xn8ENCsEgp8+jB2f07UigVRvlBepUzxboHzPM4PqFCcv2InRdSmcD38XhOeYH4YQTHJraDfyE2J9/i9KI9tbRbFky9S8Zu+6kw0zSVGpSvTNdCJ4QisIJ0f3IjpmRAAI7+zuHSM2OWLLj+HkmEm/w36iAwAxG6SE0SZPvsH2LzeuUTOmtk+f29amXIBdJPTJXKOVEfpmPi/sAKutvZJwnVFQ6O6DjYThcCv/c4FqWtjozoeR2kC4tHC7wuVsP3eJBz2zHCYxOphhDssyyYzQjB+JC2oDs4UQFzI3ZCkNvyYyH1cF9FDm4Hf/IETw20VCopo045TFqnbP6ZYUfs0T7soF0l2d0zsN1QncMyZvMCogWPnvtIRdDDBV73rtitzHxlcmBV2njQ71DCDb49uQJBEB0LH67PVrDnsNUPa+DQQMOnv9+MG/PMTATe3YGf2gmSLfWFx+iVZVUuwkgGnF4gHi7ueQoxyE6J8HnBD+uYAK9LlqD5fP9KfY+TI3wNRUXg6BU/KZS8w6kSXctcC6Yv1vvTt0Tj3Lf5RhiFOS7KRs1aAgeVRys4BlNGIA07+IvSvttnIAZj/XMXniTx2B+G7r/LvdheWPLYC5Kcs3AgJUG16P1Ud7c0rT1JRp9zjCQXL5FYY6NLv6BaBxK1YusF2e9bswhFbGU+UeB94IP1qUG/Q/DFKzleDxWFRLR7R6ALIUL3Ne2qINNYw/hRy6rcFmZDKc7PEel4UDP3GRXjqJuLiws3BVQsQDANciCiJu8kz8/YRh9SLttVm4ktAebLO7r4eFbH4GydlJv1vCboZ0wR67PmA8rnIV3Hkzn+/pZe+zDvdPiXnO+4sEE9ayT0fj/kufq9gNfdF9QQR53qpzKuYXfG2JygcwKsjet4fgZcNvYRG2AxkKi9550ZMv+SG2XEyYdIclFR5VthJUY09FLJ7rPXpIsJ7Fag/CHSVW0ifv6veP9DxCbGnl+HAhAXCq4nSKYdBUtLjy5+ffLG/kcNHBSBUaB5skMEfm/vQDm1jv12HogyU8GznfSn3k0u6rx36AG7yLBDd5O5F11rxODzEm9pif62A6yamK3WnzQwQcZamn2+M4QFtvXu038n6O9Rc/hCKWwpBFurVS0NSin9sYAF4rgI9eSpKoIWFexyN2vx4eLncA+Hpz+rOP9vgNevpBOaT1UV0QftXfq5QThC/Oq04mGAKh8XqKgtBMbiaaYdJW4lCdMLE1S+eFqypzVaGmBx44Prll/6e5T7wYKI4FGeQhdb7QgImh7g9bDkneQ5NnHvwII29RPLDea8nt4w8gXiOv1b3MqZ7arjcYrH50EMww/7kToUgejj17VFgYQKUldwj14rtARGh9FnHe3w6tiAH+BAEXI7gSCEf5xaHOLm4Z7EA37kSQcN/H3Jo89FXwiarzZ0n51k6GG7G2G7uNiJ8Vptq2dYSoKSae2CReAsyR1sgdeeHbGehMUBg+miEN8TkcB7eByHFQqiI10Se3+kVg2U9LtEckdq53u4TQgoOIeouPI5L2whX+ZwDCxMyOmIyHIEGLkmuf6SWpFmlfkB40JcSpO8Ox9wxdje4zgIsTN0cRKU2To2n9CF4RjHz4TFEhbFZ5ZN5aEYbxtUZHY7HHO3WoLgxuLqSwkrN8rLPSa1W7O6WoDPsE9Vl78tNw6riJr1CcR2QPDWORBdljGBbK7/riIZKSVSPX0y8sQDpW37TSXZ1RXdDPXmeyV7e6XjyzsNbAOH+D3fchSAENM7Z/z/V3luFEzmSOrbFsEePcbjAZUPBF241hFNav/4Vnb5qz6k+xyPG+kwKU7xOC98nwsdBWAml+nxriC4oBpqC5d7pQnLvU9U90WOAjCTO8VaBF3PFf6q20boWmE7F4Eej3oc+3mJVpqoeqRN53pX49g8yb+7RRFYhWyZYdmY8MlsjNQxpo044SvSPmULSfb0RPLLwxdwxLH72jm1stbKr2kfioq/XDcKJj/kcFs5ouMBvgWHhfh+O4q70/3LYpP2FsNU027zOA552YJYsbbzEPEYFz8v8rxgfXnW8RicD/zeajkK2Hfucy1bCP/enxT5udfrQsWVLZa7l2JS6cX5XLHl9VyT7m+qRgdSOfYQPyvgQ2KtwBSBNQJmkE30Z2zFLOcbk+rtk1hbszSMGx1WJFr4J9DUIE3rTHSJVC0FyPF3SMb/YbHJ5dsEX5odIj4mDpCAkcIBJ3yXJxWcDVF7d1GRn4v3gT/hYsfjMDE2BeijyY7nhRXKpeJXpSYTBFBc43HctlL+7dYog2u8kcdxcDNYWORnpy3drpMqMg+slB6Cqd5+G/xWWSH4gNjAGBeaZcUsCqR8wPp3kMfivEvnsFS1njhF4IpgCzNt/YNfyJAVH6XJfEEAsUpaF/q7u2XYUftI48RxA4K1guPq9Ix+hJP1dTleC/+jY6pgXGC7On8qBzimdxV0I8IDa5LjZ3+Qp/98Vq2uVjPkeSxkpZ2Qcb2DAr++20I6LwQvve14zCZZ7+/6ZazOf65j8+8hff4LKqBcQHTugKtGvLlZlj75kiy68X6Jd1TUq2SJ5K+lnovJHIIVY4r4BfVg6//+aj5xisAVQTDIqIz+cTWnQTiWcgbK6a+QSialacJYaVhlzCcVTyoEVlRpK2CvWgqyWZ/QT9+V6ijlhQjU3SSPRQwpeFo2X7+QhRiCynW77WGxTvRhsFgnLpeVK65ToXD7tcQ96AQJw8NKizFNbEoSF+BTtpqQNGM8Fii4hmEF12As3ON4TIfH4qMcIP2QazT4+hyCFQHPbKRB88mecJFUsRUwr6CoY7bJEHE+Fxc+TrCCtUj4kV6wMGYPzNBgkNYtN5aWzdaT/o/mVmpLBNu658mynFnYFrklx2v3FL9o0kqxi4qhFWsdp1ISa2uR4UfsLh+eeL7EmnPuMuIh67qt/K+Qz+NptVa4OKLjAZXP4oOAkBEVPC/ca8+btr/jInhNqdKovhKJwFEeYynMOm0v6Pu5bNNPimBfwk/yI8cF7kR9JvdxKJYV7PLs63Ecdrjur/aTpwhcHmwNbZrxf+z5dmcTXNhqTeW2SEwr9xdPdnVL86RVpO2zm0ty3sJKCUCk3YDVb5WMB/Mvckxq6Rxh1VSTb7yK3Jey/tUIweTipVLAG2CcuFuKXwn5PF4TW/PaRQSuUeDveNi5WM2T+j3C5A2x6WbCPK96Yry4+6qGnd8N28twE1jb4ZhVdexFqawTcqzCB9rFz2+sLhDnciiWDVgBD/NYwILfiFsaoUjC7eDl2XDQ5IOtzBWc8ZFWpmOfKdK68doDJeUqDWrWNk4YK6POOEoSK42ohC/gBBV7f5blU4TA0nNfjmMQaLFNFY4R5M3rKOL4UY4PWjjcvx/yObwv7kEmY4v8e7aH5MyQz+tdFbcurCwkjet2PlwUwq55mxZPLowTv8Trpcb1vu3wFCPEH+S+9alzjmjgf9ZCB9ASuDybDXoodGd9qCSTEh/eIQ2rriSxV/9nN40rFAqCSLiGVcfK6LO+LvG2Fkl15Y2Ma9GV5mraxujEk1DB26miY6GKBPyLbUMoXSjLlI6ZVp2scAMhzcbeOR6ml+RYneNhc3LIXQET3GL9zr16XkOLFGzZ2EFFr691bqTHQ3FhyOeA93P1xRsR8nlhq6wr5PP6WPLXsQ5D+NQyrr6qs3XOCJMFHmNzZESfZa4WvUaPa0CK40jxy/pwtc5hFIE1RIOsWC9zSa4bGdVFhh/zZel+7g3pmzVHYk1lXojCB7CrSxpXUQHY2pIvYhlib4ppe4nN5TYxoGztV3HVoy0lywJfCm1pYkv84Rx/+6pYX6ywVttYkf1DbGDATBWBEC2I/txT26ohfR4mjO2LEIGuonShhO8jlBJ3i9kQyV0ZOSHuW9xzSnBeC8V9e4YP3WUCxPVhuFiKT+8zmC4PIT9U3AP4yoFrKibcX0wYXT6we+GT//UVfd7UjPAhlnVkxRDxpZInKhNVRlp33FQW3/7P8sYHpQXgpJVl9BlHSXxo28CWcA6QABO+dzt7fFJC/FNooN7uvCy/h+XlGyH0Ah4USJuC0l/Z6rzi2n2gN+tVYsutfSmkK4BUMVeInw+Sa53bTinN6Frs8b0bJF2ndUUB4VoveKGE78PVneP75aNNiK+QT+8ShM0Sj2sYRdcmnEfS4bvFPeYH4s+x4p6gG3PxzeJWOSvS0CdwGZ/NMiBgrcjpO4Vi5UP3M9oqWcbKHDGxAnCiEYCnHyWJEUPtFnB2TlWhtHOZ+xJ9do9kj45GWaViUzogghD1OY/OIQAH85Su+C4O6fxgTfUtI+dqMu6S0tQT7fX43ok8i0nX81pagvPq9RAlTcKqITqzOBsF0N+lcED2uYZRfJb1eozxRg7FsrCSuNd4Bm+rUaFmoAi0YOvw8Cy//yDvTYwo4a5eGX78/pLqL0/1kFR3nzTCB/D0IyU+cqgkO5fm8gFEFY6fSmWS4T6oQm0w8CE8pMj3RnLhfUy73UOYQhRfGsL5Ibhjs+wXKCV5DHc+icRLtbrwGbCxPL+Pe3x+qgTn5HpecYpA72sYlbHJ5xhxBUaBtT2OQ53rt2qpI3jzWBDxma1m4HuFp86YNK01wYjA0kfkwvIYH9Iqo846RuLDhkgqtwBE5O0PpDJ+Mngw3CHZt3SOUSHoy+/F+hP63oSwMGBr/O4QznP7FU68zwj0NVaR4V/bR5I93byrCKlvfBYYSXZbycEuzlc89A92Bi+vtc6gT6CNmP12DsHwZmHJYxfDDWNGSt/HcyWWaJBYQ0JS3d1Zl8kDpqBm99R4SfN+jSuPkcTYUQNCEIIwhwBE4MG3pHLlsJBD78Esv4fP5ZeLEJa/Mu17UrwPEtTZiWLT14wr4n22zPYtY40NEu9oL597ACEkys8WF6GRkhrIO1cFIFBwc4/jYAWcVmudQRFohcmWOUTg/wretcmkJEYPk1GnHykLb3lAel99R3rnzJO2LTaUWEvTcmIgFo9LsrNLljz7ygrLw3guYZgOApkwTkb/4GhJjBk+kKewwADfrEJ9iZNFGam3s/ztABWCPiApJ8rLhRVI8F/TzhQbVOILKoeM0tXh8ouCJBfzhJBPIupd5s8l7LaSX5ODxN0K2KPPoZqj3kUgwvFPznPR3wl053b1SMPYkTLmzK9L58PPSM9rb8nQ/XeRWHvrcoIglohL/4JOabz1IZFEPK0MJbW0Sxbd+U/VfHGJNat4TAvAieONyEQQSIckFy7JN61g+3dHcSu5FCbImzQ1y+99E3IC5GM6TcKPJEVia5QY8y1bhzqTKKX2COfVSJEq8esJCYpr+iE8LBax20oKAkC39zgOLk7PUgTWHkdIbqsZ8gN+EOhdECDS1y99M2dLy+R1pXWrjSS5qFNS87Pcz/GYDD1st+WPRcWPNVaReHuLdN79uCx59tWBZcpAUr5Vxw0IwIbRw2xJsvzl4JD3aHIF+xPpWAaby/GFYQVcw/P9sHUbJKnwUBVm2E4JUtAeIh9VTnbwFM0w3W5MERg5Wkr8ekKCMt7x9bACzme3lYx2fea7+mMhyvtXtdopDXV+gx4rue1q74mraX4gWrh7oOVe66UkubBzhePatps8YB3Etm/HvJ2t2EulJD6kbSANTAABCODjNqlC/bkgx40ySYWcK8+YdlyBSRHjF3kQEdm9kT7QkyreIc6QHue5PMfD/HqXaft5fD+kcliX82qkwLoJtWw7JJjvaEJfT2sgCRvkLnQtSThHWDe4lMDtyydX7O36PKIIrDGOMu1Tef7+bvkeXUYYLrHGrlhHmzQOG7Lc3wZEZSyQawkOHF6h/rxBsqeF+ZrHZAjhd4rk346H4L1Q7Jbu4NxaqIiyrYrIqWItftneC0Lhj2Kjw30CaVYXEiUQog+3Axd/n1KkqiEEZTlHOR6DxWsXu64kYMF3rIfmgRvSn2v5utRripj1xKYayUdlcgH1JyXV27es9Tm5wjVIZdLCwAp4UQ6R9H8eNx2CNvIV50aI/zVik302FhDFqE6ClDC75XjN/abd53neSC7O7cRokS47FrTxoUtKwcbinn1gOhckJb0ePlbAh0x7rJY7ph5FIETS101bK5IisDjSNX7LzS/FbqsN5rseq2FYFAtF7Z5v2k6Oov9ayV4nEtajK8WvBupQKS7NDCGkNvm0uJfhe5HdVjKQH9a1JF+PPo/mUATWFluI3aIsxLtVeG7YRv24zJ+JQJDLs6xgNzHtQMf3gpBEVY98ubL2Er+i34jU+61p+2b5G0rc+fh8wNF4vBBCyDLgjvJFj+OeY9eVBBgB9vA4DqL85lrvnHoTgagxeZoUDt3HFtGHVXh+M0x7rYyfh5XSj3L0FRI7j3B4r169Nu/nec1IKa4SCgIGLtOFwODPxs3umoYG281jV/gtN3QIqWcQqOYaNPa22PylJHzgG+66I4VnwvWmzaMIrC12MW3vAK/DhZ9dhecHS2A5/RfgMHtTlt/v6bHywhZwoXrAKDu3ZZHfGZPBr2XFQJBbPG54WAJXGiwAY42JgcohhJC6A8+Y73ochwpGC9h9obO+Po9cgTvYn+qhg+pJBOJckRg6SJgttlSXhvS5DVLeKGwktZxRhs9BJPBPZcVal/CD+aa4Rdu+YdqPC7xmQwm2jR8ERA6fNOh32P5/1PF9YJFcztqZXNolrdtPlqFf+qwkuxlzQEgVkhL3Gr7wN0M92qlidxxcQXAaS8aFD6qD+GRxgA/57HrooHoyVyBL+GcCvhYi0LdCBbac4Q+HvHVIkJxO2bJQbAqA6So48G9vCc4TfiUwY3+nhH2JFeupkj14BhPh5xzeC32AaOCZBcbp8VI4mMeFE8TmEcysD/13cY8gG8p5lpCaoi/g3IwFL3YC1jbtULHbwD7gefA4uz10JohfvXpUvrqiXjqpnkTg0RI8U/hHniJwithcRBBBo/OsMuH7gRI0SIPyoFhLWJhcYNo2pm1Xgn7sV4F5b5a/IWXK6eJWLxPbyXcWeA3E+2Ehnweier8lyyeyxpYMzHcuaV8oAgmpLbCIR8YD+DwP9j/G3Ia0VB26wJ8kNv1IMc/SGyVgiVLiBIIIN/Q4DtvA71ME1hawyLnUC4Ql0HU7AMmNEdgwpsDrMImsow3RswjkQK66qyS82oQfqei9VaxPRFhAwH4vxyoJ2+3f1VVxULBtjUTO+bbeW1RYdpRgXHxBbCTfOxnfB9HOmzu8RzvnWkJqijVN+3aZPgu7Q0hD0sduDxVYaH3q1cMv/LJ66qh68QlEkMIEx4HgIgJhTfpZAAGYDYSvI6ExtiLPCFGYv64iM6zcU+gPbAFfmOPvsH4e5/iefwkgfPdUsVYKsL28S8b/YQV8yvE9moUQQvy4JcTFP1nGzo6L+TRXy/IuQhSBNXKOOzmeK3zegib62EFsgERjkd8TqUYQHDFV7LZqGEAAwscN2639RbwPUsAgKOMXOfoFwRFI4NzkKFJ/V+A1CC75fonHxg4ZYwOr8Rcc36NRCCHEHbgF/Vrcd51I4efGcR7HLRab8aKuqAcRiPrAmzoe0xnwddiqRJ68sLYEsVV8iK4Otw3pPd82bT+xW9X/czwWPjEPiPWtmJrnO0O8buL43r8x7b0CrzlB8td3DgME8EzM+L9rHyWEEELc6Nd5k7kBwwdWwO09jsO2/DSKwNoDYmo1x2OClvvZR2wARthsZdptYiNiw9gehpjDNu7u+m+hQBREMiNlAer+Yis93xYpooFdfS9QnePKAq+ZqCIwVuLxsc6g8fGhwyKAEEJcQeQxdlX+zK4oiabx8edcrCKw7vJ61XpgSJOnSBut4qPQljAsbKXyCYNjK8qcbS02MOKDEN4TQSjfUQGGqhmwsmHrGeZzbIUiL9J0FX1PBBBDG+tk5lqTEf6ThfIwItBmQhnGSPsgEbhErE9oUOsut3IIIS48LKV1c6ln4OP9GY/jsOP1SD12WK2LwJVN28xTgCUkf8TW2lL6rUqA3FMb6qRxT0jv+Yq2tAhqVDGzVILnLkSKlUvE3X8RE+DfCrwGltCvlHGcTBy0Sl/ouKonhJCgbKBzHHMDhs83PHQNrIDXS3gFIqqKWt8OxsN9PU/xWMjXC36Gq5XpPBDlhHx654i71a0QsPbNV+ETVNBAOJ4nNqjCBVhWsR3dVWBMHi+Dy7GVFpSSS28794nblgCz/BNCXMDC+fe6kCbhsaNpn/Y4DgUWbqnXTqt1Ebih+Fk7VwkgAlEgvKmM54It27PE1tfdsoJ9CqvhT8QvOz62oR8o8BpEch9Q5nPqkOWjfFMOx9J/kBDiCgLpfi4MLAtTyxxh2kjH4/r1udRDEVh7QKD5bteuLPmTE6Pf1qzQecHn4R9inV9Hlvmz4f+IdDjf8jgWUXCwHuYzucPCiGTUrRUYK+l7IeEo7udz/iWEeICSZnuzG0IBPu67ehwHP/mb6l091ypDihCBCAxZrUC/VTI/HLYvUdYIfgyfL2N/XiR+NYlhLTtVCqdDQHqcnSvQn9gCTlv/cF2HORw7d7n/pVISa2qUWEuTEEJIHmBoOLECi/laAwt3FEYY73Esgi8X13Pn1XJgCGq6rlOEOIYv4VN5REMUsorDKgh/QVTegK/duyX6nIkqAPfxOBbiCglRbwvwGWdXqB8XybLtgDaHSRnBNEsyfwHx1/Xs69L58NMSa2QeaUKqEOxWfKT3d2zQXJauHYxdi+EhfBZSmCF119Xsdm/SJVhdQSaM6+u982pZBK5a5AprcoEbE5UlEEDQUuHzHKmryd3EWgenSrhRTjCxw3dlE8/jYWo/N8DrzvZcyYXBXFlmCYQrwJCAx/UO7utYQ4P0zZwtPTNmSaK5VdzcCwkhEeDfYitO9OUxEsA1Bjsyk8QmJka6sNEenwVBCR9oVHWia4kfe3s+O1AjeF69d14tbwdPlOISDRdKLfOkriSiAurgogrHg2Lr7Q4p4r3Qb/B5hAXv1iIEIFIgnBBAlGIV95UK9l1mDsZ1HY6DCFw+MATbwQ0JHXgUgIRUIZivUDno3RztbbFlLx8z7VqxaUm2M+06z5seOzobsNu9wKL9KI/jUK3qr+y+2heBxbBugdUFBtGjETtn+EYgOfYdYi1w+4u1iAa9zsNU/KIUHhJnflP8LZ2viq0k8nGB162hn9daoT7rk2Xb6FiVu5Tr6+bqnZCafC66FAHoV1GIjAmXenwePuuzUvrqSLXIl9UA4sqVUjr3qaqilreDi602MUIFQb78QVeINeWPiOD5f0EbciD9R2ypNgx6bH0u1okLogd+LdjG2EjPF5PRkCI/GxbSg3RizAeE3/liE29XindMe19/Rl+41JxcShFICMlYUCK7AQISXatWwBp4sdR5kIIjQ9VQ4QpKg8JIwq2aGheBxSbihAXs8wVE4NOm3Wza0RHuh021iU4w2UTgGAkvXxVC7g8T6zNZCGwV71Ph/sH3TQf5wIrqUgEFInAOpxFCiIKk+yiluZm47W5sqc+s6ezCwMDQ4BP8iRrBr7L7LLW8HTwmhPeAKChk5UPQw4wq6RNY+JD6Bv4nqPu7nk48YQnA58X69z0d4LVw5j0rAmPwGVnms3iY47FLKAIJIYO4V9xdhdp0TibB++v/PI6bJdYXsJ9dWPsicFgI7wF/tUKl0bDFegaHktxn2r4SzAKItDbwnemo8HeelzFZQxjv4ng8BOAiXnpCyKDF4f0ex01m1wVmf/ELpnlYWLO5bkRgWwjv0RFQGCDX0CV1OoaworpKbHTv/wK8HgE7fzRtbAS++xsZEwK2pl3zfsGXkH4lhJDBIMfsbMdjGCEcDLgxYdfGNSM/3KCmis3/SJRa9gkMK3/f50xb3bS38rwGqUKwtYlo4n3raPzAwfZCbUHEEML5kdh6kwh8d3zfv4u15MEyeYDH8e9wConcohYPh1UDjseYCvk/UcyTkJkmduvRJXcgFsgJ4VZlIZBce0uP45DS5252X/2IwLDODalivmja7wq8DluLx+sqZa86GDvIR3i22MSqQRivD9vtIvL9kRsQFsx2FfCuiV5RYeQNTiGRAg9QRGeu53AMruGfKQJJyHykInBDh2MwR2L3iRkHcoMdPpQXHep4HKx/F7P7sq+cSWEOCygScON/TWq7BBD84H4oNgAkqABcybQ/iLWqRgUkeUWuxyNV5LuCHIGMMIse80v8ekKCgEXF+47HQNiwjnB+Pi22OpYrsALew+6rLxEY5r7/1mKrcAQBfiAoOXSmhFu+LQr9eaNYH0kkd/444HGIPkY+xd0jdC5Yof9MbF7Ec8Rab12B5fe/nEIIIXmeBS5gHhrGbssJkmrD99zH3x/VtLjNXmcisCfE94Lv0Hck+JYhSon9WOy28JNS3VtNELIPm7aHWIvocw7HImH3NQ4Culz8SCeS3xex8sY24oIV76iYxBobhBBS97gmfsZzpp3dlpP1xK+8KNKA0QpYhyKwM+T3Q+TWNxyPQZqAncRaBf9XZf2Hbd+/mXaoWOvfPxyFNW7Yv+r5R4mH9LpAnBYToLJiLsRYTFKdXdL79kxzZ9HTgpA6B88glx2pmIST1aJWOUL80opdlnXBTmpeBJYif9u3xGaCd50IfiLWj+Hnpr0Y8X57RayFDM63sP6hYkqf43tsrQJw64id2wcq/i4wbcci3+uxFWbwhoT0vvOhLLj5Pok3NQshpK5B1giXXSCIwEZ2W1ZQ5OBwj+NeFmvMIDmo5X2r2SV4T+SR+5XY7U3XlQXq6J4u1j9uiopC/BsFR2CUTUPSZFjJHpfCNX/zgb65RG/aKAEhiy1cBIJ8psj3Qum9Z7P+JRE3M7mZy1NZ5/6UuLsGlKqofKWL1cdK9J6u7+tzTUi0xkaUrx/HVjigOsgoj+Ou1MU/qUMR+GGJ3nd7sdGxJxchuNAQZLGmWGvZjvrvGmXqG0REvqSCD+IPUa6ofNJd5PseI9baOSKC4wFW7y3Els4rlv8Uschwtao2lkgwud77/ZLbsbrf47yaJfydiAZxt6S4WmtqlaTn2EyU4Lv4XMMoJgCOe9y7YZ5HTCq/2AuDCbp4dwXGjNt5a9evCHy/hO99olgz85VFvAe2q5/Xhjxl8HVYS+x2M5IXIz8hkocO1+uU0Bs6HmASSemDGT8v0JUQCpNjK/o5vTlm63cII2IK3w259k7Th3sUiYckAMED4h945Cq0W0s0kbd6fO/ePA9h1/7oKMH80yTuVQR6hKSFvGs2g2Ypzfala6L/rogKedeFTirP/OBjsa6ku1eYFvajxGaZcOUOqT5ffIrAEHm7hO8N0XO+WGvj30N4v6XakGfwsYwbeIQO/vH6L6KTkUuqXSeYhoyHMCaPThV9COqYldHm5XmAF8uaKgAPr5N7Bv34z+zr7pjEWgpqENeApY4STeauDtaL80zqvR7idnQJ5p8O8SslRawIdO2LdgmvMlMaiErX4IiwFrNh0+64gMP9tSTP4r6SllrXxX0ypGuCeeJAj+NgBLqGtzVFYCmBf8KlYpND31eC90+qmEObFsH+RWmuL4vdFp9YR/fMo7lWl6meXln66Itm2s07785z/LwR+lBcGOI5wAro6ota6Hu7+siuJOFbjVcSdwvnbCHp+cb1Gg6X8KNZh4h7NYh5HgKpHPhUtVic529dHiI0rGe8a+qaHo/vm41D1dDgCp7JL/C2LkwtRwe/G/KDMxvwVcBW7l51NGawMvu62Ojfi+tMAIJ7sgqimFnw9/dL78yPzY95F/9zHD9vpIqbMBnj8YD6uMi/Z1tEhX1eq3qcF53G/QXxOBWCYTJC3AMAsNvRG8H+HOv4eoimfBVsOj36sinE6+J6LsWmacPch5ruri4HsAz/mrczRSAe1NPL8DmYCKeadmyNjxU8sE8y7U7TLjdtmzq8XzBBP5L1L6mUxNpaZfgRe0iqr7fQA8tl67TBcyWcjzU9VvbvFvj7TMf3w8S+TsjnhcAq123ud/gY8BbE2AqeVALhtKrH946aJRBW7lUcj5mrLSyR7rPYy7fAciHtmlQMSFG2ucdxSAnzHG9nikAMwpfK9FlYJcEqVkwFiqiykWnnifWD+0Wdir802Ap+PutfYjFJLuqU2T+bKrH8OQJRr9jVQh12vsXJHiv7QmmDIAJdfcq2KcFYdZ3T3uRj4BPgk+yaX3XjkJ8jCI5ztQRGUchPUBHmKmaXFlhAuor0MFJ1Nei95cJCKc7VAs9R5Kp1tWRiMUArIEXgAF05H9ilW/khlxG2Cz8n1etv2agrWNyAd6rwOVVsBZBEHd8rKb22uYVOMim9782yW8O5me6xQt45xHsVX25bx/fDxPpagde8Je5bwjtLeNtVq6i4dX1QvcXHwCfgof2u4zGflfBSQmHO3F7cginge/b2cjdqX3+he7AcbCo2oM+F6ZI/ovZjj4XWZiGcC3YNtnA8Zr7HfJDJp8VWqnLlLslWzYnUpQgESONS7qixLXQg/tK09aukn9p0Bf4lsRZN5MH7i1hz/FDeJgMgGOTuggqrcN1gRK255rDEKnyHkM5jQ9O2cjxmhhS2trzpYalYTyf7MNjKw1rxhjAwJJNZHqIY/b5xSJ8/ybRdHY+BAPwovQiLD22XhpVHm2VLxXeHPy3uLhcvBbg+rvfYrlJ88A52IiY4HoMdD9/AEBhUviruqbHwrEeJuF7eyhSBmdaJ6RX4XAzib5p2r9iScVErnxbTCRd1fb9t2h/EWrluM+04cff/qAceNO2/IbxPv7i7KWBb54SQzgMR3a7BPM8FsEAgtcVrju8Lq/PxIZwT3mc/cbcqPiHhRDDWCrjGr3gcF9bY3MdDbLycXlQle3ukdfL6MmTPHSS5eGkl+xH3124exxXauYLgdfXbhEgv1u3iq46vT3nMBZnAirqnx3EPi925IhSBy61Gnqjg50NMnWHaraZdLTaX3uoV+B6jdTLAFu/Zpt1g2s0q+mCxPFjKV62kGpmnK8yw+Le4Jyne3XNizORT4hfA9LAEs6g/5nFee2grBmwr7+1x3L8kmqlFKsmT4p40ep8QriGS45/icdwzkhGFmurrk1R3T6W3gyGaXIOeIO4K+d0uFPeSnuiI04o4ly09BO0iKc4V60SPBR1S6PxR3H1a656GGj+/HhVgeEBUclsTviHId4Rwd1gnX9XJC3mMsI0G34kF4l+9AJYiREXCmXZlXYlOUBEK0TlW/zZawk/uWg/cp9crLFCjeY64+Qwh/935Ol58rDVwUr9Yx4cLXSqWggBrKaIbxzme1wVit9t9zgsPWwQsuW55zZTy+gxXC0/pGHPZWk/o2II/4Ysenwmfwt+Ie0qV+RVe5GfjC2J3V3wWJHMDXp8jxC1tyi66+HNdyCJn449NG+Z4HLas/+PZf5t4LuhgAfwHb1+KwGzcpZaX3SPwXZp0xYsG/7slsqxayEKd1BZk/A4P4HRdzJhOtk0q5PDwbFfxN1Rbi/69Wf+NcYiHspAIFG0WawgcN4PtK6Sa2d/xu8CH7lp9yPxTgtUZxRiABfAi06Z4Tq5vB3wtxMOzHpaD9Hl9Qz8vFfC8EID1K9M28Divu6X0CeWrEQi5p8XdvxI7CcgdeoIucvoDXsMN9Rru7PFdX5DSBAGkxN2XHDc/LPW/9RBNAO44QVJHPaILGJeoX/Tzz/TZcmPAeWOkLq4+73EuT4h7PtQ03xL3hO84n79I/hyLpI5FILZ7rhLr/9Yaoe8VUxHXzmEYadIR0gWvZt/MOS7VMq/yEIHplfIdOulhW/9/WRYL6YXABF1soKrNKM/zv81xcr1G/PyhNtG+hrXierHbY0v0vFJ6XumSYrAYHaaCw8fpvV8/i3WDs3O72F0L175dR8fLdSrq38wyNhtlWQ49JNk/RuwOhY9Qu18GRdqHtBWcylhEZ3uzeMZ5tOoi5hBtPs9UBF49FvC1r+hCyzX1C6ytU8VGc1+h91e3Ph9TGecEQwJch7A17+NLiPe72bPfNxE/l5endcw2RsTwkZIqCk6pBxEo+tD8j94AhAQFD7ALAyn6RELmXHjtQNUQSQRytX1YrP/VVh7fC9s0/6dtpk7oc1XUNMmymtPFBvi8LQEiogcByzsCaNb2+DxUnzhNrQHT9PPTJcEa1cIySWw0ajEl5x6TXEm/CbhH+39Lj2OxqD1aG8YmrN5zVHA06TUepyKwGJ90+HvfuMwWlJJ4W6s0bbCGpHqLdvNEqqFf6YM8nmXxjrE3VM9jorgnhR7MzZKjFGUOUKVqD4/nd7OK7qPERvsjqnqh3l8Qf2l3orFFnMt/VZz7cKjnggDzwvf0/KIgApeIn38rRWAJwc0M0zbSbNRzrjviBibbp4K+ONbc6OKWjO1++FEVW+R8vLjnIwsKrDmu0fUL9bwuKeJz8UDaXPyqBQSZC3BeH3N45x2b2NacGuGxiYX9G5+YXsziq2nieBl68K7SP3t+sdZACLuvl6mvIZCvE7cApb+L9b30zQGYrkK0ZgnOBwUTFnse6ys+0y5WUaG/mkRgrUcHZ/IPvXkICQJ8o37jMjmnurpdPwNWs4cjev5Y0ftGRF/nIp7LzH9UBJL8YEv+3xH9brB8X7DC/WeEYHJJVxQSRbv2s6tfIyaan0fwXF7R8ylmgVYLVFXu0XoSgdin/2ERqxRSX0AAvRx4cPUnZeRJBwXdCk4DqxnySEYtrQG2lX8p7tUj0mBr+myJns8d+vlccS/bV49AaGBrfkkEv9u5RYzNqC20sEPlU9AAPnC3ROx8fiS0sIOqWoXE6+ziIOntbzhGSQHgq3el2xIjJY2rjfWxQtwfwTEJ5/7Li3wPWN0vidh5wb/zPg7vwDyui5Qoce0K96a59+JDWmXU947wscZXCojrM8U/Qr1Hj58RkfO5PIKilFAEZgU+C6/x0pMc9KlYcC3tVoxDOrZ2bo7I+UMAnyzB0kgUAnkN747Ied0g0dxCizoQ8tdE5LsgV+fpksXCnFraLbG2Fpfo/Erzcx2TxfCK3quVrnoDF4ufCsu1UQRWCYgqw3ZCNy8/yQIqu9xY5s/ENiVy5N0ZAQGINBfvh/R+s/S8Ku1bBgvF8bznvcDW+bcrcE8MBmlRkCR5xW3gZEpat59cTb6AyDv645DeC7kZzwxp0eYDAsdQIvUd3ioUgdXELSGswkjtAQvxGRX6bFgej5Rwy9O5gCCVAyT8WtvIFYdyifdX6LywTYU8iXM5vL2BnxdSi1TKbeFvOjanrfAXI/z6+3oGooJjDVWR7AK+tqdKuDZL7FycXoFFDiruHCilSdhNKAJLCszn3xduC5NlYCvjFPHYBg4RpItA3czjxD/jvivYw4bf16ElXM2/LdbCeFUZ+xI+V9/R/mQlgeJZoPfH8WW8R2DdQhTwYbqYWPEF3d0y/ODdJNbaPBAdHGFgUUVy89NKINYgKM/XxVa5AmYQmIKa0c/w1qAIrFZm6OqJ0cJEdBItOoVQqq/oBxH8nS4VW4MU/nSlfLI9rxP5WTKo8kIJ+EjF7aG5HughAh8lVEq5ULgFHPZCCT7VKCV2Z5nGJizz83K9KJaISdMGq0uspXkgQCSCoI+QfPuLpv1O3PIBunKjzhu3lvDazFYhe5iw7CJFYA1wm2TJN0XqjrskYGWQ/HdTXBrGjQrrYfSMChlsw6FGalipOvAQQnUCpEvaWc+9XP5EEGQIMviMCk8k+w3TqR3+vj/SB+H9HNYl4yXT9hO7zR7m2IRwgTUa/nKo7nRHPtHU390lQw/4gjRvvNZAYEjEgPX5IV30oLrHY2X6XOxufUWsa8kLId5f2Jn4q14XLJhLkdYqVSP3R1WdR71UDMkHCmuj9uNB7Iq6BD5w381nbQh22yNNRZuMOukgmfWdiyTW3BzGd4NV8EqdfFFnFTV5UV8TZaqGO7xPp9gSXshLdq++3/sV7HMEjJyrlpG9VLThvFCpwaW+d0rPC6L2HhWYb0V4rHU4vh6lyaJa4Qhj80+m3TRobKJU4QiPsTldx+ZNKubzX/hkUhrGjJDGSSsXsr6na7SXGiykYO3Gdiws0X/T86kEsNherUaOfU3b3bSNdN7ocLi3PtJrgcAu+NE/ErH7I6qMqKYvSxFob5gT9QbZgd1RV8BP5xsSlm+oEYKpnpJkSVikAucaFUob6r8oIj9GbC3hJhUMSR3TsM7AoX+GTuQ4x1ci1v+wLlylbS3TNhVb/mmSaaP0oYAScnF9KHWraJiv5zVdzwuRo0urYLz928EyE1Ox3Bnxc8L3u07bBB2bq+vPK+UYm7hWs/UavqvXcJrTrdbbI61bbSOtUzaX/llz8kUG4zNfUEEd1hZpv47FRToW39OFCNwcUM5tQUSuDb7fVG1r6bWZpPPGaF1wNelYS58T5sQP9brg/oLVt1xBVa+a9oRULtI5DGJVcM9SBGYBE9KRaiHZjN1RF2DSg7/RPVX2vd+RFQM4Enovp8VSn5TW96gUTJflI5Mh/tr0IZU+r7SA6KzSMXe+Xqcg20UxfRhWk0/je7KiFQ/XrrEUYxMLrlR3T6HUMBDdCHxqD1Fc4H16dKHVVaX3V/r5n543+vX+qqQAu8i0K2pABFbV3EsRuAys5I4Sm5F+A3ZHzYPkpr+vIUHbX2PXp6uKHrBB6ZHoldIrNZUWshCe86RYd4/aJGqLxQUSHStq3RBnFywHtg2+atrr7IqaBrnjzqnyFSchhBBCERgyT4kNEnmRXVGTwHcJpZb62RWEEEIoAslgnhObAoFpJmoL1OdFIEgnu4IQQghFIMkF0mmgJM7l7IqaACkOkHOP5cMIIYQQisCCpMt4fUtKkxyTlAdk0v86BSAhhBBCEegCItt+LTbh5qN1cs6IGEPOJuRfq/bgicvERn1TABJCCCEUgV4g0SvqP6LSwewaPUeE5//LtKNNu0Rs5vNqHSNIxYFqMN8U+gASQgghFIFFgi1h1DxFLUj4mC2ugXOC1Q+VJK7Q85oiNpP8eWIz/1cjyOKPUnBIBt3LYUsIIYSsCJNF+4HSNqjJuI9ph5u2q9gKB9UEciGixuWDpj0gtpYsaihfb9oBVXxtcF6niK3dSQghhBCKwJJwq2l3m/YZ075i2t5iazJGlZfFbmujPS/W7w+g1A0CYL4ttrZktXKnaadlnBchhBBCKAJLBmqZ3q/CCrUP4TcIS9rmYmu6VhIUA0cVlIe0oXYkioNnlgra2bQzTdsuAt+3mGtwvvY/yw4RQgghFIFlBVHEr2i71LQNTdvFtJ1M28i0DrFbxrESfHavfj58FrEd+ozYrd7nVPSh0Hlq0HVfX+y2KZJit1dxvyOC+VSx29opDkNCCCGEIrCSIBr1SW0/MW1V0zYzbbJYv7s1TBtpWpvYIIxm05r0egwWiUkVeT0q9LrEWr5g8XrPtLdV+EF8vmHarDzfq10F6VdNO0SFabWCPrjKtB8Ii8MTQgghFIERZYa2O/T/iMoea9o408aITcUyVEVho/4dVq1+FX+IQl6kYgfpaT7Sf/sCfj5E6PZifRb3VOFZzUDsIlXPXzi0CCGEEIrAagLWvZnaSgUEJQJWpoj1+9u6RvruGtN+LjbIhRBCCCEUgcSwkmnbqvDDv2tKtKOVXXjNtHNMu03sljghhBBCKALrDkTxYtt4ZbEBHluYtqVpG4jdXh5SQ+eKrfDfiq1gMoOXnhBCCKEIrCYaVawNE1vNAr59CPaAzx+2hjOjWhEYElehhwb/veFirXwQfWuZtrZp64jN6dcm1ZvapZD4u1fs1u9THEKEEEIIRWA10qtC7UDTdhNrrftYxWC3/j2lAhCCsVnFHaJ5O7TVS4k/RP2ifvFlYpNxE0IIIYQisKpBzr4TTNtBbHoWROpOYrd8AqKg7xEb8Yu6zH3sEkIIIYQisJb4l7bLTdvLtMNMW72O+wOWv7+adp3YqiudHCKEEEIIRWAt84y2qWKtghCDm9bR+SM9zg0q/l5SMUgIIYQQisC64S2xNW+vFJva5VCxpeYQDFJrfoAI9nhWhd/fTPtAbHAMIYQQQigC6xaUgbtdGyKA9zFtV7Fl5lau4vNCZZN3xUb6ItDjSV5qQgghhCKQZGe6aRdoQ/4/VPzYXpbVHY4674utZ/y4aQ+Z9qgwwTMhhBBCEUiceErbL0zbSNvmpm2sP4+NwHeEte9V06aJ3e59SRuDPAghhBCKQFIkSJnyvDakUUGuQWwTI7J4Q7HJqNfV/48q4feYI3Z793UVfq+Y9qZpH0ppayETQgghhCKQiE02jfaC2Hq6qCySTi69immrmjZBfx6nohHBJm362sZB1x8iE7n6sG2LAI4FKvgg7mZoe0/sVi/+DivfEl4GQgghpDqJpVIp9gIhhBBCSJ0RZxcQQgghhFAEEkIIIYQQikBCCCGEEEIRSAghhBBCKAIJIYQQQghFICGEEEIIoQgkhBBCCCEUgYQQQgghhCKQEEIIIYRQBBJCCCGEEIpAQgghhBBCEUgIIYQQQigCCSGEEEIIRSAhhBBCCKEIJIQQQgghhfh/AQYAet/ybEnKzzsAAAAASUVORK5CYII=",
            noimage:"data:image/jpeg;base64,/9j/4QnJRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAgAAAAcgEyAAIAAAAUAAAAkodpAAQAAAABAAAAqAAAANQACvyAAAAnEAAK/IAAACcQQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKQAyMDEyOjA2OjI5IDE0OjM0OjIxAAAAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAyCgAwAEAAAAAQAAAcIAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABIgEbAAUAAAABAAABKgEoAAMAAAABAAIAAAIBAAQAAAABAAABMgICAAQAAAABAAAIjwAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAf/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAFoAoAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/AIAIjQoNCMwJKZMajsaoMarDGpKZMYjMYkxqOxiSlm1oza1JjEZtaSkQrUvTVgVqXppKavpKJrVz01E1pKaTq0J1avOrQXsSU0XsQHsV57FXexJTSe1Be1W3tQHtSU1XBDIR3hCcElP/0E0I7AhMCOwJKSsCsVtQqwrNYSUlrarNbUKtqtVtSUzYxHYxNW1HYxJTHaGtLnEBrQS4ngAaklQGThGmrIGRUaMhwZRaHjY9ziWsrqf9F73Ob9FGyWfqeR/xNv8A1DlxeHVkYuF0Frpf0/qGTh5dZP8Ag8hp9PKp/qZDHNvr/wCvJKesuzum0ZLcS/LpqynxtofY1rzP0fY4/nIzqyNCNVypv6Vi9L6v0zq1W7rN92QfSdUXW3usP6lbjO2+9n0Nmx36NdVgU5FXTsSvLM5LKK23kmTvDWh8lJSJ7EB7Fee1V7GpKaNjVXsartjVWsakppWNVd7VcsCrWBJTUeEFwVh4QHhJT//RmxHYgMVitJTYrCs1hV61ZrSU2awrVYVatWq0lNmsKVmXjY92Nj3PLbcx5rx27Sdzmj1HN3NG1ns/fTVqn1Sm6zq3QrK63vZTk2uue1pLWNNW1rrXD+ba537ySm3i9c6RkYN/UKcicXEn13lj2lsAP/m3tFjtzXezY33olnXuk09Lb1ezILcGww20sfJJJrj0dnrfSa78xcl0ronU/SwcR+PbXidU2nqQcxwNf2O665vqyP0X2yn0a2b/AOdR8Hp/Ws3D6P0/7FtZhUW5OQzNFlVLrbXW49dLnMre/wBemmyy70/5aSnq+odV6f06ivIy7g1lpDaNoNj3lw3NbRXUH2Wbv5Cpn6xdFOC7PGQfs7LBTZ+jf6jLDxVbj7fWrf8A2FiYdfVsP9kZmXh5GQOiHJwMhjGFzywhrMbOxa/a7Ip9Jra32VqtlVdYysPOfZj5Vm/JwrqbTiMpyLGtdZ6tzmYzBvfTsb6fre+lno+p6fqJKejwurdO6k6xuE97zSAbN9VlcB0hseuyvd9H81EsCqdFue85DHu6m+Nrg7qdbWAfSbtxjW2vd/wqu2JKalgVWwK3aqtiSmpYFVsCt2KrYkprPVd6sWID0lP/0psVitVmFHYUlNqtWqyqlZVmspKblZVqrUgDuqVZRMizJZiXOxGepkhh9FkgS8jaz3PLW+36aSlq/rFhOrz31122fswF9jWhs2MDjW+zHl3uaxzHfT2LQr6tjHLwcSsOsf1Cp2RW9sbWVNaH+rfLvbu3bG7fz1gYHRc3AycF+5uVj+m/Ey62tFZZTaDZY573P/WtuQ7d/pFDC6P1vFws2K/UyxQ3p3TzvYIxy9zrsjdu9n6N/wDxqSnocH6zdPzMDNz6W2mvp4c61hDQ9zA02Mtq923Zc1rvS3qzgddxs7LGJVXa15xas3c8N27LvoM9r3O9Vv5/5i513QOqYZuqxnMzaMrp9mC7Y1tGw1s/UNzH2n1d0vq9b/txGxMbrfTc+vLp6ccsHp2NiPaLqq9tlQm0S9x3bXe1JTudU+sGD0zKxMXJ3l+a7axzI2sG5tXq37nN21+pYxir5/1gZi59uAzCysu+mtt1hxwxwDHzB99jH/mrJzuh9X6zfn5WSW4AupZjUY7w25xYwDI9t1Vjfs/67/hFXv6d1bJzRm5/S7Mp12HTVdXXlNoLbmbm3brKrW+qx3/baSnax+v4Wbfi044scM3HdlVWEANDGO9N7LPdu9Xd+77EDN6wKM44FeHk5d7a23OGO1roY4lv5z2u+k1Z1GD1zAu6be7GGdZi4dmNaGW11hpfYXVV737d/o07GbmV+9C6hhZ2X1EZuR0x9jLMZlZoZlNrLLGuf9K6p7PV/R/2P0iSnYba62llrq30OeJNNoh7f5NjRu9yBYU+Nubh0sdUcctYG+i5/qFgHtDXXf4X2/nqFhSU17Cq1iPYVWsKSkD0B6M8oDykp//TdpRmFV2lGYUlNqsqxW5U2OVhjklN2tys1uVFjlYY9JTeY9HY9UWPR22JKbrXqYsVNtin6iSmybFBz0H1FB1iSkj3oD3pnWIL3pKWscq1jlJ70B7klI7HKs8or3Ku9ySkbygOKI8oLikp/9SAKK0oKI1JSdjkdjlWajMSU2mPR2PVRiM1JTcbYitsVRqK35pKbQsUvUVYKSSk/qKJsQkxSUydYhPsTO+aG5JSz3qu96m9Bekpg9yA9ym9Bckpg4oRKm5DKSn/2f/tFKBQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAADxwBWgADGyVHHAIAAAIAAAA4QklNBCUAAAAAABDNz/p9qMe+CQVwdq6vBcNOOEJJTQQ6AAAAAADlAAAAEAAAAAEAAAAAAAtwcmludE91dHB1dAAAAAUAAAAAUHN0U2Jvb2wBAAAAAEludGVlbnVtAAAAAEludGUAAAAAQ2xybQAAAA9wcmludFNpeHRlZW5CaXRib29sAAAAAAtwcmludGVyTmFtZVRFWFQAAAABAAAAAAAPcHJpbnRQcm9vZlNldHVwT2JqYwAAAAwAUAByAG8AbwBmACAAUwBlAHQAdQBwAAAAAAAKcHJvb2ZTZXR1cAAAAAEAAAAAQmx0bmVudW0AAAAMYnVpbHRpblByb29mAAAACXByb29mQ01ZSwA4QklNBDsAAAAAAi0AAAAQAAAAAQAAAAAAEnByaW50T3V0cHV0T3B0aW9ucwAAABcAAAAAQ3B0bmJvb2wAAAAAAENsYnJib29sAAAAAABSZ3NNYm9vbAAAAAAAQ3JuQ2Jvb2wAAAAAAENudENib29sAAAAAABMYmxzYm9vbAAAAAAATmd0dmJvb2wAAAAAAEVtbERib29sAAAAAABJbnRyYm9vbAAAAAAAQmNrZ09iamMAAAABAAAAAAAAUkdCQwAAAAMAAAAAUmQgIGRvdWJAb+AAAAAAAAAAAABHcm4gZG91YkBv4AAAAAAAAAAAAEJsICBkb3ViQG/gAAAAAAAAAAAAQnJkVFVudEYjUmx0AAAAAAAAAAAAAAAAQmxkIFVudEYjUmx0AAAAAAAAAAAAAAAAUnNsdFVudEYjUHhsQFIAAAAAAAAAAAAKdmVjdG9yRGF0YWJvb2wBAAAAAFBnUHNlbnVtAAAAAFBnUHMAAAAAUGdQQwAAAABMZWZ0VW50RiNSbHQAAAAAAAAAAAAAAABUb3AgVW50RiNSbHQAAAAAAAAAAAAAAABTY2wgVW50RiNQcmNAWQAAAAAAAAAAABBjcm9wV2hlblByaW50aW5nYm9vbAAAAAAOY3JvcFJlY3RCb3R0b21sb25nAAAAAAAAAAxjcm9wUmVjdExlZnRsb25nAAAAAAAAAA1jcm9wUmVjdFJpZ2h0bG9uZwAAAAAAAAALY3JvcFJlY3RUb3Bsb25nAAAAAAA4QklNA+0AAAAAABAASAAAAAEAAQBIAAAAAQABOEJJTQQmAAAAAAAOAAAAAAAAAAAAAD+AAAA4QklNA/IAAAAAAAoAAP///////wAAOEJJTQQNAAAAAAAEAAAAeDhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAThCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAAAAAAAAAIAAjhCSU0EAgAAAAAABgAAAAAAADhCSU0EMAAAAAAAAwEBAQA4QklNBC0AAAAAAAYAAQAAAAU4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAAF9AAAAAYAAAAAAAAAAAAAAcIAAAMgAAAAFwBOAG8ALQBJAG0AYQBnAGUALQBBAHYAYQBpAGwAYQBiAGwAZQAtAHcAOAAwADAAAAACAAAAqQAAAAQAAAACAAAAAAAAAAEAAAAAAAAAAAAAAyAAAAHCAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAyAAAAHCAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAEAAAAAAABudWxsAAAAAgAAAAZib3VuZHNPYmpjAAAAAQAAAAAAAFJjdDEAAAAEAAAAAFRvcCBsb25nAAAAAAAAAABMZWZ0bG9uZwAAAAAAAAAAQnRvbWxvbmcAAAHCAAAAAFJnaHRsb25nAAADIAAAAAZzbGljZXNWbExzAAAAAk9iamMAAAABAAAAAAAFc2xpY2UAAAASAAAAB3NsaWNlSURsb25nAAAAqQAAAAdncm91cElEbG9uZwAAAAQAAAAGb3JpZ2luZW51bQAAAAxFU2xpY2VPcmlnaW4AAAANdXNlckdlbmVyYXRlZAAAAABUeXBlZW51bQAAAApFU2xpY2VUeXBlAAAAAEltZyAAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAABwgAAAABSZ2h0bG9uZwAAAyAAAAADdXJsVEVYVAAAAAEAAAAAAABudWxsVEVYVAAAAAEAAAAAAABNc2dlVEVYVAAAAAEAAAAAAAZhbHRUYWdURVhUAAAAAQAAAAAADmNlbGxUZXh0SXNIVE1MYm9vbAEAAAAIY2VsbFRleHRURVhUAAAAAQAAAAAACWhvcnpBbGlnbmVudW0AAAAPRVNsaWNlSG9yekFsaWduAAAAB2RlZmF1bHQAAAAJdmVydEFsaWduZW51bQAAAA9FU2xpY2VWZXJ0QWxpZ24AAAAHZGVmYXVsdAAAAAtiZ0NvbG9yVHlwZWVudW0AAAARRVNsaWNlQkdDb2xvclR5cGUAAAAATm9uZQAAAAl0b3BPdXRzZXRsb25nAAAAAAAAAApsZWZ0T3V0c2V0bG9uZwAAAAAAAAAMYm90dG9tT3V0c2V0bG9uZwAAAAAAAAALcmlnaHRPdXRzZXRsb25nAAAAAE9iamMAAAABAAAAAAAFc2xpY2UAAAASAAAAB3NsaWNlSURsb25nAAAAAAAAAAdncm91cElEbG9uZwAAAAAAAAAGb3JpZ2luZW51bQAAAAxFU2xpY2VPcmlnaW4AAAANYXV0b0dlbmVyYXRlZAAAAABUeXBlZW51bQAAAApFU2xpY2VUeXBlAAAAAEltZyAAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAABwgAAAABSZ2h0bG9uZwAAAyAAAAADdXJsVEVYVAAAAAEAAAAAAABudWxsVEVYVAAAAAEAAAAAAABNc2dlVEVYVAAAAAEAAAAAAAZhbHRUYWdURVhUAAAAAQAAAAAADmNlbGxUZXh0SXNIVE1MYm9vbAEAAAAIY2VsbFRleHRURVhUAAAAAQAAAAAACWhvcnpBbGlnbmVudW0AAAAPRVNsaWNlSG9yekFsaWduAAAAB2RlZmF1bHQAAAAJdmVydEFsaWduZW51bQAAAA9FU2xpY2VWZXJ0QWxpZ24AAAAHZGVmYXVsdAAAAAtiZ0NvbG9yVHlwZWVudW0AAAARRVNsaWNlQkdDb2xvclR5cGUAAAAATm9uZQAAAAl0b3BPdXRzZXRsb25nAAAAAAAAAApsZWZ0T3V0c2V0bG9uZwAAAAAAAAAMYm90dG9tT3V0c2V0bG9uZwAAAAAAAAALcmlnaHRPdXRzZXRsb25nAAAAADhCSU0EKAAAAAAADAAAAAI/8AAAAAAAADhCSU0EFAAAAAAABAAAAAg4QklNBAwAAAAACKsAAAABAAAAoAAAAFoAAAHgAACowAAACI8AGAAB/9j/7QAMQWRvYmVfQ00AAf/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAFoAoAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/AIAIjQoNCMwJKZMajsaoMarDGpKZMYjMYkxqOxiSlm1oza1JjEZtaSkQrUvTVgVqXppKavpKJrVz01E1pKaTq0J1avOrQXsSU0XsQHsV57FXexJTSe1Be1W3tQHtSU1XBDIR3hCcElP/0E0I7AhMCOwJKSsCsVtQqwrNYSUlrarNbUKtqtVtSUzYxHYxNW1HYxJTHaGtLnEBrQS4ngAaklQGThGmrIGRUaMhwZRaHjY9ziWsrqf9F73Ob9FGyWfqeR/xNv8A1DlxeHVkYuF0Frpf0/qGTh5dZP8Ag8hp9PKp/qZDHNvr/wCvJKesuzum0ZLcS/LpqynxtofY1rzP0fY4/nIzqyNCNVypv6Vi9L6v0zq1W7rN92QfSdUXW3usP6lbjO2+9n0Nmx36NdVgU5FXTsSvLM5LKK23kmTvDWh8lJSJ7EB7Fee1V7GpKaNjVXsartjVWsakppWNVd7VcsCrWBJTUeEFwVh4QHhJT//RmxHYgMVitJTYrCs1hV61ZrSU2awrVYVatWq0lNmsKVmXjY92Nj3PLbcx5rx27Sdzmj1HN3NG1ns/fTVqn1Sm6zq3QrK63vZTk2uue1pLWNNW1rrXD+ba537ySm3i9c6RkYN/UKcicXEn13lj2lsAP/m3tFjtzXezY33olnXuk09Lb1ezILcGww20sfJJJrj0dnrfSa78xcl0ronU/SwcR+PbXidU2nqQcxwNf2O665vqyP0X2yn0a2b/AOdR8Hp/Ws3D6P0/7FtZhUW5OQzNFlVLrbXW49dLnMre/wBemmyy70/5aSnq+odV6f06ivIy7g1lpDaNoNj3lw3NbRXUH2Wbv5Cpn6xdFOC7PGQfs7LBTZ+jf6jLDxVbj7fWrf8A2FiYdfVsP9kZmXh5GQOiHJwMhjGFzywhrMbOxa/a7Ip9Jra32VqtlVdYysPOfZj5Vm/JwrqbTiMpyLGtdZ6tzmYzBvfTsb6fre+lno+p6fqJKejwurdO6k6xuE97zSAbN9VlcB0hseuyvd9H81EsCqdFue85DHu6m+Nrg7qdbWAfSbtxjW2vd/wqu2JKalgVWwK3aqtiSmpYFVsCt2KrYkprPVd6sWID0lP/0psVitVmFHYUlNqtWqyqlZVmspKblZVqrUgDuqVZRMizJZiXOxGepkhh9FkgS8jaz3PLW+36aSlq/rFhOrz31122fswF9jWhs2MDjW+zHl3uaxzHfT2LQr6tjHLwcSsOsf1Cp2RW9sbWVNaH+rfLvbu3bG7fz1gYHRc3AycF+5uVj+m/Ey62tFZZTaDZY573P/WtuQ7d/pFDC6P1vFws2K/UyxQ3p3TzvYIxy9zrsjdu9n6N/wDxqSnocH6zdPzMDNz6W2mvp4c61hDQ9zA02Mtq923Zc1rvS3qzgddxs7LGJVXa15xas3c8N27LvoM9r3O9Vv5/5i513QOqYZuqxnMzaMrp9mC7Y1tGw1s/UNzH2n1d0vq9b/txGxMbrfTc+vLp6ccsHp2NiPaLqq9tlQm0S9x3bXe1JTudU+sGD0zKxMXJ3l+a7axzI2sG5tXq37nN21+pYxir5/1gZi59uAzCysu+mtt1hxwxwDHzB99jH/mrJzuh9X6zfn5WSW4AupZjUY7w25xYwDI9t1Vjfs/67/hFXv6d1bJzRm5/S7Mp12HTVdXXlNoLbmbm3brKrW+qx3/baSnax+v4Wbfi044scM3HdlVWEANDGO9N7LPdu9Xd+77EDN6wKM44FeHk5d7a23OGO1roY4lv5z2u+k1Z1GD1zAu6be7GGdZi4dmNaGW11hpfYXVV737d/o07GbmV+9C6hhZ2X1EZuR0x9jLMZlZoZlNrLLGuf9K6p7PV/R/2P0iSnYba62llrq30OeJNNoh7f5NjRu9yBYU+Nubh0sdUcctYG+i5/qFgHtDXXf4X2/nqFhSU17Cq1iPYVWsKSkD0B6M8oDykp//TdpRmFV2lGYUlNqsqxW5U2OVhjklN2tys1uVFjlYY9JTeY9HY9UWPR22JKbrXqYsVNtin6iSmybFBz0H1FB1iSkj3oD3pnWIL3pKWscq1jlJ70B7klI7HKs8or3Ku9ySkbygOKI8oLikp/9SAKK0oKI1JSdjkdjlWajMSU2mPR2PVRiM1JTcbYitsVRqK35pKbQsUvUVYKSSk/qKJsQkxSUydYhPsTO+aG5JSz3qu96m9Bekpg9yA9ym9Bckpg4oRKm5DKSn/2QA4QklNBCEAAAAAAFUAAAABAQAAAA8AQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAAAATAEEAZABvAGIAZQAgAFAAaABvAHQAbwBzAGgAbwBwACAAQwBTADYAAAABADhCSU0EBgAAAAAABwAEAAAAAQEA/+ER+Gh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wOkNyZWF0ZURhdGU9IjIwMTItMDYtMjlUMDk6NDg6MjQrMDM6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMTItMDYtMjlUMTQ6MzQ6MjErMDM6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDEyLTA2LTI5VDE0OjM0OjIxKzAzOjAwIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiBwaG90b3Nob3A6SUNDUHJvZmlsZT0ic1JHQiBJRUM2MTk2Ni0yLjEiIGRjOmZvcm1hdD0iaW1hZ2UvanBlZyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowOTgwMTE3NDA3MjA2ODExOEE2RDkyMTM0QzlDM0Y3MyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowNDgwMTE3NDA3MjA2ODExODA4M0E1QUI0NEQ1MkE1RiIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjA0ODAxMTc0MDcyMDY4MTE4MDgzQTVBQjQ0RDUyQTVGIj4gPHBob3Rvc2hvcDpUZXh0TGF5ZXJzPiA8cmRmOkJhZz4gPHJkZjpsaSBwaG90b3Nob3A6TGF5ZXJOYW1lPSJOTyBJTUFHRSBBVkFJTEFCTEUiIHBob3Rvc2hvcDpMYXllclRleHQ9Ik5PIElNQUdFIEFWQUlMQUJMRSIvPiA8L3JkZjpCYWc+IDwvcGhvdG9zaG9wOlRleHRMYXllcnM+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6MDQ4MDExNzQwNzIwNjgxMTgwODNBNUFCNDRENTJBNUYiIHN0RXZ0OndoZW49IjIwMTItMDYtMjlUMDk6NDg6MjQrMDM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjA1ODAxMTc0MDcyMDY4MTE4QTZEOTIxMzRDOUMzRjczIiBzdEV2dDp3aGVuPSIyMDEyLTA2LTI5VDE0OjMyOjQxKzAzOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDowODgwMTE3NDA3MjA2ODExOEE2RDkyMTM0QzlDM0Y3MyIgc3RFdnQ6d2hlbj0iMjAxMi0wNi0yOVQxNDozNDoyMSswMzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY29udmVydGVkIiBzdEV2dDpwYXJhbWV0ZXJzPSJmcm9tIGFwcGxpY2F0aW9uL3ZuZC5hZG9iZS5waG90b3Nob3AgdG8gaW1hZ2UvanBlZyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iZGVyaXZlZCIgc3RFdnQ6cGFyYW1ldGVycz0iY29udmVydGVkIGZyb20gYXBwbGljYXRpb24vdm5kLmFkb2JlLnBob3Rvc2hvcCB0byBpbWFnZS9qcGVnIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDowOTgwMTE3NDA3MjA2ODExOEE2RDkyMTM0QzlDM0Y3MyIgc3RFdnQ6d2hlbj0iMjAxMi0wNi0yOVQxNDozNDoyMSswMzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDg4MDExNzQwNzIwNjgxMThBNkQ5MjEzNEM5QzNGNzMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDQ4MDExNzQwNzIwNjgxMTgwODNBNUFCNDRENTJBNUYiIHN0UmVmOm9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowNDgwMTE3NDA3MjA2ODExODA4M0E1QUI0NEQ1MkE1RiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/+IMWElDQ19QUk9GSUxFAAEBAAAMSExpbm8CEAAAbW50clJHQiBYWVogB84AAgAJAAYAMQAAYWNzcE1TRlQAAAAASUVDIHNSR0IAAAAAAAAAAAAAAAEAAPbWAAEAAAAA0y1IUCAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARY3BydAAAAVAAAAAzZGVzYwAAAYQAAABsd3RwdAAAAfAAAAAUYmtwdAAAAgQAAAAUclhZWgAAAhgAAAAUZ1hZWgAAAiwAAAAUYlhZWgAAAkAAAAAUZG1uZAAAAlQAAABwZG1kZAAAAsQAAACIdnVlZAAAA0wAAACGdmlldwAAA9QAAAAkbHVtaQAAA/gAAAAUbWVhcwAABAwAAAAkdGVjaAAABDAAAAAMclRSQwAABDwAAAgMZ1RSQwAABDwAAAgMYlRSQwAABDwAAAgMdGV4dAAAAABDb3B5cmlnaHQgKGMpIDE5OTggSGV3bGV0dC1QYWNrYXJkIENvbXBhbnkAAGRlc2MAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAADzUQABAAAAARbMWFlaIAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9kZXNjAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2aWV3AAAAAAATpP4AFF8uABDPFAAD7cwABBMLAANcngAAAAFYWVogAAAAAABMCVYAUAAAAFcf521lYXMAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAKPAAAAAnNpZyAAAAAAQ1JUIGN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANwA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCkAKkArgCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf///+4ADkFkb2JlAGQAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAEHBwcNDA0YEBAYFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgBwgMgAwERAAIRAQMRAf/dAAQAZP/EAaIAAAAHAQEBAQEAAAAAAAAAAAQFAwIGAQAHCAkKCwEAAgIDAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAACAQMDAgQCBgcDBAIGAnMBAgMRBAAFIRIxQVEGE2EicYEUMpGhBxWxQiPBUtHhMxZi8CRygvElQzRTkqKyY3PCNUQnk6OzNhdUZHTD0uIIJoMJChgZhJRFRqS0VtNVKBry4/PE1OT0ZXWFlaW1xdXl9WZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+Ck5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6voRAAICAQIDBQUEBQYECAMDbQEAAhEDBCESMUEFURNhIgZxgZEyobHwFMHR4SNCFVJicvEzJDRDghaSUyWiY7LCB3PSNeJEgxdUkwgJChgZJjZFGidkdFU38qOzwygp0+PzhJSktMTU5PRldYWVpbXF1eX1RlZmdoaWprbG1ub2R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+DlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+v/aAAwDAQACEQMRAD8AD4q7FXYq2MVbGKrgMVbAxVcMVXAYquAxVcBiq8DFVwGKrgMVXAYqvAxVcBiq8DFVwGKrwMVXAYqvAxVeBiq4DFV4GKrgMVXgYquAxVcBiq8DFVwGKrgMVXAYqvAxVsDFVwXFVwXFVwXFVwXFWwuKthcVXBcVb44q3xxVvjiruOKt8cVdxxVvjiruOKu44q7jiruOKtccVdxxVrjirXHFWuOKtFcVaK4qtK4q0VxVaVxVaVxVoriq0jFVpGKrSMVWkYqsIxVaRiq0jFVhGKrSMVWEYqtIxVYRiq0jFVhGKrSMVWEYqtIxVYRiq0jFVhGKrSMVWEYqtIxVaRiq0jFVpGKrSMVWnFVpxVaRiq04q0cVaxV2KuxV/9APirsVdirYxVcMVXDFVwGKrhiq4DFVwGKrgMVXAYqvAxVcBiq4DFV4GKrgMVXgYqvAxVcBiq8DFVwGKrwMVXgYquAxVeBiq4DFV4GKrgMVXgYquAxVeBiq4DFVwGKrgMVXBcVXBcVXBcVbC4quC4q2FxVdxxVvjirfHFW+OKt8cVdxxVvjiruOKu44q7jiruOKu44q1xxV3HFWuOKtccVa44q0VxVaVxVorirRXFVpXFVpXFVpXFVpGKrSMVWkYqtIxVaRiqwjFVpGKrCMVWkYqsIxVaRiqwjFVpGKrCMVWEYqsIxVaRiqwjFVpGKrCMVWkYqsIxVaRiqwjFVpGKrSMVWkYqtOKrSMVWnFVpxVo4qtxV2KuxV//9EPirsVbGKtjFVwGKrhiq4DFVwxVcBiq4DFVwGKrwMVXAYqvAxVcBiq8DFVwGKrwMVXgYquAxVeBiq8DFV4GKrgMVXgYqvAxVcBiq8DFVwGKrwMVXAYqvAxVcBiq4DFVwXFVwGKrguKrguKrguKrguKthcVXBcVb44q3xxVvjiruOKt8cVb44q7jiruOKu44q7jiruOKtccVdxxVrjirXHFWuOKtFcVWlcVaK4qtK4q0VxVaVxVaRiq0jFVpGKrCMVWkYqtIxVYRiq0jFVhGKrSMVWEYqsIxVaRiqwjFVhGKrSMVWEYqsIxVYRiq0jFVhGKrSMVWEYqsIxVaRiq0jFVhGKrSMVWnFVpGKrTiq04q0cVWnFWsVdir//SD4q7FVwxVsYquGKrhiq4YquAxVcBiq8DFVwGKrgMVXgYquAxVeBiq8DFVwGKrwMVXgYqvAxVeBiq4DFV4GKrwMVXgYqvAxVcBiq8DFVwGKrwMVXgYquAxVcBiq8DFVwGKrgMVXBcVXBcVXBcVbC4quC4q2FxVdxxVvjirfHFXccVb44q3xxV3HFXccVdxxV3HFXccVa44q7jirXHFWuOKtccVWlcVaK4q0VxVaVxVaVxVaVxVaRiq0jFVpGKrSMVWEYqtIxVYRiq0jFVhGKrSMVWEYqsIxVYRiq0jFVhGKrCMVWEYqsIxVaRiqwjFVhGKrCMVWkYqsIxVaRiqwjFVpGKrSMVWHFVpGKrTiq0jFVpxVo4qtOKuxV//9MPirYxVsYquGKrhiq4DFVwxVcBiq8DFVwGKrgMVXgYqvAxVcBiq8DFV4GKrgMVXgYqvAxVeBiq8DFV4GKrwMVXAYqvAxVeBiq8DFV4GKrgMVXgYqvAxVcBiq8DFVwGKrgMVXAYqvC4q2FxVcFxVcFxVcFxVsLirfHFV3HFW+OKu44q3xxVvjiruOKu44q7jiruOKu44q1xxV3HFWuOKtccVaK4qtK4q0VxVaVxVoriq0riq0riq0jFVpGKrCMVWkYqtIxVYRiqwjFVpGKrCMVWEYqtIxVYRiqwjFVhGKrCMVWEYqsIxVYRiq0jFVhGKrCMVWEYqsIxVaRiqwjFVpGKrCMVWkYqtIxVYRiq04qtOKrTirRGKrTirWKv/9QPiq4Yq2MVXDFVwxVcMVXDFV4GKrgMVXAYqvAxVcBiq8DFV4GKrwMVXgYquAxVeBiq8DFV4GKrwMVXgYqvAxVeBiq8DFV4GKrwMVXgYquAxVeBiq8DFVwGKrwMVXAYqvAxVcBiq4DFVwGKrguKrguKthcVXBcVbC4q2FxVdxxV3HFW+OKt8cVdxxV3HFXccVdxxV3HFWuOKtccVdxxVrjiq0rirRXFWiuKrSuKrSuKrSMVWkYqtIxVaRiq0jFVhGKrSMVWEYqsIxVaRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVpGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWkYqsIxVYRiq0jFVhGKrTiq04qsIxVacVWnFVpxVo4qtxV//1UBirYxVcMVXDFVwGKrhiq4YqvAxVcBiq8DFVwGKrwMVXgYqvAxVcBiq8DFV4GKrwMVXgYqvAxVeBiq8DFV4GKqgGKrwMVXgYqvAxVcBiq8DFV4GKrwMVXAYqvAxVeBiq4DFVwGKrwMVXAYquC4q2FxVcFxVcFxVsLirYXFW+OKt8cVb44q3xxV3HFXccVdxxV3HFXccVa44q1xxVrjirXHFWiuKtFcVWlcVaK4qtK4qtIxVaRiq0jFVhGKrSMVWEYqtIxVYRiqwjFVpGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVpGKrCMVWkYqsIxVacVWHFVpxVacVWnFWjiq04q//1kBiq4YquGKrhiq4YquGKrwMVXAYqvAxVcBiq8DFV4GKrgMVXgYqvAxVeBiq8DFV4GKrwMVXgYqqAYqvAxVeBiq8DFV4GKrwMVXgYqvAxVeBiq8DFV4GKrwMVXAYqvAxVcBiq8DFVwGKrgMVXAYquC4quC4q2FxVcFxVsLirfHFW+OKt8cVb44q7jiruOKu44q7jiruOKtccVa44q1xxVorirRXFWiuKrSuKrSuKrSMVWkYqtIxVaRiq0jFVhGKrSMVWEYqsIxVYRiqwjFVpGKrCMVUyMVWEYqsIxVYRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVpGKrCMVWnFVpxVYRiq04qtOKrTirRxV//9dEYquGKrhiq4YquGKrhiq8YquAxVeMVXAYqvAxVeBiq8DFV4GKrwMVXgYqvAxVeBiq8DFVQDFV4GKrwMVXgYqvAxVUAxVeBiq8DFV4GKrwMVXgYqvAxVeBiq4DFV4GKrwMVXAYquAxVeBiq4DFVwGKthcVXBcVXBcVb44q3xxVvjirfHFXccVb44q7jiruOKu44q1xxV3HFWuOKtccVaK4q0VxVaVxVoriq0jFVpGKrSMVWkYqsIxVaRiqwjFVpGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWEYqsIxVYRiqmRiqwjFVhGKrCMVWEYqpkYqsIxVYRiqwjFVhGKrCMVWkYqsIxVYcVWkYqsOKrTiqw4qtOKrTiq04qtOKv/0Ehiq4YquGKrhiq4YquGKrxiq4YqvAxVeBiq8DFVwGKrwMVVAMVXgYqvAxVeBiq8DFV4GKqgGKrwMVXgYqvAxVUAxVeBiq8DFV4GKrwMVVAMVXgYqvAxVcBiq8DFV4GKrwMVXAYqvAxVcBiq4DFVwGKrgMVXBcVbC4quC4q3xxVvjirfHFXccVb44q6mKupiruOKtccVdxxVrjirXHFWiuKrSuKtFcVWkYqtIxVaRiq0jFVpGKrCMVWkYqsIxVaRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVMjFVhGKrCMVWEYqpkYqsIxVYRiqwjFVMjFVhGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWkYqsOKrTiqw4qtOKrTiq04qtOKv8A/9FIYquGKrhiq8YquGKrhiq8YquGKrwMVXgYqvAxVeBiq8DFV4GKrwMVXgYqqAYqvAxVeBiqoBiq8DFV4GKqgGKrwMVXgYqqAYqvAxVeBiq8DFV4GKrwMVXgYqvAxVeBiq8DFVwGKrwMVXAYquAxVcBiq4Liq4Liq4LirYXFWwuKt8cVb44q3xxV3HFW+OKu44q1xxV3HFWuOKu44q0VxVaVxVoriq0rirRXFVpGKrSMVWEYqtIxVaRiqwjFVhGKrSMVWEYqsIxVYRiqwjFVMjFVhGKrCMVWEYqpkYqsIxVYRiqmRiqwjFVhGKqZGKrCMVWEYqsIxVTIxVYRiqwjFVhGKrDiq0jFVhxVYcVWnFVhxVacVWnFVpxVacVf/9JMYquGKrhiq4YqvGKrhiq8YqvGKrgMVXgYqvAxVeBiq8DFVQDFV4GKrwMVXgYqqAYqvAxVeBiqoBiq8DFVQDFV4GKrwMVVAMVXgYqvAxVUAxVeBiq8DFV4GKrwMVXgYqvAxVcBiq8DFVwGKrwMVXAYquAxVcBirYGKrguKt8cVbpirfHFW+OKu44q7jiruOKu44q7jirXHFWuOKtFcVaK4qtIxVojFVpGKrSMVWkYqtIxVYRiq0jFVhGKrCMVWEYqsIxVYRiqwjFVhGKrCMVWEYqpkYqsIxVYRiqmRiqwjFVMjFVhGKqZGKrCMVWEYqpkYqsIxVYRiqwjFVhGKrCMVWHFVhxVYcVWHFVpxVYcVWnFVhxVacVWnFX//02DFVwxVcMVXDFV4xVcMVXjFV4xVcBiq8DFV4GKqgxVeBiq8DFV4GKqgGKrwMVXgYqqAYqvUYqqAYqvAxVUAxVeBiqoBiq8DFV4GKqgGKrwMVXgYqqAYqvAxVeBiq8DFV4GKrgMVXgYquAxVeBiq4DFVwGKrgMVXAYq2Biq7jirfHFW+OKu44q3xxV3HFXccVdxxVrjiruOKtccVa44qtIxVojFVpGKrSMVWkYqtIxVaRiqwjFVpGKrCMVWEYqsIxVYRiqwjFVhGKrCMVUyMVWEYqpsMVWEYqsIxVTIxVYRiqmRiqxhiqmRiqxhiqmRiqwjFVhGKqZGKrCMVWEYqsIxVYcVWEYqsOKrDiq04qsOKrDiq04qtOKrTir//1GDFVwxVeMVXDFVwxVeMVXjFV4xVeMVXgYqvAxVeMVXgYqvAxVUAxVeBiqooxVeBiq9RiqooxVUUYqvUYqqKMVXqMVVAMVXgYqqAYqvAxVeBiqoBiq8DFV4GKrwMVXgYqvAxVeBiq8DFVwGKrwMVXAYquAxVcBiq4DFVwGKu2GKuDLiq8AHFW6Yq1yUYq4EHFW6Yq0aDFVhkUYq71EPfFW9jiriMVaIxVaRiq0jFWiMVWEYqtIxVaRiqwjFVpGKrCMVWEYqsIxVYRiqmRiqwjFVhGKrCMVUyMVWEYqpsMVWEYqpkYqsYYqpsMVU2GKrGGKqZGKrGGKrCMVUyMVWEYqpkYqsIxVYcVWEYqsIxVYcVWHFVhxVacVWHFVpxVacVWnFX/9VoxVcMVXDFV4xVcMVXjFV4xVeMVXjFV4xVeBiq8YqvAxVUAxVeBiq8YqqAYqqKMVXqMVVFGKr1GKqijFV6jFVRRiqooxVeoxVUAxVeBiqoBiq8DFV4GKqgGKrwMVXgYqvAxVeBiq8DFVwGKrwMVXAYquAxVcBiq4DFV1MVYn+YnmHUNC0B76wEZuBLGgEqlloxodgVxV5qPzZ85JRylowBBK+m4qPCvPFXsXlzXLTWdLt9QtjWKdQ3E9Vboyn/AClb4TiqavstcVeQ+cvzJ80aX5pv9MshbfVbb0vTMkbM/wAcKOakOP2mPbFWUflj5q1bzDp13PqQiEsFx6SeipUceCtvUtvU4qzaRwq1xV515v8AzWs9NnksNLjF9fRkrK5NIY2HYkbuw7qv/B4qwG7/ADG863L8vr62ynf04YowB9Lh2/4bFW7T8x/Ots3L68t0o/3XNEhH3oEb/hsVZ/5P/NKy1WdLHUI/qN++0e9YpD4Kx+y3+Q3/AATYq9AjcMMVXEYqtIxVojFVpGKrCMVWkYqtIxVYRiqwjFVhGKrCMVWEYqsIxVYRiqwjFVMjFVhGKqbDFVjDFVNhiqmwxVYwxVTYYqsYYqpsMVWMMVU2GKqZGKrCMVUziqwjFVhGKqZxVYcVWHFVhxVYcVWHFVhxVacVWHFVpxVYcVWnFX//1mjFV4xVcMVXDFV4xVeMVXjFVwxVUGKrxiq8YqvGKrwMVVFxVeBiqoMVXgYqqKMVVFGKr1GKqijFVRRiq9RiqooxVUUYqvUYqqKMVXgYqqAYqvAxVUAxVeBiq8DFV4GKqgGKrgMVXgYqvAxVeBiq4DFVwGKrgMVXAYquptirz784h/zqj/8AGeH/AIlirxwfZGKsv/LDzSdI1j9GXL0sNQcemT0jn6A/KT7H+twxV7jy5RVxV8+/mF/ynmrfOH/qHjxVm/5Jf8cnUf8AmL/5lJiqYfmt5rm0nSUsrNyl9qHKNHXZkiAHqOPBtwq/824q8c0/T7m8vINPsk53VwwSMdvEsT/Ko+JsVetaL+TmhR26nUzJfXBFZDzaKMHwUIVan+s2Krdc/J3RXt2bSmeyuVFUq7SRk+DByzf8C2KvJr2yubW6msrtDFdWzlJF8GHQg+B6qcVez/ld5ql1nRzb3b87+wIimY9XQj93IfdgOLf5S4qzylRirRGKrSMVWkYqsIxVaRiqwjFVpGKrCMVWEYqsIxVYRiqmRiqwjFVjDFVMjFVjDFVNhiqxhiqmwxVTYYqsYYqpsMVU2GKrGGKqbDFVNhiqwjFVMjFVhxVTbFVhGKqZxVYcVWHFVhxVYcVWHFVhxVYcVWnFVhxVacVWnFX/16GKrhiq8YquGKrxiq8YquGKrxiq8YqqDFV4xVeMVVBiq9cVVBiq9RiqooxVeoxVUUYqqKMVXqMVVFGKqijFVRRiq9RiqooxVUUYqvUYqqKMVXqMVVAMVXgYqqAYqvAxVeBiq8DFV4GKrwMVXAYqvAxVcBiq4DFVwGKrqbYq89/OQf8AOpv/AMZ4f+JYq8k0W3iu9YsLOUViuJVjcDY0Y0xVZrWk3Omalcabc19SE1jfpzQ7o4+YxV7L+XHm/wDTei+lcvXUrKkV0D1cU+CT/Zjr/l8sVeY/mEa+fNW+cP8A1Dx4qzb8lP8Ajjaj/wAxZ/5NJirFfzTvGufOssJaq2UEUQXsCw9U/fzGKpn+TGnJca1qF861NtGkURPjKSWp70j/AOGxV7ZHGAoxVqWMFTtirw/84tOjtvMVneIOP1yFkendoWG//AuoxVC/lNePb+cGgDUju7d1K+LIQ6n6AGxV7vHuuKtkYqtIxVYRiq0jFVpGKrCMVWEYqsIxVYRiqwjFVhGKrCMVUyMVWMMVU2GKrGGKqbDFVNhiqxhiqmwxVTYYqsYYqpsMVU2GKqbDFVjYqpsMVWMMVUziqxsVUyMVWHFVM4qsOKrDiqw4qsOKrDiqw4qsOKrTiq04qsOKv//QoYqvGKrhiq8YquGKrxiq8YqvGKrxiq8YqqDFV4xVUGKr1GKqi4qvGKqijFVRRiq9RiqooxVUUYqqKMVXqMVVFGKqijFVRRiq9RiqooxVUUYqvAxVUAxVeoxVUAxVeBiq8DFV4GKrwMVXgYqvAxVeBiq4DFVwGKrgMVXU2xV55+co/wCdTf8A4zw/8SxV5R5XFfNOjDxuov8AiQxV6f8Amh5PbUtJGp2iVv8AT1LUA3kh6unzX7a/7L+fFXlvlrX59E1eDUoKtH9i6iH7cTfaHzH2l/ysVRfnW4iuvNt/dQsHhnWB43HQqbeOhxVnv5Jiui6j/wAxZ/5NJirDvzKt3h8937N0uEhkT5CJY/1ocVZJ+SFwi3er2zbOfQlXfcgc1b7vhxV7KgqoxVqTZTirxX87LhH1XSbcEc4o5pGHekjIB/ybbFUl/K2BpvPEDL0ggmkf5EBP1uMVe/xL8GKtkYqtIxVaRiq0jFVhGKrCMVWkYqsIxVYRiqmRiqwjFVjDFVMjFVhGKqbDFVNhiqxhiqmwxVTYYqsYYqpsMVU2GKqbDFVjDFVNhiqmwxVTbFVjYqpsMVWNiqmcVWHFVM4qsOKrDiqw4qsOKrDiqw4qsOKrTiqw4qtOKv8A/9HDFVwxVeMVXDFV4xVeMVXjFV4xVeMVXjFVQYqvGKqgxVeuKqi4qvXFVRRiqouKqi4qvUYqqKMVVFGKqijFVRRiq9RiqooxVUUYqqKMVXqMVVFGKqijFV4GKqijFV4GKrwMVXgYqvAxVeBiq8DFV4GKrgMVXgYquAxVum2KvPPznH/OpP8A8xEP/EsVeT+Vf+Ur0X/mLi/4kMVfSiwh4enbFXgH5jeVD5f15pYEppmoFpIKDaOTq8f48k/yf9TFWM+/tir1r8jVro2o/wDMYf8Ak0mKoX87PL0gFpr8KVWEfVrwgdEY1jY+wcsv+zXFWA+V/MEvl/XINUjUyRUMd1EvVompyA/ygQHX/KXFX0HofmfSdWtFubC5SeMjfifiX2ZT8St7MMVWa/5p0jSLRri/uUhQD4VJ+Nj4Io+Jj8sVfPnmPXZ9c1q51WYFEei28R/YiX7K/P8Aab/KbFXon5K+XpEgutdnShu6Q2tRv6SGrt8nfb/nnir1kLQYq0Riq0jFVpGKrCMVWkYqsIxVYRiqwjFVhGKqZGKrGGKrCMVU2GKrCMVU2GKqbDFVNhiqxhiqmwxVTYYqpsMVU2GKrGGKqbDFVNhiqm2KqbYqsbFVNsVWNiqmcVU2xVYcVWHFVM4qsOKrDiqw4qsOKrTiqw4qtOKrDir/AP/SwxVeMVXDFV4xVeMVXDFV4xVUGKrxiq8YqvGKqi4qvXFVQYqqLiq9cVVFxVUXFVRcVXriqouKqijFVRRiqooxVUUYqqKMVXqMVVFGKqijFVRRiq9RiqoBiq9RiqoBiq8DFV4GKqgGKrwMVXAYqvAxVeBiq4DFVwGKrqYq8/8Azktp5fKMggieZ/XhPCNSzU5eAriryfylY6ifNmjFrKdEW7iLO0TgAcupJGKvpiBKJTFUh86eVbfzBos9hLRXYcreU9Y5V3Rv4N/kYq+dJdL1q2mltp7C4E0LNHIBE5HJTQ0IFCPfFXrn5GWl1Fo2oevBJAzXhKrIrISPSTcBgMVej6hptve2kltcRiWCZSksbCoZWFCDirwrzd+VOuaPNJPpEb6jphqREvxXEY8Cv+7B4FPi/wAnFWCPxhnKyK0FwmxVgUdT8jQjFW4ws03GJXnnfoqgu5+gVOKs88o/lRrOqzx3GtRtYacKMbc/DPKPCn+6l/m5fH/xLFXuNjYwWtvHBCixwxKEjjUUVVUUAAHYDFUSRiq0jFVhGKrSMVWkYqsIxVYRiqwjFVhGKrCMVUyMVWMMVUyMVWMMVU2GKqbDFVjDFVNhiqmwxVTYYqpsMVWNiqmwxVTbFVNsVU2xVTbFVjYqptiqm2KqbYqsbFVM4qsbFVhxVTOKrDiqw4qsOKrDiqw4qtOKrDiq04q//9Oxiq4YqvGKrhiq8YqvGKrxiq8YqvGKqgxVeMVVFxVeMVVFxVeuKqi4qqLiqouKqi4qvXFVRcVVFGKqi4qqKMVVFxVUXFVRRiqooxVeoxVUUYqqKMVXqMVVFGKqgGKrwMVXgYqvAxVUAxVcBiq8DFV4GKrgMVXgYquAxVpog2KtLbKDWmKqyrTFXMgI3xVTNohNaYqvSAL0GKqnHFVN4FbtiqDudHs7jaeGOUDoHUN+vFWrfSLO32hhSIHqEUL+rFUUsCr2xVdTFVpGKrSMVWkYqsIxVaRiqwjFVhGKrCMVWEYqpkYqsIxVTYYqsYYqpsMVWMMVU2GKqbDFVNhiqmwxVY2KqbDFVNsVU2xVTbFVNsVU2xVTbFVNsVWNiqm2KqbYqsbFVNsVWHFVM4qsOKrDiqmcVWHFVpxVYcVWHFVhxVacVf/UsYquGKrxiq8YqvGKrxiq8YqvGKrxiq8YqqDFV64qqLiq9cVVFxVUXFVRcVVFxVUXFV64qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qvUYqqKMVVFGKr1xVUUYqvAxVUAxVeBiq8DFV4GKrwMVXgYqvAxVcBiq4DFVwGKrgMVbAxVumKt0xVumKupirVMVdTFWiMVWkYqtIxVaRiq0jFVpGKrCMVWkYqsIxVYRiqmRiqwjFVhGKqZGKrGxVTYYqsYYqpsMVU2xVTYYqptiqxsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVY2KqbYqptiqw4qptiqw4qpnFVhxVYcVWHFVhxVYcVWHFVpxVYcVf/VsYqvGKrhiq8YqvGKrxiq8YqvGKrxiqoMVXriqoMVVFxVeuKqi4qqLiq9cVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVeuKqi4qqLiq9RiqoMVXgYqqAYqvAxVeMVXgYqvGKrhiq8DFVwGKrgMVXAYquAxVsDFW6Yq6mKupirqYq0RirRGKrTTFVppiq0jFVpGKrTiqw4qsOKrDiqw4qsIxVYRiqmRiqxsVU2xVY2KqbDFVNsVU2xVTbFVNsVWNiqm2KqbYqptiqm2KqbYqptiqm2KqbYqptiqm2KqbYqsOKqbYqptiqw4qpnFVhxVYcVUziqw4qsOKrDiq04qsOKrTir/AP/WcMVXDFV4xVcMVXjFV4xVeMVVBiq8YqvGKqgxVeuKqi4qqLiq9cVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVXriqouKqi4qvXFVQYqvAxVUGKpfruu2GiabNqN+5jtIOPqOqs5HNwi/CoJ+0wxVjMf5weS2dV+tyVchV/cS9SaD9nFWciUca4qwyf83/JkU8kLXcnOJmRx6Epoymh344qr6Z+bHk++v7exhu3+sXLrFCrRSKC7GgHJlAFTirOImDDFV7GgxViXmD8zPK2hai2nahdMl2qq7IkUj0DdKlVIxVLh+dfkVd2vJQB/xRN/zTirPNOvYb20huoDyhnRZYyRSquAwND7HFUXiqR+Z/NujeXLJLzVZzDA8giQqrOS5BIAVQT0U4qxhfzs8h8hyvZFBNKmCan/ABHFXoEEqyKGU1B3BGKqlMVWsQBviqVaz5g0vSbZrnULqO1gG3ORgKnwA6sf8lcVYDffnp5Xicraw3d4O0kcaoh/5GMjf8JirVh+efleaQJcxXVkP9+SRh0/5Js7f8JirO9J13TdVtVurC4jubd+kkbBhXwPgfY4qjyRSuKsX80effL/AJduIbfU52iluFLxKsbvVVNDugNMVSM/nH5L/wCWqT/kRL/zTiq0/nF5M/5apP8AkRL/AM04qtP5w+Tf+WqT/kRL/wA04qmnl3z75f8AMF3JaabO8k8Seo6tG6ALUL1YDucVZJ1GKrDiqm2KrGxVTbFVjYqptiqm2KqbYqptiqm2KqbYqptiqm2KqbYqptiqm2KqbYqptiqm2KrGxVTbFVNsVU2xVTbFVjYqpnFVjYqpnFVhxVYcVWHFVhxVYcVWHFVhxVacVf/XcMVXDFV4xVeMVXjFV4xVeMVXjFV4xVUXFV4xVUXFV64qqLiqouKqi4qqLiqouKr1xVUXFVRcVVFxVUXFVVcVVFxVUXFVRcVVFxVeuKqi4qqLiqouKqi4qvGKqi4qvXFVQYqwn82/+UH1Edq2/wD1ER4q8NRRzg2/bT/iQxV9Qqx9H6MVfMl2oOpXu3/HxL/xM4qpMXiZJoTwmhYSRuOoZTUH78VfT/lXWI9W0az1CPZbqJZOP8rEfEv+xaq4qmd3KscbMxAVRUk9AMVfLeu6p+mdf1DVW3W6mZoq9fTX4Yx/yLVcVS64VfSbbtir6n8m1PlzS/8AmEg/5NLiqeSnipxV8/fnVrv1/wAywaVG1YNNTlKO3rTAGn+xj4f8G2KvP5I0ZCKYq+iPyj8wHVvKNp6jcrmy/wBEn3qaxABCf9aMocVZ72rirHfOXmiz8u6Lcalc/EIxxiiBo0kjbIi/M/8AAr8WKvmnX9e1PXdQfUtVlMkhJ9KKp9OJT+xGp+yP+JftYqnWiflr5w1i2W5gto7S3kFYnumMfIeIVVd6fNcVW6/+XPm3RLZrq6gjubWMVlmtWLhB4srKj08W44qlflzzHqXl3UV1HTn+E0FzbE/u5U/lYeP8rfs4q+k9D1i11bSbbULVuUFzGJEr1FeoP+Up+FsVeP8A54b6/pld6QSf8TGKsG07SdQ1S7+p6bb/AFi5CmT0+SJ8IIBNXKjviqbf8q589/8AVo/5L2//AFUxV3/KuvPf/VoP/I+3/wCqmKsw/Kzyh5m0jXbu61Sx+qwSW/pxv6kT1bmppSN2PQYq9WA2xVacVU2xVY2KqbYqptiqxsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVY2KqbYqptiqxsVUziqw4qpnFVhxVYcVWHFVhxVYcVWnFVhxV//0HDFV4xVcMVXjFV4xVeMVXjFV4xVUGKrxiq9cVVFxVUXFV64qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qvXFVRcVVFxVeMVVBirCPzc/5QjUfnB/1ER4q8OX7cH+un/Ehir6fX+4+jFXzNeGmoXx/5eJf+JnFV95aPbxWkjVMd5D60Z/2TIw+9cVer/kbrXqaVd6TI3x2MvqRD/iqerf8AJwP/AMFirJfzS139F+T76RG4z3K/VYPHlN8JI91Tm/8AscVeB6Np73t9DZp+0GdyOyRqXY/8CuKoOVuUBPtir6o8mD/nXNL/AOYSD/k0uKo3XNRg0/T7i8uG4wW0bSyt4KgLH9WKvlDUNRuL67u9SuP7+8leZx1oXNaD2X7IxVk/nXyi+g6VoVyUKtPD6V8fC4P73f8A4J1/1Y8VTb8k9d+oeZ59KkakOpx8ogf9/Q1YU/1oy/8AwK4q+gQ1Y64q8I/PXWHuNcsNHUn0baI3Uo7GSQlFr7qqN/yMxVIPyw8uw655sjFyvO009PrMiHozggRqf9kef+wxV9HQ2yKgqMVUbmGFlKkAg7EYq8Y1P8kL59QupLHUYYbKWVnggZGJjRjUJsf2egxVn/5f+Wb3y55eXS7u5S6aOWR43jBUBJDy40P+UWxV5x+d/wDx39M/4wSf8TGKpP8AldPDB5uMs8ixx/VZBychRUsnc4q9pXW9GC/71wf8jE/rirv03o1f964P+Rif1xVGWtza3Cc4JElStOSMGFR7jFVc4qsOKqbYqsbFVM4qptiqm2KqbYqsbFVNsVU2xVTbFVNsVU2xVSbFVNsVU2xVTbFVNsVU2xVTbFVjYqptiqm2KqZxVTbFVjYqpnFVhxVYcVUziqw4qsOKrDiqw4qtOKrDir//0XDFV4xVcMVXjFV4xVeMVXjFV4xVUGKrxiqouKrxiqoMVVFxVeuKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qvXFVQYqvGKsI/Nv/lCNR+dv/1ER4q8OX7cH+un/Ehir6gX+4+jFXzHf/733/8AzES/8TOKsu1jSGuPyw0bVEFXsGb1D/xVLIUP/D+niqA/LjWf0V5xs3LcYL2tpL4fvKcP+SgTFWRfnVrZuL/T9IRvggU3U47cnqkf/AqH/wCDxVB/lho5mttb1h1+CKB7WBv8opzk+4en/wAFirAB/vJ9GKvqvyaaeW9L/wCYSD/k0uKsJ/PPzAbXQItKiak+qScXA6+jEQz/AHt6a/6rNiryzyLov6X826daMK28L/Wbnw4Q/FQ+zNxT/ZYq9p/MzQ01bydewxjlcW6i5t6bnnD8VB7snJP9lir5+sNQuLC8tNSt/wC/s5UmQVpUoa0Ps3Q4q+rtK1CC/wBMt7y3blBcxJLE3irqGH4HFXz5+bZf/lYF5yrQRQcK+HAdPprirIvyDCfX9cJ+3xtePyrLX+GKvbJCRHtir5/1782vOlt5g1Wyhmt1gtLy4ghBiBPCKVkWpruaDFUD/wAra88nf1rc/wDPEf1xV6B+VPnHXPMMOp/pVo2a1eJYfTThs4Ymu58MVYp+d/8Ax3tM/wCMEn/Exirz5baa5f0oIHuJKV9ONS7UHeig4qu/QWrf9Wq7/wCkeX/mnFXHQdWp/wAcq7/6R5f+acVeyfk1ZXdp5WkjubeS2c3UrCOVGjahVd6MAcVZ+cVWNiqw4qptiqm2KrGxVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVjYqpnFVhxVTbFVhxVYcVUziqw4qsOKrTiqw4qsOKv/9JwxVeMVXDFV4xVeMVXjFV4xVUGKrxiq8YqqLiq8YqqLiqoMVVFxVeuKqi4qqLiqouKqi4qqLiqouKqq4qqLiqouKqi4qqLiqouKqi4qqLiq9cVVFxVUXFVRcVVBiq9cVXjFWE/m5/yhGo/OD/qIjxV4av24P8AXT/iQxV9QL/c/Rir5jv/APe+/wD+Yib/AImcVe0+SdLh1P8ALW1sZv7q5t5omPcBncVHyxV4m0Nza3DwSVju7SUo1OqyRtTb5MMVRXmDVptU1O51OccXmpRa1CqqhQB92Kvb/LOgnR/y9W1deM7Wkk1xXr6kql2B/wBWvD/Y4q+fh/vJ9GKvqjym/Hyzpn/MJB/yaXFXg/5na7+mPOl2Vblbaf8A6HD4VjJ9Q/8AIwsP9jirHLTWL3TpmlsLuS0mdeLPE3Fita0qMVRf+NvNJ2bW7th4GVsVS1CjJxU1GKvcfyO183nlyXSpWrPpcnFQevoy1dD/AMF6if7HFWLfnppMlv5hsdWVf3F3D9XdgNhJExYVP+Uj7f6mKpP+VfmGLRfNqJcNwtdRT6uznoslQYyfpqn+zxV9FJMjpSuKsP1P8tfJFxcz3kumRtcXEjzTSc5AWd2LM2zU3Y4q8Q84R6PD5mu7XSIlis7WkNEJYGRR+8NWJ6N8H+xxVnP5Ff3etf8AGSH/AIi+Kpf+d3/He0v/AJh5P+JjFUs/KYA+cyD0+qS/8TTFXuqRQ8R0xVxjhHhiq5eA6YquOKrDiqmcVU2xVTbFVhxVTbFVNsVU2xVTOKqZxVTbFVNsVU2xVTbFVM4qpnFVNsVU2xVTOKqZxVTbFVNsVUziqxsVU2xVTOKrDiqmcVWHFVhxVTOKrDiqw4qtOKrDiqw4q//TcMVXjFVwxVeMVXjFV4xVeMVVBiq8YqvGKqi4qvGKqi4qqDFVRcVXriqouKqi4qqLiqouKqi4qqLiqquKqi4qqLiqouKqi4qqLiqouKqi4qvXFVRcVVFxVUGKqgxVeuKrxirCfzcH/Oj6ifeD/qJjxV4YhHODf9tP+JDFX1Eqn0cVfMV+R9fv9/8Aj4m/4mcVe9/lYnLyLpf/ABjf/k62KvMPzY0Y6Z5we5UcbfU0Eynt6i0SQfgr/wCzxVJvKGj/AKb81adpxHKEyCW57j0ovjYH/Wpw/wBlir6M1qOmi3n/ADDy/wDEDir5VDD6p17Yq+irjXhoX5bxakSA8Gnw+iD0MrxqsY/4NlxV88AyMoArJPK23dmdj+sk4q+nfKvkrTdP0OxsprWGWaCFVmkZFYtJSrmpHdicVTk+WdHp/vDB/wAik/pirxv88fLcOn3Gm6tawrDDJytLgIoVeQq8ZoNqkep/wOKsc/K3XRpHnW1DPxt9SH1SXfbk5BiP/IwKv+yxV7n5y8qWvmXQJtOnPBmo9vMBUxyr9lwPwb+ZeWKvmrWdG1LRr6TTdWgMFynQndHXs6N+0p/z+LFWR6H+afnDSbdLYSxX9ugon1oM0ijwEisrH/Z88VX61+bPnDUrd7cPDYROOLNbqwkoevxuzcf9iFbFWKpZXKacuoekRZPL6CTno0lCxA8aAYq9P/InePWiP9+Qf8RfFUu/O/bXtLr/AMs8n/ExirCNK1jUNIvTe6dKIrgoY+RUN8LEE7NXwxVOv+Vneef+W+P/AJEx/wBMVd/ys7zz/wAt8f8AyJj/AKYqyf8ALfzz5n1fzMbDUrlJrYW7yBVjRDyVkANVAP7RxV60p+HFWjiqmcVU2xVY2KqZxVTbFVNsVUziqmcVUziqmcVU2xVTbFVNsVUziqmcVUziqmcVUziqmcVU2xVTbFVM4qsbFVNsVUziqw4qpnFVhxVYcVUziqw4qsOKrTiqw4qsOKv/1HDFV4xVcMVXjFV4xVeMVXjFV4xVUGKrxiqouKrxiqouKqi4qvXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVVxVUXFVRcVVFxVeuKqi4qqLiqoMVVFxVeuKqi4qvGKpd5g0Cx1zS5tNvgzWs/D1AjFW+Bw4oR/lKMVYrF+TPk9XRvTnqhDL++bqDUYqz9YfhpirBp/yZ8nzTyzPHPzmdpHpMwFWNT+vFWY6BoVlo2lwabZBltrcFYg55NQktuT7nFUD5r8j6J5mjt01ON2+rMzQtGxRhyFGFR2NBiqH8rflt5c8u38l9p8UguZIzEzySF6ISGIFelSoxVlN1Zx3FtJBIPglRkemxowocVYAPyK8kU4+lccfD12xVketeQ9G1jRLbRrwS/UbX0/TSOQoT6S8E5MN2oMVSfTvyX8m2V/bXsUMzTWsizRB5WZeaHktR33GKvQokCCmKquKpJ5o8raX5j0x9O1KMvbOyv8DFWDIaghh0xVh0f5GeSUkSRUuVeNgyMJ3BDKagg+2KvRVjAWmKpPr/lfRtbtjbanaR3UW5UON1J7owoyH3U4q8+vvyF8vvIWs7y7tQf918kkUfLkvL73xVfpf5FeWreUSXs1zf039KRhHGfmIwG/4fFWU635E0LV9Ji0q6t+FlAyvDHCfT4FAVHHj02Y4q7yr5G0XyylwmlpIouirS+o5epQEClenXFUN5r/AC+0HzJcQXGpJK0tuhSIxyFBRjU1piqQn8lfKA/YuP8Akc2KrD+S/lH+S4/5HNiq0/kz5S/kuP8Akc2Kpn5d/Ljy/oOonULFJRcmNoiXkLjixBOx/wBXFWV0oMVWHFVhxVTbFVNsVWNiqmcVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVM4qptiqmcVU2xVY2KqZxVYcVU2xVYcVWHFVM4qsOKrDiq04qsOKrDir/AP/VcMVXjFVwxVeMVXjFV4xVeMVXjFVQYqvGKqi4qvXFVQYqvXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVXriqouKrxiqoMVXjFV4xVUGKrxiq8YquGKrxiq4YqvGKrhiq4Yq2DirdcVdUYq0cVaJxVacVWkDFVhpiq04qtOKrDiqw0xVYaYqsNMVWHFVM4qsbFVNsVWNiqm2KqbYqptiqxsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVJsVWNiqk2KrGxVTbFVNsVU2xVTbFVM4qsbFVNsVUziqxsVWHFVM4qsOKrDiqw4qtOKrDiqw4q//9ZwxVcMVXjFV4xVeMVXjFV4xVeuKrxiqouKrxiqoMVVFxVeuKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qvXFVRcVVFxVUXFV64qqLiq8YqqDFV4xVeMVXjFV4xVcMVXg4quGKrwcVbBxVcDirdcVbrirq4q1XFWicVaJxVaTiq04qsJxVacVWHFVhxVYcVWHFVhxVYcVUziqxsVU2xVTbFVjYqptiqm2KqbYqptiqm2KqbYqptiqm2KqbYqptiqm2KqbYqptiqm2KqbYqptiqm2KrGxVTbFVNsVWNiqmcVWHFVM4qsOKrDiqw4qsOKrDiq04qsOKv//XcMVXDFV4xVeMVXDFV4xVUGKrxiq8YqvGKqgxVeuKqi4qqLiqouKr1xVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVRcVXcwMVcLhQaVxVXjkDdMVVOYGKuFwtaVxVXjcHFVUHFXGVV64q2lwp74qiFYEVxVxnVepxVtLlSeuKq6sDiq+tMVWtcKuKr0nVumKrmmCjFWluVJpXFVZWriq6uKurirVcVU3lC9cVUvrSVpXFV6yBhtiricVUpJQuKqDXaeOKrDeJ4jFVv1tD3xVsSq2KtMwAxVQe4UGlcVaEobFWicVWNiqjJIFxVDtdKO+KqbXSeOKqbXSeOKrfrCnvirfKuKrGxVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVWNiqm2KqbYqsbFVM4qsOKrDiqw4qsOKrDiqw4qtOKrDir//QcMVXDFV4xVcMVXjFV4xVeMVXjFVRcVXjFV64qqLiqoMVXriqouKqi4qqLiqouKqi4qvXFVRcVVFxVUXFVRcVVFxVUXFVRcVVFxVUXFVQHFWOeedcbSvL91PG3Gd19G3I6+pJ8II/1ft/7HFXji3+scBXUbqv/GeT/mrFXpv5Ua/Pd6dPp93M01zZyVV5GLO0UlStSxJPFg4/4HFWdajB9ZsZ7fm0YmjaMyISGXkpFVIoQRXbFXzy97r8Mk1vNqd2J4HaKT9/L9pCVP7XiMVe0/llrkmp+WLUzSGS5tq207MeTFo/slidyShQ4qzMtRa4q8m/OHzFeJd2GlWVzJbsA1zcNC7IxBqkYqpG20m2Ksf/AC9Otan5xs4pNRu5La25XNwjTyFSsf2QwLUIMhSo/lxV78HpHXFXif5r+aNQn8ypp1jeTW8NhHSb0JHj5Sy0YhuBHLinCn8vx4qxSw8za9puo2t/+kLqZLaVJJIXmkZXRT8SlS1DyXbFX0zpl7Fd20VxCweKZFeNx0KsKgj6MVRjtRcVeDfm9qmrRedFitr+5t4RaRH0oZpI1qXep4qQK4qmP5I6nqc+t6nHd3s90iwRlFmleQKeZ3AYmmKsm/Oq/vbbynG9pcS20pu4l9SF2jahV6iqkGmKvJfLmta9/ifRFfVLx43v7VZEa4lKspmUFWBahBGKvqG2eqDFVauKurirTNQYq8y/OjzPPpugR2NnO0F7qMoRJI2KOsUZDyMpBBH7Cf7PFXizav5g4/8AHWva+P1iX/mrFXvn5W+ZH1nypaSTyGS7tR9VumY1YvFsGYncs6cHP+tirNSdsVeX/njfX1t5csmtLmW2ka/jVnhdo2K+jKeJKkGlRirx46vrgFTqt5/0kS/81YqpnXtW/wCrtd/9JEv/ADViqpDr+vI3KHWLxW9riX8fixVmvkr80tZg1K30/XJRdWly4iS7ICyRs2y8uIAZK9a/Ev2uWKvZHlrHXFXzjr2q61/iPWFXUrpUS+uVRFnkCqomYAABtgMVen/lBeXdx5aka6nkuJBdSASSuztQKtBViTirPidsVWMcVYX+aFzcQeT797eV4ZQYeMkbFGFZ4waMtD0xV42NR1fiCdRuv+R8n/NWKrDqupDrqVz/AMj5P+asVd+ldR/6uVz/AMjpP64qradqupfpbT1/SNywa5hDKZpCCDIAQRXFXvURJXfFW2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVY2KqbYqptiqm2KrDiqmcVWHFVhxVYcVUziqw4qtOKrDiqw4qtOKv/9Gxiq8YquGKrxiq8YqvGKrxiq8YqvGKqgxVeMVXjFVRcVVFxVeuKqi4qqLiqouKqi4qqLiqouKqi4qqLiqouKqi4qvXFVRcVVFxVUXFVzNRcVeT/mjqv1nVbbTUNY7VfWmH/Fj7KD/qp/xPFWLRWdxLptzqCD9xaukbn/XqPwPH/gsVTPyRq36M80WkhakF3/o03h+8I4H/AIMLir3ZCHixV4l+YumfUPNksiikN+izr4c/sOPvXl/s8VTf8oNUNvq95pjtRLlBPED/ADxni1PmrD/gMVewSSgRVxV88eatUOqeZtRva1iEhhg7j04vgBH+tTn/ALLFXoH5LaRxsrzVpF+K6kEUJP8AvuLqR83Yj/YYq9F1nUYNO024vJzSG2jaV/kgqcVfNFzeT3dzPfT/ABXF3I0r03+J2rQfftiqJ1nS7nTL97C6H71UR/Y81DbfI/Dir2P8m9eN75bWykas+muYDXr6Z+KI/LieH+wxV6K7VjxV8/8A5wf8puP+YSL/AIm+Kpl+R/8Ax39U/wCMEf8AxM4qyb87z/zqcX/MZF/xF8VeR+Xf+Uo0L/toWv8AyfTFX1RaH4BiqIrirq4qozycUOKvnD80Nc/S3nG5VG5W2mj6rFTpzXeU/PmeH+wxVIZtMuotGtdWcf6LdzSQxn3jpv8A7I8x/wA88VZl+TGufUvMNxpUjUh1BPUhB/39EKkD/Wj5f8Bir3hHqmKvLPz2/wCUdsf+Y+P/AJMzYq8u8tKr+Z9IjdQyPdRBlIqCC3cYq+g4tC0cpU2kH/ItP6YqxD8x/KuhP5fvbxLaKC7tYzLFPGqo1U34kinIN9mjYq8Un5NAAoJc7KB1qelMVfTsXqC0T1Pt8Ry+dN8VfOmuf8pHrX/Mdc/8nmxV6h+Tf/KNy/8AMVJ/xFMVeh12xVYxxVhH5q/8odf/ADg/5Px4q8YB/uhSoLKCPpxV7bD5U8tMtW0uzJ94Iv8AmnFVx8peWP8Aq1Wf/IiL/mnFW4vKvluORJE0u0WRCGRxBGCGBqCCF2IxVNwKDFVrHFVNsVU2xVTbFVNsVU2xVTbFVNsVU2xVTbFVNsVWNiqm2KqbYqptiqxsVU2xVTOKrDiqw4qpnFVhxVYcVWHFVhxVacVWHFVpxV//0rGKrhiq8YquGKrxiq8YqvGKrxiqoMVXjFV4xVUXFV4xVUXFVRcVXriqouKqi4qqLiqouKqi4qqLiqouKqi4qvU4qqLiqopxVUU4qqKcVQ9/dRW9vJNKwSONS7segVRUnFXgl/fyX15d6jLs9zI0gB7Kfsr/ALFaLir0zy75YQ+SvqMw4y30TSSk9Q8oqpPug4f8Diryxo5VRo3BSaFirDoVZTQ/cRir3zydrI1bQrS8J/eSJSYeEi/C/wDwwOKsa/N7SvV0eDUUH7yxlHI/8Vy0Vv8Ah/TxV5zoep/ovXNP1GtEhlHqn/it/gf/AIRjir2rzdrn6N8s3t4rUlWIrAf+LJPgT/hmBxV4IFKQBEBLtQKo6knoMVfR3lLSV0nQbKxoOUESrIR0LkVc/S5Y4qxH85dc9HR4dKiakuoSfvAOvoxEM33vwH/BYq8+8j6R+lfNdjbkVgtz9Zn/ANWLda/OTguKsq/OPRvSfT9XjG29rOfnV4/+ZmKpN+Vut/ovzdHC7Ut9TX0G8PUHxRn76oP+MmKvoJZOUWKvBPze/wCU3H/MJF/xN8VTH8kP+O9qf/GCP/iZxVkv53H/AJ1OL/mLi/4i+KvJfL3/ACk+h/8AbQtf+T6Yq+pbQ/AMVRFcVdXFWPec9ej0bQL7UWIrbxExKf2pD8Ma/wCycqMVfMSrcTsqLWW6unAH8zySN+tmOKvbPNXk+Mfl02mQDlLp0CSwEdWkgHJyPeQcx/s8VeM6fqM1he2mp2/99aSJKo6V4mpU+zD4cVfUWk6hBe2MF1A3OG4jWWJvFXHIH7jirzv89T/zrtj/AMx8f/JmbFXklneTWN7bX0IUzWzrLGHBKllNRWhB/HFWVj84/N67CGwp/wAY5v8AqriqVa9578x6/bG1vJIorViDJDbqVD0NRyLM7de3LFUZ+Xmk6Fea5BJqN4gngcNa6ewI9R13BLMOLcftemuKvc3I9HbFXzfrn/KRaz/zHXP/ACebFXqH5Nn/AJ1uT/mLk/4imKvQ67YqsY4qwf8ANT/lD7/5wf8AJ+PFXjYB4qR1WhHzGKshH5h+a12WaED/AIxj+uKu/wCViebf9/w/8ix/XFU98j+c9f1TXzZX0kbweg8gCJxPJWUDevvir0cGoxVaxxVTY4qptiqmxxVTbFVNsVU2xVTbFVNsVU2xVY2KqbYqptiqm2KrGxVTbFVNsVWHFVM4qsOKrDiqw4qsOKrDiqw4qsOKrTiqw4q//9Oxiq4YquGKrxiq8YqvGKrxiq8YqvGKrxiqoMVXjFVRcVXriqouKqi4qvXFVRcVVFxVUXFVRcVVFxVUXFV6nFVRTiqopxVUU4qqKcVXg7Yqwr8zdXNton1ONqTXzekAOvpj4nP3fB/s8VeYosStF6qc4lZTJGDQsoO4r7jFWcL+agVeI0x6f8ZR/wA04qxDU7uK+1O5vYoTAly3MxE8qMR8RrQdW+LFWa/lHq5iuLzSJG2NLm3B+hZB/wAQP/BYq9E13TotS0m5spPsXETRk+BYUB+g74q+eTE4R4JRSWMlHXwZTQj78VZR5j8xvqPlfRbEuWlA5XfjWAGNa/6+74qhvI2lfpPzbYwkcobY/Wpu+0VCv/JQpir6AUhIsVeBefNY/S3my7kVuVvaf6LB4fuyeZ+mQt/seOKrvJ/mu38tzXU5smup7gKoYOE4otTTcH7ROKpr5k/Mq317R59Nl0x4vVAKSGUHi6sGU04juMVYTzlhaOeFuM0DLJE46hkNVP3jFX0r5b1mLVtEtNQj+zcxK5X+ViPiX/YtVcVeOfm7/wApsP8AmEi/4m+Kph+SX/He1P8A4wR/8TOKsl/Ow/8AOqRf8xcX/EXxV5N5f/5SbQ/+Y+1/5PLir6jtWHAYqiOQxVa7gLirxv8APHXeRsdDibdybq5AP7K1WMH2Lc2/2GKvOtEv7fTNYtNQngNylo3qLCGC1cD4TUg/Zb4sVZ8351xsvA6Q9Dt/fL/zRirzWUQtJMYU9OB3YxRE1KoSSq19hir2b8mde+t+Xn06Vqz6a5jAJ39J6tGf+Jp/sMVQ/wCeRr5dsf8AmPj/AOTM2KvLdCt4LrX9NtJ09SCe4jjlQ1AKsaEbYq9oj/LDyYy1OmJX/Xk/5qxVLtc/KfyxLauLKE2NyAfTmjd2APbkrEqw8f2sVeMPHIpYNVJ4WIqpoVdD2I8CMVe9+Stbm1fytZXs5rcMhSY+LxMUZv8AZceWKvEtc/5SLWf+Y65/5PNir0/8nTTy5L/zFSf8RTFXoXIUxVYzDFWEfmmf+dPv/nB/yfjxV43UUjB6Myg/ImmKvVo/yz8ruKmCTf8A4tk/5qxVcfyv8rD/AHRJ/wAjpP8AmrFUbovkjQdIvfrtlCyXBQx8mkdhxYgnZiR2xVkHQYqsY4qpscVWMcVU2OKqbHFVNsVU2xVTY4qptiqxsVU2xVTbFVNsVU2xVY2KqbYqsOKqZxVYcVUziqw4qsOKrDiqw4qsOKrTiqw4qtOKv//UwxVcMVXjFV4xVcMVXjFV4xVeMVXjFVQYqvGKr1xVUXFV64qqLiqouKr1xVUXFVRTiqouKqi4qvU4qqKcVVFOKqinFVRTiqopxVepxV0j0XFXj3nrVP0h5jkjU1hsV9FfDn1kP3/D/sMVXeVPKJ8wRXE0k7wRROI4ygB5NSrdfCq4qyIflJbn/pYzf8AmKpT5n8g/oXSnv4bmS49Jl9RHUD4GPGu3gSMVY/omqHS9astSBokUgE1O8b/C/wDwprir6AicSQgg1BFQRirxLz7pv6P82XPEUivALlPCr7P/AMOrN/ssVSIgAV8MVel/k3pNLa91Vx8VxIIYSf5It2I/1nan+wxVmfnPXBo/l+8vQaSRxlYB4yv8Kf8ADHfFXz5GspCIgMk0rBVHUszGg69ycVeoQfkxbyxqX1KYOQOQVEpXvSuKqh/JO0Ar+k5/+ATFWA+ZtDfQ9cn0x3MiKqvDKwoWRx1oPBuS/wCxxV6B+TGuE2V5o0jfFav68AP++5ftAf6riv8Az0xVjn5smvnQf8wkX/E3xVMfyU/472p/8YI/+JnFWS/nX/yisX/MXF/xF8VeRaZcx2mradeygmK1uYZ5OIqeMcgc0HjQYq9dj/OzywgoYrvb/ipf+a8VX/8AK8PK/wDvq7/5FL/zXiqbeXfzL0TzFczWlis6SwR+q5mQIvGoXYhm8cVeJ+aNaOteY7/U+XKF5Clt4elH8KU/1gOf+s2Ksn8n/leuv6LFqVzdy2xnZ/SjRVPwK3EE18SDiqen8jLL/q6XH/AJirE/PPkT/C8NpcQ3ElzBPI0UjSKBxenJOn8wD4qp/lrrJ0rzdArNS31Ffq0g7czvGfnzHH/Z4qzT8635eXLH/mPj/wCTMuKvMdFuobPXNOvZyVgtp0kkYAkhVNTsMVeux/m75PRaG6k/5Ey/804qgNZ/ODQfqz/o9Zbu5IPpqUMaBu3ItQ0/1RiryKSRgryStV3JZ28STU4q9y8gaXPp3lCxguFKzMrTOp2I9VzIAR4hWAOKvG9a/wCUi1n/AJjrn/k82Kso8keeNK8v6S9pdpM0jTPJ+7QMKMFHdh4YqyT/AJXD5e/31df8i1/5rxVa35w+W1FWjuqf8Y1/5qxVV/Me6S58k3cyV4yi3da9aNNGRiryOgpGx6KQT9Brir02P809BjFDHc/RGv8AzVirZ/NfQf8AfVz/AMi1/wCasVcv5q6AXVTHcjkQo/dr1O382KszR+QrirmOKqbHFVNjiqmxxVYxxVTY4qpscVU2OKqbYqptiqxsVU2xVTbFVNsVWHFVNsVWNiqmcVWHFVhxVYcVUziqw4qtOKrDiqw4qtOKrDir/9XDFVwxVcMVXjFV4xVcMVXjFV4xVUGKrxiq8YqqDFV4xVeuKqinFVRcVXriqopxVUU4qqKcVXqcVVFOKqinFVRTiq9TiqopxVUU4qqKcVUb0zi2lMCh5gjGJCaAtT4QT23xV5L/AMq/84sXaSOFpZGLO3qjdmNSemKvTPJ+iNpGiWtnIB66rynI3Bkc8m370J44qyFQMVQer6bFqGn3FnIPguI2jb25ClcVeQf8q084+n6ZigJ6V9Xb9WKvWfKFrqlroFpbaoqi8gT0pODcwQhohr7oFr/lYqkX5keT7/W4rObTVQ3ls7Bg7cQYnG+/iGVcVYQ35Z+dGUqIYK/8ZR/TFXsHlXRhpGh2dhtygiVZCOhc7u30uWOKsc/M7y95k12GztNLSM20btNcmR+FWA4oAPar1/2OKsc8pfln5gt/MVneapHEtnasZSEcOS6j93tTs9GxV7HCgVaUxVVKgjFXnH5neRdU1y4srzSljNzCGinEjcKxn4l3ofsty/4PFUj8meQvOujeZrXUJIoRa/FFdhZQSYnG9BT9luL/AOxxVF/mF5A8zax5jW/06OJrYW6REvJxPJWYnan+UMVR/wCV3kjzDoOp31zqiRLHPEiRem/M1DEmuwxVPPzN8t6pr3l9LPTVRrhbiOUiRuI4qGB3+nFXmP8AyqrzwNvRt/8AkaP6Yq7/AJVT53/3xb/8jR/TFXf8qp87/wC+Lf8A5Gj+mKpt5f8Ay/8AO2lwauyxQi5vLQ21sVlGzO68mJptxTkR/lYqk6/lF55KCMRW6g7FvV6e/TFXu+jaZBp2n29lCKRW0aRR/JAFH6sVR5AxVi/n7y2+veXLuwgC/WmAktixoBIh5Lv25U4/7LFXkP8Ayqnz6pRo44EljIaNxMKhlNQRt44qz7z/AOWfMHmDyzp9vbRRLqEc8U9zGz0RaQurhWpvR32xVgJ/K3zsNjDB/wAjR/TFWv8AlVvnP/fEH/I0f0xVdH+VfnN24lLaIfzNKaf8KrHFWW+WPyktLK5S81acX08ZDRQheMKsO5BJMlPfiv8AkYq9CaIBKDFXi2r/AJb+bpda1K6hihMFzdTTREyAHhJIzLUU8DiqEP5a+cf98wf8jR/TFVv/ACrbzh/viD/kaP6Yqsl/LTzjIhUQwAn/AItH9MVeg+Z9A1G/8nPplqqm8McCgM1FrHIjNv8AJTirz/8A5V35vUAGGHb/AItH9MVaP5e+bf8AfMP/ACMH9MVa/wCVf+bP98w/8jB/TFWh+XvmtnQmGEBWBP7wdAflir16IEKAcVbY4qpscVWMcVU2OKqbHFVNjiqxjiqmxxVTY4qpscVWMcVU2OKqbHFVjYqptiqmTiqw4qpnFVhxVYcVWHFVhxVYcVWHFVhxVacVWHFVpxV//9ahiq4YqvGKrhiq8YqvGKrxiq8YqvGKrxiq8YqqDFV4OKrxiqoMVVFOKr1xVUXFVRTiq9TiqopxVUU4qvU4qqKcVVFOKqinFV6nFVRTiq8b4quCLiqqoAxVUBxVeKHFWwi4qqqAMVXUBxVcqLiqqtBiq7iDiq5VUYqqA4quBxVugOKthFxVdwXFW1VRiq6gOKtemmKu9NMVb9NcVdwXFXcFGKrthirVcVWtQ4qsKLirRVcVWmNcVWGNcVa9NcVaoBiq1sVUmRTiqwxp4YqsMa4qtMa4qsZRiqm0a4qpmNcVWGNcVWFFGKrTiqxjiqxjiqmxxVTY4qsY4qpscVU2OKqbHFVjHFVNjiqm2KrGOKqbHFVM4qsbFVM4qsOKrDiqmcVWHFVhxVYcVWHFVhxVacVWHFVpxVYcVf/XoYquGKrhiq8YquGKrxiq8YqvGKrxiq8YqvGKrxiqoDiq9cVVBiq9TiqouKr1OKqinFVRTiq9TiqopxVUU4qqKcVXqcVVFOKr1OKqgOKqgOKr1OKqgOKrwcVXg4qvBxVeDiq8HFV4OKrwcVXg4quBxVcDiq8HFWwcVXA4quBxVsHFW64q3XFXVxVuuKurirVcVdXFWq4q0TirROKrScVaJxVaTiq0nFVhOKrScVWE4qtJxVYTiqwnFVhOKrCcVUycVWE4qsY4qpk4qpk4qsY4qpscVWMcVU2OKqbHFVjHFVNjiqmxxVYxxVTY4qpscVWNiqmcVWMcVUziqwnFVhxVTJxVYcVWHFVhxVYcVWnFVhxVacVWHFVpxV//0GjFVwxVeMVXDFV4xVcMVXjFV4xVeMVXjFV4xVeMVVBiq8HFVRcVXg4qqA4qvU4qqKcVXqcVVFOKqinFV6nFVRTiq9TiqopxVUU4qvBxVUBxVeDiq8HFVQHFV4OKrwcVXg4qvBxVeDiq4HFV4OKrgcVXA4qvBxVsHFVwOKrq4q3XFW64q3XFXVxVuuKurirVcVdXFWq4q1XFWicVWk4q0Tiq0nFVpOKrScVWE4qtJxVYTiqwnFVhOKrCcVWE4qsJxVYTiqmTiqwnFVNjiqwnFVNjiqxjiqmxxVYxxVTY4qpscVWMcVU2OKqbHFVhOKqZxVY2KrCcVUziqwnFVhxVTOKrDiqw4qsOKrTiqw4qsOKrTiq04qsOKv8A/9FoxVcMVXDFVwxVeMVXDFV4xVeMVXjFV4OKrxiq8HFV4xVUBxVeDiqoDiq9TiqopxVepxVUU4qvU4qqKcVXqcVVFOKrwcVVAcVXg4qqA4qvBxVUBxVeDiq8HFV4OKrwcVXg4qvBxVeDiq8HFVwOKrgcVXA4quBxVcDiq4HFWwcVbBxVuuKt8sVbrirq4q6uKurirVcVdXFWicVWk4q0Tiq0nFWicVWk4qtJxVYTiq0nFVhOKrScVWE4qsJxVYTiqwnFVMnFVhOKrCcVUycVWMcVUycVWE4qpscVWMcVU2OKrGOKqbHFVjHFVNjiqxjiqmTiqwnFVMnFVhxVYTiqmTiqw4qsOKrDiqw4qtOKrDiq04qsOKrTiq04q//SYMVXDFVwxVcMVXjFV4xVcMVXjFV4xVeMVXg4qvBxVeMVXg4qqA4qvBxVUBxVeDiq8HFVRTiq9TiqoDiq8HFVRTiq8HFVQHFV4OKqgOKrwcVXg4qqA4qvBxVeDiq8HFV4OKrgcVXg4qvBxVcDiq4HFVwOKrgcVXA4q2Diq7lirfLFW+WKt1xV3LFXcsVdyxV3LFXVxVrlirXLFWuWKtE4qtJxVaTirROKrCcVWk4qtJxVYTiq0nFVhOKrCcVWE4qsJxVYTiqwnFVMnFVhOKrCcVUycVWE4qpk4qsJxVTJxVYxxVTY4qsJxVTJxVYTiqwnFVMnFVhOKrCcVUycVWE4qsOKrDiqw4qtOKrDiqw4qtOKrTiqw4qtOKv/00xiq4YquGKrxiq4YqvGKrhiq8YqvGKrxiq8HFV4OKrwcVXg4qvBxVUBxVeDiq8HFVQHFV6nFVQHFV6nFV4OKqgOKrwcVVAcVXg4qvBxVUBxVeDiq8HFV4OKrwcVXg4qvBxVcDiq8HFV4OKrgcVXA4quBxVcDiq4HFWwcVb5Yq3XFW+WKt8sVdyxVuuKuriruWKtcsVa5Yq7liq0tirROKtE4qtJxVaTiq0nFVpOKrScVWE4qtJxVYTiqwnFVhOKrCcVWE4qsJxVYTiqwnFVMnFVhOKrCcVUycVWE4qpk4qsJxVYxxVTJxVYTiqmTiqwnFVhOKqZOKrCcVWE4qsJxVYTiqw4qsOKrTiqw4qsOKrTiq04qtOKrTir/9RMYquGKrhiq4YquGKrxiq4YqvGKrxiq4HFV4OKrwcVXg4qqA4qvBxVeDiq8HFVQHFV4OKrwcVVAcVXg4qvBxVUBxVeDiq8HFVQHFV4OKrwcVXg4qvBxVeDiq8HFV4OKrwcVXA4qvBxVcDiq8HFVwOKrgcVbDYquDYquDYq2GxVvlirfLFXcsVbriruWKu5Yq6uKtcsVdyxVrlirRbFWi2KrS2KtFsVWlsVWk4qtJxVaTiqwnFVpOKrCcVWE4qtJxVYTiqwnFVhOKrCcVUycVWE4qsJxVTJxVYTiqwnFVMnFVhOKrCcVUycVWE4qsJxVTJxVYTiqwnFVhOKrCcVWE4qsJxVYTiqw4qtOKrDiq04qsOKrTiq04qtOKv/1Uhiq4YquGKrhiq4YqvGKrhiq8YquGKrwcVXg4qvBxVeDiq8HFV4OKrwcVVAcVXg4qvBxVeDiqoDiq8HFV4OKqgOKrwcVXg4qvBxVeDiqoDiq8HFV4OKrwcVXA4qvBxVeDiq4HFV4OKrgcVXA4quBxVcDiq4HFVwbFWw2Kt8sVb5Yq3yxVvliruWKurirq4q7liruWKtcsVa5Yq1yxVotirRbFVpOKrScVaJxVaTiqwnFVpOKrScVWE4qsJxVaTiqwnFVhOKrCcVWE4qsJxVYTiqmTiqwnFVhOKrCcVUycVWE4qsJxVTJxVYTiqwnFVhOKrCcVUycVWE4qsJxVYTiq0nFVhOKrDiqw4qtOKrTiqw4qtOKrTiq04q/wD/1kRiq4YquGKrhiq4YqvGKrhiq8YquBxVeDiq8HFV4OKrwcVXA4qqA4qvBxVeDiq8HFV4OKrwcVVAcVXg4qvBxVeDiq8HFVQHFV4OKrwcVXg4qvBxVeDiq8HFVwOKrwcVXg4quBxVeDiq4HFVwOKrgcVXBsVbDYquDYq3yxVvlirfLFW+WKt8sVdyxV3LFXcsVdyxV3LFWuWKu5Yq1yxVaWxVotirRbFVpbFVpOKrScVWk4qtJxVYTiq0nFVhOKrScVWE4qsJxVYTiqwnFVhOKrCcVWE4qsJxVYTiqmTiqwnFVhOKrCcVWE4qpk4qsJxVYTiqwnFVhOKrCcVWE4qsJxVYTiq0nFVhxVYcVWnFVpOKrDiq04qtOKtHFX//10RirYxVcMVXDFVwxVeMVXDFVwxVeDiq8HFVwOKrwcVXg4qvBxVeDiq8HFV4OKrwcVXg4qvBxVeDiqoDiq8HFV4OKrwcVXg4qvBxVeDiq8HFV4OKrwcVXg4quBxVeDiq8HFVwOKrgcVXBsVXBsVXBsVXBsVXBsVbDYquDYq3yxVvliruWKt8sVdyxVvliruWKtcsVdyxVrliruWKtcsVaLYqtLYq0WxVaWxVotiq0tiq0tiq0nFVhOKrScVWE4qtJxVYTiqwnFVhOKrScVWE4qsJxVTJxVYTiqwnFVhOKrCcVWE4qsJxVYTiqmTiqwnFVhOKrCcVWE4qsJxVYTiq0nFVhOKrCcVWnFVhxVaTiq04qsOKtHFVpxVacVf/0EBiq4Yq2MVXDFVwxVeDiq4YquBxVeDiq4HFV4OKrwcVXA4qvBxVeDiq8HFV4OKrwcVXg4qvBxVeDiq8HFV4OKrwcVXg4qvBxVeDiq8HFV4OKrwcVXg4qvBxVcDiq8HFVwOKrwcVXA4quBxVcGxVcGxVcGxVsNiq4NirfLFW+WKt8sVb5Yq7liruWKt8sVdyxVrliruWKu5Yq1yxVrlirXLFWi2KrS2KtFsVWlsVWlsVWk4qtJxVaTiq0nFVhOKrScVWE4qsJxVaTiqwnFVhOKrCcVWE4qsJxVYTiqwnFVhOKrCcVWE4qsJxVTJxVYTiqwnFVhOKrScVWE4qsJxVYTiq0nFVhOKrScVWE4qtJxVacVWk4qtOKrTiq04q/wD/0Q4xVcMVXDFWxiq4YquBxVeDiq4YquBxVeDiq4HFV4OKrwcVXg4quBxVeDiq8HFV4OKrwcVXg4qvBxVeDiq8HFV4OKrwcVXg4qvBxVeDiq4HFV4OKrwcVXg4quBxVeDiq4HFVwOKrgcVXBsVXBsVXBsVXBsVbDYquDYq2GxVvlirfLFW+WKu5Yq7lirfLFXcsVdyxVrliruWKtcsVa5Yq1yxVotirRbFVpbFVpbFWi2KrS2KrScVWk4qsJxVaTiqwnFVpOKrCcVWk4qsJxVYTiqwnFVhOKrCcVWE4qsJxVYTiqwnFVhOKrCcVWE4qsJxVaTiqwnFVhOKrCcVWE4qtJxVYTiq0nFVhxVaTiq04qtOKrTiq04qtOKrTir/AP/SD4q2MVXDFVwxVsHFVwxVeDiq4HFVwOKrgcVXg4qvBxVcDiq8HFV4OKrgcVXg4qvBxVeDiq8HFV4OKrwcVXg4quBxVeDiq8HFV4OKrwcVXg4quBxVeDiq4HFV4OKrgcVXg4quBxVcDiq4Niq4Niq4NirYbFVwbFWw2Kt8sVb5Yq3yxVvliruWKu5Yq3yxV3LFXcsVa5Yq7lirXLFWuWKtcsVaLYq0WxVaWxVotiq0tiq0tiq0nFVpOKrScVWk4qsJxVaTiqwnFVpOKrCcVWE4qsJxVaTiqwnFVhOKrCcVWE4qsJxVYTiqwnFVhOKrScVWE4qsJxVYTiqwnFVpOKrCcVWk4qsJxVaTiq0nFVpxVaTiq04qtOKrcVaxV//TD4q4YquGKrhirYOKrhiq4HFV4xVcDiq4HFVwOKrwcVXA4qvBxVeDiq4HFV4OKrwcVXg4qvBxVcDiq8HFV4OKrwcVXg4qvBxVcDiq8HFV4OKrgcVXg4quBxVeDiq4HFVwOKrgcVXBsVXBsVXBsVXBsVbDYquDYq3yxVvlirfLFW+WKt8sVdyxV3LFW+WKu5Yq1yxV3LFXcsVa5Yq1yxVrlirXLFWi2KtFsVWlsVWlsVWlsVaLYqtJxVYTiq0nFVpOKrScVWE4qtJxVYTiqwnFVpOKrCcVWE4qsJxVYTiq0nFVhOKrCcVWE4qsJxVaTiqwnFVhOKrCcVWk4qsJxVaTiqwnFVpOKrScVWk4qtOKrTiq04qtOKtHFWsVf//UD4q7FWxiq4Yq2MVXDFVwOKrhiq4HFV4OKrgcVXA4qvBxVcDiq8HFV4OKrgcVXg4qvBxVcDiq8HFV4OKrwcVXA4qvBxVeDiq4HFV4OKrwcVXA4qvBxVcDiq4HFV4OKrgcVXA4quDYquDYq2GxVcGxVcGxVsNirfLFW+WKt8sVb5Yq3yxV3LFXcsVb5Yq7lirXLFXcsVdyxVrlirXLFWuWKtcsVaLYq0WxVaWxVotiq0tiq0tiq0nFVpOKrScVWk4qsJxVaTiqwnFVpOKrCcVWk4qsJxVYTiq0nFVhOKrCcVWE4qtJxVYTiqwnFVpOKrCcVWE4qtJxVYTiq0nFVhOKrScVWnFVpOKrDiq0nFWjiq04qtOKtYq7FX//1Q+KuxVwxVcMVbGKrgcVXDFVwOKrgcVXA4quBxVeDiq4HFV4OKrgcVXg4quBxVeDiq4HFV4OKrwcVXA4qvBxVeDiq4HFV4OKrwcVXA4qvBxVcDiq4HFV4OKrgcVXA4quDYquBxVcGxVcGxVcGxVsNiq4NirYbFW+WKt8sVb5Yq3yxVvliruWKu5Yq3yxV3LFWuWKu5Yq7lirXLFWuWKtcsVa5Yq0WxVotiq0tirRbFVpbFVpbFWi2KrScVWE4qtJxVaTiq0nFVhOKrScVWE4qtJxVYTiq0nFVhOKrCcVWk4qsJxVYTiq0nFVhOKrCcVWk4qsJxVaTiqwnFVpOKrScVWk4qsOKrScVWnFVpOKtHFVpxVacVdirsVf/9YPirsVdirYxVsYquBxVcMVXA4quBxVcDiq4HFVwOKrgcVXg4quBxVeDiq4HFV4OKrgcVXg4quBxVeDiq8HFVwOKrwcVXA4qvBxVcDiq8HFVwOKrgcVXg4quBxVcDiq4Niq4HFVwbFWw2Krg2KthsVXBsVbDYq3yxVvlirfLFW+WKt8sVdyxV3LFW+WKu5Yq1yxV3LFXcsVa5Yq1yxVrlirXLFWi2KtFsVaLYqtLYq0WxVaWxVaWxVaTiq0nFVpOKrScVWk4qsJxVaTiqwnFVpOKrCcVWk4qtJxVYTiqwnFVpOKrCcVWE4qtJxVYTiq0nFVhOKrScVWk4qsJxVaTiq0nFVpxVaTiq04qtOKtHFVpxVrFXYq7FX/1w+KuxV2KuGKrhirYxVcMVXDFVwxVcMVXDFVwxVcMVXjFVwxVeMVXDFVwxVeMVXDFV4xVcMVXjFV4xVcMVXDFV4xVcMVXjFVwxVcMVXDFV4xVcMVXDFVwxVsYquGKrhirYxVsYquGKtjFW8VbxV2Kt4q7FW8VdirsVdirWKuxVrFXYq1irRxVo4q0cVWnFWjiq04q0cVWnFVpxVacVWnFVpxVacVWHFVpxVacVWHFVpxVYcVWnFVhxVacVWHFVpxVYcVWnFVhxVacVWHFVpxVacVWHFVpxVacVWnFVpxVacVaOKrTirRxVrFXYq7FX//2Q==",

            like:"data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAyMSA3OS4xNTU3NzIsIDIwMTQvMDEvMTMtMTk6NDQ6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE0IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDozRDg2Qzc0MjlGRDIxMUU1OTAyNUQ1QzMxNjlDNDQ2NSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDozRDg2Qzc0MzlGRDIxMUU1OTAyNUQ1QzMxNjlDNDQ2NSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjNEODZDNzQwOUZEMjExRTU5MDI1RDVDMzE2OUM0NDY1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjNEODZDNzQxOUZEMjExRTU5MDI1RDVDMzE2OUM0NDY1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPwCBAwERAAIRAQMRAf/EAJ4AAQACAgMBAAAAAAAAAAAAAAAHCAQGAwkKBQEBAAMBAQAAAAAAAAAAAAAAAAMEBQIBEAAABgIBAwIEBQQDAAAAAAABAgMEBQYABxESEwgUFSFBIgkxMkIWF1FDNLZ3GDgRAAIBBAEDAgMCCQsFAAAAAAECAwAREgQhMSIFQRNRYRSBI/BxseEyQjNzBpGhwdFigpKiskMkg7M0FTX/2gAMAwEAAhEDEQA/APfxilMUpilMUpilMUpilMUpilMUpilfHn7BDVeJeTk/IN4uKYp9xy8cmECEARApCEIQDKrLqnECkTIUxzmEAKAiPGcPIkYGXUmwHUk/AAcn1PyAJPAJrtI3kJCDoLn4AfEnoB/Tx1qvDfy31StIlYnStLVuZbte7OIhoMcUnVx6gxG8ovKAj8/8YT8fpyeONn62U/P81x/Pb51HIcL49wHw/Pb+urJRskwmGDSUi3jd/HvkSOGjxqqVZu4RUDkqiahBEpgH5/MB+A/EM8kjeJzHILOPw+0EcgjgjkcV4jrIodDdT+FviCDwQeQeDzWbnFdUxSmKUxSmKVwuEzrILopuFmiiqKiabpuVAy7Y5yGKVwgV0g5amWRMPUUFE1ExMAdRTByA8SIzxsisyMVIDC11JHUZBluOoyUi/UEcVJC6xyrI6LIisCVbIKwBuVbFlax6HFlax4YHmqJRdquNI2bvpXYvkXtKT1zoWF1haPRO6to0FrC3tcRYJGWiZs9c0tEzD0HTuLRbs04xWPdCZXoBQxjFMX2LYWPVfZeMyy/XtqogBJZng0zEAAQfcM20QpLCPhMxiHLdPqtJsxa0biNG0l2HdiAFCz7ayFiQQI1i1gzcFhdypuVC7zbfLVrUq1d5CR03tOIt9QpTbYDOh2s9ChJGyVNxIJRKs7HTUVdbNBIMoeRcopyLdRb3Zp30+WRxMBc9cMHWOOzyje1taRQwvGdp8InvfGRXZXSNomeMyrjJJEmUi+QRCZDKSUgOps7EbEECQakYlmQAgMjxxkSssio3t3KLI+MbSDbN3StLrKNomtGbkFm3g5qx2dqw/id+6pUPBKqesczyzfa5ol+uoySF2k2h3Uq6O3/tgoUyZeZpYddiZnC6qIjNLZii5kgggKZD7eJMrKjIi2YtYg1zDFNOAIlJ2HlZEjuuTlQhDDnBVcvinuMhZlYWsLnh1jsOVue1tyRYS/uNNhIbT0xS2/oGrT0jS6VWTmZBfvFZt5Nx7idJJTpdHUFHjpIBA5DL0unPpps6+6uO/r+V2NdhcHERa+i2HBxOMkspyFycrXKhQIPein+lm17/AE8/jYp+epaTY3EyPwvHFGLdBje1yxObpe42O2MNrr2CRB+rWt07OqcIcWTJuDKv12UI3h2ApsEWPqwZoiIdxUxl1f1qCPxyj5eePx38La/mOk30W7LIbZXMG/vwocM0BxhgjTENGGxuWDMzmUC/lJ9b/ZRtcAXtYSaerK/Nj1kkdrkG17AYgAQJX/KVdO1Vl9PSEjYIW9aF1VbqXSK5U4mHstrvl8tNubg2gYJ3Zp5w0erQkYkZw3c2FzHMG7VVwo4ImVRbLuvAH2N/x4Bffj3tSOIZA4RNqPNsyMbRgQowEju4+7GMaszuqyVTMCEnLCPVjk8mkpIPLauzr68SKBmzuzNIkYQBpi2ZjQDGOYZjyXqcLYpGJcVHYC9egLbXaFbdjs42uLUOoXOzBFkY1+adGtKdicKtXU4zbvHUfGvmDJdyQizgg88Qaijc2I9eJgPqJ5YYGa4GxLEXVljW2YyeN442lSNZJFKqTU+yzamu08ytnFrLPKgsXhjZQ93sSrMsbCVkiaRxERJjiQT9zXu8WOzJ2Sj61Qb8Najpu01w2xHgUZOpLTNQkFouWYiwa3l5fY1U71AxW/r4Rp3i8HD6DFMPGp/ytKPe/ZxTa6zRhuskbsApXHJQ1jkUZlYKGuAwK13tKdTdk0H7tiGYxSY9I3CliDe2Si2OceaFiMWIINQR5nSUkROjRJRUJEuDTD9YCiYEnD9t6FBEFA/KYzVBycS/07o5Wjs3kWy/UhXH5Zs+R/yL+L7Te3YL4/JD3PKQ3yCqCv2MWb/APhUKOdhEd6ldxU0tBKoumsfWanTolqzBSBcwSrN2/u8kdVVaQZvpYix0gMHweKHUEekpOMvbqiaRGFzsPIJMh+jFGt0MS/vLA4H9Fe/klRVLTAiDjpEiMpB6yySd2Z9LR+hHANlAHJqzPh5JSLqiz7BydRRhF2ESxvWBhKiDtmi4dt0jiPHbKtwp0h+Uyoj+rLm0A2lBK37XKRP7i4FT/K7i/wABb0qpFZdyVF6FUYj+0c1J+0Io+z8dW5zOq5TFKYpTFKYpTFKrjaPHaNt8jv8AcS9lepsd6V2hQZm8dHptX1Sc0JjLIMJZrIKvXSMsuq/kEnJUjtkUyCh0G7pTiIRBJY4MIHw2V8gu4j2BxkRdQRqVP6Sh9RWblSyuVGJAc2ROh2FkmQPB9C2q6XIzjd9oyEMLFSybTKLXxKhub4jT53xltWwUbs82rtdjY7NYtUvtS1yVquvCU6LrEVJyLOak5x3CPbhbV5+flJiKZKLiD1k0BJuKaKCPcEwduuIeXVCx70m5pz5G7Iv0UpmhiRLg+2ZHkLl5Hdsls6hbHiKdl9mHY+90YYNyMKeHP10A1p5GcWHueyAqFY1VTclWvatW2h4hWXcMmpN3/YWt7HJyOvxockab0Wefjq+oEhNOf3ZqqIsG0pVjr+0vGcmkk6dLlmDqKNSKpCh9JCQya0bDbWLsO0VYOOZYysWHtpIeRrlvvDGuMgLOROHYOEGxNH9KJiJF1i/bbFJQ5Q5zIO151xYK9hFZgpgKqQ0/av1B/G0/apz9w+9fuetaur3pfafbvQ/xtWF636zv+5v/AFPvXf73b6E/TcdHUrz1Zt+T8j/7Hd3dzDD6zymxuWvfD349dPbvYZYexfOy5Z2xGNzR14PY19WC9/ptCLWva2XtSTyZ29MvftjzbG+RysNepmqdo0Wx208RsmguKJcdkWnYElX5LU1hWtrclseg8fw7K5tdyM4YhkAKBUXKkAoAfiZI34Zh+QgbyXgT4HZYCIau3AjqLEDZm2pwzAlg3tybJuBhmiBQUY5i1Ib7bbcPa7tCWDdw+6hhgNrY2ySEEXvizX7gLGK0/DBgklSJBG+naXbWWode6617d2VWIhK1ix0CUl5EtwRSNYFQdxFmQlRZSUGop2HTLrTO4MJymTv/AFEke3tb+rim1s7evKSRkPaigaCXXcAqXjnVrkhkMbBHUF0VhUGrE8Tau1k+m82/KVBxYPubKbMbo3dhJqugwkAJe5uFUsjZf/T+LJsub2AoTSFiLbLZE3ayFv3jvC3O4Mp1BKNLPFol9dXNjIViKl147vNm75tNGjFVTCioPz80XXx8qtrZJHFsSyxMrYTL7krzKHlUAO0cjkrIscb4hVv2qRP5D3N+I5lfqX1UifIZwlkiEIkWJjdQUVcozK0ZYE2GTA71X9Cz7LczTb07baUq7j0bM17VH1apQrJbGU8mDdix2baSXywNry1riJCHZl9tYiR0mVYBAeSjDoAacMiv3SywKjY9kRcSJI0/td33rFCuQcWWSQcgi0m+w3J0dBjDHOXTPvkRPbkjWFZe37sBwSCpLFEPFqlHaGsoTaVcNBSyijNy3VF3EyyCZVHEa96BJ3ATMYhXDZYg9KqQmKBy8cGKYCmLFJGWdZkNpUv8wQbXUj4GwIPUEA9LgyRShFaNxlE3UfMdCPmLn7CR61UofFTZj1vE1uSulW/aUM+fumHYRfKSLX3M6Av1yNBiWveWcA2IPbUfGIQQ+kQ5Hm2HSSRZZwfcWPAW57QzPj6frMTci/PqABUDAxq6Qcq7hjfi5AxB9bdo6A2/LVy6FR4XXlZY1iCIp6Vr1qruVxKLp+9XEDOXroxClKKqogAAABwQhSlD4FDOticzuDbGNRZR8B16+pJJJPqSTYDgRwxe0CSbyMbsfibAcD0AAAA+A5JNydyyCpaYpTFKYpTFKYpTFKYpTFKYpTFK0e3bKoVBf1SMulshaw9vEwav1NGZeEZFm5oqPfCOaLK8IA5OQQKQDmIB1DkTKInOQpvYQdjaXShBbbdHdVHVlQqGt8SM17R3G/ANjZKRBqPvTdupGyqznopfIrf4CysSx7VAJYgVvGeUpilMUpilMUpilMUpilMUpilMUpilMUpilMUqHJPyJ8foWYe16Z3ppyJn41+rFyEHJ7OpLCYYSaCwt1457GOptJ61fouAEh0TkKoU4dIhz8M81mXcCHUIlEhAXDuyJNgFxvck8C3rxSb/AI4LbHYqrkcu2y2yub2sMeb9Lc9K6WPu6bsrdhv+qdU1uabryWuXEpZLZIMHAuk4OYm1IpCMjVgagfiTjGUadyqQOpQhXCZeAN1BlTws+f8AFMfki4Xx+qY0VwCe8yK8zAi5YII4gMRw4kXlgQL3kIvZ8A+o6l9jZYuY+B2IjolyTwZTJILNjZVVuVcEdk1f+4z4aS7uGgm27my8rIrMItBR7R9mxbRZ+5Mk2IZzJStLZRzFFVc/1KuFUkiAPJzFABHNZIX3dz2dRRnI5CLcDqe1bkgX9AL8ngcmsDWMnjvFRnyUheWDXX3ZLE5MijN7AFjcgt0vV5Mq1o0xSmKUxSmKUxSmKUxSmKUxSmKUxSmKUxSvGf5cf+ofIH/l6+/7G/zA/hf/AOBq/uz/AKjWn5f/AMz/AKMP/Zjqu+b9ZlTf41U51ft/agqTRtFvFJe/VwijWbbPnkOu0aSCL58nJN40h3ajM7JqoB+npKAfE5yE6jl3f4bVR5mKeQXhgDzPYXOEKNI2NwVD2W0ZcFBJjmCtwcnzl28VNAhtJMoiU3t3SkRrc89uTDKwJxvYE2Fe0nMKtamKUxSmKUxSmKUxSmKUxSmKUxSmKUxSmKV0Y7q+2lvzZl32W/jH3js0rt33RYNms7I/eXZDaDaGmFHKRKw4lW9EfxyUUVsv6gzMorJg+DrBUS/jS8Jprp6Xj9LyBtHqiRZDD1kWV42JOQAZoxGRCSAF9yQG4bizv7LSnYk1Apllh11X3BwjwRuvbibqsjP97a5dY4uAVrNnfsr1xxKLLVnyCm4iEMflvHzuuWFjlEk+fyrTEfcas0XPx+orFMOfllqMSAfekE39AR+UtULlD+zBHHqb8/YB+HrVpvFf7b+uvGW7s9mBe7XebvHMJOOj112rCu1xqhLszsXq3sbU0m+cOjNlTkIKr86RANz2xOBTlvx7YgglghX9ugRiSScQ6SALbEC7IL3DcdLdTSk1jLNHLI3ETZKALclHQ5E3uLObAY2PUnoOxvKdWqYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpX/9k=",
            page:"data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAyMSA3OS4xNTU3NzIsIDIwMTQvMDEvMTMtMTk6NDQ6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE0IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1OTVENkM4NjlGRDIxMUU1QkExQTgzN0FBQUI1RDI5NSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1OTVENkM4NzlGRDIxMUU1QkExQTgzN0FBQUI1RDI5NSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjU5NUQ2Qzg0OUZEMjExRTVCQTFBODM3QUFBQjVEMjk1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjU5NUQ2Qzg1OUZEMjExRTVCQTFBODM3QUFBQjVEMjk1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPwCBAwERAAIRAQMRAf/EAJYAAQACAgEFAAAAAAAAAAAAAAAHCAYJCgECAwQFAQEAAwEBAAAAAAAAAAAAAAAAAwQFAgEQAAAGAgEEAgICAgMAAAAAAAECAwQFBgAHEhETFAghFRYJMVFBIkIXGBEAAgICAQMCBQEEBgsAAAAAAQIRAwAEEiExBSITQVEyIxRh8IFCUnGRobEzFcHR4WJygkMkRCUG/9oADAMBAAIRAxEAPwDn8YxjGMYxjGMYxjGMYxjGMYxjGMYxnxLFY4OpxDqdsUi3i4pkUDLu3Am6dTD0IkkkmU6zhwqb4ImmUxzj8AA5w9i1gcu5MADuT8gP7f0AJMAE52lb2EhR2Ek/AD5k/D5f0kAdSMr409ttUuZEjFRK0sW51u0Ms7iGgxxCiPQFzkaSjqTBH/Pw2EwB/wAcnrrZ+5Cn9f7pEj95MfMxkdh4dvUP0/2wf9P6ZZNg/ZSjJtIxrtu+YPUSOGjxqqRdu4QUDkRVJVMTEOQwf5Ac8srepylgIcfD9vn3B+I65yjpYodDKn9v3EdiD1B6HPbzjO8YxjGMYxjGMxS5QMtYoYzCFvFo1+9I4TdffVFpS3sqZJFNYDsDI3yoXaD8RyY4GOIMgXASF4KFDkBqPkrrNXRt26jD01O4HQhiqMQrfHjMH0lWkDrEgyV8Z4MAeUCesr1BkQQJgEdQRBPSYIppo7ctri9X6ltl7tm1957F3XHSRoDXMNAaXi/GNXHD5aemIldnAapjYuLYMSoA6VmJtVPuKpkRL3D9B3dyuumyrR1azZa3jNTdseetSXa2u7z6lUVtfsBKxwe1iFVWMWE1ypV777LOFFW9fqqsT7jLfalfQKzcxXQ7MwK1KObPHoAkM/ta2kJXWkXUNRbItji+zV7rkrGpvNfVyxUixa8brqWSuzcTa7rDM1pmOOkU6oJvCtDNDgq2cOTiRE9amr3thERlOlbo/kpcJ4FPfq12lSBYPassKXDh7i2KEWtx7j1SWxVrWWOG/Lq3BrtVAmWos2EIefbYW1oGqKsUZSxeytgiWSO53SkwvkRRZvXWyK4hZp6bq1UvUsyqJqbYZ6EhJWwKM2QRlxkrczTexMI6Warvoho2cFQNxU69AHK2thzobRpYVeQr0b70RoLMtJVGZY5p6WdG4uQSpBAMwWwyaqi6w8tb3aa2ZeyteVWsEGGILstZZAypYwDMvUiK4naV7c+kUlt5ee5bDb6Mt9xSsX1kMTt2OLr8y+YyX1JY4IIfHdNEz9kzUW5uPQxBARAbn/0xXx66v47eylp8UHboYGydQXmXDAchbYZiFn0xAix4lF2fKPr3DlSvkdqoDt6K9m2tFkQeiKqzPIxJJJJzGtS7gm3uwrFGVja8t7Q66i9SqXCYmoaM1nJStZvraSZpsKTE2TWsDR6ZNSFpilXaqcc4J5jY7MonWKQ4hliwmuvcY0s1FWxQlDqCDb7huW1fUwR1p402NanFEWzi8mDmRoW27Ol4q+5wnktqiw7VJHEa8V0vXYRxaxedjW0ipiW9BfspyTV/Z+uRsbbD2LX2zKxaalOUGvL6/l2lHWtEvI7PeCwo/wBNIQt7l6Ko2m3iSyYquZlsRsZuoDjtCBeUQ+4aa9eLdi7as1wi9CttVI2HDF+KQKGFgZWYMOiy0Lmk1dldd2xsKatSjTXaaxvpNLXPrhlC8nJ99DWV4AhupAUMy+lG+1MJIMpoyuq9sR1hjNjpamjaS4T1k/tFpvgRCtgk4eB+g2dM19JtAQSfmPX0i/j2CaA8irH4nAnCOLl1zrfcbZF7KB0K167Gu21y/FVRbFNY6lmfiAp9ysvzZFDXjaIqTXWgszdQW2eBoRePIl7BYhAiIJ6ylgXon7X1V8tX4aC19s6wXmbkrzDutcs21BirZXZPXK0Yla2Uy4tOwK9UXLpmWZaqopxsrIHdoLFVRBRPkYOlIt9Wv9yn8QbBZQfTWbbKPUjBbea3VW1sgrLI1ZLAK1Zf10alSdj7b/kigKSDydqResMvJArUsrq7OFIYKCXDKsT+3s7KvkNdNjs5CKjJCLdzisZIAim7byipWZDMpJNq4dNAkIpBbtnBNVUhTqH4nMHyPBqWvy1tRdXNVYClTKmXcMymASG4LB+QHQScnVifGJYFZTZYeQPccVUqCASARzeepBI6H05FcnsFOQ1OtFzK0Ev56cZA1GoQ7ZmX8U/G1GashbH51FVpFm/nyqnTAP4dmOqcRApeOWt1RdcjiTsNZ7vIfTXWA9fsr/xEBuB+hQrfxLlXT+0rjtSiFCD3td/V7hn+TqOQ7t6YETlo/UGSkXmu5Vk7OooyirG4QjBU5iVJJwzaO3DdIxhEvbI5VMfiH8GUH+8tbYB1aLD/AIsMv/KpHE/2soPyUAdsp0+ncurX6OKNH+83IH+sKvT9/wActfmfl3GMYxjGMYxjO05eZDk69ORTF6/z05AIdenx16dcq7ut+Zp3anLj7tTJMTHJSsxImJmJE/POlPFg3yOVMhfWidp9P0lHUvZTOMvOkWVliIu0TVHUnKzY4S3mA0/FztLbXGDfkIsdq1UQVbTSKqCrfryMU5iZp3bVrbg2qAqBvFa2jaDLc01q9dUsUyvBxZr+4vRgA7Vv7gg5wy12JaloMv5CzbQgwUex7zxMhgye3sPW3RWJCupQgg+OP9ZrDXyUifruzWBNlVvYOwtjWO1WGgjL163TG0WC8ba0Pw+JuNZcQTZJoKBI/tSiwtiti93yRMcRjrsWi3XSgEePp0rNZlY8rLFt2a9uyz3QAFsbYRnn22UB+ATioxbzvp2BYR+VdtVXoVEJWadd9WuvgSzNWKWhps9wsOQsEwMUpfpmWq7IrOwXdwqEw+q18uF2Snv+p2zba1oJbou2RYwt62s9uUxJzjKGRsxQakas45rxakBRuc3bOln1avt+MOjMWHxr6ZKDgjCxVBvsrk87yyBi7OfqsA485HHlqx5R7mnitu5TsDl6zX7OxVeKqvpVKYrNaqqhlBU8iqslkyMNF+D62PPXr8p7vlazn9dfl/0nDt/eRUjGfcfQfbn5eL5/Px/NDnw490vXqFvy3/tG12/w/YbQP83L8I65/SPd9j9eHP8Aj49bOjZ+FuvuRy5bd98do966y7jPX6efHlHWJgTAynbOs19m6osGs21mdVdabi4+NLYGrRV32isHjF2oi8jkJKLWfRUqmyFs8bFdoCu0WUT7peXLON5BubSbJClU2kuNbjnXYEfma7FlSyN2IBXrB+EGl4TX/wAn8bT47m7GrS9gOp9t59k1CxD6+DA+sfVBET8cpBsD1pfat19eG0NGViZZbft+l465Q+qPX561pdNr+vpianHs2407UZy12zYUHPj4zKVYkf8A2Dgq6igu+yIJJRsZ29Km2W1K9+/aJsU218zqqldBqQKFrNlYZGEJW/FrA7SbLfB219l0gbH+XjWU1sKbYa1w1otYnleibFjSwZmWsIgWFKfbrmln+86QtU5eka7olb1XsaMsmqHrn1iktf6+uPl118ztbSzetmxrM6lxhyqS6iRXR3scuuvwcNxSOiIns2VtY2r5Wwj86obFHtWMLlOs4pZHHEqaHNqsUUWFlFZLLxu45CtgVdnxiFhp3LRaWrBqYbCPYYLkEXAVhA448GFjIeXAESlLerks+14hruPc+uMHDOHk9ITUTG+riDSphKSyLRowsVTrkftmPcVe2QzVtxJIqPpBRQ3AwlDtlDItqsbPAFrFCU8FYNFtbmyx2eiwACoFXRePBoav3ORLsMsa1v43JlVCzXB2Uj7dihEUJdXP3OqkluSyrcIAUHJas2j4a1azrGv5aalXz+owsVGxNxkTA+nFnkZGN41WTlBUOTz1JYrcDuiCcoqHHqBgMAGCfyFh3N9/I1gV2tY7R3HF2kqe0/CGABBExBKmv46saOkui5NlQVQT2Mr2K94iSADI4mO8MK5f+VNlyKMLXpm6VcKpBOX544WqL5eQaJyi6S79RNoaJYguq4OiUeCr0xSCHQogHXr0ro9guvB9wVhOn8qlmCz0+LN6oJ6/EADOjNatXR9DvyM9PVxCz8f4QBAIHT59cufR6XC0CtsKxBJnKyZAcx1lhKZ09drG5uXrs5CkKddc/wDQABSgBQAClAMX3G9gYhFEKB2AH+skkn4sSfjkdNXtKZJLsZY/M9B2+AAAAHyA7mSctyHJcYxjGMYxjGMYxjGMYxjGMYxladw+4HrnoOyNKhtjZCNWsj6JQnG8WSsXSwq/VuXDpq3crq1euTbZoKy7NQCpqnIoIF5ceIgIwVbNF1ttFRm2lgriD0LKHAkiCeLKehMAiYnJn17q6kvsUiqyeJP8QUwSB3iek9iQQDKmMAr37E/TW0zsTXIfdbAZWbft42PLJ0/YkAwO8dqAk3I6mZ+oRkNHJnUMAd1y4SSKI/Jgy7RRbs2iigcrWmBIBJAmBJEseyqPUxhVBYgGrddXRWbrTFa9zBMD5mJgDuT2USSQATl1chyQGeo7YxjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGMZxsv2nwsbYPcGDi5a216jsV9UV0ytktDa1u4VkZF9aVk03KFKrNwsRzu1CAkn2WCpQUOAqCQnI5cbxCht/wAoSwBG0hAM+r/tdUQIBEwS3qIEA9ZgHb8qxHi/FgAkGm0EiOn37jJkgxIjpJkjpEkUg1Svo/X22HrLbz8dsaqd1awQshL6tjZorhR/NQRyxMnXm+xozXcu2kK7NHTP3HDZEpVkBEpVkh/32UdbtHbocWU7T1haXhSUsS+qwWCGPpKoykGGZWKMoDNmGwsq2tbYrKPWjlrEMwUau2soeg6yysCD6ejKwdRG/H9Yu+7Btqg7Bo8vMyVrhtPTUFCUi42BmjGWqZps0hMDCM7NHNn0s1Sk4ZvCcOZHjnkRQpBOftgofT2bLt3x2v5TdVF8pY9ldxQkq7Vitvc6gHk4shjHqK8jLszNRpWrW8hfo6hY+PSut6wwHJA5sDVyDBRSno6CASAFTgibO8zMv4xjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjON1+0SzyFO9x4Gfi/ofNaanr6aX5LUKxeofo6e2psp5NauEPPQD0/bVHtmWanMifooQSnKUwY/iHK7/lVEQ21WDIB/8bVPQkSpkdxBiVmCQdryqBvF+LJmVptPcj/r3jqAeo69jImDEgEUaceyewU0+SCGknJ+QB2//LHrcj/r89Tcz6mEPj+s1+s5i5ty/TQ/cSrD2Tk3ZWpHUjY9fPnBWLBjFMirukLwuqVnGRbZnGxzUDnHtoN0UkEi9CkIUoAAaRrWnwGrSnLguzeBLFjAr1R1ZiWY/MsST3JJykbWu83sWvx5tr0kwqqPr2OyqAoH6AAZu4zOy7jGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGM1Q+4vpP7E7k37Cbr0RtGsa7exlHY1j7B1bLzTLUwdt3M2V4MbKU6uzKwsX8dKgmce+gYwCchiCX5NQ0NfY1Nrdt5D2dq1GgEyQtVKFXEARyqDDqR2MAjL+5tU7enp0FPu6quJMEep3cMp7gxYyER2+J5ECuUp6A/sknI2QhZr21YTEPLMnUbKxMpvnfMhGycc9RO2esJBg7pqzV6ydt1DJqpKEMmoQwlMAgIhlu2mm9PbvRXr5KYYAiVIZTB6SrAMp7ggEdRlJHetxZWStgMggwQf0I7ZdX9e/qDsT1Nhtns9iT9Lm3l4k6s5ji0t7OSDZo2r7WcSWF64nK/XVQXcKTAcSJpHKBU+om6j0DSs2K28dVqLy91LrXPQRDrSqwZkn7bTIESIJ6xSSh13rNkx7b1VqPnKNaTPSI9Yjqex7dJ2LZRy3jGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGMYxjGM//9k=",
            post:"data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAyMSA3OS4xNTU3NzIsIDIwMTQvMDEvMTMtMTk6NDQ6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE0IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo0QzM2NzYwMzlGRDIxMUU1ODY0NURCOEVGNDQzOUVFQiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo0QzM2NzYwNDlGRDIxMUU1ODY0NURCOEVGNDQzOUVFQiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjRDMzY3NjAxOUZEMjExRTU4NjQ1REI4RUY0NDM5RUVCIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjRDMzY3NjAyOUZEMjExRTU4NjQ1REI4RUY0NDM5RUVCIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPwCBAwERAAIRAQMRAf/EAKQAAQACAwEBAQAAAAAAAAAAAAAGBwUICQoDBAEBAAMBAQEAAAAAAAAAAAAAAAMEBQIBBhAAAAYCAAYBAQQFDQAAAAAAAQIDBAUGAAcREhMUFQghFkEiMqcxUedoGTMkNLS1NnbWV3cYOAkRAAIBBAEDAgQCBQsFAAAAAAECAwAREgQhMRMFIhRBUWEyIxVxgTN0BvCRobHBQnKzNLQ24VKCQ3X/2gAMAwEAAhEDEQA/APfxilMUpilMUpilMUpilMUpilMUpilYGy2eBqEQ5nbHJN4qLaAXquV+YRMc/wAJoIIpFUXcuFRD7qaZTHN9gfA5w8ioBe5JNgB1J+Q/rPwAuSQATXaRtISF6AXJ6AD5k/0fUkAXJAqgGPtrqp5IpsVU7RHIKK9Lyr6JaeOIAjwBVQGUo8kSpD+vt+IfaAZPHEz9SFP1P9FxcD9JNvmajkbDpdh9P+tj/b8hWyjN40kGjd8wcoPGTtFNw1dtVSLt3CCpQOmsismYyaiZyjxAQEQEM5dHjYo4IcfA1yjpIgdDdT/L+cfEdQeDX6c5rumKUxSmKUxSqi3c0sw67s05VNhWrX0rVK9Y7Ig7q8dQpIZVaKg3ztrHSyF9pV1a+NFyiUxu1I1cDw4dYA+Mz/JzS6mlNvxN6odeVgpAwZguSluMuMbAKyghmuCcSuj4uGHc3IfHyqMZtmJS4JDqpYqwXnD1ZAksjEFFxIGYakNcbZn6nSNTmscvt72E2XuDXMFsVGpxELpmOd12NRr0M/s0k0WZx2na9G1skpYEEEgk37t6ssZNJuKhgU47Xke1reS2/Ga8bsmkzs8nJIjaRo4lbkB3YxSFEhjMhtIzDELbG0jJL43W8nsuqe6CIiWIDydvuSdvg8IrKZC74oDHyC/qksH7RM7bctc1ql6tv9mi7/VrDZHE+m7osIrUD1a2RFOscZYa9ZbfDywOapMSCqcqVAFXCZ0yAyRfgdQyFbXHuNmaIMg1Y4NaZJQSySR7SyPG4AGQVhGVW655hxIkSqrPJuu+lrpI6E7Z2p9doOBKk2umbxG5wEhBRlydYzG4cS9FObnd8izmJSmPqDsWizUlA7Fc0Oz2ZjTFa7aH9CjF3sgaJJC2+xzTTkbEB238tHMUnbcoiTm4gUcrf2ZvyHd3tYmHeg8bJtBHAzVRiga1mjySSWIPGWzXIXSwa274fUhm/iLx+jsr3vH7HkoNZ2UkKS93xJJSQB445AGUdQeQahd62jdYj0rj9strR4e8uNS63sr23ePr49rK2BrV1JuY8dJRbqtI8wSS6nIo0FqlzfCYFKAB9B5eHX1vN+yD9nRPlYYWa49EL7kcT+p8gLRswya9vu6i9YHh3l2vGe5de7tflk0wFj6pU1JJU4SxP4ig4i1+lrG1Q2k+wiVZsO0HTjbUl7D6erdeoisFfImGo0jNK7Mtdhf19LVcBK60gqjTLrKyvUYrJFQbpnYncFI5VApucK0LM+k4mgcbz+Rj19QKCDtI8LSTNZ2CqusyDKW6Rsjuf/Ua6lMabKlZlGsmlPPslvUsCxNH23ZkUlBKGkURuGdmjBSwaxtKR9q6lAMbCS1UXZFSuFelqTEH11OtaOnZ5U+xXy0bUJCIl2F7kNdKw0o/ZuUTO1p1BBqs1UTcGSU5Cn4S85iTU/Glk2m1yq8FJUhOwVfPAcwjNCpbuDhMmuB2x7ebbAMcKavuM25VoxIsLFQuTXSSSNXUqCncRnshLD7SHsNZGN01vTB9fNrlfbAZXB6oi9mtPM5KASp72KZvlV2wbPcxMm1KSVTcKKISAiLZRMzYrpQyiSU+lFHubkmp3FRYtaOZ3IbGNZJ1gBkFs7KSS3bSR/sCxsGZkinlMOuJsSWOyIgvF3vrvOCjXKEsEKgMyKCsmbIyoskr1dvFhtt0stWqFfWtS6s8hHbBlvoYlXl3NdlhhnrRBlF3mVusS8VdFMZFGUiI9U6RBMJS/ADDADNpx7jjtiWCKZFb7mSUBlIKZJexuwzupupGQIqXYIg231B+I8c7wuV6I8YOQYNi1rjEMFIJKkEqQ1a5+5slI+VpcQJlCxAR0hJEKHEElpEzkjZQx/sOds2ITl4/hBUf1jlaIA77k8lYUt9MmfK36cVv/hFXGuugthYPK1z88VTEfqzb+fnoKqid2EnKapNGTK0G4WkzRUXUarDNmhE6S3rB25ZCfdqHVWkGj+ylMYnTEeDjmUUEQACly9uqJdiOQczmQysw+1IyHjEC/S4V8T9ihDclqqahMSMvA11j7dj90jmz90/4bkZj7mJXoDW13qNIyD3WLts8UVUbRdmkGUYKnMIJtDs496ogkYw8BTI8dqm4B8AJhyzucwwOfvMZB+ZAdgL/AD49IPyAHQVSgJXbmiX9lZG/8myDD5dFVrdbsSetbSZQq7TFKYpTFKYpUdt8B9V1O0Vfu+w+pK7NwHfdDuuy8xGuY7u+26zbuO37jn6fUT5+HDmLx4hV39b3ujNp5Y96J0va9slK3tcXte9ri/zq3obXst6Hdxy7MqPa9r4MGtexte1r2NvkaoYdCWiBa6gkNebHja5dNWavS1K5mLFRl7ZWbdV+wrqS5n9VaXSrSEZIJS9ZbvWqiEsIIiJ0lQcEEOF3cll2fKbvkIzhHvAZpYMUKSTPE8bcAOnflU5q6OrD0AqDVLXRYvFanjZSzHSYtGwOI9ccccyutiWSQQwtbIFGjUq3LXxdV9a5ehSOr5qn7GbkmKcwv0Xd3llpYTJL622fcoy93N5Htoi0VgtMmFbAxUMxVKMg2apKgQ7dfk4mrtrxBJtSAyReNl8dr6ahGtLEuqsiwyRzWsJPxXLlo2DEgqEsLebfd3EeeUp+bN5Cbc7mJwM00IgYNFkLxrGkSoqyIQIhdjkxNa0L0gJTbDAWJ1dadISlfidgw57FF6iQhL7c07/AysM4lNmXt1eJ6XuM5GqvyLJqFKyanAhy9uUyhVEudiET+L2vHIEhXZ8WdMiNcIlOUDd4RXJZz2TmHka5e6GMBhJqavkPbeb1/MMpkOv5Vd0B2u5x7v4AktZYwJQFxTgIMg5xK3/PaR8369RuhvqftvH0mlU76q8L1ut9HkgieR8H5ZLp+R8L/I94bo9X8Z+X72ntbfufLr5XHHHyEW1je9+1spsYZWH3YYZW4vlibYnH04faeObQvlfRl18un7XXeDO3P255Y35tjkL3Ew2zrpPaNMcVgJp3WpJCWr1lrtiZtkXy0FZ6nOMLFX5M8a5Om1lGreUjU+u2UMUq6InJzEEQOXNdJBsa+5rsF2dabuLkuSm6PG6ut1JV4pJENmUjLJSCBVoGN9bY0tgFtPa13hkAOJKtYgq1jiyuquCB1UA8EiqgNoS/P3V4tdj2NQLPsG7xdVqbpeZ0uo91k2oFWXmH5amfXLvZbmVkPMS085dO3S1gE4qCQqZE0ycg9iKKPWk1YVHb2Nz3E4cZiR1hEMQUenARKqlCcyTkWJy44UyGdJ5GYNBrtFCU9DKZJEeaRz6hI0ojRCMUCqPQFJN8LVPWCy0Y2t5Oq7LgmM7Q53ZD5Rq41y9cUIa7s5xEry9QptOQ2Iyf0eIhRhURjChLP0mxzKCdJUh+mFvVmXW2Udu5JrHRTVmya80qJtDaVu8V9Mim8Ks0cgEPbUgmMMYZYRLFKLIk7ba7EeC4xo66z65DR3JcOHMj2dCZCx4vxKaRoSegNuH21YbdS3cmMNPQ7pHX2rVNavrh5p4wcoyGzpMt7tTa7v4QjHgzOVmw6Sipzh8DyZDosNLWnhsGaeKNWA4iVkfMzJFzjM5ujPmfw2ZLEEETbl9qWJl4jilZgT6pMSjIITJ6bxLkGxx5ZEbgg3sfbGqYXa8AnFSK6kfIsFTuYaYRSKsowcKlKRYircx0wcs3JSFBRPmIIiUogYBAMrvGe4J47CYC30I64n+sHqp+YLAzRyhUaJxeJrH6gi9iD9LkEdCCehsRquHqpsqUJBwU/dKwFWgDviRosUXriRZtpF13b3ptTxEcVdVwuHNwVdnAg/hHh8DcV43l784Pc7apx/2rcqt+BxkfVYnnkGwFVzkiGKC2DSF+ePUQqk/H+6q8XA4+Fya3TplQhqJXI6sQKJ04+OTMAKLCU7l04VOKjh47UKUgKOXCphMYQAAAOBSgBQAATzNO4Y8KAAoHQAfAfruT8ySTya4hi7SkE3cm5PzJ/sAsAPgABUoyGpaYpTFKYpTFKYpTFKYpTFKYpTFKYpULlti0WCuFb1/M2yDjLrcGsg9rFZeP0UJabbRRQM+Vj2pzAZYES8RAPxHAh+UDch+X2EHYlkgh9UsUQkcDqqElQx+lwf0AEngE15KRBCmxN6YJJO2rHgF7A4g/PlR+llHVlBmmeV7TFKYpTFKYpTFKYpTFKYpTFKYpTFKYpUdt751F1O0SbFXoPo6uzT5mvyJq9F00jXLhur01iKIqdNZMB5TlMUeHAQEPjMzzWxNqeG29vXOOxFqyupsDZlRipsQQbEA2IIPxFbH8PasG95/R0tpctWbchR1uRdXkVWFwQRcEi4II+BBryr/xMfdz/Wv8t9Sf5CzTrHraSg+6vuRX4mAs+x9iyFxtOwEkm2mNFxuvdcNZ++OJQws4622kYSktJ6EohXB+LJNuqhITixQBuZJsCjkJ5oZFc+L11D+akTkE2TVRly7kpuAZinriiYhUX8fZtGEiniiliwPktpu34eJj6v72w6NYxRcH8MMCksoB9QMMN5sjF0G9E9WeQZ3HdmzdiI7k3FM3mSQsD4F0peq64tUbFoR8pDUlx0zRgyjOKkSR7x/GCVn0EgZtjGbo86s0a62h4nX1fGkvqyrJIJzfKUPIUkKFvX2pJIciTYzlElIxEYWu/ut7yMs/kFEZjwT29haMqBLGZF5HdjEnoXloC8oZjK8lukGUqu0xSmKUxSmKUxSmKUxSmKUxSmKUxSmKVEr9/cS6/wCErH/Y7zMb+I/+Pb/7lP8A5TV9B/Cf/KvGf/Q1/wDOSvE1S5OJhLhVJmfZDJQUTZIOSmo4rVq+M/imMm1dSDIrJ6oiyeC6aJHICSxypKc3KYQKIjn03jtiPV3otmW/bRweFVyLdCEYhWsecWIBtYmvlPIQzbGjNr67Y7DxOqm5FmKkA3FyLH4gEj4Vstdd91uMC2Sur5i+2fZuyVpNO6bm2NAQlUtMPWXpO0Ck68ga/cbwzrbJ7Hfzd9IlflcqMgKybItGwKlXx1hl9oPGmw0yMp2yZpdqQsWbvMwuIy3rdMnaeUl5pGULGNWSWJ9s74FpkYDXjACxasagBO2BwZFHojbFFgRQYkEhzXt9/wCR/wD1PU/3QuP9RrufR+T/ANF479zb/dbNYmj/AKrc/eV/28FdPsx60aYpTFKYpTFKYpTFKYpTFKYpTFKYpTFKxM/F+cgpqE6/a+YiZKL7npdft/IM1mnX6PUR63R63Ny85ebhw4h+nKXktP8AMPHT6GWHfhePK18c1K3tcXte9ri/zFaHid/8r8rreTw7nttiOXG+OXbcPjexte1r2Nutj0rhz/BQ/eX/ACa/atl2s+n8FD95f8mv2rYpXT31H9b/APixqc2r/rP675rRM2Tzn079McPLIRyPZeM87Yf6P2HHqdx9/n/CHD5ubO37mHXhxt7eEx3vfK8sst+gt+0xtz0vfmwrQa/ZlmkvfvSB7W6WjRLdefsvfjra3Fzs/lOrNMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUr/2Q==",
            reach:"data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAyMSA3OS4xNTU3NzIsIDIwMTQvMDEvMTMtMTk6NDQ6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE0IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4MTQ3MjEyODlGRDIxMUU1ODAwRkIxQTkxQTRCNTc1RCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4MTQ3MjEyOTlGRDIxMUU1ODAwRkIxQTkxQTRCNTc1RCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjgxNDcyMTI2OUZEMjExRTU4MDBGQjFBOTFBNEI1NzVEIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjgxNDcyMTI3OUZEMjExRTU4MDBGQjFBOTFBNEI1NzVEIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPwCBAwERAAIRAQMRAf/EAIcAAQADAQEBAQAAAAAAAAAAAAAHCAkGBQQKAQEBAQEBAQAAAAAAAAAAAAAABAMCBQEQAAAGAgICAwACAwAAAAAAAAECAwQFBgAHEQgSEyEUFSIWQRcJEQACAgIBAwMDAgUDBQAAAAABAgADEQQSITETQSIFUTIUcSPwYUJSFYGRJGIzY1Ql/9oADAMBAAIRAxEAPwD9/GIjERiIxEYiMRGIjERiIxEr9c+y2s6XLLwi68vPP2ihkXxa4zaPEGTgg8HbrOn0hHN1Fkx+DgkZTwMAlNwYBAOKnF45V9az2b0P6epH0IGCOoJmllTVdHwH/t9R+voP0JyPUCd7r7aVN2YyWdViQMou1Aovop6kDWVYgcRAhnDbzUIdI4h8KJHUS5+PLy5DKXodEFvQ1k4yPQ98H1B+mR1wcZAMnW1Wc1nIcDOD6j6j0I+vqMjOMiSJmM0jERiIxEYiMRGIjERiJXXtDd5bX+smc5FXf/XBXOw9Z1+ZuvhVTf16tWO7Q0NY5H23WInawz9EO7WN9h21VSQ48xD4yZ3H+S0deyzx612xYrn2jIXU2bVXLA4zZWnbBP2g9ZQFcfFfI7VNXl3NfSNlS4Y+8W1L9qEFvazDH889wJEmuuwi0EntR3L3aa35r+JudCqWnrxXoSmr2HZNpuEcoEvRoN/T2dN15aHFelEk+X7dNi1RIqqDlYPQcxbkW59TXDUsN6/c2KqwPZ5Kaqku8zLa3sRANkNaWWq1dctUM9GnZal2LXS4HUp06rbckN47Htsr8Q8almd/+OVqAaxWuUHp1E3xW9E56AnJCF1dtGStdXuAUez6ySa0VvdoCaPGt5lFaQdP760oQwrmHet3SL5GcVaqpOE/A4mEShkSGop2tc+XVuNg5L0CNSzpaj8+BDI6FSFDZJXjkMDOFcee3Wu/bvqrrfB680t6VshTkCCeQOSOBrsD8eBnhI9m63JRtPNXKFsu0W+5PLsyba1iY6pNbnCKa4mla5eFrKvYLnCUuJZ1+eIVoZYZg6bldZMrYVhN8cLYlzp+IfNrtpVbXkT7Fpux4iefFuVh5KqBS5NdhxxXJ+l0r8i3kV3V7f43E9S13iF/FCnJGHgZLi4bgtbqWIJwI5W7Mz1P2BvMtroezp2j0ON1vY3X4MNQTp6pgZuhsJuxHtLha1xD+bcIP1l13CUWeeWbpN1TEAEATE9afjDWNdzKtn+ZfWF5J8TCyn48UIoHvP711jM/iHEXVh2xxVenW1rqloVmsf45bvCMeQuLtsP1J4cmSutETye9lIrVjyJs9saVesNcXCYhDnM8Qq0q8YLI+QHTEWCpyOkhDgxToJm9hR/wJc835JWrpamz25sRG/RrFRx/sSM+ncTX4i7X3Wq2qWD671+Ss+j+wvWOvo54jr9ZmPqC3P4CWesmsxC1hKUBF/MWqRQQXlG8VBFcSTuGifurEbLrT4lBIW4h5OVPWXyAOefWDVLT+8M61R5sijrb2QV49R7s/wDSOTdQMSSxHsu5Kf8AkWDgrknFZY5NmfQgDGTn6DBOZ1epLP8Ab7DMJesMjQ8XY7BLp/kIkKCaULIJulztlUW4FRKRApCq8FDwTOQBD4KGffiamSh9a85TwWE9TgFVZ0AJ74YKgJ6sOndo+Uet3W6voVtrAOACcla2OOvVwWyOuOXfIzNSMim0YiMRGIjERiIxEYiMRI32hr7/AGRCQUP+v+N+LfaDd/sfQ/R+z/R7XF2f8z0/dY+n9T830e7yP6PPz9anHgPKLw39Xe/9ayx+P93PXvoxn0x5uecHPHjgZyF37vxm78d2/M1TTy/szZW/LH9X2Yxle+c9MGBb/wBSY26t77FI2iOZ1i1bArO3IKoztGjrlWq7smLI4Qs76RhpiVJG2ip3tBbzfRJ0mopvDquEnJTq8F511Ovra2v7LBp7FrUB1DIlF1divrlQQeKPddZQ6NW1LOAOXBSNbbBbdbb7lOzqJVcVbDM9NiNTcmBwVglddVwdLReiDkQc556W6cklKzXIVFxoeCPB3d5bpGsVzrknCaZuKbivDBNGlz1Uw2ikvY5KHUMLpo8czShUlR8RQEgAGauWa2lyeVVVVycX94VrmqJtpBwtVirUE5FXJVmye2MVGKrqz7Wtag8k9jkU+X9uxurWVObeRQFAGRSPXPtVHqvO61jaGvrfZEBAW+juNpNSvpDVzd3RparbUtv9yka2bX1duNQCFTgpZu1GPUj5JsmiRASCiZM/rJypsqceJ2NJ0adZ/ITY7DXZ2pt8hIItAssDlg62cySoIGMF1q2a17wOTb42qwg4LW506tS1Me4vXYtQfBYMp45dmDO/cyOhJCZhOwEfLXdBeV35SomrSco1qwtG0DIMdcjQ3k03jBsTgX6D5ycz4jQXCHpKIIe4/HuHrcCbOgnx9QKVV/MjfBJ5HC/4/FJ6Lk40P+5/5c+P2e+3Vuejfo37MNZVrV1ED2hiluxaWH3cQ3nxx92OOeR5YFgkmCJY1KMcFI6blYkYLlVTD1uUQQBuqVRIwnL4LE5ASiIhwPHzn3aK7b2M49lpbIz6NnIz0+vfpPN+I1G+J+O1dFH5PrU11h8ccmtVUNjJxnGcZOO2TKXzfVu0ws9JyWrLVERsdMMZKNcMLCRcFmcbLJGQfRqDpKLmCum6iJxKRQU0Vky8B5CYPMc6/J4G1bzzpYAE9iwVg65x6hlUkgjJHYA4npWNW1q7NY43K3ID0BwRkfywT7SDj6n0krSfXpnrB0exTUgjN2pRuo1QO1SOSMiUVv4uAZCuUrhy5XIHiZY5U+CCJQIHIia1thVpNNQPuxyY9yBg8QPQZ6nqScDtjBj8TPaLLcYQ5UD64IyT0z0JAGMDJPU4Ispkk3jERiIxEYiMRGIjERiIxEYiMRGIjERiIxEYiMRGIjERiIxEYiMRGIjERiIxE+Z66KxZu3p0nK5GbZd0dBk2WePFit0jqmSaM25FHDpyoBOE00ymOcwgUoCIhmV9vgpe7iz8FLcVGWOBnCj1Y9gPU9JpTWbrVpBVS7AZY4UZOMsT0AHcn0HWYH7F76duOx+wZzWHU2hzVKjYZw+bSL5aHjT3RBm2UUaOX9ymrKBqxrlqVVI38TGRWbq/wF2c/BQy1KNnf1jv2uqaPENlXArC45Dld05MQCwFZXIyALAOR23LNfQv/BVGbd5ccMpLlgcEJVjIXJAJcN6MfHniIc/pXbtVJV5J/wDR/UsHPlOf3VB/3WmW02k6A3AshZRrlzXUlxUESgT7IEAQ45D4yr2qAdci7oMceuf5e/GT/GSZMeRY/kDxH15DGP8ARc4H8dp3cJ3G7zdQpavNuwkIptLWs4Kf5UzJu4iZQmWJwBYzinbYqwvmEnIigIKemQUfKAmPAppeQHLvTt61mx+DvVOmzjsF4WDt1CdEtUcgTxwScL5VzMH1bxT+ZpWK9GcH3c0Jychj1esniwXPQYZhW+DN9aHb2d/plZu0fGTsMxtEMxmmsXZopeEnmCL5EqxG0pGOOVGzlMDcDwY6Zw4Omc5DFMPW5qvpbL6rlWdDgkdu2cdcEEdmUgMrAqwBBE409kbmsuyFKhs9Dg9iRkEdGVsckYdGUhvWdZk0pjERiIxEYiMRGIjERiIxEYiMRGImeXeTqXft6UgsbpGzxlHfS1nbyuwqqVFlXIDZRnX0o8LBbJqIjf2ZSUrjVAqhU3RnKK6CYgVMFyJiaF9Zm29fyMP8ZWzkqfspZyXe9FHQux6PkFuoZWU+QWWrtqmpf7T+eyKA4wXsSsYXXZmPtQdfHghQSVcFSrVY+u9G/wDPOquV9fW/s9s97sRkupGS13q1GKtqyFmkFDIO2x4tSCf2CYaMnKZiCu1fikqHBgMUPIC2JbR8gqv8WxWlvse0YFgIyGCgqU5Z/rbAOTyK4YyPVfosyfIqGuU4ZKz1Q5wwLEEPx6/aoY4+0N7Zpn0Z6UXrT6lwNsfYcBsXTsw4i5TX9Iatm9hptmVIePnYLaC8ZYGbxKtyzYhUgbEZiC5liidRY6SSIqeouxdq6Y1t6vj8zReSuTk6r1syE02DDcrMt24qEbLKbW/Zgaqm7aOxoWZ+LvoAY44/kJYAQltZyOK9D15EsMBggIs1czzZbGIjERiIxEYiMRGIjERiIxEYiMRGIjESntw6M9fLxvSI35PVUFrIwAzuVr6f1yVG1WBE6J4yy2OHFuYH0mwAhhOAHKi8OBDOCKCU3m+P/wDmXWX6nR36r9K3JJaysf0u2e/UK2bEC2kvPu8T8hRXRs9VToSO7oBha3Pqi+nZiv7bE1AILhYnwDHQdoxEYiMRGIjERiIxEYiMRGIjERiIxEYiMRGIjERiIxEYiMRGIjERiJ//2Q==",
            share:"data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAyMSA3OS4xNTU3NzIsIDIwMTQvMDEvMTMtMTk6NDQ6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE0IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxNERDMzE5RTlGRDIxMUU1QTFGMTkxODI0MTJFMzZDMyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxNERDMzE5RjlGRDIxMUU1QTFGMTkxODI0MTJFMzZDMyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjE0REMzMTlDOUZEMjExRTVBMUYxOTE4MjQxMkUzNkMzIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjE0REMzMTlEOUZEMjExRTVBMUYxOTE4MjQxMkUzNkMzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPwCBAwERAAIRAQMRAf/EAIsAAQACAgMBAQAAAAAAAAAAAAAGCAcJBAUKAQMBAQEBAAMBAAAAAAAAAAAAAAAEAwECBQYQAAEEAwABAwMEAgMAAAAAAAUCAwQGAAEHCBESEyIUFSExFglBI2EyFxEAAgEEAQQBAwMDBQEAAAAAAQIDABESBCExIhMFQVEjFGFxMpFCFYFSYjMkQ//aAAwDAQACEQMRAD8A9/GKUxSmKUxSmKUxSmKUxSmKUxSmKUxSsBXbyS5rRyzwOU+WOEojimZ7VdiRZjUB9G/RceRKmzx0VT7e/wBFpaW5ttWtpV7Va3rOkTicZR3Mfw3wf2+SP1AsfgmtJI2j/nYP9Pn/AF+n7E3/AE6VNue9WpnTYrz9YIOKlREpXOET2tRC0JCle1Djsb5HW3WFb9NfIyt1vW96TtWlfplLwOiCXgxk2uPg9bH5B+lxzY2vY1OsqlzGbh+vPyPqPg/r8ji4FxfI+Y1pTFKYpTFK+b1663rW9p3vW9aVr09dev8AnXu1vXrr/nW9ZnKjSRNGjtG7KQGXHJSRYMMgy3HUZKy36gjiuRwb9aotHNXan9i61AvHkn1B3m3Heb896hOSQrXB0KIxjM++rsQ03ICcRgE3xO4NRZQy2PVEn+rrmkv7WpvaOkG3HFo7e7soZGg311kQAkyeTX13QWUgmVpp8UsVUnAFepPbf15pfZet9dpHx/5DR2p2Yc+NoNmOIFSwYCNY2ZnMgkPblkACDNj/AJYwQFat5UhyDqgOwAOYmesV2p25FGByrzUgOo/5meHKDrpYRw1QBE6O8ShzlRzMaM8lSILzu0sq7bUjapaMqG2otnXhlQEXi/KkMUTsRdGRpQYg0TOBJYOUQlx102i3JdXxtfT3CwilAujsq54AEqwd47vGHCBlVjkApInhftR4JUhtqncE7I9uUOMlyoQY7yIiTrYoI1HfeIFZTfWEAZCJ8d/5IkeFMlz30Nr19ulado1puvDoSSCd8daGPOSQg4xjJ1KsACzMoTNxEJFVGU5EkgY+meX3errzaihtjZ8YjRST5DKispjLKnaS2CmUROWHMagi/A5r0+dfesXaKPLLnc/Vyfil7p0RwfFiOM6v273Llz1u/aNFFLJQBcL1ZkOLSztv6Eo2pfu1SCWEb8OyQdjW9q+uCOgVdXVcqOlx5JHYMRl3WvYADiedHf1cmsCsG36uWdgepYbOCE8mxEfaQpx+eTzWLqvfrbM7dYKp0js9k5sfa6MVZ59yWVS6QNo3ReaQUsqCyK7cDtMmH7WdNRPe7M0NsLcmE8lxvcNtKPXfT1A8+ojtef2Cx7B2Ij2mIiSZY2iUYu0KRiOXykOrgkNjzbX2BMM5FhFpEa4ikXuV2ZI2kWRiWVXaQyRCO0bAYFMiRXI4r5LEi4qojeh1XoC37X0zoPPBXUnw1NhUUofFW+6pAAGo4uwR7SztAEEiG3NWDbgvyo6k7krc2pe+PVpJv6mjbjc2PVQ7Fm/+pXUjn2WXG6oeJphG/jLRozRIUC3q90sWh7T2UMIy1dX2s8Fl58SHcaGBSWILgK0KMymQqWAkIfO2W3e7Qh11jUyx856VVNF1XFqrWc3Cpyq3bpFHHSzBiOH0GuhexQVyBEB6VDWUHj2pTLe9oXve9a3I+5FF6+ffmDI0Gk200RH3TEjIj2AJQOrSxgozqwyHHW3MenNNtQa8ADxz7UeuJBfxrLKkkiBrgPYrFJyqMLqRUVEeUgE5Uatbx3MetOx+hzxA7l4VwbR49i6Q8TFEDcqRXx0i/NNiRYEWMefmTTjoiH8OkuMOvtrQtV88UuvuLoFSdxhK+IsMYYghMzliqqjeRAiMRPk6o0Sucajjkik15dvMDUiZI2ezFTO7Mn462BLSqyOGsDH2OVdlRyPwZ8sKoSerwet8+6dZ7qcXeGZnPR8WhibZWZXOSUMVbYBvduv9arcogPmT2vYwLIknZDKvmaStr686AeQhoPua34sewZFvYRyySwi6NjLks0M0Ui+O8bxkNa6ZayI0K32PtyHabXCkg5SLEk3DqWjwMUsUiuXCuJAFJZZFTN1yMEo3OrKdHRZkAq1USZOLEk6ZQQHy/wAS7JQ3ITGeksamQF7+tLbjifejekqVr03ub2qeGKSFXVlEgQstypUuEZlJANsSSCQLDkgVp61lmmid1K5WbFgL3tcIw5FybKeo/cVq/wCR2wiDNTYcMyFrCjSW3ydwKNNPERowR85WdCFqlvIjOyTmmvh2yrW1SXNoRpSfXPUVo0gtIL60R8jIoF5QoxEVvkG9wBa3U3AtUMqvJMGXnYkBRWY8RlyCZSfgqAeTfgkAZEGpXzW0pm+RAw5VYSgwywWiWygW022lKAxLTqZDL0eNrTCNfF/u2lPqhpetb1vek63j1UbpE+vLyDDKSLkgWVpEAPFwjBQDxcDoL2rt7N0dllS91kiAPQk3VGY/q4LE9f5GtpuR1rTFKYpTFKYpVf7RwEVcbJ2cscOylCOy8urHMZ4iDD1EmhI9dXdFqMwzCpkhEqVK3b9bbaVFQhlcXW1bd05tKZTrMNLY1o3KzS+wj20cAfbkhj11jGJuHAfXWQ3IBBKW/uNDbDH2Xr/YADHS0tnWZDz5U2pVkfn+zsVo7AMTlkGUi1Qaf41Wu7NGVdb6vDuE1XH7tx2pya5z1FLaAj+gQoUGzWsvGkW+17sNrnsCYafcy4NgIQ0vTcVG3dqTrtRLONqeP7Xsdt9VmdeUUac/5USLGbnA7FncNIzMAFVlAvUmii6D6MUVz6/QkzRD/wBjsEMSF5RYHCJpEUBACXLtkwFRe+eJVr6hNrxPoPROa2+YKokuhS41i4Qs9XYrL85yQ3cqTWDvUyYmpdD3DXpl4jIQVjubbb2iK0hGm853YYtuTclQGOXdVLsDd4mUzXXXc90UL+Rck7pckyGwCwx59YP8Tqaenrd0Gi/20fujdAkKKs8Z4mZPExDHFCJCpiNiWzVyXieuWlVEtWXZzW+T8h5fpjYb8bvWuUwLBB0c27+Vn+uz35z3fbe3X2vxenyve71TZLP5Zt2Yix3PYPtW/wBmcMMWH/K3iyy4vlbHi5yMAx0FU2XR0G1h855S+XP4xt/HHm/W46V01o450W/WEBq8dQrBHn1X6UM6QHrgblrwW4bkVss4YqoQhepXQTQxyALkfEiQ9FBQpcxltSNut/Ive5NJWgk19raPl3deNwGXsVnkheBpHTvJ7JHIRWVMiCQQoWttsefUn0YrLrbKokmQybBZI5GVGGOBZoxZrFlBIBvzQd4+/YU3mFS/l23f/N+wkusfkdAfj2Z/IWO82D8DqL+Zc/HbZ/mfxfd/I/7vtvf8Ovk9qNNI/iDUW7f+X1zat1OLHL1kvrfIps2BHk84Wzcr4785ij2E358+/NbH87dbYt1wy303sPjK2Piy7b3zsP4Vh6g+E6abb63bZdzp5QlWXugK/kULkjAro9ybv1esYJ9zpPRZd2OlrXPEqONusqaagRFaYUlUb3rQ6xFNqCX1M/rFIjE3qjpHxrjGD9k+cx3JaVjCS5eQ3LnAxgsH3TdMftY/Y45pH7IbYDnKQYiUCFZbdkQWUgKqAdiFgxUWnVw8Uxds5PxXnkozWSZTiLABsMRu/ORl7pVjWKq7tUINWjnJYzHZmQCsJ5TqEtEmZUOQltbUj3I3tV+8/wCX7n/MqqByksZRxmuEpjY2IwZZFeKMq4IFslZWVyB5+ip1PUzeoZnMMuyJwVOBEiySst+GDKEnljZDw2QftZVt1R3xZKleeROdQJnjuBCOLPSiw4V4wR4VfiGC+2molnogUd1ce/TLWJhtaQ3Oclk3FuJQ5vWtoSnMtlPyWjBeVVjhCIyvaaNvJI7tDIBaMPkvYENmTIli7Ct9eX8dGxRC7TF3Ui8Uq4RqiTR37yhVznmt1cJYBAatfXwaQlZCVqTPmnkiAQ0G+TNOfeETKYA9mA7PLPL1vUqaR01tx9W/+7i1b3++Ub8ke9sTTMipFM7nAdAHJOI6cAGw6cVJoxNpa8MKMS8KIoboboAMuvB4v14+tVDPeLlmDWKeZ5TaBAmGUik4Lo6wIf8AfAgl47kWePiymxhlMqM4w8pKFLabeaT6fWpWvfueLyCB9WY5wuuJP9zLcNY/rdQSwIv9B81yNG0qbCDGZGyAHQNYi4/qe0ggfB6AT/injtE5nO3ZTxFg3aPgdjxPs23ECxDb6dtvrirkJRIlyn2t7T8qkNaShSk6R+vu3adhViMUIN3/AJMetrg4gc2Fxcm9zYdBcGMxNJIHltipuoH1+p/rwOg68m2NmskremKUxSmKUxSmKUxSmKUxSmKV1Zw2KrQUvYjs5gWEAjJxkwSlK2iMPGDIrs2fNfVrSt6ZjRWVLV6a3v0T+2YbOzBpwNs7DBYVHJ/c2AAHJZiQqqLlmIUAkgVrBBLszJrwi8rsFAuByTYcmwH7kgDqTaqVcP8A7EPHfvvSVcsqD9xE2KVqduvSbYCgixFsUPbekvtApEM0Ultvqgx1vobnsQnFtp3rWvk+jLtTWm29Z541s8aZNGbZhehbi6kKSMgrEgHKxVWIl254dOZY5GBjZwgcfwyJsou1mGZ7Vuti1lvdlBvTmFa0xSmKUxSmKUxSmKUxSmKUxSmKUxSmKUxSqjl/O/xHBWaw1Ar22tRDtWSR/Ms7gWR0e08KSrc6FCOsBHQJgk2pG0Jiw5MiQ67rbaEKX9OYRbEU+q27CS+uACCAbsCwUFF/k4JIOSArjd74AsN5daaDZGpMMZz8EjtOJYhzeyEAWIcqQ3YRmQtVNt/Yu/ed4g1znxyoKuecDskeYAtvdeqDtMu2EO+lcYnDpdc9X1PokNpcbS41qS767Tpx0cv13nLeqm3o0k9wfx9DJJFjHdK5Rw6E2IAUMFawKoShtM4JjJPaR6ErD1X/AKPYAOhflYkyUqwuRe5U27gWAf8A6eA4x74p/wBW954x3cD1Hol+ppmv0WbLKVcfUtnlljZLceTDGvHGygoZFCR4qZGpC22H5/yOI017vbva89b1e+2hHJOVtvvBJEAOUUTI0cpyIBY+NmRRgOWzuCgVvN9jq/lOkMLH8RZUcsRZz43V0GPcoyZRkcjZQQOWyXdZnn1XTFKYpTFKYpTFKYpTFKYpTFKYpTFKYpXHmR/u4kqJ8z0f7qO/H+4jr+OQx8zSm/mYc9N+x5r3e5O/8K1reR+w1Pz9CfRyw80Lx5WvjmpW9ri9r3tcfvW2vN+PsR7GKvg6tiwurYkGzD5BtYj5FeY5f9X3lwB6VPcE0+mWkDXS75wNYT1sr38cubQuT+RgC5gRwkmxtvHtNJYdalRo8fS1qSuQhv8A25Z6fbm0Fh3pYI22ddkPhNmSQqygheQpTq3eUJQWtnZC9vHDuzzRQyuNfYL/AHOVeMOGN24JzF7XQOM7EErcjbxxnzmpko1E493qmyvGPrQ1qKNj1a2pSPpBltlGokZ2nWZxmILbgSVM+kVl3aWVJ2huNIlb+rK4zB7UtP6+RpJy12jkuJgW5sQQC5II6hZGJv4gOagkWf1arFuxqmuB2yR2MRAsD/EkKAb3ILRqB3SA8VcWtdT5jdDBOvU7o1Dth8Jp1RkHWrfXzpgSll/UV7ZMYLIypsDTUlWm1fKhHtXv27/X9MkjBmg/Jh79bjvHK88juHHIBI55txVMpEM3483ZOb9rcNx14PPHzxxU8xSmKUxSmKUxSmKUxSmKUxSmKUxSmKUxSmKUxSsH+QvD6p37l9rotiB10iUnV85Hp5o6NRMcqdnmDnWRJ8dKQjZAe5Dn6aW7uOpKnWkbQrS0b2jfm+z05tmDPTYR76MrI31CurNGT1CShSjcEANlixUCrdHZSCdRsXbSLDyKLG69CQCQCwBOJuCD0ZetabfCb+v3yb5P5KVboN/FD6TUqO8akSS0O2ATC7a1JFzxLQoVAAlJZBEQiqYlx3ZBqIlMdKvVO3Pa3v6P1W5FrwTzbC90uu0YiPJycCzMRkloiA4sxJkVMeLsvie01pJniggIITYR/KLgBY2DGwOL/cAMR4Ha7ZXHa3oCzzqupilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUpilMUr//Z"

        }
    };
  
}

ShareOfVoice.prototype = {

 createSingleQueryCall :function (queryName, duration) {
    globalDuration = duration;
    var esClientFb = null;
    var esClientInstagram = null;
    var esClientYt = null;
    var esTwitterClient = null;
    var esClientMedia = null;
    var esInstances = null;
    var fbQueryVoice = null;
    var twitterQueryVoice = null;
    var mediaQueryVoice = null;
// alert(globalStartDuration);
    esClientFb = new Elasticsearch(esHost, fbQueryVoice,this.liveParametersTagCompare,"fbQueryVoice");
    esClientInstagram = new Elasticsearch(esHost + '/instagram', function () {
        return {}
    }, {},"instagramQueryVoice");
    esClientYt = new Elasticsearch(esHost + '/youtube/video', function () {
        return {}
    }, {},"youtubeQueryVoice");
    esTwitterClient = new Elasticsearch(esHost, twitterQueryVoice, this.liveParametersTagCompare,"twitterQueryVoice");
    esClientMedia = new Elasticsearch(esMediaHost + '/mediabuzz-new', mediaQueryVoice, this.liveParametersTagCompare,"mediaQueryVoice");

    esClientFb.addIndexPattern("fbdata-",facebookTimeFormat);
    esClientFb.addIndexType("post");
    esClientFb.addTermMatch("Page.name.raw", getFacebookKeywords(queryName));
    esClientFb.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);

    //CALL-02 INSTAGRAM
    esClientInstagram.addTermMatch("ownedBy.name", user);
    esClientInstagram.addTermMatch("ownedBy.query", queryName.toLowerCase());
    esClientInstagram.addDateRangeMatch("created_time",globalStartDuration,globalEndDuration);

    //CALL-03 YOUTUBE

    esClientYt.addTermMatch("ownedBy.name", user);
    esClientYt.addTermMatch("ownedBy.query", queryName.toLowerCase());
    esClientYt.addDateRangeMatch("snippet.publishedAt",globalStartDuration,globalEndDuration);


    //CALL-04 TWITTER
    esTwitterClient.addIndexPattern("twitter-", twitterTimeFormat);
    esTwitterClient.addTermMatch("ownedBy.id", user + "%%%" + queryName);
    esTwitterClient.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);
    //esTwitterClient.addTermMatch("ownedBy.name", user);
    //esTwitterClient.addTermMatch("ownedBy.query",queryName.toLowerCase());
    //esTwitterClient.addDateRangeMatch("created_at", "now-" + globalDuration, null);

    //CALL-05 MEDIA
    esClientMedia.addDateRangeMatch("actualTimeStamp",globalStartDuration,globalEndDuration);
    var keywords = getKeywords(queryName);
    esClientMedia.addMultiMatch(["Title", "Content"], keywords.join(" "));

    

    esInstances = [esClientFb, esClientInstagram, esClientYt, esTwitterClient, esClientMedia];
//     var clientNamesSov = [esClientFb, esClientInstagram, esClientYt, esTwitterClient, esClientMedia];
//     esInstances = this.esInstancesGenerationSov(clientNamesSov);
     console.log('sov clients',esInstances);
     
    return esInstances;
},

esInstancesGenerationSov : function(clientNamesSov){
    var clientNamesSov = clientNamesSov;
//    var clientNamesSov = [esClientFb, esClientInstagram, esClientYt, esTwitterClient, esClientMedia];
    var splitUserPermissions = {}; //to isolate the permissions list for analytics section
    var namesArray = [];
    this.namesArrayCompare = [];
    var positionsInArray = {};
    var socialMedia = ['Facebook','Twitter','Media','Youtube','Instagram'];
    var clientNamesString = ['esClientFb','esClientInstagram', 'esClientYt', 'esTwitterClient', 'esClientMedia'];
    
    for(var inderPer in userPer){
        var newIndex = inderPer.split(/(?=[A-Z])/);
            if(newIndex[0]=="analytics"){
                splitUserPermissions[inderPer]=userPer[inderPer]
            }
    }
    
    for(var splitIndex in splitUserPermissions){
        if(splitUserPermissions[splitIndex]){
            var partName = splitIndex.split(/(?=[A-Z])/);
            for(var socialMediaIndex=0;socialMediaIndex< socialMedia.length;socialMediaIndex++){
                if(partName[1]==socialMedia[socialMediaIndex]){
                    namesArray.push(clientNamesSov[socialMediaIndex]);
                    this.namesArrayCompare.push(clientNamesString[socialMediaIndex]);
                }
            }
        }
    }

    this.mapForClientsSov = {};
    for(var index=0;index<clientNamesString.length;index++){
        var indexInSplit = this.namesArrayCompare.indexOf(clientNamesString[index]);
            this.mapForClientsSov[clientNamesString[index]] = indexInSplit;
    }
    console.log('map for clients',this.mapForClientsSov);
    console.log('client names in string',this.namesArrayCompare);
    return namesArray;
    
},

sunburstCall : function (tags, duration) {
    globalDuration = duration;
    this.sunburstParameters.user1 = user + '%%%' + tags[0];
    this.sunburstParameters.user2 = user + '%%%' + tags[1];
    var esClientSunburst = null;
    var sunburstQueryFunction = null;
//  sunburstQueryFunction(this.sunburstParameters);
    esClientSunburst = new Elasticsearch(esHost, sunburstQueryFunction,this.sunburstParameters,"sunburstQuery");
    esClientSunburst.addIndexPattern("twitter-", twitterTimeFormat);
    esClientSunburst.addDateRangeMatch("created_at", globalStartDuration,globalEndDuration);
    esClientSunburst.addTermMatch("ownedBy.id" , [ this.sunburstParameters.user1 , this.sunburstParameters.user2] );
    return esClientSunburst;
},     
initTab:function() {
  if (this.isRendered) {
        return;
    }
    this.isRendered = true;
    
    $(".compare-tag1").select2({
        data: this.tagCompareFunction()
    });

    $(".compare-tag2").select2({
        data: this.tagCompareFunction()
    });
// selectedTagHolder=["Ghana Police Octobuz", "BokoHaram"];
    var self=this;
$('#submit-search').on('click', function () {
        //alert("csx z");
        var values = [];
        //      values = $("#compare-box").val();
        var newVal = [];
        newVal.push($(".compare-tag1").find('.select2-chosen').text());
        newVal.push($(".compare-tag2").find('.select2-chosen').text());
        
        self.selectedTagHolder = newVal;

        self.selectedTagHolder =self.selectedTagHolder.filter(function (n) {
            return n != undefined
        });
        console.log("selected tag holder",self.selectedTagHolder);
        
        self.renderShareOfVoice();
        self.titlePrinterFunction();

        

});
},

titlePrinterFunction :function () {
  $('.social-media-stats-name-tag1').html('Social Media Statistics' + ' ' + titilizeName(this.selectedTagHolder[0]));
  $('.social-media-stats-name-tag2').html('Social Media Statistics' + ' ' + titilizeName(this.selectedTagHolder[1]));  
  $('.ssm-voice-pie-tag1').html('Social Media Sentiments' + ' ' + titilizeName(this.selectedTagHolder[0]));
  $('.ssm-voice-pie-tag2').html('Social Media Sentiments' + ' ' + titilizeName(this.selectedTagHolder[1]));
  $('.tweetmap-tag1').html('Tweet Map' + ' ' + titilizeName(this.selectedTagHolder[0]));
  $('.tweetmap-tag2').html('Tweet Map' + ' ' + titilizeName(this.selectedTagHolder[1]));
 
},

onTimeChange:function(startValue,endValue){
    
    this.isRendered = false;
    this.handleLoading(false);
    this.renderShareOfVoice();
},
customTimeChange:function(startValue,endValue){
    
    this.isRendered = false;
    this.handleLoading(false);
    this.renderShareOfVoice();
},




tagCompareFunction:function () {
    var count = 0;
    var sampleArray = [];
    for (count; count < tagValuesArray.length; count++) {
        sampleArray[count] = ({
            id: count,
            text: tagValuesArray[count]
        });

    }
    return sampleArray;
},

 renderShareOfVoice: function(){
	 comparisonCharts=[];
     
var esInstances = [];
var testIdx =[];
        for (var index in this.selectedTagHolder) {
            var esInstancesTemp = this.createSingleQueryCall(this.selectedTagHolder[index],globalDuration);
            for (var idx in esInstancesTemp) {
                esInstances.push(esInstancesTemp[idx]);
                testIdx.push(idx);
            }
        }

            var esInstanceSunburst = this.sunburstCall(this.selectedTagHolder,globalDuration);
            esInstances.push(esInstanceSunburst);   
            console.log("instances tag compare",esInstances);
            console.log("instances tag compare test",testIdx);
        
        
      /*  var esInstanceSunburst = this.sunburstCall(this.selectedTagHolder,globalDuration);
            esInstances.push(esInstanceSunburst);   */
           
        
        this.handleLoading(true);
        var esClientMulti = new ElasticsearchMultiQuery(esHost);
        esClientMulti.applyESInstances(esInstances);
        //esClientMulti.search(this.tagCompareRender);
        var self =this;
    esClientMulti.search(function(response){
        self.tagCompareRender(response);
    });

},
handleLoading:function (isLoading) {

    if (isLoading) {
		$('.dataContentSunburst').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
		$('.dataContentSmsTag1').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
		$('.dataContentSmsTag2').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
		$('.dataContentMapTag1').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
		$('.dataContentMapTag2').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
		$('.dataContentSentimentsTag1').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
		$('.dataContentSentimentsTag2').html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

    } else {
     	$('.dataContentSunburst').empty();
		$('.dataContentSmsTag1').empty();
		$('.dataContentSmsTag2').empty();
		$('.dataContentMapTag1').empty();
		$('.dataContentMapTag2').empty();
		$('.dataContentSentimentsTag1').empty();
		$('.dataContentSentimentsTag2').empty();
    }
},

clientListFiltering : function(){ //here we filter out to create an object removing the -1 values from the mapForClientsSov.
                                  //thus we get the valid clients and their position numbers
    var newClientList = {};
    for(var index in this.mapForClientsSov){
        if(this.mapForClientsSov[index]>=0){
            newClientList[index] = this.mapForClientsSov[index];
        }
    }

    return newClientList;
},

tagCompareRender:function (returnObject) {
    var newClientList = this.clientListFiltering();
//sample newClientList = Object {esTwitterClient: 0, esClientMedia: 2, esClientInstagram: 1}
    console.log("returnObject",returnObject);
    
    for(var index in this.mapForClientsSov){
        if(index.indexOf('Twitter')>=0){
            var indexOfTwitter = this.mapForClientsSov[index]
        }
    }
    
    var incrementValueForTwitter = Object.keys(newClientList).length;
    console.log('increment value',incrementValueForTwitter);
    console.log('index of twitter',indexOfTwitter);
    
    twitterSentiArray=[];
    twitterSentiArray.push(returnObject.responses[3]);
    twitterSentiArray.push(returnObject.responses[8]);
    //SUNBURST GRAPH RENDER FUNCTION (1 SUNBURST GRAPH)
	   this.sunburstDataGen(returnObject); 
    var tagResponseHash = [];
	var count =0;
    //SOCIAL MEDIA STATISTICS (2 PIE CHARTS)
	for (count; count < this.selectedTagHolder.length; count++) {
//		tagResponseHash = initialTreeMapData(tagResponseHash, count, returnObject);
		this.socialMediaStatisticsResponsesGen(count,returnObject);
	}
    //MAP FUNCTION (2 WORLD MAPS)
	this.countryMaps(twitterSentiArray);	
	//SOCIAL MEDIA SENTIMENTS (2 PIE CHARTS)
    this.pieGraphSentimentsTwitterVoice(twitterSentiArray,returnObject);

},


socialMediaStatisticsResponsesGen: function(count,returnObject){
	console.log('inside social media statistics',count)
	var firstVal = count * 5;
	var secondVal = firstVal + 5;
	var finalArray = [];
	finalArray = returnObject.responses.slice(firstVal, secondVal);
	
	var innerCount = 0;
	for(innerCount;innerCount<finalArray.length;innerCount++){
		finalArray[innerCount].tagName = this.selectedTagHolder[count];
	}
	console.log('final array',finalArray);
//	multiResponses.push(finalArray);
	this.multiGraphDataPieVoice(finalArray,count);
},

sunburstDataGenForTags : function(tagParam,returnObject){
var sumPositive=0;
    var sumNegative=0;
    var sumNeutral=0;
    var sumMixed=0;
    var childrenArray = [];
    for(index=0;index<tagParam.length;index++){
        var identifierTag1 = tagParam[index];
        console.log('identifier tag',identifierTag1);
        var bucket = returnObject.responses[identifierTag1].aggregations.sentimentsSunburst.buckets;
        console.log('test bucket',bucket);
        for(var innerIndex in bucket){
            console.log('inner index',innerIndex)
            console.log('inside bucket',bucket[innerIndex].key);
            console.log('inside bucket',bucket[innerIndex].doc_count);
            if(bucket[innerIndex].key=='positive'){
                sumPositive=sumPositive+bucket[innerIndex].doc_count;
            }if(bucket[innerIndex].key=='negative'){
                sumNegative=sumNegative+bucket[innerIndex].doc_count;
            }if(bucket[innerIndex].key=='neutral'){
                sumNeutral=sumNeutral+bucket[innerIndex].doc_count;
            }if(bucket[innerIndex].key=='mixed'){
                sumMixed=sumMixed+bucket[innerIndex].doc_count;
            }
        }

    }
    
    var sentiments = ['positive','negative','neutral','mixed'];
    var sumBucket =[sumPositive,sumNegative,sumNeutral,sumMixed];
    for(var index=0;index<sentiments.length;index++){
        var colorInfo = sentiments[index];
        childrenArray.push({
            name:sentiments[index],
            color: this.colorMapSunburst[colorInfo],
            size:sumBucket[index]
        })
    }
    console.log('child array new',childrenArray);

    return childrenArray;
    
    
    
},

sunburstDataGen : function(returnObject){
    var sunburstResponse = returnObject.responses[10];
    $('#sunburst-graph').empty();
    //new sunburst implementation code fragment starts here
        //initially there was only twitter sentiments to be shown, in the new representation facebook and media sentiments are also being taken into account
        var sunburstFirstParamsTag1 = [0,3,4]; //0 for facebook response-tag1
                                               //3 for twitter response -tag1
                                               //4 for media response   -tag1
        var sunburstFirstParamsTag2 = [5,8,9]; //5 for facebook response-tag2
                                               //8 for twitter response -tag2
                                               //9 for media response   -tag2
        var childrenArray1 = this.sunburstDataGenForTags(sunburstFirstParamsTag1,returnObject);
        var childrenArray2 = this.sunburstDataGenForTags(sunburstFirstParamsTag2,returnObject);
        var mergedArray = [childrenArray1,childrenArray2];
        var l2ChildrenArray = [];
        for(index=0;index<this.selectedTagHolder.length;index++){
                l2ChildrenArray.push({
                    name: this.selectedTagHolder[index],
        //                  color:colorIndexTags[index],
                    children: mergedArray[index]
                })
            }
    //new sunburst implementation ends here
    var sbFirstLevelData = {
            name:"",
            color:"white",
//            children:this.sbSecondLevelDataGen(sunburstResponse,returnObject) // to restore old sunburst implementaion just uncomment here
            children:l2ChildrenArray //new sunburst implemtation being called. To restore the old sunburst please comment here
    }
    
    console.log('the final data structure sunburst is ',sbFirstLevelData)
    drawSunBurst(sbFirstLevelData,null);
},
//old sunburst data generation functions
        //note in old sunburst only twitter sentiments was considered
    sbSecondLevelDataGen : function(response,returnObject){
        console.log('sample twitter response',response)
        var parentArray =[];
        var childrenArray1 = [];
        var childrenArray2 = [];
        var l2ChildrenArray = [];

    for(var innerIndex in response.aggregations.user1.sentiments.buckets){
            var bucket=response.aggregations.user1.sentiments.buckets;
            childrenArray1.push({
                name:bucket[innerIndex].key,
                color:this.colorMapSunburst[bucket[innerIndex].key],
                size:bucket[innerIndex].doc_count
            })
        }

    for(var innerIndex in response.aggregations.user2.sentiments.buckets){
                var bucket=response.aggregations.user2.sentiments.buckets;
                childrenArray2.push({
                    name:bucket[innerIndex].key,
                    color:this.colorMapSunburst[bucket[innerIndex].key],
                    size:bucket[innerIndex].doc_count
                })
            }

    console.log('child array 1',childrenArray1);
    console.log('child array 2',childrenArray2);
    var mergedArray =[childrenArray1,childrenArray2];

    var index =0;
    for(index;index<this.selectedTagHolder.length;index++){
                l2ChildrenArray.push({
                    name: this.selectedTagHolder[index],
        //                  color:colorIndexTags[index],
                    children: mergedArray[index]
                })
            }
        return l2ChildrenArray;

        for(index in returnObject){

        }
    },
    initialTreeMapData:function (tagResponseHash, count, returnObject) {
        tagResponseHash.push({
            name: this.selectedTagHolder[count],
            children: individualResponse(count, returnObject)
        });
        return tagResponseHash
    },

    individualResponse :function (count, returnObject,tagValue) {
        var firstVal = count * 5;
        var secondVal = firstVal + 5;
        var finalArray = [];
        finalArray = returnObject.responses.slice(firstVal, secondVal);
        var childrenArray = [];
        var innerCount = 0;
        for(innerCount = 0;innerCount<finalArray.length;innerCount++){
            if(finalArray[innerCount].hits.total == 0){
                continue;
            }
            childrenArray.push({
                "name":this.responseNames[innerCount],
                "size":finalArray[innerCount].hits.total
            });
        }
        return childrenArray;
    },
//old sunburst data generations end here

 multiGraphDataPieVoice :function (finalArray,count) {
	var graphLabel = ["Facebook", "Instagram", "Youtube", "Twitter", "Media"];
	var downLoadLabelAmCharts = ["social media statistics facebook", "social media statistics instagram", "social media statistics youtube", "social media statistics twitter", "social media statistics media"];
	var colorCodes = ["#3b5998", "#3f729b", "#B0171F", "#55acee", "#f37520"];
	 
//    var chartCodesSms = ['ssmPieFb', 'ssmPieInsta','ssmPieYt','ssmPieTwitter','ssmPieMedia'];
    var chartCodesSms = ["socialMediaStatisticsTag1","socialMediaStatisticsTag2"];
     var chartdataContent = ['dataContentSmsTag1', 'dataContentSmsTag2'];
	var chartCodesSmSenti = ['#pieChartSentiVoiceTwitter-tag1', '#pieChartSentiVoiceTwitter-tag2'];

	var dataHolder = [];
	var data = [];
	var totalComments = 0;
	var dataIndex = 0;
	for (dataIndex; dataIndex < finalArray.length; dataIndex++) {
		if (dataIndex == 0) {
            console.log('excecuted',dataIndex)
			console.log('facebook response',finalArray[dataIndex])
			if (!isNull(finalArray[dataIndex].aggregations)) {
				var bucket = finalArray[dataIndex].aggregations.trends.buckets;
				for (var index in bucket) {
                        totalComments += bucket[index].comments.value;
//					for (var innerBucket in bucket[index]) {
//						totalComments += bucket[index].comments.value;
//					}
				}
			}
//			var totalFbHits = totalComments + finalArray[dataIndex].hits.total;
			var totalFbHits = totalComments;
            console.log('totalFbComments',totalComments)
			//ssmPieClass(dataIndex,count,commaSeparateNumber(totalFbHits));
			data.push({
				color: colorCodes[dataIndex],
				label: graphLabel[dataIndex],
				value: finalArray[dataIndex].hits.total + totalComments
			});

		} else {
			//ssmPieClass(dataIndex,count,finalArray)
			data.push({
				color: colorCodes[dataIndex],
				label:graphLabel[dataIndex],
				value: finalArray[dataIndex].hits.total
			});

		}
		
		
	}

    this.graphDataAllChart(data, chartCodesSms[count],chartdataContent[count],downLoadLabelAmCharts[count]);

},


graphDataAllChart:function (graphData,chartId,chartLoader,downloadLabelAmCharts) {
    $('.'+chartLoader).empty();
	if(graphData.length<1){
		$("."+chartLoader).html('No data available');
	}  
    var piechart = AmCharts.makeChart(chartId, {
        "type": "pie",
        "theme": "light",
        
        "autoMargins":true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        // "marginTop": 0,
        // "marginBottom": 0,
        // "marginLeft": 0,
        // "marginRight": 0,
       // "pullOutRadius": -60,
        "dataProvider": graphData,
        "valueField": "value",
        "startDuration":0,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "titleField": "label",
        "labelsEnabled":false,
        "colorField": "color",
        "innerRadius": "20%",
        "legend":{
         "autoMargins":false,
           "marginLeft":20,
            "enabled": true,
            "spacing":20,
            "markerType":"circle",
           
            "horizontalGap" :20,
        },
        "depth3D": 10,
        "angle": 15,
        "export": {
                "enabled": true,
                "fileName":downloadLabelAmCharts,
               
                "menu": [ {
                    "class": "export-main",
                    "menu": [ 
                        {
                          "label": "Download as image",
                          "menu": [ "PNG", "JPG", "SVG","PDF" ]
                        }, 
                       {
                          "label": "Download data",
                          "menu": [ "CSV", "XLSX","JSON" ]
                       },
                        {
                         "label": "Print",
                         "menu" : ["PRINT"]
                        }
                    ]
                  } ]
                }
    });
comparisonCharts.push(piechart);
},

 pieGraphSentimentsTwitterVoice :function (histogramResponses,returnObject) {
     console.log('return object sentiments',returnObject);
    var colorMapVoiceSentiPie = {
            positive:"#57c17b",
            negative:"#9e3533",
            neutral :"#96a6db",
            mixed: "#daa05d"
    };

    var colorCodes = ["#2dd3d3", "#04a182"];

    var chartCodesId = ['socialMediaSentimentsTag1', 'socialMediaSentimentsTag2'];
    var chartDataLoaders = ['dataContentSentimentsTag1', 'dataContentSentimentsTag2'];     

    var pieChartSentiParamsTag1 = [0,3,4]; //0 for facebook response-tag1
                                           //3 for twitter response -tag1
                                           //4 for media response   -tag1
    var pieChartSentiParamsTag2 = [5,8,9]; //5 for facebook response-tag2
                                           //8 for twitter response -tag2
                                           //9 for media response   -tag2   
    var graphDataTag1 =    this.pieChartSentiDataAggregator(pieChartSentiParamsTag1,returnObject);
    var graphDataTag2 =    this.pieChartSentiDataAggregator(pieChartSentiParamsTag2,returnObject);
     console.log('graphData1',graphDataTag1);
     console.log('graphData2',graphDataTag2);
    var mergeDataTags=[graphDataTag1,graphDataTag2];
    for(var index=0;index<this.selectedTagHolder.length;index++){
     this.pieGraphSentimentsGenerateVoice(mergeDataTags[index],chartCodesId[index],chartDataLoaders[index],this.selectedTagHolder[index]);
    }
         
     


console.log('individual responses for pie',histogramResponses);
    var count =0;
    var nuetralData = [];
    var positiveData = [];
    var negativeData =[];
    var mixedData=[];
//        for (count in histogramResponses)
//        {
//            var graphData=[];
//      for (var bucketIndex in histogramResponses[count].aggregations.twitterSentimentsPie.buckets) {
//        
//        var bucket = histogramResponses[count].aggregations.twitterSentimentsPie.buckets[bucketIndex];
//            if(bucket.key=="neutral"){
//            
//        graphData.push({
//            key: "neutral",
//            color:colorMapVoiceSentiPie['nuetral'],
//            label:bucket.key,
//            value: bucket.doc_count
//        });
//        }
//        else if(bucket.key=="positive"){
//            
//        graphData.push({
//             key: "positive",           
//            color:colorMapVoiceSentiPie['positive'],
//            label:bucket.key,
//            value: bucket.doc_count
//        });
//        }
//        else if(bucket.key=="negative"){
//            
//        graphData.push({
//             key: "negative",           
//            color:colorMapVoiceSentiPie['negative'],
//            label:bucket.key,
//            value: bucket.doc_count
//        });
//        }
//        else if(bucket.key=="mixed"){
//            
//        graphData.push({
//             key: "mixed",          
//            color:colorMapVoiceSentiPie['mixed'],
//            
//            label:bucket.key,
//            value: bucket.doc_count
//        });
//        }        
//    }
//            
//            console.log('graph data social media',graphData);
//  this.pieGraphSentimentsGenerateVoice(graphData,chartCodesId[count],chartDataLoaders[count],this.selectedTagHolder[count]);
//
//};
  // this.pieGraphSentimentsGenerateVoice(positiveData,"pieChartSentiPositive","dataContentSmsPositive");
  // this.pieGraphSentimentsGenerateVoice(negativeData,"pieChartSentiNegative","dataContentSmsNegative");
  // this.pieGraphSentimentsGenerateVoice(mixedData,"pieChartSentiMixed","dataContentSmsMixed");

},

pieChartSentiDataAggregator : function(param,returnObject){
    var sumPositive =0;
    var sumNegative =0;
    var sumNeutral =0;
    var sumMixed =0;
    for(var index=0;index<param.length;index++){
        var identifierTag1 = param[index];
        var bucket = returnObject.responses[identifierTag1].aggregations.sentimentsSunburst.buckets;
        for(var innerIndex in bucket){
            console.log('inner index',innerIndex)
            console.log('inside bucket',bucket[innerIndex].key);
            console.log('inside bucket',bucket[innerIndex].doc_count);
            if(bucket[innerIndex].key=='positive'){
                sumPositive=sumPositive+bucket[innerIndex].doc_count;
            }if(bucket[innerIndex].key=='negative'){
                sumNegative=sumNegative+bucket[innerIndex].doc_count;
            }if(bucket[innerIndex].key=='neutral'){
                sumNeutral=sumNeutral+bucket[innerIndex].doc_count;
            }if(bucket[innerIndex].key=='mixed'){
                sumMixed=sumMixed+bucket[innerIndex].doc_count;
            }
        }
    }
    
    var sentimentsArray = ['positive','negative','neutral','mixed'];
    var sentimentsAggregateValueArray = [sumPositive,sumNegative,sumNeutral,sumMixed];
    var graphData = [];
    var colorMapVoiceSentiPie = {
        positive:"#57c17b",
        negative:"#9e3533",
        neutral :"#96a6db",
        mixed: "#daa05d"
    };
    for(var index=0;index<sentimentsArray.length;index++){
        var colorCode = sentimentsArray[index];
        var value = sentimentsAggregateValueArray[index]
        graphData.push({
            color:colorMapVoiceSentiPie[colorCode],
            key:colorCode,
            label:colorCode,
            value:value
        })
    }
    
    console.log('graph data new',graphData);
    return graphData;
                           
    },
    

pieChartSentimentsGraphAggregateDataGen : function(returnObject){
 

},

 pieGraphSentimentsGenerateVoice:function(graphData,chartId,chartLoader,tagName) {
  // alert(chartId);
     $('.'+chartLoader).empty();
	 
     if(graphData.length < 1){
       $('.'+chartLoader).html('No data available');
     }
  
 var piechart = AmCharts.makeChart(chartId, {
        "type": "pie",
        "theme": "light",
        "autoMargins":true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        
        "dataProvider": graphData,
        "valueField": "value",
        "startDuration":0,
        "colorField": "color",
        //"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "titleField": "label",
         "labelsEnabled":false,
         "legend":{
           "autoMargins":false,
           "marginLeft":20,
            "enabled": true,
            "spacing":20,
            "markerType":"circle",
            "horizontalGap" :20,
            
         },
          "labelsEnabled":false,


       
        //"maxLabelWidth":60,
       
        "innerRadius": "20%",
        "depth3D": 10,
        "angle": 15,
        "export": {
                "enabled": true,
                "fileName":"social media sentiments "+tagName,
               
                "menu": [ {
                    "class": "export-main",
                    "menu": [ 
                        {
                          "label": "Download as image",
                          "menu": [ "PNG", "JPG", "SVG","PDF" ]
                        }, 
                       {
                          "label": "Download data",
                          "menu": [ "CSV", "XLSX","JSON" ]
                       },
                        {
                         "label": "Print",
                         "menu" : ["PRINT"]
                        }
                    ]
                  } ]
                }
    });




  comparisonCharts.push(piechart);



},


 CreateReport:function() {
  var pdf_images = 0;
  var pdf_layout = this.layoutTagCompare; // loaded from another JS file
  console.log("comparison charts.....",comparisonCharts);
  for (var i = 0; i <comparisonCharts.length; i++ ) {
     var chart =comparisonCharts[ i ];
    // Capture current state of the chart
    chart.export.capture( {}, function() {

      // Export to PNG
      this.toJPG( {
        // pretend to be lossless
      
        // Add image to the layout reference
      }, function( data ) {
        console.log("pie chart data",data);
        pdf_images++;
        pdf_layout.images[ "image_" + pdf_images ] = data;

        // Once all has been processed create the PDF
        if ( pdf_images == comparisonCharts.length ) {

          // Save as single PDF and offer as download
          this.toPDF( pdf_layout, function( data ) {
                this.download( data, "application/pdf", "Share of Voice report.pdf" );
                $('.button-download').removeClass('global-report-loader');
                $('.button-download').html('<i class="icon-download"></i>');
          } );
        }
      } );
    } );
  }
},


countryMaps :function(response) {
    var chartdiv = ['mapTag1', 'mapTag2'];
    var chartLoader = ['dataContentMapTag1', 'dataContentMapTag2'];
    var count =0;
    for (count in response)
    {
     
    var twitterMapData = [];
    var index = 0;
    var buckets = response[count].aggregations.locations.buckets;
    for (index in buckets) {
        twitterMapData.push({
            key: buckets[index].key,
            count: buckets[index].doc_count,

        });
    }
    var data = ConvertToCountryCode(twitterMapData);
    this.twitterCountryMap(data,chartdiv[count],chartLoader[count],this.selectedTagHolder[count]);
    }
},
 twitterCountryMap :function(data,chartdiv,chartLoader,tagName) {
	$('.'+chartLoader).empty();
    if(data.length < 1){
      $('.'+chartLoader).html('No data available');
    }
    var map = new AmCharts.AmMap();
    map.type = "map";
    map.theme = "light";
    var dataProvider = {
        "map": "worldIndiaLow",
        "areas": data,
        "getAreasFromMap": true
    };
  

    map.dataProvider = dataProvider;

    map.areasSettings = {
        "color": "#D3EBED",
        "autoZoom":false,
        "selectedColor": "#CC0000",
        "selectable": true,
        "balloonText": "[[title]]: [[value]]"
    };


    map.export = {
                "enabled": true,
                "fileName":"tweet maps",
                
                "menu": [ {
                    "class": "export-main",
                    "menu": [ 
                        {
                          "label": "Download as image",
                          "menu": [ "PNG", "JPG", "SVG","PDF" ]
                        }, 
                       {
                          "label": "Download data",
                          "menu": [ "CSV", "XLSX","JSON" ]
                       },
                        {
                         "label": "Print",
                         "menu" : ["PRINT"]
                        }
                    ]
                  } ]
                }
    comparisonCharts.push(map);
   
    map.write(chartdiv);


}






}


