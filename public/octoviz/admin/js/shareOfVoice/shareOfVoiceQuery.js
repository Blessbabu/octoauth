var fbQueryVoice = function (liveParametersTagCompare) {
var size = liveParametersTagCompare.size || 10;
	var granularity = liveParametersTagCompare.granularity || "hour";
	var queryBody = {
		"size": 0,
		"aggs": {
               "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": "hour"
      }
		,
        "aggs" : {
        	"sentimentsFb": {
               "terms" : {
                  "field" : "sentiments.overallSentiment"
                    }
					},
			"comments": {
               "sum" : {
                  "field" : "CommentsSummary.commentsCount"
                    }
					}
		}
    }

		}
	};
	return queryBody;
};



var mediaQueryVoice = function (liveParametersTagCompare) {
var size = liveParametersTagCompare.size || 10;
	var granularity = liveParametersTagCompare.granularity || "hour";
	var queryBody = {
		"size": 0,
		"aggs": {
							"source_data": {
								"terms": {
									"field": "SourceName"
								}
							}
				}
	};
	return queryBody;
};



var twitterQueryVoice = function (liveParametersTagCompare) {
var size = liveParametersTagCompare.size || 10;
	var granularity = liveParametersTagCompare.granularity || "hour";
	var queryBody = {
		"size": 0,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        },
					{
						"range": {
							"created_at": {
								"gt": "now-1w"
							}
						}
        }
      ]
			}
		},
		"aggs": {
             "trends" : {
                "date_histogram": {
					"field": "created_at",
					"interval": "day"
				}        
            ,
                "aggs" : {
                    			"sentimentsTwitter": {
                        "terms" : {
                            "field" : "senti.overallSentiment"
                    }

                   

                }}
			},
			 "locations": {
             "terms": {
               "field": "place.country_code"
                }
             },
			"twitterSentimentsPie":{
			   "terms":{
			     "field": "senti.overallSentiment"
			   }
			},
			"filterRetweets": {
				"filter": {
					"terms": {
						"tweetType": [ "retweet" , "reply"]
					}
				},
				"aggs": {
					"trendingUser": {
						"terms": {
							"field": "user.screen_name",
							"order": {
								"followers": "desc"
							},
							"size": 31
						},
						"aggs": {
							"followers": {
								"max": {
									"field": "user.followers"
								}
							},
							"details": {
								"top_hits": {
									"size": 1,
									"sort" : { "created_at" : "desc" }
								}
							}
						}
					}
				}
			},
			"tags": {
				"terms": {
					"field": "hashtag.text",
					"size": 500
				},
				"aggs": {
					"sentiments": {
						"sum": {
							"field": "senti.overallScore"
						}
					}
				}
			}
		}
	};
	return queryBody;
};
