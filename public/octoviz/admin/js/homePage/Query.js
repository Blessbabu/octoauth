var fbQueryHome = function (liveParametersHome) {
var size = liveParametersHome.size || 10;
	var granularity = liveParametersHome.granularity || "hour";
	var queryBody = {
		"size": 0,
		"aggs": {
               "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": "hour"
      }
		,
        "aggs" : {
        	"sentimentsFb": {
               "terms" : {
                  "field" : "sentiments.overallSentiment"
                    }
					},
			"comments": {
               "sum" : {
                  "field" : "CommentsSummary.commentsCount"
                    }
					}
		}
    }

		}
	};
	return queryBody;
};



var mediaQueryHome = function (liveParametersHome) {
var size = liveParametersHome.size || 10;
	var granularity = liveParametersHome.granularity || "hour";
	var queryBody = {
		"size": 0,
		"aggs": {
							"source_data": {
								"terms": {
									"field": "SourceName"
								}
							}
				}
	};
	return queryBody;
};


var twitterQueryHome = function (liveParametersHome) {
var size = liveParametersHome.size || 10;
	var granularity = liveParametersHome.granularity || "hour";
	var queryBody = {
		"size": 0,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        },
					{
						"range": {
							"created_at": {
								"gt": "now-1w"
							}
						}
        }
      ]
			}
		},
		"aggs": {
             "trends" : {
                "date_histogram": {
					"field": "created_at",
					"interval": "hour"
				}        
            ,
                "aggs" : {
                    			"sentimentsTwitter": {
                        "terms" : {
                            "field" : "senti.overallSentiment"
                    }

                   

                }}
			},
			"twitterSentimentsPie":{
			   "terms":{
			     "field": "senti.overallSentiment"
			   }
			},
			"filterRetweets": {
				"filter": {
					"terms": {
						"tweetType": [ "retweet" , "reply"]
					}
				},
				"aggs": {
					"trendingUser": {
						"terms": {
							"field": "user.screen_name",
							"order": {
								"followers": "desc"
							},
							"size": 31
						},
						"aggs": {
							"followers": {
								"max": {
									"field": "user.followers"
								}
							},
							"details": {
								"top_hits": {
									"size": 1,
									"sort" : { "created_at" : "desc" }
								}
							}
						}
					}
				}
			},
			tags: {
				terms: {
					field: "hashtag.text",
					size: 500
				},
				"aggs": {
					"sentiments": {
						"sum": {
							"field": "senti.overallScore"
						}
					}
				}
			}
		}
	};
	return queryBody;
};



