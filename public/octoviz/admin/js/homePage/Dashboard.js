function Dashboard(){
this.esClientFbHome =null;
this.esClientInstagramHome =null;
this.esClientYtHome =null;
this.esTwitterClientHome =null;
this.esClientMediaHome =null;
this.esInstancesHome =null;
this.keyHashTags=[];
this.namesArrayCompare = [];
this.mapForClients = {};
this.liveParametersHome ={
    granularity: null,
    size:null,
    bubbleUserId:null
};



    this.layoutHome = {
        pageMargins: [40, 100, 40, 40],

        /*
         ** Header; Shown on every page
         */
        header: function(currentPage, totalPage) {
            return {
                image: "logo",
                fit: [70, 70],
                margin: 40,
                alignment:"right"
            }
        },

        setBackupPoint: function(){

            this.backupPoint = JSON.parse(JSON.stringify(this.content));

            // this.backupPoint = new this.content();
        },

        rewind: function(){
            this.content = this.backupPoint;
        },

        content: [

            /*
             ** PAGE 1
             */
            {
                text: "Home Page",
                style: ["header", "safetyDistance"]
            }, {
                columnGap: 40,
                columns: [{
                    stack: [{
                        text: "Social Media Statistics",
                        style: "subheader"
                    }, {
                        image: "image_1",
                        fit: [(595.28 / 2) - 60, (595.28 / 2) - 60],// 1 column width incl. margins
                    }]
                },
                {
                    stack: [{
                            text: "Social Media Sentiments",
                            style: "subheader"
                        },


                        {
                            image: "image_4",
                            fit: [(595.28 / 2) - 60, (595.28 / 2) - 60],
                           
                           

                        }

                    ]
                }
                ],
                style: "safetyDistance"
            },

            {
                columnGap: 40,
                columns: [{
                        stack: [{
                            text: "Tweet Trends",
                            style: "subheader"
                        }, {
                            image: "image_3",
                            fit: [(500) - 60, (400) - 60]
                           // 1 column width incl. margins
                        }]
                    }
                ],
                style: "safetyDistance"

            },

            {
                columnGap: 40,
                columns: [
                {
                    stack: [{
                        text: "News Media Statistics",
                        style: "subheader"
                    }, {
                        image: "image_2",
                        fit: [(500) - 60, (400) - 60] 
                                    // 1 column width incl. margins
                    }]
                }

                 
                ],
                style: "safetyDistance"
            }



        ],

        /*
         ** Footer; Shown on every page
         */
        footer: function(currentPage, totalPage) {
            return {
                text: [currentPage, "/", totalPage].join(""),
                alignment: "center"
            }
        },

        /*
         ** Predefined styles which can be applied through "style" on every content element
         */
        styles: {
            header: {
                fontSize: 18,
                bold: true
            },
            subheader: {
                bold: true
            },
            tableExample: {
                margin: [0, 5, 0, 15]
            },
            description: {
                fontSize: 10,
                color: "#CCCCCC",
                margin: [0, 5, 0, 10]
            },
            safetyDistance: {
                color: "#3A907F",
                margin: [0, 0, 20, 20]
            },

        },

        /*
         ** Predefined images
         */
        images: {
            logo: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoEAAADICAYAAACXmzs1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCNTlENTg1OUQ4NzExRTVBRkNFQjFEOUM2NzJCQ0I0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCNTlENTg2OUQ4NzExRTVBRkNFQjFEOUM2NzJCQ0I0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUI1OUQ1ODM5RDg3MTFFNUFGQ0VCMUQ5QzY3MkJDQjQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUI1OUQ1ODQ5RDg3MTFFNUFGQ0VCMUQ5QzY3MkJDQjQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6aKkUXAABIxUlEQVR42uydB5hcVfnGv5nZvtn0kAQICb1L6KCUCKJIE6RIRxDhDygiKAgKgtgAEVFUQIQoUqWDSgeVIr2GZpAWCIH0ZJOtM//z7vmGTDZT7jlzZ+bOzPt7nvNkszt3Zu655577nu98JZZKpYQQQgghhNQXcXYBIYQQQghFICGEEEIIoQgkhBBCCCEUgYQQQgghhCKQEEIIIYRQBBJCCCGEEIpAQgghhBBCEUgIIYQQQigCCSGEEEIIRSAhhBBCCKEIJIQQQgghFIGEEEIIIYQikBBCCCGEUAQSQgghhBCKQEIIIYQQQhFICCGEEEIoAgkhhBBCCEUgIYQQQghFICGEEEIIoQgkhBBCCCEUgYQQQgghhCKQEEIIIYRQBBJCCCGEkOqhoVZPrKmlpTq+aDIp8WFDZKWLTjY/p3zfJWFam2mtpg01beWMNt60lUwbZdpw0zr0dc2mNeqxA9/EtF7TekzrNG2xaQtMm2PaLNNmmva+aR/ozwtNW6KvTfJW0lVVW4ss+ffzMufX10q8uYUdQkgpps3uLmnfZVsZceKBklywmB1CIsV7h5xBEVgDxKAlxVpLU1n+llTBlKrAd2tTgbeKaeuYtq62NfR3w0r8+QtUEP7XtNdNe820N0z7UAXiEg4fQgghhCKwWhlh2gWmtecQgRA6P1MhVBzxmMSHtJoVbad551i+V25j2t4q9jYybe0KXcNh2jbI+F23aW+aNs20F7Th57c4lAghhBCKwGoC4u/wPH0EYXhF0SLQiL7kwk5ZdMP90nHg5yU5f1E+IYit20PFWvuiRrOKQrT9VST/V4Xgv0x7wLTpHFaEEEIIRWCl2dG0zcVasAbkmNjt33tUuPSL9Ycbm+P4eWJ96IoDgq8vKQuuv3vg544DPifJxUtzvfpR03Yw3/RiSab2kFQqyv2LLetNtEEUYvv4OdPuMu3vYreOCSGEEEIRWHYOMe0oWX6rF0KwS0VgSv9fehJxiccarRA0P7fvvKUkO7tyvfp/5sWHxttbfxJrajwu1dNbDRHeCECZpG0PsYEmEIJ/Me0ZoQ8hIYQQQhFYRvpkWWRsJpWJdI0bIdjYKItuuNe0+3K/LpWS/r7eBa2T1/3GqFO/+k6sveXcVGdXcxX1OwThqqYdY9rRpj1s2pWmPSg2qIQQQggh5ZAedXzuKcfflx5sDRsxiECRPK09kWg4YOnzr+8956d/vDg5b/HhsfbWhVU8/nYSaxG8zbQTxQa9EEIIIYQisKIiMHpOd7HY8dLYcEOiqfHWrpen3zH3vKmv9b0z80AjBOdF3EewEFuZdrFpN5t2utgUOIQQQggpEfW8Hdye4/ctGQJ5aJ7jh2T0H0QLUsq4bCVjKxr59i41bbbDcXsNiD1sHzc1faF7+rufmnfRtUcNP/ErBzWuvvJ1qc6uEQXSzESdydoOFLtN/EexiasJIYQQQhEYCvepaBscHfy8/h/Rv8dmiMJMYnrcdBWTp4ityOHKfNNuchSB1o9xIGwlJvHm5vE9Mz68af5vbth7xLcOOrhh0vhbUouXtla5EASfMu0Xph1k2jmm3S1RtMwSQgghFIFVx3WmXa/CIpu4QMTq1QHeB6XYujy/AyqO9DseM3fQd3zECMGNe9778OZ5l92y88hTDjk2MWr4VanOroTEamJ8bm3arWL9Bs+TMJJzE0IIIaTufQKT+i8sgKipO6RA69DXDsY3otjnuFczfn7ctH1N2ybe1PR8z3/fuXXhn/92jyRTP4u1NNXStUL089dMu9+0IyX3Vj4hhBBCAsKKIVYIf9W0LwYQZbCtYRv59xX8vo8PEpEQSLMlFjsu3tA4deljL/wl1ty077Aj99pG4vHPSTJZS9dqNbF+grhW2CKexuFLCCGEUAQWA7Yc9w742kUVFoFPifUlxDY06gePFFtG7rvSkLjc/Hvu4oeeOrJ1642Obd58/f+klnSNqcHrhQokm6gQvJbDlxBCCHEnzi4YoNPhtYsq/F1Rbu1u/Xm8aZuZ9qZpu0kqtX+sIX5dPBb7waLb/xVPLV5yxkDewdpkHdP+YNpvJH8UNyGEEEIoAkOh0uEWqFd8U8b/Uf4OwSJ/Nu3zkpLWWFPT9J5X37x4znl/uik2pPW+iOcPTKkI/0CslRPl5G4Qa+FDzkBsv6Pm8Ex9XWa9ZtQn/oa+bj0OTUIIISQ43A6uTv5j2kumbSzWPw5WMUTOIkhkF4jEWCJxbP+sudv2TnvzzKZ1J22fXLy0ZaDiSHTAljaCXP4htnTca6bNkex+mUiLgxQ8k8RuA2P7flOx5edWMu1zpt1r2tdNu4fDgxBCCCkMLYHVyfum3Zgh5H9g2iumnWHamoII5oaGl/vmLfjRgqv//kyspflKSURGACKtzV9NO9S07Uw717R/m/ax5A7MQRqdj0x7UuwW8NF67H6mnSk2ahjR29gmR87GGIcIIYQQkh9aAqsXbIEeY9oEsUEtu5l2iWkbmNZq2nXxRMMvez+c/aXO+5+4qO2zW+ybnL94bIXl0bOm/cS0W3L8faxYqyYsfAh8aVEBiG1gbHm/JzZBN6yIS1U8oiGp9Ke1H44XG0V8jiyfU5EQQgghFIE1AbZSkez6u2Ituhea9oJpJ6mY6peGxM/7Fyz+7tJHX9imdbtNr4k1xE9O9VcsZQz8GL9j2jtZ/jbFtMNM21y/OyKeByc6ROk4VHH5SM/9UbH+g++KTdb9oDaU4dtDrK/kDfr68hGPSayRtxUhhJDow+3g6gZ1dd/SnxEYcYEKIgitGaa9lGho2HLpi2/stfTxF38ba2/7oELfE9VZjswiABHdjLQ2sAweJdbfb5xkT8iN7d4JKhSxlXyxaY+IrSQCP8hGfR22xc837U6xVsSy2T5jDQnpfesDmX/lbRJvauboJIQQQhFYY/2QCLEf40WKlNdNuyzj/wepAErXO35b4om49PQe0j3tzf+lurrukESi3H17l9ht68WDfr+5/g3BHCM83rdJRSEsfrAIIjDkANOGfXLuNtCkfKHRsZikunulb95CqYHazYQQQih+6gKX/bvBKgpPe98yZu0hXIPfmfZ0xv+xPYwtUfgIfmpgezIe337JPY9t3DN9xkWxxoZ5ZexX1Pk9PosA3EJsYMtmIV6/KWK3f2FV/JLYSirlx4i/GONSCCGE1Jj4qVXgJHe9Cpb+Qo94sTnrMkGAws/FWrNcHO4gJhcISr4VB5JXnyY21Up6G/UIbTKQI7ChYbz09e2YnL/oklgidqf5zeFl6Ffk8/u22GCOTLDdi23sNUr0uTtpQ97EX5v2DIc4IYQQQhGYi4e1+QAfvPMq/P0REHGRisEVlWs8Lv3J5HbxjvYrpLEBkbRfkdJbyuCr97csv/+pwEJZeiB0dxRbUQRl/pZwmBNCCCHL4HZw7QBr5N1Z/5JKSbyxcbP5V9w6vv+j+S/FmhrvLPF3maXiazC7ivXbKxcTxaaPub1MwpMQQgipGmgJtCCydLLY9CRBt3SxNbxQbALj7gicA3LnwR9wddPWXeHLJhKr98yYNa5/4aK34iM6LpXu3n1L6LyGZNDPZemvr4m//2Qu0PfYkkcQCLbWYZlFYMxobYgqRkWR+7V/rha3bXtCCCGEIrCGgTD5qYoFF6bpMR8Oei+ID1hZSxGZOjOP6HzZtJNV6Ixc/k+phrjEx80581IZ98eznjaS7AGP8w0CfBTvyPJ7pH/ZKqTPwNYuUuM8ZNo/TXtRbO3hzow+h/hbRT93B7F+gkhHs41pZ4mtUEIIIYRQBNY5EA69Hsf1ZhF6O4v1EWyT8C1OsKbtb9pTeV6DdClIyQI/uDGZZxhriLcgRUyqr2+BxGNXSzK1k4TvEvCE2NrGg0G939WKfG9Y/FAbGImnkV5mQZ7XIiL5dW2IRB5q2pfF5iOEGDxbbHJtQgghhCKQhALy1K1Vwr5tDfAalJRDKphfirWEWeLxDyRhNF8SinBALD6l4ixMMQ3L3KJBv4fQ3LTI98Z2LtLh3O4prrF1P9W0a8RWJzlWxeHDHLKEEEIoAkkYwDrYXcK+7Q/4OkQM7ys2GTMsfg9I2oKItDESg//cTSGLwDkqArMJ49U93xOC8mem/UGKT6eTvj5Xiq02sq1pW0p+y2oUiWt/rmnayqaNEuuGkFCBvFRFL0rmIUXPWyH1XTkYr+e0ko6b1iznhcCj98W6RpQCWND/r0bnx5QuqB4p4WesOWhstmlfpq8h7umPdWy+WeKx2aDzH1JS9YX0nngfuKTM17H4gZTevQT3/J6mfUb7sBBIF5beBQkjMwL676iAz5+4zrMwRkwr8Fr448NdB37sPVV8X6G/Z+i9VVWZKCgCaxtMsGfoRDxfVtzyhlUNFrG1Qvo83ATZ8vKN0Ie7Ky+JzTX4gMMxEAyr6qSCfzv0vD/USXG6ivQ3xFYVWVO/W/GCAlbW0tVmhhhCypsvivVrXFUfri06kWYmMU/qg6on46GL8/2X2G30aRKd4BiMjSli/VMn63m16qTaKMuq6qQyzqtbJ1oIwRd0wfOQLkLCAAFiZ9Xw/NgfsgiE/+2nxUb/Y2G1WsCx2aWi/g39PnfpPR9sbGIxmyzodo1UWMfrfROWj3ZK+7BXz6NTBS3uq0d1Ify+hOsTjrG4t2lfdTgGffr3kEQJnhHfd+yjNwOIwBZ9Bu0t1R2wF1NjwrMUgSRqJDNXqanu5RZbSJB9T4gi8OUcNwAeCMMd3+sunbzfC/DaRj2H3bVBTKSrscQzBESPCkGIX0Qwv6qtSV/nPwnFYtL79kyZff6fJdYcagpGiNQDTTvYtLVlWY3kQivxJm14QI/RlfwXdCLHNUci7ccruPrGuRyu57Wa51w0UcXH1/Whi4CoK2TFGtU+90wtR5CHZRHDmDpAr+F6nmNztL7Prjo2Uf7xYhVTOcdmvLlFOh98UprWmSjtX/y0JBcszidGEpK9HnmYwDIPixb8seGrfJ9pf9LzWBiS8HQVlf0hjuOkx+v7A55XIuDYiTqNUs4ypSHBPIH1hFk5N22wxuC6tpio5oY0STyb429NEsyXMQ0STR8aUACiBjESZT9m2gVqVRqeYYGI6ThvUDEK38SzdZV8ngqQnmIny1hDQuacf7WklnSFdbVW0Yfiv037sWkbhDBRpkscflkftr8ybVKZR2GHintY7n6gAqDYxWiDCsIfqBXmWBUY5XzoVdOisNgHFSzn39OxCVeNjUN6iGOO+FKGEFyzkHzoffM9Sc5fBH/nQmKoXGDOQWYGJOSHFQ4uN3tJ9przPkKwUmM4VcJxViv3Wj9FIIm2BkwmZcRx+9lty2U8pZN5sWCL7oUCAiQIKCl3nOSP/E2LpHPEJsg+QdwtjdiGRN5AWAX3DKN/Y40NgwW2L7Bm3qzib3yJhkOT9vPtKgrLAQQ4yvn9Vq9fKYAYRO3sK1U4k3D5vAobiL+VS/QZEPXwyUSqqf1yPryam2XhPY9J71vvS6y1Kar9tYvYeuawvK/K4UMoAkllzQCdWX2KkSOxs8i3xjbwG3lWkUFWSLBKfktsepd87GbarWL9tkYX+b2xdYxtxBMjcolOERvBvHWZPu9TKszODslakYsDVNjuXabz2l8fvrvzrg8N+OfeIHYLvhxsoGPzJ5KtzGUqJYmGRll068OSnLNgoDxmRMF9dbyO/604jAhFIKkMqZw6DFVPLi7y3eGH9UGeSbDQlh8e2N8sIEbxHgh0wXbxliH2DKJQsZV8eoWvEEr/naffp5xgi/iHYi10LSV4/5PE+uqtXubzWlcF/td48xf9nDhHx+bwMn92q97zl2f77FhDg3Q996okl3aHZYUvJRCAfy2jiCaEIpBkTJit5vkezzlRwq/uuSLeHo7QuXw7GguIi8d0pbwoz2uQ7BkJsM8Vu5UbNthPOtu0b1Ti0oi1ap4qpbXGFeJYHQdhfgdYj84X6wtYCTBWkF/yUM4A3sBt4gdSWef9w3Wh1jp4YRtvapa55/1JUr291dCX8EH+k2Qp7UkIRWD19UXV9F+yu1tGnnSwNIwZmSuNCXJ1wdl7qc/bi93qyEVHHhH4tlj/n1l5jof/GLaFji5xn0MI/khC8hF04GAVgFEwZeBafD+k94Kv4TlS+cg/XFdYOXfkNOd1Dc+NyFyH+//krBPQwk6zyG2uFrd8ZDJA3tPhHF6EIjA6/bBMpGBbIRVoNmmrsT6ENQ/+N65TKRJkvpjn7yvl+D18/7AF/FKeYyeJ3f79Upn6AJaj86X4EndB2UCFUnuExgEsPzsV+R6I+v2xVM4COBhYkn8jNgcgCcZEvReilL7j+9nGZqqvX7qenCbSUDXT8fZiLZsJDjNSSZgn0IKghksGxEwq1ZPq70d9XasOm5py+ZpgYkRusgU11A8Qf0gbspHY3HRBwTZtd56/r5rns+7KcxyEGIIkyu1Dg5xn8ENCsEgp8+jB2f07UigVRvlBepUzxboHzPM4PqFCcv2InRdSmcD38XhOeYH4YQTHJraDfyE2J9/i9KI9tbRbFky9S8Zu+6kw0zSVGpSvTNdCJ4QisIJ0f3IjpmRAAI7+zuHSM2OWLLj+HkmEm/w36iAwAxG6SE0SZPvsH2LzeuUTOmtk+f29amXIBdJPTJXKOVEfpmPi/sAKutvZJwnVFQ6O6DjYThcCv/c4FqWtjozoeR2kC4tHC7wuVsP3eJBz2zHCYxOphhDssyyYzQjB+JC2oDs4UQFzI3ZCkNvyYyH1cF9FDm4Hf/IETw20VCopo045TFqnbP6ZYUfs0T7soF0l2d0zsN1QncMyZvMCogWPnvtIRdDDBV73rtitzHxlcmBV2njQ71DCDb49uQJBEB0LH67PVrDnsNUPa+DQQMOnv9+MG/PMTATe3YGf2gmSLfWFx+iVZVUuwkgGnF4gHi7ueQoxyE6J8HnBD+uYAK9LlqD5fP9KfY+TI3wNRUXg6BU/KZS8w6kSXctcC6Yv1vvTt0Tj3Lf5RhiFOS7KRs1aAgeVRys4BlNGIA07+IvSvttnIAZj/XMXniTx2B+G7r/LvdheWPLYC5Kcs3AgJUG16P1Ud7c0rT1JRp9zjCQXL5FYY6NLv6BaBxK1YusF2e9bswhFbGU+UeB94IP1qUG/Q/DFKzleDxWFRLR7R6ALIUL3Ne2qINNYw/hRy6rcFmZDKc7PEel4UDP3GRXjqJuLiws3BVQsQDANciCiJu8kz8/YRh9SLttVm4ktAebLO7r4eFbH4GydlJv1vCboZ0wR67PmA8rnIV3Hkzn+/pZe+zDvdPiXnO+4sEE9ayT0fj/kufq9gNfdF9QQR53qpzKuYXfG2JygcwKsjet4fgZcNvYRG2AxkKi9550ZMv+SG2XEyYdIclFR5VthJUY09FLJ7rPXpIsJ7Fag/CHSVW0ifv6veP9DxCbGnl+HAhAXCq4nSKYdBUtLjy5+ffLG/kcNHBSBUaB5skMEfm/vQDm1jv12HogyU8GznfSn3k0u6rx36AG7yLBDd5O5F11rxODzEm9pif62A6yamK3WnzQwQcZamn2+M4QFtvXu038n6O9Rc/hCKWwpBFurVS0NSin9sYAF4rgI9eSpKoIWFexyN2vx4eLncA+Hpz+rOP9vgNevpBOaT1UV0QftXfq5QThC/Oq04mGAKh8XqKgtBMbiaaYdJW4lCdMLE1S+eFqypzVaGmBx44Prll/6e5T7wYKI4FGeQhdb7QgImh7g9bDkneQ5NnHvwII29RPLDea8nt4w8gXiOv1b3MqZ7arjcYrH50EMww/7kToUgejj17VFgYQKUldwj14rtARGh9FnHe3w6tiAH+BAEXI7gSCEf5xaHOLm4Z7EA37kSQcN/H3Jo89FXwiarzZ0n51k6GG7G2G7uNiJ8Vptq2dYSoKSae2CReAsyR1sgdeeHbGehMUBg+miEN8TkcB7eByHFQqiI10Se3+kVg2U9LtEckdq53u4TQgoOIeouPI5L2whX+ZwDCxMyOmIyHIEGLkmuf6SWpFmlfkB40JcSpO8Ox9wxdje4zgIsTN0cRKU2To2n9CF4RjHz4TFEhbFZ5ZN5aEYbxtUZHY7HHO3WoLgxuLqSwkrN8rLPSa1W7O6WoDPsE9Vl78tNw6riJr1CcR2QPDWORBdljGBbK7/riIZKSVSPX0y8sQDpW37TSXZ1RXdDPXmeyV7e6XjyzsNbAOH+D3fchSAENM7Z/z/V3luFEzmSOrbFsEePcbjAZUPBF241hFNav/4Vnb5qz6k+xyPG+kwKU7xOC98nwsdBWAml+nxriC4oBpqC5d7pQnLvU9U90WOAjCTO8VaBF3PFf6q20boWmE7F4Eej3oc+3mJVpqoeqRN53pX49g8yb+7RRFYhWyZYdmY8MlsjNQxpo044SvSPmULSfb0RPLLwxdwxLH72jm1stbKr2kfioq/XDcKJj/kcFs5ouMBvgWHhfh+O4q70/3LYpP2FsNU027zOA552YJYsbbzEPEYFz8v8rxgfXnW8RicD/zeajkK2Hfucy1bCP/enxT5udfrQsWVLZa7l2JS6cX5XLHl9VyT7m+qRgdSOfYQPyvgQ2KtwBSBNQJmkE30Z2zFLOcbk+rtk1hbszSMGx1WJFr4J9DUIE3rTHSJVC0FyPF3SMb/YbHJ5dsEX5odIj4mDpCAkcIBJ3yXJxWcDVF7d1GRn4v3gT/hYsfjMDE2BeijyY7nhRXKpeJXpSYTBFBc43HctlL+7dYog2u8kcdxcDNYWORnpy3drpMqMg+slB6Cqd5+G/xWWSH4gNjAGBeaZcUsCqR8wPp3kMfivEvnsFS1njhF4IpgCzNt/YNfyJAVH6XJfEEAsUpaF/q7u2XYUftI48RxA4K1guPq9Ix+hJP1dTleC/+jY6pgXGC7On8qBzimdxV0I8IDa5LjZ3+Qp/98Vq2uVjPkeSxkpZ2Qcb2DAr++20I6LwQvve14zCZZ7+/6ZazOf65j8+8hff4LKqBcQHTugKtGvLlZlj75kiy68X6Jd1TUq2SJ5K+lnovJHIIVY4r4BfVg6//+aj5xisAVQTDIqIz+cTWnQTiWcgbK6a+QSialacJYaVhlzCcVTyoEVlRpK2CvWgqyWZ/QT9+V6ijlhQjU3SSPRQwpeFo2X7+QhRiCynW77WGxTvRhsFgnLpeVK65ToXD7tcQ96AQJw8NKizFNbEoSF+BTtpqQNGM8Fii4hmEF12As3ON4TIfH4qMcIP2QazT4+hyCFQHPbKRB88mecJFUsRUwr6CoY7bJEHE+Fxc+TrCCtUj4kV6wMGYPzNBgkNYtN5aWzdaT/o/mVmpLBNu658mynFnYFrklx2v3FL9o0kqxi4qhFWsdp1ISa2uR4UfsLh+eeL7EmnPuMuIh67qt/K+Qz+NptVa4OKLjAZXP4oOAkBEVPC/ca8+btr/jInhNqdKovhKJwFEeYynMOm0v6Pu5bNNPimBfwk/yI8cF7kR9JvdxKJYV7PLs63Ecdrjur/aTpwhcHmwNbZrxf+z5dmcTXNhqTeW2SEwr9xdPdnVL86RVpO2zm0ty3sJKCUCk3YDVb5WMB/Mvckxq6Rxh1VSTb7yK3Jey/tUIweTipVLAG2CcuFuKXwn5PF4TW/PaRQSuUeDveNi5WM2T+j3C5A2x6WbCPK96Yry4+6qGnd8N28twE1jb4ZhVdexFqawTcqzCB9rFz2+sLhDnciiWDVgBD/NYwILfiFsaoUjC7eDl2XDQ5IOtzBWc8ZFWpmOfKdK68doDJeUqDWrWNk4YK6POOEoSK42ohC/gBBV7f5blU4TA0nNfjmMQaLFNFY4R5M3rKOL4UY4PWjjcvx/yObwv7kEmY4v8e7aH5MyQz+tdFbcurCwkjet2PlwUwq55mxZPLowTv8Trpcb1vu3wFCPEH+S+9alzjmjgf9ZCB9ASuDybDXoodGd9qCSTEh/eIQ2rriSxV/9nN40rFAqCSLiGVcfK6LO+LvG2Fkl15Y2Ma9GV5mraxujEk1DB26miY6GKBPyLbUMoXSjLlI6ZVp2scAMhzcbeOR6ml+RYneNhc3LIXQET3GL9zr16XkOLFGzZ2EFFr691bqTHQ3FhyOeA93P1xRsR8nlhq6wr5PP6WPLXsQ5D+NQyrr6qs3XOCJMFHmNzZESfZa4WvUaPa0CK40jxy/pwtc5hFIE1RIOsWC9zSa4bGdVFhh/zZel+7g3pmzVHYk1lXojCB7CrSxpXUQHY2pIvYhlib4ppe4nN5TYxoGztV3HVoy0lywJfCm1pYkv84Rx/+6pYX6ywVttYkf1DbGDATBWBEC2I/txT26ohfR4mjO2LEIGuonShhO8jlBJ3i9kQyV0ZOSHuW9xzSnBeC8V9e4YP3WUCxPVhuFiKT+8zmC4PIT9U3AP4yoFrKibcX0wYXT6we+GT//UVfd7UjPAhlnVkxRDxpZInKhNVRlp33FQW3/7P8sYHpQXgpJVl9BlHSXxo28CWcA6QABO+dzt7fFJC/FNooN7uvCy/h+XlGyH0Ah4USJuC0l/Z6rzi2n2gN+tVYsutfSmkK4BUMVeInw+Sa53bTinN6Frs8b0bJF2ndUUB4VoveKGE78PVneP75aNNiK+QT+8ShM0Sj2sYRdcmnEfS4bvFPeYH4s+x4p6gG3PxzeJWOSvS0CdwGZ/NMiBgrcjpO4Vi5UP3M9oqWcbKHDGxAnCiEYCnHyWJEUPtFnB2TlWhtHOZ+xJ9do9kj45GWaViUzogghD1OY/OIQAH85Su+C4O6fxgTfUtI+dqMu6S0tQT7fX43ok8i0nX81pagvPq9RAlTcKqITqzOBsF0N+lcED2uYZRfJb1eozxRg7FsrCSuNd4Bm+rUaFmoAi0YOvw8Cy//yDvTYwo4a5eGX78/pLqL0/1kFR3nzTCB/D0IyU+cqgkO5fm8gFEFY6fSmWS4T6oQm0w8CE8pMj3RnLhfUy73UOYQhRfGsL5Ibhjs+wXKCV5DHc+icRLtbrwGbCxPL+Pe3x+qgTn5HpecYpA72sYlbHJ5xhxBUaBtT2OQ53rt2qpI3jzWBDxma1m4HuFp86YNK01wYjA0kfkwvIYH9Iqo846RuLDhkgqtwBE5O0PpDJ+Mngw3CHZt3SOUSHoy+/F+hP63oSwMGBr/O4QznP7FU68zwj0NVaR4V/bR5I93byrCKlvfBYYSXZbycEuzlc89A92Bi+vtc6gT6CNmP12DsHwZmHJYxfDDWNGSt/HcyWWaJBYQ0JS3d1Zl8kDpqBm99R4SfN+jSuPkcTYUQNCEIIwhwBE4MG3pHLlsJBD78Esv4fP5ZeLEJa/Mu17UrwPEtTZiWLT14wr4n22zPYtY40NEu9oL597ACEkys8WF6GRkhrIO1cFIFBwc4/jYAWcVmudQRFohcmWOUTg/wretcmkJEYPk1GnHykLb3lAel99R3rnzJO2LTaUWEvTcmIgFo9LsrNLljz7ygrLw3guYZgOApkwTkb/4GhJjBk+kKewwADfrEJ9iZNFGam3s/ztABWCPiApJ8rLhRVI8F/TzhQbVOILKoeM0tXh8ouCJBfzhJBPIupd5s8l7LaSX5ODxN0K2KPPoZqj3kUgwvFPznPR3wl053b1SMPYkTLmzK9L58PPSM9rb8nQ/XeRWHvrcoIglohL/4JOabz1IZFEPK0MJbW0Sxbd+U/VfHGJNat4TAvAieONyEQQSIckFy7JN61g+3dHcSu5FCbImzQ1y+99E3IC5GM6TcKPJEVia5QY8y1bhzqTKKX2COfVSJEq8esJCYpr+iE8LBax20oKAkC39zgOLk7PUgTWHkdIbqsZ8gN+EOhdECDS1y99M2dLy+R1pXWrjSS5qFNS87Pcz/GYDD1st+WPRcWPNVaReHuLdN79uCx59tWBZcpAUr5Vxw0IwIbRw2xJsvzl4JD3aHIF+xPpWAaby/GFYQVcw/P9sHUbJKnwUBVm2E4JUtAeIh9VTnbwFM0w3W5MERg5Wkr8ekKCMt7x9bACzme3lYx2fea7+mMhyvtXtdopDXV+gx4rue1q74mraX4gWrh7oOVe66UkubBzhePatps8YB3Etm/HvJ2t2EulJD6kbSANTAABCODjNqlC/bkgx40ySYWcK8+YdlyBSRHjF3kQEdm9kT7QkyreIc6QHue5PMfD/HqXaft5fD+kcliX82qkwLoJtWw7JJjvaEJfT2sgCRvkLnQtSThHWDe4lMDtyydX7O36PKIIrDGOMu1Tef7+bvkeXUYYLrHGrlhHmzQOG7Lc3wZEZSyQawkOHF6h/rxBsqeF+ZrHZAjhd4rk346H4L1Q7Jbu4NxaqIiyrYrIqWItftneC0Lhj2Kjw30CaVYXEiUQog+3Axd/n1KkqiEEZTlHOR6DxWsXu64kYMF3rIfmgRvSn2v5utRripj1xKYayUdlcgH1JyXV27es9Tm5wjVIZdLCwAp4UQ6R9H8eNx2CNvIV50aI/zVik302FhDFqE6ClDC75XjN/abd53neSC7O7cRokS47FrTxoUtKwcbinn1gOhckJb0ePlbAh0x7rJY7ph5FIETS101bK5IisDjSNX7LzS/FbqsN5rseq2FYFAtF7Z5v2k6Oov9ayV4nEtajK8WvBupQKS7NDCGkNvm0uJfhe5HdVjKQH9a1JF+PPo/mUATWFluI3aIsxLtVeG7YRv24zJ+JQJDLs6xgNzHtQMf3gpBEVY98ubL2Er+i34jU+61p+2b5G0rc+fh8wNF4vBBCyDLgjvJFj+OeY9eVBBgB9vA4DqL85lrvnHoTgagxeZoUDt3HFtGHVXh+M0x7rYyfh5XSj3L0FRI7j3B4r169Nu/nec1IKa4SCgIGLtOFwODPxs3umoYG281jV/gtN3QIqWcQqOYaNPa22PylJHzgG+66I4VnwvWmzaMIrC12MW3vAK/DhZ9dhecHS2A5/RfgMHtTlt/v6bHywhZwoXrAKDu3ZZHfGZPBr2XFQJBbPG54WAJXGiwAY42JgcohhJC6A8+Y73ochwpGC9h9obO+Po9cgTvYn+qhg+pJBOJckRg6SJgttlSXhvS5DVLeKGwktZxRhs9BJPBPZcVal/CD+aa4Rdu+YdqPC7xmQwm2jR8ERA6fNOh32P5/1PF9YJFcztqZXNolrdtPlqFf+qwkuxlzQEgVkhL3Gr7wN0M92qlidxxcQXAaS8aFD6qD+GRxgA/57HrooHoyVyBL+GcCvhYi0LdCBbac4Q+HvHVIkJxO2bJQbAqA6So48G9vCc4TfiUwY3+nhH2JFeupkj14BhPh5xzeC32AaOCZBcbp8VI4mMeFE8TmEcysD/13cY8gG8p5lpCaoi/g3IwFL3YC1jbtULHbwD7gefA4uz10JohfvXpUvrqiXjqpnkTg0RI8U/hHniJwithcRBBBo/OsMuH7gRI0SIPyoFhLWJhcYNo2pm1Xgn7sV4F5b5a/IWXK6eJWLxPbyXcWeA3E+2Ehnweier8lyyeyxpYMzHcuaV8oAgmpLbCIR8YD+DwP9j/G3Ia0VB26wJ8kNv1IMc/SGyVgiVLiBIIIN/Q4DtvA71ME1hawyLnUC4Ql0HU7AMmNEdgwpsDrMImsow3RswjkQK66qyS82oQfqei9VaxPRFhAwH4vxyoJ2+3f1VVxULBtjUTO+bbeW1RYdpRgXHxBbCTfOxnfB9HOmzu8RzvnWkJqijVN+3aZPgu7Q0hD0sduDxVYaH3q1cMv/LJ66qh68QlEkMIEx4HgIgJhTfpZAAGYDYSvI6ExtiLPCFGYv64iM6zcU+gPbAFfmOPvsH4e5/iefwkgfPdUsVYKsL28S8b/YQV8yvE9moUQQvy4JcTFP1nGzo6L+TRXy/IuQhSBNXKOOzmeK3zegib62EFsgERjkd8TqUYQHDFV7LZqGEAAwscN2639RbwPUsAgKOMXOfoFwRFI4NzkKFJ/V+A1CC75fonHxg4ZYwOr8Rcc36NRCCHEHbgF/Vrcd51I4efGcR7HLRab8aKuqAcRiPrAmzoe0xnwddiqRJ68sLYEsVV8iK4Otw3pPd82bT+xW9X/czwWPjEPiPWtmJrnO0O8buL43r8x7b0CrzlB8td3DgME8EzM+L9rHyWEEELc6Nd5k7kBwwdWwO09jsO2/DSKwNoDYmo1x2OClvvZR2wARthsZdptYiNiw9gehpjDNu7u+m+hQBREMiNlAer+Yis93xYpooFdfS9QnePKAq+ZqCIwVuLxsc6g8fGhwyKAEEJcQeQxdlX+zK4oiabx8edcrCKw7vJ61XpgSJOnSBut4qPQljAsbKXyCYNjK8qcbS02MOKDEN4TQSjfUQGGqhmwsmHrGeZzbIUiL9J0FX1PBBBDG+tk5lqTEf6ThfIwItBmQhnGSPsgEbhErE9oUOsut3IIIS48LKV1c6ln4OP9GY/jsOP1SD12WK2LwJVN28xTgCUkf8TW2lL6rUqA3FMb6qRxT0jv+Yq2tAhqVDGzVILnLkSKlUvE3X8RE+DfCrwGltCvlHGcTBy0Sl/ouKonhJCgbKBzHHMDhs83PHQNrIDXS3gFIqqKWt8OxsN9PU/xWMjXC36Gq5XpPBDlhHx654i71a0QsPbNV+ETVNBAOJ4nNqjCBVhWsR3dVWBMHi+Dy7GVFpSSS28794nblgCz/BNCXMDC+fe6kCbhsaNpn/Y4DgUWbqnXTqt1Ebih+Fk7VwkgAlEgvKmM54It27PE1tfdsoJ9CqvhT8QvOz62oR8o8BpEch9Q5nPqkOWjfFMOx9J/kBDiCgLpfi4MLAtTyxxh2kjH4/r1udRDEVh7QKD5bteuLPmTE6Pf1qzQecHn4R9inV9Hlvmz4f+IdDjf8jgWUXCwHuYzucPCiGTUrRUYK+l7IeEo7udz/iWEeICSZnuzG0IBPu67ehwHP/mb6l091ypDihCBCAxZrUC/VTI/HLYvUdYIfgyfL2N/XiR+NYlhLTtVCqdDQHqcnSvQn9gCTlv/cF2HORw7d7n/pVISa2qUWEuTEEJIHmBoOLECi/laAwt3FEYY73Esgi8X13Pn1XJgCGq6rlOEOIYv4VN5REMUsorDKgh/QVTegK/duyX6nIkqAPfxOBbiCglRbwvwGWdXqB8XybLtgDaHSRnBNEsyfwHx1/Xs69L58NMSa2QeaUKqEOxWfKT3d2zQXJauHYxdi+EhfBZSmCF119Xsdm/SJVhdQSaM6+u982pZBK5a5AprcoEbE5UlEEDQUuHzHKmryd3EWgenSrhRTjCxw3dlE8/jYWo/N8DrzvZcyYXBXFlmCYQrwJCAx/UO7utYQ4P0zZwtPTNmSaK5VdzcCwkhEeDfYitO9OUxEsA1Bjsyk8QmJka6sNEenwVBCR9oVHWia4kfe3s+O1AjeF69d14tbwdPlOISDRdKLfOkriSiAurgogrHg2Lr7Q4p4r3Qb/B5hAXv1iIEIFIgnBBAlGIV95UK9l1mDsZ1HY6DCFw+MATbwQ0JHXgUgIRUIZivUDno3RztbbFlLx8z7VqxaUm2M+06z5seOzobsNu9wKL9KI/jUK3qr+y+2heBxbBugdUFBtGjETtn+EYgOfYdYi1w+4u1iAa9zsNU/KIUHhJnflP8LZ2viq0k8nGB162hn9daoT7rk2Xb6FiVu5Tr6+bqnZCafC66FAHoV1GIjAmXenwePuuzUvrqSLXIl9UA4sqVUjr3qaqilreDi602MUIFQb78QVeINeWPiOD5f0EbciD9R2ypNgx6bH0u1okLogd+LdjG2EjPF5PRkCI/GxbSg3RizAeE3/liE29XindMe19/Rl+41JxcShFICMlYUCK7AQISXatWwBp4sdR5kIIjQ9VQ4QpKg8JIwq2aGheBxSbihAXs8wVE4NOm3Wza0RHuh021iU4w2UTgGAkvXxVC7g8T6zNZCGwV71Ph/sH3TQf5wIrqUgEFInAOpxFCiIKk+yiluZm47W5sqc+s6ezCwMDQ4BP8iRrBr7L7LLW8HTwmhPeAKChk5UPQw4wq6RNY+JD6Bv4nqPu7nk48YQnA58X69z0d4LVw5j0rAmPwGVnms3iY47FLKAIJIYO4V9xdhdp0TibB++v/PI6bJdYXsJ9dWPsicFgI7wF/tUKl0bDFegaHktxn2r4SzAKItDbwnemo8HeelzFZQxjv4ng8BOAiXnpCyKDF4f0ex01m1wVmf/ELpnlYWLO5bkRgWwjv0RFQGCDX0CV1OoaworpKbHTv/wK8HgE7fzRtbAS++xsZEwK2pl3zfsGXkH4lhJDBIMfsbMdjGCEcDLgxYdfGNSM/3KCmis3/SJRa9gkMK3/f50xb3bS38rwGqUKwtYlo4n3raPzAwfZCbUHEEML5kdh6kwh8d3zfv4u15MEyeYDH8e9wConcohYPh1UDjseYCvk/UcyTkJkmduvRJXcgFsgJ4VZlIZBce0uP45DS5252X/2IwLDODalivmja7wq8DluLx+sqZa86GDvIR3i22MSqQRivD9vtIvL9kRsQFsx2FfCuiV5RYeQNTiGRAg9QRGeu53AMruGfKQJJyHykInBDh2MwR2L3iRkHcoMdPpQXHep4HKx/F7P7sq+cSWEOCygScON/TWq7BBD84H4oNgAkqABcybQ/iLWqRgUkeUWuxyNV5LuCHIGMMIse80v8ekKCgEXF+47HQNiwjnB+Pi22OpYrsALew+6rLxEY5r7/1mKrcAQBfiAoOXSmhFu+LQr9eaNYH0kkd/444HGIPkY+xd0jdC5Yof9MbF7Ec8Rab12B5fe/nEIIIXmeBS5gHhrGbssJkmrD99zH3x/VtLjNXmcisCfE94Lv0Hck+JYhSon9WOy28JNS3VtNELIPm7aHWIvocw7HImH3NQ4Culz8SCeS3xex8sY24oIV76iYxBobhBBS97gmfsZzpp3dlpP1xK+8KNKA0QpYhyKwM+T3Q+TWNxyPQZqAncRaBf9XZf2Hbd+/mXaoWOvfPxyFNW7Yv+r5R4mH9LpAnBYToLJiLsRYTFKdXdL79kxzZ9HTgpA6B88glx2pmIST1aJWOUL80opdlnXBTmpeBJYif9u3xGaCd50IfiLWj+Hnpr0Y8X57RayFDM63sP6hYkqf43tsrQJw64id2wcq/i4wbcci3+uxFWbwhoT0vvOhLLj5Pok3NQshpK5B1giXXSCIwEZ2W1ZQ5OBwj+NeFmvMIDmo5X2r2SV4T+SR+5XY7U3XlQXq6J4u1j9uiopC/BsFR2CUTUPSZFjJHpfCNX/zgb65RG/aKAEhiy1cBIJ8psj3Qum9Z7P+JRE3M7mZy1NZ5/6UuLsGlKqofKWL1cdK9J6u7+tzTUi0xkaUrx/HVjigOsgoj+Ou1MU/qUMR+GGJ3nd7sdGxJxchuNAQZLGmWGvZjvrvGmXqG0REvqSCD+IPUa6ofNJd5PseI9baOSKC4wFW7y3Els4rlv8Uschwtao2lkgwud77/ZLbsbrf47yaJfydiAZxt6S4WmtqlaTn2EyU4Lv4XMMoJgCOe9y7YZ5HTCq/2AuDCbp4dwXGjNt5a9evCHy/hO99olgz85VFvAe2q5/Xhjxl8HVYS+x2M5IXIz8hkocO1+uU0Bs6HmASSemDGT8v0JUQCpNjK/o5vTlm63cII2IK3w259k7Th3sUiYckAMED4h945Cq0W0s0kbd6fO/ePA9h1/7oKMH80yTuVQR6hKSFvGs2g2Ypzfala6L/rogKedeFTirP/OBjsa6ku1eYFvajxGaZcOUOqT5ffIrAEHm7hO8N0XO+WGvj30N4v6XakGfwsYwbeIQO/vH6L6KTkUuqXSeYhoyHMCaPThV9COqYldHm5XmAF8uaKgAPr5N7Bv34z+zr7pjEWgpqENeApY4STeauDtaL80zqvR7idnQJ5p8O8SslRawIdO2LdgmvMlMaiErX4IiwFrNh0+64gMP9tSTP4r6SllrXxX0ypGuCeeJAj+NgBLqGtzVFYCmBf8KlYpND31eC90+qmEObFsH+RWmuL4vdFp9YR/fMo7lWl6meXln66Itm2s07785z/LwR+lBcGOI5wAro6ota6Hu7+siuJOFbjVcSdwvnbCHp+cb1Gg6X8KNZh4h7NYh5HgKpHPhUtVic529dHiI0rGe8a+qaHo/vm41D1dDgCp7JL/C2LkwtRwe/G/KDMxvwVcBW7l51NGawMvu62Ojfi+tMAIJ7sgqimFnw9/dL78yPzY95F/9zHD9vpIqbMBnj8YD6uMi/Z1tEhX1eq3qcF53G/QXxOBWCYTJC3AMAsNvRG8H+HOv4eoimfBVsOj36sinE6+J6LsWmacPch5ruri4HsAz/mrczRSAe1NPL8DmYCKeadmyNjxU8sE8y7U7TLjdtmzq8XzBBP5L1L6mUxNpaZfgRe0iqr7fQA8tl67TBcyWcjzU9VvbvFvj7TMf3w8S+TsjnhcAq123ud/gY8BbE2AqeVALhtKrH946aJRBW7lUcj5mrLSyR7rPYy7fAciHtmlQMSFG2ucdxSAnzHG9nikAMwpfK9FlYJcEqVkwFiqiykWnnifWD+0Wdir802Ap+PutfYjFJLuqU2T+bKrH8OQJRr9jVQh12vsXJHiv7QmmDIAJdfcq2KcFYdZ3T3uRj4BPgk+yaX3XjkJ8jCI5ztQRGUchPUBHmKmaXFlhAuor0MFJ1Nei95cJCKc7VAs9R5Kp1tWRiMUArIEXgAF05H9ilW/khlxG2Cz8n1etv2agrWNyAd6rwOVVsBZBEHd8rKb22uYVOMim9782yW8O5me6xQt45xHsVX25bx/fDxPpagde8Je5bwjtLeNtVq6i4dX1QvcXHwCfgof2u4zGflfBSQmHO3F7cginge/b2cjdqX3+he7AcbCo2oM+F6ZI/ovZjj4XWZiGcC3YNtnA8Zr7HfJDJp8VWqnLlLslWzYnUpQgESONS7qixLXQg/tK09aukn9p0Bf4lsRZN5MH7i1hz/FDeJgMgGOTuggqrcN1gRK255rDEKnyHkM5jQ9O2cjxmhhS2trzpYalYTyf7MNjKw1rxhjAwJJNZHqIY/b5xSJ8/ybRdHY+BAPwovQiLD22XhpVHm2VLxXeHPy3uLhcvBbg+rvfYrlJ88A52IiY4HoMdD9/AEBhUviruqbHwrEeJuF7eyhSBmdaJ6RX4XAzib5p2r9iScVErnxbTCRd1fb9t2h/EWrluM+04cff/qAceNO2/IbxPv7i7KWBb54SQzgMR3a7BPM8FsEAgtcVrju8Lq/PxIZwT3mc/cbcqPiHhRDDWCrjGr3gcF9bY3MdDbLycXlQle3ukdfL6MmTPHSS5eGkl+xH3124exxXauYLgdfXbhEgv1u3iq46vT3nMBZnAirqnx3EPi925IhSBy61Gnqjg50NMnWHaraZdLTaX3uoV+B6jdTLAFu/Zpt1g2s0q+mCxPFjKV62kGpmnK8yw+Le4Jyne3XNizORT4hfA9LAEs6g/5nFee2grBmwr7+1x3L8kmqlFKsmT4p40ep8QriGS45/icdwzkhGFmurrk1R3T6W3gyGaXIOeIO4K+d0uFPeSnuiI04o4ly09BO0iKc4V60SPBR1S6PxR3H1a656GGj+/HhVgeEBUclsTviHId4Rwd1gnX9XJC3mMsI0G34kF4l+9AJYiREXCmXZlXYlOUBEK0TlW/zZawk/uWg/cp9crLFCjeY64+Qwh/935Ol58rDVwUr9Yx4cLXSqWggBrKaIbxzme1wVit9t9zgsPWwQsuW55zZTy+gxXC0/pGHPZWk/o2II/4Ysenwmfwt+Ie0qV+RVe5GfjC2J3V3wWJHMDXp8jxC1tyi66+HNdyCJn449NG+Z4HLas/+PZf5t4LuhgAfwHb1+KwGzcpZaX3SPwXZp0xYsG/7slsqxayEKd1BZk/A4P4HRdzJhOtk0q5PDwbFfxN1Rbi/69Wf+NcYiHspAIFG0WawgcN4PtK6Sa2d/xu8CH7lp9yPxTgtUZxRiABfAi06Z4Tq5vB3wtxMOzHpaD9Hl9Qz8vFfC8EID1K9M28Divu6X0CeWrEQi5p8XdvxI7CcgdeoIucvoDXsMN9Rru7PFdX5DSBAGkxN2XHDc/LPW/9RBNAO44QVJHPaILGJeoX/Tzz/TZcmPAeWOkLq4+73EuT4h7PtQ03xL3hO84n79I/hyLpI5FILZ7rhLr/9Yaoe8VUxHXzmEYadIR0gWvZt/MOS7VMq/yEIHplfIdOulhW/9/WRYL6YXABF1soKrNKM/zv81xcr1G/PyhNtG+hrXierHbY0v0vFJ6XumSYrAYHaaCw8fpvV8/i3WDs3O72F0L175dR8fLdSrq38wyNhtlWQ49JNk/RuwOhY9Qu18GRdqHtBWcylhEZ3uzeMZ5tOoi5hBtPs9UBF49FvC1r+hCyzX1C6ytU8VGc1+h91e3Ph9TGecEQwJch7A17+NLiPe72bPfNxE/l5endcw2RsTwkZIqCk6pBxEo+tD8j94AhAQFD7ALAyn6RELmXHjtQNUQSQRytX1YrP/VVh7fC9s0/6dtpk7oc1XUNMmymtPFBvi8LQEiogcByzsCaNb2+DxUnzhNrQHT9PPTJcEa1cIySWw0ajEl5x6TXEm/CbhH+39Lj2OxqD1aG8YmrN5zVHA06TUepyKwGJ90+HvfuMwWlJJ4W6s0bbCGpHqLdvNEqqFf6YM8nmXxjrE3VM9jorgnhR7MzZKjFGUOUKVqD4/nd7OK7qPERvsjqnqh3l8Qf2l3orFFnMt/VZz7cKjnggDzwvf0/KIgApeIn38rRWAJwc0M0zbSbNRzrjviBibbp4K+ONbc6OKWjO1++FEVW+R8vLjnIwsKrDmu0fUL9bwuKeJz8UDaXPyqBQSZC3BeH3N45x2b2NacGuGxiYX9G5+YXsziq2nieBl68K7SP3t+sdZACLuvl6mvIZCvE7cApb+L9b30zQGYrkK0ZgnOBwUTFnse6ys+0y5WUaG/mkRgrUcHZ/IPvXkICQJ8o37jMjmnurpdPwNWs4cjev5Y0ftGRF/nIp7LzH9UBJL8YEv+3xH9brB8X7DC/WeEYHJJVxQSRbv2s6tfIyaan0fwXF7R8ylmgVYLVFXu0XoSgdin/2ERqxRSX0AAvRx4cPUnZeRJBwXdCk4DqxnySEYtrQG2lX8p7tUj0mBr+myJns8d+vlccS/bV49AaGBrfkkEv9u5RYzNqC20sEPlU9AAPnC3ROx8fiS0sIOqWoXE6+ziIOntbzhGSQHgq3el2xIjJY2rjfWxQtwfwTEJ5/7Li3wPWN0vidh5wb/zPg7vwDyui5Qoce0K96a59+JDWmXU947wscZXCojrM8U/Qr1Hj58RkfO5PIKilFAEZgU+C6/x0pMc9KlYcC3tVoxDOrZ2bo7I+UMAnyzB0kgUAnkN747Ied0g0dxCizoQ8tdE5LsgV+fpksXCnFraLbG2Fpfo/Erzcx2TxfCK3quVrnoDF4ufCsu1UQRWCYgqw3ZCNy8/yQIqu9xY5s/ENiVy5N0ZAQGINBfvh/R+s/S8Ku1bBgvF8bznvcDW+bcrcE8MBmlRkCR5xW3gZEpat59cTb6AyDv645DeC7kZzwxp0eYDAsdQIvUd3ioUgdXELSGswkjtAQvxGRX6bFgej5Rwy9O5gCCVAyT8WtvIFYdyifdX6LywTYU8iXM5vL2BnxdSi1TKbeFvOjanrfAXI/z6+3oGooJjDVWR7AK+tqdKuDZL7FycXoFFDiruHCilSdhNKAJLCszn3xduC5NlYCvjFPHYBg4RpItA3czjxD/jvivYw4bf16ElXM2/LdbCeFUZ+xI+V9/R/mQlgeJZoPfH8WW8R2DdQhTwYbqYWPEF3d0y/ODdJNbaPBAdHGFgUUVy89NKINYgKM/XxVa5AmYQmIKa0c/w1qAIrFZm6OqJ0cJEdBItOoVQqq/oBxH8nS4VW4MU/nSlfLI9rxP5WTKo8kIJ+EjF7aG5HughAh8lVEq5ULgFHPZCCT7VKCV2Z5nGJizz83K9KJaISdMGq0uspXkgQCSCoI+QfPuLpv1O3PIBunKjzhu3lvDazFYhe5iw7CJFYA1wm2TJN0XqjrskYGWQ/HdTXBrGjQrrYfSMChlsw6FGalipOvAQQnUCpEvaWc+9XP5EEGQIMviMCk8k+w3TqR3+vj/SB+H9HNYl4yXT9hO7zR7m2IRwgTUa/nKo7nRHPtHU390lQw/4gjRvvNZAYEjEgPX5IV30oLrHY2X6XOxufUWsa8kLId5f2Jn4q14XLJhLkdYqVSP3R1WdR71UDMkHCmuj9uNB7Iq6BD5w381nbQh22yNNRZuMOukgmfWdiyTW3BzGd4NV8EqdfFFnFTV5UV8TZaqGO7xPp9gSXshLdq++3/sV7HMEjJyrlpG9VLThvFCpwaW+d0rPC6L2HhWYb0V4rHU4vh6lyaJa4Qhj80+m3TRobKJU4QiPsTldx+ZNKubzX/hkUhrGjJDGSSsXsr6na7SXGiykYO3Gdiws0X/T86kEsNherUaOfU3b3bSNdN7ocLi3PtJrgcAu+NE/ErH7I6qMqKYvSxFob5gT9QbZgd1RV8BP5xsSlm+oEYKpnpJkSVikAucaFUob6r8oIj9GbC3hJhUMSR3TsM7AoX+GTuQ4x1ci1v+wLlylbS3TNhVb/mmSaaP0oYAScnF9KHWraJiv5zVdzwuRo0urYLz928EyE1Ox3Bnxc8L3u07bBB2bq+vPK+UYm7hWs/UavqvXcJrTrdbbI61bbSOtUzaX/llz8kUG4zNfUEEd1hZpv47FRToW39OFCNwcUM5tQUSuDb7fVG1r6bWZpPPGaF1wNelYS58T5sQP9brg/oLVt1xBVa+a9oRULtI5DGJVcM9SBGYBE9KRaiHZjN1RF2DSg7/RPVX2vd+RFQM4Enovp8VSn5TW96gUTJflI5Mh/tr0IZU+r7SA6KzSMXe+Xqcg20UxfRhWk0/je7KiFQ/XrrEUYxMLrlR3T6HUMBDdCHxqD1Fc4H16dKHVVaX3V/r5n543+vX+qqQAu8i0K2pABFbV3EsRuAys5I4Sm5F+A3ZHzYPkpr+vIUHbX2PXp6uKHrBB6ZHoldIrNZUWshCe86RYd4/aJGqLxQUSHStq3RBnFywHtg2+atrr7IqaBrnjzqnyFSchhBBCERgyT4kNEnmRXVGTwHcJpZb62RWEEEIoAslgnhObAoFpJmoL1OdFIEgnu4IQQghFIMkF0mmgJM7l7IqaACkOkHOP5cMIIYQQisCCpMt4fUtKkxyTlAdk0v86BSAhhBBCEegCItt+LTbh5qN1cs6IGEPOJuRfq/bgicvERn1TABJCCCEUgV4g0SvqP6LSwewaPUeE5//LtKNNu0Rs5vNqHSNIxYFqMN8U+gASQgghFIFFgi1h1DxFLUj4mC2ugXOC1Q+VJK7Q85oiNpP8eWIz/1cjyOKPUnBIBt3LYUsIIYSsCJNF+4HSNqjJuI9ph5u2q9gKB9UEciGixuWDpj0gtpYsaihfb9oBVXxtcF6niK3dSQghhBCKwJJwq2l3m/YZ075i2t5iazJGlZfFbmujPS/W7w+g1A0CYL4ttrZktXKnaadlnBchhBBCKAJLBmqZ3q/CCrUP4TcIS9rmYmu6VhIUA0cVlIe0oXYkioNnlgra2bQzTdsuAt+3mGtwvvY/yw4RQgghFIFlBVHEr2i71LQNTdvFtJ1M28i0DrFbxrESfHavfj58FrEd+ozYrd7nVPSh0Hlq0HVfX+y2KZJit1dxvyOC+VSx29opDkNCCCGEIrCSIBr1SW0/MW1V0zYzbbJYv7s1TBtpWpvYIIxm05r0egwWiUkVeT0q9LrEWr5g8XrPtLdV+EF8vmHarDzfq10F6VdNO0SFabWCPrjKtB8Ii8MTQgghFIERZYa2O/T/iMoea9o408aITcUyVEVho/4dVq1+FX+IQl6kYgfpaT7Sf/sCfj5E6PZifRb3VOFZzUDsIlXPXzi0CCGEEIrAagLWvZnaSgUEJQJWpoj1+9u6RvruGtN+LjbIhRBCCCEUgcSwkmnbqvDDv2tKtKOVXXjNtHNMu03sljghhBBCKALrDkTxYtt4ZbEBHluYtqVpG4jdXh5SQ+eKrfDfiq1gMoOXnhBCCKEIrCYaVawNE1vNAr59CPaAzx+2hjOjWhEYElehhwb/veFirXwQfWuZtrZp64jN6dcm1ZvapZD4u1fs1u9THEKEEEIIRWA10qtC7UDTdhNrrftYxWC3/j2lAhCCsVnFHaJ5O7TVS4k/RP2ifvFlYpNxE0IIIYQisKpBzr4TTNtBbHoWROpOYrd8AqKg7xEb8Yu6zH3sEkIIIYQisJb4l7bLTdvLtMNMW72O+wOWv7+adp3YqiudHCKEEEIIRWAt84y2qWKtghCDm9bR+SM9zg0q/l5SMUgIIYQQisC64S2xNW+vFJva5VCxpeYQDFJrfoAI9nhWhd/fTPtAbHAMIYQQQigC6xaUgbtdGyKA9zFtV7Fl5lau4vNCZZN3xUb6ItDjSV5qQgghhCKQZGe6aRdoQ/4/VPzYXpbVHY4674utZ/y4aQ+Z9qgwwTMhhBBCEUiceErbL0zbSNvmpm2sP4+NwHeEte9V06aJ3e59SRuDPAghhBCKQFIkSJnyvDakUUGuQWwTI7J4Q7HJqNfV/48q4feYI3Z793UVfq+Y9qZpH0ppayETQgghhCKQiE02jfaC2Hq6qCySTi69immrmjZBfx6nohHBJm362sZB1x8iE7n6sG2LAI4FKvgg7mZoe0/sVi/+DivfEl4GQgghpDqJpVIp9gIhhBBCSJ0RZxcQQgghhFAEEkIIIYQQikBCCCGEEEIRSAghhBBCKAIJIYQQQghFICGEEEIIoQgkhBBCCCEUgYQQQgghhCKQEEIIIYRQBBJCCCGEEIpAQgghhBBCEUgIIYQQQigCCSGEEEIIRSAhhBBCCKEIJIQQQgghhfh/AQYAet/ybEnKzzsAAAAASUVORK5CYII=",

        }
    };

};

Dashboard.prototype = {
 constructor:Dashboard,
initTab:function() {
    $('.column').matchHeight();
    this.initOrReset();
    this.conductSearch();
},

 initOrReset:function(){    
    if(isNull(this.esClientFbHome)){
        esClientFbHome  = new Elasticsearch( esHost, fbQueryHome,this.liveParametersHome,"fbQueryHome");
    }
    else{
        esClientFbHome.reset();
    }
    if(isNull(this.esClientInstagramHome)){
        esClientInstagramHome  = new Elasticsearch( esHost + '/instagram', function(){return {}},this.liveParametersHome,"instagramQueryHome");
    }
    else{
        esClientInstagramHome.reset();
    }
    if(isNull(this.esClientYtHome)){
        esClientYtHome  = new Elasticsearch( esHost +  '/youtube/video', function(){return {}},this.liveParametersHome,"youtubeQueryHome");
    }
    else{
        esClientYtHome.reset();
    }
    if(isNull(this.esTwitterClientHome)){
        esTwitterClientHome  = new Elasticsearch(esHost ,twitterQueryHome,this.liveParametersHome,"twitterQueryHome");
    }
    else{
         esTwitterClientHome.reset();
    }
    if(isNull(this.esClientMediaHome)){
        esClientMediaHome  = new Elasticsearch( esMediaHost + '/mediabuzz', mediaQueryHome,this.liveParametersHome,"mediaQueryHome");
    }
    else{
        esClientMediaHome.reset();
    }
    if(isNull(this.esClientMulti)){
//        esInstances = [esClientFbHome,esClientInstagramHome,esClientYtHome,esTwitterClientHome,esClientMediaHome];
        esInstances = this.esInstancesGeneration();
        console.log("esInstances...",esInstances);

        esClientMulti =  new ElasticsearchMultiQuery(esHost);
    }
    else{
        esClientMulti.reset();
    }

    //CALL-01 FACEBOOK
    // alert(globalStartDuration);
    esClientFbHome.addIndexPattern("fbdata-",facebookTimeFormat);
    esClientFbHome.addIndexType("post");
    esClientFbHome.addTermMatch("Page.name.raw", getFacebookKeywords(globalTagValue));
    //esClientFbHome.addTermMatch("Group.name.raw", getFaceBookKeywordsGroups(globalTagValue));
    esClientFbHome.addDateRangeMatch("created_at",globalStartDuration, globalEndDuration);

    //CALL-02 INSTAGRAM
    esClientInstagramHome.addTermMatch("ownedBy.name", user);
    esClientInstagramHome.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esClientInstagramHome.addDateRangeMatch("created_time",globalStartDuration,globalEndDuration);

    //CALL-03 YOUTUBE

    esClientYtHome.addTermMatch("ownedBy.name", user);
    esClientYtHome.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esClientYtHome.addDateRangeMatch("snippet.publishedAt",globalStartDuration,globalEndDuration);


    //CALL-04 TWITTER
    esTwitterClientHome.addIndexPattern("twitter-",twitterTimeFormat);
    esTwitterClientHome.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esTwitterClientHome.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);

    //CALL-05 MEDIA
    esClientMediaHome.addDateRangeMatch("actualTimeStamp",globalStartDuration,globalEndDuration);
    var keywords = getKeywords(globalTagValue);
    esClientMediaHome.addMultiMatch([ "Title" , "Content" ] , keywords.join(" "));

    esFoamTree.removeTermMatch("ownedBy.query");
    esFoamTree.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esFoamTree.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esFoamTree.addDateRangeMatch("created_at",globalStartDuration,globalEndDuration);

    //MULTI INSTANCES
    esClientMulti.applyESInstances(esInstances);

},

esInstancesGeneration : function(){
    var splitUserPermissions = {};
    for(var index in userPer){
      var newIndex = index.split(/(?=[A-Z])/);
        if(newIndex[0]=="analytics"){
            splitUserPermissions[index]=userPer[index]
        }
    }
    var namesArray = [];
    this.namesArrayCompare = [];
    var positionsInArray = {};
    var socialMedia = ['Facebook','Twitter','Media','Youtube','Instagram'];
    var clientNames = [esClientFbHome,esTwitterClientHome,esClientMediaHome,esClientYtHome,esClientInstagramHome];
    var clientNamesString = ['esClientFbHome','esTwitterClientHome','esClientMediaHome','esClientYtHome','esClientInstagramHome'];
    
    for(var splitIndex in splitUserPermissions){
        if(splitUserPermissions[splitIndex]){
            var partName = splitIndex.split(/(?=[A-Z])/);
            for(var socialMediaIndex=0;socialMediaIndex< socialMedia.length;socialMediaIndex++){
                if(partName[1]==socialMedia[socialMediaIndex]){
                    namesArray.push(clientNames[socialMediaIndex]);
                    this.namesArrayCompare.push(clientNamesString[socialMediaIndex]);
                }
            }            
        }
    }
    console.log('names array generated',namesArray);
    console.log('names array 1 generated',this.namesArrayCompare);
    
    mapForClients = {};
    for(var index=0;index<clientNamesString.length;index++){
        var indexInSplit = this.namesArrayCompare.indexOf(clientNamesString[index]);
//        if(indexInSplit==-1){
//            this.mapForClients[clientNamesString[index]] = null;
//        }
//        else{
            this.mapForClients[clientNamesString[index]] = indexInSplit;
//        }
    }
    console.log('map for clients',this.mapForClients);
    return namesArray;
},    
//        esInstances = [esClientFbHome,esClientInstagramHome,esClientYtHome,esTwitterClientHome,esClientMediaHome];

globalFunctionChange :function(globalTagValue){

    this.initOrReset();
   
    this.conductSearch();
},

onTimeChange :function(startValue,endValue){
// alert(startValue);
    console.log('globalDuration,tagValue',globalDuration,globalTagValue);
    this.dateRangeChangeFunctions(startValue,endValue);
  
    this.conductSearch();
 
},

customTimeChange:function(startValue,endValue){

    esClientFbHome.addDateRangeMatch("created_at", startValue,endValue);
    esClientInstagramHome.addDateRangeMatch("created_time",startValue,endValue);
    esClientYtHome.addDateRangeMatch("snippet.publishedAt",startValue,endValue);
    esTwitterClientHome.addDateRangeMatch("created_at",startValue,endValue);
    esClientMediaHome.addDateRangeMatch("actualTimeStamp",startValue,endValue);

    esFoamTree.removeTermMatch("ownedBy.query");
    esFoamTree.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esFoamTree.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esFoamTree.addDateRangeMatch("created_at",startValue,endValue);
  
    this.conductSearch();
 
},


 dateRangeChangeFunctions:function(startValue,endValue){
    
    esClientFbHome.addDateRangeMatch("created_at", startValue,endValue);
    esClientInstagramHome.addDateRangeMatch("created_time",startValue,endValue);
    esClientYtHome.addDateRangeMatch("snippet.publishedAt",startValue,endValue);
    esTwitterClientHome.addDateRangeMatch("created_at",startValue,endValue);
    esClientMediaHome.addDateRangeMatch("actualTimeStamp",startValue,endValue);

    esFoamTree.removeTermMatch("ownedBy.query");
    esFoamTree.addTermMatch("ownedBy.id", user + "%%%" + globalTagValue);
    esFoamTree.addTermMatch("ownedBy.query",globalTagValue.toLowerCase());
    esFoamTree.addDateRangeMatch("created_at",startValue,endValue);

},




 conductSearch:function() {
 esFoamTreeCluster =new ElasticsearchClusterClient(esHost +"/" +  esFoamTree.indicesString , generatemediaFoamMainQuery,{});

   this.handleLoading(true);
     var self =this;
    esClientMulti.search(function(response){
      self.multiRenderFunction(response);
    });
},

 handleLoading:function(isLoading) {
    if (isLoading) {
        //printFunctionClearFb();
//      dataContentFbBar,dataContentFbPie,dataContentFbPages,dataContentFbPosts

        $(".dataContentChartHomeMultiPie").empty();
        $(".dataContentChartHomeMediaStats").empty();
        $(".dataContentChartHomeTrends").empty();
        $(".dataContentChartHomeSocialSenti").empty();
         $(".dataContentHashTagTable").empty();
        $(".dataContentChartHomeMultiPie").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $(".dataContentChartHomeMediaStats").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $(".dataContentChartHomeTrends").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
       $(".dataContentChartHomeSocialSenti").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
      $(".dataContentHashTagTable").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
     $(".dataContentFoamtreeListening").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

    } else {
        $(".dataContentChartHomeMultiPie").empty();
        $(".dataContentChartHomeMediaStats").empty();
        $(".dataContentChartHomeTrends").empty();
        $(".dataContentChartHomeSocialSenti").empty();
         $(".dataContentHashTagTable").empty();
//         $(".dataContentFoamtreeListening").empty();

    }
},

    
    
multiRenderFunction:function(returnObject){
    homeCharts=[];
    console.log('return object',returnObject);
    var key = 0;
    indexForFbResponse = this.mapForClients.esClientFbHome;
    indexForTwitterResponse = this.mapForClients.esTwitterClientHome;
    indexForMediaResponse = this.mapForClients.esClientMediaHome;
    indexForYtResponse = this.mapForClients.esClientYtHome;
    indexForInstagramResponse = this.mapForClients.esClientInstagramHome;
//    {esClientFbHome: null, esTwitterClientHome: 0, esClientMediaHome: 2, esClientYtHome: null, esClientInstagramHome: 1}

    var responseFb = returnObject.responses[indexForFbResponse];
    var responseInstagram = returnObject.responses[indexForInstagramResponse];
    var responseYt = returnObject.responses[indexForYtResponse];
//    var responseTwitter = returnObject.responses[indexForTwitterResponse];
//    var responseMedia = returnObject.responses[indexForMediaResponse];
     this.handleLoading(false);
    
    if(indexForTwitterResponse >= 0){
            this.loaderStateForPermissionsTwitter();
            var responseTwitter = returnObject.responses[indexForTwitterResponse];
            createFoamTree(foamtreeId.home,esFoamTree);
            this.twitterGraphDataMulti(responseTwitter);
            this.pieGraphSentimentsTwitter(responseTwitter);
            this.createHashTagTable(responseTwitter);
    }
    if(indexForTwitterResponse < 0){
            this.loaderStateForNoPermissionsTwitter();
    }
    if(indexForMediaResponse >=0){
            this.loaderStateForPermissionsMedia()
            var responseMedia = returnObject.responses[indexForMediaResponse];
            this.barGraphDataGenerate(responseMedia);
    }
    if(indexForMediaResponse < 0){
            this.loaderStateForNoPermissionsMedia();
    }
    this.multiGraphDataPie(returnObject);
//    this.multiGraphDataPie(returnObject);
//    this.barGraphDataGenerate(responseMedia);
//    this.twitterGraphDataMulti(responseTwitter);
//    this.pieGraphSentimentsTwitter(responseTwitter);
//    this.createHashTagTable(responseTwitter);
    slimScrollCall("slimScrollTableHome","500px");
    slimScrollCall("slimScrollComboModalHome","500px");


},   

loaderStateForNoPermissionsTwitter : function(){
    $('.dataContentFoamtreeListening').html('No access permissions');
    $('.dataContentHashTagTable').html('No access permissions');
    $('.dataContentChartHomeTrends').html('No access permissions');
    $('.dataContentChartHomeSocialSenti').html('No access permissions');
},
loaderStateForPermissionsTwitter : function(){
    $('.dataContentFoamtreeListening').html(' ');
    $('.dataContentHashTagTable').html(' ');
    $('.dataContentChartHomeTrends').html(' ');
    $('.dataContentChartHomeSocialSenti').html(' ');
},
loaderStateForPermissionsMedia : function(){
    $('.dataContentChartHomeMediaStats').html(' ');
},
loaderStateForNoPermissionsMedia : function(){
    $('.dataContentChartHomeMediaStats').html('No access permissions');
},    
    
 multiGraphDataPie :function(returnObject) {
     
     var graphLabels = {
        Fb : '#2dd3d3',
        Instagram : '#0060c2',
        Yt : '#279cf9',
        Twitter : '#f9506d',
        Media : '#04a182'
     } 
     var data = [];
     var statsPieTableRenderArray = [];
     var totalComments = 0;
     var newListFromMapping = {};
     
     for(var index in this.mapForClients){
        if(this.mapForClients[index]>=0){
            newListFromMapping[index]=this.mapForClients[index];
        }
     }
     
     console.log('the new list',newListFromMapping)
     
     for(var dataIndex in newListFromMapping){
         
         var indexNumberForResponse = newListFromMapping[dataIndex];
         console.log('index number for response 1',newListFromMapping);
         console.log('index number for response',newListFromMapping[dataIndex]);
        if(dataIndex.indexOf('Fb')>=0){
            var alertVal = dataIndex.indexOf('Fb');
            if(!isNull(returnObject.responses[newListFromMapping[dataIndex]].aggregations)){
                var bucket = returnObject.responses[newListFromMapping[dataIndex]].aggregations.trends.buckets;
                    for(var index in bucket){
                        console.log("comments,");
                        totalComments += bucket[index].comments.value;
                    }
            }
             console.log("comments....",totalComments);
            var totalFbHits = totalComments + returnObject.responses[newListFromMapping[dataIndex]].hits.total;
            for(var index in graphLabels){
                if(index.indexOf('Fb')>=0){
                    var colorFb = graphLabels[index];
                    var keyFb = "Facebook"
                }
            }
             data.push({
                color:colorFb,
                key:keyFb,
                label: keyFb,
                value:returnObject.responses[newListFromMapping[dataIndex]].hits.total + totalComments 
            });
            console.log('display value fb dashboard',returnObject.responses[newListFromMapping[dataIndex]].hits.total + totalComments);
        }
         
           else {
               var keyVal = newListFromMapping[dataIndex];
//               console.log('hi ther is this data',returnObject.responses[newListFromMapping[dataIndex]]);
             //alert(colorPie[dataIndex]);
               var keyWordsForCompare = ["Media","Yt","Instagram","Twitter"];
             
               if(dataIndex.indexOf('Media')>=0){
                  var colorInfo = graphLabels['Media'];
                  var keyInfo = 'Media'
               }
               if(dataIndex.indexOf('Yt')>=0){
                  var colorInfo = graphLabels['Yt'];
                  var keyInfo = 'Youtube'
               }
               if(dataIndex.indexOf('Instagram')>=0){
                  var colorInfo = graphLabels['Instagram'];
                  var keyInfo = 'Instagram'
               }
               if(dataIndex.indexOf('Twitter')>=0){
                  var colorInfo = graphLabels['Twitter'];
                  var keyInfo = 'Twitter'
               }
               
            if(!isNull(returnObject.responses[keyVal].hits)){
            data.push({
                color:colorInfo,
                key:keyInfo,
                label: keyInfo,
                value:returnObject.responses[keyVal].hits.total
            });
            }
        }
     }

 graphLabel = ["Facebook","Instagram","Youtube","Twitter","Media"];
 colorPie = ['#2dd3d3','#0060c2','#279cf9','#f9506d','#04a182'];
//var data = [];
//var statsPieTableRenderArray = [];
//var totalComments = 0;
//    for(var dataIndex in returnObject.responses){
//        if(dataIndex == 0) {
//            if(!isNull(returnObject.responses[dataIndex].aggregations)){
//            var bucket = returnObject.responses[dataIndex].aggregations.trends.buckets;
//              
//
//                    for(var index in bucket){
//                        console.log("comments,");
//                       totalComments += bucket[index].comments.value;
//                    }
//                
//            }
//            console.log("comments....",totalComments);
//            var totalFbHits = totalComments + returnObject.responses[dataIndex].hits.total;
//        
//            data.push({
//                color:colorPie[dataIndex],
//                key:graphLabel[dataIndex],
//                label: graphLabel[dataIndex],
//                value:returnObject.responses[dataIndex].hits.total + totalComments 
//            });
//        
//        }
//        else {
//             //alert(colorPie[dataIndex]);
//            if(!isNull(returnObject.responses[dataIndex].hits)){
//        data.push({
//                color:colorPie[dataIndex],
//                
////              label: graphLabel[dataIndex] +' - '+ commaSeparateNumber((returnObject.responses[dataIndex].hits.total)),
//                 key:graphLabel[dataIndex],
//                label: graphLabel[dataIndex],
//                value:returnObject.responses[dataIndex].hits.total
//            });
//            }
//        }
//            
//    }   
console.log("dataaas",data);
    this.graphDataAllChartHome(data);
    
},

graphDataAllChartHome:function (graphData) {
    console.log("graph data insta pie",graphData);
    if(graphData.length < 1){
      $('.dataContentChartHomeMultiPie').html('No data available');
    }
    // $(".dataContentAllPie").empty();
    var piechart = AmCharts.makeChart("chartHomeMultiPie", {
       "type": "pie",
        "theme": "light",
        "autoMargins": true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        
        "dataProvider": graphData,
        "titleField":"label",
        "valueField": "value",
        "startDuration":0,
         "responsive": {
            "enabled":true
         },
         "legend":{
           "autoMargins":false,
           "marginLeft":20,
            "enabled": true,
            //"spacing":20,
            "markerType":"circle",
            "markerLabelGap":2
           
            //"horizontalGap" :20,
            
         },
          "labelsEnabled":false,
        //"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      
        "colorField": "color",
        "innerRadius": "60%",
       
        //"angle": 15,
        "export": {
              "enabled": true,
               "fileName":"social media statistics",
               "menu": [
                {
                  "class": "export-main",
                  "menu": [
                    {
                      "label": "Download as image",
                      "menu": [
                        "PNG",
                        "JPG",
                        "SVG",
                        "PDF"
                      ]
                    },
                    {
                      "label": "Download data",
                      "menu": [
                        "CSV",
                        "XLSX",
                        "JSON"
                      ]
                    },
                    {
                      "label": "Print",
                      "menu": [
                        "PRINT"
                      ]
                    }
                  ]
                }
              ]
            }
    });

 





homeCharts.push(piechart);
  
},

 barGraphDataGenerate :function (response) {
    var keys = [];
    var totalTrends = [];
     var dataBar = [];
     if(!isNull(response.aggregations)){
         for (var bucketIndex in response.aggregations.source_data.buckets) {
        var colorVar = bucketIndex;
        totalTrends.push({
                
                
                key: (response.aggregations.source_data.buckets[bucketIndex].key),
                value: (response.aggregations.source_data.buckets[bucketIndex].doc_count)
        });
    }
         
     }
    
   
//    console.log("Total trends are " , totalTrends);
    
    this.barGraphCallMedia(totalTrends);

},

barGraphCallMedia:function (data) {
    if(data.length < 1) {
       $('.dataContentChartHomeMediaStats').html('No data available');
    }
    var chart = AmCharts.makeChart("chartHomeMediaStats", {
        "type": "serial",
        "theme": "light",
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0.5,
            "gridAlpha": 0,
             "minimum":0,
             "integersOnly":true
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "fillColors":"#04A182",
            "valueField": "value"
        }],
        "chartCursor": {

            "cursorAlpha": 0

        },
        "categoryField": "key",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "labelRotation":-40

        },
        
        "export": {
          "enabled": true,
          "fileName" : "news media statistics",
          "menu": [
            {
              "class": "export-main",
              "menu": [
                {
                  "label": "Download as image",
                  "menu": [
                    "PNG",
                    "JPG",
                    "SVG",
                    "PDF"
                  ]
                },
                {
                  "label": "Download data",
                  "menu": [
                    "CSV",
                    "XLSX",
                    "JSON"
                  ]
                },
                {
                  "label": "Print",
                  "menu": [
                    "PRINT"
                  ]
                }
              ]
            }
          ]
        }

    });
homeCharts.push(chart);
},

 twitterGraphDataMulti :function(response) {
    data = [];

    // var tweetTrend = [],
    //     retweetTrend = [],
    //     replyTrend = [];
    console.log("arun", response);
    for (var bucketIndex in response.aggregations.trends.buckets) {
        barGraph = {};
        var bucket = response.aggregations.trends.buckets[bucketIndex];
        var milliSecondValue = bucket.key;
        var date = new Date(milliSecondValue);
        var day = date.getDate();
        var month = date.getMonth()+1;
        var hour=date.getHours();
        var minute=date.getMinutes();
        var seconds=date.getSeconds();
       
        var year = date.getFullYear();
        barGraph["dateFull"] = year + '/' + month +'/'+day+' '+hour+':'+minute+':'+seconds;
        month=monthAbbr[month-1];
        barGraph["date"] = day + ' ' + month + ' ' + year;
        barGraph["positive"]=0;
        barGraph["negative"]=0;
        barGraph["neutral"] =0;
         barGraph["mixed"] =0;


        // barGraph["date"]=milliSecondValue;
        for (var typeBucketIndex in bucket.sentimentsTwitter.buckets) {
            var typeBucket =bucket.sentimentsTwitter.buckets[typeBucketIndex];
                  
            if (typeBucket.key == "positive") {
                barGraph["positive"] = typeBucket.doc_count;
             
            } else if (typeBucket.key == "negative") {
               
                barGraph["negative"] = typeBucket.doc_count;;
            } else if (typeBucket.key == "neutral") {
                
                barGraph["neutral"] = typeBucket.doc_count;
            }
           else if (typeBucket.key == "mixed") {
                
                barGraph["mixed"] = typeBucket.doc_count;
            }
        }
        data.push(barGraph);
     
    
};
 this.twitterTrendGraphHome(data);

},

twitterTrendGraphHome:function (data) {

    console.log("data trend bar",data);
    $('.dataContentChartHomeTrends').empty();
    if(data.length < 1){
        $('.dataContentChartHomeTrends').html('No data available');
    }
    var barOrLine = "smoothedLine";
    if(data.length<barOrAreaThreshold){
        barOrLine ="column"
    }
    var chart = AmCharts.makeChart("chartHomeTrends", {
        "type": "serial",
        "theme": "light",

       
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.5,
            "gridAlpha": 0
        }],
        "colors":["#04a182","#f9506d","#279cf9","#04A283"],
        "dataProvider": data,
        "legend":{
            "enable":true,
            // "data":[{"title": "positive", "color": "#50b6b6"},{"title": "negative", "color": "#298080"},{"title": "Neutral", "color": "#2dd3d3"},{"title": "mixed", "color": "#04A283"}],
          "markerType":"circle"
        },
        
        "graphs": [{
             "balloonText": "<span class='amchartTooltip'>Positive: [[positive]]</span><span class='amchartTooltip'>Negative :[[negative]]</span><span class='amchartTooltip'>Neutral :[[neutral]]</span><span class='amchartTooltip'>Mixed :[[mixed]]</span>",
          

            "fillAlphas": 0.9,
            "lineAlpha": 0.3,

           // "labelText": "[[value]]",
            "title": "Positive",
            "type": barOrLine,
            "fillColors": "#04a182",
            "valueField": "positive"
        }, {
            "showBalloon": false,
            "fillAlphas": 0.9,
            "lineAlpha": 0.3,
          //  "labelText": "[[value]]",
            "title": "Negative",
            "type": barOrLine,
            "fillColors": "#f9506d",
            "valueField": "negative"
        }, {

            "showBalloon": false,
            "fillAlphas": 0.9,
            "lineAlpha": 0.3,
          //  "labelText": "[[value]]",
            "title": "Neutral",
             "type": barOrLine,
            "fillColors": "#279cf9",
            "valueField": "neutral"
        },
        {

            "showBalloon": false,
            "fillAlphas": 0.9,
            "lineAlpha": 0.3,
          //  "labelText": "[[value]]",
            "title": "Mixed",
            "type": barOrLine,
            "fillColors": "#04A283",
            "valueField": "mixed"
        }],
       

        "chartCursor": {
            "cursorAlpha": 0
        },
       
        "categoryField": "date",

        
        "categoryAxis": {

            "axisColor": "#DADADA",
           
             "labelRotation":-40,

           

        },
        "export": {
          "enabled": true,
          "fileName": "tweet trends",
          "menu": [
            {
              "class": "export-main",
              "menu": [
                {
                  "label": "Download as image",
                  "menu": [
                    "PNG",
                    "JPG",
                    "SVG",
                    "PDF"
                  ]
                },
                {
                  "label": "Download data",
                  "menu": [
                    "CSV",
                    "XLSX",
                    "JSON"
                  ]
                },
                {
                  "label": "Print",
                  "menu": [
                    "PRINT"
                  ]
                }
              ]
            }
          ]
        },
            
    });

   homeCharts.push(chart);
   // var self = this;
    chart.addListener("zoomed", function(e) {
        console.log("zoomed event index",e.startIndex);
          console.log("zoomed event start",e.chart.dataProvider[e.startIndex].dateFull);
             console.log("zoomed event end",e.chart.dataProvider[e.endIndex].dateFull);
          var startValue=e.chart.dataProvider[e.startIndex].dateFull;
          var endValue=e.chart.dataProvider[e.endIndex].dateFull;
          var startDate=e.startValue;
          var endDate=e.endValue;
        convertDatetoEs(startValue,endValue,startDate,endDate,true);
    });
},
 pieGraphSentimentsTwitter :function(response) {
      colorMapPieTwitter = {
       positive: "#04a182",
        negative: "#f9506d",
        neutral: "#279cf9",
        mixed: "#0060c2"
   };
    var graphData = [];
   var  bucketIndex=0;
    console.log('response for twitter senti is', response);
    for ( bucketIndex=0;  bucketIndex<(response.aggregations.twitterSentimentsPie.buckets).length ; bucketIndex++) {
        var bucket = response.aggregations.twitterSentimentsPie.buckets[bucketIndex];
          console.log("bucket chart",bucket);
        if (bucket.key == "neutral") {
            // displayPieLegendsSenti(0, 'neutral', bucket.doc_count);
            graphData.push({
                key: "Neutral",
                color: colorMapPieTwitter['neutral'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "positive") {
            // displayPieLegendsSenti(1, 'positive', bucket.doc_count);
            graphData.push({
                key: "Positive",
                color: colorMapPieTwitter['positive'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "negative") {
            // displayPieLegendsSenti(2, 'negative', bucket.doc_count);
            graphData.push({
                key: "Negative",
                color: colorMapPieTwitter['negative'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "mixed") {
            // displayPieLegendsSenti(3, 'mixed', bucket.doc_count);
            graphData.push({
                key: "Mixed",
                color: colorMapPieTwitter['mixed'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        }


    }
    // flexiSliderCall();
     this.pieGraphSentimentsGenerate(graphData);
},

 pieGraphSentimentsGenerate:function(graphData) {
    if(graphData.length < 1){
      $('.dataContentChartHomeSocialSenti').html('No data available')
    }
    // $(".dataContentAllPie").empty();
    var piechart = AmCharts.makeChart("chartHomeSocialSenti", {
        "type": "pie",
        "theme": "light",
        "autoMargins": true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        
        "dataProvider": graphData,
        "titleField":"key",
        "valueField": "value",
        "startDuration":0,
         "responsive": {
            "enabled":true
         },
         "legend":{
           "autoMargins":false,
           "marginLeft":20,
            "enabled": true,
            "spacing":5,
            "markerType":"circle",
           
            "horizontalGap" :5,
            
         },
          "labelsEnabled":false,
        //"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      
        "colorField": "color",
        "innerRadius": "60%",
       
        "angle": 15,
        "export": {
          "enabled": true,
          "fileName":"social media sentiments",
          "menu": [
            {
              "class": "export-main",
              "menu": [
                {
                  "label": "Download as image",
                  "menu": [
                    "PNG",
                    "JPG",
                    "SVG",
                    "PDF"
                  ]
                },
                {
                  "label": "Download data",
                  "menu": [
                    "CSV",
                    "XLSX",
                    "JSON"
                  ]
                },
                {
                  "label": "Print",
                  "menu": [
                    "PRINT"
                  ]
                }
              ]
            }
          ]
        }
    });

 
homeCharts.push(piechart);

},

 createHashTagTable:function (response) {
                
         var tagWords = [];
         var buckets = response.aggregations.tags.buckets;
        
         for (var bucketIndex in buckets) {
             bucket = buckets[bucketIndex];
            
             tagWords.push({
                 key: bucket.key,
                 count: bucket.doc_count,
                
             });
              this.keyHashTags.push({
                 Tag: bucket.key,
                 Count: bucket.doc_count,
                
             });
            
         }
    if(tagWords.length < 1){
      $('.dataContentHashTagTable').html('No data available')
    } 

     $("#hashTagTable").html($("#hashTagTable1").render(tagWords));
     
     },

   CreateReport:function() {
    var self=this;
  var imageData=foamtreeHome.get("imageData");
  var pdf_images = 0;
  var pdf_layout = this.layoutHome; // loaded from another JS file
  for (var i = 0; i <homeCharts.length; i++ ) {
     var chart =homeCharts[ i ];
    // Capture current state of the chart
    chart.export.capture( {}, function() {

      // Export to PNG
      this.toJPG( {
        // pretend to be lossless
         
        // Add image to the layout reference
      }, function( data ) {
        pdf_images++;
          console.log("pdf_image",pdf_images);
        pdf_layout.images[ "image_" + pdf_images ] = data;

        // Once all has been processed create the PDF
        if ( pdf_images == homeCharts.length ) {



              pdf_layout.setBackupPoint();
              
              var foamTreeData=foamtreeHome.get("dataObject");
                 if(foamTreeData.groups.length>0){
                    pdf_layout.content.push(
                            {
                             text: "Comb Graph",
                             color:"#3A907F",
                              style: "subheader"
                         }, {
                             image:imageData, // reference to the image mapping below
                             fit: [523.28, 769.89],
                            margin: [0, 20, 20, 20],
                             style: 'safetyDistance',
                             pageBreak:"after" // fits the image to those dimensions (A4)
                         });

                    }
              pdf_layout.content.push(
                                 //    {
                                 //     text: "Comb Graph",
                                 //     color:"#3A907F",
                                 //      style: "subheader"
                                 // }, {
                                 //     image:imageData, // reference to the image mapping below
                                 //     fit: [523.28, 769.89],
                                 //     margin: [0, 20, 20, 20],
                                 //     style: 'safetyDistance',
                                 //      pageBreak:"after" // fits the image to those dimensions (A4)
                                 // },
                                 {
                                     text: "Key Hash Tags",
                                      color:"#3A907F",
                                      style: "subheader",
                                      margin: [0, 0, 0, 20]
                                 },


                                {
                               
                                style: 'safetyDistance',

                                table: {
                                    widths:[250,250],
                                    headerRows: 1,
                                 
                                    

                                    body: buildTableBody(self.keyHashTags,['Tag','Count'])
                                }

                              }
                            );

            
          // Save as single PDF and offer as download
          this.toPDF( pdf_layout, function( data ) {
            this.download( data, "application/pdf", "dashboard report.pdf" );
            $('.button-download').removeClass('global-report-loader');
            $('.button-download').html('<i class="icon-download"></i>');
          } );
          pdf_layout.rewind();
        }
      } );
    } );
  }
}





};

