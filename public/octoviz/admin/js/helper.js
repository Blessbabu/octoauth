var foamtreeId ={
    listening: "foamtreeListening",
	home:"foamtreeHome",
    facebook:"foamtreeFb"
	
};
var foamtreeIndexId ={
	twitterIndex: "octobuzz",
	twitterIndexType: "twitter"
}
//var duration ="1w";
$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
};

var titilizeName = function(name){
			name = name.substring(0,1).toLocaleUpperCase() + name.substring(1);
			return name;
}

function applyScoring(esClient,date,socialMedia){
    esClient.addNotLookupTermsMatch("_id", "newsentiments", "relevancy", user, "hide." + socialMedia +".raw");
    // esClient.addScoringFunction({
    //     "filter": {
    //         "terms": {
    //             "_id": {
    //                 "index": "newsentiments",
    //                 "type": "relevancy",
    //                 "id": user,
    //                 "path": "relevant." + socialMedia + ".raw",
    //                 "cache": false
    //             }
    //         }
    //     },
    //     "weight": 1000
    // });
    // esClient.addScoringFunction({
    //     "filter": {
    //         "terms": {
    //             "_id": {
    //                 "index": "newsentiments",
    //                 "type": "relevancy",
    //                 "id": user,
    //                 "path": "irrelevant."  + socialMedia + ".raw",
    //                 "cache": false
    //             }
    //         }
    //     },
    //     "weight": -1000
    // });

    // esClient.addScoringFunction({
    //     "field_value_factor": {
    //         "field": date,
    //         "factor": .00001,
    //           "missing": 0
    //     }
    // });


}

function resetTimeSelector(){
}

function alertError(text) {
    $.notify(text,{ 
  className:'error',
  autoHide: false,
  clickToHide: true
});
}

function alertWarning(text) {
    $.notify(text, "warn");
}

function alertInfo(text) {
    $.notify(text,"success");
}


function getFromString(){
    var timeString =  $(".timeFilter.active").attr("duration");
  
    if(isNull(timeString)){
        alert("Bad time string");
    }
    return "now-" + timeString;
}

var jsonify=function(o){
    var seen=[];
    var jso=JSON.stringify(o, function(k,v){
        if (typeof v =='object') {
            if ( !seen.indexOf(v) ) { return '__cycle__'; }
            seen.push(v);
        } return v;
    });
    return jso;
};
function isEmpty(text){
	return isWhitespaceOrEmpty(text);
}



function isEmpty(text){
	return isWhitespaceOrEmpty(text);
}

function isWhitespaceOrEmpty(text) {
   return !isNull(text) && !/[^\s]/.test(text);
}


var getKeywords = function(tag){
	console.log('global config in media',globalConfig);
var keywords = [];
if(!isNull(globalConfig._source[tag]) && !isNull(globalConfig._source[tag].media)){
    keywords = globalConfig._source[tag].media.keywordsMedia;
    if(isNull(keywords)){
        keywords = [];
    }
}
return keywords;
}   

	
function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

function getFacebookKeywords(tag){
    if(!isNull(globalConfig._source[tag]["faceBook"]) 
            && !isNull(globalConfig._source[tag]["faceBook"]["pageNames"]) 
            && globalConfig._source[tag]["faceBook"]["pageNames"].length != 0){
        var ids = [];
        for(var tagIndex in globalConfig._source[tag]["faceBook"]["pageNames"]){
            ids.push(globalConfig._source[tag]["faceBook"]["pageNames"][tagIndex]);
        }
		console.log('page ids are',ids)
        return ids;
    }
    else{
        return "NA";
    }
}

function getFaceBookKeywordsGroups(tag){
	if(!isNull(globalConfig._source[tag]["faceBook"]) 
            && !isNull(globalConfig._source[tag]["faceBook"]["groupNames"]) 
            && globalConfig._source[tag]["faceBook"]["groupNames"].length != 0){
        var ids = [];
        for(var tagIndex in globalConfig._source[tag]["faceBook"]["groupNames"]){
            ids.push(globalConfig._source[tag]["faceBook"]["groupNames"][tagIndex]);
        }
        return ids;
    }
    else{
        return "NA";
    }
}


var convertDate = function(d){

    console.log("D is " ,d);
    var currentDate = new Date();
	if(isNull(d)){
	
	}
	else if(d =="now-1M"){
        currentDate.setMonth(currentDate.getMonth()-1);
	}
	else if(d =="now-24h"){
        currentDate.setDate(currentDate.getDate()-1);
	}
	else if(d =="now-1w"){
        currentDate.setDate(currentDate.getDate()-7);
	}
	else if(d =="now-1h"){
        currentDate.setHours(currentDate.getHours() - 1);
	}
    	else if(d =="now-4M"){
        	currentDate.setMonth(currentDate.getMonth()-4);
    	}
    	else if(d =="now-1y"){
        	currentDate.setMonth(currentDate.getMonth()-12);
    	}
	else {
		var date = d.replace("now-","");
        console.log("given date..",date);
		currentDate = moment(date, "YYYY-MM-DDTHH:mm:ss.SSSZ ").toDate();
		console.log("DATE is ",moment(date, "YYYY-MM-DDTHH:mm:ss.SSSZ").toDate());
	}
    
	console.log('current date',currentDate);
   return currentDate;
};


var graphClearFunction = function(){
        $('#chartID').empty();
		$(".dataContent").html("No data Available");
};

var diffIndex =[];

var initialTagValueGen = function(response){

    console.log("initial tag value",response);

	$('.tagDropdown').empty();
//	$('.tagAdd-btn').show();
	var comparisonArray =["twitter", "faceBook", "youtube", "instagram", "media", "tags" ,"_id","tweetMan"];
	var keys = [];
	var count = 0;
	for(var k in response._source) keys.push(k);
	diffIndex = arr_diffIndex(keys,comparisonArray);
	diffIndex = jQuery.grep(diffIndex, function(value) {
		  return value != "facebook";
		});
	
		for(count; count < diffIndex.length ; count++){
			if(count == 0){
			$('.tagDropdown').append('<li><a role="menuitem" tabindex="-1" id="listId'+diffIndex[count]+'" class="liTags active" tagnamepick="'+diffIndex[count]+'" >'+diffIndex[count]+'</a><span class="editQuery  tag-edit" editTagName="'+diffIndex[count]+'" data-target="#tagModel" data-toggle="modal"><i class="ion-edit"></i></span></li>');
			}
			else{
				$('.tagDropdown').append('<li ><a role="menuitem" tabindex="-1" id="listId'+diffIndex[count]+'" class="liTags" tagnamepick="'+diffIndex[count]+'" role="presentation">'+diffIndex[count]+'</a><span class="editQuery tag-edit" editTagName="'+diffIndex[count]+'" data-target="#tagModel" data-toggle="modal"><i class="ion-edit"></i></span></li>');
			}

	}
	tagValuesArray = diffIndex;

	$('.tagsAddition').html('<i class="ion-pricetag"></i>'+diffIndex[0]+'<i class="ion-ios-arrow-down"></i>');
	return diffIndex[0];
}

//$('body').on('click','.tag-edit')

function arr_diffIndex(a1, a2)
{
  var a=[], diff=[];
  for(var i=0;i<a1.length;i++)
    a[a1[i]]=true;
  for(var i=0;i<a2.length;i++)
    if(a[a2[i]]) delete a[a2[i]];
  for(var k in a)
    diff.push(k);
  return diff;
}

function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }


function preloadImages(srcs, imgs,callBackParameters, callback) {
    var img;
    var remaining = srcs.length;
for (var i = 0; i < srcs.length; i++) {
        img = new Image();
        img.onload = function() {
            --remaining;
            if (remaining <= 0) {
		console.log("Completed loading",new Date());
                callback(callBackParameters);
            }
        };
	img.onerror = function() {
	     console.log("Error loading  this" , this);
            --remaining;
            if (remaining <= 0) {
                console.log("Completed loading",new Date());
                callback(callBackParameters);
            }
        };
        img.src = srcs[i];
        imgs.push(img);
    }
}






var imageArrayGen = function (count, response, callback) {
	var imageArray = [];
	if (count == null) {
		var totalAggs = response.aggregations.filterRetweets.trendingUser.buckets;
	} else {
		var totalAggs = response[count].aggregations.filterRetweets.trendingUser.buckets;
	}
	var index = 0;
	for (index in totalAggs) {
		if (index == 0)
			continue;
		var imageUrl = totalAggs[index].details.hits.hits[0]._source.user.profile_image_url.replace(/normal/, "400x400");
		imageArray.push(totalAggs[index].details.hits.hits[0]._source.user.profile_image_url.replace(/normal/, "400x400"));
	}
	preloadImages(imageArray, [], response, callback);
}



var tabSwitchFunctionModal = function() {
    $(".bhoechie-tab-menu>.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $(".bhoechie-tab>.bhoechie-tab-content").removeClass("active");
        $(".bhoechie-tab>.bhoechie-tab-content").eq(index).addClass("active");
    });
};

$.views.helpers({
    getFields: function( object ) {
		alert(object);
		console.log(object);
        var key, value,
            fieldsArray = [];
        for ( key in object ) {
            if ( object.hasOwnProperty( key )) {
                value = object[ key ];
                // For each property/field add an object to the array, with key and value
                fieldsArray.push({
                    key: key,
                    value: value
                });
            }
        }
        // Return the array, to be rendered using {{for ~fields(object)}}
        return fieldsArray;
    }
});

var utcToDateFormat = function(d){
	var date = d.substring(0,10);
	return date;
}

var utcToDateFormatReverse = function(d){
    var date = d.substring(0,10);
    var pieces = date.split('-');
    pieces.reverse();
    var reversed = pieces.join('-');
    return reversed;
}


var isoCountries = {

            // 'Australia' : 'AUS',
            // 'India' : 'IND',
            // 'China':'CHN'

' Aruba ':'ABW',
'Afghanistan': 'AFG',
'Angola' : 'AGO',
'Anguilla' : 'AIA' ,
'Åland Islands' :'ALA',
'Albania': 'ALB',
'Andorra' :'AND',
'United Arab Emirates ':'ARE',
'Argentina' :'ARG',
'Armenia'  :'ARM',
' American Samoa':'ASM',
'Antarctica'  :'ATA',
'French Southern Territories' :'ATF', 
'Antigua and Barbuda' :'ATG',
'Australia': 'AUS',
'Austria' :  'AUT',
'Azerbaijan' :'AZE',
'Burundi' : 'BDI ',
'Belgium'  :'BEL',
'Benin' :'BEN ',
'Bonaire, Sint Eustatius and Saba' :'BES',
' Burkina Faso ':'BFA',
'Bangladesh' :'BGD',
'Bulgaria'  :'BGR',
'Bahrain' :'BHR ',
'Bahamas' :'BHS',
'Bosniaand Herzegovina' :'BIH',
'Saint Barthélemy' :'BLM ',
'Belarus': 'BLR',
'Belize' :'BLZ',
'Bermuda' : 'BMU',
' Bolivia' :'BOL',
'Brazil' : 'BRA',
'Barbados' : 'BRB',
'Brunei Darussalam':'BRN',
'Bhutan' :'BTN',
' Bouvet Island':'BVT',
' Botswana ':'BWA',
'Central African Republic':'CAF',
'Canada' :'CAN',
'Cocos (Keeling) Islands' :'CCK',
'Switzerland'  :'CHE',
'Chile': 'CHL',
'China' :'CHN',

'Cameroon'  :'CMR',

'Congo' :'COG',
' Cook Islands' :'COK',
'Colombia' :'COL',
' Comoros ' :'COM',
'Cabo Verde':'CPV',
'Costa Rica' :'CRI',
'Cuba ':'CUB',
' Curaçao' :'CUW',
' Christmas Island' :'CXR',
'Cayman Islands ' :'CYM',
'Cyprus' :'CYP', 
'Czech Republic':'CZE',             
'Germany':'DEU', 

'Denmark':'DNK',
'Dominican Republic':'DOM',
'Algeria' :'DZA',
'Ecuador':'ECU',
'Egypt':'EGY',
'Eritrea' :'ERI',
'Western Sahara':'ESH',
'Spain' :'ESP',
'Estonia'  :'EST',
'Ethiopia' :'ETH',
'Finland': 'FIN', 
'Fiji': 'FJI', 
'France': 'FRA', 
'Faroe Islands': 'FRO',

'Gabon' :'GAB', 
'United Kingdom':'GBR', 
'Georgia' :'GEO', 
'Guernsey' :'GGY', 
'Ghana': 'GHA', 
'Gibraltar' :'GIB',
'Guinea' :'GIN', 
'Guadeloupe' :'GLP', 
'Gambia' :'GMB', 
' Guinea-Bissau': 'GN',
' Equatorial Guinea':'GNQ',
'Greece':'GRC',
'Grenada' :'GRD ',
'Greenland': 'GRL',
'Guatemala': 'GTM ',
'French Guiana' :'GUF', 
'Guam': 'GUM', 
'Guyana' :'GUY', 
'Hong Kong':'HKG', 

'Honduras' :'HND', 
'Croatia' :'HRV',
'Haiti' :'HTI', 
'Hungary' :'HUN', 
'Indonesia' :'IDN', 
'Isle of Man' :'IMN', 
'India' :'IND', 


'Ireland' : 'IRL',
'Iran' :'IRN', 
'Iraq' :'IRQ',
'Iceland': 'ISL',
'Israel' :'ISR' ,
'Italy' :'ITA', 
'Jamaica' :'JAM', 
'Jersey' : 'JEY', 
'Jordan' : 'JOR', 
'Japan' :'JPN', 
'Kazakhstan' :'KAZ' ,
'Kenya': 'KEN', 
'Kyrgyzstan' :'KGZ ',
'Cambodia' :'KHM', 
'Kiribati': 'KIR',

'Korea' :'KOR',
'Kuwait':'KWT' ,

'Lebanon' :'LBN',
'Liberia': 'LBR', 
'Libya': 'LBY' ,
'Saint Lucia':'LCA', 
'Liechtenstein' :'LIE', 

'Lesotho': 'LSO', 
'Lithuania' :'LTU', 
'Luxemborg' :'LUX', 
'Latvia' :'LVA', 
'Macao' :'MAC', 
'Saint Martin (French part)' :'MAF', 
'Morocco': 'MAR', 
'Monaco'  :'MCO', 
'Moldova'  :'MDA', 
'Madagascar' :'MDG', 
'Maldives' :'MDV', 
'Mexico' :'MEX', 


'Mali':'MLI', 
'Malta' :'MLT', 
'Myanmar'  :'MMR', 
'Montenegro' : 'MNE', 
'Mongolia' : 'MNG', 
'Northern Mariana Islands' : 'MNP', 
'Mozambique': 'MOZ', 
'Mauritania'  : 'MRT', 
'Montserrat' : 'MSR', 
'Martinique' : 'MTQ', 
'Mauritius' : 'MUS', 
'Malawi' :'MWI', 
'Malaysia'  :'MYS', 
'Mayotte': 'MYT', 
'Namibia' :'NAM', 
'New Caledonia': 'NCL', 
'Niger' :'NER', 
'Norfolk Island ':'NFK', 
'Nigeria' : 'NGA', 
'Nicaragua'  :'NIC ',
'Niue' : 'NIU',


'Netherlands':'NLD', 
'Norway'  :'NOR', 
'Nepal':  'NPL', 
'Nauru' :'NRU', 
'New Zealand' :'NZL', 
'Oman' :'OMN', 
'Pakistan' :'PAK', 
'Panama': 'PAN', 
'Pitcairn' :'PCN', 
'Peru' :'PER' ,
'Philippines': 'PHL', 
'Palau' :'PLW', 
'Papua New Guinea'  :'PNG', 
'Poland'  :'POL', 
'Puerto Rico' :'PRI' ,
'South Korea': 'PRK', 
'Portugal': 'PRT', 
'Paraguay' :'PRY', 
'Palestine' :'PSE', 
'French Polynesia ':'PYF', 
'Qatar' :'QAT', 

'Romania' :'ROU', 
'Russia':' RUS', 
'Rwanda' :'RWA', 
'Saudi Arabia':'SAU', 
'Sudan'  :'SDN', 
'Senegal' :'SEN', 
'Singapore' :'SGP', 

' Solomon Islands ' :'SLB', 
'Sierra Leone': 'SLE', 
'El Salvador' :'SLV',
'San Marino' :'SMR', 
'Somalia' :'SOM', 

'Serbia'  :'SRB', 
'South Sudan'  :'SSD', 
'Sao Tome and Principe' :'STP', 
'Suriname' :'SUR', 
'Slovakia': 'SVK', 
'Slovenia' :'SVN', 
'Sweden' :'SWE', 
'Swaziland'  :'SWZ', 
'Sri Lanka':'LKA', 
'SriLanka':'LKA',

'Syrian Arab Republic':'SYR', 

'Togo' :'TGO', 
'Thailand' :'THA', 
'Tajikistan':' TJK', 
'Tokelau' :'TKL', 
'Turkmenistan': 'TKM', 
'Timor-Leste' :'TLS', 
'Tonga' :'TON', 
'Trinidad and Tobago': 'TTO', 
'Tunisia' :'TUN', 
'Turkey'   :'TUR',
'Tuvalu'  : 'TUV', 
'Taiwan': 'TWN', 
' Tanzania' :'TZA', 
'Uganda'   :'UGA', 

'Ukranie':'UKR',

'Uruguay'  :'URY', 
'US': 'USA',
'Uzbekistan' :' UZB', 
'United Arab Emirates':'ARE',
'Holy See (Vatican City State)' :'VAT', 

'Venezuela' :'VEN', 
'Virgin Islands' :'VGB', 
'Virgin Islands' :'VIR', 
'Vietnam' :'VNM', 
'Vanuatu' : 'VUT', 

'Samoa' : 'WSM', 

'Yemen': 'YEM', 
'South Africa': 'ZAF', 
'Zambia' :'ZMB',
'Zimbabwe' : 'ZWE'
};


isoTwoDigitCountries={
BD: "BGD",
BE: "BEL",
BF: "BFA",
BG: "BGR",
BA: "BIH",
BB: "BRB",
WF: "WLF",
BL: "BLM",
BM: "BMU",
BN: "BRN",
BO: "BOL",
BH: "BHR",
BI: "BDI",
BJ: "BEN",
BT: "BTN",
JM: "JAM",
BV: "BVT",
BW: "BWA",
WS: "WSM",
BQ: "BES",
BR: "BRA",
BS: "BHS",
JE: "JEY",
BY: "BLR",
BZ: "BLZ",
RU: "RUS",
RW: "RWA",
RS: "SRB",
TL: "TLS",
RE: "REU",
TM: "TKM",
TJ: "TJK",
RO: "ROU",
TK: "TKL",
GW: "GNB",
GU: "GUM",
GT: "GTM",
GS: "SGS",
GR: "GRC",
GQ: "GNQ",
GP: "GLP",
JP: "JPN",
GY: "GUY",
GG: "GGY",
GF: "GUF",
GE: "GEO",
GD: "GRD",
GB: "GBR",
GA: "GAB",
SV: "SLV",
GN: "GIN",
GM: "GMB",
GL: "GRL",
GI: "GIB",
GH: "GHA",
OM: "OMN",
TN: "TUN",
JO: "JOR",
HR: "HRV",
HT: "HTI",
HU: "HUN",
HK: "HKG",
HN: "HND",
HM: "HMD",
VE: "VEN",
PR: "PRI",
PS: "PSE",
PW: "PLW",
PT: "PRT",
SJ: "SJM",
PY: "PRY",
IQ: "IRQ",
PA: "PAN",
PF: "PYF",
PG: "PNG",
PE: "PER",
PK: "PAK",
PH: "PHL",
PN: "PCN",
PL: "POL",
PM: "SPM",
ZM: "ZMB",
EH: "ESH",
EE: "EST",
EG: "EGY",
ZA: "ZAF",
EC: "ECU",
IT: "ITA",
VN: "VNM",
SB: "SLB",
ET: "ETH",
SO: "SOM",
ZW: "ZWE",
SA: "SAU",
ES: "ESP",
ER: "ERI",
ME: "MNE",
MD: "MDA",
MG: "MDG",
MF: "MAF",
MA: "MAR",
MC: "MCO",
UZ: "UZB",
MM: "MMR",
ML: "MLI",
MO: "MAC",
MN: "MNG",
MH: "MHL",
MK: "MKD",
MU: "MUS",
MT: "MLT",
MW: "MWI",
MV: "MDV",
MQ: "MTQ",
MP: "MNP",
MS: "MSR",
MR: "MRT",
IM: "IMN",
UG: "UGA",
TZ: "TZA",
MY: "MYS",
MX: "MEX",
IL: "ISR",
FR: "FRA",
IO: "IOT",
SH: "SHN",
FI: "FIN",
FJ: "FJI",
FK: "FLK",
FM: "FSM",
FO: "FRO",
NI: "NIC",
NL: "NLD",
NO: "NOR",
NA: "NAM",
VU: "VUT",
NC: "NCL",
NE: "NER",
NF: "NFK",
NG: "NGA",
NZ: "NZL",
NP: "NPL",
NR: "NRU",
NU: "NIU",
CK: "COK",
XK: "XKX",
CI: "CIV",
CH: "CHE",
CO: "COL",
CN: "CHN",
CM: "CMR",
CL: "CHL",
CC: "CCK",
CA: "CAN",
CG: "COG",
CF: "CAF",
CD: "COD",
CZ: "CZE",
CY: "CYP",
CX: "CXR",
CR: "CRI",
CW: "CUW",
CV: "CPV",
CU: "CUB",
SZ: "SWZ",
SY: "SYR",
SX: "SXM",
KG: "KGZ",
KE: "KEN",
SS: "SSD",
SR: "SUR",
KI: "KIR",
KH: "KHM",
KN: "KNA",
KM: "COM",
ST: "STP",
SK: "SVK",
KR: "KOR",
SI: "SVN",
KP: "PRK",
KW: "KWT",
SN: "SEN",
SM: "SMR",
SL: "SLE",
SC: "SYC",
KZ: "KAZ",
KY: "CYM",
SG: "SGP",
SE: "SWE",
SD: "SDN",
DO: "DOM",
DM: "DMA",
DJ: "DJI",
DK: "DNK",
VG: "VGB",
DE: "DEU",
YE: "YEM",
DZ: "DZA",
US: "USA",
UY: "URY",
YT: "MYT",
UM: "UMI",
LB: "LBN",
LC: "LCA",
LA: "LAO",
TV: "TUV",
TW: "TWN",
TT: "TTO",
TR: "TUR",
LK: "LKA",
LI: "LIE",
LV: "LVA",
TO: "TON",
LT: "LTU",
LU: "LUX",
LR: "LBR",
LS: "LSO",
TH: "THA",
TF: "ATF",
TG: "TGO",
TD: "TCD",
TC: "TCA",
LY: "LBY",
VA: "VAT",
VC: "VCT",
AE: "ARE",
AD: "AND",
AG: "ATG",
AF: "AFG",
AI: "AIA",
VI: "VIR",
IS: "ISL",
IR: "IRN",
AM: "ARM",
AL: "ALB",
AO: "AGO",
AQ: "ATA",
AS: "ASM",
AR: "ARG",
AU: "AUS",
AT: "AUT",
AW: "ABW",
IN: "IND",
AX: "ALA",
AZ: "AZE",
IE: "IRL",
ID: "IDN",
UA: "UKR",
QA: "QAT",
MZ: "MOZ"

}


isoCountrytoTwoLetter={
"Afghanistan": "AF",
"Albania": "AL",
"Algeria" :"DZ",
"American Samoa":  "AS",
"Andorra": "AD",
"Angola"  :"AO",
"Anguilla"   : "AI",
"Antarctica" : "AQ",
"Antigua & Barbuda":"AG",
"Argentina" :  "AR",
"Armenia": "AM",
"Australia":   "AU",
"Austria": "AT",
"Azerbaijan" : "AZ",
"Bahama" : "BS",
"Bahrain" :"BH",
"Bangladesh":  "BD",
"Barbados"  :  "BB",
"Belarus": "BY",
"Belgium" :"BE",
"Belize" : "BZ",
"Benin"  : "BJ",
"Bermuda" :"BM",
"Bhutan"  :"BT",
"Bolivia" :"BO",
"Bosnia and Herzegovina" : "BA",
"Botswana"   : "BW",
"Bouvet Island ":"BV",
"Brazil" :"BR",
"Bulgaria" : "BG",
"Burkina Faso " :"BF",
"Burma"  : "BU",
"Burundi": "BI",
"Cambodia"  :  "KH",
"Cameroon"  :  "CM",
"Canada" : "CA",
"Cape Verde ": "CV",
"Cayman Islands" : "KY",
"Central African Republic": "CF",
"Chad" : "TD",
"Chile": "CL",
"China": "CN",

"Colombia":    "CO",
"Comoros" :"KM",
"Congo"   :"CG",
"Cook Iislands" :  "CK",
"Costa Rica" : "CR",
"Côte D'ivoire": "CI",
"Croatia": "HR",
"Cuba "  : "CU",
"Cyprus"  :"CY",
 "Czech Republic" :"CZ",
"Czechoslovakia" :  "CS",
"Democratic Yemen": "YD",
"Denmark" :"DK",
"Djibouti"  :  "DJ",
"Dominica"   : "DM",
"Dominican Republic":  "DO",
"East Timor"  :"TP",
"Ecuador": "EC",
"Egypt"  : "EG",
"El Salvador": "SV",
"Equatorial Guinea"  : "GQ",
"Eritrea": "ER",
"Estonia": "EE",
"Ethiopia"  :  "ET",
"Falkland Islands (Malvinas)" :"FK",
"Faroe Islands " : "FO",
"Fiji" :   "FJ",
"Finland": "FI",
"France" : "FR",
"France, Metropolitan":    "FX",
"French Guiana"  : "GF",
"French Polynesia"  :  "PF",
"French Southern Territories": "TF",
"Gabon"  : "GA",
"Gambia" : "GM",
"Georgia" :"GE",
"German Democratic Republic (no longer exists) " : "DD",
"Germany": "DE",
"Ghana" : "GH",
"Gibraltar"  : "GI",
"Greece" : "GR",
"Greenland" :  "GL",
"Grenada": "GD",
"Guadeloupe" : "GP",
"Guam" :   "GU",
"Guatemala" :  "GT",
"Guinea" : "GN",
"Guinea-Bissau"  : "GW",
"Guyana" : "GY",
"Haiti" :  "HT",
"Heard & McDonald Islands " :  "HM",
"Honduras"  :  "HN",
"Hong Kong " : "HK",
"Hungary": "HU",
"Iceland" :"IS",
"India" :  "IN",
"Indonesia"  : "ID",
"Iraq"  :  "IQ",
"Ireland": "IE",
"Islamic Republic of Iran" :   "IR",
"Israel":  "IL",
"Italy" :  "IT",
"Jamaica": "JM",
"Japan" :  "JP",
"Jordan" : "JO",
"Kazakhstan" : "KZ",
"Kenya" :  "KE",
"Kiribati":    "KI",
"South Korea"  :"KP",
"North Korea":  "KR",
"Kuwait" : "KW",
"Kyrgyzstan" : "KG",
"Lao People's Democratic Republic" :   "LA",
"Latvia" : "LV",
"Lebanon" :"LB",
"Lesotho" :"LS",
"Liberia" :"LR",
"Libyan Arab Jamahiriya":  "LY",
"Liechtenstein" :  "LI",
"Lithuania" :  "LT",
"Luxembourg" : "LU",
"Macau"  : "MO",
"Madagascar":  "MG",
"Malawi" : "MW",
"Malaysia"  :  "MY",
"Maldives" :   "MV",
"Mali" :   "ML",
"Malta" :  "MT",
"Marshall Islands  ":  "MH",
"Martinique" : "MQ",
"Mauritania" : "MR",
"Mauritius":   "MU",
"Mayotte": "YT",
"Mexico" : "MX",
"Micronesia" : "FM",
"Moldova, Republic of " :  "MD",
"Monaco" : "MC",
"Mongolia" :   "MN",
"Monserrat" :  "MS",
"Morocco" :"MA",
"Mozambique" : "MZ",
"Myanmar": "MM",
"Nambia" : "NA",
"Nauru"  : "NR",
"Nepal"  : "NP",
"Netherlands": "NL",
"Netherlands Antilles" :   "AN",
"Neutral Zone (no longer exists)": "NT",
"New Caledonia"  : "NC",
"New Zealand": "NZ",
"Nicaragua" :  "NI",
"Niger":   "NE",
"Nigeria": "NG",
"Niue" :   "NU",
"Norfolk Island":  "NF",
"Northern Mariana Islands"  :  "MP",
"Norway":  "NO",
"Oman"   : "OM",
"Pakistan" :   "PK",
"Palau" :  "PW",
"Panama" : "PA",
"Papua New Guinea ":   "PG",
"Paraguay" :   "PY",
"Peru"  :  "PE",
"Philippines": "PH",
"Pitcairn" :   "PN",
"Poland" : "PL",
"Portugal":    "PT",
"Puerto Rico" :"PR",
"Qatar" :  "QA",
"Réunion" :"RE",
"Romania": "RO",
"Russia" : "RU",
"Rwanda":  "RW",
"Saint Lucia": "LC",
"Samoa"  : "WS",
"San Marino ": "SM",
"Saudi Arabia" :   "SA",
"Senegal" :"SN",
"Seychelles" : "SC",
"Sierra Leone"  :  "SL",
"Singapore" :  "SG",
"Slovakia" :   "SK",
"Slovenia"  :  "SI",
"Solomon Islands": "SB",
"Somalia": "SO",
"South Africa" :   "ZA",
"South Georgia and the South Sandwich Islands" :   "GS",
"Spain"  : "ES",
"SriLanka" :  "LK",
"St. Helena" : "SH",
"St. Kitts and Nevis": "KN",
"St. Pierre & Miquelon"  : "PM",
"St. Vincent & the Grenadines" :   "VC",
"Sudan" :  "SD",
"Suriname" :   "SR",
"Svalbard & Jan Mayen Islands" :   "SJ",
"Swaziland" :  "SZ",
"Sweden" : "SE",
"Switzerland": "CH",
"Syrian Arab Republic" :   "SY",
"Taiwan, Province of China " : "TW",
"Tajikistan" : "TJ",
"Tanzania, United Republic of"  :  "TZ",
"Thailand"  :  "TH",
"Togo" :   "TG",
"Tokelau" :"TK",
"Tonga"  : "TO",
"Trinidad & Tobago" :  "TT",
"Tunisia": "TN",
"Turkey" : "TR",
"Turkmenistan" :   "TM",
"Turks & Caicos Islands":  "TC",
"Tuvalu" : "TV",
"Uganda" : "UG",
"Ukraine": "UA",
"Union of Soviet Socialist Republics (no longer exi":  "SU",
"United Arab Emirates" :   "AE",
"United Kingdom": "GB",
"United States Minor Outlying Islands " :  "UM",
"United States of America" :   "US",
"US" :   "US",
"United States Virgin Islands" :  "VI",

"Uruguay": "UY",
"Uzbekistan" : "UZ",
"Vanuatu": "VU",
"Vatican"  : "VA",
"Venezuela": "VE",
"Vietnam"  : "VN",
"Wallis & Futuna Islands": "WF",
"Western Sahara":  "EH",
"Yemen"  : "YE",
"Yugoslavia" : "YU",
"Zaire" :  "ZR",
"Zambia" : "ZM",
"Zimbabwe":  "ZW",
}


var ConvertToCountryMedia =function(CountryData){
  
  console.log("country media",CountryData);
 var dataBucketMedia=[];
 var country;
 var count=0;
 var fillValue;

var colors=["#BAF0F4","#AAE8ED","#A0E0E5","#96DBE0","#92D6DB","#89D1D6","#81CED3","#79C8CE","#72C6CC","#69C0C6","#60BBC1","#5BB8BF","#55B3BA","#4EB0B7","#49ABB2","#41A6AD","#3FA3AA","#3AA1A8","#359BA3","#3099A0","#228991"];
var min = Infinity;
var max =0;

for(i=0;i<CountryData.length;i++){
  if (CountryData[i].count<min) {
      min = CountryData[i].count;
    }
   if (CountryData[i].count>max) {
      max=CountryData[i].count;
    }
}
var stepSize =20;
var stepVal=(max-min)/stepSize;
var stepArr=[];
for(i=0;i<=stepSize;i++){
   stepArr.push(min+(stepVal*(i)));
}


 for(i=0;i<CountryData.length;i++){
      var countryName=CountryData[i].source;
          countryName= countryName[0].toUpperCase() + countryName.slice(1);
    if(isoCountrytoTwoLetter.hasOwnProperty(countryName)) {
       var key=isoCountrytoTwoLetter[countryName];

        
         count=CountryData[i].count;
        for(j=0;j<=stepSize;j++)
          { 
            if((count>=stepArr[j]) && (count<=stepArr[j+1]))
                 fillValue=colors[j];
          }

       
         dataBucketMedia.push({id:key,color:fillValue,value:count});
        
    }

    else {

        console.log("not in",CountryData[i].source);
    }
    
}
console.log("country value",dataBucketMedia);
  globalObject.createMediaMap(dataBucketMedia);
};

// function ConvertToCountryCode(tdaSource){
// console.log("country code",tdaSource);
//  var data={};
//  var country;
//  var count;
//  var fillValue;

// var colors=["#BAF0F4","#AAE8ED","#A0E0E5","#96DBE0","#92D6DB","#89D1D6","#81CED3","#79C8CE","#72C6CC","#69C0C6","#60BBC1","#5BB8BF","#55B3BA","#4EB0B7","#49ABB2","#41A6AD","#3FA3AA","#3AA1A8","#359BA3","#3099A0","#228991"];
// var min = Infinity;
// var max =0;

// for(i=0;i<tdaSource.length;i++){
//   if (tdaSource[i].count<min) {
//       min = tdaSource[i].count;
//     }
//    if (tdaSource[i].count>max) {
//       max=tdaSource[i].count;
//     }
// }
// var stepSize =20;
// var stepVal=(max-min)/stepSize;
// var stepArr=[];
// for(i=0;i<=stepSize;i++){
//    stepArr.push(min+(stepVal*(i)));
// }


//  for(i=0;i<tdaSource.length;i++){
//         var key=tdaSource[i].key;
//             key=key.toUpperCase();
//     if(isoTwoDigitCountries.hasOwnProperty(key)) {
     
//          country=isoTwoDigitCountries[key];
//          count=tdaSource[i].count;
//         for(j=0;j<=stepSize;j++)
//           { 
//             if((count>=stepArr[j]) && (count<=stepArr[j+1]))
//                 fillValue=colors[j]
//           }

//          data[country]={fillColor:fillValue,countValue:count};
        
//     }

//     else {
//         console.log("not in",tdaSource[i].key);
//     }
    
// }

// //console.log("country value",data);
//  // twitterCountryMap(data);

// }


function ConvertToCountryCode(tdaSource){
  
console.log("country code twitter",tdaSource);
 var data=[];
 var countryColor={};
 var country;
 var count=0;
 var fillValue;

var colors=["#BAF0F4","#AAE8ED","#A0E0E5","#96DBE0","#92D6DB","#89D1D6","#81CED3","#79C8CE","#72C6CC","#69C0C6","#60BBC1","#5BB8BF","#55B3BA","#4EB0B7","#49ABB2","#41A6AD","#3FA3AA","#3AA1A8","#359BA3","#3099A0","#228991"];
var min = Infinity;
var max =0;

for(i=0;i<tdaSource.length;i++){
  if (tdaSource[i].count<min) {
      min = tdaSource[i].count;
    }
   if (tdaSource[i].count>max) {
      max=tdaSource[i].count;
    }
}
var stepSize =20;
var stepVal=(max-min)/stepSize;
var stepArr=[];
for(i=0;i<=stepSize;i++){
   stepArr.push(min+(stepVal*(i)));
}


 for(i=0;i<tdaSource.length;i++){
        var id;
        var key=tdaSource[i].key;
            key=key.toUpperCase();
   
     
         count=tdaSource[i].count;
         
        for(j=0;j<=stepSize;j++)
          { 
            if((count>=stepArr[j]) && (count<=stepArr[j+1]))
                fillValue=colors[j]
          }
	 data.push({id:key,color:fillValue,value:count});
    
 }

console.log("country value",data);
 return data;

}



function CountryMapCircle(tdaSource,count){
  
// console.log("country code twitter",tdaSource+'   '+count);
 var data=[];
 var countryColor={};
 var country;
 // var count=0;
 var fillValue;
var colors=["#16a3ae","#1598a3","#0f919c","#118892","#0c818b","#0d7d86","#07767f","#0a7179","#066a72","#08646c","#035f67","#065a61","#01555c","#055056","#024a50","#03454a","#004247","#023c41","#053a3e","#033034"];
//var colors=["#abfaff","#9dedf2","#8ce3e8","#80d9df","#6fced4","#61c1c7","#54b5bb","#4baeb4","#41a3a9","#35959b","#2c898f","#268187","#217a7f","#1a6f74","#146469","#105a5f","#0c5458","#085155","#034c51","#004347"];
var min = Infinity;
var max =0;

for(i=0;i<tdaSource.length;i++){
  if (tdaSource[i]<min) {
      min = tdaSource[i];
    }
   if (tdaSource[i]>max) {
      max=tdaSource[i];
    }
}
var stepSize =20;
var stepVal=(max-min)/stepSize;
var stepArr=[];
for(i=0;i<=stepSize;i++){
   stepArr.push(min+(stepVal*(i)));
}


 for(i=0;i<tdaSource.length;i++){
     
   
     
         // count=tdaSource[i];
         
        for(j=0;j<=stepSize;j++)
          {  
            // console.log("count...",count);
            if((count>=stepArr[j]) && (count<=stepArr[j+1]))
                fillValue=colors[j]
          }
     // data.push({id:key,color:fillValue,value:count});
    
 }

   return fillValue;

}



var slimScrollCall = function(className,value){
	console.log('calling slim');
        $('.'+className+'').slimScroll({
        height: value,
         railVisible: true,
        alwaysVisible: true
   });
}

var languages=[
  {
    "alpha2": "aa",
    "English": "Afar"
  },
  {
    "alpha2": "ab",
    "English": "Abkhazian"
  },
  {
    "alpha2": "ae",
    "English": "Avestan"
  },
  {
    "alpha2": "af",
    "English": "Afrikaans"
  },
  {
    "alpha2": "ak",
    "English": "Akan"
  },
  {
    "alpha2": "am",
    "English": "Amharic"
  },
  {
    "alpha2": "an",
    "English": "Aragonese"
  },
  {
    "alpha2": "ar",
    "English": "Arabic"
  },
  {
    "alpha2": "as",
    "English": "Assamese"
  },
  {
    "alpha2": "av",
    "English": "Avaric"
  },
  {
    "alpha2": "ay",
    "English": "Aymara"
  },
  {
    "alpha2": "az",
    "English": "Azerbaijani"
  },
  {
    "alpha2": "ba",
    "English": "Bashkir"
  },
  {
    "alpha2": "be",
    "English": "Belarusian"
  },
  {
    "alpha2": "bg",
    "English": "Bulgarian"
  },
  {
    "alpha2": "bh",
    "English": "Bihari"
  },
  {
    "alpha2": "bi",
    "English": "Bislama"
  },
  {
    "alpha2": "bm",
    "English": "Bambara"
  },
  {
    "alpha2": "bn",
    "English": "Bengali"
  },
  {
    "alpha2": "bo",
    "English": "Tibetan"
  },
  {
    "alpha2": "br",
    "English": "Breton"
  },
  {
    "alpha2": "bs",
    "English": "Bosnian"
  },
  {
    "alpha2": "ca",
    "English": "Catalan"
  },
  {
    "alpha2": "ce",
    "English": "Chechen"
  },
  {
    "alpha2": "ch",
    "English": "Chamorro"
  },
  {
    "alpha2": "co",
    "English": "Corsican"
  },
  {
    "alpha2": "cr",
    "English": "Cree"
  },
  {
    "alpha2": "cs",
    "English": "Czech"
  },
  {
    "alpha2": "cu",
    "English": "Church Slavic"
  },
  {
    "alpha2": "cv",
    "English": "Chuvash"
  },
  {
    "alpha2": "cy",
    "English": "Welsh"
  },
  {
    "alpha2": "da",
    "English": "Danish"
  },
  {
    "alpha2": "de",
    "English": "German"
  },
  {
    "alpha2": "dv",
    "English": "Divehi"
  },
  {
    "alpha2": "dz",
    "English": "Dzongkha"
  },
  {
    "alpha2": "ee",
    "English": "Ewe"
  },
  {
    "alpha2": "el",
    "English": "Greek Modern"
  },
  {
    "alpha2": "en",
    "English": "English"
  },
  {
    "alpha2": "eo",
    "English": "Esperanto"
  },
  {
    "alpha2": "es",
    "English": "Spanish"
  },
  {
    "alpha2": "et",
    "English": "Estonian"
  },
  {
    "alpha2": "eu",
    "English": "Basque"
  },
  {
    "alpha2": "fa",
    "English": "Persian"
  },
  {
    "alpha2": "ff",
    "English": "Fulah"
  },
  {
    "alpha2": "fi",
    "English": "Finnish"
  },
  {
    "alpha2": "fj",
    "English": "Fijian"
  },
  {
    "alpha2": "fo",
    "English": "Faroese"
  },
  {
    "alpha2": "fr",
    "English": "French"
  },
  {
    "alpha2": "fy",
    "English": "Western Frisian"
  },
  {
    "alpha2": "ga",
    "English": "Irish"
  },
  {
    "alpha2": "gd",
    "English": "Gaelic"
  },
  {
    "alpha2": "gl",
    "English": "Galician"
  },
  {
    "alpha2": "gn",
    "English": "Guarani"
  },
  {
    "alpha2": "gu",
    "English": "Gujarati"
  },
  {
    "alpha2": "gv",
    "English": "Manx"
  },
  {
    "alpha2": "ha",
    "English": "Hausa"
  },
  {
    "alpha2": "he",
    "English": "Hebrew"
  },
  {
    "alpha2": "hi",
    "English": "Hindi"
  },
  {
    "alpha2": "ho",
    "English": "Hiri Motu"
  },
  {
    "alpha2": "hr",
    "English": "Croatian"
  },
  {
    "alpha2": "ht",
    "English": "Haitian"
  },
  {
    "alpha2": "hu",
    "English": "Hungarian"
  },
  {
    "alpha2": "hy",
    "English": "Armenian"
  },
  {
    "alpha2": "hz",
    "English": "Herero"
  },
  {
    "alpha2": "ia",
    "English": "Interlingua"
  },
  {
    "alpha2": "id",
    "English": "Indonesian"
  },
  {
    "alpha2": "ie",
    "English": "Interlingue"
  },
  {
    "alpha2": "ig",
    "English": "Igbo"
  },
  {
    "alpha2": "ii",
    "English": "Sichuan Yi"
  },
  {
    "alpha2": "ik",
    "English": "Inupiaq"
  },
  {
    "alpha2": "io",
    "English": "Ido"
  },
  {
    "alpha2": "is",
    "English": "Icelandic"
  },
  {
    "alpha2": "it",
    "English": "Italian"
  },
  {
    "alpha2": "iu",
    "English": "Inuktitut"
  },
  {
    "alpha2": "ja",
    "English": "Japanese"
  },
  {
    "alpha2": "jv",
    "English": "Javanese"
  },
  {
    "alpha2": "ka",
    "English": "Georgian"
  },
  {
    "alpha2": "kg",
    "English": "Kongo"
  },
  {
    "alpha2": "ki",
    "English": "Kikuyu"
  },
  {
    "alpha2": "kj",
    "English": "Kuanyama"
  },
  {
    "alpha2": "kk",
    "English": "Kazakh"
  },
  {
    "alpha2": "kl",
    "English": "Kalaallisut"
  },
  {
    "alpha2": "km",
    "English": "Khmer"
  },
  {
    "alpha2": "kn",
    "English": "Kannada"
  },
  {
    "alpha2": "ko",
    "English": "Korean"
  },
  {
    "alpha2": "kr",
    "English": "Kanuri"
  },
  {
    "alpha2": "ks",
    "English": "Kashmiri"
  },
  {
    "alpha2": "ku",
    "English": "Kurdish"
  },
  {
    "alpha2": "kv",
    "English": "Komi"
  },
  {
    "alpha2": "kw",
    "English": "Cornish"
  },
  {
    "alpha2": "ky",
    "English": "Kirghiz"
  },
  {
    "alpha2": "la",
    "English": "Latin"
  },
  {
    "alpha2": "lb",
    "English": "Luxembourgish"
  },
  {
    "alpha2": "lg",
    "English": "Ganda"
  },
  {
    "alpha2": "li",
    "English": "Limburgan"
  },
  {
    "alpha2": "ln",
    "English": "Lingala"
  },
  {
    "alpha2": "lo",
    "English": "Lao"
  },
  {
    "alpha2": "lt",
    "English": "Lithuanian"
  },
  {
    "alpha2": "lu",
    "English": "Luba-Katanga"
  },
  {
    "alpha2": "lv",
    "English": "Latvian"
  },
  {
    "alpha2": "mg",
    "English": "Malagasy"
  },
  {
    "alpha2": "mh",
    "English": "Marshallese"
  },
  {
    "alpha2": "mi",
    "English": "Maori"
  },
  {
    "alpha2": "mk",
    "English": "Macedonian"
  },
  {
    "alpha2": "ml",
    "English": "Malayalam"
  },
  {
    "alpha2": "mn",
    "English": "Mongolian"
  },
  {
    "alpha2": "mr",
    "English": "Marathi"
  },
  {
    "alpha2": "ms",
    "English": "Malay"
  },
  {
    "alpha2": "mt",
    "English": "Maltese"
  },
  {
    "alpha2": "my",
    "English": "Burmese"
  },
  {
    "alpha2": "na",
    "English": "Nauru"
  },
  {
    "alpha2": "nb",
    "English": "Bokmål"
  },
  {
    "alpha2": "nd",
    "English": "Ndebele"
  },
  {
    "alpha2": "ne",
    "English": "Nepali"
  },
  {
    "alpha2": "ng",
    "English": "Ndonga"
  },
  {
    "alpha2": "nl",
    "English": "Dutch"
  },
  {
    "alpha2": "nn",
    "English": "Nynorsk"
  },
  {
    "alpha2": "no",
    "English": "Norwegian"
  },
  {
    "alpha2": "nr",
    "English": "Ndebele"
  },
  {
    "alpha2": "nv",
    "English": "Navajo"
  },
  {
    "alpha2": "ny",
    "English": "Nyanja"
  },
  {
    "alpha2": "oc",
    "English": "Occitan"
  },
  {
    "alpha2": "oj",
    "English": "Ojibwa"
  },
  {
    "alpha2": "om",
    "English": "Oromo"
  },
  {
    "alpha2": "or",
    "English": "Oriya"
  },
  {
    "alpha2": "os",
    "English": "Ossetian"
  },
  {
    "alpha2": "pa",
    "English": "Panjabi"
  },
  {
    "alpha2": "pi",
    "English": "Pali"
  },
  {
    "alpha2": "pl",
    "English": "Polish"
  },
  {
    "alpha2": "ps",
    "English": "Pushto"
  },
  {
    "alpha2": "pt",
    "English": "Portuguese"
  },
  {
    "alpha2": "qu",
    "English": "Quechua"
  },
  {
    "alpha2": "rm",
    "English": "Romansh"
  },
  {
    "alpha2": "rn",
    "English": "Rundi"
  },
  {
    "alpha2": "ro",
    "English": "Romanian"
  },
  {
    "alpha2": "ru",
    "English": "Russian"
  },
  {
    "alpha2": "rw",
    "English": "Kinyarwanda"
  },
  {
    "alpha2": "sa",
    "English": "Sanskrit"
  },
  {
    "alpha2": "sc",
    "English": "Sardinian"
  },
  {
    "alpha2": "sd",
    "English": "Sindhi"
  },
  {
    "alpha2": "se",
    "English": "Northern Sami"
  },
  {
    "alpha2": "sg",
    "English": "Sango"
  },
  {
    "alpha2": "si",
    "English": "Sinhala"
  },
  {
    "alpha2": "sk",
    "English": "Slovak"
  },
  {
    "alpha2": "sl",
    "English": "Slovenian"
  },
  {
    "alpha2": "sm",
    "English": "Samoan"
  },
  {
    "alpha2": "sn",
    "English": "Shona"
  },
  {
    "alpha2": "so",
    "English": "Somali"
  },
  {
    "alpha2": "sq",
    "English": "Albanian"
  },
  {
    "alpha2": "sr",
    "English": "Serbian"
  },
  {
    "alpha2": "ss",
    "English": "Swati"
  },
  {
    "alpha2": "st",
    "English": "Sotho"
  },
  {
    "alpha2": "su",
    "English": "Sundanese"
  },
  {
    "alpha2": "sv",
    "English": "Swedish"
  },
  {
    "alpha2": "sw",
    "English": "Swahili"
  },
  {
    "alpha2": "ta",
    "English": "Tamil"
  },
  {
    "alpha2": "te",
    "English": "Telugu"
  },
  {
    "alpha2": "tg",
    "English": "Tajik"
  },
  {
    "alpha2": "th",
    "English": "Thai"
  },
  {
    "alpha2": "ti",
    "English": "Tigrinya"
  },
  {
    "alpha2": "tk",
    "English": "Turkmen"
  },
  {
    "alpha2": "tl",
    "English": "Tagalog"
  },
  {
    "alpha2": "tn",
    "English": "Tswana"
  },
  {
    "alpha2": "to",
    "English": "Tonga"
  },
  {
    "alpha2": "tr",
    "English": "Turkish"
  },
  {
    "alpha2": "ts",
    "English": "Tsonga"
  },
  {
    "alpha2": "tt",
    "English": "Tatar"
  },
  {
    "alpha2": "tw",
    "English": "Twi"
  },
  {
    "alpha2": "ty",
    "English": "Tahitian"
  },
  {
    "alpha2": "ug",
    "English": "Uighur"
  },
  {
    "alpha2": "uk",
    "English": "Ukrainian"
  },
  {
    "alpha2": "ur",
    "English": "Urdu"
  },
  {
    "alpha2": "uz",
    "English": "Uzbek"
  },
  {
    "alpha2": "ve",
    "English": "Venda"
  },
  {
    "alpha2": "vi",
    "English": "Vietnamese"
  },
  {
    "alpha2": "vo",
    "English": "Volapük"
  },
  {
    "alpha2": "wa",
    "English": "Walloon"
  },
  {
    "alpha2": "wo",
    "English": "Wolof"
  },
  {
    "alpha2": "xh",
    "English": "Xhosa"
  },
  {
    "alpha2": "yi",
    "English": "Yiddish"
  },
  {
    "alpha2": "yo",
    "English": "Yoruba"
  },
  {
    "alpha2": "za",
    "English": "Zhuang"
  },
  {
    "alpha2": "zh",
    "English": "Chinese"
  },
  {
    "alpha2": "zu",
    "English": "Zulu"
  }
];


var googleLanguages= [
{
"language": "af",
"name": "Afrikaans"
},
{
"language": "sq",
"name": "Albanian"
},
{
"language": "ar",
"name": "Arabic"
},
{
"language": "hy",
"name": "Armenian"
},
{
"language": "az",
"name": "Azerbaijani"
},
{
"language": "eu",
"name": "Basque"
},
{
"language": "be",
"name": "Belarusian"
},
{
"language": "bn",
"name": "Bengali"
},

{
"language": "bg",
"name": "Bulgarian"
},
{
"language": "ca",
"name": "Catalan"
},
{
"language": "ceb",
"name": "Cebuano"
},
{
"language": "ny",
"name": "Chichewa"
},
{
"language": "zh",
"name": "Chinese (Simplified)"
},
{
"language": "zh-TW",
"name": "Chinese (Traditional)"
},
{
"language": "hr",
"name": "Croatian"
},
{
"language": "cs",
"name": "Czech"
},
{
"language": "da",
"name": "Danish"
},
{
"language": "nl",
"name": "Dutch"
},
{
"language": "en",
"name": "English"
},
{
"language": "eo",
"name": "Esperanto"
},
{
"language": "et",
"name": "Estonian"
},
{
"language": "tl",
"name": "Filipino"
},
{
"language": "fi",
"name": "Finnish"
},
{
"language": "fr",
"name": "French"
},
{
"language": "gl",
"name": "Galician"
},
{
"language": "ka",
"name": "Georgian"
},
{
"language": "de",
"name": "German"
},
{
"language": "el",
"name": "Greek"
},
{
"language": "gu",
"name": "Gujarati"
},
{
"language": "ht",
"name":"Haitian Creole"
},
{
"language": "ha",
"name": "Hausa"
},
{
"language": "iw",
"name": "Hebrew"
},
{
"language": "hi",
"name": "Hindi"
},
{
"language": "hmn",
"name": "Hmong"
},
{
"language": "hu",
"name": "Hungarian"
},
{
"language": "is",
"name": "Icelandic"
},
{
"language": "ig",
"name": "Igbo"
},
{
"language": "id",
"name": "Indonesian"
},
{
"language": "ga",
"name": "Irish"
},
{
"language": "it",
"name": "Italian"
},
{
"language": "ja",
"name": "Japanese"
},
{
"language": "jw",
"name": "Javanese"
},
{
"language": "kn",
"name": "Kannada"
},
{
"language": "kk",
"name": "Kazakh"
},
{
"language": "km",
"name": "Khmer"
},
{
"language": "ko",
"name": "Korean"
},
{
"language": "lo",
"name": "Lao"
},
{
"language": "la",
"name": "Latin"
},
{
"language": "lv",
"name": "Latvian"
},
{
"language": "lt",
"name": "Lithuanian"
},
{
"language": "mk",
"name": "Macedonian"
},
{
"language": "mg",
"name": "Malagasy"
},
{
"language": "ms",
"name": "Malay"
},
{
"language": "ml",
"name": "Malayalam"
},
{
"language": "mt",
"name": "Maltese"
},
{
"language": "mi",
"name": "Maori"
},
{
"language": "mr",
"name": "Marathi"
},
{
"language": "mn",
"name": "Mongolian"
},
{
"language": "my",
"name": "Myanmar (Burmese)"
},
{
"language": "ne",
"name": "Nepali"
},
{
"language": "no",
"name": "Norwegian"
},
{
"language": "fa",
"name": "Persian"
},
{
"language": "pl",
"name": "Polish"
},
{
"language": "pt",
"name": "Portuguese"
},
{
"language": "pa",
"name": "Punjabi"
},
{
"language": "ro",
"name": "Romanian"
},
{
"language": "ru",
"name": "Russian"
},
{
"language": "sr",
"name": "Serbian"
},
{
"language": "st",
"name": "Sesotho"
},
{
"language": "si",
"name": "Sinhala"
},
{
"language": "sk",
"name": "Slovak"
},
{
"language": "sl",
"name": "Slovenian"
},
{
"language": "so",
"name": "Somali"
},
{
"language": "es",
"name": "Spanish"
},
{
"language": "su",
"name": "Sundanese"
},
{
"language": "sw",
"name": "Swahili"
},
{
"language": "sv",
"name": "Swedish"
},
{
"language": "tg",
"name": "Tajik"
},
{
"language": "ta",
"name":" Tamil"
},
{
"language": "te",
"name": "Telugu"
},
{
"language": "th",
"name": "Thai"
},
{
"language": "tr",
"name": "Turkish"
},
{
"language": "uk",
"name": "Ukrainian"
},
{
"language": "ur",
"name": "Urdu"
},
{
"language": "uz",
"name": "Uzbek"
},
{
"language": "vi",
"name": "Vietnamese"
},
{
"language": "cy",
"name": "Welsh"
},
{
"language": "yi",
"name": "Yiddish"
},
{
"language": "yo",
"name": "Yoruba"
},
{
"language": "zu",
"name": "Zulu"
}
];

var userNamesStreamsArray =["2arunpmohan","celestinetimmy"];


var dropDownFunction = function(userNamesStreamsArray){
//	alert('hi');
	var array = userNamesStreamsArray;
	var count = 0 ;
	var length = array.length;
	for(count;count<length;count++){
$('.compose-dropdown').append('<li><span><input type="checkbox" user-name="'+array[count]+'" class="compose-checkbox" id="myCheckbox'+count+'" /></span><span class="media-icon"></span>'+array[count]+'<i class="ion-star"></i> <i class="ion-pin"></i></li>');
	}
}


var monthAbbr=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var changetoMonth={'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12};

// var convertDatetoEs=function(startValue,endValue,customPicker){

//  if(customPicker){
//  var startValue=startValue;
//  startValue=startValue.split(/\b\s+(?!$)/);
//  startValue=changetoMonth[startValue[1]]+'/'+startValue[0]+'/'+startValue[2];
//  var endValue=endValue;
//  endValue=endValue.split(/\b\s+(?!$)/);
//  endValue=changetoMonth[endValue[1]]+'/'+endValue[0]+'/'+endValue[2];
//   $("#date-fld1").val(startValue);
//    $("#date-fld2").val(endValue);
//    $(".timeFilter").removeClass("active");
//    $(".customDateSelect").addClass("active"); 
//   }

//  updateTimeSelectionPanel(startValue,endValue);
// // self.onTimeChange(e.startValue,e.endValue);
//  startValue=moment(startValue,"YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss")+".000Z";
//  endValue=moment(endValue,"YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss")+".000Z";
//  globalStartDuration=startValue;
//  globalEndDuration=endValue;
//  console.log("startValue  "+startValue +"endValue   "+endValue);
//  globalObject.customTimeChange(startValue,endValue);

// };



var updateTimeSelectionPanel=function(fromDate,toDate){

fromDate=fromDate.split("/");
toDate=toDate.split("/");
     // console.log("from date",fromDate);
$('.time-select span').html('<i class="fa fa-clock-o"></i>' + fromDate[2]+' '+monthAbbr[fromDate[1]-1] +' '+fromDate[0]+'-'+ toDate[2]+' '+monthAbbr[toDate[1]-1] +' '+toDate[0]+ '<i class="ion-ios-arrow-down"></i>');


};

var convertDatetoEs=function(startValue,endValue,startDate,endDate,customPicker){
 if(customPicker){

  $("#date-fld1").val(startValue);
   $("#date-fld2").val(endValue);
   $(".timeFilter").removeClass("active");
   $(".customDateSelect").addClass("active");
   updateTimeSelectionPanel1(startDate,endDate);

  }
   else{
    updateTimeSelectionPanel(startValue,endValue);
    }
 startValue=moment(startValue,"YYYY/MM/DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss")+".000Z";
 endValue=moment(endValue,"YYYY/MM/DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss")+".000Z";
 globalStartDuration=startValue;
 globalEndDuration=endValue;
 console.log("startValue  "+startValue +" endValue   "+endValue);
 globalObject.customTimeChange(startValue,endValue);

};

var updateTimeSelectionPanel1=function(fromDate,toDate){

     // console.log("from date",fromDate);
$('.time-select span').html('<i class="fa fa-clock-o"></i>' + fromDate+'-'+ toDate+ '<i class="ion-ios-arrow-down"></i>');


};

var buildTableBody=function(data, columns) {
    var body = [];

    body.push(columns);

    data.forEach(function(row) {
        var dataRow = [];

        columns.forEach(function(column) {
            dataRow.push(row[column].toString());
        })

        body.push(dataRow);
    });

    return body;
};


var convertImgToBase64=function()
{   var dataURL=0;
    var canvas = document.createElement('CANVAS');
    img = document.createElement('img'),
    img.src = 'http://pbs.twimg.com/profile_images/662986216753631233/48yIn97Z_normal.jpg';
    img.onload = function()
    {
        canvas.height = img.height;
        canvas.width = img.width;
         dataURL = canvas.toDataURL('image/png');
         
        canvas = null; 
    };

};

var fillDateWithZero = function(data){
    // var d=new Date();
    // console.log("d.............",d);
    console.log("fill with data ..",data);
      var arr=[];
      var index1;
      var firstDate;var secondDate;
     for(var index in data){
          arr.push(data[index]);
        if(index<data.length-1)
         {
              index1=+index+1;
              var firstDate=data[index].key;
              var secondDate=data[index1].key;
          while((firstDate+(1000*60*60*24))<secondDate)
           {
            firstDate=firstDate+(1000*60*60*24);
            arr.push({key:firstDate,likes:{buckets:[]},comments:{buckets:[]},shares:{buckets:[]}});
            var date=new Date(firstDate);
            console.log("date format",date);

           }
         }
    
        }
      return arr;
   };

   Date.prototype.getIndex = function() {
      var currentDate = new Date(this.getFullYear(),0,1);
      return this.getFullYear()+'-'+padZero(Math.ceil((((this - currentDate) / 86400000) + currentDate.getDay()+1)/7));
  }

  var padZero = function(num){
     return (num < 10) ? ("0" + num) : num;
  }
  
  var foamTreeNegationKeywords = function(identifier){
//      identifier = 'faceBook';
      var fieldName;
      if(identifier=='faceBook'){
        fieldName = 'foamTreeFbKeys';
      }
      if(identifier=="twitter"){
        fieldName = 'foamTreeTwitterKeys';
      }
      if(!isNull(globalConfig._source[''+globalTagValue+''][''+identifier+''][''+fieldName+''])){
            var keyWordsForNegation = globalConfig._source[''+globalTagValue+''][''+identifier+''][''+fieldName+''];
            console.log('foam tree negation keywords',keyWordsForNegation);
            var splitKeyWords = keyWordsForNegation.split('+++');
            console.log('foam tree negation keywords',splitKeyWords);
      }
      

      return splitKeyWords;
  }

  var removeKeywordsFoamTree=function(negatedKeywords,result){

    for (index in negatedKeywords) {
        for (var clusterIndex in result.clusters) {
            if (result.clusters[clusterIndex].label == negatedKeywords[index]) {
                result.clusters.splice(clusterIndex, 1);
            }

        }

    }
    return result;

  }
  
  var divInfo = {
    divInfoTwitterSmm : [],
    divInfoFacebook: [],
    divInfoDHomepage: [],
    divInfoYoutube: [],
    divInfoTwitterStats: [],
    divInfoTwitterAnalytics: [],
    divInfoFacbookAnalytics : ['dataContentFbBar',
    'dataContentFoamtreeFb',
    'dataContentFbPie',
    'dataContentFbPages',
    'dataContentFbPosts',
    'dataContentFbBarCommentsWeekDays',
    'dataContentFbBarCommentsHourlyStats'
    ],
    divInfoInstagramAnalytics: [],
    divInfoYoutubeAnalytics : [] 
}

var permissionDenialDisplay = function(divInfoPage){
    console.log('getting called');
    divList = divInfo[''+divInfoPage+''];
    arrayLength = divList.length ;
    for(var index =0; arrayLength; index++){
        $('.'+divList[index]+'').html('Sorry, no permission for the selected user')
    }
}

var noPermissionsHtml = function(){
    var html = '<div class="no-access-permission">'+
                    '<section class="panel panel-default">'+
                        '<article class="panel-body clearfix text-center">'+
                            '<h2> No Access Permission</h2>'+
                        '</article>'+
                    '</section>'+
                '</div>'
    
    return html
}

var idsHandlesTestPowerTrack = "Marvel,Comics,SuperMan,IronMan"
var prependum = "from:"

var powerTrackFormatConversion = function(prepend,string){
    //first break the strings at ","
    var stringArray = string.split(",");
    //adding the prependum to each element in the array(which is got by splitting the string)
    for(var index =0;index<stringArray.length;index++){
        stringArray[index] = prepend+stringArray[index];
    }
    //converting the prependum array to a string
    var result = stringArray.toString();

    stringArray=[];
    //returning the result
    result = result.replace(/,/g,' ');
    return result;
}

//var result = powerTrackFormatConversion(prependum,idsHandlesTestPowerTrack);
//console.log('ids handles for powertrack',result)

var adminSectionPermissionHandler = function(){
    console.log('user permissions function',userPer);
    var adminPanelClasses = {
        smmTwitter : "smmTwitter-smm",
        smmFacebook : "smmFacebook-smm",
        analyticsTwitter : "analyticsTwitter-admin",
        analyticsFacebook : "analyticsFacebook-admin",
        analyticsInstagram : "analyticsInstagram-admin",
        analyticsYoutube : "analyticsYoutube-admin",
        analyticsMedia : "analyticsMedia-admin",
    }
    for(var index in userPer){
        console.log('user per',index);
        console.log('user per values',userPer[index]);
        console.log('user per maps',adminPanelClasses[index]);
        
        if(userPer[index] == false){
            $('.'+adminPanelClasses[index]+'').html(noPermissionsHtml());
        }
    }
}

//adminSectionPermissionHandler();
var gnipKeywords = '(Ghana AND NPP) OR (Gabby AND Darko) OR (Kwabena AND Agyapong) OR (Paul AND Afoko AND NPP) OR (Gabby AND NPP) OR (Nana AND akufo) OR "New Patriotic Party" from:GabbyDarko from: odekro from: mbawumia from: ghananpp from: edemsuleman from: nppghana'


var gnipKeywordsCharacterCount = function(gnipKeywords){
var characterCountlength = gnipKeywords.length;
console.log('character count length',characterCountlength);
var newGnipKeywords = gnipKeywords.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
//    newGnipKeywords = newGnipKeywords.replace(/)/g , '');
console.log('new gnip keywords',newGnipKeywords);
}

//gnipKeywordsCharacterCount(gnipKeywords);