// JavaScript Document
var esClientAdmin = new Elasticsearch(esHost, assignValue, null);
var esClientValidate = new Elasticsearch(esHost, function() {
    return {};
}, {});

var tagsGetAdmin = function(user) {
    esClientAdmin.updateGet(user, function(response) {
        console.log('tagsGetAdmin response is', response);
        $('.tagDropdownButton').empty();
        tagsAutoLoad(response);
        $('.tagDropdown').empty();
        initialTagValueGenAdminHeader(response);
    })
};

function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

function arr_diffIndex(a1, a2) {
    var a = [],
        diff = [];
    for (var i = 0; i < a1.length; i++)
        a[a1[i]] = true;
    for (var i = 0; i < a2.length; i++)
        if (a[a2[i]]) delete a[a2[i]];
    for (var k in a)
        diff.push(k);
    return diff;
}
var initialTagValueGen = function(response) {
    var comparisonArray = ["twitter", "faceBook", "youtube", "instagram", "media"];
    var keys = [];
    var count = 0;
    for (var k in response._source) keys.push(k);
    diffIndex = arr_diffIndex(keys, comparisonArray);
    diffIndex = jQuery.grep(diffIndex, function(value) {
        return value != "facebook";
    });

    for (count; count < diffIndex.length; count++) {
        $('.tagDropdown').append('<li id="listId' + diffIndex[count] + '" class="contentTags" rvalue=' + count + ' value="' + diffIndex[count] + '" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + diffIndex[count] + '</a></li>');
    }
    tagValuesArray = diffIndex;

    $('.tagsAddition').html(diffIndex[0]);
    return diffIndex[0];
}
var initialTagValueGenAdminHeader = function(response) {
    var comparisonArray = ["twitter", "faceBook", "youtube", "instagram", "media"];
    var keys = [];
    var count = 0;
    for (var k in response._source) keys.push(k);
    diffIndex = arr_diffIndex(keys, comparisonArray);
    diffIndex = jQuery.grep(diffIndex, function(value) {
        return value != "facebook";
    });

    for (count; count < diffIndex.length; count++) {
        $('.tagDropdown').append('<li id="listId' + diffIndex[count] + '" class="contentTags" rvalue=' + count + ' value="' + diffIndex[count] + '" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + diffIndex[count] + '</a></li>');
    }
}
var tagTrimmer = function(tagValue) {
    var tagLength = tagValue.length;
    var trimmedValue = null;

    if (tagLength > 7) {
        trimmedValue = tagValue.substring(0, 7) + "...";
    } else {
        trimmedValue = tagValue;
    }
    return trimmedValue;
}
var diff = [];
var tagsAutoLoad = function(response) {
    var comparisonArray = ["twitter", "faceBook", "youtube", "instagram", "media", "facebook"];
    var keys = [];
    var count = 0;
    for (var k in response._source) keys.push(k);
    //	console.log(arr_diff(comparisonArray,keys));
    diff = arr_diff(comparisonArray, keys);
    diff = jQuery.grep(diff, function(value) {
        return value != "facebook";
    });
    console.log('diff is', diff);
    for (count; count < diff.length; count++) {
        $('.tagDropdownButton').append('<li class="contentTags" id=' + count + ' rvalue=' + count + ' value="' + diff[count] + '" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + diff[count] + '</a><a class="removeQuery" removeValue=' + diff[count] + '>x</a></li>');
    }
};
var assignValue = function() {
    $("#next").click(function() {
        if (currentWizardIndex == 0) {
            var id = "twitterFormat";
            twitterArray.keywords = $('#keywordsTwitter').val().split(",");
            twitterArray.boolTracks = $('#keywordsTwitter').val();
            twitterArray.idsHandles = $('#idsHandlesTwitter').val().split(",");
            updateFn("configs", "config", user, id);
        } else if (currentWizardIndex == 1) {
            // alert("ok");
            // var id = "faceBookFormat";
            // // var tempArray = $('#pageLinkFb').val().split(/[\n, \t]+/);
            // // faceBookArray.pagelink = tempArray.filter(function (v) {
            // // 	return v !== ''
            // // });
            //    console.log("fb array",faceBookArray);
            // updateFn("configs", "config", user, id);

        } else if (currentWizardIndex == 2) {
            var id = "youtubeFormat";
            youtubeArray.keywordsYt = $('#keywordsYt').val().split(",");
            youtubeArray.channelLinks = $('#channelLinksYt').val().split(",");
            youtubeArray.youtubeLinks = $('#youtubeLinks').val().split(",");
            updateFn("configs", "config", user, id);

        } else if (currentWizardIndex == 3) {
            var id = "instagramFormat";
            instagramArray.tracks = $('#keywordsInsta').val().split(",");

            for (index = 0; index < instagramArray.tracks.length; index++) {
                var tracks = instagramArray.tracks[index].trim();
                if (tracks.indexOf(' ') !== -1) {
                    alert("Tags should not contain space");
                    return true;
                }
            }
            console.log("instagram  Array", instagramArray.tracks);
            instagramArray.follows = $('#followsInsta').val().split(",");
            updateFn("configs", "config", user, id);
        }

    })


    var validateMediaQuery = function(response) {
        console.log('response valid/not', response.valid);
        var id = "mediaFormat";
        if (response.valid == true) {
            updateFn("configs", "config", user, id);
        } else {
            alertError("QUERY FORMAT IMPORPER,PLEASE CHECK AND ENTER AGAIN")
        }
    };


};