var generateFbMainQuery = function(liveParametersFb){
var size = liveParametersFb.size || 10;
var granularity = liveParametersFb.granularity || "hour";
var isFirstSearch = true;
	
var queryBodyFb = {
  "size":size,
  "aggs": {
    "pieChartSentiFb": {
      "terms": {
        "field": "sentiments.overallSentiment"
      }
    },
    "differentPages": {
      "terms": {
        "field": "Page.name.raw",
        "size": size
      }
    },
	  
//	  ,
//	  "differentGroups": {
//      "terms": {
//        "field": "Group.name.raw",
//        "size": size
//      }
//    },
    "totalPages": {
      "value_count": {
        "field": "Page.name.raw"
      }
    },
    "uniqueUsers": {
      "value_count": {
        "field": "comments.id"
      }
    },
    "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": granularity
      },
      "aggs": {
        "comments": {
          "terms": {
            "field": "CommentsSummary.commentsCount"
          }
        },
        "likes": {
          "terms": {
            "field": "likesCount"
          }
        },
        "shares": {
          "terms": {
            "field": "sharesCount"
          }
        }
      }
    },
    "postsCount": {
      "filter": {
        "term": {
          "_type": "post"
        }
      }
    },
    "sumOfLikes": {
      "sum": {
        "field": "likesCount"
      }
    },
    "sumOfShares": {
      "sum": {
        "field": "sharesCount"
      }
    },
    "pageCateogries": {
      "terms": {
        "field": "Page.category.raw"
      }
    }
  }
};
	return queryBodyFb;
	
};

var genFbCommentsQuery = function(queryParams){
	var queryBody = {
		"aggs":{ 
		  "byDays": {
			  "terms": {
				"script": "dateConversion",
				"params": {
				  "date_field": "created_at",
				  "format": "EEEEEE"
				}
			  },
        "aggs":{
            "byHours": {
        "terms": {
          "order" : { "_term" : "asc" },
        "script": "dateConversion",
        "params": {
          "date_field": "created_at",
          "format": "H"
        }
        }
         }


        }
			 }
			
			}
    };

  //    var queryBody = {
  //   "aggs":{ 
  //     "byDays": {
  //       "terms": {
  //       "script": "dateConversion",
  //       "params": {
  //         "date_field": "created_at",
  //         "format": "EEEEEE"
  //       }
  //       }
      
       


 
  //      },
  //     "byHours": {
  //       "terms": {
  //         "order" : { "_term" : "asc" },
  //       "script": "dateConversion",
  //       "params": {
  //         "date_field": "created_at",
  //         "format": "HH"
  //       }
  //       }
  //        }
      
  //     }
  // };








		return queryBody;
};