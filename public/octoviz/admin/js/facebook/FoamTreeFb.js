//ES data request


 var foamTreeFb=null;
 var idListFb =[];
var csv = {
 "foamTreeCsvBFb" : ""
}

var generateFbFoamMainQuery = function (liveParameters) {
		var ftQuery = liveParameters.foamtreeQuery || "*";
    var request = {
        "search_request": {
            "fields": ["text"],
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "query": ftQuery
                            }
                        }
                        ]
                }
            },
            "sort": [
                {
                    "created_at": {
                        "order": "desc"
                    }
            }
         ],
            "size": 1000
        },

        "query_hint": "*",
        "algorithm": "lingo",
        "max_hits": 10,
        "include_hits": false,
        "field_mapping": {
            "title": ["fields.text"],
            "content": ["fields.text"],
            "url": ["fields._id"]
        }
    };
    return request;
}

// var esFoamTreeClusterFb =new ElasticsearchClusterClient(esHost + '/facebook/post', generateFbFoamMainQuery,{});




function setFoamTreeFb(input,foamtreeIds,result) {
		console.log("result from setfoamtree",result);
    // alert(foamtreeIds);
	  $(".dataContentFoamtreeFb").empty();

       // if (currentFoamTreeId!==foamtreeIds) {
       
        $(".dataContentFoamtreeFb").empty();
        if(foamTreeFb)
           foamTreeFb.dispose();
        foamTreeFb = new CarrotSearchFoamTree({
              id:"foamtreeFb",
              backgroundColor:"#fff",
               logging: true,
              wireframePixelRatio: 1,
              layout: "squarified",
              layoutByWeightOrder: true,
              showZeroWeightGroups: true,
              groupMinDiameter: 10,
              rectangleAspectRatioPreference: -1,
              relaxationInitializer: "ordered",
              relaxationMaxDuration: 3000,
              relaxationVisible: false,
              relaxationQualityThreshold: 1.03,
              stacking: "hierarchical",
              descriptionGroupType: "floating",
              descriptionGroupPosition: 225,
              descriptionGroupDistanceFromCenter: 1,
              descriptionGroupSize: 0.22,
              descriptionGroupMinHeight: 53,
              descriptionGroupMaxHeight: 0,
              descriptionGroupPolygonDrawn: true,
              maxGroups: 50000,
              maxGroupLevelsDrawn: 4,
              maxGroupLabelLevelsDrawn: 3,
              groupGrowingDuration: 0,
              groupGrowingEasing: "bounce",
              groupGrowingDrag: 0,
              groupResizingBudget: 2,
              groupBorderRadius: 0,
              groupBorderWidth: 0,
              groupBorderWidthScaling: 0.6,
              groupInsetWidth: 0,
              groupBorderRadiusCorrection: 1,
              groupSelectionOutlineWidth: 5,
              groupSelectionOutlineColor: "#fff",
              groupSelectionOutlineShadowSize: 2,
              groupSelectionOutlineShadowColor: "#000",
              groupSelectionFillHueShift: 0,
              groupSelectionFillSaturationShift: 0,
              groupSelectionFillLightnessShift: 0,
              groupSelectionStrokeHueShift: 0,
              groupSelectionStrokeSaturationShift: 0,
              groupSelectionStrokeLightnessShift: -10,
              groupFillType: "plain",
              groupFillGradientRadius: 1.2,
              groupFillGradientCenterHueShift: 0,
              groupFillGradientCenterSaturationShift: 0,
              groupFillGradientCenterLightnessShift: 30,
              groupFillGradientRimHueShift: 0,
              groupFillGradientRimSaturationShift: 0,
              groupFillGradientRimLightnessShift: 0,
              groupStrokeType: "plain",
              groupStrokeWidth: 2.5,
              groupStrokePlainHueShift: 0,
              groupStrokePlainSaturationShift: 0,
              groupStrokePlainLightnessShift: -10,
              groupStrokeGradientRadius: 1,
              groupStrokeGradientAngle: 45,
              groupStrokeGradientUpperHueShift: 0,
              groupStrokeGradientUpperSaturationShift: 0,
              groupStrokeGradientUpperLightnessShift: 20,
              groupStrokeGradientLowerHueShift: 0,
              groupStrokeGradientLowerSaturationShift: 0,
              groupStrokeGradientLowerLightnessShift: 0,
              groupHoverFillHueShift: 0,
              groupHoverFillSaturationShift: 0,
              groupHoverFillLightnessShift: 20,
              groupHoverStrokeHueShift: 0,
              groupHoverStrokeSaturationShift: 0,
              groupHoverStrokeLightnessShift: 10,
              groupExposureScale: 1.15,
              groupExposureShadowColor: "#000",
              groupExposureShadowSize: 50,
              groupExposureZoomMargin: 0.1,
              groupUnexposureLightnessShift: -50,
              groupUnexposureSaturationShift: -65,
              groupUnexposureLabelColorThreshold: 0.15,
              exposeDuration: 700,
              exposeEasing: "squareInOut",
              groupContentDecoratorTriggering: "onLayoutDirty",
              openCloseDuration: 500,
              rainbowColorDistribution: "linear",
              rainbowColorDistributionAngle: 45,
              rainbowLightnessDistributionAngle: 45,
              rainbowSaturationCorrection: 0.1,
              rainbowLightnessCorrection: 0.4,
              rainbowStartColor: "hsla(164, 83%, 36%, 1)",
              rainbowEndColor: "hsla(176, 70%, 87%, 1)",
              rainbowLightnessShift: 30,
              rainbowLightnessShiftCenter: 0.4,
              parentFillOpacity: 0.7,
              parentStrokeOpacity: 1,
              parentLabelOpacity: 1,
              parentOpacityBalancing: true,
              wireframeDrawMaxDuration: 15,
              wireframeLabelDrawing: "auto",
              wireframeContentDecorationDrawing: "auto",
              wireframeToFinalFadeDuration: 500,
              wireframeToFinalFadeDelay: 300,
              finalCompleteDrawMaxDuration: 80,
              finalIncrementalDrawMaxDuration: 100,
              finalToWireframeFadeDuration: 200,
              androidStockBrowserWorkaround: false,
              incrementalDraw: "fast",
              groupLabelFontFamily: "sans-serif",
              groupLabelFontStyle: "normal",
              groupLabelFontWeight: "normal",
              groupLabelFontVariant: "normal",
              groupLabelLineHeight: 1.05,
              groupLabelHorizontalPadding: 1,
              groupLabelVerticalPadding: 1,
              groupLabelMinFontSize: 6,
              groupLabelMaxFontSize: 160,
              groupLabelMaxTotalHeight: 0.9,
              groupLabelUpdateThreshold: 0.05,
              groupLabelDarkColor: "#000",
              groupLabelLightColor: "hsla(173, 73%, 78%, 1)",
              groupLabelColorThreshold: 0.35,
              rolloutStartPoint: "center",
              rolloutEasing: "squareOut",
              rolloutMethod: "groups",
              rolloutDuration: 2000,
              rolloutScalingStrength: -0.7,
              rolloutTranslationXStrength: 0,
              rolloutTranslationYStrength: 0,
              rolloutRotationStrength: -0.7,
              rolloutTransformationCenter: 0.7,
              rolloutPolygonDrag: 0.1,
              rolloutPolygonDuration: 0.5,
              rolloutLabelDelay: 0.8,
              rolloutLabelDrag: 0.1,
              rolloutLabelDuration: 0.5,
              rolloutChildGroupsDrag: 0.1,
              rolloutChildGroupsDelay: 0.2,
              pullbackStartPoint: "center",
              pullbackEasing: "squareIn",
              pullbackMethod: "individual",
              pullbackDuration: 1500,
              pullbackScalingStrength: -0.7,
              pullbackTranslationXStrength: 0,
              pullbackTranslationYStrength: 0,
              pullbackRotationStrength: -0.7,
              pullbackTransformationCenter: 0.7,
              pullbackPolygonDelay: 0.3,
              pullbackPolygonDrag: 0.1,
              pullbackPolygonDuration: 0.8,
              pullbackLabelDelay: 0,
              pullbackLabelDrag: 0.1,
              pullbackLabelDuration: 0.3,
              pullbackChildGroupsDelay: 0.1,
              pullbackChildGroupsDrag: 0.1,
              pullbackChildGroupsDuration: 0.3,
              fadeDuration: 1200,
              fadeEasing: "cubicInOut",
              zoomMouseWheelFactor: 1.5,
              zoomMouseWheelDuration: 500,
              zoomMouseWheelEasing: "squareOut",
              maxLabelSizeForTitleBar: 0,
              titleBarFontFamily: null,
              titleBarFontStyle: "normal",
              titleBarFontWeight: "normal",
              titleBarFontVariant: "normal",
              titleBarBackgroundColor: "rgba(0, 0, 0, 0.5)",
              titleBarTextColor: "rgba(255, 255, 255, 1)",
              titleBarMinFontSize: 0,
              titleBarMaxFontSize: 0,
              titleBarTextPaddingLeftRight: 20,
              titleBarTextPaddingTopBottom: 15,
              attributionText: null,
              attributionLogo: null,
              attributionLogoScale: 0.1,
              attributionUrl: "http://carrotsearch.com/foamtree",
              attributionPosition: 45,
              attributionDistanceFromCenter: 1,
              attributionWeight: 0.025,
              attributionTheme: "light",
              interactionHandler: "builtin",
              imageData: null,
              viewport: null,
              times: null

        });
 currentFoamTreeId=foamtreeIds;
    // }

    foamTreeFb.set("onGroupClick", function (info) {
        var ids = [];
        var topic = "";
        var sentiments = {};
        for (var clusterIndex in currentResults.clusters) {
            var cluster = currentResults.clusters[clusterIndex];
            if (cluster.label == info.group.label) {
                ids = cluster.documents;
                topic = cluster.label;
                sentiments = cluster.sentiments;
            }
        }
		idListFb = [];
	    for(var i=0;i<ids.length;i++){
			idListFb.push(ids[i])
		}
		console.log('id lists on group click',idListFb);
		esFoamTreeClusterFb.search(ids,10,modalRenderFunctionFb);
		
//		alert(info.group.label);
		
    });
    foamTreeFb.set("dataObject", input);
    //foamTreeFb=null;

};

$('body').on('click', '.sizePickerModalAnalytics', function() {
//    alert("x");
    $(".sizePickerModalAnalytics").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
//	alert('hi');
	esFoamTreeClusterFb.search(idListFb,sizeValue,modalRenderFunctionFb);

});


var modalRenderFunctionFb = function(response){
   csv.foamTreeCsvFb = "Post,Category,Name,Link,Date\n" +
        convertBucketsToCSVFoamtreeFb(response);
	var docStatsArray = [];
	var totalHitsCount = response.hits.total;
	var hit = response.hits.hits;
	for(var index in hit){
			docStatsArray.push({
			 	text:hit[index]._source.text,
				category:hit[index]._source.Page.category,
				name: hit[index]._source.Page.name,
				link:hit[index]._source.link,
				date:utcToDateFormat(hit[index]._source.created_at)
//				author:hit[index]._source.user.name
			});
			}

	 $("#docStatsFb").html($("#docStatsTableFb").render(docStatsArray));
		$(".dataContentModal").empty();
    $('#myModalFb').modal('show');
    $(".dataContentModal").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');

//		$("#myModal").modal('show');
	};

var convertBucketsToCSVFoamtreeFb = function(response){
//console.log('response',response);
    var csvInside = "";
	if(!isNull(response.hits)){
//        var totalHits = response.hits.hits;
        var hitArray = response.hits.hits;
		var hitArrayLength = hitArray.length;
        console.log('hits array',hitArray);
//        var hitIndex = 0;
        for (var hitIndex in hitArray) {
//            console.log('hitindex',hitIndex)
            var hit = hitArray[hitIndex];
            if (isNull(hit) || isNull(hit._source)) {
                csvInside += "";
                continue;
            }
            csvInside += (hit._source.text.replace(/,/g, '')).replace(/(\r\n|\n|\r)/gm,"") + ',' + hit._source.Page.category + ',' + hit._source.Page.name + ','+ hit._source.link + ','+ utcToDateFormat(hit._source.created_at)+ '\n';
    }
    }
    
//	console.log('csvInside build',csvInside);
    return csvInside;

};

$('body').on('click', '.sizePickerModalFb', function() {
//    alert("x");
    $(".sizePickerModalFb").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
//	alert('hi');
	console.log('id lists fb,on size change',idListFb);
	esFoamTreeClusterFb.search(idListFb,sizeValue,modalRenderFunctionFb);
});


$('body').on('click','.downloadLinkModalFoamFb',function(){
	    var downloadToken = $(this).attr('downloadtokenAnalyticsFb');
//		alert(downloadToken);
		var blob = new Blob([csv[downloadToken]], {
			type: "text/plain;charset=utf-8"
		});
    saveAs(blob, "downloadToken.csv");
});


var createFoamTreeFb = function (foamtreeIds,searchInstance) {
    var index = "facebook";
    var indexType = "post";
		$(".dataContentFoamtreeFb").html('<div  ><img  id="img-spinner"  src="img/spinner.gif" alt="Loading" /> </div>');

    searchInstance.search(function (result) {
		
		$(".dataContentFoamtreeFb").empty();
            currentResults = result;
            console.log('clusters here',result);
            var idx = -1;
            for (var index in result.clusters) {
                if (result.clusters[index].label == "Other Topics") {
                    idx = index;
                }

            }
            if (idx != -1) {
                result.clusters.splice(idx, 1);
            }
           
          var negatedKeywords=foamTreeNegationKeywords("faceBook");
          console.log("negatedKeywords.......",negatedKeywords);
          result=removeKeywordsFoamTree(negatedKeywords,result);
            function calculateUniqueDocumentsCount(cluster) {
             

                var uniqueIds = {};
                if (cluster.documents) {
                    cluster.documents.forEach(function (id) {
                        uniqueIds[id] = true;
                    });
                }

                if (cluster.clusters) {
                    cluster.clusters.forEach(function (subcluster) {
                        for (var key in calculateUniqueDocumentsCount(subcluster)) {
                            uniqueIds[key] = true;
                        };
                    });
                }
                cluster.uniqueDocumentsCount = Object.keys(uniqueIds).length;
                return uniqueIds;
				
            }

            result.clusters.forEach(function (cluster) {
                calculateUniqueDocumentsCount(cluster);
            });
           
            // Convert the results to the format required by the visualization:
            // http://download.carrotsearch.com/circles/demo/api/#dataObject
            var visualizationInput = {
                groups: result.clusters.map(function mapper(cluster) {
                    return {
                        label: cluster.phrases[0],
                        weight: cluster.uniqueDocumentsCount,
                        groups: (cluster.clusters || []).map(mapper)
                    }
                })
            };
            if (result.info.algorithm == "lingo") {

                setFoamTreeFb(visualizationInput,foamtreeIds,result);
				$(".dataContentFoamtreeFb").empty();
//				$(".dataContentFoamtreeFb").html("No data Available");
            }
        });
}
