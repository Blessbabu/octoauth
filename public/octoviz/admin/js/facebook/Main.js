

$(document).ready(function() {
    $('.key').tooltip();
});


//GRANULARITY CHANGER FOR GRAPH
$('body').on('click', '.granularityPickerFb', function() {
    // var currentGranularity = $(this).attr("gValue");
    // $('.granularityTitle span:first').html(currentGranularity);
    $(".granularityPickerFb").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var granularityValue = $(this).attr("granularityValue");
    globalObject.liveParametersFb.granularity = granularityValue;
    globalObject.initTab();
});

//SIZE PICKER 
$('body').on('click', '.sizePickerFb', function() {
    $('#filter-result-fb').empty();
    $(".sizePickerFb").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    globalObject.liveParametersFb.size = sizeValue;
    globalObject.initTab();
});

//DOWNLOAD CSV DATA
//$('body').on('click', '.downloadLinkFb', function(e) {
//    e.preventDefault();
//    var downloadToken = $(this).attr('downloadTokenFb');
//    var blob = new Blob([faceBookAnalyticsGlobal.csvFb[downloadToken]], {
//        type: "text/plain;charset=utf-8"
//    });
//    saveAs(blob, "downloadToken.csv");
//});


//REMOVE FILTER TAGS
$('body').on('click', '.selectedTagFb', function() {
    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
   
//     alert(field);
    console.log("Removing ", field, value);
    esClientFb.removeTermMatch(field, value);
     esFoamTreeFb.removeTermMatch(field, value);
    console.log("After removal ", esClientFb.queryParameters);
    if (!esClientFb.queryParameters.hasOwnProperty('Page.name.raw') || esClientFb.queryParameters['Page.name.raw'].values.length == 0) {
        var pageIDS = getFacebookKeywords(globalTagValue);
        esClientFb.addTermMatch("Page.name.raw", pageIDS);
         esFoamTreeFb.addTermMatch("Page.name.raw", pageIDS);
    }
//	 if (!esClientFb.queryParameters.hasOwnProperty('Group.name.raw') || esClientFb.queryParameters['Group.name.raw'].values.length == 0) {
//        var groupIDS = getFaceBookKeywordsGroups(globalTagValue);
//        esClientFb.addTermMatch("Group.name.raw", groupIDS);
//         esFoamTreeFb.addTermMatch("Group.name.raw", groupIDS);
//    }
    globalObject.conductSearch();
    $(this).remove();
});


//FILTER TAG ADDITION
$('body').on('click', '.checkbox', function() {
    var testingTit = $(this).children(".pd1").attr("id");
    var testingIpt = $(this).children(".pd2").attr("id");
    var testingInt = $(this).children(".pd3").attr("id");
    var identifier = $(this).attr('fieldType');

    if (($("#" + testingTit).is(':checked')) ||
        ($("#" + testingIpt).is(':checked')) ||
        ($("#" + testingInt).is(':checked'))) {

        var name = $(this).attr('value');
//        clickedFiltersFb.push({
//            "field": "Page.name.raw",
//            "value": name
//        });
        globalObject.tagAddFunctionFb({
            "field": "Page.name.raw",
            "value": name
        });
        esClientFb.removeTermMatch("Page.name.raw");
        esClientFb.addTermMatch("Page.name.raw", name);
        esFoamTreeFb.removeTermMatch("Page.name.raw");
        esFoamTreeFb.addTermMatch("Page.name.raw", name);

        globalObject.conductSearch();
    }

});


$('body').on('click', '.fbsinglePost', function() {
    $('.dataContentCommentList').empty();
    $("#commentstListFb").empty();
    $("#subCommentstListFb").empty();
    $("#dataContentSubCommentList").empty();
    var parentId= $(this).attr("parentId");
    globalObject.selectedPostId  = parentId;
    globalObject.getCommentList();
});

$('body').on('click', '.fbsingleComment', function() {
    $(".fbsingleComment").removeClass("active");
    $(this).addClass("active");
    $("#subCommentstListFb").empty();
    $("#dataContentSubCommentList").empty();
    var commentId= $(this).attr("commentId");
    globalObject.selectedCommentId = commentId;
    globalObject.getSubCommentList();
});

$('body').on('click', '.PostPicketFb', function() {  
    $(".PostPicketFb").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var postValue = $(this).attr("postValue");
    $('.dataContentCommentList').empty();
    $("#commentstListFb").empty();
    $("#subCommentstListFb").empty();
    $("#dataContentSubCommentList").empty();
    globalObject.sortIdentifier = postValue;
    globalObject.postSortedRenderFunction();
});

$('body').on('click', '.CommentPicketFb', function() {  
    $(".CommentPicketFb").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var postValue = $(this).attr("postValue");
    $('.dataContentCommentList').empty();
    $("#commentstListFb").empty();
    $("#subCommentstListFb").empty();
    $("#dataContentSubCommentList").empty();
    globalObject.sortIdentifierComment = postValue;
    globalObject.getCommentList();
});

$('body').on('click', '.SubCommentPicketFb', function() {  
    $(".SubCommentPicketFb").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var postValue = $(this).attr("postValue");
    $("#subCommentstListFb").empty();
    $("#dataContentSubCommentList").empty();
    globalObject.sortIdentifierSubComment = postValue;
    globalObject.getSubCommentList();
});






       
  
















