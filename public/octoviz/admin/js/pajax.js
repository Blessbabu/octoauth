
function checkURL() {
    url = location.hash.replace(/^#/, ""), 
    container = $("#content-wrapper"),
    url ? ( 
    title = $('#side-menu a[href="' + url + '"]').attr("title"),
     document.title = title || document.title, 
     loadURL(url, container)) : ($this = $('#side-menu > ul > li:first-child > a[href!="#"]'), window.location.hash = $this.attr("href"))
}

function loadURL(t, a) {
    $.ajax({
        type: "GET",
        url: t,
        dataType: "html",
        cache: !0,
        beforeSend: function() {
            a.html('<h1 style="text-align:center;"><i class="fa fa-cog fa-spin"></i> Loading...</h1>')
        },
        success: function(t) {
            a.css({
                opacity: "1.0"
            }).html(t).delay(50).animate({
                opacity: "1.0"
            }, 300)
        },
        error: function() {
            a.html('<h4 style="margin-top:10px; display:block; text-align:center"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>')
        },
        async: !1
    })
}
console.log("Adding load Inner HTML");
var loadInnerHTMLPage = function(){
$.root_ = $("body"), 
$.navAsAjax = !0, 
$.navAsAjax && ($("#side-menu").length && checkURL(), $(document).on("click", '#side-menu a[href!="#"]', function(t) {
    t.preventDefault(), $this = $(t.currentTarget), window.setTimeout(function() {
        window.location.hash = $this.attr("href")
    }, 150)
}), 
$(document).on("click", '#side-menu a[target="_blank"]', function(t) {

    t.preventDefault(), $this = $(t.currentTarget), window.open($this.attr("href"))
}),
 $(document).on("click", '#side-menu a[target="_top"]', function(t) {
    t.preventDefault(), $this = $(t.currentTarget), window.location = $this.attr("href")
}), $(document).on("click", '#side-menu a[href="#"]', function(t) {
    t.preventDefault()
}), $(window).on("hashchange", function() {
    checkURL()
}));
}