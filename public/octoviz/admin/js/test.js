//RENDER GUIDE FOR JS RENDER TEMPLATE NAMINGS - CHANNEL
//pageFbDivRender -> channelYtDivRender
//pageLinkFbRender -> channelLinkYtRender
//pageLinkFb -> channelLinksYt
//fbAddBtn -> channelYtAddBtn
//fbremoveBtn -> channelYtRemoveBtn

//INSTANCE 01 CHANNEL
//INSIDE getUserDocument() function  lineNumber 144 in commonCalls.js
channelLinksYtArray = [];
if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].youtube)) { // line number 223
    var indexCyt = 0; //initialising index for channel count
	var renderChannelYtArray = []; // initialising array for using jsrender
	channelLinksYtArray = globalConfig._source[diff[0]].youtube.channels;  // loading user saved channels to channelLinksYtArray
	
	for (indexCyt; indexCyt <= channelLinksYtArray.length; indexCyt++) { //preparing the render array
                renderChannelYtArray.push({
                    link: channelLinksYtArray[indexCyt]
                })
            }
	
	 $("#channelYtForm").html($("#channelYtLinkRender").render(renderChannelYtArray));
	
}

//INSTANCE 02 CHANNEL
//INSIDE $('body').on('click', '.editQuery', function() {} in commonCalls
channelLinksYtArray = []; //emptying the channel links holder
//INSIDE ClientAdmin.updateGet(user, function(response) {}) 
//INSIDE if (!isNull(response._source[liSelect].youtube)) {}
var indexCyt = 0;
            var renderChannelYtArray = [];
			
			if(isNull(response._source[liSelect].youtube.channels)){
				channelLinksYtArray =[];
			}
			else{
				channelLinksYtArray = response._source[liSelect].youtube.channels;
			}
            console.log("yt channel array", channelLinksYtArray);
            for (indexCyt; indexCyt <= channelLinksYtArray.length; indexCyt++) {
                renderChannelYtArray.push({
                    link: channelLinksYtArray[indexCyt]
                })
            }
			$("#channelYtForm").html($("#channelYtLinkRender").render(renderChannelYtArray));



//INSTANCE 03 CHANNEL
//INSIDE $('body').on('click', '.allTags', function() {}
channelLinksYtArray = []; //emptying the channel links holder
//INSIDE esClientAdmin.updateGet(user, function(response) {}
//INSIDE if (!isNull(response._source[liSelect].youtube)) {}
var indexCyt = 0;
            var renderChannelYtArray = [];
			
			if(isNull(response._source[liSelect].youtube.channels)){
				channelLinksYtArray =[];
			}
			else{
				channelLinksYtArray = response._source[liSelect].youtube.channels;
			}
            console.log("yt channel array", channelLinksYtArray);
            for (indexCyt; indexCyt <= channelLinksYtArray.length; indexCyt++) {
                renderChannelYtArray.push({
                    link: channelLinksYtArray[indexCyt]
                })
            }
			$("#channelYtForm").html($("#channelYtLinkRender").render(renderChannelYtArray));


//INSTANCE 04 CHANNEL
//INSIDE var tagsAdditionSuccessShow = function(tagObject, tagsArray) {}
			$("#channelYtForm").html($("#channelYtLinkRender").render(["1"])); //THIS IS FOR NEW TAGS ADDITION

//INSTANCE 05 CHANNEL
$("body").on("click", ".channelYtAddBtn", function(e) {
    e.preventDefault();
 
    var n = channelLinksYtArray.length + 1;


    channelYtField = $(this).parent().parent().find(".channelLinksYt").val();
    //alert(channelYtField);
    var data = {
        "page": channelYtField
    };
    var structure = $(' <ul class="vertical-list"> <li class="channelYtDivRender' + n + '"><span class="col-md-2"> <label class="control-label" id="pageLink" for="username">Page Link</label></span><span class="col-md-7"><input name="username"  type="text" class="channelLinksYt form-control"></span><span class="col-md-1"><button class="btn btn-primary channelYtAddBtn "> <img src="img/follower.gif"  class="loading_image" style="display: none;"><span class="fa fa-plus-circle addLikefb"></span></button></span><span><button class="btn btn-danger channelYtRemoveBtn" indexVal=' + n + ' ><span class="fa fa-times"></span></button></span></li></ul>');

    //alert( $(this).find("#loading_image").html());
    //$(this).children('.loading_image').show();
    var loader = $(this).children('.loading_image');
    var fbFont = $(this).children('.addLikefb');
    loader.show();
    channelLinksYtArray.push(channelYtField);
    var id = "channelYtFormat";
    updateFn("configs", "config", user, id);
    loader.hide();
    fbFont.removeClass("fa-plus-circle").addClass("fa-check");
    $(this).addClass("disabled");
    $('#channelYtForm').append(structure);


});


//INSTANCE 06
$("body").on("click", ".channelYtRemoveBtn", function(e) {
    e.preventDefault();
    var index = $(this).attr("indexVal");
    if (index > -1) {
        channelLinksYtArray.splice(index, 1);
        var id = "channelYtFormat";
        updateFn("configs", "config", user, id);
        $('.channelYtDivRender' + index).remove();
    }
});

//YOUTUBE LINK RENDER
//channelYtLinkRender -> videoYtLinkRender
//channelYtDivRender  -> videoYtDivRender
//channelLinkYtRender -> videoLinkYtRender
//channelLinksYt     ->  videoLinksYt
//channelYtAddBtn    ->  videoYtAddBtn
//channelYtRemoveBtn  -> videoYtRemoveBtn
//RENDER INSTANCE
	


//INSTANCE 01 LINK
//INSIDE getUserDocument() function  lineNumber 144 in commonCalls.js
videoLinksYtArray = [];
if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].youtube)) { // line number 223
    var indexVidLink = 0; //initialising index for video link count
	var renderVideoYtArray = []; // initialising array for using jsrender
	videoLinksYtArray = globalConfig._source[diff[0]].youtube.links;  // loading user saved video links to videoLinksYtArray
	
	for (indexVidLink; indexVidLink <= videoLinksYtArray.length; indexVidLink++) { //preparing the render array
                renderVideoYtArray.push({
                    link: videoLinksYtArray[indexVidLink]
                })
            }
	
	 $("#linkYtForm").html($("#videoYtLinkRender").render(renderVideoYtArray));
	
	indexVidLink =0;
			for(indexVidLink;indexVidLink<videoLinksYtArray.length;indexVidLink++){
				    $('.videoLinkYtRender' + indexVidLink + '').val(videoLinksYtArray[indexVidLink]);
			}
	
}

//INSTANCE 02 LINK
//INSIDE $('body').on('click', '.editQuery', function() {} in commonCalls
videoLinksYtArray = []; //emptying the channel links holder
//INSIDE ClientAdmin.updateGet(user, function(response) {}) 
//INSIDE if (!isNull(response._source[liSelect].youtube)) {}
			var indexVidLink = 0;
            var renderVideoYtArray = [];
			
			if(isNull(response._source[liSelect].youtube.links)){
				videoLinksYtArray =[];
			}
			else{
				videoLinksYtArray = response._source[liSelect].youtube.links;
			}
            console.log("yt channel array", videoLinksYtArray);
            for (indexVidLink; indexVidLink <= videoLinksYtArray.length; indexVidLink++) {
                renderVideoYtArray.push({
                    link: videoLinksYtArray[indexVidLink]
                })
            }
	 $("#linkYtForm").html($("#videoYtLinkRender").render(renderVideoYtArray));
	indexVidLink =0;
			for(indexVidLink;indexVidLink<videoLinksYtArray.length;indexVidLink++){
				    $('.videoLinkYtRender' + indexVidLink + '').val(videoLinksYtArray[indexVidLink]);
			}


//INSTANCE 03 LINK
//INSIDE $('body').on('click', '.allTags', function() {}
videoLinksYtArray = []; //emptying the channel links holder
//INSIDE esClientAdmin.updateGet(user, function(response) {}
//INSIDE if (!isNull(response._source[liSelect].youtube)) {}
var indexVidLink = 0;
            var renderVideoYtArray = [];
			
			if(isNull(response._source[liSelect].youtube.links)){
				videoLinksYtArray =[];
			}
			else{
				videoLinksYtArray = response._source[liSelect].youtube.links;
			}
            console.log("yt video links array", videoLinksYtArray);
            for (indexVidLink; indexVidLink <= videoLinksYtArray.length; indexVidLink++) {
                renderVideoYtArray.push({
                    link: videoLinksYtArray[indexVidLink]
                })
            }
	 $("#linkYtForm").html($("#videoYtLinkRender").render(renderVideoYtArray));

indexVidLink =0;
			for(indexVidLink;indexVidLink<videoLinksYtArray.length;indexVidLink++){
				    $('.videoLinkYtRender' + indexVidLink + '').val(videoLinksYtArray[indexVidLink]);
			}


//channelYtLinkRender -> videoYtLinkRender
//channelYtDivRender  -> videoYtDivRender
//channelLinkYtRender -> videoLinkYtRender
//channelLinksYt     ->  videoLinksYt
//channelYtAddBtn    ->  videoYtAddBtn
//channelYtRemoveBtn  -> videoYtRemoveBtn
//
//channelLinksYtArray -> videoLinksYtArray



//INSTANCE 04 LINK
//INSIDE var tagsAdditionSuccessShow = function(tagObject, tagsArray) {}
			$("#linkYtForm").html($("#videoYtLinkRender").render(["1"])); //THIS IS FOR NEW TAGS ADDITION


//INSTANCE 05 CHANNEL
$("body").on("click", ".videoYtAddBtn", function(e) {
    e.preventDefault();
 
    var n = videoLinksYtArray.length + 1;


    videoLinkYtField = $(this).parent().parent().find(".videoLinksYt").val();
    //alert(videoLinkYtField);
    var data = {
        "page": videoLinkYtField
    };
    var structure = $(' <ul class="vertical-list"> <li class="videoYtDivRender' + n + '"><span class="col-md-2"> <label class="control-label" id="pageLink" for="username">Video Link</label></span><span class="col-md-7"><input name="username"  type="text" class="videoLinksYt form-control"></span><span class="col-md-1"><button class="btn btn-primary videoYtAddBtn "> <img src="img/follower.gif"  class="loading_image" style="display: none;"><span class="fa fa-plus-circle addLikefb"></span></button></span><span><button class="btn btn-danger videoYtRemoveBtn" indexVal=' + n + ' ><span class="fa fa-times"></span></button></span></li></ul>');

    //alert( $(this).find("#loading_image").html());
    //$(this).children('.loading_image').show();
    var loader = $(this).children('.loading_image');
    var fbFont = $(this).children('.addLikefb');
    loader.show();
    videoLinksYtArray.push(videoLinkYtField);
    var id = "videoLinkYtFormat";
    updateFn("configs", "config", user, id);
    loader.hide();
    fbFont.removeClass("fa-plus-circle").addClass("fa-check");
    $(this).addClass("disabled");
    $('#linkYtForm').append(structure);


});


//INSTANCE 06
$("body").on("click", ".videoYtRemoveBtn", function(e) {
    e.preventDefault();
    var index = $(this).attr("indexVal");
    if (index > -1) {
        videoLinksYtArray.splice(index, 1);
        var id = "videoLinkYtFormat";
        updateFn("configs", "config", user, id);
        $('.videoYtDivRender' + index).remove();
    }
});








