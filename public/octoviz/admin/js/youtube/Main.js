$(document).ready(function() {
    $('.key').tooltip();
});


$('.main-search').keypress(function(e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});


//SORT BY CATEGORIES FOR VIDEOS
$('body').on('click', '.sortPicker', function () {
    $('#filter-result-yt').empty();
	$(".sortPicker").removeClass("selectedGran");
	$(this).addClass("selectedGran");
	sortIdentifier = $(this).attr("sortIdentifier");
	globalObject.sortIdentifier = $(this).attr("sortIdentifier");
	// alert(globalObject.sortIdentifier);
	globalObject.initTab();
});


//GRANULARITY CHANGER FOR GRAPH
$('body').on('click', '.granularityPickerYt', function () {
	// var currentGranularity = $(this).attr("gValue");
	// $('.granularityTitle span:first').html(currentGranularity);
	$(".granularityPickerYt").removeClass("selectedGran");
	$(this).addClass("selectedGran");
	var granularityValue = $(this).attr("granularityvalue");
	globalObject.liveParametersYt.granularity = granularityValue;
	globalObject.initTab();
});


//SIZE PICKER CHANNELS
$('body').on('click', '.sizePickerYt', function() {
    $(".sizePickerYt").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    globalObject.liveParametersYt.size = sizeValue;
	globalObject.initTab();
});


//DOWNLOAD CSV DATA
$('body').on('click', '.downloadLinkYt', function (e) {
	e.preventDefault();
	var downloadToken = $(this).attr('downloadTokenYt');
//	alert(downloadToken);
	var blob = new Blob([globalObject.csvYt[downloadToken]], {
		type: "text/plain;charset=utf-8"
	});
	var saveName = null;
	if(downloadToken=="channelCount"){
		saveName="Influencer Channels.csv"
	}
	else if (downloadToken=="statsCSV"){
		saveName="Video Statistics.csv"
	}
	saveAs(blob, saveName);
});


//REMOVE FILTER TAGS
$('body').on('click', '.selectedTagYt', function() {
    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClientYt.removeTermMatch(field, value);
    globalObject.conductSearch();
//	alert($("#filter-result-yt li").length == 0)
    $(this).remove();
});


//FILTER TAG ADDITION
$('body').on('click', '.checkbox', function() {
    var testingMn = $(this).children(".s1").attr("id");
    var testingMp = $(this).children(".s2").attr("id");
    var testingTt = $(this).children(".s3").attr("id");
    var identifier = $(this).attr('fieldType');

    if (($("#" + testingMn).is(':checked')) ||
        ($("#" + testingMp).is(':checked')) ||
        ($("#" + testingTt).is(':checked'))) {

        var name = $(this).attr('value');
        globalObject.tagAddFunctionYt({
            "field": "snippet.channelTitle.raw",
            "value": name
        }, identifier);
		
		esClientYt.addTermMatch("snippet.channelTitle.raw", name);
		globalObject.conductSearch();
     }
});
