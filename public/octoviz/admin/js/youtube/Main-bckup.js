var liveParametersYt = {
    pageNum: 0,
    granularity: null
};
//var pageSize = 10;
//var isFirstSearch = true;
var csvYt = {
    statsCSV: "",
    videoDetails: null,
    channelCount: null,
    graphYt: null
};

//var chartYt = null;
//var videoStatsLoading = false;

//var searchFieldYt = null;
var esClientYt = null;
var sortIdentifier = "statistics.viewCount";
var youtubeCharts=[];

function initOrReset() {
    //	alert(user);
    //	alert(globalTagValue);
    //	globalTagValue= (globalTagValue == null ? globalTagValue : globalTagValue) ;
    esClientYt = new Elasticsearch(esHost + '/youtube/video', generateYtMainQuery, liveParametersYt);
    esClientYt.addTermMatch("ownedBy.name", user.toLowerCase());
    esClientYt.addTermMatch("ownedBy.query", globalTagValue.toLowerCase());
    esClientYt.addDateRangeMatch("snippet.publishedAt", getFromString(), null);
	applyScoring(esClientYt, sortIdentifier, "youtube");
    conductSearchYt();
}


initOrReset();

globalFunctionChangeYt = function(globalTagValue) {
//	initOrReset();
    
    esClientYt.removeTermMatch("ownedBy.query");
    esClientYt.addTermMatch("ownedBy.query", globalTagValue.toLowerCase());
    conductSearchYt();
}

$(document).ready(function() {
    $('.key').tooltip();
});


//$('#submit-search').on('click', function() {
//    if (isFirstSearch) {
//        disableTimeRange(esClientYt);
//        isFirstSearch = false;
//    }
//    searchFieldYt = $(".main-search").val();
//    cp = 1;
//    esClientYt.addQueryString("_all", searchFieldYt);
//    conductSearchYt();
//});


$('body').on('click', '.sortPicker', function () {
	$(".sortPicker").removeClass("selectedGran");
	$(this).addClass("selectedGran");
	sortIdentifier = $(this).attr("sortIdentifier");
	initOrReset();
});


$('.main-search').keypress(function(e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});


$('body').on('click', '.granularityPickerYt', function () {
	var currentGranularity = $(this).attr("gValue");
	$('.granularityTitle span:first').html(currentGranularity);
	$(".granularityPicker").removeClass("selectedGran");
	$(this).addClass("selectedGran");
	var granularityValue = $(this).attr("granularityvalue");
	liveParametersYt.granularity = granularityValue;
	generateYtMainQuery(liveParametersYt);
	conductSearchYt();
});

$('body').on('click', '.sizePickerYt', function() {
    $(".sizePickerYt").removeClass("selectedGran");
    $(this).addClass("selectedGran");
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    liveParametersYt.size = sizeValue;
	generateYtMainQuery(liveParametersYt);
	conductSearchYt();
});

function initTabYt() {
    //	alert('inside yt');
    conductSearchYt();
};


onTimeChange = function(globalDuration) {
    esClientYt.addDateRangeMatch("snippet.publishedAt", getFromString(), null);
    conductSearchYt();
}

function disableTimeRange(client) {
    client.removeDateRange('created_at');
    $(".filter-list").find(".active").removeClass("active");
};


function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

//function queryFunctionYt() {
//	conductSearchYt();	
//};

function emptyFunctionsYt () {
    $('#ytStats').empty();
	$('#videoCount').empty();
	$('#views').empty();
	$('#likes').empty();
	$('#dislikes').empty();
	$('#favorites').empty();
	$('#comments').empty();
}

onSearchYt = function(searchField){

			esClientYt.addQueryString("_all", searchField);
			conductSearchYt();
}

onTimeChangeYt = function(globalDuration,toDate) {
  
    esClientYt.addDateRangeMatch("snippet.publishedAt","now-" + globalDuration, toDate);
    conductSearchYt();
}


function conductSearchYt() {
    //     alert('inside conduct search yt');
    handleLoadingYt(true);
	emptyFunctionsYt();
    esClientYt.search(singleRenderFuntionYt);
};

var convertBucketsToCSVChannelCount = function (hits) {
    var csv = "";
    for (var hitIndex in hits) {
        var hit = hits[hitIndex];
        csv += "www.youtube.com/channel/"+hit.key + "," + hit.doc_count + "\n";
    }
	console.log('csv',csv);
    return csv;
};

var singleRenderFuntionYt = function(response) {
    // alert('inside srf yt');
    
    csvYt.channelCount = "Channel Links\n" +
   convertBucketsToCSVChannelCount(response.aggregations.channels.buckets);
    youtubeCharts=[];
    console.log('response yt is', response);
   
 
    graphYt(response);
    statisticsTableData(response);
    tableTrendingChannel(response);
//	$('#ytStats').isotope({
//	    itemSelector: '.jango',
//        layoutMode : 'masonry'
//	});
    var totalHitsPn = response.hits.total;
    if (isPaginationTriggered == false) {
        addPaginationYt(totalHitsPn);
    }
    isPaginationTriggered = false;
     $('.column').matchHeight();
  

  
};

function handleLoadingYt(isLoading) {
    if (isLoading==true) {
    $(".dataContentYtTrends").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
    $(".dataContentYtInfluencers").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
    $(".dataContentYtVideos").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

    } 
    
}

$('body').on('click', '.downloadLinkYt', function (e) {
	e.preventDefault();
	var downloadToken = $(this).attr('downloadTokenYt');
//	alert(downloadToken);
	var blob = new Blob([csvYt[downloadToken]], {
		type: "text/plain;charset=utf-8"
	});
	saveAs(blob, "downloadToken.csv");
});



var tagAddFunctionYt = function(element, identifier) {
    //    var className = "tag";
    //	 if (identifier == "tag") {
    //
    //    } else if (identifier == "source") {
    //        className = "tagInf";
    //    }
    $('#filter-result-yt').append('<li class="blue tags selectedTag" field =' + element.field + ' fieldValue="' + element.value + '"><a>' + element.value + '<span data-role="remove">x</span></a></li>').html();

};
$('body').on('click', '.selectedTag', function() {

    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClientYt.removeTermMatch(field, value);
    conductSearchYt();
//	alert($("#filter-result-yt li").length == 0)
    $(this).remove();
});
$('body').on('click', '.checkbox', function() {
    var testingMn = $(this).children(".s1").attr("id");
    var testingMp = $(this).children(".s2").attr("id");
    var testingTt = $(this).children(".s3").attr("id");
    var identifier = $(this).attr('fieldType');

    if (($("#" + testingMn).is(':checked')) ||
        ($("#" + testingMp).is(':checked')) ||
        ($("#" + testingTt).is(':checked'))) {

        var name = $(this).attr('value');
        tagAddFunctionYt({
            "field": "snippet.channelTitle.raw",
            "value": name
        }, identifier);
        esClientYt.addTermMatch("snippet.channelTitle.raw", name);
        conductSearchYt();
    }

});



var printFunctionYt = function(response) {
    $("#views").html(response.aggregations.viewCount.value);
    $("#likes").html(response.aggregations.likeCount.value);
    $("#dislikes").html(response.aggregations.disLikeCount.value);
    $("#favorites").html(response.aggregations.favoriteCount.value);
    $("#comments").html(response.aggregations.commentCount.value);
    $("#videoCount").html(response.hits.total);
    $('span.numbers').digits();

};
var graphYt = function(response) {

    var data = []
    var viewTrend = [];
    for (var bucketIndex in response.aggregations.trends.buckets) {
        var bucket = response.aggregations.trends.buckets[bucketIndex].key;

        var date = new Date(bucket);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        date = day + '-' + month + '-' + year;

        viewTrend.push({
            date: date,
            views: (response.aggregations.trends.buckets[bucketIndex].doc_count)
        });
    }

    pageLoadYt(viewTrend);
};

function pageLoadYt(data) {
    if(data.length<1){
    $(".dataContentYtTrends").html("No data Available");   
    }
    else{
    $(".dataContentYtTrends").empty();  
    }
   
    var chart = AmCharts.makeChart("ytViewTrends", {
        "type": "serial",
        "theme": "light",
        "marginRight": 30,
        // "legend": {
        //     "equalWidths": false,
        //     "periodValueText": "total: [[value.sum]]",
        //     "position": "top",
        //     "valueAlign": "left",
        //     "valueWidth": 100
        // },
        "dataProvider": data,

        "graphs": [{
            "balloonText": "<span><b>Views :</b></span><span style='font-size:14px; color:#000000;'><b>[[views]]</b></span>",


            "fillAlphas": 0.6,
            "lineAlpha": 0.4,
            "title": "Views",
            "fillColors": "#50b6b6",
            "valueField": "views"
        }],
        "plotAreaBorderAlpha": 0,
        "marginTop": 10,
        "marginLeft": 0,
        "marginBottom": 0,
        "chartCursor": {
            "cursorAlpha": 0
        },
        "categoryField": "date",
        "categoryAxis": {
            "startOnAxis": true,
            "axisColor": "#DADADA",
            "gridAlpha": 0.07
            

        },
        "export": {
          
            "enabled": true,
         "libs": {
            "path": "http://amcharts.com/lib/3/plugins/export/libs/"
        }
        }
    });
   youtubeCharts.push(chart);
};

//function visualizeData(data) {
//    nv.addGraph(function() {
//        var width = 600, height = 400;
//        chart = nv.models.multiBarChart().x(function(d) {
//            return d.x;
//        }).y(function(d) {
//            return d.y;
//        }).color(['#aec7e8', '#7b94b5', '#486192']).stacked(true)
//        //.margin({top:150,right:150,bottom:150,left:150})
//        .width(width).height(height);
//
//        chart.multibar.hideable(false);
//
//        chart.xAxis.showMaxMin(true).tickFormat(d3.format(',f'));
//
//        chart.yAxis.tickFormat(d3.format(',.1f'));
//
//        d3.select('#chart svg').datum(data).transition().duration(500).call(chart).style({ 'width': width, 'height': height });
//
//        nv.utils.windowResize(chart.update);
//
//        return chart;
//    });
//}

var tableTrendingChannel = function(response) {
    console.log("youtube table", response);


    var tableIndex = 0;
    var totalAggs = response.aggregations.channels.buckets;
    var totalAggsCount = totalAggs.length;
    var tdaChannel = [];
    for (tableIndex in totalAggs) {
        tdaChannel.push({
            channel: totalAggs[tableIndex].key,
            count: totalAggs[tableIndex].doc_count,
            channelLink: totalAggs[tableIndex].top_channel_hits.hits.hits[0]._source.snippet.channelId
        });
    }
     if(tdaChannel.length<1){
    $(".dataContentYtInfluencers").html("No data Available");   
    }
    else{
    $(".dataContentYtInfluencers").empty();  
    }
    $("#channelTable").html($("#tableChannel").render(tdaChannel.slice(0, 9)));
    // $("#channelTable2").html($("#tableChannel2").render(tdaChannel.slice(10, 19)));
    // $("#channelTable3").html($("#tableChannel3").render(tdaChannel.slice(20, 29)));

};

var cp = 1;
var pageClick = false;
var isPaginationTriggered = false;



var addPaginationYt = function(totalHitsPn) {
    $("#pagination").pagination({
        items: totalHitsPn,
        itemsOnPage: 10,
        cssStyle: 'dark-theme',
        currentPage: cp,
        displayedPages: 10,
        onPageClick: function(a) {
            cp = a;
            console.log(a);
            isPaginationTriggered = true;
            //			liveParametersYt.pageNum=((a-1)*10);
            esClientYt.addQueryParameter("pageNum", (cp - 1) * 10);
            conductSearchYt();
            return false;
        }
    });
};

var statisticsTableData = function(response) {
   
    console.log('youtube response is:', response);
    var hitIndex = 0;
    var totalHits = response.hits.hits;
    var totalHitsPn = response.hits.total;
    var ytStats = [];
    for (hitIndex in totalHits) {
        if (isNull(response.hits)) {
            alert("isnull");
            $("#ytStats").empty();
            continue;
        }
        var title = jQuery.trim(totalHits[hitIndex].fields['snippet.title'][0]).substring(0, 60);
        ytStats.push({
            channelName: totalHits[hitIndex].fields['snippet.channelTitle'][0],
            videoTitle: title,
            imgUrl: totalHits[hitIndex].fields['snippet.thumbnails.high.url'][0],
            videoURL: totalHits[hitIndex]._id,
            viewCount: totalHits[hitIndex].fields['statistics.viewCount'][0],
            likeCount: totalHits[hitIndex].fields['statistics.likeCount'][0],
            dislikeCount: totalHits[hitIndex].fields['statistics.dislikeCount'][0],
            favoriteCount: totalHits[hitIndex].fields['statistics.favoriteCount'][0],
            commentCount: totalHits[hitIndex].fields['statistics.commentCount'][0],
            publishedAt: utcToDateFormatReverse(totalHits[hitIndex].fields['snippet.publishedAt'][0])
        });
        $("#ytStats").html($("#statsContent").render(ytStats));
        //			$('span.numbers').digits();
    
        printFunctionYt(response);

//        $('.match-all').each(function() {
//            $(this).children('.column').matchHeight({});
//        });
		 

    }
     if(ytStats.length<1){
    $(".dataContentYtVideos").html("No data Available");   
    }
    else{
    $(".dataContentYtVideos").empty();  
    }
    console.log('ytStats', ytStats);

};


var alertFunction = function() {
    alert('inside alert function')
}

// function youtubeCreateReport() {
//  var images = [];
//       var pending = youtubeCharts.length;
//       for ( var i = 0; i < youtubeCharts.length; i++ ) {
//         var chart = youtubeCharts[ i ];
//         console.log(chart);
//         chart.export.capture( {}, function() {
//           this.toJPG( {}, function( data ) {
//             images.push( {
//               "image": data,
//               "fit": [ 523.28, 769.89 ]
//           } );
//             pending--;
//             if ( pending === 0 ) {
//               // all done - construct PDF
//               chart.export.toPDF( {
//                 content: images
//             }, function( data ) {
//                 this.download( data, "application/pdf", "amCharts.pdf" );
//             } );
//           }
//       } );
//       } );
//     }

//     }

var layoutyoutubeCharts = {
        /*
        ** Array of objects or strings which represents the output
        */
    content: [
        {
            text: "Youtube Trends",
            color:"#3A907F"
        }, {
            image: "image_1", // reference to the image mapping below
            fit: [523.28, 769.89] // fits the image to those dimensions (A4)
        }
         
         

    ],


    //  ** Mapping object, holds the actual imagery

    images: {

        }
}
function youtubeCreateReport() {
  var pdf_images = 0;
  var pdf_layout = layoutyoutubeCharts; // loaded from another JS file
  for (var i = 0; i <youtubeCharts.length; i++ ) {
     var chart =youtubeCharts[ i ];
    // Capture current state of the chart
    chart.export.capture( {}, function() {

      // Export to PNG
      this.toJPG( {
        // pretend to be lossless
         
        // Add image to the layout reference
      }, function( data ) {
        pdf_images++;
        pdf_layout.images[ "image_" + pdf_images ] = data;

        // Once all has been processed create the PDF
        if ( pdf_images == youtubeCharts.length ) {

          // Save as single PDF and offer as download
          this.toPDF( pdf_layout, function( data ) {
            this.download( data, "application/pdf", "amcharts.pdf" );
          } );
        }
      } );
    } );
  }
}
