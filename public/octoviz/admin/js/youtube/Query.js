var generateYtMainQuery = function (liveParametersYt) {
	console.log('live',liveParametersYt);
var size = liveParametersYt.size || 12;
    var granularity = liveParametersYt.granularity || "day";
    //var sortVariable = 	liveParametersYt.sortField || "likes.count";

    var queryBodyYt = {
        "from": liveParametersYt.pageNum,
        "size": size,
        "query": {},
		 "fields": [
				"statistics.likeCount",
				"statistics.dislikeCount",
				"statistics.viewCount",
				"statistics.commentCount",
				"statistics.favoriteCount",
				"publishedAt",
				"snippet.channelTitle.raw",
			 	"snippet.title",
			 	"snippet.channelTitle",
			 	"id",
			    "snippet.thumbnails.high.url",
			 	"snippet.publishedAt"
  			],
        "aggs": {
            "likeCount": {
                "sum": {
                    "field": "statistics.likeCount"
                }
            },
            "disLikeCount": {
                "sum": {
                    "field": "statistics.dislikeCount"
                }
            },
            "viewCount": {
                "sum": {
                    "field": "statistics.viewCount"
                }
            },
            "commentCount": {
                "sum": {
                    "field": "statistics.commentCount"
                }
            },
            "favoriteCount": {
                "sum": {
                    "field": "statistics.favoriteCount"
                }
            },
            "trends": {
                "date_histogram": {
                    "field": "publishedAt",
                    "interval": granularity
                }
            },
            "channels": {
                "terms": {
                    "field": "snippet.channelTitle.raw",
                    "size": size
                },
                "aggs": {
                    "top_channel_hits": {
                        "top_hits": {}
                    }
                }
            }
        }

    };

    return queryBodyYt;

};