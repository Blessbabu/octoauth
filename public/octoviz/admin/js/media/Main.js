//GRANULARITY CHANGER FOR GRAPH
$('body').on('click', '.granularityPickerMedia', function() {
    // var currentGranularity = $(this).attr("gValue");
    // $('.granularityTitle span:first').html(currentGranularity);
    var granularityValue = $(this).attr("granularityvalue");
	$(".granularityPickerMedia").removeClass("selectedGran");
	$(this).addClass("selectedGran");
    globalObject.liveParametersMedia.granularity = granularityValue;
//    generateMediaMainQuery(liveParametersMedia);
    globalObject.initTab();
});

//SIZE PICKER MEDIA
$('body').on('click', '.sizePickerMedia', function() {
    var identifier = $(this).attr("sizeIdentifier");
	$('.sizePickerMedia').removeClass("selectedGran");
	$(this).addClass("selectedGran");
    var sizeValue = $(this).attr("sizeValue");
    globalObject.liveParametersMedia.size = sizeValue;
    globalObject.initTab();
});

$('body').on('click', '.sortPickerMedia', function() {
    // alert($(this).html());
    var identifier = $(this).attr("sortIdentifier");
    $('.sortPickerMedia').removeClass("selectedGran");
    $(this).addClass("selectedGran");
    // alert(identifier);
    // var sizeValue = $(this).attr("sizeValue");
    globalObject.liveParametersMedia.sort = identifier;
    globalObject.initTab();
});

//FILTER TAG ADDITION
$('body').on('click', '.sourceCb', function() {
    var testingMn = $(this).children(".s1").attr("id");
    var testingMp = $(this).children(".s2").attr("id");
    var testingTt = $(this).children(".s3").attr("id");

    var identifier = $(this).attr('fieldType');
    if (($("#" + testingMn).is(':checked')) ||
        ($("#" + testingMp).is(':checked')) ||
        ($("#" + testingTt).is(':checked'))) {
        var name = $(this).attr('value');
        var fieldName = $(this).attr('fieldType');
        globalObject.checkBoxClickCalls(name, fieldName);
    }
});

//REMOVE FILTER TAGS
$('body').on('click', '.selectedTagMedia', function() {
    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClientMedia.removeTermMatch(field, value);
    globalObject.conductSearch();
    $(this).remove();
});

//DOWNLOAD CSV DATA
$('body').on('click', '.downloadLinkMedia', function (e) {
	e.preventDefault();
    var downloadToken = $(this).attr('downloadTokenMedia');
    var blob = new Blob([globalObject.csvMedia[downloadToken]], {
        type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "downloadToken.csv");
});

