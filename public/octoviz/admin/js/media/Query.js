var generateMediaMainQuery = function(liveParametersMedia){
var size = liveParametersMedia.size || 100;

var granularity = liveParametersMedia.granularity || "hour";
var queryBodyMedia = {
  "from": liveParametersMedia.pageNum,
  "size":size,
  "fields": [
    "Title",
    "actualTimeStamp",
    "SourceName",
    "Link",
    "Tags",
    "Categories.SentimentOut.overallSentiment",
    "Categories.Types.Entities.Person.entities.entity",
    "Categories.Types.Entities.Country.entities.entity",
    "Categories.Types.Entities.Company.entities.entity",
    "Categories.Types.Entities.Regions.entities.entity",
    "Categories.Types.Entities.City.entities.entity",
    "Categories.Types.Entities.State.entities.entity"
  ],
  "highlight": {
    "fields": {
      "Content": {},
      "Title": {}
    }
  },
  "aggs": {
    "sources": {
      "terms": {
        "field": "SourceName",
        "size": size
      }
    },
    "trends": {
      "date_histogram": {
        "field": "actualTimeStamp",
        "interval": granularity
      }
    },
    "pieChartSentiMedia": {
      "terms": {
        "field": "Categories.SentimentOut.overallSentiment",
        "size": 30
      }
    },
    "country": {
      "terms": {
        "field": "Categories.Types.Entities.Country.entities.entity",
        "size": 30
      }
    }
  },
  "query": {
    "function_score": {
      "score_mode": "sum",
      "boost_mode": "sum",
      "functions": [
        {
          "filter": {
            "terms": {
              "_id": {
                "index": "newsentiments",
                "type": "relevancy",
                "id": user,
                "path": "relevant.media",
                "cache": false
              }
            }
          },
          "weight": 1000
        },
        {
          "filter": {
            "terms": {
              "_id": {
                "index": "newsentiments",
                "type": "relevancy",
                "id": user,
                "path": "irrelevant.media",
                "cache": false
              }
            }
          },
          "weight": -1000
        }
      ],
      "filter": {
        "bool": {
          "must_not": [
            {
              "terms": {
                "_id": {
                  "index": "newsentiments",
                  "type": "relevancy",
                  "id": user,
                  "path": "hide.media",
                  "cache": false
                }
              }
            }
          ]
        }
      }
    }
  }
}
	
	;
	return queryBodyMedia;
	
};

