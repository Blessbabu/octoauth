var clickedFiltersMedia = [];
var isFirstSearch = true;
var csvMedia = {
    mediaCount: "",
    linkCount: "",
    graphMedia: "",
    personCount: "",
    countryCount: "",
    companyCount: "",
    regionCount: "",
    cityCount: "",
    stateCount: ""
};


//dataContentMediaMap,dataContentMediaPie,dataContentMediaTrends,dataContentMediaSource,dataContentMediaNews
var liveParametersMedia = {
    granularity: null,
    size: null
};
var mediaCharts = [];

var searchFieldMedia = null;
var timeField = "actualTimeStamp";
//var esClientMedia = new Elasticsearch( esMediaHost + '/algotree', generateMediaMainQuery, liveParametersMedia);
var esClientMedia = null;
var sampleKeywords = ["modi", "bjp"];

function resetOrInit() {
    //    alert(globalTagValue);
    if (esClientMedia == null) {
        esClientMedia = new Elasticsearch(esMediaHost + '/algotree', generateMediaMainQuery, liveParametersMedia);
    } else {
        esClientMedia.reset();
    }
    $("#main-search").val("");
    esClientMedia.addDateRangeMatch(timeField, "now-" + globalDuration, null);
      var keywords = getKeywords(globalTagValue);
      esClientMedia.addMultiMatch(["Title", "Content"], keywords.join(" "));
     // esClientMedia.addMultiMatch(["Title", "Content"], sampleKeywords.join(" "));
    applyScoring(esClientMedia, "actualTimeStamp", "media");
}


var chartMedia = null;

function queryFunctionMedia() {
    conductSearchMedia();
};

function disableTimeRange(client) {
    client.removeDateRange('actualTimeStamp');
    $(".filter-list").find(".active").removeClass("active");
}



globalFunctionChangeMedia = function(globalTagValue) {
    //esClientMedia.addTermLookupMatch("Content","configs","config",user, globalTagValue + ".media.keywordsMedia");
    resetOrInit();
	keywords = getKeywords(globalTagValue);
    esClientMedia.addMultiMatch(["Title", "Content"], keywords.join(" "));
    emptyPieChartValuesOnChangeMedia();
    conductSearchMedia();
}


function isNull(obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
}

function initTabMedia() {
    resetOrInit();
    conductSearchMedia();
};


function conductSearchMedia(isStreaming) {
//    	alert('inside conduct search');
        if (isNull(isStreaming)) {
            handleLoadingMedia(true);
        }
    esClientMedia.search(singleRenderFuntionMedia);
}

var singleRenderFuntionMedia = function(response) {
     mediaCharts=[];
    console.log('media response is', response);
    $('#chartIDMedia').empty();
    $('#pieIDMedia').empty();
	$(".dataContentMedia").html('');
   
	handleLoadingMedia(false);
    tableTrendingCountry(response);  //map
    graphDataMedia(response);		 //bar
    graphDataSentiMedia(response);   //pie
    tableTrendingSource(response);   //source table
    highlightContent(response);      //news
	slimScrollCall("slimScrollTable","250px");

    $('.column').matchHeight();

};







onSearchMedia = function(searchField) {
    esClientMedia.addQueryString("Content", searchField);
    conductSearchMedia();
}

onTimeChangeMedia = function(globalDuration, toDate) {
    esClientMedia.addDateRangeMatch(timeField, "now-" + globalDuration, toDate);
    conductSearchMedia();
};


function handleLoadingMedia(isLoading) {

    if (isLoading) {
//dataContentMediaMap,dataContentMediaPie,dataContentMediaTrends,dataContentMediaSource,dataContentMediaNews

        $(".dataContentMediaMap").empty();
        $(".dataContentMediaPie").empty();
        $(".dataContentMediaTrends").empty();
        $(".dataContentMediaSource").empty();
        $(".dataContentMediaNews").empty();
        $(".dataContentMediaMap").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $(".dataContentMediaPie").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $(".dataContentMediaTrends").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $(".dataContentMediaSource").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');
        $(".dataContentMediaNews").html('<span class="dataLoader"></span><span class="dataText">Loading..</span>');

    } else {
        $(".dataContentMediaMap").empty();
        $(".dataContentMediaPie").empty();
        $(".dataContentMediaTrends").empty();
        $(".dataContentMediaSource").empty();
        $(".dataContentMediaNews").empty();
    }
}




$('#submit-search').on('click', function() {
    searchFieldMedia = $("#main-search").val();
    esClientMedia.addQueryString("Content", searchFieldMedia);
    emptyPieChartValuesOnChangeMedia();
    conductSearchMedia();
});

$('#main-search').keypress(function(e) {
    if (e.keyCode == 13)
        $('#submit-search').click();
});

$('body').on('click', '.granularityPickerMedia', function() {
    var currentGranularity = $(this).attr("gValue");
    $('.granularityTitle span:first').html(currentGranularity);
    var granularityValue = $(this).attr("granularityvalue");
    liveParametersMedia.granularity = granularityValue;
    generateMediaMainQuery(liveParametersMedia);
    conductSearchMedia();
});


onTimeChange = function(globalDuration) {
    esClientMedia.addDateRangeMatch(timeField, "now-" + globalDuration, "now");
    emptyPieChartValuesOnChangeMedia();
    conductSearchMedia();
}

$('body').on('click', '#customButton', function() {
    var fromDate = $("#datepicker1").val();
    var toDate = $("#datepicker2").val();
    esClientMedia.addDateRangeMatch(timeField, fromDate, toDate);
    conductSearchMedia();
});

$('body').on('click', '.sizePickerMedia', function() {
    var identifier = $(this).attr("sizeIdentifier");
    var sizeValue = $(this).attr("sizeValue");
    //    alert(sizeValue);
    liveParametersMedia.size = sizeValue;
    generateMediaMainQuery(liveParametersMedia);
    conductSearchMedia();
});


var tableTrendingSource = function(response) {
    var tableIndex = 0;
    var totalAggs = response.aggregations.sources.buckets;
    var totalAggsCount = totalAggs.length;
	if(totalAggsCount == 0){
		$('.dataContentMediaSource').html('No data available');
	}
    var tdaSource = [];
    for (tableIndex in totalAggs) {
        tdaSource.push({
            source: totalAggs[tableIndex].key,
            count: totalAggs[tableIndex].doc_count
        });
    }
    $("#sourceTable").html($("#tableSource").render(tdaSource.slice(0, 9)));
    $("#sourceTable2").html($("#tableSource2").render(tdaSource.slice(10, 19)));
    $("#sourceTable3").html($("#tableSource3").render(tdaSource.slice(20, 29)));

};


var tableTrendingCountry = function(response) {

    var fieldType = "Categories.Types.Entities.Country.entities.entity";
    var tableIndex = 0;
    var totalAggs = response.aggregations.country.buckets;

    var totalAggsCount = totalAggs.length;
	if(totalAggsCount==0){
//	dataContentMediaPie,dataContentMediaTrends,dataContentMediaSource,dataContentMediaNews
		$('#chartMediaMap').empty();
		$('.dataContentMediaMap').html('No data available');
	}
    var tdaSource = [];
    for (tableIndex in totalAggs) {
        tdaSource.push({
            source: totalAggs[tableIndex].key,
            count: totalAggs[tableIndex].doc_count,
            fieldType: fieldType
        });
    }
    console.log("tdaSourceCountry", tdaSource);
    ConvertToCountryMedia(tdaSource);
};


var tagAddFunctionMedia = function(element) {
    $('#filter-result-media').append('<li class="blue tags selectedTagMedia" field =' + element.field + ' fieldValue="' + element.value + '"><a>' + element.value + '<span data-role="remove">x</span></a></li>').html();
};

$('body').on('click', '.selectedTag', function() {

    var field = $(this).attr('field');
    var value = $(this).attr('fieldValue');
    esClientMedia.removeTermMatch(field, value);
    conductSearchMedia();
    $(this).remove();
});


$('body').on('click', '.sourceCb', function() {
    var testingMn = $(this).children(".s1").attr("id");
    var testingMp = $(this).children(".s2").attr("id");
    var testingTt = $(this).children(".s3").attr("id");

    var identifier = $(this).attr('fieldType');


    if (($("#" + testingMn).is(':checked')) ||
        ($("#" + testingMp).is(':checked')) ||
        ($("#" + testingTt).is(':checked'))) {
        var name = $(this).attr('value');
        var fieldName = $(this).attr('fieldType');
        checkBoxClickCalls(name, fieldName);
    }

});



var checkBoxClickCalls = function(name, fieldName) {
    tagAddFunctionMedia({
        "field": fieldName,
        "value": name
    });
    esClientMedia.addTermMatch(fieldName, name);
    conductSearchMedia();
}

var mediaNames = [];

var graphDataMedia = function(response) {
    var keys = [];

    var totalTrends = [];
	var barBucketLength = response.aggregations.trends.buckets.length;
	
    for (var bucketIndex in response.aggregations.trends.buckets) {
        var bucket = response.aggregations.trends.buckets[bucketIndex].key;
        var current=new Date();
        var date = new Date(bucket);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
       var  dateValue = day + '-' + month + '-' + year;
          if(date<current)
            {
              totalTrends.push({

               date: dateValue,
               total: (response.aggregations.trends.buckets[bucketIndex].doc_count)
             });
           }
    }

    pageLoadMedia(totalTrends);

};

var dropTags = function(textArray) {
    var output = []
    for (var textIndex in textArray) {
        var text = textArray[textIndex];
        text = text.replace(/<em>/g, '###');
        text = text.replace(/<\/em>/g, '%%%%');
        text = text.replace(/<(?:.|\n)*?>/gm, '');
        text = text.replace(/###/g, '<em>');
        text = text.replace(/%%%%/g, '</em>');
        text = text.substring(1, 300);
        output.push(text);
    }
    return output;
}


var highlightContent = function(response) {
    console.log('highlight response is', response);
    $('.news-highlights-ul').empty();
    $('.newsLists').empty();
    var titleContent = [];
    var totalHitsPn = response.hits.total;
    var content = [];
    var buckets = response.hits.hits;
	
	if(buckets.length == 0){
		$('.dataContentMediaNews').html('No data available')
	}
	
    for (var index in buckets) {
			var titleBucket = buckets[index].fields.Title;
			var contentBucket = dropTags(buckets[index].highlight.Content);
        	var title = buckets[index].fields.Title[0];
		 	var titleHeadingContent = '<tr>'+
									'<td>'+'<h3><a href=' + buckets[index].fields.Link[0] + ' >' + buckets[index].fields.Title[0] + '</a></h3>' + 
									'<ul class="headingContent'+index+' contentBucket"></ul>'+ '</td>'+
									'<td><span class="posted-content">'+buckets[index].fields.actualTimeStamp[0]+'</span></td>'+
									'<td><span class="source-content">'+buckets[index].fields.SourceName+'</span></td>'+
									'<td><span class="id-sentiments'+index+'"><i class="identifier-sentiments'+index+'"></i>' + 													buckets[index].fields["Categories.SentimentOut.overallSentiment"] + '</span> </td>'+ '</tr>' ;
		
        	$('.news-highlights-ul').append(titleHeadingContent);
		
			if(buckets[index].fields["Categories.SentimentOut.overallSentiment"]=="POSITIVE"){
				  $(".identifier-sentiments"+index).addClass('ion-arrow-graph-up-right');
				  $(".id-sentiments"+index).addClass('news-positive');
			}
			if (buckets[index].fields["Categories.SentimentOut.overallSentiment"]=="NEGATIVE"){
				$(".identifier-sentiments"+index).addClass('ion-arrow-graph-down-right');
				  $(".id-sentiments"+index).addClass('news-negative');
			}
			if (buckets[index].fields["Categories.SentimentOut.overallSentiment"]=="MIXED"){
				$(".identifier-sentiments"+index).addClass('ion-shuffle');
				  $(".id-sentiments"+index).addClass('news-mixed');
			}
		
			for (var contentIndex in contentBucket) {
				var contentIndexNum = parseInt(contentIndex) + 1;
				var contentHighlight = '<li>'+'<i class="ion-android-done"></i>' + contentBucket[contentIndex] + '</li>';
				$('.headingContent' + index + '').append(contentHighlight);
			}

    }
    if (isPaginationTriggeredMedia == false) {
        addPaginationMedia(totalHitsPn);
    }
    isPaginationTriggeredMedia = false;
}



var cpMedia = 1;
var isPaginationTriggeredMedia = false;

var addPaginationMedia = function(totalHitsPn) {
    $("#media-pagination").pagination({
        items: totalHitsPn,
        itemsOnPage: 10,
        cssStyle: 'dark-theme',
        currentPage: cpMedia,
        displayedPages: 10,
        onPageClick: function(a) {
            cpMedia = a;
            console.log('clicked page is ', a);
            isPaginationTriggeredMedia = true;
            esClientMedia.addQueryParameter("pageNum", (cpMedia - 1) * 10);
            conductSearchMedia();
            return false;
        }
    });
};


var colorMapPieMedia = {
    Total: "#006e8a",
    positive: "#349292",
    negative: "#50b6b6",
    neutral: "#298080",
    mixed: "#2dd3d3"
};

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

var nameClassCode = ["ssm-pie-neutral", "ssm-pie-positive", "ssm-pie-negative", "ssm-pie-mixed"];
var nameClassValue = ["ssm-pie-neutral-val", "ssm-pie-positive-val", "ssm-pie-negative-val", "ssm-pie-mixed-val"];

var emptyPieChartValuesOnChangeMedia = function() {
    var count = 0;
    for (count; count < nameClassValue.length; count++) {
        $('.' + nameClassValue[count] + '').empty();
    }
}

var displayPieLegendsSenti = function(indexVal, sentiVal, docCount) {

    $('.' + nameClassCode[indexVal] + '').html(sentiVal);
    $('.' + nameClassCode[indexVal] + '').css('color', colorMapPieMedia[sentiVal]);
    $('.' + nameClassValue[indexVal] + '').html(commaSeparateNumber(docCount));
}

var graphDataSentiMedia = function(response) {

    console.log("graph data response", response);
    var graphData = [];
			//	dataContentMediaPie,dataContentMediaSource,dataContentMediaNews

    for (var bucketIndex in response.aggregations.pieChartSentiMedia.buckets) {
        var bucket = response.aggregations.pieChartSentiMedia.buckets[bucketIndex];
        if (bucket.key == "neutral") {
            // displayPieLegendsSenti(0, 'neutral', bucket.doc_count);
            graphData.push({
                key: "Neutral",
                color: colorMapPieMedia['neutral'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "positive") {
            // displayPieLegendsSenti(1, 'positive', bucket.doc_count);
            graphData.push({
                key: "Positive",
                color: colorMapPieMedia['positive'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "negative") {
            // displayPieLegendsSenti(2, 'negative', bucket.doc_count);
            graphData.push({
                key: "Negative",
                color: colorMapPieMedia['negative'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        } else if (bucket.key == "mixed") {
            // displayPieLegendsSenti(3, 'mixed', bucket.doc_count);
            graphData.push({
                key: "Mixed",
                color: colorMapPieMedia['mixed'],
                seriesIndex: bucketIndex,
                label: bucket.key,
                value: bucket.doc_count
            });
        }

    }
    console.log('graphData format', graphData);
    graphSentiChartMedia(graphData);
    //pageLoad(data);
};

function graphSentiChartMedia(graphData) {
	if(graphData.length<1){
	   $('.dataContentMediaPie').html('No data available');
	}
    var piechart = AmCharts.makeChart("mediaChartPie", {
        "type": "pie",
        "theme": "light",

        // "legend": {
        //     "legendColor": true
        // },
        "autoMargins": true,
        "marginTop": 0,
        "marginBottom": 10,
        "marginLeft": 0,
        "marginRight": 0,
        "pullOutRadius": 10,
        "dataProvider": graphData,
        "valueField": "value",
        "startDuration":0,
        //"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "titleField": "key",
        "colorField": "color",
        "innerRadius": "20%",
        "depth3D": 10,
        "angle": 15,
        "export": {
            "enabled": true
        }
    });

    mediaCharts.push(piechart);

    piechart.addListener("clickSlice", function(val) {
          var  valueField=val.dataItem.title;
               valueField=valueField.toLowerCase();
        // alert(val.dataItem.title);
        tagAddFunctionMedia({
            "field": "Categories.SentimentOut.overallSentiment",
            "value": valueField
        });
        esClientMedia.addTermMatch("Categories.SentimentOut.overallSentiment", valueField);
        conductSearchMedia();
    });
};

function unique(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
};


var genrateTrendDataMedia = function(histogram, dataKey, innerBucket) {
    //		var series=0;
    var dataKey = dataKey;
    var trend = [];
    for (var index in histogram) {
        var milliSecondValue = (histogram[index].key);
        var histogramBuckets = histogram[index][innerBucket].buckets;
        for (var bucketIndex in histogramBuckets) {
            mediaNames.push(histogramBuckets[bucketIndex].key);

            if ((histogramBuckets[bucketIndex].key) == dataKey) {
                trend.push({
                    series: 0,
                    x: (milliSecondValue),
                    y: (histogramBuckets[bucketIndex].doc_count)
                });
            } else {
                trend.push({
                    series: 0,
                    x: (milliSecondValue),
                    y: 0
                });
            }
        }

    }
    //	    console.log(mediaNames);
    mediaNames = unique(mediaNames);
    //	  console.log(mediaNames);
    return trend;
};




function pageLoadMedia(data) {
	if(data.length < 1) {
		$('.dataContentMediaTrends').html('No data available');
	}
    var chart = AmCharts.makeChart("mediaColumnTotal", {
        "type": "serial",
        "theme": "light",
        "marginRight": 30,
        // "legend": {
        //     "equalWidths": false,
        //     "periodValueText": "total: [[value.sum]]",
        //     "position": "top",
        //     "valueAlign": "left",
        //     "valueWidth": 100
        // },
        "dataProvider": data,

        "graphs": [{
            "balloonText": "<span><b>Total :</b></span><span style='font-size:14px; color:#000000;'><b>[[total]]</b></span>",


            "fillAlphas": 0.6,
            "lineAlpha": 0.4,
            "title": "Total",
            "fillColors": "#50b6b6",
            "valueField": "total"
        }],
        "plotAreaBorderAlpha": 0,
        "marginTop": 10,
        "marginLeft": 0,
        "marginBottom": 0,
        "chartCursor": {
            "cursorAlpha": 0
        },
        "categoryField": "date",
        "categoryAxis": {
            "startOnAxis": true,
            "axisColor": "#DADADA",
            "gridAlpha": 0.07
            

        },

        "export": {
            "enabled": true
        }
    });
    mediaCharts.push(chart);

};

var createMediaMap = function(data) {
    console.log("CountryMap", data);
    var map = new AmCharts.AmMap();
    map.type = "map";
    map.theme = "light"
    var dataProvider = {
        "map": "worldLow",
        "areas": data,
        "getAreasFromMap": true
    };

    map.dataProvider = dataProvider;
    map.areasSettings = {
        "color": "#D3EBED",
        "autoZoom":false,
        "selectedColor": "#CC0000",
        "selectable": true,
        "balloonText": "[[title]] :[[value]]"
    };

    map.export = {
        enabled: true,
        exportTitles: true
    }

    mediaCharts.push(map);
    map.write("chartMediaMap");

    map.addListener('clickMapObject', function(event) {
        // deselect the area by assigning all of the dataProvider as selected object
        //alert(event.mapObject.title);
        var name = event.mapObject.title;
        var fieldName = "Categories.Types.Entities.Country.entities.entity";
        checkBoxClickCalls(name, fieldName);
    });

};

// function mediaCreateReport() {
//     var images = [];
//     var pending = mediaCharts.length;
//     for (var i = 0; i < mediaCharts.length; i++) {
//         var chart = mediaCharts[i];
//         console.log(chart);
//         chart.export.capture({}, function() {
//             this.toJPG({}, function(data) {
//                 images.push({
//                     "image": data,
//                     "fit": [523.28, 769.89]
//                 });
//                 pending--;
//                 if (pending === 0) {
//                     // all done - construct PDF
//                     chart.export.toPDF({
//                         content: images
//                     }, function(data) {
//                         this.download(data, "application/pdf", "amCharts.pdf");
//                     });
//                 }
//             });
//         });
//     }
   
// }




//dont delete

var layoutMedia = {
        /*
        ** Array of objects or strings which represents the output
        */
    content: [
        {
            text: "Media Map",
            color:"#3A907F"
        }, {
            image: "image_1", // reference to the image mapping below
            fit: [523.28, 769.89] // fits the image to those dimensions (A4)
        },

         {
            text: "Media Trends",
            color:"#3A907F"
        }, {
            image: "image_2", // reference to the image mapping below
            fit: [523.28, 769.89] // fits the image to those dimensions (A4)
        },
         {
            text: "Sentiment Chart",
            color:"#3A907F"
        }, {
            image: "image_3", // reference to the image mapping below
            fit: [300,500]// fits the image to those dimensions (A4)
        }

    ],


    //  ** Mapping object, holds the actual imagery

    images: {

        }
}
function mediaCreateReport() {
  var pdf_images = 0;
  var pdf_layout = layoutMedia; // loaded from another JS file
  for (var i = 0; i <mediaCharts.length; i++ ) {
     var chart =mediaCharts[ i ];
    // Capture current state of the chart
    chart.export.capture( {}, function() {

      // Export to PNG
      this.toJPG( {
        // pretend to be lossless
         
        // Add image to the layout reference
      }, function( data ) {
        pdf_images++;
        pdf_layout.images[ "image_" + pdf_images ] = data;

        // Once all has been processed create the PDF
        if ( pdf_images == mediaCharts.length ) {

          // Save as single PDF and offer as download
          this.toPDF( pdf_layout, function( data ) {
            this.download( data, "application/pdf", "amcharts.pdf" );
          } );
        }
      } );
    } );
  }
}
