var currentWizardIndex = 0;
var esClientAdmin = new Elasticsearch(esHost, null, null);
var esClientValidate = new Elasticsearch(esHost, function() {
    return {};
}, {}, "emptyQueryFunction");

//var sampleObject = {
//  arun : ["hi","how"]
//}
//var esClientTest = new Elasticsearch('localhost:9200',null,null);
//esClientTest.updateGetSmm('http://192.168.1.53:9200','smm-state-index','smm-state-type',user,function(response){
//  console.log('test response is',response)
//});

var tagField = null;
var diff = [];
var twitterArray = {
    boolTracks:[],
    keywords: null,
    idsHandles: null,
    merged: null,
    hashTags: null
};

// var faceBookArray = {
//  pagelink: null
// };

var faceBookArray = [];

var mediaArray = {
    keywords: []
    //    /,
    //    source: []
};

var youtubeArray = {
    "keywordsYt": null,
    "channelLinks": null,
    "youtubeLinks": null
};

var instagramArray = {
    "tracks": [],
    "follows": []
};
var myWindow;
var oldRule = {};
var presentRule={};
var deleteRule=[];
var gnipTagValue = null;

//COMMON SEARCH
$('body').on('click', '#searchButton', function(e) {
    e.preventDefault();
    var searchField = null;
    searchField = $(".search-box").val();

    globalObject.onSearch(searchField);
});

$('.main-search').keyup(function(e) {
    searchConduct();

    // $('#searchButton').click();

});



//CUSTOM DATE TIME -DATEPICKER
$('body').on('click', '#customButton', function() {
    //  $('.customDatePick').hide();

    var fromDate = $("#date-fld1").val();
    var toDate = $("#date-fld2").val();

    $(".timeFilter").removeClass("active");
    $(".customDateSelect").addClass("active");
    // var pathName = location.hash.replace(/^#/, "");
    convertDatetoEs(fromDate, toDate, "", "", false);

});




//TIME FILTER CHANGE HEADER
$('body').on('click', '.timeFilter', function() {
    var pathName = location.hash.replace(/^#/, "");
    //  alert(pathName);

    $(".timeFilter").removeClass("active");
    $(".customDateSelect").removeClass("active");

    $(this).addClass("active");
    duration = $(this).attr("duration");

    globalStartDuration = "now-" + duration;

    console.log('new duration', duration);
    var name = $(this).attr("name");
    globalEndDuration = null;
    $('.time-select span').html('<i class="fa fa-clock-o"></i>' + name + '<i class="ion-ios-arrow-down"></i>');

    globalObject.onTimeChange(globalStartDuration, globalEndDuration);


});


//GLOBAL TAG CHANGE
$('body').on('click', '.liTags', function() {
    //   alertInfo("Changed Tab");
    var pathName = location.hash.replace(/^#/, "");
    globalTagValue = $(this).attr('tagnamepick');
    gnipTagValue = globalTagValue;
    //    alert(globalTagValue);
    $(".liTags").removeClass("active");
    $(this).addClass("active");
    $('.tagsAddition').html('<i class="ion-pricetag"></i>' + globalTagValue + '<i class="ion-ios-arrow-down"></i>');

    //emptying the global search box and the translate box values on tab change
    $('.search-box').val(' ');
    $('#translatedValue').val(' ');
    //clearing the fb filter tags
    //        $('.selectedTagFb').remove();
    globalObject.globalFunctionChange(globalTagValue);

});

function downloadaspdf() {

    var pathName = location.hash.replace(/^#/, "");
    $('.button-download').addClass('global-report-loader');
    $('.global-report-loader').html('<img src="../img/loading.gif">');
    globalObject.CreateReport();
};

//$('body').on('click', '.customDateSelect', function() {
//  $('#customDate').modal('show');
//});

//$('.custom-datetimepicker').datetimepicker({
//      language: 'ru'
//  });


    function changePassword() 
    {
        var newPass=$("#new-password").val();
        if($("#new-password").val()==$("#re-enter-new-password").val()){
                        
                $.ajax({
                    type: "POST",
                    url: esHostServer + "/changeUserPassword",

                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({password:newPass}),
                    dataType: "JSON",
                    success: function(data) {
                        console.log("response change password..",data);
                         $(".pass-alert").notify("Password changed successfully","success");
                         $("#re-enter-new-password").val("");
                         $("#new-password").val("");
               
                    },
                    error: function(error) {
                        console.log("response change password..",error);
                        $(".pass-alert").notify("Could not reset password ", "error");

                    }

                });

        }

        else{
             $(".pass-alert").notify("Passwords does not match ", "warn");
        }
      
    }



$(document).ready(function() {
    $('.successNotification').hide();
    $('.deleteNotification').hide();
    $('#tagRemovalModal').hide();
    $('[data-toggle="tooltip"]').tooltip();
})


var getUserDocument = function(user) {
    fbArray = [];
    fbGroupsArray = [];
    var failureReasonArray = [];

    //  youtube 
    channelLinksYtArray = [];
    videoLinksYtArray = [];

    tagsAutoLoad(globalConfig);
    $('.adminSelectTag').html(tagTrimmer(diff[0]));
    $('#dropdownMenu1').attr('title', globalTagValue);
    if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]])) {
        if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].twitter)) {

              twitterArray.boolTracks=globalConfig._source[diff[0]].twitter.boolTracks;

             var content=globalConfig._source[diff[0]].twitter;
             updateTwitterAdminContent(content);
           /* console.log('boolTracks', globalConfig._source[diff[0]].twitter.boolTracks);

            $('#keywordsTwitter').val(globalConfig._source[diff[0]].twitter.boolTracks);
            $('#idsHandlesTwitter').val(globalConfig._source[diff[0]].twitter.follows);*/
        }
        if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].faceBook)) {

            var index = 0;
            var renderArray = [];

            fbArray = globalConfig._source[diff[0]].faceBook.pages;
            failureReasonArray = globalConfig._source[diff[0]].faceBook.failureReason;
            for (index; index <= fbArray.length; index++) {
                renderArray.push({
                    link: fbArray[index]
                })
            }
            $("#pageFbForm").html($("#fbLinkRender").render(renderArray));
            index = 0;
            for (index; index < fbArray.length; index++) {
                console.log('array index', fbArray[index]);
                $('.pageLinkFbRender' + index + '').val(fbArray[index]);
            }

            console.log('render array is', renderArray);

            if (!isNull(failureReasonArray)) {
                if (failureReasonArray.length > 0) {
                    fbFailureReason(fbArray, failureReasonArray);

                }
            }
            //          $('#pageLinkFb').val(globalConfig._source[diff[0]].faceBook.pages);

            var indexGroups = 0;
            var renderArrayGroups = [];
            if (!isNull(globalConfig._source[diff[0]].faceBook.groups)) {
                fbGroupsArray = globalConfig._source[diff[0]].faceBook.groups;
            } else {
                fbGroupsArray = [];
            }
            failureReasonArrayGroups = globalConfig._source[diff[0]].faceBook.grpfailureReason;
            console.log('failure reason array', failureReasonArrayGroups);
            for (indexGroups; indexGroups <= fbGroupsArray.length; indexGroups++) {
                renderArrayGroups.push({
                    link: fbGroupsArray[indexGroups]
                })
            }
            $("#groupFbForm").html($("#fbGroupLinkRender").render(renderArrayGroups));

            indexGroups = 0;
            for (indexGroups; indexGroups < fbGroupsArray.length; indexGroups++) {
                $('.groupLinkFbRender' + indexGroups + '').val(fbGroupsArray[indexGroups]);
            }

            //          failureReason for groupps
            if (!isNull(failureReasonArrayGroups)) {
                if (failureReasonArrayGroups.length > 0) {
                    fbFailureReasonGroups(fbGroupsArray, failureReasonArrayGroups);

                }
            }
            //failureMessageGroup
        } else {

            $("#pageFbForm").html($("#fbLinkRender").render(["1"]));
            $("#groupFbForm").html($("#fbGroupLinkRender").render(["1"]));

        }
        if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].youtube)) {
            $('#keywordsYt').val(globalConfig._source[diff[0]].youtube.boolQuery);

            var indexCyt = 0; //initialising index for channel count
            var renderChannelYtArray = []; // initialising array for using jsrender
            channelLinksYtArray = globalConfig._source[diff[0]].youtube.channels; // loading user saved channels to channelLinksYtArray
            for (indexCyt; indexCyt <= channelLinksYtArray.length; indexCyt++) { //preparing the render array
                renderChannelYtArray.push({
                    link: channelLinksYtArray[indexCyt]
                })
            }

            $("#channelYtForm").html($("#channelYtLinkRender").render(renderChannelYtArray));

            indexCyt = 0;
            for (indexCyt; indexCyt < channelLinksYtArray.length; indexCyt++) {
                $('.channelLinkYtRender' + indexCyt + '').val(channelLinksYtArray[indexCyt]);
            }

            var indexVidLink = 0; //initialising index for video link count
            var renderVideoYtArray = []; // initialising array for using jsrender
            videoLinksYtArray = globalConfig._source[diff[0]].youtube.links; // loading user saved video links to videoLinksYtArray

            for (indexVidLink; indexVidLink <= videoLinksYtArray.length; indexVidLink++) { //preparing the render array
                renderVideoYtArray.push({
                    link: videoLinksYtArray[indexVidLink]
                })
            }

            $("#linkYtForm").html($("#videoYtLinkRender").render(renderVideoYtArray));

            indexVidLink = 0;
            for (indexVidLink; indexVidLink < videoLinksYtArray.length; indexVidLink++) {
                $('.videoLinkYtRender' + indexVidLink + '').val(videoLinksYtArray[indexVidLink]);
            }

            //            $('#channelLinksYt').val(globalConfig._source[diff[0]].youtube.channels);
            //            $('#youtubeLinks').val(globalConfig._source[diff[0]].youtube.links);
        } else {
            $("#channelYtForm").html($("#channelYtLinkRender").render(["1"]));
            $("#linkYtForm").html($("#videoYtLinkRender").render(["1"]));

        }
        if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].instagram)) {
            $('#keywordsInsta').val(globalConfig._source[diff[0]].instagram.tracks);
        }
        if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].instagram)) {
            $('#keywordsInsta').val(globalConfig._source[diff[0]].instagram.tracks);
            $('#followsInsta').val(globalConfig._source[diff[0]].instagram.follows);
        }
        if (!isNull(globalConfig._source) && !isNull(globalConfig._source[diff[0]].media)) {
            $('#keywordsMedia').val(globalConfig._source[diff[0]].media.keywordsMedia);
        }
    }

}


$('body').on('click', '.successNotifyClose', function() {
    $('.successNotification').hide();
});

$('body').on('click', '.deleteNotifyClose', function() {
    $('.deleteNotification').hide();
});

$('body').on('click', '.tagAdd-btn', function() {
    adminSectionPermissionHandler();
    console.log('first position', globalTagValue);
    gnipTagValue = globalTagValue;

    getUserDocument(user);
    //  $('#tagModel').modal('show');

});



var updateTwitterAdminContent=function(data){
 
   $('#idsHandlesTwitter').val(data.follows);
   $('.twitterKeywordListAll').empty();

     console.log("data booltracks....",data.boolTracks);
      if( typeof data.boolTracks === 'string' ) {
        

          appendTwitterKeyword(data.boolTracks ,0,false);
       }

      else{

           for(i=0;i<data.boolTracks.length;i++){
               // console.log("fs1...",data.boolTracks[i]);
               appendTwitterKeyword(data.boolTracks[i],i,false);
             
           }
      }
  

};

$('body').on('click', '.editQuery', function() {
    adminSectionPermissionHandler();
    fbArray = [];
    fbGroupsArray = [];
    channelLinksYtArray = [];
    videoLinksYtArray = [];

    // var tagSelectCount = parseInt(($(this).attr("rvalue")));
    // var liSelect = diff[tagSelectCount];
    // alert(liSelect);
    // globalTagValue = liSelect;
    // tagField = liSelect;
    var liSelect = $(this).parent().find('.liTags').attr('tagnamepick');
    //  alert(liSelect);
    globalTagValue = liSelect;
    gnipTagValue = globalTagValue;
    console.log('second position', gnipTagValue);
  




    tagsAutoLoad(globalConfig);
    //    var oldRuleValue = oldRuleGeneration(gnipTagValue);
    //    console.log('second position old rule generation',oldRuleValue)

    // console.log('second position value for bool tracks old', oldRule);
    $('.adminSelectTag').html(tagTrimmer(liSelect));

    //    $('.tagsAddition').html('<i class="ion-pricetag"></i>' + globalTagValue + '<i class="ion-ios-arrow-down"></i>');
    $('.tagsAdditionButton').html(tagTrimmer(liSelect));

    $('#dropdownMenu1').attr('title', liSelect);

    esClientAdmin.updateGet(user, function(response) {
        if (!isNull(response._source[liSelect].twitter)) {
            twitterArray.boolTracks=globalConfig._source[globalTagValue].twitter.boolTracks;
            console.log("twitter booltrack array....",twitterArray.boolTracks);

             var content=response._source[liSelect].twitter;
             updateTwitterAdminContent(content);
           /* $('#keywordsTwitter').val(response._source[liSelect].twitter.boolTracks);
            $('#idsHandlesTwitter').val(response._source[liSelect].twitter.follows);*/
        }
        if (!isNull(response._source[liSelect].faceBook)) {
            // $('#pageLinkFb').val(response._source[liSelect].faceBook.pages);
            var failureReasonArray = [];
            var index = 0;
            var renderArray = [];

            if (isNull(response._source[liSelect].faceBook.pages)) {
                fbArray = [];
            } else {
                fbArray = response._source[liSelect].faceBook.pages;
            }
            failureReasonArray = response._source[liSelect].faceBook.failureReason;
            console.log("fb array", fbArray);
            for (index; index <= fbArray.length; index++) {
                renderArray.push({
                    link: fbArray[index]
                })
            }
            $("#pageFbForm").html($("#fbLinkRender").render(renderArray));
            index = 0;
            for (index; index <= fbArray.length; index++) {
                console.log('array index', fbArray[index]);
                $('.pageLinkFbRender' + index + '').val(fbArray[index]);
            }

            console.log('render array is', renderArray);

            if (!isNull(failureReasonArray)) {
                if (failureReasonArray.length > 0) {

                    fbFailureReason(fbArray, failureReasonArray);

                }
            }

            var indexGroups = 0;
            var renderArrayGroups = [];
            if (isNull(response._source[liSelect].faceBook.groups)) {
                fbGroupsArray = [];
            } else {
                fbGroupsArray = response._source[liSelect].faceBook.groups;
            }
            console.log('fb groups array', fbGroupsArray);
            failureReasonArrayGroups = globalConfig._source[liSelect].faceBook.grpfailureReason;
            console.log('failure reason array groups line 325', failureReasonArrayGroups);
            for (indexGroups; indexGroups <= fbGroupsArray.length; indexGroups++) {
                renderArrayGroups.push({
                    link: fbGroupsArray[indexGroups]
                })
            }
            $("#groupFbForm").html($("#fbGroupLinkRender").render(renderArrayGroups));

            indexGroups = 0;
            for (indexGroups; indexGroups < fbGroupsArray.length; indexGroups++) {
                $('.groupLinkFbRender' + indexGroups + '').val(fbGroupsArray[indexGroups]);
            }

            //          failureReason for groupps

            if (!isNull(failureReasonArrayGroups)) {
                if (failureReasonArrayGroups.length > 0) {
                    fbFailureReasonGroups(fbGroupsArray, failureReasonArrayGroups);

                }
            }
            //failureMessageGroup

        } else {

            $("#pageFbForm").html($("#fbLinkRender").render(["1"]));
            $("#groupFbForm").html($("#fbGroupLinkRender").render(["1"]));

        }
        if (!isNull(response._source[liSelect].youtube)) {
            $('#keywordsYt').val(response._source[liSelect].youtube.boolQuery);

            var indexCyt = 0;
            var renderChannelYtArray = [];

            if (isNull(response._source[liSelect].youtube.channels)) {
                channelLinksYtArray = [];
            } else {
                channelLinksYtArray = response._source[liSelect].youtube.channels;
            }
            console.log("yt channel array", channelLinksYtArray);
            for (indexCyt; indexCyt <= channelLinksYtArray.length; indexCyt++) {
                renderChannelYtArray.push({
                    link: channelLinksYtArray[indexCyt]
                })
            }
            $("#channelYtForm").html($("#channelYtLinkRender").render(renderChannelYtArray));

            indexCyt = 0;
            for (indexCyt; indexCyt < channelLinksYtArray.length; indexCyt++) {
                $('.channelLinkYtRender' + indexCyt + '').val(channelLinksYtArray[indexCyt]);
            }

            var indexVidLink = 0;
            var renderVideoYtArray = [];

            if (isNull(response._source[liSelect].youtube.links)) {
                videoLinksYtArray = [];
            } else {
                videoLinksYtArray = response._source[liSelect].youtube.links;
            }
            console.log("yt video array", videoLinksYtArray);
            for (indexVidLink; indexVidLink <= videoLinksYtArray.length; indexVidLink++) {
                renderVideoYtArray.push({
                    link: videoLinksYtArray[indexVidLink]
                })
            }
            $("#linkYtForm").html($("#videoYtLinkRender").render(renderVideoYtArray));
            indexVidLink = 0;
            for (indexVidLink; indexVidLink < videoLinksYtArray.length; indexVidLink++) {
                $('.videoLinkYtRender' + indexVidLink + '').val(videoLinksYtArray[indexVidLink]);
            }

            //            $('#channelLinksYt').val(response._source[liSelect].youtube.channels);
            //            $('#youtubeLinks').val(response._source[liSelect].youtube.links);
        } else {
            $("#channelYtForm").html($("#channelYtLinkRender").render(["1"]));
            $("#linkYtForm").html($("#videoYtLinkRender").render(["1"]));
        }
        if (!isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val(response._source[liSelect].instagram.tracks);
        }
        if (!isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val(response._source[liSelect].instagram.tracks);
            $('#followsInsta').val(response._source[liSelect].instagram.follows);
        }
        if (!isNull(response._source[liSelect].media)) {
            $('#keywordsMedia').val(response._source[liSelect].media.keywordsMedia);
        }

        if (isNull(response._source[liSelect].twitter)) {
            $('#keywordsTwitter').val('');
            $('#idsHandlesTwitter').val('');
        }
        if (isNull(response._source[liSelect].faceBook)) {
            $('#pageLinkFb').val('');
        }
        if (isNull(response._source[liSelect].youtube)) {
            $('#keywordsYt').val('');
            $('#channelLinksYt').val('');
            $('#youtubeLinks').val('');
        }
        if (isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val('');
        }
        if (isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val('');
            $('#followsInsta').val('');
        }
        if (isNull(response._source[liSelect].media)) {
            $('#keywordsMedia').val('');
        }

    });
});






var tagsAutoLoad = function(response) {
    console.log("delete tag.....", response);

    $('.tagDropdownButton').empty();
    var comparisonArray = ["twitter", "faceBook", "youtube", "instagram", "media", "facebook", "tags", "_id", "tweetMan"];
    var keys = [];
    var count = 0;
    for (var k in response._source) keys.push(k);
    //  console.log(arr_diff(comparisonArray,keys));
    diff = arr_diff(comparisonArray, keys);
    diff = jQuery.grep(diff, function(value) {
        return value != "facebook";
    });
    console.log('diff is', diff);
    for (count; count < diff.length; count++) {

        $('.tagDropdownButton').append('<li class="allTags" id=' + count + ' rvalue=' + count + ' value="' + diff[count] + '" role="presentation"><a role="menuitem" tabindex="-1">' + diff[count] + '</a><a class="removeQuery" removeValue=' + diff[count] + '  >x</a></li>');
    }
};

function arr_diff(a2, a1) {
    var a = [],
        diff = [];
    for (var i = 0; i < a1.length; i++)
        a[a1[i]] = true;
    for (var i = 0; i < a2.length; i++)
        if (a[a2[i]]) delete a[a2[i]];
    for (var k in a)
        diff.push(k);
    return diff;
}
var tagTrimmer = function(tagValue) {
    var tagLength = tagValue.length;
    var trimmedValue = null;

    if (tagLength > 7) {
        trimmedValue = tagValue.substring(0, 7) + "...";
    } else {
        trimmedValue = tagValue;
    }
    return trimmedValue;
}




$('body').on('click', '.allTags', function() {
    fbArray = [];
    fbGroupsArray = [];
    //  YOUTUBE
    channelLinksYtArray = [];
    videoLinksYtArray = [];

    var tagSelectCount = parseInt(($(this).attr("rvalue")));

    var liSelect = diff[tagSelectCount];

    globalTagValue = liSelect;
    tagField = liSelect;
    gnipTagValue = globalTagValue;
 

    $('.adminSelectTag').html(tagTrimmer(globalTagValue));

    //    $('.tagsAddition').html('<i class="ion-pricetag"></i>' + globalTagValue + '<i class="ion-ios-arrow-down"></i>');
    $('.tagsAdditionButton').html(tagTrimmer(globalTagValue));

    $('#dropdownMenu1').attr('title', globalTagValue);

    esClientAdmin.updateGet(user, function(response) {
        if (!isNull(response._source[liSelect].twitter)) {

             var content=response._source[liSelect].twitter;
             updateTwitterAdminContent(content);
          

         /*   $('#keywordsTwitter').val(response._source[liSelect].twitter.boolTracks);
            $('#idsHandlesTwitter').val(response._source[liSelect].twitter.follows);*/
        }
        if (!isNull(response._source[liSelect].faceBook)) {
            // $('#pageLinkFb').val(response._source[liSelect].faceBook.pages);
            var failureReasonArray = [];
            var index = 0;
            var renderArray = [];
            if (isNull(response._source[liSelect].faceBook.pages)) {
                fbArray = [];
            } else {
                fbArray = response._source[liSelect].faceBook.pages;
            }
            failureReasonArray = response._source[liSelect].faceBook.failureReason;
            console.log("fb array", fbArray);
            for (index; index <= fbArray.length; index++) {
                renderArray.push({
                    link: fbArray[index]
                })
            }
            $("#pageFbForm").html($("#fbLinkRender").render(renderArray));
            index = 0;
            for (index; index <= fbArray.length; index++) {
                console.log('array index', fbArray[index]);
                $('.pageLinkFbRender' + index + '').val(fbArray[index]);
            }

            console.log('render array is', renderArray);

            if (!isNull(failureReasonArray)) {
                if (failureReasonArray.length > 0) {

                    fbFailureReason(fbArray, failureReasonArray);

                }
            }

            var indexGroups = 0;
            var renderArrayGroups = [];
            if (isNull(response._source[liSelect].faceBook.groups)) {
                fbGroupsArray = [];
            } else {
                fbGroupsArray = response._source[liSelect].faceBook.groups;
            }
            failureReasonArrayGroups = globalConfig._source[diff[0]].faceBook.grpfailureReason;
            for (indexGroups; indexGroups <= fbGroupsArray.length; indexGroups++) {
                renderArrayGroups.push({
                    link: fbGroupsArray[indexGroups]
                })
            }
            $("#groupFbForm").html($("#fbGroupLinkRender").render(renderArrayGroups));

            indexGroups = 0;
            for (indexGroups; indexGroups < fbGroupsArray.length; indexGroups++) {
                console.log('render group array', fbGroupsArray);
                console.log('render group names', fbGroupsArray[indexGroups]);
                $('.groupLinkFbRender' + indexGroups + '').val(fbGroupsArray[indexGroups]);
            }


            //          failureReason for groupps

            if (!isNull(failureReasonArrayGroups)) {
                if (failureReasonArrayGroups.length > 0) {
                    fbFailureReasonGroups(fbGroupsArray, failureReasonArrayGroups);

                }
            }
            //failureMessageGroup

        } else {

            $("#pageFbForm").html($("#fbLinkRender").render(["1"]));
            $("#groupFbForm").html($("#fbGroupLinkRender").render(["1"]));

        }
        if (!isNull(response._source[liSelect].youtube)) {
            $('#keywordsYt').val(response._source[liSelect].youtube.boolQuery);

            var indexCyt = 0;
            var renderChannelYtArray = [];
            if (isNull(response._source[liSelect].youtube.channels)) {
                channelLinksYtArray = [];
            } else {
                channelLinksYtArray = response._source[liSelect].youtube.channels;
            }
            console.log("render channel yt array", renderChannelYtArray);
            for (indexCyt; indexCyt <= channelLinksYtArray.length; indexCyt++) {
                renderChannelYtArray.push({
                    link: channelLinksYtArray[indexCyt]
                })
            }
            $("#channelYtForm").html($("#channelYtLinkRender").render(renderChannelYtArray));

            indexCyt = 0;
            for (indexCyt; indexCyt < channelLinksYtArray.length; indexCyt++) {
                $('.channelLinkYtRender' + indexCyt + '').val(channelLinksYtArray[indexCyt]);
            }

            var indexVidLink = 0;
            var renderVideoYtArray = [];

            if (isNull(response._source[liSelect].youtube.links)) {
                videoLinksYtArray = [];
            } else {
                videoLinksYtArray = response._source[liSelect].youtube.links;
            }
            console.log("yt video links array", videoLinksYtArray);
            for (indexVidLink; indexVidLink <= videoLinksYtArray.length; indexVidLink++) {
                renderVideoYtArray.push({
                    link: videoLinksYtArray[indexVidLink]
                })
            }
            $("#linkYtForm").html($("#videoYtLinkRender").render(renderVideoYtArray));

            indexVidLink = 0;
            for (indexVidLink; indexVidLink < videoLinksYtArray.length; indexVidLink++) {
                $('.videoLinkYtRender' + indexVidLink + '').val(videoLinksYtArray[indexVidLink]);
            }
            //            $('#channelLinksYt').val(response._source[liSelect].youtube.channels);
            //            $('#youtubeLinks').val(response._source[liSelect].youtube.links);
        } else {
            $("#channelYtForm").html($("#channelYtLinkRender").render(["1"]));
            $("#linkYtForm").html($("#videoYtLinkRender").render(["1"]));
        }
        if (!isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val(response._source[liSelect].instagram.tracks);
        }
        if (!isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val(response._source[liSelect].instagram.tracks);
            $('#followsInsta').val(response._source[liSelect].instagram.follows);
        }
        if (!isNull(response._source[liSelect].media)) {
            $('#keywordsMedia').val(response._source[liSelect].media.keywordsMedia);
        }

        if (isNull(response._source[liSelect].twitter)) {
            $('#keywordsTwitter').val('');
            $('#idsHandlesTwitter').val('');
        }
        if (isNull(response._source[liSelect].faceBook)) {
            $('#pageLinkFb').val('');
        }
        if (isNull(response._source[liSelect].youtube)) {
            $('#keywordsYt').val('');
            $('#channelLinksYt').val('');
            $('#youtubeLinks').val('');
        }
        if (isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val('');
        }
        if (isNull(response._source[liSelect].instagram)) {
            $('#keywordsInsta').val('');
            $('#followsInsta').val('');
        }
        if (isNull(response._source[liSelect].media)) {
            $('#keywordsMedia').val('');
        }

    });
});
$('body').on('click', '.nav-tabs a', function(e) {
    e.preventDefault();
    currentWizardIndex = $($(this).attr('href')).index();
});

 
var saveRuleAndScreenName=function(){
   if((globalConfig._source[globalTagValue].twitter !== undefined) &&(globalConfig._source[globalTagValue].twitter.boolTracks !== undefined) ){
            twitterArray.boolTracks=globalConfig._source[globalTagValue].twitter.boolTracks; 
        }
        else{
           twitterArray.boolTracks=[]; 
        }
     gnipRuleArray=[];
  


  $('.twitterKeywordListAll li .twitterKeywordList .twitterKeyVal textarea').each(function() {


        var prevVal;
     
         var string=$(this).val();
         string=string.trim();
         if (string.indexOf(',') !== -1) {
          alert('Keywords should be in boolean format');
          return true;
        }
        if (/\bor\b/.test(string)) {
            alert("Keywords should not allow 'or'");
            return true;
        }

         prevVal=($(this).data('content')).trim();
          var oldRuleVal=prevVal;
      
         if( typeof twitterArray.boolTracks === 'string' ) {
                if(twitterArray.boolTracks !==string){
                  twitterArray.boolTracks=[twitterArray.boolTracks];
                }
             
         }


     else if((twitterArray.boolTracks.indexOf(string)) <= -1){
        
         if(twitterArray.boolTracks.indexOf(prevVal)>-1){
            var index=twitterArray.boolTracks.indexOf(prevVal); 
            twitterArray.boolTracks.splice(index,1);
         }      
      
           twitterArray.boolTracks.push(string);
        }
      

       $(this).data('content',string);
        
       var presentruleVal=string;
       

        oldRule={
           "tag":user + '%%%' + globalTagValue,
            "value": oldRuleVal


        }

        presentRule={
            "tag":user + '%%%' + globalTagValue,
            "value":presentruleVal

        }

          var rulesObj = {
           "presentRule": presentRule,
           "oldRule": oldRule
         };
       
      
       gnipRuleArray.push(rulesObj);

    
      
      

  });

 
   if(gnipRuleArray.length>0){
        var id = "twitterFormat";
         updateFn("configs", "config", user, id);
        console.log("gnip rule arr",gnipRuleArray);

      addGnipRule(gnipRuleArray,user, globalTagValue);

   }
      




}


$('body').on('click', '#saveTag', function(e) {
    e.preventDefault();
    if (currentWizardIndex == 0) {
        if(userPer.analyticsTwitter == true){

            saveRuleAndScreenName();
     
        }
        else{
            alert('no permissions')
        }
        //updateFn("configs", "config", user, id);
    } else if (currentWizardIndex == 1) {

        // alert("ok");
        //         var id = "faceBookFormat";
        //          var tempArray = $('#pageLinkFb').val().split(/[\n, \t]+/);
        //          faceBookArray.pagelink = tempArray.filter(function (v) {
        //              return v !== ''
        //          });
        //            console.log("fb array",faceBookArray);
        //         updateFn("configs", "config", user, id);

    } 
//    else if (currentWizardIndex == 2) {
//        if(userPer.analyticsInstagram == true){
//            var id = "instagramFormat";
//            instagramArray.tracks = $('#keywordsInsta').val().split(",");
//            for (index = 0; index < instagramArray.tracks.length; index++) {
//                var tracks = instagramArray.tracks[index].trim();
//                if (tracks.indexOf(' ') !== -1) {
//                    alert("Tags should not contain space");
//                    return true;
//                }
//            }
//            console.log("instagram  Array", instagramArray.tracks);
//            instagramArray.follows = $('#followsInsta').val().split(",");
//            updateFn("configs", "config", user, id);
//        }
//        else{
//            alert('no access permissions');
//        }
//
//    }
    else if (currentWizardIndex == 2) {

   
        if(userPer.analyticsYoutube == true){
            var id = "youtubeFormat";
            // youtubeArray.keywordsYt = $('#keywordsYt').val().split(",");
            youtubeArray.keywordsYt = $('#keywordsYt').val();
//            alert(youtubeArray.keywordsYt);
            // youtubeArray.channelLinks = $('#channelLinksYt').val().split(",");
            // youtubeArray.youtubeLinks = $('#youtubeLinks').val().split(",");
            esClientValidate.addQueryString("_all", youtubeArray.keywordsYt);
            esClientValidate.validateQuery(validateYoutubeQuery);
        }
        else{
            alert('no access permissions');
        }

    } else if (currentWizardIndex == 3) {
        if(userPer.analyticsMedia == true){
            var id = "mediaFormat";
            mediaArray.keywords = [$("#keywordsMedia").val()];
            //alert(mediaArray.keywords[0]);
            esClientValidate.addQueryString("_all", mediaArray.keywords[0]);
            esClientValidate.validateQuery(validateMediaQuery);
            updateFn("configs", "config", user, id);
        }
        else{
            alert('no access permissions');
        }
    }

})


var validateMediaQuery = function(response) {
    console.log('media response valid/not', response.valid);
    var id = "mediaFormat";
    if (response.valid == true) {
        updateFn("configs", "config", user, id);
    } else {
        alert("QUERY FORMAT IMPORPER,PLEASE CHECK AND ENTER AGAIN")
    }
};

var validateTwitterQuery = function(response) {
    console.log("validate twitter query....", response);
    console.log('twitter response valid/not', response.valid);
    var id = "twitterFormat";
    if (response.valid == true) {
        updateFn("configs", "config", user, id);

    } else {
        alert("QUERY FORMAT IMPORPER,PLEASE CHECK AND ENTER AGAIN")
    }
};

var validateYoutubeQuery = function(response) {
    console.log('youtube response valid/not', response.valid);
    var id = "youtubeFormat";
    if (response.valid == true) {
        updateFn("configs", "config", user, id);
    } else {
        alert("QUERY FORMAT IMPORPER,PLEASE CHECK AND ENTER AGAIN")
    }
};






var updateFn= function(indexName, typeName, refId, update) {


    var newFeedTwitter = {
        "twitter": {
            //          "tracks": twitterArray.keywords,
            "boolTracks": twitterArray.boolTracks,
            "follows": twitterArray.idsHandles
        }
    };
    var newFeedFb = {
        "faceBook": {
            "pages": fbArray
        }
    };
    var newFeedFbGroups = {
        "faceBook": {
            "groups": fbGroupsArray
        }
    };
    var newFeedMedia = {
        "media": {
            "keywordsMedia": mediaArray.keywords,
            "source": mediaArray.source
        }
    };
    var newFeedYt = {
        "youtube": {

            //     "channels": youtubeArray.channelLinks,
            "boolQuery": youtubeArray.keywordsYt
          

        }
    };

    var newFeedYtChannel = {
        "youtube": {
            "channels": channelLinksYtArray
        }
    };
    var newFeedYtLinks = {
        "youtube": {
            "links": videoLinksYtArray
        }
    };

    var newFeedInsta = {
        "instagram": {
            "tracks": instagramArray.tracks,
            "follows": instagramArray.follows
        }
    };



    var ob = {};
    var tagTitle = globalTagValue;
    //  tagValueGLobal = tagTitle;
    if (update == "twitterFormat") {
        ob[tagTitle] = newFeedTwitter;
        //      var feed = newFeedTwitter;
    } 
    else if (update == "twitterFormatDelete") {
        ob[tagTitle] = newFeedTwitter;
         $('.configs-gnip-update').fadeIn();
         $('.configs-gnip-update').addClass('influencer-loader');
         $('.configs-gnip-update').html('deleting from gnip <img src="../img/loading.gif">');
        deleteGnipRule(deleteRule,user, globalTagValue);
    
    }

     else if (update == "twitterFormatFollows") {
        ob[tagTitle] = newFeedTwitter;
       
    }




    else if (update == "faceBookFormat") {
        ob[tagTitle] = newFeedFb;
        //      var feed = newFeedFb;
        //      feedArray = newFeedFb;
    } else if (update == "mediaFormat") {
        ob[tagTitle] = newFeedMedia;
        //      var feed = newFeedMedia;
        //      feedArray = newFeedMedia;
    } else if (update == "youtubeFormat") {
        ob[tagTitle] = newFeedYt;
        //      var feed = newFeedYt;
        //      feedArray= newFeedYt;
    } else if (update == "channelYtFormat") {
        ob[tagTitle] = newFeedYtChannel;
    } else if (update == "videoLinkYtFormat") {
        ob[tagTitle] = newFeedYtLinks;
    } else if (update == "instagramFormat") {
        ob[tagTitle] = newFeedInsta;
        //      var feed = newFeedInsta;
        //      feedArray= newFeedInsta;

    } else if (update == "faceBookFormatGroups") {
        ob[tagTitle] = newFeedFbGroups;
    }



    if (!isNull(tagTitle)) {
        esClientAdmin.update(user, ob, function(response) {
            var error = null;
            if (error) {
                alert("Could not update your settings");
            } else {
                alert("Succesfully updated your settings");

                updateGlobalSource();
            }

        });

    }


};

var addGnipRule = function(data,user, globalTagValue) {
    // alert("vfs");
      $('.configs-gnip-update').fadeIn();
      $('.configs-gnip-update').addClass('influencer-loader');
      $('.configs-gnip-update').html('updating to gnip <img src="../img/loading.gif">');

  

    console.log('data with present and old rule', data)


    $.ajax({
        type: "POST",
        url: esHostServer + "/_addGnip",

        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({data:data}),
        dataType: "JSON",
        success: function(data) {
//            alert("successfull added to gnip");
            $('.configs-gnip-update').removeClass('influencer-loader');
	        $('.configs-gnip-update').html('Gnip successfully updated');
            $('.configs-gnip-update').fadeOut(1000);
        },
        error: function(error) {
            alertError(err.responseJSON.error);
            alert("error in adding to gnip");

        }

    });



};



var deleteGnipRule = function(deleteRule,user, globalTagValue) {

    console.log("delete bool Query.....",deleteRule);
     var info=[];
      info=deleteRule;

    $.ajax({
        type: "POST",
        url: esHostServer + "/_deleteGnip",

        contentType: 'application/json; charset=utf-8',
        data:JSON.stringify({deleteRule:info}),
        dataType: "JSON",
        success: function(data) {
            $('.configs-gnip-update').removeClass('influencer-loader');
            $('.configs-gnip-update').html('Gnip successfully deleted');
            $('.configs-gnip-update').fadeOut(1000);
        },
        error: function(err) {
              alertError(err.responseJSON.error);
            alert("error in deleting from gnip");

        }

    });



};


$('body').on('click', '#tagsModalDeleteNo', function() {
    $('#smallModal').hide();
});

$('body').on('click', '#tagsModalAdd', function() {
    console.log('addition working here')
    var tagValue = $('#idsTagsAddition').val();
    tagField = $('#idsTagsAddition').val();
    if (isEmpty(tagValue)) {
        alert('Empty spaces not allowed');
    } else {
        var tagObject = {};
        var tagsArray = [];
        tagsArray = $('#idsTagsAddition').val().split(","); //addition of multiple tags. Here we split the words at "," and add to tags
        var index = 0;
        var length = tagsArray.length;

        for (index; index < length; index++) {
            tagObject[tagsArray[index]] = {};

        }

        tagsAdditionSuccessShow(tagObject, tagsArray);
    }



});

var tagsAdditionSuccessShow = function(tagObject, tagsArray) {
    fbArray = [];
    fbGroupsArray = [];
    channelLinksYtArray = [];
    videoLinksYtArray=[];

    if (!isNull(tagObject)) {
        esClientAdmin.update(user, tagObject, function(error, responseUpdate) {
            console.log('initial response', responseUpdate);
            if (responseUpdate == "success") {
                $('.successNotification').show();
                $('.successNotification').fadeOut(3000);
                initialKeyWordsValue(tagsArray);
                esClientAdmin.updateGet(user, function(response) {
                    $("#pageFbForm").html($("#fbLinkRender").render(["1"]));
                    $("#groupFbForm").html($("#fbGroupLinkRender").render(["1"]));
                    $("#channelYtForm").html($("#channelYtLinkRender").render(["1"]));
                    $("#linkYtForm").html($("#videoYtLinkRender").render(["1"]));

                    console.log('inner response', response);
                    //displayAdminPanel(response);
                    initialTagValueGen(response);
                    $('.tagDropdownButton').empty();
                    tagsAutoLoad(response);
                  
                });
                updateGlobalSource();

            } else {
                alert("Error,queries not indexed");
            }
        });
    } else {
        alert("tag value null or empty");
    }

}

var initialKeyWordsValue = function(tagsArray) {
    globalTagValue = tagsArray[0];
    gnipTagValue = globalTagValue;
   

    $('.adminSelectTag').html(tagTrimmer(globalTagValue));
    $('#dropdownMenu1').attr('title', globalTagValue);
    $('.tagsAddition').html('<i class="ion-pricetag"></i>' + globalTagValue + '<i class="ion-ios-arrow-down"></i>');
    $('#keywordsTwitter').val('');
    $('#idsHandlesTwitter').val('');
    $('#pageLinkFb').val('');
    $('#keywordsYt').val('');
    $('#channelLinksYt').val('');
    $('#youtubeLinks').val('');
    $('#keywordsInsta').val('');
    $('#followsInsta').val('');
    $('#keywordsMedia').val('');
}

$('body').on('click', '.removeQuery', function(e) {
    e.preventDefault();
    var tagSelectCount = parseInt($(this).parent().attr('rvalue'));

    tagDeleteId = diff[tagSelectCount];
    $('.deleteNotification').hide();
    $('.deleteEntry').html(tagDeleteId);

    $('#smallModal').modal('show');
});



$('body').on('click', '#tagsModalDelete', function(e) {
    e.preventDefault();
     var twitterRuleArray=[];
    var twitterRuleFollows=[];
      if((globalConfig._source[tagDeleteId].twitter !== undefined)&&(globalConfig._source[tagDeleteId].twitter.boolTracks !== undefined)){
          twitterRuleArray=globalConfig._source[tagDeleteId].twitter.boolTracks;


      }
    
       if((globalConfig._source[tagDeleteId].twitter !== undefined)&&(globalConfig._source[tagDeleteId].twitter.follows !== undefined)){
            if(globalConfig._source[tagDeleteId].twitter.follows!==null){
              twitterRuleFollows=globalConfig._source[tagDeleteId].twitter.follows;
             
            }
             
      

       }

         deleteRuleFollows(twitterRuleArray,twitterRuleFollows);  
   

   



    var newGlobalConfig = globalConfig;
    var deleteFlag = delete newGlobalConfig._source[tagDeleteId];
    
    esClientAdmin.reindex(user, newGlobalConfig._source, function(error, responseRemove) {
        if (responseRemove == "success") {
            $('.deleteNotification').show();
            $('.deleteNotification').fadeOut(3000);
            $('.tagDropdownButton').empty();

            $('#smallModal').hide();

            esClientAdmin.updateGet(user, function(response) {
                $('.tagDropdownButton').empty();
                $('#keywordsTwitter').val("");
                tagsAutoLoad(response);
                initialTagValueGen(response);
                if (!isNull(response._source)) {
                    $('.tagsAddition').html('<i class="ion-pricetag"></i>' + diff[0] + '<i class="ion-ios-arrow-down"></i>');
                    $('.adminSelectTag').html(tagTrimmer(diff[0]));
                    $('#dropdownMenu1').attr('title', globalTagValue);
                    $("#0").click();
                }

            });

        } else {
            alert('remove operation failed');
        }
    });
});



$("body").on("click", ".fbAddBtn", function(e) {
    e.preventDefault();
    //     var indexNum = $(this).parent().attr('index');
    //     alert(indexNum);
    //  fbArray = [];
    var n = fbArray.length + 1;


    fbField = $(this).parent().parent().find(".pageLinkFb").val();
    //alert(fbField);
    var data = {
        "page": fbField
    };
    var structure = $(' <ul class="vertical-list"> <li class="pageFbDivRender' + n + '"><span class="col-md-2"> <label class="control-label" id="pageLink" for="username">Page Link</label></span><span class="col-md-7"><input name="username"  type="text" class="pageLinkFb form-control"></span><span class="col-md-1"><button class="btn btn-primary fbAddBtn "> <img src="img/follower.gif"  class="loading_image" style="display: none;"><span class="fa fa-plus-circle addLikefb"></span></button></span><span><button class="btn btn-danger fbremoveBtn" indexVal=' + n + ' ><span class="fa fa-times"></span></button></span></li></ul>');

    var loader = $(this).children('.loading_image');
    var fbFont = $(this).children('.addLikefb');
    loader.show();
    fbArray.push(fbField);
    var id = "faceBookFormat";
    updateFn("configs", "config", user, id);
    loader.hide();
    fbFont.removeClass("fa-plus-circle").addClass("fa-check");
    $(this).addClass("disabled");
    $('#pageFbForm').append(structure);


});


$("body").on("click", ".fbremoveBtn", function(e) {
    e.preventDefault();
    var index = $(this).attr("indexVal");
    if (index > -1) {
        fbArray.splice(index, 1);
        var id = "faceBookFormat";
        updateFn("configs", "config", user, id);
        $('.pageFbDivRender' + index).remove();
    }



});

var fbFailureReason = function(fbArray, failureReasonArray) {
    //alert("entered");
    var index = 0;
    var failureIndex = 0;
    var fblink;
    console.log("failure reason", failureReasonArray);


    for (failureIndex; failureIndex < failureReasonArray.length; failureIndex++) {

        for (index; index < fbArray.length; index++) {
            fblink = fbArray[index].split('?');

            if (fblink[0] == failureReasonArray[failureIndex].url) {
                //alert(failureReasonArray[failureIndex].issue);
                $('.failureMessage' + index).html("Issue:" + failureReasonArray[failureIndex].issue);

            }
        }

    }
}


var fbFailureReasonGroups = function(fbGroupsArray, failureReasonArrayGroups) {
    //alert("entered");
    var index = 0;
    var failureIndex = 0;
    var fblink;
    console.log("failure reason", failureReasonArrayGroups);


    for (failureIndex; failureIndex < failureReasonArrayGroups.length; failureIndex++) {

        for (index; index < fbGroupsArray.length; index++) {
            fblink = fbGroupsArray[index].split('?');

            if (fblink[0] == failureReasonArrayGroups[failureIndex].url) {
                //alert(failureReasonArray[failureIndex].issue);
                $('.failureMessageGroup' + index).html("Issue:" + failureReasonArrayGroups[failureIndex].issue);

            }
        }

    }
}



$("body").on("click", ".fbGroupAddBtn", function(e) {
    e.preventDefault();
    //     var indexNum = $(this).parent().attr('index');
    //     alert(indexNum);
    //  fbArray = [];
    var n = fbGroupsArray.length + 1;


    fbGroupField = $(this).parent().parent().find(".groupLinkFb").val();
    //alert(fbField);
    var data = {
        "groups": fbGroupField
    };
    var structure = $(' <ul class="vertical-list"> <li class="groupFbDivRender' + n + '"><span class="col-md-2"> <label class="control-label" id="groupLink" for="username">Group Link</label></span><span class="col-md-7"><input name="username"  type="text" class="groupLinkFb form-control"></span><span class="col-md-1"><button class="btn btn-primary fbGroupAddBtn "> <img src="img/follower.gif"  class="loading_image" style="display: none;"><span class="fa fa-plus-circle addLikefb"></span></button></span><span><button class="btn btn-danger fbGroupRemoveBtn" indexVal=' + n + ' ><span class="fa fa-times"></span></button></span></li></ul>');

    //alert( $(this).find("#loading_image").html());
    //$(this).children('.loading_image').show();
    var loader = $(this).children('.loading_image');
    var fbFont = $(this).children('.addLikefb');
    loader.show();
    fbGroupsArray.push(fbGroupField);
    var id = "faceBookFormatGroups";
    updateFn("configs", "config", user, id);
    loader.hide();
    fbFont.removeClass("fa-plus-circle").addClass("fa-check");
    $(this).addClass("disabled");
    $('#groupFbForm').append(structure);


});


$("body").on("click", ".fbGroupRemoveBtn", function(e) {
    e.preventDefault();
    var index = $(this).attr("indexVal");
    if (index > -1) {
        fbGroupsArray.splice(index, 1);
        var id = "faceBookFormatGroups";
        updateFn("configs", "config", user, id);
        $('.groupFbDivRender' + index).remove();
    }
});



var langTarget = "en";
var translatedText;
// $('body').on('click', '#translateBtn', function(e) {
// alert(" x");
// e.preventDefault();
var searchConduct = function() {
        var selectedLanguage = $(".search-language").find('.select2-chosen').text();
        $.each(googleLanguages, function(index, data) {
            if (data.name == selectedLanguage)
                langTarget = data.language;

        });

        //alert(langTarget);


        var text = $.trim($('.search-box').val());
        //alert(text);

        $('#translatedValue').html("");
        if (text.length > 0) {

            var langSource = "en";


            var apiurl = "https://www.googleapis.com/language/translate/v2?key=" + translateApiKey +
                "&source=" + langSource + "&target=" + langTarget + "&q=";
            console.log('google lang apiurl', apiurl);

            $.ajax({
                url: apiurl + encodeURIComponent(text),
                dataType: 'jsonp',
                async: false,
                success: function(response) {
                    // alert("success");
                    console.log("translated data");
                    //        var data=JSON.parse(data.translations);
                    if (response.data)
                        translatedText = response.data.translations[0].translatedText;
                    // alert(translatedText);
                    if (translatedText)
                        $('#translatedValue').val(translatedText);
                    else
                        $('#translatedValue').val(text);
                    //alert("done");
                    // translateSearch(translatedText);
                },
                error: function(error) {
                    console.log('error', error);

                }

            });

        }
    }


var translateSearch = function(searchField) {


    $(".search-box").val(searchField);

    globalObject.onSearch(searchField);

}

$(document).ready(function() {


    $(".search-language").select2({
        data: laguageSelectionArray()
    });

    $('div.search-advance,  div.search-overlay').hide();
    $('.search-box').focus(function() {
        $('div.search-advance,  div.search-overlay').css('display', 'block');


        $.each(googleLanguages, function(index, data) {
            $('.languageDropdown').append('<li id="translateLanguage' + index + '" class="translateLangKey" translateLangId="' + data.language + '" translateLangName="' + data.name + '" role="presentation"><a role="menuitem" tabindex="-1">' + data.name + '</a></li>');
        });
        // for(count; count < length ; count++){
        slimScrollCall("slimScrollTranslate", "200px");
        //     $('.tagDropdown').append('<li id="listId'+diffIndex[count]+'" class="liTags active" tagnamepick="'+diffIndex[count]+'" role="presentation"><a role="menuitem" tabindex="-1">'+diffIndex[count]+'</a></li>');


        // }
    });

});

$('.advance-close, .search-btn, .advance-search-btn').click(function() {
    $('div.search-advance,  div.search-overlay').fadeOut('medium');
});

$('div.search-overlay').click(function() {
    // alert("zcvzc");
    $('div.search-advance,  div.search-overlay').fadeOut('medium');
});

$('body').on('click', '.translateLangKey', function(e) {

    e.preventDefault();
    langTarget = $(this).attr("translateLangId");
    langName = $(this).attr("translateLangName");
    //alert(langName);
    $("#selectedLanguage").html(langName);


});



$('body').on('click', '#advanceSearchButton', function(e) {

    e.preventDefault();
    if (isNull(translatedText)) {
        translatedText = $('.search-box').val()
    }
    translateSearch(translatedText);
});



var laguageSelectionArray = function() {
    var count = 0;
    var sampleArray = [];
    for (count; count < googleLanguages.length; count++) {
        //alert( googleLanguages.name);
        sampleArray[count] = ({
            id: count,
            text: googleLanguages[count].name
            // lan: googleLanguages[count].language


        });

    }
    return sampleArray;

}

$(document).ready(function() {


    $(".choose-language").select2({
        data: laguageSelectionArray()
    });


    slimScrollCall("slimScrollTranslate", "200px");


});

$('body').on('click', '#addTranslatedKeywords', function(e) {

    e.preventDefault();
    var translatedText = $('#keywordsTranslated').val();
    // $('#keywordsTwitter').val($('#keywordsTwitter').val()+' '+$('#keywordsTranslated').val());
    // $('#keywordsTranslated').val("");
    addtokeyWords(translatedText);

});



$('body').on('keyup', '.translateKeywords', function() {

    var text = $.trim($(this).val());
    if (text.length > 0) {


        var langSource = "en";
        var selectedLanguage = $(".choose-language").find('.select2-chosen').text();

        $.each(googleLanguages, function(index, data) {
            if (data.name == selectedLanguage)
                langTarget = data.language;

        });
        // alert(langTarget);


        var apiurl = "https://www.googleapis.com/language/translate/v2?key=" + translateApiKey +
            "&source=" + langSource + "&target=" + langTarget + "&q=";

        $.ajax({
            url: apiurl + encodeURIComponent(text),
            dataType: 'jsonp',
            async: false,
            success: function(response) {
                // alert("success");
                //console.log("translated data",data);
                //        var data=JSON.parse(data.translations);
                if (response.data)
                    translatedText = response.data.translations[0].translatedText;
                // alert(translatedText);
                //  addtokeyWords(translatedText);
                $('#keywordsTranslated').val(translatedText);

                //alert("done");
                // translateSearch(translatedText);
            }

        });

    }

    // $('#searchButton').click();

});

var addtokeyWords = function(translatedText) {



    if (currentWizardIndex == 0)
        $('#keywordsTwitter').val($('#keywordsTwitter').val() + ' ' + translatedText);

    else if (currentWizardIndex == 2)
        $('#keywordsInsta').val($('#keywordsInsta').val() + ' ' + translatedText);

    else if (currentWizardIndex == 3)
        $('#keywordsYt').val($('#keywordsYt').val() + ' ' + translatedText);

    else if (currentWizardIndex == 4)
        $('#keywordsMedia').val($('#keywordsMedia').val() + ' ' + translatedText);

}


//INSTANCE 05 CHANNEL
$("body").on("click", ".channelYtAddBtn", function(e) {
    e.preventDefault();

    var n = channelLinksYtArray.length + 1;


    channelYtField = $(this).parent().parent().find(".channelLinksYt").val();
    //alert(channelYtField);
    var data = {
        "page": channelYtField
    };
    var structure = $(' <ul class="vertical-list"> <li class="channelYtDivRender' + n + '"><span class="col-md-2"> <label class="control-label" id="pageLink" for="username">Channel Link</label></span><span class="col-md-7"><input name="username"  type="text" class="channelLinksYt form-control"></span><span class="col-md-1"><button class="btn btn-primary channelYtAddBtn "> <img src="img/follower.gif"  class="loading_image" style="display: none;"><span class="fa fa-plus-circle addLikefb"></span></button></span><span><button class="btn btn-danger channelYtRemoveBtn" indexVal=' + n + ' ><span class="fa fa-times"></span></button></span></li></ul>');

    //alert( $(this).find("#loading_image").html());
    //$(this).children('.loading_image').show();
    var loader = $(this).children('.loading_image');
    var fbFont = $(this).children('.addLikefb');
    loader.show();
    channelLinksYtArray.push(channelYtField);
    var id = "channelYtFormat";
    updateFn("configs", "config", user, id);
    loader.hide();
    fbFont.removeClass("fa-plus-circle").addClass("fa-check");
    $(this).addClass("disabled");
    $('#channelYtForm').append(structure);


});


//INSTANCE 06
$("body").on("click", ".channelYtRemoveBtn", function(e) {
    e.preventDefault();
    var index = $(this).attr("indexVal");
    if (index > -1) {
        channelLinksYtArray.splice(index, 1);
        var id = "channelYtFormat";
        updateFn("configs", "config", user, id);
        $('.channelYtDivRender' + index).remove();
    }
});

$("body").on("click", ".videoYtAddBtn", function(e) {
    e.preventDefault();

    var n = videoLinksYtArray.length + 1;


    videoLinkYtField = $(this).parent().parent().find(".videoLinksYt").val();
    //alert(videoLinkYtField);
    var data = {
        "page": videoLinkYtField
    };
    var structure = $(' <ul class="vertical-list"> <li class="videoYtDivRender' + n + '"><span class="col-md-2"> <label class="control-label" id="pageLink" for="username">Video Link</label></span><span class="col-md-7"><input name="username"  type="text" class="videoLinksYt form-control"></span><span class="col-md-1"><button class="btn btn-primary videoYtAddBtn "> <img src="img/follower.gif"  class="loading_image" style="display: none;"><span class="fa fa-plus-circle addLikefb"></span></button></span><span><button class="btn btn-danger videoYtRemoveBtn" indexVal=' + n + ' ><span class="fa fa-times"></span></button></span></li></ul>');

    //alert( $(this).find("#loading_image").html());
    //$(this).children('.loading_image').show();
    var loader = $(this).children('.loading_image');
    var fbFont = $(this).children('.addLikefb');
    loader.show();
    videoLinksYtArray.push(videoLinkYtField);
    var id = "videoLinkYtFormat";
    updateFn("configs", "config", user, id);
    loader.hide();
    fbFont.removeClass("fa-plus-circle").addClass("fa-check");
    $(this).addClass("disabled");
    $('#linkYtForm').append(structure);


});

$("body").on("click", ".videoYtRemoveBtn", function(e) {
    e.preventDefault();
    var index = $(this).attr("indexVal");
    if (index > -1) {
        videoLinksYtArray.splice(index, 1);
        var id = "videoLinkYtFormat";
        updateFn("configs", "config", user, id);
        $('.videoYtDivRender' + index).remove();
    }
});


var twitterLiveInterval;

function locationHashChanged() {
    console.log("got");
    clearInterval(twitterLiveInterval);

}

window.onhashchange = locationHashChanged;

var appendTwitterKeyword=function(value,index,temp){
  /*  var count=$('.twitterKeywordListAll > li').length;
    alert(count);*/
    // alert(value);
    value=value ||'';
    // var replacedValue=value.replace(/"/g,"$");
    // replacedValue='"'+replacedValue+'"'

    var keywordTemplate=' <li class="clearfix">'+
              '<div class="twitterKeywordList clearfix">'+
                '<span class=" col-md-2">'+
            '<label class="control-label"  for="username">Query</label>'+
            '</span> <span class="twitterKeyVal col-md-8">'+
            '<textarea id="keywordsTwitter" rows="6" class="form-control clearfix twitterKey'+index+'" type="text">'+value+'</textarea>'+
           '<a type="button" class="ask-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" ></a>'+

            '</span>'+
            '<span class="col-md-2">'+

        '<button class="btn btn-danger twitterkeyRemoveBtn" data-toggle="tooltip" data-placement="top" title="Delete Rule">'+
           ' <i class="fa fa-times"></i>'+
        ' </button>'+

            '</span>'+
    '</div>'+
    '</li>';
     
     $(' .twitterKeywordListAll').prepend(keywordTemplate);
      $('.twitterKey'+index).data('content',value);
         
  
    
    

};

$("body").on("click", ".twitteraddKeyBox", function(e) {
    e.preventDefault();
    alert($('.twitterKeywordList').length);

    appendTwitterKeyword("","",true);
  

   
});

// $("body").on("click", ".twitterkeyAddBtn", function(e) {
//     e.preventDefault();

//     var value=$(this).parent().parent().find("#keywordsTwitter").val();
//     updateboolTracks(value,$(this));

   
// });

$("body").on("click", ".idHandleAddBtn", function(e) {
    var gnipRuleArray=[];
    e.preventDefault();
    var prevVal="";

      if((globalConfig._source[globalTagValue].twitter !== undefined) &&(globalConfig._source[globalTagValue].twitter.follows !== undefined) ){
            if(globalConfig._source[globalTagValue].twitter.follows !==null){
                prevVal=(globalConfig._source[globalTagValue].twitter.follows).toString();

            }
            
        }
      
     var curVal=$(this).parent().parent().find("#idsHandlesTwitter").val();
     curVal=curVal.trim();
     prevVal=prevVal.trim();

         if(prevVal){
          var oldRuleVal=convertToGnipRule(prevVal);  
         }
         if(curVal){
         var presentruleVal=convertToGnipRule(curVal);   
         }
  
        oldRule={
           "tag":user + '%%%' + globalTagValue,
            "value": oldRuleVal


        }

        presentRule={
            "tag":user + '%%%' + globalTagValue,
            "value":presentruleVal

        }

        var rulesObj = {
           "presentRule": presentRule,
           "oldRule": oldRule
         };
       
      
       gnipRuleArray.push(rulesObj);

        twitterArray.idsHandles=curVal;

           if(curVal){
              var id = "twitterFormat";

              updateFn("configs", "config", user, id);
              addGnipRule(gnipRuleArray,user, globalTagValue);


      
           }
          else{
             deleteRule=[{
              "tag":user + '%%%' + globalTagValue,
              "value":oldRuleVal

             }];
             var id = "twitterFormatDelete";
             updateFn("configs", "config", user, id);

          }
       
     // alert(prevVal);
    // alert(prevVal);
    // updateboolTracks(value,$(this));

   
});


$("body").on("click", ".twitterkeyRemoveBtn", function(e) {
    e.preventDefault();
      var deleteRuleVal;
      var prevVal=$(this).parent().parent().find("#keywordsTwitter").data('content');
      var curVal=$(this).parent().parent().find("#keywordsTwitter").val();
      curVal=curVal.trim();

       if(twitterArray.boolTracks.indexOf(curVal)>-1){
            deleteRuleVal=curVal;
            var index=twitterArray.boolTracks.indexOf(curVal); 
            twitterArray.boolTracks.splice(index,1);
            deleteRuleVal=curVal;
         }
       else if(twitterArray.boolTracks.indexOf(prevVal)>-1){
            var index=twitterArray.boolTracks.indexOf(prevVal); 
            twitterArray.boolTracks.splice(index,1);
            deleteRuleVal=prevVal;

       }  

        deleteRule=[{
            "tag":user + '%%%' + globalTagValue,
            "value":deleteRuleVal

        }];


        var id = "twitterFormatDelete";

        updateFn("configs", "config", user, id);

      // console.log("twitter array.......", twitterArray.boolTracks);

      $(this).parent().parent().parent().remove();
  

   
});

/*var updateboolTracks=function(value,$this){
     

    var string=value.trim();
    if (string.indexOf(',') !== -1) {
        alert('Keywords should be in boolean format');
        return true;
    }
    if (/\bor\b/.test(string)) {
        alert("Keywords should not allow 'or'");
        return true;
    }

    twitterArray.idsHandles = $('#idsHandlesTwitter').val().split(",");
        if((globalConfig._source[globalTagValue].twitter !== undefined) &&(globalConfig._source[globalTagValue].twitter.boolTracks !== undefined) ){
            twitterArray.boolTracks=globalConfig._source[globalTagValue].twitter.boolTracks; 
        }
        else{
           twitterArray.boolTracks=[]; 
        }
   
   
    if(twitterArray.boolTracks.indexOf(string) <= -1){
       var prevVal=$this.parent().parent().find("#keywordsTwitter").data('content');
         if(twitterArray.boolTracks.indexOf(prevVal)>-1){
            var index=twitterArray.boolTracks.indexOf(prevVal); 
            twitterArray.boolTracks.splice(index,1);
         }      
      
       twitterArray.boolTracks.push(string);
       var oldRuleVal=prevVal;

       $this.parent().parent().find("#keywordsTwitter").data('content',string);
        
       var presentruleVal=string
       

        oldRule={
           "tag":user + '%%%' + globalTagValue,
            "value": oldRuleVal


        }

        presentRule={
            "tag":user + '%%%' + globalTagValue,
            "value":presentruleVal

        }


       esClientValidate.addQueryString("_all",string);
       esClientValidate.validateQuery(validateTwitterQuery);
       
        console.log("twitter array.......", twitterArray.boolTracks);

    }

    else{

        alert("Already contains same query");
    }
   



    

}
*/

deleteRuleFollows=function(ruleArray,ruleFollows){
    deleteRule=[];
    var followRule=ruleFollows.toString();
       followRule=convertToGnipRule(followRule);
       console.log("ruleArray.....",ruleArray);
       console.log("followRule.....",followRule);
       deleteRule=ruleArray.concat(followRule);

       var id="twitterFormatDelete";
       updateFn("configs", "config", user, id);







}

var convertToGnipRule=function(string){
        var ruleVal="";
       
/*
        var followsPullSave;
          if ((globalConfig._source[globalTagValue].follows !== undefined) &&(globalConfig._source[globalTagValue].twitter.follows !== undefined)) {
                 if(globalConfig._source[globalTagValue].twitter.follows[0]){
                     followsPullSave = (globalConfig._source[globalTagValue].twitter.follows).toString();*/
              if(string!==""){
                  ruleVal = powerTrackFormatConversion("from:",string.trim());
              }
           
                      // ruleVal= boolTracks + ' ' + followsPullSave;

                 // }
             // console.log("follows.....",globalConfig._source[globalTagValue].twitter.follows);
             
           
       
       
           
      /*  } else {
            ruleVal= boolTracks;
        }*/
        return ruleVal;
 



}
$(document).ready(function(){
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    $('[data-toggle="tooltip"]').tooltip(); 
    $('.btn ').tooltip();
});