var srID = ($('#srID').val());
var iterationsID =($('#iterationsID').val());
var hostname = ($('#fileName').val());
var itID = $('#iterationID');
var  fileID= $('#fileName');


function applyAutoComplete(id, field, dataFunction) {
//	alert("Appying on -#" + id);
	$("#" + id).select2({
		placeholder: "Search SR",
		minimumInputLength: 0,
		initSelection : function(x,y){
		},
		ajax: {
			url: "http://104.131.229.234:9200/logs/logs/_search?search_type=count",
			dataType: 'json',
			quietMillis: 100,
			type: 'POST',
			data: dataFunction,
			results: function (data, page) {
				console.log(data);
				var results = [];
				var totalEventBuckets = data.aggregations.ids.buckets;
				console.log(totalEventBuckets);		
				for (var index in totalEventBuckets) {
					var value = data.aggregations.ids.buckets[index].key;
					results.push({
						id: value + "" ,
						text: value + ""
					});
				}
				console.log(results);
				return {
					results: results
				};
			}
		},
		escapeMarkup: function (m) {
			return m;
		} // we do not want to escape markup since we are displaying html in results
	});
}

var srIDdataFunction = function (term, page) { // page is the one-based page number tracked by Select2
	console.log($('#iterationID'));
	itID.select2("val" , null);
	fileID.select2("val", null);
	
	var query = {
		"aggs": {
			"ids": {
				"terms": {
					"field": "serviceRequest"
				}
			}
		}
	};
	if (term.length > 0) {
		var q = {
			"prefix": {}
		};
		q.prefix[field] = term;
		query.query = q;
	}
	return JSON.stringify(query);
};
var iterationsDataFunction = function (term, page) { // page is the one-based page number tracked by Select2
	fileID.select2("val", null);
	var query = {
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							serviceRequest: $('#srID').val()
						}
					}
				]
			}
		},
		"aggs": {
			"ids": {
				"terms": {
					"field": "iteration"
				}
			}
		}
	};
	if (term.length > 0) {
		var q = {
			"prefix": {}
		};
		q.prefix['iteration'] = term;
		query.query.bool.must.push(q);
	}
	return JSON.stringify(query);
};
var hostnameDataFunction = function (term, page) { // page is the one-based page number tracked by Select2
	var query = {
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							serviceRequest: $('#srID').val()
						}
								},
					{
						"term": {
							"iteration": $('#iterationID').val()
						}
					}
				]
			}
		},
		"aggs": {
			"ids": {
				"terms": {
					"field": "hostname"
				}
			}
		}
	};
	if (term.length > 0) {
		var q = {
			"prefix": {}
		};
		q.prefix[field] = term;
		query.bool.must.push(q);
	}
	return JSON.stringify(query);
};
var srHide =function(){
	$('#fileName').select2({data:null});
	$('#iterationID').select2({data:null});
//	alert("inside srHide");
};
var iterationHide =function(){
//	$('#fileName').select2("val",null);
//		alert("inside iterationHide");
}
//$(document).ready(function () {
//	$(document).on('click', $('#srID').select2, function () {
//			alert(results[1]);
//		});
//
//});

applyAutoComplete("srID", "hostGroupName", srIDdataFunction);
applyAutoComplete("iterationID", "iteration", iterationsDataFunction);
applyAutoComplete("fileName", "messageList.hostname",hostnameDataFunction);

