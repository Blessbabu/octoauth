var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var path = require('path');
var cors = require('cors');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var ejs = require('ejs')

var configDB = require('./config/database.js');

mongoose.connect(configDB.url); 

require('./config/passport')(passport); 


app.use(morgan('dev')); 
app.use(cors())
app.use(cookieParser()); 
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

//app.set('view engine', 'ejs'); 
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);

app.use(session({ secret: 'secret#fact' })); 

app.use(passport.initialize());
app.use(passport.session()); 
app.use(flash());

app.use(express.static(__dirname+"/public/octoviz", { maxAge:'7d'}));
//app.use("/home", express.static(__dirname + "/public/home"));

require('./app/routes.js')(app, passport); 
require('./app/esroute.js')(app); 

app.listen(port);
console.log('Listening on port ' + port);
