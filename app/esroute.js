var es = require('./Clients/ES-Server.js');
var foamTree = require('./Clients/foamTree-Server.js');
var Elastic = require('./common/elasticsearch');
var config = require('./configs.js');
var moment = require('moment');
var esClient = require('./services/es.js');
var Gnip = require('gnip');
var esClient = require('./common/esTweet.js');
var elastic = new Elastic(config.esHost, null, null, null);
var async = require('async');

var rules = new Gnip.Rules({
  url: 'https://api.gnip.com:443/accounts/Octobuz/publishers/twitter/streams/track/prod/rules.json',
  user: 'info@octobuz.com',
  password: 'octobuz123',

});



module.exports = function(app) {

  app.post('/_query', checkAuthentication, function(req, res) {
    // console.log("user email query", req.user.local.email);
    console.log("body query", req.body);
    es.search(req.body, req.user.local.email, function(err, response) {

      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in search ' + err
          });
        }

      } else {
        res.send(response);
      }
    })
  });

  app.get('/getFeeds',function(req, res) {
    var data={
      "size": 10,
      "aggs": {
        "positive": {
          "filter": {
            "term": {
              "senti.overallSentiment": "positive"
            }
          },
          "aggs": {
            "tweets": {
              "top_hits": {
                "size": 10,
                "sort": [
                  {
                    "retweet_count": {
                      "order": "desc"
                    }
                  }
                ]
              }
            }
          }
        }
      },
      "query": {
        "function_score": {
          "query": {
            "bool": {
              "must": []
            }
          },
          "filter": {
            "bool": {
              "must": [
                {
                  "terms": {
                    "ownedBy.id": [
                      "glo.octobuz@gmail.com%%%M.anifest"
                    ]
                  }
                },
                {
                  "range": {
                    "created_at": {
                      "gte": "now-1w",
                      "format": "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
                    }
                  }
                }
              ],
              "should": [],
              "must_not": [
                {
                  "terms": {
                    "_id": {
                      "index": "newsentiments",
                      "type": "relevancy",
                      "id": "glo.octobuz@gmail.com",
                      "path": "hide.twitter.raw",
                      "cache": false
                    }
                  }
                }
              ]
            }
          }
        }
      }
    };
    esClient.search('twitter*','',data, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in search ' + err
          });
        }

      } else {
        res.send(response);
        console.log("responce from twitter*.........",response);
      }
    })
  });



  app.post('/_queryFoamTree', function(req, res) {
    console.log("user email foamTree", req.user.local.email);
    console.log("body FoamTree", req.body);
    foamTree.search(req.body, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in foam tree search  ' + err
          });
        }

      } else {
        res.send(response);
      }

    })
  });




  app.post('/_updateContent', function(req, res) {

    elastic.updateContent(req.body.indexType, req.body.data, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in updating content  ' + err
          });
        }

      } else {
        res.send(response);
      }
    });



  });


  app.post('/_reindex', saveConfig, function(req, res) {

    console.log("reindex....", req.body.data);

    elastic.reindex(req.body.id, req.body.data, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in reindex  ' + err
          });
        }

      } else {
        res.send(response);
      }
    });


  });

  app.post('/_update', saveConfig, function(req, res) {

    console.log("update....", req.body);

    elastic.update(req.body.id, req.body.docData, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in updating  ' + err
          });
        }

      } else {
        res.send(response);
      }
    });



  });

  app.post('/_updateRest', function(req, res) {

    // console.log("update....", req.body);

    elastic.updateRest(req.body.id,req.body.index,req.body.type,req.body.docData, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server error in updating  ' + err
          });
        }

      } else {
        res.send(response);
      }
    });



  });

  app.post('/_updateGet', function(req, res) {

    console.log("update get....", req.body);

    elastic.updateGet(req.body.id, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server in updateGet' + err
          });
        }

      } else
      res.send(response);
    });


  });

  app.post('/_mse', function(req, res) {

    // console.log("msearch", req.body);
    es.multiSearch(req.body.data, req.user.local.email, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server in multisearch' + err
          });
        }

      } else
      res.send(response);
    });



  });

  app.post('/_validateQuery', function(req, res) {


    es.validateQuery(req.body, req.user.local.email, function(err, response) {
      if (err) {
        if (err.message) {
          res.status(400).json({
            error: err.message
          });
        } else {
          res.status(400).json({
            error: 'internal server in validating Query' + err
          });
        }

      } else
      res.send(response);
    });


  });

  app.post('/_addGnip', function(req, res) {
    console.log(req.body.data);
    var rulesValues=req.body.data;

    async.each(rulesValues, function(ruleValue, callback) {

      var oldRule=[ruleValue.oldRule];
      var presentRule=[ruleValue.presentRule];
      console.log("present rule....",presentRule);
      if(!ruleValue.oldRule.value.trim()){

        rules.live.add(presentRule, function(err, response) {
          if (err)
          callback(err);

          else
          callback();


        });


      }
      else{
        rules.live.remove(oldRule, function(err) {

          if (err)
          callback(err);

          else {
            rules.live.add(presentRule, function(err, response) {
              if (err)
              callback(err);

              else
              callback();


            });
          }


        });



      }



    }, function(err) {

      if (err) {

        res.status(400).json({
          error: 'could not add to gnip' + err
        });
      } else {
        res.status(200).json({
          message: 'Successfully added to gnip'
        });
      }
    });

  });


  /*        async.waterfall([
  function(callback) {
  rules.live.remove(oldRules, function(err) {
  if (err){
  callback(err,null);
}

else {
callback(null,'removed');
}

});
},
function(arg1, callback) {

rules.live.add(newRules, function(err, response) {
if (err)
callback(err,null);

else
callback(null,'added');




});


}
], function (err, result) {
if (err)
res.status(400).json({
error: 'could not add to gnip' + err
});
else
res.status(200).json({
message: 'Successfully added to gnip'
});

// result now equals 'done'
});*/

//     var oldRules = [req.body.data[0].oldRule];
//     var newRules = [req.body.data[0].presentRule];

//     rules.live.remove(oldRules, function(err) {
//         if (err)
//             res.status(400).json({
//                 error: 'could not add to gnip' + err
//             });
//         else {
//             rules.live.add(newRules, function(err, response) {
//                 if (err)
//                     res.status(400).json({
//                         error: 'could not add to gnip' + err
//                     });
//                 else
//                     res.status(200).json({
//                         message: 'Successfully added to gnip'
//                     });


//             });
//         }


//     });

// });






app.post('/_deleteGnip', function(req, res) {

  var newRules =req.body.deleteRule;
  console.log("delete rule...",newRules);
  rules.live.remove(newRules,function(err) {
    if (err)
    res.status(400).json({
      error: 'could not delete from gnip' + err
    });
    else
    res.status(200).json({
      message: 'Successfully deleted from gnip'
    });



  });



});









// res.send("Sorry dude no access!!!!!!");
};

function checkAuthentication(req, res, next) {
  console.log("user details.......................", JSON.stringify(req.user));
  console.log("req body.......................", JSON.stringify(req.body));

  // console.log("req.body.prefix", req.body.prefix);
  // console.log("req.body.analyticsFacebook", req.user.permission.analyticsFacebook);
  if ((req.body.prefix == "fbdata-" && req.user.permission.analyticsFacebook == true) || (req.body.prefix == "twitter-" && req.user.permission.analyticsTwitter == true) || (req.body.index == "instagram" && req.user.permission.analyticsInstagram == true) || (req.body.index == "youtube" && req.user.permission.analyticsYoutube == true) || (req.body.index == "mediabuzz-new" && req.user.permission.analyticsMedia == true) || (req.body.index == "management-fb" && req.user.permission.smmFacebook == true) || (req.body.index == "fbmandata" && req.user.permission.smmFacebook == true)||(req.body.index == "tweetman-new" && req.user.permission.smmTwitter == true))

  {
    next();
  } else {
    res.status(401).json({
      error: 'you are not authorised to perform this action'
    });

  }

}

function saveConfig(req, res, next) {

  elastic.updateGet(req.body.id, function(err, response) {
    if (err) {
      if (err.message) {
        res.status(400).json({
          error: err.message
        });
      } else {
        res.status(400).json({
          error: 'internal server error in updateGet' + err
        });
      }

    } else {

      var response = JSON.parse(response);
      // console.log("found..................",response._id);
      var data = {
        "user": req.body.id,
        "created_at": moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
        "logs": response._source

      };

      esClient.create(data, function(err, response) {

        if (err) {
          if (err.message) {
            res.status(400).json({
              error: err.message
            });
          } else {
            res.status(400).json({
              error: 'internal server error in creating config logs ' + err
            });
          }

        } else {
          next();
        }
      });


    }

  });

}
