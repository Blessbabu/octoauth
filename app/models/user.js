
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');


var userSchema = mongoose.Schema({

    local: {
        name         : String,
        password     : String,
		email        : String,
		mobile       : String,
		country      : String,
		state        : String,
		city         : String,
		active       : Boolean,
		activateKey  : String,
		resetPasswordToken : String,
		resetPasswordExpires : Date
		
    },
    permission:{
        smmTwitter:{ type: Boolean, default:1},
        smmFacebook:{ type: Boolean, default:1},
        analyticsTwitter:{ type: Boolean, default:1},
        analyticsFacebook:{ type: Boolean, default:1},
        analyticsInstagram:{ type: Boolean, default:1},
        analyticsYoutube:{ type: Boolean, default:1},
        analyticsMedia:{ type: Boolean, default:1},
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }

});


userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};


userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};


module.exports = mongoose.model('User', userSchema);
