var graph     = require('fbgraph')
var request = require('request');
var async = require('async');
var es = require('./es');

var fbConf = {
    client_id:      '1208506172499784'
  , client_secret:  '55cb472d8b71c4c673cc5dbfa29036e7'
  , scope:          'email, user_about_me, user_birthday,user_posts,user_events,publish_actions,read_insights, user_location,pages_show_list,manage_pages,publish_pages'
  , redirect_uri:   'http://octobuz.com/auth/facebook'
};


module.exports = {
	initAuth: function (req,res,callback) {
		console.log(req.user)
		if (!req.query.code) {
    	  var authUrl = graph.getOauthUrl({
    	      "client_id":     fbConf.client_id
    	    , "redirect_uri":  fbConf.redirect_uri
    	    , "scope":         fbConf.scope
    	  });   
    	  if (!req.query.error) { //checks whether a user denied the app facebook login/permissions
    	    res.redirect(authUrl);
    	  } else {  //req.query.error == 'access_denied'
    	    res.send('access denied');
    	  }
    	  return;
    	}
    	async.waterfall([
    		function(done) {
    			console.log("init authorizing")
    			graph.authorize({
    		 		  "client_id":      fbConf.client_id
    		 		, "redirect_uri":   fbConf.redirect_uri
    		 		, "client_secret":  fbConf.client_secret
    		 		, "code":           req.query.code
                    , "scope":         fbConf.scope
    			}, function (err, facebookRes) {
    				if (err) {
    					console.log("error initializing auth")
    					done(err,null);
    					return;
    				};
    				console.log("done authorizing",facebookRes)
    				done(null,facebookRes);

    			});  		
    		},
    		function(facebookRes,done) {
        		request("https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=" + 
    		          fbConf.client_id + "&client_secret=" + fbConf.client_secret + "&fb_exchange_token=" + facebookRes.access_token, 
    		          function (error, response, body) {
    		              if (!error && response.statusCode == 200) {
    		                 token = body.split('=')[1]
    		                 t = token.split('&')[0]
    		                 console.log("got token",t)
    		                 done(null,t,facebookRes.access_token)
    		           		}
    		           		else{
    		           			console.log("error getting first access_token")
    		           			done(error,null)
    		           			return;
    		           		}
    		           	});

    		},
    		function(t,t2,done){
			console.log("token in waterfall 3 " + t)
    			request("https://graph.facebook.com/me/accounts?access_token=" + t, 
    		    	function (error, response, body) {
    		    	    if (!error && response.statusCode == 200) {
    		    	       // Show the HTML for the Google homepage. 
    		    	       console.log(body)  
    		    	       done(null,t,t2,body)                               
    		    	    }else{
    		    	    	console.log("error getting sec access_token")
    		    	    	done(error,null,null);
    		    	    	return;
    		    	    }

    		    	});
    		},
    		function(t,t2,data,done){
    			console.log("requesting id")
			console.log("token in waterfall 4 " + t)
    			request("https://graph.facebook.com/me?access_token=" + t,
    				function(error,response,body){
                        console.log("bodu",body)
    					if (!error && response.statusCode == 200) {
    						console.log(body)
    						dataJson = JSON.parse(data);
    						userJson = JSON.parse(body);
    						dataJson['userInfo'] = userJson;
						dataJson['userToken1'] = t;
						dataJson['userToken2']= t2;
						console.log("username is ", req.user)
						console.log("session", req.session)
						if(req.user){
							console.log("username",req.user)
    							dataJson['userName'] = req.user.local.email;
						}
    						es.index(userJson.id,'management-fb','credential',dataJson)
    						done(null,dataJson)

    					}else{
    						console.log("error getting id",error)
    						done(error,null)

    					}

    			})
    		}
    	],function (err, result) {
    		console.log("all Done")
    		if(err)
    			callback(err,null)
    		else
    			callback(null,result)
    		
		});

    	
		
	}


}

