var config = require('../configs');
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: config.esHost,
    log: false
});

module.exports = {

    index: function(id, indexName, indexType, data) {
        client.index({
            index: indexName,
            type: indexType,
            id: id,
            body: data
        }, function(error, response) {
            if (error) {
                console.log(error)
            } else {
                console.log(response)
            }

        });

    },
    get: function(id, callback) {
        client.get({
            index: 'contacts',
            type: 'contact',
            id: id
        }, function(error, response) {
            if (error) {
                callback(error, null)
            } else {
                callback(null, response)
            };

        });
    },
    upsert: function(id, indexName, indexType, data) {
        client.update({
            index: indexName,
            type: indexType,
            id: id,
            body: {
                script: 'update-config',
                params: {
                    "data": data
                }
            }
        }, function(error, response) {
            if (error)
                console.log(error)
            else
                console.log(response)
                // ...
        })

    },
    create: function(body,callback) {
       // console.log("found..................",body);
        client.create({
            index: 'configs-logs',
            type: 'logs',
            body:JSON.stringify(body)
        }, function(error, response) {
            if (error) {
                // console.log(error);
                callback(error, null);
            } else {
                callback(null, response);
            };
        });



    }

}

