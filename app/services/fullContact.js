var request = require('request');
var es = require('./es');

function processData(json) {
    for (var i = 0; i < json.socialProfiles.length; i++) {
    	profile = json.socialProfiles[i]
    	if (profile.username) {
    		json[profile.type] = profile.username

    	} else{
    		json[profile.type] = profile.id
    	}
    }
}



module.exports = {
	getData: function (id, callback) {
		console.log("getting full contact for id", id)
		baseUrl = "http://api.fullcontact.com/v2/person.json?"
		apiKey = "&apiKey=c5b49118b4895db8"
		url = baseUrl + id + apiKey;
		console.log("url is " , url)
		indexId = id.split("=").pop()
		console.log("indexId",indexId)
		urlObj = {}
		urlObj["id"] = id
		urlObj["url"] = url
		urlObj["indexId"] = indexId
		es.get(indexId,function(err,response){
            	if (err) {
                	console.log("error getting from elasticsearch")
                	console.log("requesting from ", urlObj.url)
                	request(urlObj.url, function (error, response, body) {
                    	  if (!error && response.statusCode == 200) {
                        	console.log("sucess")
                        	data = JSON.parse(body)
                        	processData(data);
                        	data["query"] = urlObj.id
                        	data["limit"] = response.headers["x-rate-limit-limit"]
                        	data["remainingHits"] = response.headers["x-rate-limit-remaining"]
                        	es.index(urlObj.indexId,'contacts','contact',data)
                        	console.log("fullcaontact",data)
                        	callback(null,data);
                    	  }else{
                        	console.log("no data")
                        	callback("Fullcontact: No data from api",null)
                    	  }
               	     });
            } else{
                callback(null,response._source);
            };
        })

		
    }





}
