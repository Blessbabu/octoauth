var helper = require('../common/helpers');
var moment = require('moment');
var config = require('../configs');

function FoamTreeSearch(esURL, esQueryFunction, esQueryParameters, queryParameters) {
    this.esURL = esURL;
    this.esQueryFunction = esQueryFunction;
    this.esQueryParameters = esQueryParameters;
    this.queryParameters = queryParameters;
    this.prefix = "";
    this.format = "";
    this.indicesString = "";
    this.indexType = "";

}

FoamTreeSearch.prototype = {
    constructor: FoamTreeSearch,
    addIndexType: function(indexType) {
        this.indexType = indexType
    },
    addQueryParameter: function(key, value) {
        this.esQueryParameters[key] = value;
    },
    reset: function() {
        this.queryParameters = {};
    },
    createQuery: function(callBack) {
        var esGlobalBody = this.esQueryFunction(this.esQueryParameters);
        esGlobalBody.search_request.query = {};
        esGlobalBody = this.resetQuery(esGlobalBody);
        esBody = esGlobalBody.search_request;
        for (var field in this.queryParameters) {
            if (this.queryParameters[field].type == "queryString") {
                esBody.query.bool.must.push({
                    "query_string": {
                        "default_field": field,
                        "query": this.queryParameters[field].text
                    }
                });
            } else if (this.queryParameters[field].type == "termMatch") {
                var termQuery = {
                    "terms": {}
                };
                termQuery.terms[field] = this.queryParameters[field].values;
                if (this.queryParameters[field].values.length != 0) {
                    esBody.query.bool.must.push(termQuery);
                }
            } else if (this.queryParameters[field].type == "termLookupMatch") {
                var termQuery = {
                    "terms": {}
                };
                var params = this.queryParameters[field].params;
                termQuery.terms[field] = {
                    "index": params.index,
                    "type": params.indexType,
                    "id": params.docID,
                    "path": params.field,
                    "cache": false
                };
                esBody.query.bool.must.push(termQuery);
            }

            if (this.queryParameters[field].type == "dateRange") {
                var rangeQuery = {
                    "range": {}
                };
                rangeQuery.range[field] = {
                    "gte": this.queryParameters[field].from,
                    "lte": this.queryParameters[field].to,
                    "format": "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
                }
                if (this.queryParameters[field].from == null) {
                    delete rangeQuery.range[field].gte;
                }
                if ((this.queryParameters[field].to == null) || (this.queryParameters[field].to == '')) {
                    delete rangeQuery.range[field].lte;
                }
                esBody.query.bool.must.push(rangeQuery);
            }
        };

        console.log("esBody foamTree", esBody);

        callBack(esGlobalBody);

    },


    addQueryString: function(field, text) {
        if (field == null) {
            field = "_all";
        }
        if (isWhitespaceOrEmpty(text)) {
            text = "*";
        }
        this.queryParameters[field] = {
            type: "queryString",
            text: text
        };
    },
    addTermLookupMatch: function(referanceField, index, indexType, docID, field, isShould) {
        this.queryParameters[referanceField] = {
            type: "termLookupMatch",
            isShould: isShould,
            params: {
                referanceField: referanceField,
                index: index,
                indexType: indexType,
                docID: docID,
                field: field
            }
        };
    },
    addTermMatch: function(field, value) {
        var values = [];
        if (typeof value === 'string') {
            values = [value];
        } else {
            values = value;
        }
        if (!this.queryParameters.hasOwnProperty(field)) {
            this.queryParameters[field] = {
                type: "termMatch",
                values: values
            };
        } else {
            for (var nValueIndex in values) {
                var nValue = values[nValueIndex];
                this.queryParameters[field].values.push(nValue);
            }
        }
    },
    removeTermMatch: function(field, value) {
        if (isNull(value)) {
            delete this.queryParameters[field];
        } else if (this.queryParameters.hasOwnProperty(field) && this.queryParameters[field].values.indexOf(value) != -1) {
            this.queryParameters[field].values.splice(this.queryParameters[field].values.indexOf(value), 1);
            if (this.queryParameters[field].values.length == 0) {
                delete this.queryParameters[field];
            }
        }
    },
    removeDateRange: function(field) {
        if (this.queryParameters.hasOwnProperty(field)) {
            delete this.queryParameters[field];
        }
    },
    addIndexPattern: function(prefix, format) {
        this.prefix = prefix;
        this.format = format;
    },
  /*  getIndices: function(fromDate, toDate, prefix, format) {
        var fromDate = helper.convertDate(fromDate);
        var toDate = helper.convertDate(toDate);
        console.log("UTC ", fromDate, toDate);

        fromDate = new Date(fromDate.getTime() + fromDate.getTimezoneOffset() * 60000);
        toDate = new Date(toDate.getTime() + toDate.getTimezoneOffset() * 60000);
        prefix = prefix;
        format = format;
        console.log("format.....", format);
        var indices = [];
        if (helper.isEmpty(format)) {
            return indices;
        }

        var tempDate = new Date(toDate);
        console.log("UTC ", fromDate, toDate, tempDate);
        while (tempDate >= fromDate) {

            var index = prefix + moment(tempDate).format(format);


            // console.log("UTC ",tempDate,index);
            if (indices.indexOf(index) == -1) {
                if (index.length > 0) {
                    indices.push(index);
                }
                if (index.localeCompare(config.firstAvaiableIndex) == 0) {
                    break;
                }
            }
            tempDate.setDate(tempDate.getDate() - 1);
        }
            while (tempDate <= toDate) {
             if(prefix=="twitter-"){
               console.log("index");
               var index = prefix +helper.getIndex(tempDate);
             }
            else{
            var index = prefix + moment(tempDate).format(format);
            }
            console.log("UTC ", tempDate, index);
            if (indices.indexOf(index) == -1) {
                if (index.length > 0) {
                    indices.push(index);
                }
                if (index.localeCompare(config.firstAvaiableIndex) == 0) {
                    break;
                }
            }
            tempDate.setDate(tempDate.getDate() + 1);
        }
        var index = indices.indexOf("twitter-2016-53");
        if (index > -1) {
            indices.splice(index, 1);
        }
       
        indices = indices.join(",");
        console.log('foamtree indices...', indices);
        return indices;
    },*/

  /*     getIndices: function(fromDate, toDate, prefix, format) {
        var fromDate = helper.convertDate(fromDate);
        var toDate = helper.convertDate(toDate);
        fromDate = new Date(fromDate.getTime() + fromDate.getTimezoneOffset() * 60000);
        toDate = new Date(toDate.getTime() + toDate.getTimezoneOffset() * 60000);
        var prefix = prefix;
        var format = format;
        var indices = [];
        if (helper.isEmpty(format)) {
            return indices;
        }
        var tempDate = new Date(fromDate);
        console.log("UTC ", fromDate, toDate, tempDate);
        while (tempDate <= toDate) {
             if(prefix=="twitter-"){
               console.log("index");
               var index = prefix +helper.getIndex(tempDate);
             }
            else{
            var index = prefix + moment(tempDate).format(format);
            }
            console.log("UTC ", tempDate, index);
            if (indices.indexOf(index) == -1) {
                if (index.length > 0) {
                    indices.push(index);
                }
                if (index.localeCompare(config.firstAvaiableIndex) == 0) {
                    break;
                }
            }
            tempDate.setDate(tempDate.getDate() + 1);
        }
        var index = indices.indexOf("twitter-2015-53");
        if (index > -1) {
            indices.splice(index, 1);
        }
        indices = indices.join(",");
        console.log('indices', indices);
        return indices;
    },*/

     getIndices: function(fromDate, toDate, prefix, format) {
        var fromDate = helper.convertDate(fromDate);
        var toDate = helper.convertDate(toDate);
        fromDate = new Date(fromDate.getTime() + fromDate.getTimezoneOffset() * 60000);
        toDate = new Date(toDate.getTime() + toDate.getTimezoneOffset() * 60000);
        var prefix = prefix;
        var format = format;
        var indices = [];
        if (helper.isEmpty(format)) {
            return indices;
        }
        var tempDate = new Date(fromDate);
        console.log("UTC ", fromDate, toDate, tempDate);
        while (tempDate <= toDate) {
             if(prefix=="twitter-"){
               console.log("index");
               var index = prefix +helper.getIndex(tempDate);
             }
            else{
            var index = prefix + moment(tempDate).format(format);
            }
            console.log("UTC ", tempDate, index);
            if (indices.indexOf(index) == -1) {
                if (index.length > 0) {
                    indices.push(index);
                }
                if (index.localeCompare(config.firstAvaiableIndex) == 0) {
                    break;
                }
            }
            tempDate.setDate(tempDate.getDate() + 1);
        }
        var index = indices.indexOf("twitter-2015-53");
        if (index > -1) {
            indices.splice(index, 1);
        }
        indices = indices.join(",");
        console.log('indices', indices);
        return indices;
    },
    addDateRangeMatch: function(field, from, to) {
        this.queryParameters[field] = {
            type: "dateRange",
            from: from,
            to: to
        };
        var fromDate = convertDate(from);
        var toDate = convertDate(to);
        console.log("From foamtree ", fromDate, " To foamtree ", toDate);
        var indices = this.getIndices(fromDate, toDate);
        this.indicesString = indices.join(",");
    },
    resetQuery: function(esBody) {
        delete esBody.search_request['query'];
        esBody.search_request['query'] = {
            "bool": {
                "must": []
            }
        };
        return esBody;
    }
}


module.exports = FoamTreeSearch;