function ElasticsearchClusterClient(esURL, esQueryFunction, esQueryParameters) {
	this.esURL = esURL;
	this.esQueryFunction = esQueryFunction;
	this.esQueryParameters = esQueryParameters;
	this.queryParameters = {};
}

ElasticsearchClusterClient.prototype = {
	constructor: ElasticsearchClusterClient,
	addQueryParameter: function (key, value) {
		this.esQueryParameters[key] = value;
	},
	search: function (ids,size, callBack) {
		var esBody = this.esQueryFunction(this.esQueryParameters);
		var size = size;
		esBody = {
			"size": size,
			query: {
					"ids": {
						"values": ids
					}
			}
		};
		$.ajax({
			type: "POST",
			url: this.esURL + "/_search",
			data: JSON.stringify(esBody),
			success: callBack,
			dataType: "JSON"
		});
	}
}

module.exports =ElasticsearchClusterClient;