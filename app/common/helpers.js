var moment=require('moment');
var self = module.exports={

 convertDate :function(d){

        console.log("D is " ,d);
        var currentDate = new Date();
        if(self.isNull(d)||(d=='')){
        
        }
        else if(d =="now-1M"){
            currentDate.setMonth(currentDate.getMonth()-1);
        }
        else if(d =="now-24h"){
            currentDate.setDate(currentDate.getDate()-1);
        }
        else if(d =="now-1w"){
            currentDate.setDate(currentDate.getDate()-7);
        }
        else if(d =="now-1h"){
            currentDate.setHours(currentDate.getHours() - 1);
        }
            else if(d =="now-4M"){
                currentDate.setMonth(currentDate.getMonth()-4);
            }
            else if(d =="now-1y"){
                currentDate.setMonth(currentDate.getMonth()-12);
            }
        else {
            console.log("custom date..",d);
            var date = d.replace("now-","");
            console.log("given date..",date);
            currentDate = moment(date, "YYYY-MM-DDTHH:mm:ss.SSSZ ").toDate();
            console.log("DATE is ",moment(date, "YYYY-MM-DDTHH:mm:ss.SSSZ").toDate());
        }
        
        console.log('current date',currentDate);
       return currentDate;
    },

   isNull: function (obj) {
    if (typeof obj === 'undefined' || obj == null) {
        return true;
    }
    return false;
    },
    
    isEmpty:function(text){
	return self.isWhitespaceOrEmpty(text);
    },

	isWhitespaceOrEmpty:function (text) {
	   return !self.isNull(text) && !/[^\s]/.test(text);
	},
    applyScoring:function(esClient,date,socialMedia,user){
    console.log("helper user server",user);
    esClient.addNotLookupTermsMatch("_id", "newsentiments", "relevancy", user, "hide." + socialMedia +".raw");
    esClient.addScoringFunction({
        "filter": {
            "terms": {
                "_id": {
                    "index": "newsentiments",
                    "type": "relevancy",
                    "id": user,
                    "path": "relevant." + socialMedia + ".raw",
                    "cache": false
                }
            }
        },
        "weight": 1000
    });
    esClient.addScoringFunction({
        "filter": {
            "terms": {
                "_id": {
                    "index": "newsentiments",
                    "type": "relevancy",
                    "id": user,
                    "path": "irrelevant."  + socialMedia + ".raw",
                    "cache": false
                }
            }
        },
        "weight": -1000
    });

    esClient.addScoringFunction({
        "field_value_factor": {
            "field": date,
            "factor": .00001,
              "missing": 0
        },
          "weight": 1000
    });


   },
     
      getIndex : function(date) {
      var currentDate = new Date(date.getFullYear(),0,1);
      return date.getFullYear()+'-'+self.padZero(Math.ceil((((date - currentDate) / 86400000) + currentDate.getDay()+1)/7));
  },

//     getIndex :function(date) { 
//     var determinedate = new Date(); 
//     determinedate.setFullYear(date.getFullYear(), date.getMonth(), date.getDate()); 
//     var D = determinedate.getDay(); 
//     if(D == 0) D = 7; 
//     determinedate.setDate(determinedate.getDate() + (4 - D)); 
//     var YN = determinedate.getFullYear(); 
//     var ZBDoCY = Math.floor((determinedate.getTime() - new Date(YN, 0, 1, -6)) / 86400000); 
//     var WN = 1 + Math.floor(ZBDoCY / 7); 
//     console.log("week no....",WN);
//     return WN; 
// },

   padZero:function(num){
     return (num < 10) ? ("0" + num) : num;
  }




}