var elasticsearch = require('elasticsearch');
var config=require('../configs');

var client = new elasticsearch.Client({
  host: config.esHost,
  log: false
});

module.exports = {

	search:function(indexName,indexType,data,cb) {
         client.search({
		  index:indexName,
		  type: indexType,
		  body:data
		}, function (error, response) {
		    if (error) {
				cb(error,null);
			} else{
				cb(null,response);
			};
		});
	}

}
