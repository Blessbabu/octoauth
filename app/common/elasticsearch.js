var helper = require('../common/helpers');
var moment = require('moment');
var config = require('../configs');
var esClient = require('../common/esClient');
var request = require('request');
var logger=require('../services/logger');

function Elasticsearch(esURL, esQueryFunction, esQueryParameters, user) {
    console.log("esURL", esURL);
    this.esURL = esURL;
    this.queryParameters = {};
    this.esQueryFunction = esQueryFunction;
    this.esQueryParameters = esQueryParameters;
    this.prefix = "";
    this.format = "";
    this.indicesString = "";
    this.typeString = "";
    this.isCount = false;
    this.scoreFunctions = [];
    this.indexType = "";
    this.user = user;

}

Elasticsearch.prototype = {
    constructor: Elasticsearch,
    addIndexType: function(indexType) {
        this.indexType = indexType
    },
    addQueryParameter: function(key, value) {
        this.esQueryParameters[key] = value;
    },
    setCount: function() {
        this.isCount = true;
    },
    getIndex: function() {
        this.esURL = this.esURL.replace(/\/$/, '');
        var tokens = this.esURL.split(/\//);
        if (tokens.length == 5) {
            this.indicesString = tokens[3];
            this.typeString = tokens[4];
        } else if (tokens.length == 4) {
            this.indicesString = tokens[3];
        }
        return this.indicesString;
    },
    addNotLookupTermsMatch: function(referanceField, index, type, docID, field) {
        this.queryParameters[referanceField] = {
            type: "notTermsMatch",
            params: {
                index: index,
                indexType: type,
                docID: docID,
                field: field,
                referanceField: referanceField
            }
        };
    },
    addScoringFunction: function(scoringFunction) {
        this.scoreFunctions.push(scoringFunction);
    },
    addMultiMatch: function(fields, keyWords) {
        if (fields == null) {
            fields = "_all";
        }
        this.queryParameters["_multiMatch"] = {
            type: "multiMatch",
            fields: fields,
            keyWords: keyWords
        };
    },
    getType: function() {
        this.getIndex();
        return this.typeString;
    },
    reset: function() {
        this.queryParameters = {};
        this.scoreFunctions = [];
    },
    isCount: function() {
        return this.isCount;
    },
    createQuery: function(queryParams, cb) {
        this.queryParameters = queryParams;
        // console.log("query",this.queryParameters);
        var esBody = this.esQueryFunction(this.esQueryParameters);

        esBody = this.resetQuery(esBody);

        for (var field in this.queryParameters) {
            if (this.queryParameters[field].type == "queryString") {
                esBody.query.function_score.query.bool.must.push({
                    "query_string": {
                        "default_field": field,
                        "query": this.queryParameters[field].text
                    }
                });
            } else if (this.queryParameters[field].type == "termMatch") {
                var termQuery = {
                    "terms": {}
                };
                termQuery.terms[field] = this.queryParameters[field].values;
                if (this.queryParameters[field].values.length != -1) {

                    if (this.queryParameters[field].isShould == true) {
                        esBody.query.function_score.filter.bool.should.push(termQuery);
                    } else {
                        esBody.query.function_score.filter.bool.must.push(termQuery);
                    }
                }
            } else if (this.queryParameters[field].type == "termLookupMatch") {
                var termQuery = {
                    "terms": {}
                };
                var params = this.queryParameters[field].params;
                termQuery.terms[field] = {
                    "index": params.index,
                    "type": params.indexType,
                    "id": params.docID,
                    "path": params.field,
                    "cache": false
                };
                if (this.queryParameters[field].isShould == true) {
                    esBody.query.function_score.filter.bool.should.push(termQuery);
                } else {
                    esBody.query.function_score.filter.bool.must.push(termQuery);
                }
            } else if (this.queryParameters[field].type == "multiMatch") {
                var termQuery = {
                    "query_string": {
                        fields: this.queryParameters[field].fields,
                        query: this.queryParameters[field].keyWords
                    }
                };
                esBody.query.function_score.query.bool.must.push(termQuery);
            }
            if (this.queryParameters[field].type == "dateRange") {
                var rangeQuery = {
                    "range": {}
                };
                rangeQuery.range[field] = {
                    "gte": this.queryParameters[field].from,
                    "lte": this.queryParameters[field].to,
                    "format": "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
                }
                if (this.queryParameters[field].from == null) {
                    delete rangeQuery.range[field].gte;
                }
                if ((this.queryParameters[field].to == null) || (this.queryParameters[field].to == '')) {
                    delete rangeQuery.range[field].lte;
                }
                esBody.query.function_score.filter.bool.must.push(rangeQuery);
            }
            if (this.queryParameters[field].type == "notTermsMatch") {
                var termQuery = {
                    "terms": {}
                };
                var params = this.queryParameters[field].params;
                termQuery.terms[field] = {
                    "index": params.index,
                    "type": params.indexType,
                    "id": params.docID,
                    "path": params.field,
                    "cache": false
                };
                esBody.query.function_score.filter.bool.must_not.push(termQuery);
            }
        }
        if (this.scoreFunctions.length != 0) {
            console.log("scoring not zero");
            esBody.query.function_score.functions = [];
            esBody.query.function_score.score_mode = "sum";
            esBody.query.function_score.boost_mode = "replace";
            for (var scoreFunctionIdx in this.scoreFunctions) {
                var scoreFunction = this.scoreFunctions[scoreFunctionIdx];
                esBody.query.function_score.functions.push(scoreFunction);
            }
        }


//         console.log("esBody",JSON.stringify(esBody));
        cb(esBody);
    },
    searchWithCustomAgg: function(agg, callBack) {
        var esBody = this.createQuery();
        esBody.aggs = agg;
        this.search(callBack, esBody);
    },
    search: function(callBack, esBody) {
        if (helper.isNull(esBody)) {
            esBody = this.createQuery();
        }
        var url = this.esURL;
        if (this.indicesString != "") {
            if (url[url.length - 1] == ',') {
                url += this.indicesString;
            } else {
                url += "/" + this.indicesString;
            }
        }
        if (this.indexType != "") {
            url = url + "/" + this.indexType;
        }
        var body = JSON.stringify(esBody);

        url = url + "/_search?ignore_unavailable"
        console.log("url...,", url);
        esClient.search(this.indicesString, this.indexType, body, function(err, response) {
            if (err) {
                console.log("not able to connect to server, call failed with " + err);
                callBack(err,null);
            } else {
                console.log(response);
                callBack(null,response);
            }


        });


    },

 

    resetQuery: function(esBody) {
        // delete esBody['query'];
        esBody['query'] = {
            "function_score": {
                "query": {
                    "bool": {
                        "must": []
                    }
                },
                "filter": {
                    "bool": {
                        "must": [],
                        "should": [],
                        "must_not": []
                    }
                }
            }
        };
        return esBody;
    },
    update: function(id, data, callBack) {

    
        var options = {
            uri: config.esHost + "/configs/config/" + id + "/_update",
            method: 'POST',
            body: JSON.stringify(data)
        };

        request(options, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                // console.log("update........................",body);
               logger.info({updated:JSON.stringify(data),id:id});
                callBack(null, body); // Show the HTML for the Google homepage. 
            } else {
                // console.log(body)
                console.log("not able to connect to server, call failed with " + error);
                callBack(error,null);
            }
        });


    },


     updateRest: function(id,index,type,data, callBack) {

    
        var options = {
            uri: config.esHost +"/"+index+"/"+type+"/" + id + "/_update",
            method: 'POST',
            body: JSON.stringify(data)
        };

        request(options, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                // console.log("update........................",body);
               // logger.info({updated:JSON.stringify(data),id:id});
                callBack(null, body); // Show the HTML for the Google homepage. 
            } else {
                // console.log(body)
                console.log("not able to connect to server, call failed with " + error);
                callBack(error,null);
            }
        });


    },
    updateContent: function(indexType,data, callBack) {

       console.log("update content data....",config.esHost + '/' + indexType + '?op_type=create');
        /*$.ajax({
            type: "POST",
            url: this.esURL + '/'+indexType+'?op_type=create',
            data: JSON.stringify(data),
            success: callBack,
            dataType: "JSON"
        });*/

        var options = {
            uri: config.esHost + '/' + indexType + '?op_type=create',
            method: 'POST',
            body: JSON.stringify(data)
        };

        request(options, function(error, response, body) {
            // comsole.log("error...",error):
            if (!error) {

                callBack(null, body); // Show the HTML for the Google homepage. 
            } else {
                // console.log("upate content error body...",JSON.stringify(error), JSON.stringify(response))
                // console.log("not able to connect to server, call failed with " + error);
                callBack(error,null);
            }
        });

    },

    addIndexPattern: function(prefix, format) {
        this.prefix = prefix;
        this.format = format;
    },
    getIndices: function(fromDate, toDate, prefix, format) {
        var fromDate = helper.convertDate(fromDate);
        var toDate = helper.convertDate(toDate);
        fromDate = new Date(fromDate.getTime() + fromDate.getTimezoneOffset() * 60000);
        toDate = new Date(toDate.getTime() + toDate.getTimezoneOffset() * 60000);
        prefix = prefix;
        format = format;
        var indices = [];
        if (helper.isEmpty(format)) {
            return indices;
        }
        var tempDate = new Date(fromDate);
        console.log("UTC ", fromDate, toDate, tempDate);
        while (tempDate <= toDate) {
             if(prefix=="twitter-"){
               console.log("index");
               var index = prefix +helper.getIndex(tempDate);
             }
            else{
            var index = prefix + moment(tempDate).format(format);
            }
            console.log("UTC ", tempDate, index);
            if (indices.indexOf(index) == -1) {
                if (index.length > 0) {
                    indices.push(index);
                }
                if (index.localeCompare(config.firstAvaiableIndex) == 0) {
                    break;
                }
            }
            tempDate.setDate(tempDate.getDate() + 1);
        }
        var index = indices.indexOf("twitter-2016-53");
        if (index > -1) {
            indices.splice(index, 1);
        }
      
        indices = indices.join(",");
        console.log('indices', indices);
        return indices;
    },
    reindex: function(id, updateData, callBack) {
        console.log("updateData", updateData)

 
        var options = {
            uri: config.esHost + "/configs/config/" + id,
            method: 'POST',
            body: JSON.stringify(updateData)

        };

        request(options, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                callBack(null, body); // Show the HTML for the Google homepage. 
            } else {
                // console.log(body)
                console.log("not able to connect to server, call failed with " + error);
                callBack(error,null);
            }
        });
    },
    validateQuery: function(callBack) {
        var esBody = this.createQuery();
        $.ajax({
            type: "POST",
            data: JSON.stringify(esBody),
            url: this.esURL + "/configs/config/_validate/query",
            success: callBack,
            error: callBack,
            dataType: "JSON"
        });
    },
    updateGet: function(id, callBack) {
  

        var options = {
            uri: config.esHost + "/configs/config/" + id,
            method: 'GET'
        };

        request(options, function(error, response, body) {

            if (!error) {

                callBack(null,body); // Show the HTML for the Google homepage. 

            } else {

                console.log("not able to connect to server, call failed with " + error);
                callBack(error,null);
            }
        });
    }
}

module.exports = Elasticsearch;