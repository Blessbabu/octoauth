var helper = require('./helpers');
var query = require('../Clients/Query');
var config = require('../configs');
var request = require('request');
var moment=require('moment');
var queryObject = {
    "fbQueryHome": query.fbQueryHome,
    "instagramQueryHome": query.instagramQueryHome,
    "youtubeQueryHome": query.youtubeQueryHome,
    "twitterQueryHome": query.twitterQueryHome,
    "mediaQueryHome": query.mediaQueryHome,
    "fbCommentsQuery": query.fbCommentsQuery,
    "fbQueryVoice": query.fbQueryVoice,
    "mediaQueryVoice": query.mediaQueryVoice,
    "twitterQueryVoice": query.twitterQueryVoice,
    "instagramQueryVoice": query.instagramQueryVoice,
    "youtubeQueryVoice": query.youtubeQueryVoice,
    "sunburstQuery": query.sunburstQuery,
    "followersAnalytics": query.followersAnalytics,
    "retweetsAnalytics": query.retweetsAnalytics,
    "mentionsAnalytics": query.mentionsAnalytics,
    "genIndFbSt": query.genIndFbSt,
    "viewsTimeLine": query.viewsTimeLine,
    "viewsByCountry": query.viewsByCountry


};


function ElasticsearchMultiQuery(esURL, user) {
    this.esURL = esURL;
    this.esInstances = [];
    this.queryParameters = {};
    this.esQueryParameters;
    this.esQueryFunction;
    this.scoreFunctions = [];
    this.user = user;
}

ElasticsearchMultiQuery.prototype = {
    applyESInstances: function(esInstances) {
        this.esInstances = esInstances;
    },
    reset: function() {
        for (var instanceIndex in this.esInstances) {
            var instance = this.esInstances[instanceIndex];
            if (!isNull(instance)) {
                instance.reset();
            }
        }
    },
    createQuery: function(queryParams, queryName, liveParameters) {
        this.queryParameters = queryParams;
        console.log("query parameters", this.queryParameters);
        this.esQueryFunction = queryObject[queryName];
        this.esQueryParameters = liveParameters;
        // console.log("query",this.queryParameters);
        var esBody = this.esQueryFunction(this.esQueryParameters, this.user);

        esBody = this.resetQuery(esBody);

        for (var field in this.queryParameters) {
            if (this.queryParameters[field].type == "queryString") {
                esBody.query.function_score.query.bool.must.push({
                    "query_string": {
                        "default_field": field,
                        "query": this.queryParameters[field].text
                    }
                });
            } else if (this.queryParameters[field].type == "termMatch") {
                var termQuery = {
                    "terms": {}
                };
                termQuery.terms[field] = this.queryParameters[field].values;
                if (this.queryParameters[field].values.length != -1) {

                    if (this.queryParameters[field].isShould == true) {
                        esBody.query.function_score.filter.bool.should.push(termQuery);
                    } else {
                        esBody.query.function_score.filter.bool.must.push(termQuery);
                    }
                }
            } else if (this.queryParameters[field].type == "termLookupMatch") {
                var termQuery = {
                    "terms": {}
                };
                var params = this.queryParameters[field].params;
                termQuery.terms[field] = {
                    "index": params.index,
                    "type": params.indexType,
                    "id": params.docID,
                    "path": params.field,
                    "cache": false
                };
                if (this.queryParameters[field].isShould == true) {
                    esBody.query.function_score.filter.bool.should.push(termQuery);
                } else {
                    esBody.query.function_score.filter.bool.must.push(termQuery);
                }
            } else if (this.queryParameters[field].type == "multiMatch") {
                var termQuery = {
                    "query_string": {
                        fields: this.queryParameters[field].fields,
                        query: this.queryParameters[field].keyWords
                    }
                };
                esBody.query.function_score.query.bool.must.push(termQuery);
            }
            if (this.queryParameters[field].type == "dateRange") {
                var rangeQuery = {
                    "range": {}
                };
                rangeQuery.range[field] = {
                    "gte": this.queryParameters[field].from,
                    "lte": this.queryParameters[field].to,
                    "format": "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
                }
                if (this.queryParameters[field].from == null) {
                    delete rangeQuery.range[field].gte;
                }
                if ((this.queryParameters[field].to == null) || (this.queryParameters[field].to == '')) {
                    delete rangeQuery.range[field].lte;
                }
                esBody.query.function_score.filter.bool.must.push(rangeQuery);
            }
            if (this.queryParameters[field].type == "notTermsMatch") {
                var termQuery = {
                    "terms": {}
                };
                var params = this.queryParameters[field].params;
                termQuery.terms[field] = {
                    "index": params.index,
                    "type": params.indexType,
                    "id": params.docID,
                    "path": params.field,
                    "cache": false
                };
                esBody.query.function_score.filter.bool.must_not.push(termQuery);
            }
        }
        if (this.scoreFunctions.length != 0) {
            esBody.query.function_score.functions = [];
            esBody.query.function_score.score_mode = "sum";
            esBody.query.function_score.boost_mode = "replace";
            for (var scoreFunctionIdx in this.scoreFunctions) {
                var scoreFunction = this.scoreFunctions[scoreFunctionIdx];
                esBody.query.function_score.functions.push(scoreFunction);
            }
        }


        return esBody;
    },
    resetQuery: function(esBody) {
        // delete esBody['query'];
        esBody['query'] = {
            "function_score": {
                "query": {
                    "bool": {
                        "must": []
                    }
                },
                "filter": {
                    "bool": {
                        "must": [],
                        "should": [],
                        "must_not": []
                    }
                }
            }
        };
        return esBody;
    },
    getIndices: function(fromDate, toDate, prefix, format) {
        var fromDate = helper.convertDate(fromDate);
        var toDate = helper.convertDate(toDate);
        fromDate = new Date(fromDate.getTime() + fromDate.getTimezoneOffset() * 60000);
        toDate = new Date(toDate.getTime() + toDate.getTimezoneOffset() * 60000);
        var prefix = prefix;
        var format = format;
        var indices = [];
        if (helper.isEmpty(format)) {
            return indices;
        }
        var tempDate = new Date(fromDate);
        console.log("UTC ", fromDate, toDate, tempDate);
        while (tempDate <= toDate) {
             if(prefix=="twitter-"){
               console.log("index");
               var index = prefix +helper.getIndex(tempDate);
             }
            else{
            var index = prefix + moment(tempDate).format(format);
            }
            console.log("UTC ", tempDate, index);
            if (indices.indexOf(index) == -1) {
                if (index.length > 0) {
                    indices.push(index);
                }
                if (index.localeCompare(config.firstAvaiableIndex) == 0) {
                    break;
                }
            }
            tempDate.setDate(tempDate.getDate() + 1);
        }
        var index = indices.indexOf("twitter-2015-53");
        if (index > -1) {
            indices.splice(index, 1);
        }
        indices = indices.join(",");
        console.log('indices', indices);
        return indices;
    },

    search: function(callBack) {
        var esBody = "";
        for (var esInstanceIndex in this.esInstances) {
            var esInstance = this.esInstances[esInstanceIndex];
              var esIndices;

            if (esInstance.prefix && esInstance.format)
               esIndices = this.getIndices(esInstance.fromDate, esInstance.toDate, esInstance.prefix, esInstance.format);
            else
                esIndices = esInstance.index;
            // esInstance=JSON.parse(esInstance);
            // console.log("esInstances..........", esInstance);
            var esIndex = esIndices;
            var esType = esInstance.type;
            var queryParams = esInstance.queryParams;
            var queryName = esInstance.queryName;
            var liveParams = esInstance.liveParams;
            var isCount = true; //esInstance.isCount();
            var header = {};
            if (!helper.isNull(esIndex)) {
                header['index'] = esIndex;
            }
            if (!helper.isNull(esType)) {
                header['type'] = esType;
            }
            header['ignore_unavailable'] = true;
            //            if (!isNull(isCount)) {
            //                header['search_type'] = "count";
            //            }
            // console.log(header);
            // console.log("multi search index..........",esIndex);
            esBody += JSON.stringify(header) + "\n";
            //console.log("JSONIFY is " + jsonify(esInstance.createQuery()));
            esBody += JSON.stringify(this.createQuery(queryParams, queryName, liveParams)) + "\n";
          
        }

        // console.log("esBody....",esBody);

        var options = {
            uri: config.esHost + "/_msearch",
            method: 'POST',
            body: esBody
        };

        request(options, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                callBack(null,body); // Show the HTML for the Google homepage. 
            } else {
                console.log("body.............",body);
                console.log("not able to connect to server, call failed with " + error);
                callBack(error,null);
            }
        });

    }
}

module.exports = ElasticsearchMultiQuery;