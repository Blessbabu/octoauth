var FoamTree = require('../common/FoamTreeSearch');
var config = require('../configs');
var elasticsearch = require('elasticsearch');
var request = require('request');
var esClient = new elasticsearch.Client({
    host: config.esHost,
    log: false
});

var query = require('./Query');
var queryObject = {
    "fbFoamTreeQuery": query.fbFoamTreeQuery,
    "twitterFoamTreeQuery": query.twitterFoamTreeQuery
};


module.exports = {


    search: function(body, cb) {
        // console.log(body.url);
        var esBody = "";
        var queryFunction = queryObject[body.queryName];
        var foamTree = new FoamTree(config.esHost, queryFunction, body.liveParams, body.queryParams);
        foamTree.createQuery(function(esGlobalBody) {

            console.log("body.url " + body);
            console.log("foamtree query..........",JSON.stringify(esGlobalBody));

            if (body.prefix && body.format)
                var indices = foamTree.getIndices(body.fromDate, body.toDate, body.prefix, body.format);

            console.log("foamtree Indices....", indices);

            urlTemp = config.esHost + "/" + indices + "/" + '_search_with_clusters';
            var options = {
                uri: urlTemp,
                method: 'POST',
                body: JSON.stringify(esGlobalBody)
            };

            request(options, function(error, response, body) {
              
                if (!error && response.statusCode == 200) {
                    // console.log("body....",body)
                        cb(null,body);
                        
                } else {
                        var body=JSON.parse(body);

                        if(body.error)
                           cb(body.error, null);
                        else
                          cb(error, null);
                }
            });

        });





    }
}