var Elastic = require('../common/elasticsearch');
var ElasticMultiQuery = require('../common/ElasticsearchMultiQuery');
var config = require('../configs');
var helper = require('../common/helpers');
var elasticsearch = require('elasticsearch');
var esClient = new elasticsearch.Client({
    host: config.esHost,
    log: false
});
var request = require('request');

var query = require('./Query');
var queryObject = {
    "twitterQuery": query.twitterMainQuery,
    "twitterPositiveQuery": query.positiveTweetsQuery,
    "twitterNegativeQuery": query.negativeTweetsQuery,
    "fbMainQuery": query.fbMainQuery,
    "twitterAnalyticsQuery": query.twitterAnalyticsQuery,
    "twitterInfluencersQuery": query.twitterInfluencersQuery,
    "youtubeMainQuery": query.youtubeMainQuery,
    "mediaMainQuery": query.mediaMainQuery,
    "instagramMainQuery": query.instagramMainQuery,
    "emptyQueryFunction": query.emptyQueryFunction,
    "mediaAnalyticsQuery": query.mediaAnalyticsQuery,
    "mediaInfluencersQuery": query.mediaInfluencersQuery,
    "twitterBubbleQuery": query.twitterBubbleQuery,
    "streamingLiveTweetsQuery": query.streamingLiveTweetsQuery,
    "favouritesTimelineQuery":query.favouritesTimelineQuery,
    "fbCommentsQuery":query.fbCommentsQuery,
    "ssmQueryFunction":query.ssmQueryFunction,
    "ssmFbUserListQuery":query.ssmFbUserListQuery
};

module.exports = {


    search: function(body, user, cb) {
        // console.log("body .....",body);
        var queryFunction = queryObject[body.queryName];
        // console.log("type",body.type);
        var elastic = new Elastic(config.esHost, queryFunction, body.liveParams, user);
        if (body.socialMedia) {
            helper.applyScoring(elastic, body.scoringField, body.socialMedia, user);
        }
        elastic.createQuery(body.queryParams, function(esQuery) {
             console.log("final query", JSON.stringify(esQuery));
            var indices;

            if (body.prefix && body.format)
                indices = elastic.getIndices(body.fromDate, body.toDate, body.prefix, body.format);
            else
                indices = body.index;

            console.log("indices.......", indices);
            console.log("ES query is ", JSON.stringify(esQuery));
            esClient.search({
                index: indices,
                ignoreUnavailable: true,
                type: body.type,
                body: esQuery
            }, function(error, response) {

                if (error) {
                    cb(error,null);
                } else {
                    cb(null,response);
                };

            });

        });

    },


    multiSearch: function(esInstances, user, cb) {
        // console.log("gettttt",esInstances);
        var esClientMulti = new ElasticMultiQuery(config.esHost, user);
        esClientMulti.applyESInstances(esInstances);
        esClientMulti.search(function(error,response){
            if (error)
                cb(error,null);
            else
                cb(null,response);
        });

    },

    validateQuery: function(body, user, callBack) {
        //    console.log("body .....",body);
        var queryFunction = queryObject[body.queryName];
        /*console.log("function",queryFunction);*/
        var elastic = new Elastic(config.esHost, queryFunction, body.liveParams, user);
        elastic.createQuery(body.queryParams, function(esBody) {
            var options = {
                uri: config.esHost + "/configs/config/_validate/query",
                method: 'POST',
                body: JSON.stringify(esBody)

            };

            request(options, function(error, response, body) {
                if (!error && response.statusCode == 200) {

                    callBack(null, body); // Show the HTML for the Google homepage. 
                } else {
                    // console.log(body)
                    console.log("not able to validate, call failed with " + error);
                    callBack(error,null);
                }
            });



        });

    }



}
