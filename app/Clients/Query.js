
module.exports={
twitterMainQuery :function (liveParameters,user) {
	var size = liveParameters.size || 10;
	var granularity = liveParameters.granularity || "hour";

	// var size =  10;
	// var granularity = "hour";

	var queryBody =
{
  "size": size,
  "aggs": {
    "geo" : {
      "geohash_grid" : {
        "field" : "geoLocationInfo.geo",
        "precision" :5
      },
      "aggs": {
          "sub_region": {
              "terms": {
                  "field": "geoLocationInfo.name"
              }
          }
      }
    },
	  "negative": {
				"filter": {
					"term": {
						"senti.overallSentiment": "negative"
					}
				},
				"aggs": {
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			},
			"positive": {
				"filter": {
					"term": {
						"senti.overallSentiment": "positive"
					}
				},
				"aggs": {
					"tweets": {
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			},
    "uniqueUsers": {
      "cardinality": {
        "field": "user.id"
      }
    },
    "topMentions": {
      "terms": {
        "field": "mention.screen_name",
        "size":15
      }
    },
    "retweetsCount": {
      "filter": {
        "term": {
          "tweetType": "tweet"
        }
      },
      "aggs": {
        "retweetSum": {
          "sum": {
            "field": "retweet_count"
          }
        },
        "favoriteSum": {
          "sum": {
            "field": "favorite_count"
          }
        },
        "followers": {
          "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
          }
        }
      }
    },
    "languages": {
      "terms": {
        "field": "language",
        "size": 50
      }
    },
    "locations": {
      "terms": {
        "field": "geoLocationInfo.countrycode"
      }
    },
    "tweetTypes": {
      "terms": {
        "field": "tweetType"
      },
        "aggs":{
            "byDays": {
			  "terms": {
				"script": "dateConversion",
				"params": {
				  "date_field": "created_at",
				  "format": "EEEEEE"
				}
			  }
			 },
			"byHours": {
			  "terms": {
				  "order" : { "_term" : "asc" },
				"script": "dateConversion",
				"params": {
				  "date_field": "created_at",
				  "format": "HH"
				},
                  "size": 24
			  }
			}
            
        }
    },
    "sentiments": {
      "terms": {
        "field": "senti.overallSentiment"
      }
    },
    "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": granularity
      },
      "aggs": {
        "tweetTypes": {
          "terms": {
            "field": "tweetType"
          }
        }
      }
    },
    "trendingTweets": {
      "top_hits": {
        "size": size,
        "sort": [
          {
            "retweet_count": {
              "order": "desc"
            }
          }
        ]
      }
    },
   "followers": {
        "scripted_metric": {
            "init_script_file": "init",
            "map_script_file": "map",
            "combine_script_file": "combine",
            "reduce_script_file": "reduce"
        }
    },

   "country_region": {

      "terms": {
        "size":size,
        "field": "geoLocationInfo.name"

      }
    }
 
      
  }
}
	return queryBody;
},


liveTweetQueryFunction :function () {
	var liveTweetQuery = {
		"size": 10,
		"query": {
			"bool": {
				"must": [
					{
						"term": {
							"owners": user
						}
        }
      ]
			}
		},
		"sort": [
			{
				"created_at": {
					"order": "desc"
				}
    }
  ]
	};

	return liveTweetQuery;
},

positiveTweetsQuery :function(liveParameters,user) {
  var size = liveParameters.size || 10;
  var granularity = liveParameters.granularity || "hour";

	// var size =  10;
	// var granularity = "hour";
	var queryBodyPositive = {
	"size" : 10,
		"aggs":{
			"positive" : {
			   "filter":{
			   		"term" : {
					  "senti.overallSentiment" : "positive"
					}
			   },
				"aggs":{
				  "tweets" :{
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			}
		}
	};
	return queryBodyPositive;
},


 negativeTweetsQuery :function(liveParameters,user){
	// var size =  10;
	// var granularity = "hour";
  var size = liveParameters.size || 10;
  var granularity = liveParameters.granularity || "hour";
	var queryBodyNegative = {
	"size" : 10,
		"aggs":{
			"negative" : {
			   "filter":{
			   		"term" : {
					  "senti.overallSentiment" : "negative"
					}
			   },
				"aggs":{
				  "tweets" :{
						"top_hits": {
							"size": size,
							"sort": [
								{
									"retweet_count": {
										"order": "desc"
									}
              }
            ]
						}
					}
				}
			}
		}
	};
	return queryBodyNegative;

},

fbMainQuery:function(liveParametersFb,user){
var size = liveParametersFb.size ||50;
var granularity = liveParametersFb.granularity || "hour";
var isFirstSearch = true;
// var size =  10;
// var granularity = "hour";
	
var queryBodyFb = {
  "size":size,
  "aggs": {
    "pieChartSentiFb": {
      "terms": {
        "field": "sentiments.overallSentiment"
      }
    },
    "differentPages": {
      "terms": {
        "field": "Page.name.raw",
        "size": size
      }
    },
    "totalPages": {
      "value_count": {
        "field": "Page.name.raw"
      }
    },
    "uniqueUsers": {
      "value_count": {
        "field": "comments.id"
      }
    },
    "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": granularity
      },
      "aggs": {
        "comments": {
          "terms": {
            "field": "CommentsSummary.commentsCount"
          }
        },
        "likes": {
          "terms": {
            "field": "likesCount"
          }
        },
        "shares": {
          "terms": {
            "field": "sharesCount"
          }
        }
      }
    },
    "postsCount": {
      "filter": {
        "term": {
          "_type": "post"
        }
      }
    },
    "sumOfLikes": {
      "sum": {
        "field": "likesCount"
      }
    },
    "sumOfShares": {
      "sum": {
        "field": "sharesCount"
      }
    },
    "pageCateogries": {
      "terms": {
        "field": "Page.category.raw"
      }
    }
  }
};
	return queryBodyFb;
	
},

fbCommentsQuery : function(queryParams,user){
	var queryBody = {
		"aggs":{ 
		  "byDays": {
			  "terms": {
				"script": "dateConversion",
				"params": {
				  "date_field": "created_at",
				  "format": "EEEEEE"
				}
			  },
        "aggs":{
            "byHours": {
        "terms": {
          "order" : { "_term" : "asc" },
        "script": "dateConversion",																																																																																																																																																																																																																																																																				
        "params": {
          "date_field": "created_at",
          "format": "H"
        }
        }
         }


        }
			 }
			
			}
    };


		return queryBody;
},
 fbFoamTreeQuery :function (liveParameters) {
    // var ftQuery = liveParameters.foamtreeQuery || "*";
      var ftQuery ="*";
    var request = {
        "search_request": {
            "fields": ["text"],
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "query": ftQuery
                            }
                        }
                        ]
                }
            },
            "sort": [
                {
                    "created_at": {
                        "order": "desc"
                    }
            }
         ],
            "size": 1000
        },

        "query_hint": "*",
        "algorithm": "lingo",
        "max_hits": 10,
        "include_hits": false,
        "field_mapping": {
            "title": ["fields.text"],
            "content": ["fields.text"],
            "url": ["fields._id"]
        }
    };
    return request;
},

twitterAnalyticsQuery :function (liveParametersAnalytics,user) {
  var size = liveParametersAnalytics.size || 10;
  var granularity = liveParametersAnalytics.granularity || "hour";
  var queryBody = {
    "size": 0,
  "aggs": {
    "topTags": {
      "terms": {
        "field": "hashtag.text",
     "size": size
      },
      "aggs": {
        "stats": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        },
        "trends": {
          "date_histogram": {
            "field": "created_at",
            "interval": granularity
          }
        }
      }
    }
  }
  };
  return queryBody;
},

twitterFoamTreeQuery :function (liveParameters) {
    // var ftQuery = liveParameters.foamtreeQuery || "*";
      var ftQuery = "*";
    var request = {
        "search_request": {
            "fields": ["text"],
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "query": ftQuery
                            }
                        }
                        ]
                }
            },
            "sort": [
                {
                    "created_at": {
                        "order": "desc"
                    }
            }
         ],
            "size": 1000
        },

        "query_hint": "*",
        "algorithm": "lingo",
        "max_hits": 10,
        "include_hits": false,
        "field_mapping": {
            "title": ["fields.text"],
            "content": ["fields.text"],
            "url": ["fields._id"]
        }
    };
    return request;
},

twitterInfluencersQuery:function (liveParameters,user) {
    var size = liveParameters.size || 18;
    if(liveParameters.sortIdentifier=="_count"||null){
        var order1 = {
        "retweetCount":"desc"
    }
    }
    else if(liveParameters.sortIdentifier=="max_followers.value"){
        var order1 = {
        "max_followers.value":"desc"
    } 
    }   
    var granularity = liveParameters.granularity || "hour";
    var queryBody = {
          "size": 0,
          "aggs": {
            "influencers": {
              "terms": {
                "field": "user.screen_name",
                "size": size,
                  "order":order1
              },
              "aggs": {
                "retweetCount" : {
                    "sum" : {
                        "field" : "retweet_count"
                    }
                },
                "personStats": {
                  "terms": {
                    "field": "tweetType"
                  }
                },
                "personInfo": {
                  "top_hits": {
                    "size": 1,
                    "sort": {
                      "created_at": {
                        "order": "desc"
                      }
                    }
                  }
                },
                "max_followers": {
                  "max": {
                    "field": "user.followers"
                  }
                }
              }
            }
          }
};
    return queryBody;
},

youtubeMainQuery :function (liveParametersYt,user) {
    console.log('live',liveParametersYt);
    var size = liveParametersYt.size || 12;
    var granularity = liveParametersYt.granularity || "day";
    //var sortVariable =  liveParametersYt.sortField || "likes.count";

    var queryBodyYt = {
        "from": liveParametersYt.pageNum,
        "size": size,
        "query": {},
        // "sort":[
        //    {
        //     "publishedAt": {
        //       "order": "asc"
        //     }
        //   }
        // ],
     "fields": [
        "statistics.likeCount",
        "statistics.dislikeCount",
        "statistics.viewCount",
        "statistics.commentCount",
        "statistics.favoriteCount",
        "publishedAt",
        "snippet.channelTitle.raw",
        "snippet.title",
        "snippet.channelTitle",
        "id",
          "snippet.thumbnails.high.url",
        "snippet.publishedAt"
        ],
        "aggs": {
            "likeCount": {
                "sum": {
                    "field": "statistics.likeCount"
                }
            },
            "disLikeCount": {
                "sum": {
                    "field": "statistics.dislikeCount"
                }
            },
            "viewCount": {
                "sum": {
                    "field": "statistics.viewCount"
                }
            },
            "commentCount": {
                "sum": {
                    "field": "statistics.commentCount"
                }
            },
            "favoriteCount": {
                "sum": {
                    "field": "statistics.favoriteCount"
                }
            },
            "trends": {
                "date_histogram": {
                    "field": "publishedAt",
                    "interval": granularity
                }
            },
            "channels": {
                "terms": {
                    "field": "snippet.channelTitle.raw",
                    "size": size
                },
                "aggs": {
                    "top_channel_hits": {
                        "top_hits": {}
                    }
                }
            }
        }

    };

    return queryBodyYt;

},

mediaMainQuery :function(liveParametersMedia,user){
var size = liveParametersMedia.size || 10;

var granularity = liveParametersMedia.granularity || "hour";
var sortVariable =liveParametersMedia.sort|| "asc";
var queryBodyMedia = {
  "from": liveParametersMedia.pageNum,
  "size":size,
  "fields": [
    "Title",
    "actualTimeStamp",
    "SourceName",
    "Link",
    "Tags",
    "Categories.SentimentOut.overallSentiment",
    "Categories.Types.Entities.Person.entities.entity",
    "Categories.Types.Entities.Country.entities.entity",
    "Categories.Types.Entities.Company.entities.entity",
    "Categories.Types.Entities.Regions.entities.entity",
    "Categories.Types.Entities.City.entities.entity",
    "Categories.Types.Entities.State.entities.entity"
  ],
  "highlight": {
    "fields": {
      "Content": {},
      "Title": {}
    }
  },
"sort" : {
"actualTimeStamp" : {
"order" :sortVariable
}
},
  "aggs": {
    "sources": {
      "terms": {
        "field": "SourceName",
        "size": size
      }
    },
    "trends": {
      "date_histogram": {
        "field": "actualTimeStamp",
        "interval": granularity
      }
    },
    "pieChartSentiMedia": {
      "terms": {
        "field": "Categories.SentimentOut.overallSentiment",
        "size": 30
      }
    },
    "country": {
      "terms": {
        "field": "Categories.Types.Entities.Country.entities.entity",
        "size": 30
      }
    }
  },
  "query": {
    "function_score": {
      "score_mode": "sum",
      "boost_mode": "sum",
      "functions": [
        {
          "filter": {
            "terms": {
              "_id": {
                "index": "newsentiments",
                "type": "relevancy",
                "id": user,
                "path": "relevant.media",
                "cache": false
              }
            }
          },
          "weight": 100000
        },
        {
          "filter": {
            "terms": {
              "_id": {
                "index": "newsentiments",
                "type": "relevancy",
                "id": user,
                "path": "irrelevant.media",
                "cache": false
              }
            }
          },
          "weight": -1000
        },
          {
                "field_value_factor": {
                  "field": "actualTimeStamp",
                  "factor": .00000001
                }
          }
      ],
      "filter": {
        "bool": {
          "must_not": [
            {
              "terms": {
                "_id": {
                  "index": "newsentiments",
                  "type": "relevancy",
                  "id": user,
                  "path": "hide.media",
                  "cache": false
                }
              }
            }
          ]
        }
      }
    }
  }
};
  return queryBodyMedia;
  
},

instagramMainQuery :function (liveParametersInstagram,user) {
  console.log("instagram params",liveParametersInstagram);
  var size = liveParametersInstagram.size || 10;
  //var granularity = liveParametersFb.granularity || "day";

  var sortVariable = liveParametersInstagram.sortField || "comments.count";
  var granularity = liveParametersInstagram.granularity || "hour";
    
  var queryBodyInsta = {
    "from": liveParametersInstagram.pageNum,
    "size": 21,
    "sort": [
      {
        "comments.count": {
          "order": "desc"
        }
    }
  ],
    "query": {
      "bool": {
        "must": []
      }
    },
    "aggs": {
      "trendingTags": {
        "terms": {
          "field": "tags",
          "size":size
        }
      },
      "graph_data_insta": {
        "date_histogram": {
          "field": "created_time",
          "interval": granularity
        }
      },
      "totalLikes":{
           "sum": {
          "field": "likes.count"
         }
      },
      "totalComment":{
        "sum" : {
          "field" : "comments.count"
        }
      }
    }
  };
  queryBodyInsta.sort[0] = {};
  queryBodyInsta.sort[0][sortVariable] = {
    "order": "desc"
  };
  return queryBodyInsta;

},

fbQueryHome :function (liveParametersHome,user) {
var size = liveParametersHome.size || 10;
  var granularity = liveParametersHome.granularity || "hour";
  var queryBody = {
    "size": 0,
    "aggs": {
               "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": "hour"
      }
    ,
        "aggs" : {
          "sentimentsFb": {
               "terms" : {
                  "field" : "sentiments.overallSentiment"
                    }
          },
      "comments": {
               "sum" : {
                  "field" : "CommentsSummary.commentsCount"
                    }
          }
    }
    }

    }
  };
  return queryBody;
},

 mediaQueryHome :function (liveParametersHome,user) {
var size = liveParametersHome.size || 10;
  var granularity = liveParametersHome.granularity || "hour";
  var queryBody = {
    "size": 0,
    "aggs": {
              "source_data": {
                "terms": {
                  "field": "SourceName"
                }
              }
        }
  };
  return queryBody;
},


 twitterQueryHome :function (liveParametersHome,user) {
var size = liveParametersHome.size || 10;
  var granularity = liveParametersHome.granularity || "hour";
  var queryBody = {
    "size": 0,
    "query": {
      "bool": {
        "must": [
          {
            "term": {
              "owners": user
            }
        },
          {
            "range": {
              "created_at": {
                "gt": "now-1w"
              }
            }
        }
      ]
      }
    },
    "aggs": {
             "trends" : {
                "date_histogram": {
          "field": "created_at",
          "interval": "hour"
        }        
            ,
                "aggs" : {
                          "sentimentsTwitter": {
                        "terms" : {
                            "field" : "senti.overallSentiment"
                    }

                   

                }}
      },
      "twitterSentimentsPie":{
         "terms":{
           "field": "senti.overallSentiment"
         }
      },
      "filterRetweets": {
        "filter": {
          "terms": {
            "tweetType": [ "retweet" , "reply"]
          }
        },
        "aggs": {
          "trendingUser": {
            "terms": {
              "field": "user.screen_name",
              "order": {
                "followers": "desc"
              },
              "size": 31
            },
            "aggs": {
              "followers": {
                "max": {
                  "field": "user.followers"
                }
              },
              "details": {
                "top_hits": {
                  "size": 1,
                  "sort" : { "created_at" : "desc" }
                }
              }
            }
          }
        }
      },
      tags: {
        terms: {
          field: "hashtag.text",
          size: 500
        },
        "aggs": {
          "sentiments": {
            "sum": {
              "field": "senti.overallScore"
            }
          }
        }
      }
    }
  };
  return queryBody;
},

instagramQueryHome :function (liveParametersHome,user) {
 return {};

 },

youtubeQueryHome :function (liveParametersHome,user) {
   return {};
 },


 fbQueryVoice :function (liveParametersTagCompare,user) {
var size = liveParametersTagCompare.size || 10;
  var granularity = liveParametersTagCompare.granularity || "hour";
  var queryBody = {
  "size": 0,
  "aggs": {
    "trends": {
      "date_histogram": {
        "field": "created_at",
        "interval": "hour"
      },
      "aggs": {
        "sentimentsFb": {
          "terms": {
            "field": "sentiments.overallSentiment"
          }
        },
        "comments": {
          "sum": {
            "field": "CommentsSummary.commentsCount"
          }
        }
      }
    },
    "sentimentsSunburst":{
        "terms":{
            "field":"sentiments.overallSentiment"
        }
    }
  }
};
  return queryBody;
},


 mediaQueryVoice :function (liveParametersTagCompare,user) {
var size = liveParametersTagCompare.size || 10;
  var granularity = liveParametersTagCompare.granularity || "hour";
  var queryBody = {
      "size": 0,
      "aggs": {
        "source_data": {
          "terms": {
            "field": "SourceName"
          }
        },
        "sentimentsSunburst": {
          "terms": {
            "field": "Categories.SentimentOut.overallSentiment"
          }
        }
      }
    };
  return queryBody;
},



twitterQueryVoice :function (liveParametersTagCompare,user) {
var size = liveParametersTagCompare.size || 10;
  var granularity = liveParametersTagCompare.granularity || "hour";
  var queryBody = {
    "size": 0,
    "query": {
      "bool": {
        "must": [
          {
            "term": {
              "owners": user
            }
        },
          {
            "range": {
              "created_at": {
                "gt": "now-1w"
              }
            }
        }
      ]
      }
    },
    "aggs": {
             "trends" : {
                "date_histogram": {
          "field": "created_at",
          "interval": "day"
        }        
            ,
                "aggs" : {
                          "sentimentsTwitter": {
                        "terms" : {
                            "field" : "senti.overallSentiment"
                    }

                   

                }}
      },
       "locations": {
             "terms": {
               "field": "place.country_code"
                }
             },
      "twitterSentimentsPie":{
         "terms":{
           "field": "senti.overallSentiment"
         }
      },
      "sentimentsSunburst":{
         "terms":{
           "field": "senti.overallSentiment"
         }
      },
      "filterRetweets": {
        "filter": {
          "terms": {
            "tweetType": [ "retweet" , "reply"]
          }
        },
        "aggs": {
          "trendingUser": {
            "terms": {
              "field": "user.screen_name",
              "order": {
                "followers": "desc"
              },
              "size": 31
            },
            "aggs": {
              "followers": {
                "max": {
                  "field": "user.followers"
                }
              },
              "details": {
                "top_hits": {
                  "size": 1,
                  "sort" : { "created_at" : "desc" }
                }
              }
            }
          }
        }
      },
      "tags": {
        "terms": {
          "field": "hashtag.text",
          "size": 500
        },
        "aggs": {
          "sentiments": {
            "sum": {
              "field": "senti.overallScore"
            }
          }
        }
      }
    }
  };
  return queryBody;
},

instagramQueryVoice :function (liveParametersTagCompare,user) {
 return {};
},
youtubeQueryVoice :function (liveParametersTagCompare,user) {
  return {};
},

sunburstQuery:function(parameters,user){
console.log("parameters",parameters);
var queryBody = {
  "query": {
    "terms": {
      "ownedBy.id": [
        parameters.user1,
        parameters.user2
      ]
    }
  },
  "aggs": {
    "user2": {
      "filter": {
        "term": {
          "ownedBy.id": parameters.user2
        }
      },
      "aggs": {
        "sentiments": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        }
      }
    },
    "user1": {
      "filter": {
        "term": {
          "ownedBy.id": parameters.user1
        }
      },
      "aggs": {
        "sentiments": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        }
      }
    }
  }
};
return queryBody;

},

// social Media Management

//analytics query for Retweets
retweetsAnalytics :function(parametersRetweetsAnalytics,user){
    var interval = parametersRetweetsAnalytics.interval || "hour";
    var queryBody = {
        "aggs" :{
            "trendsRetweetsAnalytics" : {
                "date_histogram":{
                    "field" : "created_at",
                    "interval" : interval
//          ,
//          "min_doc_count": 0
                }
            }
        }
    }
    return queryBody;
},
//analytics query for Followers   
mentionsAnalytics : function(parametersFollowersAnalytics,user){
    var interval = parametersFollowersAnalytics.interval ||"day";
    var queryBody = {
        "aggs" : {
            "mentionsAnalytics" :{
                "date_histogram" : {
                    "field" : "created_at",
                    "interval": interval
                }
            }
        }
    }
    return queryBody;
},
//analytics query for Mentions
followersAnalytics:function(parametersMentionsAnalytics,user){
  var interval = parametersMentionsAnalytics.interval ||"day";
  var queryBody = {
    "aggs":{
      "trendsFollowersAnalytics":{
        "date_histogram":{
                   "field":"created_at",
            "interval" : interval
//          ,
//          "min_doc_count": 0
                 },
                 "aggs": {
        "followers": {
          "top_hits": {
            "size": 1,
            "_source": [
              "followers_count"
            ]
          }
        }
      }
            }
        }
    }
    return queryBody;
},



viewsTimeLine :function(parametersViewsIndAnalytics,user){
var queryBody = {
/*  "aggs": {
    "viewsTimeLine": {
      "date_histogram": {
        "field": "created_at",
        "interval": "day"
      },
      "aggs": {
        "top_hits": {
          "top_hits": {
            "size": 1
          }
        }
      }
    }
  }*/


  "aggs": {
    "viewsTimeLine": {
      "date_histogram": {
        "field": "created_at",
        "interval": "day"
      },
      "aggs": {
        "top_hits": {
          "top_hits": {
            "size": 1
          }
        }
      }
    },
    "min_views": {
      "min": {
        "field": "views"
      }
    }
  }
}
return queryBody
},

 viewsByCountry :function(parametersViewsCountryIndAnalytics,user){
    var queryBody = {
     "aggs": {
    "date_sorted": {
      "top_hits": {
        "size": 1,
        "sort": {
          "created_at": {
            "order": "desc"
          }
        }
      }
    }
  }
    
    }
    return queryBody;
},

 genIndFbSt :function(parametersGenIndAnalytics,user){
var queryBody = {
 "aggs": {
    "sumOfLikes": {
      "sum": {
        "field": "likesCount"
      }
    },
    "sumOfShares": {
      "sum": {
        "field": "sharesCount"
      }
    }
  }
}
return queryBody
},



 emptyQueryFunction:function(liveParameterEmpty,user){
  var queryBody = {};
  return queryBody
},

mediaAnalyticsQuery:function (liveParametersAnalytics) {
  var size = liveParametersAnalytics.size || 10;
  var granularity = liveParametersAnalytics.granularity || "hour";
  var queryBody = {
    "size": 0,
  "aggs": {
    "topTags": {
      "terms": {
        "field": "hashtag.text",
     "size": size
      },
      "aggs": {
        "stats": {
          "terms": {
            "field": "senti.overallSentiment"
          }
        },
        "trends": {
          "date_histogram": {
            "field": "created_at",
            "interval": granularity
          }
        }
      }
    }
  }
  };
  return queryBody;
},

mediaInfluencersQuery :function (liveParameters) {
    var size = liveParameters.size || 18;
    if(liveParameters.sortIdentifier=="_count"||null){
        var order1 = {
        "retweetCount":"desc"
    }
    }
    else if(liveParameters.sortIdentifier=="max_followers.value"){
        var order1 = {
        "max_followers.value":"desc"
    } 
    }   
    var granularity = liveParameters.granularity || "hour";
    var queryBody = {
          "size": 0,
          "aggs": {
            "influencers": {
              "terms": {
                "field": "user.screen_name",
                "size": size,
                  "order":order1
              },
              "aggs": {
                "retweetCount" : {
                    "sum" : {
                        "field" : "retweet_count"
                    }
                },
                "personStats": {
                  "terms": {
                    "field": "tweetType"
                  }
                },
                "personInfo": {
                  "top_hits": {
                    "size": 1,
                    "sort": {
                      "created_at": {
                        "order": "desc"
                      }
                    }
                  }
                },
                "max_followers": {
                  "max": {
                    "field": "user.followers"
                  }
                }
              }
            }
          }
};
    return queryBody;
},




 twitterBubbleQuery:function (liveParameters) {
    var queryBody = {
        "size": 10,
        "sort": {
            "retweet_count": {
                "order": "desc"
            }
        }
    };
    return queryBody;
},

streamingLiveTweetsQuery : function(liveParametersAnalytics){
	var size = liveParametersAnalytics.sizeStream || 10;
	var queryBody = {
		"size": size,
		  "sort": [
			{
			  "created_at": {
				"order": "desc"
			  }
			}
		  ]
		};
	return queryBody;
},

favouritesTimelineQuery:function(){

  var size = 10;
  var queryBody = {
    "size": size,
      "sort": [
      {
        "created_at": {
        "order": "desc"
        }
      }
      ]
    };
  return queryBody;



},
ssmQueryFunction:function(liveParameter,user){
     var size = liveParameter.size || 12;
     var queryBody = {
        "from": liveParameter.pageNum,
        "size": size,
        "query": {}
      }
       return queryBody;
},

   
ssmFbUserListQuery:function(liveParameter,user){
 var size = liveParameter.size || 12;

var queryBody = {
  "size": size,
  "query": {
    "bool": {
      "must": [
        {
          "query_string": {
            "query": "(_missing_:isDeleted) NOT isDeleted:true"
          }
        },
        {
          "term": {
            "userName":user
          }
        }
      ]
    }
  }
}

 return queryBody;
}

}


